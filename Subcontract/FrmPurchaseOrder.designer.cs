﻿namespace ACV
{
    partial class FrmPurchaseOrder
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        //protected override void Dispose(bool disposing)
        //{
        //    if (disposing && (components != null))
        //    {
        //        components.Dispose();
        //    }
        //    base.Dispose(disposing);
        //}

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmPurchaseOrder));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.Genpan = new System.Windows.Forms.Panel();
            this.txtscr1 = new System.Windows.Forms.TextBox();
            this.label60 = new System.Windows.Forms.Label();
            this.dtpfnt = new System.Windows.Forms.DateTimePicker();
            this.txtscr9 = new System.Windows.Forms.TextBox();
            this.txtscr7 = new System.Windows.Forms.TextBox();
            this.txtscr6 = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.txtscr4 = new System.Windows.Forms.TextBox();
            this.Txtscr2 = new System.Windows.Forms.TextBox();
            this.HFGP = new System.Windows.Forms.DataGridView();
            this.txtscr5 = new System.Windows.Forms.TextBox();
            this.txtscr8 = new System.Windows.Forms.TextBox();
            this.Txtscr3 = new System.Windows.Forms.TextBox();
            this.dateTimePicker1 = new System.Windows.Forms.DateTimePicker();
            this.Taxpan = new System.Windows.Forms.Panel();
            this.txttotaddd = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.addipan = new System.Windows.Forms.Panel();
            this.label56 = new System.Windows.Forms.Label();
            this.HFGT = new System.Windows.Forms.DataGridView();
            this.panadd = new System.Windows.Forms.Panel();
            this.txtchargessum = new System.Windows.Forms.TextBox();
            this.txtbasicval = new System.Windows.Forms.TextBox();
            this.txttax = new System.Windows.Forms.TextBox();
            this.txttotamt = new System.Windows.Forms.TextBox();
            this.label58 = new System.Windows.Forms.Label();
            this.button13 = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.lblno1 = new System.Windows.Forms.Label();
            this.lblno2 = new System.Windows.Forms.Label();
            this.flowLayoutPanel3 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel2 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.buttnnvfst = new System.Windows.Forms.Button();
            this.buttnnxtlft = new System.Windows.Forms.Button();
            this.btnfinnxt = new System.Windows.Forms.Button();
            this.buttrnxt = new System.Windows.Forms.Button();
            this.buttnext1 = new System.Windows.Forms.Button();
            this.chkact = new System.Windows.Forms.CheckBox();
            this.butedit = new System.Windows.Forms.Button();
            this.button6 = new System.Windows.Forms.Button();
            this.butcan = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.btnBagNoMapping = new System.Windows.Forms.Button();
            this.Editpan = new System.Windows.Forms.Panel();
            this.PanelSearch = new System.Windows.Forms.Panel();
            this.btnSelect = new System.Windows.Forms.Button();
            this.btnHide = new System.Windows.Forms.Button();
            this.DataGridCommon = new System.Windows.Forms.DataGridView();
            this.txtprice = new System.Windows.Forms.TextBox();
            this.label35 = new System.Windows.Forms.Label();
            this.label36 = new System.Windows.Forms.Label();
            this.label57 = new System.Windows.Forms.Label();
            this.label59 = new System.Windows.Forms.Label();
            this.label54 = new System.Windows.Forms.Label();
            this.txtttot = new System.Windows.Forms.TextBox();
            this.txtigval = new System.Windows.Forms.TextBox();
            this.label53 = new System.Windows.Forms.Label();
            this.txttbval = new System.Windows.Forms.TextBox();
            this.label45 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.txtNetValue = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.txtRoundOff = new System.Windows.Forms.TextBox();
            this.txtpadd1 = new System.Windows.Forms.RichTextBox();
            this.label55 = new System.Windows.Forms.Label();
            this.label40 = new System.Windows.Forms.Label();
            this.txttrans = new System.Windows.Forms.TextBox();
            this.Txttot = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtot = new System.Windows.Forms.TextBox();
            this.button3 = new System.Windows.Forms.Button();
            this.txtamt = new System.Windows.Forms.TextBox();
            this.button2 = new System.Windows.Forms.Button();
            this.DTPDOCDT = new System.Windows.Forms.DateTimePicker();
            this.HFIT = new System.Windows.Forms.DataGridView();
            this.Dtpdt = new System.Windows.Forms.DateTimePicker();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.txtname = new System.Windows.Forms.TextBox();
            this.Phone = new System.Windows.Forms.Label();
            this.txtgrn = new System.Windows.Forms.TextBox();
            this.txtpuid = new System.Windows.Forms.TextBox();
            this.txtgrnid = new System.Windows.Forms.TextBox();
            this.txtdcid = new System.Windows.Forms.TextBox();
            this.txttitemid = new System.Windows.Forms.TextBox();
            this.txtpluid = new System.Windows.Forms.TextBox();
            this.label44 = new System.Windows.Forms.Label();
            this.txtuom = new System.Windows.Forms.TextBox();
            this.Dtprem = new System.Windows.Forms.DateTimePicker();
            this.label39 = new System.Windows.Forms.Label();
            this.Dtppre = new System.Windows.Forms.DateTimePicker();
            this.label38 = new System.Windows.Forms.Label();
            this.pantax = new System.Windows.Forms.Panel();
            this.txttitem = new System.Windows.Forms.RichTextBox();
            this.label30 = new System.Windows.Forms.Label();
            this.txttqty = new System.Windows.Forms.TextBox();
            this.label29 = new System.Windows.Forms.Label();
            this.Txtrate = new System.Windows.Forms.TextBox();
            this.label28 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.txtbasic = new System.Windows.Forms.TextBox();
            this.label23 = new System.Windows.Forms.Label();
            this.button5 = new System.Windows.Forms.Button();
            this.txttotal = new System.Windows.Forms.TextBox();
            this.label24 = new System.Windows.Forms.Label();
            this.txtigcst = new System.Windows.Forms.TextBox();
            this.cboigst = new System.Windows.Forms.ComboBox();
            this.label22 = new System.Windows.Forms.Label();
            this.txtsgst = new System.Windows.Forms.TextBox();
            this.SGST = new System.Windows.Forms.ComboBox();
            this.label21 = new System.Windows.Forms.Label();
            this.txtcgst = new System.Windows.Forms.TextBox();
            this.cbocgst = new System.Windows.Forms.ComboBox();
            this.label20 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.txtper = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.txttaxable = new System.Windows.Forms.TextBox();
            this.txthidqty = new System.Windows.Forms.TextBox();
            this.txttempadd2 = new System.Windows.Forms.TextBox();
            this.txttempadd1 = new System.Windows.Forms.TextBox();
            this.button8 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.label33 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.txtitemname = new System.Windows.Forms.TextBox();
            this.label42 = new System.Windows.Forms.Label();
            this.buttcusok = new System.Windows.Forms.Button();
            this.label32 = new System.Windows.Forms.Label();
            this.label31 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.txtnotes = new System.Windows.Forms.TextBox();
            this.txtbval = new System.Windows.Forms.TextBox();
            this.txtqty = new System.Windows.Forms.TextBox();
            this.dtpdc = new System.Windows.Forms.DateTimePicker();
            this.txtitemcode = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label37 = new System.Windows.Forms.Label();
            this.txtplace = new System.Windows.Forms.TextBox();
            this.txtpadd2 = new System.Windows.Forms.RichTextBox();
            this.label41 = new System.Windows.Forms.Label();
            this.txtlisid = new System.Windows.Forms.TextBox();
            this.txttgstval = new System.Windows.Forms.TextBox();
            this.txttgstp = new System.Windows.Forms.TextBox();
            this.buttnnxt = new System.Windows.Forms.Button();
            this.btnaddrcan = new System.Windows.Forms.Button();
            this.button11 = new System.Windows.Forms.Button();
            this.button12 = new System.Windows.Forms.Button();
            this.txtgen2 = new System.Windows.Forms.TextBox();
            this.txtgen3 = new System.Windows.Forms.TextBox();
            this.txtigstp = new System.Windows.Forms.TextBox();
            this.label52 = new System.Windows.Forms.Label();
            this.txtsgstp = new System.Windows.Forms.TextBox();
            this.label50 = new System.Windows.Forms.Label();
            this.label51 = new System.Windows.Forms.Label();
            this.txttcgval = new System.Windows.Forms.TextBox();
            this.txttcgstp = new System.Windows.Forms.TextBox();
            this.label49 = new System.Windows.Forms.Label();
            this.label48 = new System.Windows.Forms.Label();
            this.txttsgval = new System.Windows.Forms.TextBox();
            this.txtRefNo = new System.Windows.Forms.TextBox();
            this.dcdate = new System.Windows.Forms.DateTimePicker();
            this.label34 = new System.Windows.Forms.Label();
            this.txtcharges = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.txtlistid = new System.Windows.Forms.TextBox();
            this.label61 = new System.Windows.Forms.Label();
            this.txttaxtot = new System.Windows.Forms.TextBox();
            this.label77 = new System.Windows.Forms.Label();
            this.txtTaxvalue = new System.Windows.Forms.TextBox();
            this.cboTax = new System.Windows.Forms.ComboBox();
            this.label76 = new System.Windows.Forms.Label();
            this.button7 = new System.Windows.Forms.Button();
            this.label78 = new System.Windows.Forms.Label();
            this.txtNoBag = new System.Windows.Forms.TextBox();
            this.txtexcise = new System.Windows.Forms.TextBox();
            this.label46 = new System.Windows.Forms.Label();
            this.txttdisc = new System.Windows.Forms.TextBox();
            this.label43 = new System.Windows.Forms.Label();
            this.txttdis = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label79 = new System.Windows.Forms.Label();
            this.txtHSNCode = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.txtrem = new System.Windows.Forms.RichTextBox();
            this.txtSupplyToAdd = new System.Windows.Forms.RichTextBox();
            this.label47 = new System.Windows.Forms.Label();
            this.txttprdval = new System.Windows.Forms.TextBox();
            this.label80 = new System.Windows.Forms.Label();
            this.txtdcno = new System.Windows.Forms.TextBox();
            this.HFGST = new System.Windows.Forms.DataGridView();
            this.txtSupplyTo = new System.Windows.Forms.TextBox();
            this.label82 = new System.Windows.Forms.Label();
            this.txtMillName = new System.Windows.Forms.TextBox();
            this.label81 = new System.Windows.Forms.Label();
            this.label85 = new System.Windows.Forms.Label();
            this.label84 = new System.Windows.Forms.Label();
            this.label86 = new System.Windows.Forms.Label();
            this.label83 = new System.Windows.Forms.Label();
            this.mappnl = new System.Windows.Forms.Panel();
            this.button16 = new System.Windows.Forms.Button();
            this.txtlotno = new System.Windows.Forms.TextBox();
            this.txttarewt = new System.Windows.Forms.TextBox();
            this.txtstart = new System.Windows.Forms.TextBox();
            this.txtqty1 = new System.Windows.Forms.TextBox();
            this.button10 = new System.Windows.Forms.Button();
            this.button9 = new System.Windows.Forms.Button();
            this.label74 = new System.Windows.Forms.Label();
            this.label73 = new System.Windows.Forms.Label();
            this.label72 = new System.Windows.Forms.Label();
            this.txtbags = new System.Windows.Forms.TextBox();
            this.label71 = new System.Windows.Forms.Label();
            this.label70 = new System.Windows.Forms.Label();
            this.txtlotno1 = new System.Windows.Forms.TextBox();
            this.button14 = new System.Windows.Forms.Button();
            this.cbosGReturnItem = new System.Windows.Forms.ComboBox();
            this.label62 = new System.Windows.Forms.Label();
            this.txtNoogBags = new System.Windows.Forms.TextBox();
            this.label63 = new System.Windows.Forms.Label();
            this.txtgrossgen = new System.Windows.Forms.TextBox();
            this.DataGridWeight = new System.Windows.Forms.DataGridView();
            this.label64 = new System.Windows.Forms.Label();
            this.txtWeight = new System.Windows.Forms.TextBox();
            this.label65 = new System.Windows.Forms.Label();
            this.txtBagNo = new System.Windows.Forms.TextBox();
            this.label66 = new System.Windows.Forms.Label();
            this.btnOk = new System.Windows.Forms.Button();
            this.label67 = new System.Windows.Forms.Label();
            this.txtGrossWght = new System.Windows.Forms.TextBox();
            this.label68 = new System.Windows.Forms.Label();
            this.txtTarWght = new System.Windows.Forms.TextBox();
            this.label69 = new System.Windows.Forms.Label();
            this.txtlotno2 = new System.Windows.Forms.TextBox();
            this.txtstart1 = new System.Windows.Forms.TextBox();
            this.txttarewt1 = new System.Windows.Forms.TextBox();
            this.label75 = new System.Windows.Forms.Label();
            this.txtNofBags = new System.Windows.Forms.TextBox();
            this.btnsave = new System.Windows.Forms.Button();
            this.buttnfinbk = new System.Windows.Forms.Button();
            this.Genpan.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.HFGP)).BeginInit();
            this.Taxpan.SuspendLayout();
            this.addipan.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.HFGT)).BeginInit();
            this.panadd.SuspendLayout();
            this.panel1.SuspendLayout();
            this.Editpan.SuspendLayout();
            this.PanelSearch.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridCommon)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.HFIT)).BeginInit();
            this.pantax.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.HFGST)).BeginInit();
            this.mappnl.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridWeight)).BeginInit();
            this.SuspendLayout();
            // 
            // Genpan
            // 
            this.Genpan.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.Genpan.Controls.Add(this.txtscr1);
            this.Genpan.Controls.Add(this.label60);
            this.Genpan.Controls.Add(this.dtpfnt);
            this.Genpan.Controls.Add(this.txtscr9);
            this.Genpan.Controls.Add(this.txtscr7);
            this.Genpan.Controls.Add(this.txtscr6);
            this.Genpan.Controls.Add(this.label7);
            this.Genpan.Controls.Add(this.txtscr4);
            this.Genpan.Controls.Add(this.Txtscr2);
            this.Genpan.Controls.Add(this.HFGP);
            this.Genpan.Controls.Add(this.txtscr5);
            this.Genpan.Controls.Add(this.txtscr8);
            this.Genpan.Controls.Add(this.Txtscr3);
            this.Genpan.Controls.Add(this.dateTimePicker1);
            this.Genpan.Location = new System.Drawing.Point(-1, -1);
            this.Genpan.Margin = new System.Windows.Forms.Padding(4);
            this.Genpan.Name = "Genpan";
            this.Genpan.Size = new System.Drawing.Size(1226, 462);
            this.Genpan.TabIndex = 187;
            // 
            // txtscr1
            // 
            this.txtscr1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtscr1.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtscr1.Location = new System.Drawing.Point(1, 43);
            this.txtscr1.Margin = new System.Windows.Forms.Padding(5);
            this.txtscr1.Name = "txtscr1";
            this.txtscr1.Size = new System.Drawing.Size(1220, 26);
            this.txtscr1.TabIndex = 1;
            this.txtscr1.TextChanged += new System.EventHandler(this.txtscr1_TextChanged);
            // 
            // label60
            // 
            this.label60.AutoSize = true;
            this.label60.Font = new System.Drawing.Font("Calibri", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label60.Location = new System.Drawing.Point(527, 11);
            this.label60.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label60.Name = "label60";
            this.label60.Size = new System.Drawing.Size(58, 21);
            this.label60.TabIndex = 225;
            this.label60.Text = "Month";
            // 
            // dtpfnt
            // 
            this.dtpfnt.CustomFormat = "MMM/yyyy";
            this.dtpfnt.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpfnt.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpfnt.Location = new System.Drawing.Point(586, 10);
            this.dtpfnt.Margin = new System.Windows.Forms.Padding(4);
            this.dtpfnt.Name = "dtpfnt";
            this.dtpfnt.Size = new System.Drawing.Size(104, 26);
            this.dtpfnt.TabIndex = 224;
            this.dtpfnt.Value = new System.DateTime(2017, 7, 4, 0, 0, 0, 0);
            this.dtpfnt.ValueChanged += new System.EventHandler(this.dtpfnt_ValueChanged);
            // 
            // txtscr9
            // 
            this.txtscr9.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtscr9.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtscr9.Location = new System.Drawing.Point(1105, 43);
            this.txtscr9.Margin = new System.Windows.Forms.Padding(5);
            this.txtscr9.Name = "txtscr9";
            this.txtscr9.Size = new System.Drawing.Size(112, 26);
            this.txtscr9.TabIndex = 205;
            // 
            // txtscr7
            // 
            this.txtscr7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtscr7.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtscr7.Location = new System.Drawing.Point(944, 43);
            this.txtscr7.Margin = new System.Windows.Forms.Padding(5);
            this.txtscr7.Name = "txtscr7";
            this.txtscr7.Size = new System.Drawing.Size(60, 26);
            this.txtscr7.TabIndex = 203;
            // 
            // txtscr6
            // 
            this.txtscr6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtscr6.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtscr6.Location = new System.Drawing.Point(847, 43);
            this.txtscr6.Margin = new System.Windows.Forms.Padding(5);
            this.txtscr6.Name = "txtscr6";
            this.txtscr6.Size = new System.Drawing.Size(98, 26);
            this.txtscr6.TabIndex = 202;
            this.txtscr6.TextChanged += new System.EventHandler(this.txtscr6_TextChanged);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Calibri", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(2, 11);
            this.label7.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(117, 21);
            this.label7.TabIndex = 201;
            this.label7.Text = "Purchase Order";
            // 
            // txtscr4
            // 
            this.txtscr4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtscr4.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtscr4.Location = new System.Drawing.Point(312, 43);
            this.txtscr4.Margin = new System.Windows.Forms.Padding(5);
            this.txtscr4.Name = "txtscr4";
            this.txtscr4.Size = new System.Drawing.Size(96, 26);
            this.txtscr4.TabIndex = 100;
            this.txtscr4.TextChanged += new System.EventHandler(this.txtscr4_TextChanged);
            // 
            // Txtscr2
            // 
            this.Txtscr2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Txtscr2.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txtscr2.Location = new System.Drawing.Point(89, 43);
            this.Txtscr2.Margin = new System.Windows.Forms.Padding(5);
            this.Txtscr2.Name = "Txtscr2";
            this.Txtscr2.Size = new System.Drawing.Size(90, 26);
            this.Txtscr2.TabIndex = 87;
            this.Txtscr2.TextChanged += new System.EventHandler(this.Txtscr2_TextChanged);
            // 
            // HFGP
            // 
            this.HFGP.BackgroundColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.HFGP.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.HFGP.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnF2;
            this.HFGP.Location = new System.Drawing.Point(1, 74);
            this.HFGP.Margin = new System.Windows.Forms.Padding(4);
            this.HFGP.Name = "HFGP";
            this.HFGP.ReadOnly = true;
            this.HFGP.Size = new System.Drawing.Size(1222, 388);
            this.HFGP.TabIndex = 3;
            this.HFGP.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.HFGP_CellContentClick);
            this.HFGP.CellMouseUp += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.HFGP_CellMouseUp);
            this.HFGP.KeyUp += new System.Windows.Forms.KeyEventHandler(this.HFGP_KeyUp);
            // 
            // txtscr5
            // 
            this.txtscr5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtscr5.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtscr5.Location = new System.Drawing.Point(408, 43);
            this.txtscr5.Margin = new System.Windows.Forms.Padding(5);
            this.txtscr5.Name = "txtscr5";
            this.txtscr5.Size = new System.Drawing.Size(441, 26);
            this.txtscr5.TabIndex = 90;
            this.txtscr5.TextChanged += new System.EventHandler(this.txtscr5_TextChanged);
            // 
            // txtscr8
            // 
            this.txtscr8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtscr8.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtscr8.Location = new System.Drawing.Point(1003, 43);
            this.txtscr8.Margin = new System.Windows.Forms.Padding(5);
            this.txtscr8.Name = "txtscr8";
            this.txtscr8.Size = new System.Drawing.Size(103, 26);
            this.txtscr8.TabIndex = 204;
            // 
            // Txtscr3
            // 
            this.Txtscr3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Txtscr3.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txtscr3.Location = new System.Drawing.Point(178, 43);
            this.Txtscr3.Margin = new System.Windows.Forms.Padding(5);
            this.Txtscr3.Name = "Txtscr3";
            this.Txtscr3.Size = new System.Drawing.Size(134, 26);
            this.Txtscr3.TabIndex = 88;
            this.Txtscr3.TextChanged += new System.EventHandler(this.Txtscr3_TextChanged);
            // 
            // dateTimePicker1
            // 
            this.dateTimePicker1.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dateTimePicker1.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dateTimePicker1.Location = new System.Drawing.Point(127, 101);
            this.dateTimePicker1.Margin = new System.Windows.Forms.Padding(4);
            this.dateTimePicker1.Name = "dateTimePicker1";
            this.dateTimePicker1.Size = new System.Drawing.Size(113, 22);
            this.dateTimePicker1.TabIndex = 334;
            // 
            // Taxpan
            // 
            this.Taxpan.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.Taxpan.Controls.Add(this.txttotaddd);
            this.Taxpan.Controls.Add(this.label13);
            this.Taxpan.Controls.Add(this.addipan);
            this.Taxpan.Location = new System.Drawing.Point(-1, -2);
            this.Taxpan.Margin = new System.Windows.Forms.Padding(4);
            this.Taxpan.Name = "Taxpan";
            this.Taxpan.Size = new System.Drawing.Size(1083, 466);
            this.Taxpan.TabIndex = 201;
            this.Taxpan.Paint += new System.Windows.Forms.PaintEventHandler(this.Taxpan_Paint);
            // 
            // txttotaddd
            // 
            this.txttotaddd.Location = new System.Drawing.Point(621, 467);
            this.txttotaddd.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.txttotaddd.Name = "txttotaddd";
            this.txttotaddd.Size = new System.Drawing.Size(161, 22);
            this.txttotaddd.TabIndex = 233;
            this.txttotaddd.TextChanged += new System.EventHandler(this.txttotaddd_TextChanged);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(454, 468);
            this.label13.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(149, 18);
            this.label13.TabIndex = 232;
            this.label13.Text = "Additional Charges";
            // 
            // addipan
            // 
            this.addipan.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.addipan.Controls.Add(this.label56);
            this.addipan.Controls.Add(this.HFGT);
            this.addipan.Location = new System.Drawing.Point(15, 14);
            this.addipan.Name = "addipan";
            this.addipan.Size = new System.Drawing.Size(1068, 447);
            this.addipan.TabIndex = 229;
            // 
            // label56
            // 
            this.label56.AutoSize = true;
            this.label56.Font = new System.Drawing.Font("Calibri", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label56.Location = new System.Drawing.Point(319, 22);
            this.label56.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label56.Name = "label56";
            this.label56.Size = new System.Drawing.Size(145, 21);
            this.label56.TabIndex = 312;
            this.label56.Text = "Terms && Conditions";
            // 
            // HFGT
            // 
            this.HFGT.BackgroundColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.HFGT.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.HFGT.Location = new System.Drawing.Point(25, 53);
            this.HFGT.Name = "HFGT";
            this.HFGT.Size = new System.Drawing.Size(720, 363);
            this.HFGT.TabIndex = 311;
            // 
            // panadd
            // 
            this.panadd.BackColor = System.Drawing.Color.White;
            this.panadd.Controls.Add(this.txtchargessum);
            this.panadd.Controls.Add(this.txtbasicval);
            this.panadd.Controls.Add(this.txttax);
            this.panadd.Controls.Add(this.txttotamt);
            this.panadd.Controls.Add(this.label58);
            this.panadd.Controls.Add(this.button13);
            this.panadd.Controls.Add(this.panel1);
            this.panadd.Controls.Add(this.buttnnvfst);
            this.panadd.Controls.Add(this.buttnnxtlft);
            this.panadd.Controls.Add(this.btnfinnxt);
            this.panadd.Controls.Add(this.buttrnxt);
            this.panadd.Controls.Add(this.buttnext1);
            this.panadd.Controls.Add(this.chkact);
            this.panadd.Controls.Add(this.butedit);
            this.panadd.Controls.Add(this.button6);
            this.panadd.Controls.Add(this.butcan);
            this.panadd.Controls.Add(this.button1);
            this.panadd.Location = new System.Drawing.Point(-1, 465);
            this.panadd.Name = "panadd";
            this.panadd.Size = new System.Drawing.Size(920, 36);
            this.panadd.TabIndex = 235;
            // 
            // txtchargessum
            // 
            this.txtchargessum.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtchargessum.Enabled = false;
            this.txtchargessum.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtchargessum.Location = new System.Drawing.Point(923, 5);
            this.txtchargessum.Margin = new System.Windows.Forms.Padding(5);
            this.txtchargessum.Name = "txtchargessum";
            this.txtchargessum.Size = new System.Drawing.Size(100, 26);
            this.txtchargessum.TabIndex = 240;
            this.txtchargessum.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txtbasicval
            // 
            this.txtbasicval.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtbasicval.Enabled = false;
            this.txtbasicval.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtbasicval.Location = new System.Drawing.Point(805, 6);
            this.txtbasicval.Margin = new System.Windows.Forms.Padding(5);
            this.txtbasicval.Name = "txtbasicval";
            this.txtbasicval.Size = new System.Drawing.Size(100, 26);
            this.txtbasicval.TabIndex = 238;
            this.txtbasicval.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txttax
            // 
            this.txttax.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txttax.Enabled = false;
            this.txttax.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txttax.Location = new System.Drawing.Point(1022, 5);
            this.txttax.Margin = new System.Windows.Forms.Padding(5);
            this.txttax.Name = "txttax";
            this.txttax.Size = new System.Drawing.Size(100, 26);
            this.txttax.TabIndex = 239;
            this.txttax.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txttotamt
            // 
            this.txttotamt.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txttotamt.Enabled = false;
            this.txttotamt.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txttotamt.Location = new System.Drawing.Point(1121, 5);
            this.txttotamt.Margin = new System.Windows.Forms.Padding(5);
            this.txttotamt.Name = "txttotamt";
            this.txttotamt.Size = new System.Drawing.Size(100, 26);
            this.txttotamt.TabIndex = 237;
            this.txttotamt.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label58
            // 
            this.label58.AutoSize = true;
            this.label58.Font = new System.Drawing.Font("Calibri", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label58.Location = new System.Drawing.Point(752, 8);
            this.label58.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label58.Name = "label58";
            this.label58.Size = new System.Drawing.Size(43, 21);
            this.label58.TabIndex = 236;
            this.label58.Text = "Total";
            // 
            // button13
            // 
            this.button13.BackColor = System.Drawing.Color.White;
            this.button13.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button13.Image = ((System.Drawing.Image)(resources.GetObject("button13.Image")));
            this.button13.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button13.Location = new System.Drawing.Point(503, 3);
            this.button13.Name = "button13";
            this.button13.Size = new System.Drawing.Size(117, 30);
            this.button13.TabIndex = 227;
            this.button13.Text = "Print Preview";
            this.button13.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button13.UseVisualStyleBackColor = false;
            this.button13.Visible = false;
            this.button13.Click += new System.EventHandler(this.button13_Click);
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel1.Controls.Add(this.lblno1);
            this.panel1.Controls.Add(this.lblno2);
            this.panel1.Controls.Add(this.flowLayoutPanel3);
            this.panel1.Controls.Add(this.flowLayoutPanel2);
            this.panel1.Controls.Add(this.flowLayoutPanel1);
            this.panel1.Location = new System.Drawing.Point(64, 2);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(74, 30);
            this.panel1.TabIndex = 214;
            // 
            // lblno1
            // 
            this.lblno1.AutoSize = true;
            this.lblno1.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblno1.ForeColor = System.Drawing.Color.Black;
            this.lblno1.Location = new System.Drawing.Point(4, 7);
            this.lblno1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblno1.Name = "lblno1";
            this.lblno1.Size = new System.Drawing.Size(15, 18);
            this.lblno1.TabIndex = 163;
            this.lblno1.Text = "1";
            // 
            // lblno2
            // 
            this.lblno2.AutoSize = true;
            this.lblno2.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblno2.ForeColor = System.Drawing.Color.Black;
            this.lblno2.Location = new System.Drawing.Point(27, 7);
            this.lblno2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblno2.Name = "lblno2";
            this.lblno2.Size = new System.Drawing.Size(31, 18);
            this.lblno2.TabIndex = 162;
            this.lblno2.Text = "of 1";
            // 
            // flowLayoutPanel3
            // 
            this.flowLayoutPanel3.Location = new System.Drawing.Point(61, 30);
            this.flowLayoutPanel3.Name = "flowLayoutPanel3";
            this.flowLayoutPanel3.Size = new System.Drawing.Size(200, 100);
            this.flowLayoutPanel3.TabIndex = 2;
            // 
            // flowLayoutPanel2
            // 
            this.flowLayoutPanel2.Location = new System.Drawing.Point(38, 35);
            this.flowLayoutPanel2.Name = "flowLayoutPanel2";
            this.flowLayoutPanel2.Size = new System.Drawing.Size(200, 100);
            this.flowLayoutPanel2.TabIndex = 1;
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.Location = new System.Drawing.Point(62, -129);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(200, 100);
            this.flowLayoutPanel1.TabIndex = 0;
            // 
            // buttnnvfst
            // 
            this.buttnnvfst.BackColor = System.Drawing.Color.White;
            this.buttnnvfst.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.buttnnvfst.FlatAppearance.BorderSize = 0;
            this.buttnnvfst.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttnnvfst.Image = ((System.Drawing.Image)(resources.GetObject("buttnnvfst.Image")));
            this.buttnnvfst.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttnnvfst.Location = new System.Drawing.Point(6, 2);
            this.buttnnvfst.Name = "buttnnvfst";
            this.buttnnvfst.Size = new System.Drawing.Size(19, 31);
            this.buttnnvfst.TabIndex = 213;
            this.buttnnvfst.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.buttnnvfst.UseVisualStyleBackColor = false;
            this.buttnnvfst.Click += new System.EventHandler(this.buttnnvfst_Click);
            // 
            // buttnnxtlft
            // 
            this.buttnnxtlft.BackColor = System.Drawing.Color.White;
            this.buttnnxtlft.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.buttnnxtlft.FlatAppearance.BorderSize = 0;
            this.buttnnxtlft.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttnnxtlft.Image = ((System.Drawing.Image)(resources.GetObject("buttnnxtlft.Image")));
            this.buttnnxtlft.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttnnxtlft.Location = new System.Drawing.Point(40, 2);
            this.buttnnxtlft.Name = "buttnnxtlft";
            this.buttnnxtlft.Size = new System.Drawing.Size(18, 31);
            this.buttnnxtlft.TabIndex = 212;
            this.buttnnxtlft.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.buttnnxtlft.UseVisualStyleBackColor = false;
            this.buttnnxtlft.Click += new System.EventHandler(this.buttnnxtlft_Click);
            // 
            // btnfinnxt
            // 
            this.btnfinnxt.BackColor = System.Drawing.Color.White;
            this.btnfinnxt.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.btnfinnxt.FlatAppearance.BorderSize = 0;
            this.btnfinnxt.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnfinnxt.Image = ((System.Drawing.Image)(resources.GetObject("btnfinnxt.Image")));
            this.btnfinnxt.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnfinnxt.Location = new System.Drawing.Point(178, 2);
            this.btnfinnxt.Name = "btnfinnxt";
            this.btnfinnxt.Size = new System.Drawing.Size(19, 31);
            this.btnfinnxt.TabIndex = 211;
            this.btnfinnxt.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnfinnxt.UseVisualStyleBackColor = false;
            this.btnfinnxt.Click += new System.EventHandler(this.btnfinnxt_Click);
            // 
            // buttrnxt
            // 
            this.buttrnxt.BackColor = System.Drawing.Color.White;
            this.buttrnxt.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.buttrnxt.FlatAppearance.BorderSize = 0;
            this.buttrnxt.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttrnxt.Image = ((System.Drawing.Image)(resources.GetObject("buttrnxt.Image")));
            this.buttrnxt.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttrnxt.Location = new System.Drawing.Point(144, 2);
            this.buttrnxt.Name = "buttrnxt";
            this.buttrnxt.Size = new System.Drawing.Size(18, 31);
            this.buttrnxt.TabIndex = 210;
            this.buttrnxt.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.buttrnxt.UseVisualStyleBackColor = false;
            this.buttrnxt.Click += new System.EventHandler(this.buttrnxt_Click);
            // 
            // buttnext1
            // 
            this.buttnext1.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.buttnext1.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttnext1.Image = ((System.Drawing.Image)(resources.GetObject("buttnext1.Image")));
            this.buttnext1.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttnext1.Location = new System.Drawing.Point(436, 3);
            this.buttnext1.Name = "buttnext1";
            this.buttnext1.Size = new System.Drawing.Size(65, 30);
            this.buttnext1.TabIndex = 208;
            this.buttnext1.Text = "Exit";
            this.buttnext1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.buttnext1.UseVisualStyleBackColor = false;
            this.buttnext1.Click += new System.EventHandler(this.buttnext1_Click);
            // 
            // chkact
            // 
            this.chkact.AutoSize = true;
            this.chkact.BackColor = System.Drawing.Color.White;
            this.chkact.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkact.Location = new System.Drawing.Point(227, 9);
            this.chkact.Name = "chkact";
            this.chkact.Size = new System.Drawing.Size(65, 22);
            this.chkact.TabIndex = 187;
            this.chkact.Text = "Active";
            this.chkact.UseVisualStyleBackColor = false;
            this.chkact.CheckedChanged += new System.EventHandler(this.chkact_CheckedChanged);
            // 
            // butedit
            // 
            this.butedit.BackColor = System.Drawing.Color.White;
            this.butedit.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.butedit.Image = ((System.Drawing.Image)(resources.GetObject("butedit.Image")));
            this.butedit.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.butedit.Location = new System.Drawing.Point(380, 3);
            this.butedit.Name = "butedit";
            this.butedit.Size = new System.Drawing.Size(55, 30);
            this.butedit.TabIndex = 185;
            this.butedit.Text = "Edit";
            this.butedit.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.butedit.UseVisualStyleBackColor = false;
            this.butedit.Click += new System.EventHandler(this.butedit_Click);
            // 
            // button6
            // 
            this.button6.BackColor = System.Drawing.Color.White;
            this.button6.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.button6.FlatAppearance.BorderSize = 0;
            this.button6.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button6.Image = ((System.Drawing.Image)(resources.GetObject("button6.Image")));
            this.button6.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button6.Location = new System.Drawing.Point(291, 3);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(88, 30);
            this.button6.TabIndex = 184;
            this.button6.Text = "Add new";
            this.button6.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button6.UseVisualStyleBackColor = false;
            this.button6.Click += new System.EventHandler(this.button6_Click_1);
            // 
            // butcan
            // 
            this.butcan.BackColor = System.Drawing.Color.White;
            this.butcan.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.butcan.Image = ((System.Drawing.Image)(resources.GetObject("butcan.Image")));
            this.butcan.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.butcan.Location = new System.Drawing.Point(298, 3);
            this.butcan.Name = "butcan";
            this.butcan.Size = new System.Drawing.Size(110, 30);
            this.butcan.TabIndex = 186;
            this.butcan.Text = "Invoice Cancel";
            this.butcan.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.butcan.UseVisualStyleBackColor = false;
            this.butcan.Click += new System.EventHandler(this.butcan_Click);
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.White;
            this.button1.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.Image = ((System.Drawing.Image)(resources.GetObject("button1.Image")));
            this.button1.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button1.Location = new System.Drawing.Point(436, 3);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(65, 30);
            this.button1.TabIndex = 216;
            this.button1.Text = "Print";
            this.button1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Visible = false;
            this.button1.Click += new System.EventHandler(this.button1_Click_1);
            // 
            // btnBagNoMapping
            // 
            this.btnBagNoMapping.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnBagNoMapping.Location = new System.Drawing.Point(937, 469);
            this.btnBagNoMapping.Name = "btnBagNoMapping";
            this.btnBagNoMapping.Size = new System.Drawing.Size(165, 28);
            this.btnBagNoMapping.TabIndex = 351;
            this.btnBagNoMapping.Text = "BagNo Mapping";
            this.btnBagNoMapping.UseVisualStyleBackColor = true;
            this.btnBagNoMapping.Visible = false;
            this.btnBagNoMapping.Click += new System.EventHandler(this.button15_Click);
            // 
            // Editpan
            // 
            this.Editpan.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.Editpan.Controls.Add(this.PanelSearch);
            this.Editpan.Controls.Add(this.txtprice);
            this.Editpan.Controls.Add(this.label35);
            this.Editpan.Controls.Add(this.label36);
            this.Editpan.Controls.Add(this.label57);
            this.Editpan.Controls.Add(this.label59);
            this.Editpan.Controls.Add(this.label54);
            this.Editpan.Controls.Add(this.txtttot);
            this.Editpan.Controls.Add(this.txtigval);
            this.Editpan.Controls.Add(this.label53);
            this.Editpan.Controls.Add(this.txttbval);
            this.Editpan.Controls.Add(this.label45);
            this.Editpan.Controls.Add(this.label17);
            this.Editpan.Controls.Add(this.txtNetValue);
            this.Editpan.Controls.Add(this.label16);
            this.Editpan.Controls.Add(this.txtRoundOff);
            this.Editpan.Controls.Add(this.txtpadd1);
            this.Editpan.Controls.Add(this.label55);
            this.Editpan.Controls.Add(this.label40);
            this.Editpan.Controls.Add(this.txttrans);
            this.Editpan.Controls.Add(this.Txttot);
            this.Editpan.Controls.Add(this.label4);
            this.Editpan.Controls.Add(this.txtot);
            this.Editpan.Controls.Add(this.button3);
            this.Editpan.Controls.Add(this.txtamt);
            this.Editpan.Controls.Add(this.button2);
            this.Editpan.Controls.Add(this.DTPDOCDT);
            this.Editpan.Controls.Add(this.HFIT);
            this.Editpan.Controls.Add(this.Dtpdt);
            this.Editpan.Controls.Add(this.label6);
            this.Editpan.Controls.Add(this.label5);
            this.Editpan.Controls.Add(this.label2);
            this.Editpan.Controls.Add(this.label3);
            this.Editpan.Controls.Add(this.txtname);
            this.Editpan.Controls.Add(this.Phone);
            this.Editpan.Controls.Add(this.txtgrn);
            this.Editpan.Controls.Add(this.txtpuid);
            this.Editpan.Controls.Add(this.txtgrnid);
            this.Editpan.Controls.Add(this.txtdcid);
            this.Editpan.Controls.Add(this.txttitemid);
            this.Editpan.Controls.Add(this.txtpluid);
            this.Editpan.Controls.Add(this.label44);
            this.Editpan.Controls.Add(this.txtuom);
            this.Editpan.Controls.Add(this.Dtprem);
            this.Editpan.Controls.Add(this.label39);
            this.Editpan.Controls.Add(this.Dtppre);
            this.Editpan.Controls.Add(this.label38);
            this.Editpan.Controls.Add(this.pantax);
            this.Editpan.Controls.Add(this.txttempadd2);
            this.Editpan.Controls.Add(this.txttempadd1);
            this.Editpan.Controls.Add(this.button8);
            this.Editpan.Controls.Add(this.button4);
            this.Editpan.Controls.Add(this.label33);
            this.Editpan.Controls.Add(this.label15);
            this.Editpan.Controls.Add(this.txtitemname);
            this.Editpan.Controls.Add(this.label42);
            this.Editpan.Controls.Add(this.buttcusok);
            this.Editpan.Controls.Add(this.label32);
            this.Editpan.Controls.Add(this.label31);
            this.Editpan.Controls.Add(this.label14);
            this.Editpan.Controls.Add(this.txtnotes);
            this.Editpan.Controls.Add(this.txtbval);
            this.Editpan.Controls.Add(this.txtqty);
            this.Editpan.Controls.Add(this.dtpdc);
            this.Editpan.Controls.Add(this.txtitemcode);
            this.Editpan.Controls.Add(this.label10);
            this.Editpan.Controls.Add(this.label11);
            this.Editpan.Controls.Add(this.label37);
            this.Editpan.Controls.Add(this.txtplace);
            this.Editpan.Controls.Add(this.txtpadd2);
            this.Editpan.Controls.Add(this.label41);
            this.Editpan.Controls.Add(this.txtlisid);
            this.Editpan.Controls.Add(this.txttgstval);
            this.Editpan.Controls.Add(this.txttgstp);
            this.Editpan.Controls.Add(this.buttnnxt);
            this.Editpan.Controls.Add(this.btnaddrcan);
            this.Editpan.Controls.Add(this.button11);
            this.Editpan.Controls.Add(this.button12);
            this.Editpan.Controls.Add(this.txtgen2);
            this.Editpan.Controls.Add(this.txtgen3);
            this.Editpan.Controls.Add(this.txtigstp);
            this.Editpan.Controls.Add(this.label52);
            this.Editpan.Controls.Add(this.txtsgstp);
            this.Editpan.Controls.Add(this.label50);
            this.Editpan.Controls.Add(this.label51);
            this.Editpan.Controls.Add(this.txttcgval);
            this.Editpan.Controls.Add(this.txttcgstp);
            this.Editpan.Controls.Add(this.label49);
            this.Editpan.Controls.Add(this.label48);
            this.Editpan.Controls.Add(this.txttsgval);
            this.Editpan.Controls.Add(this.txtRefNo);
            this.Editpan.Controls.Add(this.dcdate);
            this.Editpan.Controls.Add(this.label34);
            this.Editpan.Controls.Add(this.txtcharges);
            this.Editpan.Controls.Add(this.label12);
            this.Editpan.Controls.Add(this.txtlistid);
            this.Editpan.Controls.Add(this.label61);
            this.Editpan.Controls.Add(this.txttaxtot);
            this.Editpan.Controls.Add(this.label77);
            this.Editpan.Controls.Add(this.txtTaxvalue);
            this.Editpan.Controls.Add(this.cboTax);
            this.Editpan.Controls.Add(this.label76);
            this.Editpan.Controls.Add(this.button7);
            this.Editpan.Controls.Add(this.label78);
            this.Editpan.Controls.Add(this.txtNoBag);
            this.Editpan.Controls.Add(this.txtexcise);
            this.Editpan.Controls.Add(this.label46);
            this.Editpan.Controls.Add(this.txttdisc);
            this.Editpan.Controls.Add(this.label43);
            this.Editpan.Controls.Add(this.txttdis);
            this.Editpan.Controls.Add(this.label8);
            this.Editpan.Controls.Add(this.label79);
            this.Editpan.Controls.Add(this.txtHSNCode);
            this.Editpan.Controls.Add(this.label1);
            this.Editpan.Controls.Add(this.txtrem);
            this.Editpan.Controls.Add(this.txtSupplyToAdd);
            this.Editpan.Controls.Add(this.label47);
            this.Editpan.Controls.Add(this.txttprdval);
            this.Editpan.Controls.Add(this.label80);
            this.Editpan.Controls.Add(this.txtdcno);
            this.Editpan.Controls.Add(this.HFGST);
            this.Editpan.Controls.Add(this.txtSupplyTo);
            this.Editpan.Controls.Add(this.label82);
            this.Editpan.Controls.Add(this.txtMillName);
            this.Editpan.Controls.Add(this.label81);
            this.Editpan.Controls.Add(this.label85);
            this.Editpan.Controls.Add(this.label84);
            this.Editpan.Controls.Add(this.label86);
            this.Editpan.Controls.Add(this.label83);
            this.Editpan.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Editpan.Location = new System.Drawing.Point(-5, -1);
            this.Editpan.Margin = new System.Windows.Forms.Padding(4);
            this.Editpan.Name = "Editpan";
            this.Editpan.Size = new System.Drawing.Size(1230, 466);
            this.Editpan.TabIndex = 243;
            // 
            // PanelSearch
            // 
            this.PanelSearch.Controls.Add(this.btnSelect);
            this.PanelSearch.Controls.Add(this.btnHide);
            this.PanelSearch.Controls.Add(this.DataGridCommon);
            this.PanelSearch.Location = new System.Drawing.Point(117, 214);
            this.PanelSearch.Name = "PanelSearch";
            this.PanelSearch.Size = new System.Drawing.Size(388, 248);
            this.PanelSearch.TabIndex = 427;
            this.PanelSearch.Visible = false;
            // 
            // btnSelect
            // 
            this.btnSelect.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnSelect.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSelect.Image = global::ACV.Properties.Resources.ok;
            this.btnSelect.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.btnSelect.Location = new System.Drawing.Point(238, 216);
            this.btnSelect.Name = "btnSelect";
            this.btnSelect.Size = new System.Drawing.Size(74, 28);
            this.btnSelect.TabIndex = 396;
            this.btnSelect.Text = "Select";
            this.btnSelect.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnSelect.UseVisualStyleBackColor = false;
            this.btnSelect.Click += new System.EventHandler(this.btnSelect_Click);
            // 
            // btnHide
            // 
            this.btnHide.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnHide.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnHide.Image = ((System.Drawing.Image)(resources.GetObject("btnHide.Image")));
            this.btnHide.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnHide.Location = new System.Drawing.Point(312, 217);
            this.btnHide.Name = "btnHide";
            this.btnHide.Size = new System.Drawing.Size(71, 27);
            this.btnHide.TabIndex = 395;
            this.btnHide.Text = "Close";
            this.btnHide.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnHide.UseVisualStyleBackColor = false;
            this.btnHide.Click += new System.EventHandler(this.btnHide_Click);
            // 
            // DataGridCommon
            // 
            this.DataGridCommon.AllowUserToAddRows = false;
            this.DataGridCommon.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.DataGridCommon.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DataGridCommon.Location = new System.Drawing.Point(4, 3);
            this.DataGridCommon.Name = "DataGridCommon";
            this.DataGridCommon.ReadOnly = true;
            this.DataGridCommon.RowHeadersVisible = false;
            this.DataGridCommon.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.DataGridCommon.Size = new System.Drawing.Size(379, 212);
            this.DataGridCommon.TabIndex = 0;
            this.DataGridCommon.CellMouseDoubleClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.DataGridParty_CellMouseDoubleClick);
            // 
            // txtprice
            // 
            this.txtprice.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtprice.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtprice.Location = new System.Drawing.Point(351, 179);
            this.txtprice.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.txtprice.Name = "txtprice";
            this.txtprice.Size = new System.Drawing.Size(68, 26);
            this.txtprice.TabIndex = 7;
            this.txtprice.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtprice.TextChanged += new System.EventHandler(this.txtprice_TextChanged_1);
            this.txtprice.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtprice_KeyDown);
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label35.Location = new System.Drawing.Point(480, 158);
            this.label35.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(74, 18);
            this.label35.TabIndex = 328;
            this.label35.Text = "BasicValue";
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label36.Location = new System.Drawing.Point(423, 155);
            this.label36.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(30, 18);
            this.label36.TabIndex = 326;
            this.label36.Text = "Qty";
            // 
            // label57
            // 
            this.label57.AutoSize = true;
            this.label57.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label57.Location = new System.Drawing.Point(7, 158);
            this.label57.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label57.Name = "label57";
            this.label57.Size = new System.Drawing.Size(122, 18);
            this.label57.TabIndex = 325;
            this.label57.Text = "ItemName - Count";
            // 
            // label59
            // 
            this.label59.AutoSize = true;
            this.label59.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label59.Location = new System.Drawing.Point(354, 155);
            this.label59.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label59.Name = "label59";
            this.label59.Size = new System.Drawing.Size(39, 18);
            this.label59.TabIndex = 324;
            this.label59.Text = "Price";
            // 
            // label54
            // 
            this.label54.AutoSize = true;
            this.label54.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label54.Location = new System.Drawing.Point(1001, 141);
            this.label54.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label54.Name = "label54";
            this.label54.Size = new System.Drawing.Size(91, 18);
            this.label54.TabIndex = 316;
            this.label54.Text = "Total Amount";
            // 
            // txtttot
            // 
            this.txtttot.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.txtttot.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtttot.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtttot.Location = new System.Drawing.Point(1108, 139);
            this.txtttot.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.txtttot.Name = "txtttot";
            this.txtttot.Size = new System.Drawing.Size(111, 26);
            this.txtttot.TabIndex = 315;
            this.txtttot.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtttot.TextChanged += new System.EventHandler(this.txtttot_TextChanged);
            // 
            // txtigval
            // 
            this.txtigval.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.txtigval.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtigval.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtigval.Location = new System.Drawing.Point(1109, 68);
            this.txtigval.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.txtigval.Name = "txtigval";
            this.txtigval.Size = new System.Drawing.Size(112, 26);
            this.txtigval.TabIndex = 314;
            this.txtigval.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label53
            // 
            this.label53.AutoSize = true;
            this.label53.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label53.Location = new System.Drawing.Point(1007, 70);
            this.label53.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label53.Name = "label53";
            this.label53.Size = new System.Drawing.Size(84, 18);
            this.label53.TabIndex = 311;
            this.label53.Text = "GST Amount";
            // 
            // txttbval
            // 
            this.txttbval.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.txttbval.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txttbval.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txttbval.Location = new System.Drawing.Point(1109, 36);
            this.txttbval.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.txttbval.Name = "txttbval";
            this.txttbval.Size = new System.Drawing.Size(112, 26);
            this.txttbval.TabIndex = 298;
            this.txttbval.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label45
            // 
            this.label45.AutoSize = true;
            this.label45.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label45.Location = new System.Drawing.Point(999, 38);
            this.label45.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(92, 18);
            this.label45.TabIndex = 297;
            this.label45.Text = "Basic Amount";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.Location = new System.Drawing.Point(1009, 205);
            this.label17.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(84, 18);
            this.label17.TabIndex = 294;
            this.label17.Text = "Net Amount";
            // 
            // txtNetValue
            // 
            this.txtNetValue.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.txtNetValue.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtNetValue.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNetValue.ForeColor = System.Drawing.Color.Red;
            this.txtNetValue.Location = new System.Drawing.Point(1107, 203);
            this.txtNetValue.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.txtNetValue.Name = "txtNetValue";
            this.txtNetValue.Size = new System.Drawing.Size(112, 26);
            this.txtNetValue.TabIndex = 293;
            this.txtNetValue.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(1061, 173);
            this.label16.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(39, 18);
            this.label16.TabIndex = 292;
            this.label16.Text = "R.Off";
            // 
            // txtRoundOff
            // 
            this.txtRoundOff.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.txtRoundOff.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtRoundOff.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtRoundOff.Location = new System.Drawing.Point(1108, 171);
            this.txtRoundOff.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.txtRoundOff.Name = "txtRoundOff";
            this.txtRoundOff.Size = new System.Drawing.Size(111, 26);
            this.txtRoundOff.TabIndex = 291;
            this.txtRoundOff.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtRoundOff.TextChanged += new System.EventHandler(this.TxtRoff_TextChanged_1);
            // 
            // txtpadd1
            // 
            this.txtpadd1.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtpadd1.Location = new System.Drawing.Point(249, 51);
            this.txtpadd1.MaxLength = 100;
            this.txtpadd1.Name = "txtpadd1";
            this.txtpadd1.Size = new System.Drawing.Size(315, 101);
            this.txtpadd1.TabIndex = 267;
            this.txtpadd1.Text = "";
            // 
            // label55
            // 
            this.label55.AutoSize = true;
            this.label55.Font = new System.Drawing.Font("Calibri", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label55.Location = new System.Drawing.Point(511, 440);
            this.label55.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label55.Name = "label55";
            this.label55.Size = new System.Drawing.Size(85, 21);
            this.label55.TabIndex = 264;
            this.label55.Text = "Total Value";
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label40.Location = new System.Drawing.Point(314, 63);
            this.label40.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(48, 18);
            this.label40.TabIndex = 257;
            this.label40.Text = "Inv.No";
            // 
            // txttrans
            // 
            this.txttrans.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txttrans.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txttrans.Location = new System.Drawing.Point(303, 62);
            this.txttrans.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.txttrans.Name = "txttrans";
            this.txttrans.Size = new System.Drawing.Size(118, 26);
            this.txttrans.TabIndex = 226;
            this.txttrans.Click += new System.EventHandler(this.txttrans_Click);
            this.txttrans.TextChanged += new System.EventHandler(this.txttrans_TextChanged_1);
            this.txttrans.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txttrans_KeyDown_1);
            // 
            // Txttot
            // 
            this.Txttot.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Txttot.Enabled = false;
            this.Txttot.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txttot.Location = new System.Drawing.Point(603, 437);
            this.Txttot.Margin = new System.Windows.Forms.Padding(5);
            this.Txttot.Name = "Txttot";
            this.Txttot.Size = new System.Drawing.Size(101, 26);
            this.Txttot.TabIndex = 202;
            this.Txttot.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.Txttot.TextChanged += new System.EventHandler(this.Txttot_TextChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(836, 497);
            this.label4.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(46, 18);
            this.label4.TabIndex = 238;
            this.label4.Text = "Total";
            // 
            // txtot
            // 
            this.txtot.Location = new System.Drawing.Point(906, 497);
            this.txtot.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.txtot.Name = "txtot";
            this.txtot.Size = new System.Drawing.Size(121, 26);
            this.txtot.TabIndex = 237;
            // 
            // button3
            // 
            this.button3.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.button3.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button3.Image = ((System.Drawing.Image)(resources.GetObject("button3.Image")));
            this.button3.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button3.Location = new System.Drawing.Point(1012, 549);
            this.button3.Margin = new System.Windows.Forms.Padding(4);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(124, 47);
            this.button3.TabIndex = 227;
            this.button3.Text = "Back";
            this.button3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button3.UseVisualStyleBackColor = false;
            // 
            // txtamt
            // 
            this.txtamt.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtamt.Location = new System.Drawing.Point(775, 559);
            this.txtamt.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.txtamt.Name = "txtamt";
            this.txtamt.Size = new System.Drawing.Size(161, 22);
            this.txtamt.TabIndex = 226;
            // 
            // button2
            // 
            this.button2.BackColor = System.Drawing.Color.White;
            this.button2.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button2.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button2.Location = new System.Drawing.Point(1144, 550);
            this.button2.Margin = new System.Windows.Forms.Padding(4);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(113, 46);
            this.button2.TabIndex = 224;
            this.button2.Text = "Tax";
            this.button2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button2.UseVisualStyleBackColor = false;
            // 
            // DTPDOCDT
            // 
            this.DTPDOCDT.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DTPDOCDT.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.DTPDOCDT.Location = new System.Drawing.Point(130, 29);
            this.DTPDOCDT.Margin = new System.Windows.Forms.Padding(4);
            this.DTPDOCDT.Name = "DTPDOCDT";
            this.DTPDOCDT.Size = new System.Drawing.Size(113, 26);
            this.DTPDOCDT.TabIndex = 221;
            // 
            // HFIT
            // 
            this.HFIT.BackgroundColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.HFIT.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.HFIT.Location = new System.Drawing.Point(8, 210);
            this.HFIT.Margin = new System.Windows.Forms.Padding(4);
            this.HFIT.Name = "HFIT";
            this.HFIT.ReadOnly = true;
            this.HFIT.Size = new System.Drawing.Size(950, 220);
            this.HFIT.TabIndex = 214;
            this.HFIT.CellMouseDoubleClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.HFIT_CellMouseDoubleClick);
            this.HFIT.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.HFIT_CellValueChanged);
            this.HFIT.KeyDown += new System.Windows.Forms.KeyEventHandler(this.HFIT_KeyDown_1);
            // 
            // Dtpdt
            // 
            this.Dtpdt.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Dtpdt.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.Dtpdt.Location = new System.Drawing.Point(70, 333);
            this.Dtpdt.Margin = new System.Windows.Forms.Padding(4);
            this.Dtpdt.Name = "Dtpdt";
            this.Dtpdt.Size = new System.Drawing.Size(113, 22);
            this.Dtpdt.TabIndex = 205;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(127, 4);
            this.label6.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(64, 18);
            this.label6.TabIndex = 204;
            this.label6.Text = "Doc.Date";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(74, 395);
            this.label5.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(64, 16);
            this.label5.TabIndex = 203;
            this.label5.Text = "Order.No";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(61, 346);
            this.label2.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(76, 16);
            this.label2.TabIndex = 201;
            this.label2.Text = "Order.Date";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(248, 2);
            this.label3.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(80, 18);
            this.label3.TabIndex = 195;
            this.label3.Text = "Party Name";
            // 
            // txtname
            // 
            this.txtname.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtname.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtname.Location = new System.Drawing.Point(249, 25);
            this.txtname.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.txtname.MaxLength = 100;
            this.txtname.Name = "txtname";
            this.txtname.Size = new System.Drawing.Size(314, 26);
            this.txtname.TabIndex = 0;
            this.txtname.Click += new System.EventHandler(this.txtname_Click);
            this.txtname.TextChanged += new System.EventHandler(this.txtname_TextChanged);
            this.txtname.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtname_KeyDown);
            // 
            // Phone
            // 
            this.Phone.AutoSize = true;
            this.Phone.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Phone.Location = new System.Drawing.Point(9, 6);
            this.Phone.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.Phone.Name = "Phone";
            this.Phone.Size = new System.Drawing.Size(53, 18);
            this.Phone.TabIndex = 199;
            this.Phone.Text = "Doc.No";
            // 
            // txtgrn
            // 
            this.txtgrn.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtgrn.Enabled = false;
            this.txtgrn.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtgrn.Location = new System.Drawing.Point(9, 29);
            this.txtgrn.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.txtgrn.Name = "txtgrn";
            this.txtgrn.Size = new System.Drawing.Size(119, 26);
            this.txtgrn.TabIndex = 198;
            // 
            // txtpuid
            // 
            this.txtpuid.Location = new System.Drawing.Point(377, 329);
            this.txtpuid.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.txtpuid.Name = "txtpuid";
            this.txtpuid.Size = new System.Drawing.Size(87, 26);
            this.txtpuid.TabIndex = 217;
            // 
            // txtgrnid
            // 
            this.txtgrnid.Location = new System.Drawing.Point(488, 329);
            this.txtgrnid.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.txtgrnid.Name = "txtgrnid";
            this.txtgrnid.Size = new System.Drawing.Size(28, 26);
            this.txtgrnid.TabIndex = 220;
            // 
            // txtdcid
            // 
            this.txtdcid.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtdcid.Location = new System.Drawing.Point(699, 302);
            this.txtdcid.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.txtdcid.Name = "txtdcid";
            this.txtdcid.Size = new System.Drawing.Size(23, 22);
            this.txtdcid.TabIndex = 228;
            this.txtdcid.TextChanged += new System.EventHandler(this.txtdcid_TextChanged);
            // 
            // txttitemid
            // 
            this.txttitemid.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txttitemid.Location = new System.Drawing.Point(36, 253);
            this.txttitemid.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.txttitemid.Name = "txttitemid";
            this.txttitemid.Size = new System.Drawing.Size(35, 22);
            this.txttitemid.TabIndex = 247;
            this.txttitemid.TextChanged += new System.EventHandler(this.txttitemid_TextChanged);
            // 
            // txtpluid
            // 
            this.txtpluid.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtpluid.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtpluid.Location = new System.Drawing.Point(763, 239);
            this.txtpluid.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.txtpluid.Name = "txtpluid";
            this.txtpluid.Size = new System.Drawing.Size(94, 22);
            this.txtpluid.TabIndex = 251;
            this.txtpluid.TextChanged += new System.EventHandler(this.txtpluid_TextChanged);
            // 
            // label44
            // 
            this.label44.AutoSize = true;
            this.label44.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label44.Location = new System.Drawing.Point(321, 312);
            this.label44.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(36, 16);
            this.label44.TabIndex = 263;
            this.label44.Text = "UoM";
            // 
            // txtuom
            // 
            this.txtuom.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtuom.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtuom.Location = new System.Drawing.Point(298, 312);
            this.txtuom.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.txtuom.Name = "txtuom";
            this.txtuom.Size = new System.Drawing.Size(59, 22);
            this.txtuom.TabIndex = 262;
            // 
            // Dtprem
            // 
            this.Dtprem.CustomFormat = "dd-MMM-yyyy  hh:mm tt";
            this.Dtprem.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Dtprem.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.Dtprem.Location = new System.Drawing.Point(427, 62);
            this.Dtprem.Margin = new System.Windows.Forms.Padding(4);
            this.Dtprem.Name = "Dtprem";
            this.Dtprem.Size = new System.Drawing.Size(112, 26);
            this.Dtprem.TabIndex = 255;
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label39.Location = new System.Drawing.Point(314, 112);
            this.label39.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(59, 18);
            this.label39.TabIndex = 254;
            this.label39.Text = "Inv.Date";
            // 
            // Dtppre
            // 
            this.Dtppre.AllowDrop = true;
            this.Dtppre.CustomFormat = "dd-MMM-yyyy  hh:mm tt";
            this.Dtppre.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Dtppre.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.Dtppre.Location = new System.Drawing.Point(111, 298);
            this.Dtppre.Margin = new System.Windows.Forms.Padding(4);
            this.Dtppre.Name = "Dtppre";
            this.Dtppre.Size = new System.Drawing.Size(175, 22);
            this.Dtppre.TabIndex = 253;
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label38.Location = new System.Drawing.Point(400, 391);
            this.label38.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(47, 16);
            this.label38.TabIndex = 252;
            this.label38.Text = "DC No";
            // 
            // pantax
            // 
            this.pantax.BackColor = System.Drawing.Color.Silver;
            this.pantax.Controls.Add(this.txttitem);
            this.pantax.Controls.Add(this.label30);
            this.pantax.Controls.Add(this.txttqty);
            this.pantax.Controls.Add(this.label29);
            this.pantax.Controls.Add(this.Txtrate);
            this.pantax.Controls.Add(this.label28);
            this.pantax.Controls.Add(this.label27);
            this.pantax.Controls.Add(this.label26);
            this.pantax.Controls.Add(this.label25);
            this.pantax.Controls.Add(this.txtbasic);
            this.pantax.Controls.Add(this.label23);
            this.pantax.Controls.Add(this.button5);
            this.pantax.Controls.Add(this.txttotal);
            this.pantax.Controls.Add(this.label24);
            this.pantax.Controls.Add(this.txtigcst);
            this.pantax.Controls.Add(this.cboigst);
            this.pantax.Controls.Add(this.label22);
            this.pantax.Controls.Add(this.txtsgst);
            this.pantax.Controls.Add(this.SGST);
            this.pantax.Controls.Add(this.label21);
            this.pantax.Controls.Add(this.txtcgst);
            this.pantax.Controls.Add(this.cbocgst);
            this.pantax.Controls.Add(this.label20);
            this.pantax.Controls.Add(this.label19);
            this.pantax.Controls.Add(this.textBox2);
            this.pantax.Controls.Add(this.label18);
            this.pantax.Controls.Add(this.txtper);
            this.pantax.Controls.Add(this.label9);
            this.pantax.Controls.Add(this.txttaxable);
            this.pantax.Controls.Add(this.txthidqty);
            this.pantax.Location = new System.Drawing.Point(519, 321);
            this.pantax.Name = "pantax";
            this.pantax.Size = new System.Drawing.Size(68, 10);
            this.pantax.TabIndex = 232;
            this.pantax.Visible = false;
            // 
            // txttitem
            // 
            this.txttitem.Enabled = false;
            this.txttitem.Location = new System.Drawing.Point(8, 36);
            this.txttitem.Name = "txttitem";
            this.txttitem.Size = new System.Drawing.Size(315, 51);
            this.txttitem.TabIndex = 247;
            this.txttitem.Text = "";
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label30.Location = new System.Drawing.Point(5, 16);
            this.label30.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(40, 18);
            this.label30.TabIndex = 246;
            this.label30.Text = "Item";
            // 
            // txttqty
            // 
            this.txttqty.Location = new System.Drawing.Point(216, 102);
            this.txttqty.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.txttqty.Name = "txttqty";
            this.txttqty.Size = new System.Drawing.Size(88, 26);
            this.txttqty.TabIndex = 244;
            this.txttqty.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label29.Location = new System.Drawing.Point(26, 102);
            this.label29.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(34, 18);
            this.label29.TabIndex = 243;
            this.label29.Text = "Qty";
            // 
            // Txtrate
            // 
            this.Txtrate.Enabled = false;
            this.Txtrate.Location = new System.Drawing.Point(216, 136);
            this.Txtrate.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.Txtrate.Name = "Txtrate";
            this.Txtrate.Size = new System.Drawing.Size(88, 26);
            this.Txtrate.TabIndex = 242;
            this.Txtrate.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label28.Location = new System.Drawing.Point(26, 136);
            this.label28.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(43, 18);
            this.label28.TabIndex = 241;
            this.label28.Text = "Rate";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label27.Location = new System.Drawing.Point(181, 352);
            this.label27.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(22, 18);
            this.label27.TabIndex = 240;
            this.label27.Text = "%";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label26.Location = new System.Drawing.Point(181, 314);
            this.label26.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(22, 18);
            this.label26.TabIndex = 239;
            this.label26.Text = "%";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label25.Location = new System.Drawing.Point(181, 277);
            this.label25.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(22, 18);
            this.label25.TabIndex = 238;
            this.label25.Text = "%";
            // 
            // txtbasic
            // 
            this.txtbasic.Enabled = false;
            this.txtbasic.Location = new System.Drawing.Point(216, 170);
            this.txtbasic.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.txtbasic.Name = "txtbasic";
            this.txtbasic.Size = new System.Drawing.Size(88, 26);
            this.txtbasic.TabIndex = 237;
            this.txtbasic.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.Location = new System.Drawing.Point(26, 170);
            this.label23.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(96, 18);
            this.label23.TabIndex = 236;
            this.label23.Text = "Basic Value";
            // 
            // button5
            // 
            this.button5.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.button5.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button5.Image = global::ACV.Properties.Resources.Add;
            this.button5.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button5.Location = new System.Drawing.Point(120, 425);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(83, 41);
            this.button5.TabIndex = 235;
            this.button5.Text = "Ok";
            this.button5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button5.UseVisualStyleBackColor = false;
            // 
            // txttotal
            // 
            this.txttotal.Enabled = false;
            this.txttotal.Location = new System.Drawing.Point(216, 386);
            this.txttotal.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.txttotal.Name = "txttotal";
            this.txttotal.Size = new System.Drawing.Size(88, 26);
            this.txttotal.TabIndex = 228;
            this.txttotal.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.Location = new System.Drawing.Point(25, 388);
            this.label24.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(92, 18);
            this.label24.TabIndex = 227;
            this.label24.Text = "Total Value";
            // 
            // txtigcst
            // 
            this.txtigcst.Enabled = false;
            this.txtigcst.Location = new System.Drawing.Point(216, 352);
            this.txtigcst.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.txtigcst.Name = "txtigcst";
            this.txtigcst.Size = new System.Drawing.Size(88, 26);
            this.txtigcst.TabIndex = 225;
            this.txtigcst.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // cboigst
            // 
            this.cboigst.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboigst.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboigst.FormattingEnabled = true;
            this.cboigst.Location = new System.Drawing.Point(137, 350);
            this.cboigst.Margin = new System.Windows.Forms.Padding(4);
            this.cboigst.Name = "cboigst";
            this.cboigst.Size = new System.Drawing.Size(39, 26);
            this.cboigst.TabIndex = 224;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.Location = new System.Drawing.Point(26, 352);
            this.label22.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(46, 18);
            this.label22.TabIndex = 223;
            this.label22.Text = "IGST";
            // 
            // txtsgst
            // 
            this.txtsgst.Enabled = false;
            this.txtsgst.Location = new System.Drawing.Point(216, 313);
            this.txtsgst.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.txtsgst.Name = "txtsgst";
            this.txtsgst.Size = new System.Drawing.Size(88, 26);
            this.txtsgst.TabIndex = 222;
            this.txtsgst.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // SGST
            // 
            this.SGST.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.SGST.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.SGST.FormattingEnabled = true;
            this.SGST.Location = new System.Drawing.Point(137, 311);
            this.SGST.Margin = new System.Windows.Forms.Padding(4);
            this.SGST.Name = "SGST";
            this.SGST.Size = new System.Drawing.Size(39, 26);
            this.SGST.TabIndex = 221;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.Location = new System.Drawing.Point(26, 313);
            this.label21.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(53, 18);
            this.label21.TabIndex = 220;
            this.label21.Text = "SGST";
            // 
            // txtcgst
            // 
            this.txtcgst.Enabled = false;
            this.txtcgst.Location = new System.Drawing.Point(216, 276);
            this.txtcgst.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.txtcgst.Name = "txtcgst";
            this.txtcgst.Size = new System.Drawing.Size(88, 26);
            this.txtcgst.TabIndex = 219;
            this.txtcgst.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // cbocgst
            // 
            this.cbocgst.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cbocgst.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cbocgst.FormattingEnabled = true;
            this.cbocgst.Location = new System.Drawing.Point(137, 274);
            this.cbocgst.Margin = new System.Windows.Forms.Padding(4);
            this.cbocgst.Name = "cbocgst";
            this.cbocgst.Size = new System.Drawing.Size(39, 26);
            this.cbocgst.TabIndex = 218;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.Location = new System.Drawing.Point(26, 276);
            this.label20.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(54, 18);
            this.label20.TabIndex = 217;
            this.label20.Text = "CGST";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.Location = new System.Drawing.Point(25, 241);
            this.label19.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(112, 18);
            this.label19.TabIndex = 216;
            this.label19.Text = "Taxable Value";
            // 
            // textBox2
            // 
            this.textBox2.Enabled = false;
            this.textBox2.Location = new System.Drawing.Point(216, 204);
            this.textBox2.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(88, 26);
            this.textBox2.TabIndex = 215;
            this.textBox2.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.Location = new System.Drawing.Point(181, 205);
            this.label18.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(22, 18);
            this.label18.TabIndex = 214;
            this.label18.Text = "%";
            // 
            // txtper
            // 
            this.txtper.Location = new System.Drawing.Point(137, 204);
            this.txtper.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.txtper.Name = "txtper";
            this.txtper.Size = new System.Drawing.Size(36, 26);
            this.txtper.TabIndex = 205;
            this.txtper.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(25, 205);
            this.label9.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(62, 18);
            this.label9.TabIndex = 204;
            this.label9.Text = "Discount";
            // 
            // txttaxable
            // 
            this.txttaxable.Enabled = false;
            this.txttaxable.Location = new System.Drawing.Point(216, 240);
            this.txttaxable.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.txttaxable.Name = "txttaxable";
            this.txttaxable.Size = new System.Drawing.Size(88, 26);
            this.txttaxable.TabIndex = 200;
            this.txttaxable.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txthidqty
            // 
            this.txthidqty.Location = new System.Drawing.Point(216, 101);
            this.txthidqty.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.txthidqty.Name = "txthidqty";
            this.txthidqty.Size = new System.Drawing.Size(36, 26);
            this.txthidqty.TabIndex = 245;
            // 
            // txttempadd2
            // 
            this.txttempadd2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txttempadd2.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txttempadd2.Location = new System.Drawing.Point(467, 75);
            this.txttempadd2.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.txttempadd2.Name = "txttempadd2";
            this.txttempadd2.Size = new System.Drawing.Size(45, 26);
            this.txttempadd2.TabIndex = 270;
            // 
            // txttempadd1
            // 
            this.txttempadd1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txttempadd1.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txttempadd1.Location = new System.Drawing.Point(414, 75);
            this.txttempadd1.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.txttempadd1.Name = "txttempadd1";
            this.txttempadd1.Size = new System.Drawing.Size(45, 26);
            this.txttempadd1.TabIndex = 269;
            // 
            // button8
            // 
            this.button8.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.button8.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button8.Image = global::ACV.Properties.Resources.save;
            this.button8.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button8.Location = new System.Drawing.Point(516, 368);
            this.button8.Name = "button8";
            this.button8.Size = new System.Drawing.Size(90, 38);
            this.button8.TabIndex = 236;
            this.button8.Text = "Save";
            this.button8.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button8.UseVisualStyleBackColor = false;
            // 
            // button4
            // 
            this.button4.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.button4.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button4.Image = ((System.Drawing.Image)(resources.GetObject("button4.Image")));
            this.button4.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button4.Location = new System.Drawing.Point(425, 363);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(91, 38);
            this.button4.TabIndex = 229;
            this.button4.Text = "Back";
            this.button4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button4.UseVisualStyleBackColor = false;
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label33.Location = new System.Drawing.Point(119, 312);
            this.label33.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(73, 16);
            this.label33.TabIndex = 242;
            this.label33.Text = "ItemName";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(20, 316);
            this.label15.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(73, 16);
            this.label15.TabIndex = 242;
            this.label15.Text = "ItemName";
            // 
            // txtitemname
            // 
            this.txtitemname.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtitemname.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtitemname.Location = new System.Drawing.Point(6, 179);
            this.txtitemname.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.txtitemname.Name = "txtitemname";
            this.txtitemname.Size = new System.Drawing.Size(186, 26);
            this.txtitemname.TabIndex = 5;
            this.txtitemname.Click += new System.EventHandler(this.Txtitemname_Click);
            this.txtitemname.TextChanged += new System.EventHandler(this.txtitemname_TextChanged);
            this.txtitemname.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtitemname_KeyDown);
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label42.Location = new System.Drawing.Point(609, 319);
            this.label42.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(67, 16);
            this.label42.TabIndex = 261;
            this.label42.Text = "Addnotes";
            // 
            // buttcusok
            // 
            this.buttcusok.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.buttcusok.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttcusok.Image = global::ACV.Properties.Resources.ok1_25x25;
            this.buttcusok.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.buttcusok.Location = new System.Drawing.Point(681, 282);
            this.buttcusok.Name = "buttcusok";
            this.buttcusok.Size = new System.Drawing.Size(34, 11);
            this.buttcusok.TabIndex = 248;
            this.buttcusok.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.buttcusok.UseVisualStyleBackColor = false;
            this.buttcusok.Click += new System.EventHandler(this.buttcusok_Click);
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label32.Location = new System.Drawing.Point(264, 414);
            this.label32.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(78, 16);
            this.label32.TabIndex = 246;
            this.label32.Text = "BasicValue";
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label31.Location = new System.Drawing.Point(148, 399);
            this.label31.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(29, 16);
            this.label31.TabIndex = 244;
            this.label31.Text = "Qty";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(33, 398);
            this.label14.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(41, 16);
            this.label14.TabIndex = 240;
            this.label14.Text = "Price";
            // 
            // txtnotes
            // 
            this.txtnotes.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtnotes.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtnotes.Location = new System.Drawing.Point(294, 254);
            this.txtnotes.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.txtnotes.Name = "txtnotes";
            this.txtnotes.Size = new System.Drawing.Size(200, 22);
            this.txtnotes.TabIndex = 260;
            this.txtnotes.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtnotes_KeyDown);
            // 
            // txtbval
            // 
            this.txtbval.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtbval.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtbval.Location = new System.Drawing.Point(484, 179);
            this.txtbval.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.txtbval.Name = "txtbval";
            this.txtbval.Size = new System.Drawing.Size(91, 26);
            this.txtbval.TabIndex = 9;
            this.txtbval.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtbval.TextChanged += new System.EventHandler(this.txtbval_TextChanged);
            // 
            // txtqty
            // 
            this.txtqty.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtqty.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtqty.Location = new System.Drawing.Point(419, 179);
            this.txtqty.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.txtqty.Name = "txtqty";
            this.txtqty.Size = new System.Drawing.Size(65, 26);
            this.txtqty.TabIndex = 8;
            this.txtqty.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtqty.TextChanged += new System.EventHandler(this.txtqty_TextChanged);
            this.txtqty.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtqty_KeyDown);
            // 
            // dtpdc
            // 
            this.dtpdc.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpdc.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpdc.Location = new System.Drawing.Point(428, 107);
            this.dtpdc.Margin = new System.Windows.Forms.Padding(4);
            this.dtpdc.Name = "dtpdc";
            this.dtpdc.Size = new System.Drawing.Size(113, 26);
            this.dtpdc.TabIndex = 224;
            // 
            // txtitemcode
            // 
            this.txtitemcode.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtitemcode.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtitemcode.Location = new System.Drawing.Point(267, 349);
            this.txtitemcode.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.txtitemcode.Name = "txtitemcode";
            this.txtitemcode.Size = new System.Drawing.Size(119, 22);
            this.txtitemcode.TabIndex = 272;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(9, 55);
            this.label10.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(93, 18);
            this.label10.TabIndex = 273;
            this.label10.Text = "Reference No";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(9, 105);
            this.label11.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(85, 18);
            this.label11.TabIndex = 271;
            this.label11.Text = "Invoice Date";
            this.label11.Visible = false;
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label37.Location = new System.Drawing.Point(449, 280);
            this.label37.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(108, 16);
            this.label37.TabIndex = 250;
            this.label37.Text = "Place of Supply";
            // 
            // txtplace
            // 
            this.txtplace.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtplace.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtplace.Location = new System.Drawing.Point(295, 296);
            this.txtplace.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.txtplace.MaxLength = 100;
            this.txtplace.Name = "txtplace";
            this.txtplace.Size = new System.Drawing.Size(144, 22);
            this.txtplace.TabIndex = 249;
            this.txtplace.TextChanged += new System.EventHandler(this.txtplace_TextChanged);
            this.txtplace.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtplace_KeyDown_1);
            // 
            // txtpadd2
            // 
            this.txtpadd2.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtpadd2.Location = new System.Drawing.Point(231, 302);
            this.txtpadd2.MaxLength = 100;
            this.txtpadd2.Name = "txtpadd2";
            this.txtpadd2.Size = new System.Drawing.Size(120, 113);
            this.txtpadd2.TabIndex = 268;
            this.txtpadd2.Text = "";
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label41.Location = new System.Drawing.Point(432, 357);
            this.label41.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(114, 16);
            this.label41.TabIndex = 259;
            this.label41.Text = "Document Terms";
            // 
            // txtlisid
            // 
            this.txtlisid.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtlisid.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtlisid.Location = new System.Drawing.Point(178, 372);
            this.txtlisid.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.txtlisid.Name = "txtlisid";
            this.txtlisid.Size = new System.Drawing.Size(106, 22);
            this.txtlisid.TabIndex = 258;
            // 
            // txttgstval
            // 
            this.txttgstval.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txttgstval.Enabled = false;
            this.txttgstval.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txttgstval.Location = new System.Drawing.Point(586, 330);
            this.txttgstval.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.txttgstval.Name = "txttgstval";
            this.txttgstval.Size = new System.Drawing.Size(36, 22);
            this.txttgstval.TabIndex = 319;
            this.txttgstval.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txttgstp
            // 
            this.txttgstp.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txttgstp.Enabled = false;
            this.txttgstp.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txttgstp.Location = new System.Drawing.Point(543, 330);
            this.txttgstp.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.txttgstp.Name = "txttgstp";
            this.txttgstp.Size = new System.Drawing.Size(36, 22);
            this.txttgstp.TabIndex = 318;
            this.txttgstp.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // buttnnxt
            // 
            this.buttnnxt.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.buttnnxt.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttnnxt.Image = ((System.Drawing.Image)(resources.GetObject("buttnnxt.Image")));
            this.buttnnxt.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttnnxt.Location = new System.Drawing.Point(753, 325);
            this.buttnnxt.Margin = new System.Windows.Forms.Padding(4);
            this.buttnnxt.Name = "buttnnxt";
            this.buttnnxt.Size = new System.Drawing.Size(73, 30);
            this.buttnnxt.TabIndex = 239;
            this.buttnnxt.Text = "Next";
            this.buttnnxt.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.buttnnxt.UseVisualStyleBackColor = false;
            this.buttnnxt.Click += new System.EventHandler(this.buttnnxt_Click);
            // 
            // btnaddrcan
            // 
            this.btnaddrcan.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnaddrcan.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnaddrcan.Image = ((System.Drawing.Image)(resources.GetObject("btnaddrcan.Image")));
            this.btnaddrcan.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnaddrcan.Location = new System.Drawing.Point(613, 374);
            this.btnaddrcan.Margin = new System.Windows.Forms.Padding(4);
            this.btnaddrcan.Name = "btnaddrcan";
            this.btnaddrcan.Size = new System.Drawing.Size(60, 30);
            this.btnaddrcan.TabIndex = 222;
            this.btnaddrcan.Text = "Back";
            this.btnaddrcan.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnaddrcan.UseVisualStyleBackColor = false;
            this.btnaddrcan.Click += new System.EventHandler(this.btnaddrcan_Click_1);
            // 
            // button11
            // 
            this.button11.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.button11.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button11.Image = ((System.Drawing.Image)(resources.GetObject("button11.Image")));
            this.button11.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button11.Location = new System.Drawing.Point(780, 360);
            this.button11.Margin = new System.Windows.Forms.Padding(4);
            this.button11.Name = "button11";
            this.button11.Size = new System.Drawing.Size(73, 30);
            this.button11.TabIndex = 236;
            this.button11.Text = "Next";
            this.button11.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button11.UseVisualStyleBackColor = false;
            this.button11.Click += new System.EventHandler(this.button11_Click_2);
            // 
            // button12
            // 
            this.button12.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.button12.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button12.Image = ((System.Drawing.Image)(resources.GetObject("button12.Image")));
            this.button12.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button12.Location = new System.Drawing.Point(681, 378);
            this.button12.Margin = new System.Windows.Forms.Padding(4);
            this.button12.Name = "button12";
            this.button12.Size = new System.Drawing.Size(60, 30);
            this.button12.TabIndex = 237;
            this.button12.Text = "Back";
            this.button12.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button12.UseVisualStyleBackColor = false;
            this.button12.Click += new System.EventHandler(this.button12_Click_2);
            // 
            // txtgen2
            // 
            this.txtgen2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtgen2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtgen2.Location = new System.Drawing.Point(93, 288);
            this.txtgen2.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.txtgen2.Name = "txtgen2";
            this.txtgen2.Size = new System.Drawing.Size(68, 22);
            this.txtgen2.TabIndex = 335;
            this.txtgen2.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txtgen3
            // 
            this.txtgen3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtgen3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtgen3.Location = new System.Drawing.Point(164, 253);
            this.txtgen3.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.txtgen3.Name = "txtgen3";
            this.txtgen3.Size = new System.Drawing.Size(68, 22);
            this.txtgen3.TabIndex = 336;
            this.txtgen3.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txtigstp
            // 
            this.txtigstp.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtigstp.Enabled = false;
            this.txtigstp.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtigstp.Location = new System.Drawing.Point(625, 306);
            this.txtigstp.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.txtigstp.Name = "txtigstp";
            this.txtigstp.Size = new System.Drawing.Size(36, 22);
            this.txtigstp.TabIndex = 313;
            this.txtigstp.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label52
            // 
            this.label52.AutoSize = true;
            this.label52.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label52.Location = new System.Drawing.Point(641, 328);
            this.label52.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label52.Name = "label52";
            this.label52.Size = new System.Drawing.Size(18, 16);
            this.label52.TabIndex = 312;
            this.label52.Text = "%";
            // 
            // txtsgstp
            // 
            this.txtsgstp.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtsgstp.Enabled = false;
            this.txtsgstp.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtsgstp.Location = new System.Drawing.Point(795, 277);
            this.txtsgstp.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.txtsgstp.Name = "txtsgstp";
            this.txtsgstp.Size = new System.Drawing.Size(36, 22);
            this.txtsgstp.TabIndex = 309;
            this.txtsgstp.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label50
            // 
            this.label50.AutoSize = true;
            this.label50.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label50.Location = new System.Drawing.Point(585, 292);
            this.label50.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label50.Name = "label50";
            this.label50.Size = new System.Drawing.Size(18, 16);
            this.label50.TabIndex = 308;
            this.label50.Text = "%";
            // 
            // label51
            // 
            this.label51.AutoSize = true;
            this.label51.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label51.Location = new System.Drawing.Point(750, 279);
            this.label51.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label51.Name = "label51";
            this.label51.Size = new System.Drawing.Size(44, 16);
            this.label51.TabIndex = 307;
            this.label51.Text = "SGST";
            // 
            // txttcgval
            // 
            this.txttcgval.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txttcgval.Enabled = false;
            this.txttcgval.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txttcgval.Location = new System.Drawing.Point(461, 263);
            this.txttcgval.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.txttcgval.Name = "txttcgval";
            this.txttcgval.Size = new System.Drawing.Size(112, 22);
            this.txttcgval.TabIndex = 306;
            this.txttcgval.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txttcgstp
            // 
            this.txttcgstp.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txttcgstp.Enabled = false;
            this.txttcgstp.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txttcgstp.Location = new System.Drawing.Point(795, 243);
            this.txttcgstp.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.txttcgstp.Name = "txttcgstp";
            this.txttcgstp.Size = new System.Drawing.Size(36, 22);
            this.txttcgstp.TabIndex = 305;
            this.txttcgstp.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label49
            // 
            this.label49.AutoSize = true;
            this.label49.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label49.Location = new System.Drawing.Point(585, 258);
            this.label49.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label49.Name = "label49";
            this.label49.Size = new System.Drawing.Size(18, 16);
            this.label49.TabIndex = 304;
            this.label49.Text = "%";
            // 
            // label48
            // 
            this.label48.AutoSize = true;
            this.label48.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label48.Location = new System.Drawing.Point(750, 245);
            this.label48.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label48.Name = "label48";
            this.label48.Size = new System.Drawing.Size(44, 16);
            this.label48.TabIndex = 303;
            this.label48.Text = "CGST";
            // 
            // txttsgval
            // 
            this.txttsgval.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txttsgval.Enabled = false;
            this.txttsgval.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txttsgval.Location = new System.Drawing.Point(629, 254);
            this.txttsgval.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.txttsgval.Name = "txttsgval";
            this.txttsgval.Size = new System.Drawing.Size(112, 22);
            this.txttsgval.TabIndex = 310;
            this.txttsgval.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txtRefNo
            // 
            this.txtRefNo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtRefNo.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtRefNo.Location = new System.Drawing.Point(9, 76);
            this.txtRefNo.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.txtRefNo.Name = "txtRefNo";
            this.txtRefNo.Size = new System.Drawing.Size(211, 26);
            this.txtRefNo.TabIndex = 2;
            // 
            // dcdate
            // 
            this.dcdate.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dcdate.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dcdate.Location = new System.Drawing.Point(6, 127);
            this.dcdate.Margin = new System.Windows.Forms.Padding(4);
            this.dcdate.Name = "dcdate";
            this.dcdate.Size = new System.Drawing.Size(113, 26);
            this.dcdate.TabIndex = 3;
            this.dcdate.Visible = false;
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label34.Location = new System.Drawing.Point(575, 217);
            this.label34.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(67, 16);
            this.label34.TabIndex = 329;
            this.label34.Text = "Addnotes";
            // 
            // txtcharges
            // 
            this.txtcharges.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtcharges.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtcharges.Location = new System.Drawing.Point(1108, 102);
            this.txtcharges.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.txtcharges.Name = "txtcharges";
            this.txtcharges.Size = new System.Drawing.Size(112, 26);
            this.txtcharges.TabIndex = 289;
            this.txtcharges.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtcharges.TextChanged += new System.EventHandler(this.txtcharges_TextChanged_1);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(996, 102);
            this.label12.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(96, 18);
            this.label12.TabIndex = 288;
            this.label12.Text = "Other Charges";
            // 
            // txtlistid
            // 
            this.txtlistid.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtlistid.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtlistid.Location = new System.Drawing.Point(343, 243);
            this.txtlistid.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.txtlistid.Name = "txtlistid";
            this.txtlistid.Size = new System.Drawing.Size(69, 26);
            this.txtlistid.TabIndex = 426;
            this.txtlistid.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label61
            // 
            this.label61.AutoSize = true;
            this.label61.Font = new System.Drawing.Font("Calibri", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label61.Location = new System.Drawing.Point(1025, 441);
            this.label61.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label61.Name = "label61";
            this.label61.Size = new System.Drawing.Size(85, 21);
            this.label61.TabIndex = 339;
            this.label61.Text = "Total Value";
            this.label61.Visible = false;
            // 
            // txttaxtot
            // 
            this.txttaxtot.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txttaxtot.Enabled = false;
            this.txttaxtot.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txttaxtot.Location = new System.Drawing.Point(1120, 437);
            this.txttaxtot.Margin = new System.Windows.Forms.Padding(5);
            this.txttaxtot.Name = "txttaxtot";
            this.txttaxtot.Size = new System.Drawing.Size(101, 26);
            this.txttaxtot.TabIndex = 338;
            this.txttaxtot.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txttaxtot.Visible = false;
            // 
            // label77
            // 
            this.label77.AutoSize = true;
            this.label77.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label77.Location = new System.Drawing.Point(669, 158);
            this.label77.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label77.Name = "label77";
            this.label77.Size = new System.Drawing.Size(63, 18);
            this.label77.TabIndex = 431;
            this.label77.Text = "TaxValue";
            // 
            // txtTaxvalue
            // 
            this.txtTaxvalue.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtTaxvalue.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTaxvalue.Location = new System.Drawing.Point(675, 179);
            this.txtTaxvalue.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.txtTaxvalue.Name = "txtTaxvalue";
            this.txtTaxvalue.Size = new System.Drawing.Size(91, 26);
            this.txtTaxvalue.TabIndex = 11;
            this.txtTaxvalue.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // cboTax
            // 
            this.cboTax.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboTax.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboTax.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboTax.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboTax.FormattingEnabled = true;
            this.cboTax.Location = new System.Drawing.Point(574, 179);
            this.cboTax.Name = "cboTax";
            this.cboTax.Size = new System.Drawing.Size(101, 26);
            this.cboTax.TabIndex = 10;
            this.cboTax.SelectedIndexChanged += new System.EventHandler(this.cboTax_SelectedIndexChanged);
            // 
            // label76
            // 
            this.label76.AutoSize = true;
            this.label76.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label76.Location = new System.Drawing.Point(571, 159);
            this.label76.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label76.Name = "label76";
            this.label76.Size = new System.Drawing.Size(28, 18);
            this.label76.TabIndex = 429;
            this.label76.Text = "Tax";
            // 
            // button7
            // 
            this.button7.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.button7.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button7.Image = global::ACV.Properties.Resources.ok1_25x25;
            this.button7.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.button7.Location = new System.Drawing.Point(924, 176);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(34, 32);
            this.button7.TabIndex = 14;
            this.button7.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button7.UseVisualStyleBackColor = false;
            this.button7.Click += new System.EventHandler(this.button7_Click);
            this.button7.KeyDown += new System.Windows.Forms.KeyEventHandler(this.button7_KeyDown);
            // 
            // label78
            // 
            this.label78.AutoSize = true;
            this.label78.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label78.Location = new System.Drawing.Point(755, 157);
            this.label78.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label78.Name = "label78";
            this.label78.Size = new System.Drawing.Size(73, 18);
            this.label78.TabIndex = 433;
            this.label78.Text = "No of Bags";
            // 
            // txtNoBag
            // 
            this.txtNoBag.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtNoBag.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNoBag.Location = new System.Drawing.Point(766, 179);
            this.txtNoBag.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.txtNoBag.Name = "txtNoBag";
            this.txtNoBag.Size = new System.Drawing.Size(73, 26);
            this.txtNoBag.TabIndex = 12;
            this.txtNoBag.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txtexcise
            // 
            this.txtexcise.BackColor = System.Drawing.SystemColors.HighlightText;
            this.txtexcise.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtexcise.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtexcise.Location = new System.Drawing.Point(846, 435);
            this.txtexcise.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.txtexcise.Name = "txtexcise";
            this.txtexcise.Size = new System.Drawing.Size(112, 26);
            this.txtexcise.TabIndex = 317;
            this.txtexcise.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtexcise.Visible = false;
            // 
            // label46
            // 
            this.label46.AutoSize = true;
            this.label46.Font = new System.Drawing.Font("Calibri", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label46.Location = new System.Drawing.Point(739, 435);
            this.label46.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label46.Name = "label46";
            this.label46.Size = new System.Drawing.Size(113, 21);
            this.label46.TabIndex = 300;
            this.label46.Text = "Discount Value";
            this.label46.Visible = false;
            // 
            // txttdisc
            // 
            this.txttdisc.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.txttdisc.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txttdisc.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txttdisc.Location = new System.Drawing.Point(846, 434);
            this.txttdisc.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.txttdisc.Name = "txttdisc";
            this.txttdisc.Size = new System.Drawing.Size(112, 26);
            this.txttdisc.TabIndex = 299;
            this.txttdisc.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txttdisc.Visible = false;
            // 
            // label43
            // 
            this.label43.AutoSize = true;
            this.label43.Font = new System.Drawing.Font("Calibri", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label43.Location = new System.Drawing.Point(765, 438);
            this.label43.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(87, 21);
            this.label43.TabIndex = 296;
            this.label43.Text = "Discount %";
            this.label43.Visible = false;
            // 
            // txttdis
            // 
            this.txttdis.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txttdis.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txttdis.Location = new System.Drawing.Point(846, 437);
            this.txttdis.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.txttdis.Name = "txttdis";
            this.txttdis.Size = new System.Drawing.Size(112, 26);
            this.txttdis.TabIndex = 295;
            this.txttdis.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txttdis.Visible = false;
            this.txttdis.TextChanged += new System.EventHandler(this.txttdis_TextChanged_1);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Calibri", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(748, 435);
            this.label8.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(103, 21);
            this.label8.TabIndex = 290;
            this.label8.Text = "Taxable Value";
            this.label8.Visible = false;
            // 
            // label79
            // 
            this.label79.AutoSize = true;
            this.label79.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label79.Location = new System.Drawing.Point(834, 156);
            this.label79.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label79.Name = "label79";
            this.label79.Size = new System.Drawing.Size(66, 18);
            this.label79.TabIndex = 435;
            this.label79.Text = "HSNCode";
            // 
            // txtHSNCode
            // 
            this.txtHSNCode.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtHSNCode.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtHSNCode.Location = new System.Drawing.Point(840, 179);
            this.txtHSNCode.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.txtHSNCode.Name = "txtHSNCode";
            this.txtHSNCode.ReadOnly = true;
            this.txtHSNCode.Size = new System.Drawing.Size(84, 26);
            this.txtHSNCode.TabIndex = 13;
            this.txtHSNCode.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(958, 248);
            this.label1.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(61, 18);
            this.label1.TabIndex = 233;
            this.label1.Text = "Remarks";
            // 
            // txtrem
            // 
            this.txtrem.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtrem.Location = new System.Drawing.Point(958, 268);
            this.txtrem.MaxLength = 100;
            this.txtrem.Name = "txtrem";
            this.txtrem.Size = new System.Drawing.Size(260, 121);
            this.txtrem.TabIndex = 15;
            this.txtrem.Text = "";
            // 
            // txtSupplyToAdd
            // 
            this.txtSupplyToAdd.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSupplyToAdd.Location = new System.Drawing.Point(587, 51);
            this.txtSupplyToAdd.MaxLength = 100;
            this.txtSupplyToAdd.Name = "txtSupplyToAdd";
            this.txtSupplyToAdd.Size = new System.Drawing.Size(307, 101);
            this.txtSupplyToAdd.TabIndex = 439;
            this.txtSupplyToAdd.Text = "";
            // 
            // label47
            // 
            this.label47.AutoSize = true;
            this.label47.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label47.Location = new System.Drawing.Point(760, 72);
            this.label47.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label47.Name = "label47";
            this.label47.Size = new System.Drawing.Size(94, 18);
            this.label47.TabIndex = 302;
            this.label47.Text = "Product Value";
            // 
            // txttprdval
            // 
            this.txttprdval.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txttprdval.Enabled = false;
            this.txttprdval.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txttprdval.Location = new System.Drawing.Point(753, 80);
            this.txttprdval.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.txttprdval.Name = "txttprdval";
            this.txttprdval.Size = new System.Drawing.Size(112, 26);
            this.txttprdval.TabIndex = 301;
            this.txttprdval.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label80
            // 
            this.label80.AutoSize = true;
            this.label80.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label80.Location = new System.Drawing.Point(122, 103);
            this.label80.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label80.Name = "label80";
            this.label80.Size = new System.Drawing.Size(44, 18);
            this.label80.TabIndex = 436;
            this.label80.Text = "Dc No";
            this.label80.Visible = false;
            // 
            // txtdcno
            // 
            this.txtdcno.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtdcno.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtdcno.Location = new System.Drawing.Point(122, 127);
            this.txtdcno.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.txtdcno.Name = "txtdcno";
            this.txtdcno.Size = new System.Drawing.Size(125, 26);
            this.txtdcno.TabIndex = 4;
            this.txtdcno.Visible = false;
            this.txtdcno.TextChanged += new System.EventHandler(this.txtdcno_TextChanged_1);
            this.txtdcno.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtdcno_KeyDown);
            // 
            // HFGST
            // 
            this.HFGST.BackgroundColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.HFGST.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.HFGST.Location = new System.Drawing.Point(890, 337);
            this.HFGST.Margin = new System.Windows.Forms.Padding(4);
            this.HFGST.Name = "HFGST";
            this.HFGST.Size = new System.Drawing.Size(330, 96);
            this.HFGST.TabIndex = 337;
            this.HFGST.Visible = false;
            // 
            // txtSupplyTo
            // 
            this.txtSupplyTo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtSupplyTo.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSupplyTo.Location = new System.Drawing.Point(587, 25);
            this.txtSupplyTo.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.txtSupplyTo.MaxLength = 100;
            this.txtSupplyTo.Name = "txtSupplyTo";
            this.txtSupplyTo.Size = new System.Drawing.Size(307, 26);
            this.txtSupplyTo.TabIndex = 1;
            this.txtSupplyTo.Click += new System.EventHandler(this.txtSupplyTo_Click);
            this.txtSupplyTo.TextChanged += new System.EventHandler(this.txtSupplyTo_TextChanged);
            this.txtSupplyTo.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtSupplyTo_KeyDown);
            // 
            // label82
            // 
            this.label82.AutoSize = true;
            this.label82.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label82.Location = new System.Drawing.Point(189, 157);
            this.label82.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label82.Name = "label82";
            this.label82.Size = new System.Drawing.Size(72, 18);
            this.label82.TabIndex = 441;
            this.label82.Text = "Mill Name";
            // 
            // txtMillName
            // 
            this.txtMillName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtMillName.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMillName.Location = new System.Drawing.Point(193, 179);
            this.txtMillName.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.txtMillName.Name = "txtMillName";
            this.txtMillName.Size = new System.Drawing.Size(158, 26);
            this.txtMillName.TabIndex = 6;
            this.txtMillName.Click += new System.EventHandler(this.txtMillName_Click);
            this.txtMillName.TextChanged += new System.EventHandler(this.txtMillName_TextChanged);
            // 
            // label81
            // 
            this.label81.AutoSize = true;
            this.label81.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label81.Location = new System.Drawing.Point(586, 4);
            this.label81.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label81.Name = "label81";
            this.label81.Size = new System.Drawing.Size(67, 18);
            this.label81.TabIndex = 437;
            this.label81.Text = "Supply To";
            // 
            // label85
            // 
            this.label85.AutoSize = true;
            this.label85.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.label85.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label85.ForeColor = System.Drawing.Color.Red;
            this.label85.Location = new System.Drawing.Point(566, 32);
            this.label85.Name = "label85";
            this.label85.Size = new System.Drawing.Size(15, 18);
            this.label85.TabIndex = 444;
            this.label85.Text = "*";
            // 
            // label84
            // 
            this.label84.AutoSize = true;
            this.label84.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.label84.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label84.ForeColor = System.Drawing.Color.Red;
            this.label84.Location = new System.Drawing.Point(898, 30);
            this.label84.Name = "label84";
            this.label84.Size = new System.Drawing.Size(15, 18);
            this.label84.TabIndex = 443;
            this.label84.Text = "*";
            // 
            // label86
            // 
            this.label86.AutoSize = true;
            this.label86.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.label86.ForeColor = System.Drawing.Color.Red;
            this.label86.Location = new System.Drawing.Point(964, 184);
            this.label86.Name = "label86";
            this.label86.Size = new System.Drawing.Size(15, 18);
            this.label86.TabIndex = 445;
            this.label86.Text = "*";
            // 
            // label83
            // 
            this.label83.AutoSize = true;
            this.label83.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.label83.ForeColor = System.Drawing.Color.Red;
            this.label83.Location = new System.Drawing.Point(219, 81);
            this.label83.Name = "label83";
            this.label83.Size = new System.Drawing.Size(15, 18);
            this.label83.TabIndex = 442;
            this.label83.Text = "*";
            // 
            // mappnl
            // 
            this.mappnl.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.mappnl.Controls.Add(this.button16);
            this.mappnl.Controls.Add(this.txtlotno);
            this.mappnl.Controls.Add(this.txttarewt);
            this.mappnl.Controls.Add(this.txtstart);
            this.mappnl.Controls.Add(this.txtqty1);
            this.mappnl.Controls.Add(this.button10);
            this.mappnl.Controls.Add(this.button9);
            this.mappnl.Controls.Add(this.label74);
            this.mappnl.Controls.Add(this.label73);
            this.mappnl.Controls.Add(this.label72);
            this.mappnl.Controls.Add(this.txtbags);
            this.mappnl.Controls.Add(this.label71);
            this.mappnl.Controls.Add(this.label70);
            this.mappnl.Controls.Add(this.txtlotno1);
            this.mappnl.Controls.Add(this.button14);
            this.mappnl.Controls.Add(this.cbosGReturnItem);
            this.mappnl.Controls.Add(this.label62);
            this.mappnl.Controls.Add(this.txtNoogBags);
            this.mappnl.Controls.Add(this.label63);
            this.mappnl.Controls.Add(this.txtgrossgen);
            this.mappnl.Controls.Add(this.DataGridWeight);
            this.mappnl.Controls.Add(this.label64);
            this.mappnl.Controls.Add(this.txtWeight);
            this.mappnl.Controls.Add(this.label65);
            this.mappnl.Controls.Add(this.txtBagNo);
            this.mappnl.Controls.Add(this.label66);
            this.mappnl.Controls.Add(this.btnOk);
            this.mappnl.Controls.Add(this.label67);
            this.mappnl.Controls.Add(this.txtGrossWght);
            this.mappnl.Controls.Add(this.label68);
            this.mappnl.Controls.Add(this.txtTarWght);
            this.mappnl.Controls.Add(this.label69);
            this.mappnl.Controls.Add(this.txtlotno2);
            this.mappnl.Controls.Add(this.txtstart1);
            this.mappnl.Controls.Add(this.txttarewt1);
            this.mappnl.Controls.Add(this.label75);
            this.mappnl.Controls.Add(this.txtNofBags);
            this.mappnl.Location = new System.Drawing.Point(1232, 10);
            this.mappnl.Name = "mappnl";
            this.mappnl.Size = new System.Drawing.Size(719, 461);
            this.mappnl.TabIndex = 352;
            this.mappnl.Paint += new System.Windows.Forms.PaintEventHandler(this.mappnl_Paint);
            // 
            // button16
            // 
            this.button16.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.button16.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button16.Image = ((System.Drawing.Image)(resources.GetObject("button16.Image")));
            this.button16.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button16.Location = new System.Drawing.Point(520, 349);
            this.button16.Margin = new System.Windows.Forms.Padding(4);
            this.button16.Name = "button16";
            this.button16.Size = new System.Drawing.Size(60, 30);
            this.button16.TabIndex = 15;
            this.button16.Text = "Save";
            this.button16.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button16.UseVisualStyleBackColor = false;
            this.button16.Click += new System.EventHandler(this.button16_Click);
            // 
            // txtlotno
            // 
            this.txtlotno.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtlotno.Location = new System.Drawing.Point(575, 234);
            this.txtlotno.Name = "txtlotno";
            this.txtlotno.Size = new System.Drawing.Size(88, 26);
            this.txtlotno.TabIndex = 13;
            // 
            // txttarewt
            // 
            this.txttarewt.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txttarewt.Location = new System.Drawing.Point(575, 201);
            this.txttarewt.Name = "txttarewt";
            this.txttarewt.Size = new System.Drawing.Size(88, 26);
            this.txttarewt.TabIndex = 12;
            // 
            // txtstart
            // 
            this.txtstart.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtstart.Location = new System.Drawing.Point(575, 106);
            this.txtstart.Name = "txtstart";
            this.txtstart.Size = new System.Drawing.Size(88, 26);
            this.txtstart.TabIndex = 9;
            // 
            // txtqty1
            // 
            this.txtqty1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtqty1.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtqty1.Location = new System.Drawing.Point(337, 30);
            this.txtqty1.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.txtqty1.Name = "txtqty1";
            this.txtqty1.Size = new System.Drawing.Size(69, 26);
            this.txtqty1.TabIndex = 1;
            this.txtqty1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // button10
            // 
            this.button10.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button10.Location = new System.Drawing.Point(586, 278);
            this.button10.Name = "button10";
            this.button10.Size = new System.Drawing.Size(85, 28);
            this.button10.TabIndex = 14;
            this.button10.Text = "Generate";
            this.button10.UseVisualStyleBackColor = true;
            this.button10.Click += new System.EventHandler(this.button10_Click_3);
            // 
            // button9
            // 
            this.button9.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button9.Location = new System.Drawing.Point(499, 277);
            this.button9.Name = "button9";
            this.button9.Size = new System.Drawing.Size(81, 28);
            this.button9.TabIndex = 16;
            this.button9.Text = "Reset";
            this.button9.UseVisualStyleBackColor = true;
            // 
            // label74
            // 
            this.label74.AutoSize = true;
            this.label74.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label74.Location = new System.Drawing.Point(516, 238);
            this.label74.Name = "label74";
            this.label74.Size = new System.Drawing.Size(48, 18);
            this.label74.TabIndex = 434;
            this.label74.Text = "Lot No";
            // 
            // label73
            // 
            this.label73.AutoSize = true;
            this.label73.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label73.Location = new System.Drawing.Point(509, 205);
            this.label73.Name = "label73";
            this.label73.Size = new System.Drawing.Size(56, 18);
            this.label73.TabIndex = 433;
            this.label73.Text = "Tare Wt";
            // 
            // label72
            // 
            this.label72.AutoSize = true;
            this.label72.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label72.Location = new System.Drawing.Point(510, 110);
            this.label72.Name = "label72";
            this.label72.Size = new System.Drawing.Size(58, 18);
            this.label72.TabIndex = 432;
            this.label72.Text = "Start No\r\n";
            // 
            // txtbags
            // 
            this.txtbags.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtbags.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtbags.Location = new System.Drawing.Point(410, 31);
            this.txtbags.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.txtbags.Name = "txtbags";
            this.txtbags.Size = new System.Drawing.Size(69, 26);
            this.txtbags.TabIndex = 2;
            this.txtbags.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label71
            // 
            this.label71.AutoSize = true;
            this.label71.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label71.Location = new System.Drawing.Point(410, 12);
            this.label71.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label71.Name = "label71";
            this.label71.Size = new System.Drawing.Size(67, 18);
            this.label71.TabIndex = 429;
            this.label71.Text = "NoofBags";
            // 
            // label70
            // 
            this.label70.AutoSize = true;
            this.label70.Location = new System.Drawing.Point(22, 69);
            this.label70.Name = "label70";
            this.label70.Size = new System.Drawing.Size(47, 16);
            this.label70.TabIndex = 427;
            this.label70.Text = "Lot No";
            // 
            // txtlotno1
            // 
            this.txtlotno1.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtlotno1.Location = new System.Drawing.Point(10, 85);
            this.txtlotno1.Name = "txtlotno1";
            this.txtlotno1.Size = new System.Drawing.Size(88, 26);
            this.txtlotno1.TabIndex = 3;
            // 
            // button14
            // 
            this.button14.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.button14.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button14.Image = ((System.Drawing.Image)(resources.GetObject("button14.Image")));
            this.button14.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button14.Location = new System.Drawing.Point(587, 349);
            this.button14.Name = "button14";
            this.button14.Size = new System.Drawing.Size(57, 30);
            this.button14.TabIndex = 17;
            this.button14.Text = "Exit";
            this.button14.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button14.UseVisualStyleBackColor = false;
            this.button14.Click += new System.EventHandler(this.button14_Click);
            // 
            // cbosGReturnItem
            // 
            this.cbosGReturnItem.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cbosGReturnItem.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cbosGReturnItem.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbosGReturnItem.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbosGReturnItem.FormattingEnabled = true;
            this.cbosGReturnItem.Location = new System.Drawing.Point(7, 32);
            this.cbosGReturnItem.Name = "cbosGReturnItem";
            this.cbosGReturnItem.Size = new System.Drawing.Size(323, 26);
            this.cbosGReturnItem.TabIndex = 0;
            this.cbosGReturnItem.SelectedIndexChanged += new System.EventHandler(this.cbosGReturnItem_SelectedIndexChanged);
            this.cbosGReturnItem.SelectedValueChanged += new System.EventHandler(this.cbosGReturnItem_SelectedValueChanged);
            // 
            // label62
            // 
            this.label62.AutoSize = true;
            this.label62.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label62.Location = new System.Drawing.Point(493, 140);
            this.label62.Name = "label62";
            this.label62.Size = new System.Drawing.Size(73, 18);
            this.label62.TabIndex = 415;
            this.label62.Text = "No of Bags";
            // 
            // txtNoogBags
            // 
            this.txtNoogBags.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNoogBags.Location = new System.Drawing.Point(575, 136);
            this.txtNoogBags.Name = "txtNoogBags";
            this.txtNoogBags.Size = new System.Drawing.Size(88, 26);
            this.txtNoogBags.TabIndex = 10;
            // 
            // label63
            // 
            this.label63.AutoSize = true;
            this.label63.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label63.Location = new System.Drawing.Point(503, 172);
            this.label63.Name = "label63";
            this.label63.Size = new System.Drawing.Size(65, 18);
            this.label63.TabIndex = 413;
            this.label63.Text = "Gross Wt\r\n";
            // 
            // txtgrossgen
            // 
            this.txtgrossgen.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtgrossgen.Location = new System.Drawing.Point(575, 168);
            this.txtgrossgen.Name = "txtgrossgen";
            this.txtgrossgen.Size = new System.Drawing.Size(88, 26);
            this.txtgrossgen.TabIndex = 11;
            // 
            // DataGridWeight
            // 
            this.DataGridWeight.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.ActiveCaption;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.DataGridWeight.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.DataGridWeight.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DataGridWeight.EnableHeadersVisualStyles = false;
            this.DataGridWeight.Location = new System.Drawing.Point(10, 111);
            this.DataGridWeight.Name = "DataGridWeight";
            this.DataGridWeight.RowHeadersVisible = false;
            this.DataGridWeight.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.DataGridWeight.Size = new System.Drawing.Size(469, 345);
            this.DataGridWeight.TabIndex = 409;
            this.DataGridWeight.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.DataGridWeight_CellClick);
            this.DataGridWeight.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.DataGridWeight_CellContentClick);
            this.DataGridWeight.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.DataGridWeight_CellValueChanged);
            this.DataGridWeight.DoubleClick += new System.EventHandler(this.DataGridWeight_DoubleClick);
            this.DataGridWeight.KeyDown += new System.Windows.Forms.KeyEventHandler(this.DataGridWeight_KeyDown);
            // 
            // label64
            // 
            this.label64.AutoSize = true;
            this.label64.Location = new System.Drawing.Point(348, 69);
            this.label64.Name = "label64";
            this.label64.Size = new System.Drawing.Size(50, 16);
            this.label64.TabIndex = 405;
            this.label64.Text = "Weight";
            // 
            // txtWeight
            // 
            this.txtWeight.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtWeight.Location = new System.Drawing.Point(347, 85);
            this.txtWeight.Name = "txtWeight";
            this.txtWeight.Size = new System.Drawing.Size(93, 26);
            this.txtWeight.TabIndex = 7;
            // 
            // label65
            // 
            this.label65.AutoSize = true;
            this.label65.Location = new System.Drawing.Point(104, 69);
            this.label65.Name = "label65";
            this.label65.Size = new System.Drawing.Size(54, 16);
            this.label65.TabIndex = 402;
            this.label65.Text = "Bag No";
            // 
            // txtBagNo
            // 
            this.txtBagNo.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtBagNo.Location = new System.Drawing.Point(95, 85);
            this.txtBagNo.Name = "txtBagNo";
            this.txtBagNo.Size = new System.Drawing.Size(79, 26);
            this.txtBagNo.TabIndex = 4;
            // 
            // label66
            // 
            this.label66.AutoSize = true;
            this.label66.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label66.Location = new System.Drawing.Point(7, 15);
            this.label66.Name = "label66";
            this.label66.Size = new System.Drawing.Size(37, 18);
            this.label66.TabIndex = 400;
            this.label66.Text = "Item";
            // 
            // btnOk
            // 
            this.btnOk.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnOk.Location = new System.Drawing.Point(439, 84);
            this.btnOk.Name = "btnOk";
            this.btnOk.Size = new System.Drawing.Size(40, 28);
            this.btnOk.TabIndex = 8;
            this.btnOk.Text = "OK";
            this.btnOk.UseVisualStyleBackColor = true;
            this.btnOk.Click += new System.EventHandler(this.btnOk_Click);
            // 
            // label67
            // 
            this.label67.AutoSize = true;
            this.label67.Location = new System.Drawing.Point(172, 69);
            this.label67.Name = "label67";
            this.label67.Size = new System.Drawing.Size(44, 16);
            this.label67.TabIndex = 411;
            this.label67.Text = "Gross";
            // 
            // txtGrossWght
            // 
            this.txtGrossWght.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtGrossWght.Location = new System.Drawing.Point(174, 85);
            this.txtGrossWght.Name = "txtGrossWght";
            this.txtGrossWght.Size = new System.Drawing.Size(88, 26);
            this.txtGrossWght.TabIndex = 5;
            this.txtGrossWght.TextChanged += new System.EventHandler(this.txtGrossWght_TextChanged);
            // 
            // label68
            // 
            this.label68.AutoSize = true;
            this.label68.Location = new System.Drawing.Point(262, 69);
            this.label68.Name = "label68";
            this.label68.Size = new System.Drawing.Size(82, 16);
            this.label68.TabIndex = 410;
            this.label68.Text = "Tare Weight";
            // 
            // txtTarWght
            // 
            this.txtTarWght.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTarWght.Location = new System.Drawing.Point(262, 85);
            this.txtTarWght.Name = "txtTarWght";
            this.txtTarWght.Size = new System.Drawing.Size(86, 26);
            this.txtTarWght.TabIndex = 6;
            this.txtTarWght.TextChanged += new System.EventHandler(this.txtTarWght_TextChanged);
            // 
            // label69
            // 
            this.label69.AutoSize = true;
            this.label69.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label69.Location = new System.Drawing.Point(337, 13);
            this.label69.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label69.Name = "label69";
            this.label69.Size = new System.Drawing.Size(30, 18);
            this.label69.TabIndex = 359;
            this.label69.Text = "Qty";
            // 
            // txtlotno2
            // 
            this.txtlotno2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtlotno2.Enabled = false;
            this.txtlotno2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtlotno2.Location = new System.Drawing.Point(233, 199);
            this.txtlotno2.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.txtlotno2.Name = "txtlotno2";
            this.txtlotno2.Size = new System.Drawing.Size(91, 22);
            this.txtlotno2.TabIndex = 435;
            this.txtlotno2.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txtstart1
            // 
            this.txtstart1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtstart1.Enabled = false;
            this.txtstart1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtstart1.Location = new System.Drawing.Point(229, 230);
            this.txtstart1.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.txtstart1.Name = "txtstart1";
            this.txtstart1.Size = new System.Drawing.Size(90, 22);
            this.txtstart1.TabIndex = 431;
            this.txtstart1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txttarewt1
            // 
            this.txttarewt1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txttarewt1.Enabled = false;
            this.txttarewt1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txttarewt1.Location = new System.Drawing.Point(233, 173);
            this.txttarewt1.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.txttarewt1.Name = "txttarewt1";
            this.txttarewt1.Size = new System.Drawing.Size(89, 22);
            this.txttarewt1.TabIndex = 404;
            this.txttarewt1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label75
            // 
            this.label75.AutoSize = true;
            this.label75.Font = new System.Drawing.Font("Calibri", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label75.Location = new System.Drawing.Point(229, 155);
            this.label75.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label75.Name = "label75";
            this.label75.Size = new System.Drawing.Size(86, 21);
            this.label75.TabIndex = 431;
            this.label75.Text = "No Of Bags";
            // 
            // txtNofBags
            // 
            this.txtNofBags.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtNofBags.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNofBags.Location = new System.Drawing.Point(229, 178);
            this.txtNofBags.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.txtNofBags.Name = "txtNofBags";
            this.txtNofBags.Size = new System.Drawing.Size(102, 26);
            this.txtNofBags.TabIndex = 429;
            this.txtNofBags.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // btnsave
            // 
            this.btnsave.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnsave.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnsave.Image = ((System.Drawing.Image)(resources.GetObject("btnsave.Image")));
            this.btnsave.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnsave.Location = new System.Drawing.Point(1102, 469);
            this.btnsave.Margin = new System.Windows.Forms.Padding(4);
            this.btnsave.Name = "btnsave";
            this.btnsave.Size = new System.Drawing.Size(60, 30);
            this.btnsave.TabIndex = 221;
            this.btnsave.Text = "Save";
            this.btnsave.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnsave.UseVisualStyleBackColor = false;
            this.btnsave.Visible = false;
            this.btnsave.Click += new System.EventHandler(this.Btnsave_Click);
            // 
            // buttnfinbk
            // 
            this.buttnfinbk.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.buttnfinbk.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttnfinbk.Image = ((System.Drawing.Image)(resources.GetObject("buttnfinbk.Image")));
            this.buttnfinbk.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttnfinbk.Location = new System.Drawing.Point(1161, 469);
            this.buttnfinbk.Margin = new System.Windows.Forms.Padding(4);
            this.buttnfinbk.Name = "buttnfinbk";
            this.buttnfinbk.Size = new System.Drawing.Size(60, 30);
            this.buttnfinbk.TabIndex = 241;
            this.buttnfinbk.Text = "Back";
            this.buttnfinbk.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.buttnfinbk.UseVisualStyleBackColor = false;
            this.buttnfinbk.Visible = false;
            this.buttnfinbk.Click += new System.EventHandler(this.buttnfinbk_Click);
            // 
            // FrmPurchaseOrder
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1226, 502);
            this.Controls.Add(this.mappnl);
            this.Controls.Add(this.btnBagNoMapping);
            this.Controls.Add(this.btnsave);
            this.Controls.Add(this.buttnfinbk);
            this.Controls.Add(this.panadd);
            this.Controls.Add(this.Editpan);
            this.Controls.Add(this.Genpan);
            this.Controls.Add(this.Taxpan);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Margin = new System.Windows.Forms.Padding(4);
            this.MaximizeBox = false;
            this.Name = "FrmPurchaseOrder";
            this.Text = "Purchase Order";
            this.Load += new System.EventHandler(this.FrmBill_Load);
            this.Genpan.ResumeLayout(false);
            this.Genpan.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.HFGP)).EndInit();
            this.Taxpan.ResumeLayout(false);
            this.Taxpan.PerformLayout();
            this.addipan.ResumeLayout(false);
            this.addipan.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.HFGT)).EndInit();
            this.panadd.ResumeLayout(false);
            this.panadd.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.Editpan.ResumeLayout(false);
            this.Editpan.PerformLayout();
            this.PanelSearch.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.DataGridCommon)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.HFIT)).EndInit();
            this.pantax.ResumeLayout(false);
            this.pantax.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.HFGST)).EndInit();
            this.mappnl.ResumeLayout(false);
            this.mappnl.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridWeight)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel Genpan;
        private System.Windows.Forms.DataGridView HFGP;
        private System.Windows.Forms.Panel Taxpan;
        private System.Windows.Forms.Button btnsave;
        private System.Windows.Forms.Button btnaddrcan;
        private System.Windows.Forms.Panel addipan;
        private System.Windows.Forms.TextBox txttotaddd;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Panel panadd;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label lblno1;
        private System.Windows.Forms.Label lblno2;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel3;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel2;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private System.Windows.Forms.Button buttnnvfst;
        private System.Windows.Forms.Button buttnnxtlft;
        private System.Windows.Forms.Button btnfinnxt;
        private System.Windows.Forms.Button buttrnxt;
        private System.Windows.Forms.Button buttnext1;
        private System.Windows.Forms.CheckBox chkact;
        private System.Windows.Forms.Button butcan;
        private System.Windows.Forms.Button butedit;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.TextBox txtscr5;
        private System.Windows.Forms.TextBox txtscr4;
        private System.Windows.Forms.TextBox Txtscr3;
        private System.Windows.Forms.TextBox Txtscr2;
        private System.Windows.Forms.TextBox txtscr1;
        private System.Windows.Forms.TextBox txtscr6;
        private System.Windows.Forms.Button button11;
        private System.Windows.Forms.Button button12;
        private System.Windows.Forms.Button buttnnxt;
        private System.Windows.Forms.Button buttnfinbk;
        private System.Windows.Forms.Panel Editpan;
        private System.Windows.Forms.Label label42;
        private System.Windows.Forms.TextBox txtnotes;
        private System.Windows.Forms.Label label41;
        private System.Windows.Forms.TextBox txtlisid;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.TextBox txttrans;
        private System.Windows.Forms.DateTimePicker Dtprem;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.DateTimePicker Dtppre;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.TextBox txtplace;
        private System.Windows.Forms.Button buttcusok;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.TextBox txtbval;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.TextBox Txttot;
        private System.Windows.Forms.TextBox txtqty;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtot;
        private System.Windows.Forms.Button button8;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel pantax;
        private System.Windows.Forms.RichTextBox txttitem;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.TextBox txttqty;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.TextBox Txtrate;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.TextBox txtbasic;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.TextBox txttotal;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.TextBox txtigcst;
        private System.Windows.Forms.ComboBox cboigst;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.TextBox txtsgst;
        private System.Windows.Forms.ComboBox SGST;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.TextBox txtcgst;
        private System.Windows.Forms.ComboBox cbocgst;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.TextBox txtper;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox txttaxable;
        private System.Windows.Forms.TextBox txthidqty;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.TextBox txtamt;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.DateTimePicker DTPDOCDT;
        private System.Windows.Forms.DataGridView HFIT;
        private System.Windows.Forms.DateTimePicker Dtpdt;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtdcno;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtname;
        private System.Windows.Forms.Label Phone;
        private System.Windows.Forms.TextBox txtgrn;
        private System.Windows.Forms.TextBox txtpuid;
        private System.Windows.Forms.TextBox txtgrnid;
        private System.Windows.Forms.TextBox txtdcid;
        private System.Windows.Forms.RichTextBox txtrem;
        private System.Windows.Forms.TextBox txttitemid;
        private System.Windows.Forms.Label label44;
        private System.Windows.Forms.TextBox txtuom;
        private System.Windows.Forms.Label label55;
        private System.Windows.Forms.RichTextBox txtpadd2;
        private System.Windows.Forms.RichTextBox txtpadd1;
        private System.Windows.Forms.TextBox txttempadd2;
        private System.Windows.Forms.TextBox txttempadd1;
        private System.Windows.Forms.Label label56;
        private System.Windows.Forms.DataGridView HFGT;
        private System.Windows.Forms.DateTimePicker dtpdc;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox txtitemcode;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.TextBox txtscr7;
        private System.Windows.Forms.TextBox txtexcise;
        private System.Windows.Forms.Label label54;
        private System.Windows.Forms.TextBox txtttot;
        private System.Windows.Forms.TextBox txtigval;
        private System.Windows.Forms.TextBox txtigstp;
        private System.Windows.Forms.Label label52;
        private System.Windows.Forms.Label label53;
        private System.Windows.Forms.TextBox txttsgval;
        private System.Windows.Forms.TextBox txtsgstp;
        private System.Windows.Forms.Label label50;
        private System.Windows.Forms.Label label51;
        private System.Windows.Forms.TextBox txttcgval;
        private System.Windows.Forms.TextBox txttcgstp;
        private System.Windows.Forms.Label label49;
        private System.Windows.Forms.Label label48;
        private System.Windows.Forms.Label label47;
        private System.Windows.Forms.TextBox txttprdval;
        private System.Windows.Forms.Label label46;
        private System.Windows.Forms.TextBox txttdisc;
        private System.Windows.Forms.TextBox txttbval;
        private System.Windows.Forms.Label label45;
        private System.Windows.Forms.Label label43;
        private System.Windows.Forms.TextBox txttdis;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox txtNetValue;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.TextBox txtRoundOff;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txtcharges;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox txttgstval;
        private System.Windows.Forms.TextBox txttgstp;
        private System.Windows.Forms.Button button13;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.Label label57;
        private System.Windows.Forms.Label label59;
        private System.Windows.Forms.TextBox txtRefNo;
        private System.Windows.Forms.TextBox txtprice;
        private System.Windows.Forms.TextBox txtscr8;
        private System.Windows.Forms.TextBox txtscr9;
        private System.Windows.Forms.Label label60;
        private System.Windows.Forms.DateTimePicker dtpfnt;
        private System.Windows.Forms.DateTimePicker dateTimePicker1;
        private System.Windows.Forms.DateTimePicker dcdate;
        private System.Windows.Forms.TextBox txtgen2;
        private System.Windows.Forms.TextBox txtgen3;
        private System.Windows.Forms.TextBox txtchargessum;
        private System.Windows.Forms.TextBox txtbasicval;
        private System.Windows.Forms.TextBox txttax;
        private System.Windows.Forms.TextBox txttotamt;
        private System.Windows.Forms.Label label58;
        private System.Windows.Forms.DataGridView HFGST;
        private System.Windows.Forms.Label label61;
        private System.Windows.Forms.TextBox txttaxtot;
        public System.Windows.Forms.TextBox txtpluid;
        internal System.Windows.Forms.Button button7;
        private System.Windows.Forms.Panel mappnl;
        private System.Windows.Forms.Button button14;
        private System.Windows.Forms.ComboBox cbosGReturnItem;
        private System.Windows.Forms.Label label62;
        private System.Windows.Forms.TextBox txtNoogBags;
        private System.Windows.Forms.Label label63;
        private System.Windows.Forms.TextBox txtgrossgen;
        private System.Windows.Forms.DataGridView DataGridWeight;
        private System.Windows.Forms.Label label64;
        private System.Windows.Forms.TextBox txtWeight;
        private System.Windows.Forms.Label label65;
        private System.Windows.Forms.TextBox txtBagNo;
        private System.Windows.Forms.Label label66;
        private System.Windows.Forms.Button btnOk;
        private System.Windows.Forms.Label label67;
        private System.Windows.Forms.TextBox txtGrossWght;
        private System.Windows.Forms.Label label68;
        private System.Windows.Forms.TextBox txtTarWght;
        private System.Windows.Forms.TextBox txttarewt1;
        private System.Windows.Forms.TextBox txtlistid;
        private System.Windows.Forms.Label label69;
        private System.Windows.Forms.Label label70;
        private System.Windows.Forms.TextBox txtlotno1;
        private System.Windows.Forms.TextBox txtbags;
        private System.Windows.Forms.Label label71;
        private System.Windows.Forms.TextBox txtlotno2;
        private System.Windows.Forms.Label label74;
        private System.Windows.Forms.Label label73;
        private System.Windows.Forms.Label label72;
        private System.Windows.Forms.TextBox txtstart1;
        private System.Windows.Forms.Button button10;
        private System.Windows.Forms.Button button9;
        private System.Windows.Forms.TextBox txtqty1;
        private System.Windows.Forms.Button btnBagNoMapping;
        private System.Windows.Forms.TextBox txtlotno;
        private System.Windows.Forms.TextBox txttarewt;
        private System.Windows.Forms.TextBox txtstart;
        private System.Windows.Forms.Button button16;
        private System.Windows.Forms.Panel PanelSearch;
        private System.Windows.Forms.DataGridView DataGridCommon;
        private System.Windows.Forms.Button btnSelect;
        private System.Windows.Forms.Button btnHide;
        public System.Windows.Forms.TextBox txtitemname;
        private System.Windows.Forms.Label label75;
        private System.Windows.Forms.TextBox txtNofBags;
        private System.Windows.Forms.Label label76;
        private System.Windows.Forms.Label label77;
        private System.Windows.Forms.TextBox txtTaxvalue;
        private System.Windows.Forms.Label label78;
        private System.Windows.Forms.TextBox txtNoBag;
        private System.Windows.Forms.Label label79;
        private System.Windows.Forms.Label label80;
        private System.Windows.Forms.Label label81;
        private System.Windows.Forms.TextBox txtSupplyTo;
        private System.Windows.Forms.RichTextBox txtSupplyToAdd;
        public System.Windows.Forms.TextBox txtMillName;
        private System.Windows.Forms.Label label82;
        private System.Windows.Forms.Label label84;
        private System.Windows.Forms.Label label83;
        private System.Windows.Forms.Label label86;
        private System.Windows.Forms.Label label85;
        public System.Windows.Forms.ComboBox cboTax;
        public System.Windows.Forms.TextBox txtHSNCode;
    }
}