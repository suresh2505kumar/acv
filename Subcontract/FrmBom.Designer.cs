﻿namespace Subcontract
{
    partial class FrmBom
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmBom));
            this.label14 = new System.Windows.Forms.Label();
            this.txtperqty = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.txtusedqty = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.txbomuom = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.txtbomcode = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.txtbomitem = new System.Windows.Forms.TextBox();
            this.hfg3 = new System.Windows.Forms.DataGridView();
            this.buttrqok = new System.Windows.Forms.Button();
            this.txtalterid = new System.Windows.Forms.TextBox();
            this.btnaddrcan = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txthour = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.txtprocessqty = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtsettime = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txtprocestime = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.txtrate = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.txtsqgno = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.txtproceesname = new System.Windows.Forms.TextBox();
            this.HFG4 = new System.Windows.Forms.DataGridView();
            this.txtprocessid = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.btnsave = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.hfg3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.HFG4)).BeginInit();
            this.SuspendLayout();
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Bookman Old Style", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(883, 39);
            this.label14.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(72, 19);
            this.label14.TabIndex = 142;
            this.label14.Text = "UsedPer";
            // 
            // txtperqty
            // 
            this.txtperqty.Location = new System.Drawing.Point(879, 63);
            this.txtperqty.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtperqty.Name = "txtperqty";
            this.txtperqty.Size = new System.Drawing.Size(84, 20);
            this.txtperqty.TabIndex = 141;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Bookman Old Style", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(791, 39);
            this.label13.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(73, 19);
            this.label13.TabIndex = 140;
            this.label13.Text = "UsedQty";
            // 
            // txtusedqty
            // 
            this.txtusedqty.Location = new System.Drawing.Point(787, 63);
            this.txtusedqty.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtusedqty.Name = "txtusedqty";
            this.txtusedqty.Size = new System.Drawing.Size(92, 20);
            this.txtusedqty.TabIndex = 139;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Bookman Old Style", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(723, 39);
            this.label12.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(43, 19);
            this.label12.TabIndex = 138;
            this.label12.Text = "Uom";
            // 
            // txbomuom
            // 
            this.txbomuom.Location = new System.Drawing.Point(716, 63);
            this.txbomuom.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txbomuom.Name = "txbomuom";
            this.txbomuom.Size = new System.Drawing.Size(72, 20);
            this.txbomuom.TabIndex = 137;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Bookman Old Style", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(550, 39);
            this.label11.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(80, 19);
            this.label11.TabIndex = 136;
            this.label11.Text = "ItemCode";
            // 
            // txtbomcode
            // 
            this.txtbomcode.Location = new System.Drawing.Point(548, 63);
            this.txtbomcode.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtbomcode.Name = "txtbomcode";
            this.txtbomcode.Size = new System.Drawing.Size(168, 20);
            this.txtbomcode.TabIndex = 135;
            this.txtbomcode.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtbomcode_KeyDown);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Bookman Old Style", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(155, 39);
            this.label10.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(43, 19);
            this.label10.TabIndex = 134;
            this.label10.Text = "Item";
            // 
            // txtbomitem
            // 
            this.txtbomitem.Location = new System.Drawing.Point(154, 63);
            this.txtbomitem.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtbomitem.Name = "txtbomitem";
            this.txtbomitem.Size = new System.Drawing.Size(396, 20);
            this.txtbomitem.TabIndex = 133;
            this.txtbomitem.TextChanged += new System.EventHandler(this.txtbomitem_TextChanged);
            this.txtbomitem.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtbomitem_KeyDown);
            // 
            // hfg3
            // 
            this.hfg3.BackgroundColor = System.Drawing.Color.White;
            this.hfg3.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.hfg3.Location = new System.Drawing.Point(154, 95);
            this.hfg3.Name = "hfg3";
            this.hfg3.Size = new System.Drawing.Size(812, 232);
            this.hfg3.TabIndex = 132;
            // 
            // buttrqok
            // 
            this.buttrqok.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.buttrqok.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttrqok.Image = ((System.Drawing.Image)(resources.GetObject("buttrqok.Image")));
            this.buttrqok.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.buttrqok.Location = new System.Drawing.Point(989, 58);
            this.buttrqok.Name = "buttrqok";
            this.buttrqok.Size = new System.Drawing.Size(34, 32);
            this.buttrqok.TabIndex = 144;
            this.buttrqok.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.buttrqok.UseVisualStyleBackColor = false;
            this.buttrqok.Click += new System.EventHandler(this.buttrqok_Click);
            // 
            // txtalterid
            // 
            this.txtalterid.Location = new System.Drawing.Point(424, 63);
            this.txtalterid.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtalterid.Name = "txtalterid";
            this.txtalterid.Size = new System.Drawing.Size(19, 20);
            this.txtalterid.TabIndex = 145;
            // 
            // btnaddrcan
            // 
            this.btnaddrcan.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnaddrcan.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnaddrcan.Image = ((System.Drawing.Image)(resources.GetObject("btnaddrcan.Image")));
            this.btnaddrcan.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnaddrcan.Location = new System.Drawing.Point(457, 639);
            this.btnaddrcan.Name = "btnaddrcan";
            this.btnaddrcan.Size = new System.Drawing.Size(93, 38);
            this.btnaddrcan.TabIndex = 147;
            this.btnaddrcan.Text = "Back";
            this.btnaddrcan.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnaddrcan.UseVisualStyleBackColor = false;
            this.btnaddrcan.Click += new System.EventHandler(this.btnaddrcan_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Bookman Old Style", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(155, 9);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(89, 19);
            this.label1.TabIndex = 148;
            this.label1.Text = "ItemBOM";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Bookman Old Style", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(902, 381);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(72, 19);
            this.label2.TabIndex = 182;
            this.label2.Text = "Rate/Hr";
            // 
            // txthour
            // 
            this.txthour.Location = new System.Drawing.Point(898, 407);
            this.txthour.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txthour.Name = "txthour";
            this.txthour.Size = new System.Drawing.Size(84, 20);
            this.txthour.TabIndex = 181;
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.button1.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.Image = ((System.Drawing.Image)(resources.GetObject("button1.Image")));
            this.button1.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.button1.Location = new System.Drawing.Point(989, 400);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(34, 32);
            this.button1.TabIndex = 179;
            this.button1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Bookman Old Style", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(816, 381);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(77, 19);
            this.label3.TabIndex = 178;
            this.label3.Text = "Proc. Qty";
            // 
            // txtprocessqty
            // 
            this.txtprocessqty.Location = new System.Drawing.Point(812, 407);
            this.txtprocessqty.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtprocessqty.Name = "txtprocessqty";
            this.txtprocessqty.Size = new System.Drawing.Size(86, 20);
            this.txtprocessqty.TabIndex = 177;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Bookman Old Style", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(644, 381);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(76, 19);
            this.label4.TabIndex = 175;
            this.label4.Text = "Set.Time";
            // 
            // txtsettime
            // 
            this.txtsettime.Location = new System.Drawing.Point(640, 407);
            this.txtsettime.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtsettime.Name = "txtsettime";
            this.txtsettime.Size = new System.Drawing.Size(86, 20);
            this.txtsettime.TabIndex = 174;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Bookman Old Style", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(730, 381);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(83, 19);
            this.label5.TabIndex = 176;
            this.label5.Text = "Proc.Time";
            // 
            // txtprocestime
            // 
            this.txtprocestime.Location = new System.Drawing.Point(726, 407);
            this.txtprocestime.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtprocestime.Name = "txtprocestime";
            this.txtprocestime.Size = new System.Drawing.Size(86, 20);
            this.txtprocestime.TabIndex = 173;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Bookman Old Style", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(580, 381);
            this.label6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(44, 19);
            this.label6.TabIndex = 172;
            this.label6.Text = "Rate";
            // 
            // txtrate
            // 
            this.txtrate.Location = new System.Drawing.Point(579, 407);
            this.txtrate.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtrate.Name = "txtrate";
            this.txtrate.Size = new System.Drawing.Size(61, 20);
            this.txtrate.TabIndex = 171;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Bookman Old Style", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(408, 381);
            this.label7.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(55, 19);
            this.label7.TabIndex = 170;
            this.label7.Text = "SeqNo";
            // 
            // txtsqgno
            // 
            this.txtsqgno.Location = new System.Drawing.Point(412, 407);
            this.txtsqgno.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtsqgno.Name = "txtsqgno";
            this.txtsqgno.Size = new System.Drawing.Size(167, 20);
            this.txtsqgno.TabIndex = 169;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Bookman Old Style", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(150, 381);
            this.label8.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(108, 19);
            this.label8.TabIndex = 168;
            this.label8.Text = "Processname";
            // 
            // txtproceesname
            // 
            this.txtproceesname.Location = new System.Drawing.Point(154, 407);
            this.txtproceesname.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtproceesname.Name = "txtproceesname";
            this.txtproceesname.Size = new System.Drawing.Size(258, 20);
            this.txtproceesname.TabIndex = 167;
            this.txtproceesname.TextChanged += new System.EventHandler(this.txtproceesname_TextChanged);
            this.txtproceesname.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtproceesname_KeyDown);
            // 
            // HFG4
            // 
            this.HFG4.BackgroundColor = System.Drawing.Color.White;
            this.HFG4.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.HFG4.Location = new System.Drawing.Point(154, 437);
            this.HFG4.Name = "HFG4";
            this.HFG4.Size = new System.Drawing.Size(828, 196);
            this.HFG4.TabIndex = 166;
            // 
            // txtprocessid
            // 
            this.txtprocessid.Location = new System.Drawing.Point(241, 407);
            this.txtprocessid.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtprocessid.Name = "txtprocessid";
            this.txtprocessid.Size = new System.Drawing.Size(26, 20);
            this.txtprocessid.TabIndex = 180;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Bookman Old Style", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(150, 343);
            this.label9.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(110, 19);
            this.label9.TabIndex = 183;
            this.label9.Text = "ItemProcess";
            // 
            // btnsave
            // 
            this.btnsave.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnsave.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnsave.Image = ((System.Drawing.Image)(resources.GetObject("btnsave.Image")));
            this.btnsave.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnsave.Location = new System.Drawing.Point(489, 437);
            this.btnsave.Name = "btnsave";
            this.btnsave.Size = new System.Drawing.Size(90, 38);
            this.btnsave.TabIndex = 146;
            this.btnsave.Text = "Save";
            this.btnsave.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnsave.UseVisualStyleBackColor = false;
            this.btnsave.Click += new System.EventHandler(this.btnsave_Click);
            // 
            // FrmBom
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1158, 688);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txthour);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.txtprocessqty);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.txtsettime);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.txtprocestime);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.txtrate);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.txtsqgno);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.txtproceesname);
            this.Controls.Add(this.HFG4);
            this.Controls.Add(this.txtprocessid);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnsave);
            this.Controls.Add(this.btnaddrcan);
            this.Controls.Add(this.buttrqok);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.txtperqty);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.txtusedqty);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.txbomuom);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.txtbomcode);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.txtbomitem);
            this.Controls.Add(this.hfg3);
            this.Controls.Add(this.txtalterid);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "FrmBom";
            this.Text = "FrmBom";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.FrmBom_Load);
            ((System.ComponentModel.ISupportInitialize)(this.hfg3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.HFG4)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button buttrqok;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox txtperqty;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox txtusedqty;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox txbomuom;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox txtbomcode;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox txtbomitem;
        private System.Windows.Forms.DataGridView hfg3;
        private System.Windows.Forms.TextBox txtalterid;
        private System.Windows.Forms.Button btnaddrcan;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txthour;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtprocessqty;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtsettime;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtprocestime;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtrate;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtsqgno;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txtproceesname;
        private System.Windows.Forms.DataGridView HFG4;
        private System.Windows.Forms.TextBox txtprocessid;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Button btnsave;
    }
}