﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Windows.Forms;

namespace ACV
{
    public partial class FrmCloseJobCard : Form
    {
        public FrmCloseJobCard()
        {
            InitializeComponent();
        }
        SQLDBHelper db = new SQLDBHelper();
        SqlConnection conn = new SqlConnection("Data Source=" + Genclass.data2 + "; Initial Catalog=" + Genclass.data5 + ";User id=" + Genclass.data3 + ";Password=" + Genclass.data4 + "");
        BindingSource bsIOItem = new BindingSource();
        BindingSource bsJobCard = new BindingSource();
        private void FrmJobCard_Load(object sender, EventArgs e)
        {
            LoadDataGrid();
            Genclass.Module.buttonstyleform(this);
            Genclass.Module.buttonstylepanel(panadd);
            LoadButtons(0);
        }
        protected void LoadButtons(int id)
        {
            try
            {
                if (id == 0)
                {
                    grBack.Visible = false;
                    grFront.Visible = true;
                    btnEdit.Visible = true;
                    btnExit.Visible = true;
                    btnFirst.Visible = true;
                    btnBack.Visible = true;
                    btnNxt.Visible = true;
                    btnLast.Visible = true;
                    PanelgridNos.Visible = true;
                    btnSave.Visible = false;
                    btnAddCancel.Visible = false;
                    btnSave.Text = "Save";
                    btnPrint.Visible = true;
                }
                else if (id == 1)
                {
                    grBack.Visible = true;
                    grFront.Visible = false;
                    btnEdit.Visible = false;
                    btnExit.Visible = false;
                    btnFirst.Visible = false;
                    btnBack.Visible = false;
                    btnNxt.Visible = false;
                    btnLast.Visible = false;
                    PanelgridNos.Visible = false;
                    btnSave.Visible = true;
                    btnAddCancel.Visible = true;
                    btnSave.Text = "Save";
                    btnPrint.Visible = false;
                }

                else if (id == 2)
                {
                    grBack.Visible = true;
                    grFront.Visible = false;
                    btnEdit.Visible = false;
                    btnExit.Visible = false;
                    btnFirst.Visible = false;
                    btnBack.Visible = false;
                    btnNxt.Visible = false;
                    btnLast.Visible = false;
                    PanelgridNos.Visible = false;
                    btnSave.Visible = true;
                    btnAddCancel.Visible = true;
                    btnSave.Text = "Update";
                    btnPrint.Visible = false;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }
        private void txtItemName_MouseClick(object sender, MouseEventArgs e)
        {

        }

        private void txtItemName_TextChanged(object sender, EventArgs e)
        {

        }

        private void DataGridCommon_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            try
            {

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void btnHide_Click(object sender, EventArgs e)
        {

        }
        protected void FillGrid(DataTable dt, int FillId)
        {
            try
            {

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            LoadButtons(1);
            Genclass.Module.ClearTextBox(this, grBack);
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                string Query = "Update JobCard Set IsClosed = 1,ClosedTime='" + DateTime.Now + "' Where JcUid =" + txtDocNo.Tag + "";
                db.ExecuteNonQuery(CommandType.Text, Query, conn);
                MessageBox.Show("Job Card Closed Sucessfully", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                LoadDataGrid();
                Genclass.Module.ClearTextBox(this, grBack);
                LoadButtons(0);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void btnAddCancel_Click(object sender, EventArgs e)
        {
            LoadButtons(0);
        }

        protected void LoadDataGrid()
        {
            try
            {
                DataTable dt = db.GetDataWithoutParam(CommandType.StoredProcedure, "SP_GetJobCard", conn);
                bsJobCard.DataSource = dt;
                fillGrid(dt);
                
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            try
            {
                int Index = DataGridJobCard.SelectedCells[0].RowIndex;
                txtItemName.Text = DataGridJobCard.Rows[Index].Cells[2].Value.ToString();
                txtItemName.Tag = DataGridJobCard.Rows[Index].Cells[6].Value.ToString();
                txtDocNo.Text = DataGridJobCard.Rows[Index].Cells[0].Value.ToString();
                txtDocNo.Tag = DataGridJobCard.Rows[Index].Cells[8].Value.ToString();
                txtQuantity.Text = DataGridJobCard.Rows[Index].Cells[3].Value.ToString();
                txtQuantity.Tag = DataGridJobCard.Rows[Index].Cells[7].Value.ToString();
                dtpDocDate.Text = DataGridJobCard.Rows[Index].Cells[1].Value.ToString();
                dtpreqDate.Text = DataGridJobCard.Rows[Index].Cells[4].Value.ToString();
                txtRemarks.Text = DataGridJobCard.Rows[Index].Cells[9].Value.ToString();
                txtTotalEnds.Text = DataGridJobCard.Rows[Index].Cells[10].Value.ToString();
                getjobcardDetail(txtDocNo.Text);
                LoadButtons(2);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void getjobcardDetail(string text)
        {
            try
            {
                SqlParameter[] parameters = { new SqlParameter("@JobCardNo", text) };
                DataTable dt = db.GetDataWithParam(CommandType.StoredProcedure, "SP_GetDetForCloseJobCard", parameters, conn);

                DataGridCloseJobCard.DataSource = null;
                DataGridCloseJobCard.AutoGenerateColumns = false;
                DataGridCloseJobCard.ColumnCount = 7;
                DataGridCloseJobCard.Columns[0].Name = "LoomNo";
                DataGridCloseJobCard.Columns[0].HeaderText = "LoomNo";
                DataGridCloseJobCard.Columns[0].DataPropertyName = "LoomNo";
                DataGridCloseJobCard.Columns[0].Width = 70;

                DataGridCloseJobCard.Columns[1].Name = "NetWt";
                DataGridCloseJobCard.Columns[1].HeaderText = "NetWt";
                DataGridCloseJobCard.Columns[1].DataPropertyName = "NetWt";
                DataGridCloseJobCard.Columns[1].Width = 80;

                DataGridCloseJobCard.Columns[2].Name = "Meters";
                DataGridCloseJobCard.Columns[2].HeaderText = "Meters";
                DataGridCloseJobCard.Columns[2].DataPropertyName = "Meters";
                DataGridCloseJobCard.Columns[2].Width = 80;

                DataGridCloseJobCard.Columns[3].Name = "Rolls";
                DataGridCloseJobCard.Columns[3].HeaderText = "Rolls";
                DataGridCloseJobCard.Columns[3].DataPropertyName = "Roll";
                DataGridCloseJobCard.Columns[3].Width = 50;

                DataGridCloseJobCard.Columns[4].Name = "Loadedon";
                DataGridCloseJobCard.Columns[4].HeaderText = "Loadedon";
                DataGridCloseJobCard.Columns[4].DataPropertyName = "Loadedon";
                DataGridCloseJobCard.Columns[4].Width = 150;

                DataGridCloseJobCard.Columns[5].Name = "Completedon";
                DataGridCloseJobCard.Columns[5].HeaderText = "Completedon";
                DataGridCloseJobCard.Columns[5].DataPropertyName = "Completedon";
                DataGridCloseJobCard.Columns[5].Width = 150;

                DataGridCloseJobCard.Columns[6].Name = "TotalHours";
                DataGridCloseJobCard.Columns[6].HeaderText = "TotalHours";
                DataGridCloseJobCard.Columns[6].DataPropertyName = "TotalHours";
                DataGridCloseJobCard.Columns[6].Width = 70;
                DataGridCloseJobCard.DataSource = dt;

                decimal Meters = 0;
                decimal Weight = 0;
                decimal Rolls = 0;
                decimal Hours = 0;
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    Meters += Convert.ToDecimal(dt.Rows[i]["Meters"].ToString());
                    Weight += Convert.ToDecimal(dt.Rows[i]["NetWt"].ToString());
                    Rolls += Convert.ToDecimal(dt.Rows[i]["Roll"].ToString());
                    Hours += Convert.ToDecimal(dt.Rows[i]["TotalHours"].ToString());
                }
                txtTotalMeters.Text = Meters.ToString("0.000");
                txtTotalWght.Text = Weight.ToString("0.000");
                txttotalHour.Text = Hours.ToString();
                txttotalRoll.Text = Rolls.ToString();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void fillGrid(DataTable dataTable)
        {
            try
            {
                DataGridJobCard.DataSource = null;
                DataGridJobCard.AutoGenerateColumns = false;
                DataGridJobCard.ColumnCount = 11;
                DataGridJobCard.Columns[0].Name = "DocNo";
                DataGridJobCard.Columns[0].HeaderText = "DocNo";
                DataGridJobCard.Columns[0].DataPropertyName = "DocNo";

                DataGridJobCard.Columns[1].Name = "DocDate";
                DataGridJobCard.Columns[1].HeaderText = "DocDate";
                DataGridJobCard.Columns[1].DataPropertyName = "DocDate";

                DataGridJobCard.Columns[2].Name = "ItemName";
                DataGridJobCard.Columns[2].HeaderText = "Sort No";
                DataGridJobCard.Columns[2].DataPropertyName = "ItemName";
                DataGridJobCard.Columns[2].Width = 150;
                DataGridJobCard.Columns[3].Name = "Qty";
                DataGridJobCard.Columns[3].HeaderText = "Qty";
                DataGridJobCard.Columns[3].DataPropertyName = "Qty";

                DataGridJobCard.Columns[4].Name = "ReqDate";
                DataGridJobCard.Columns[4].HeaderText = "ReqDate";
                DataGridJobCard.Columns[4].DataPropertyName = "ReqDate";

                DataGridJobCard.Columns[5].Name = "IODocNo";
                DataGridJobCard.Columns[5].HeaderText = "IODocNo";
                DataGridJobCard.Columns[5].DataPropertyName = "IODocNo";

                DataGridJobCard.Columns[6].Name = "ItemUid";
                DataGridJobCard.Columns[6].HeaderText = "ItemUid";
                DataGridJobCard.Columns[6].DataPropertyName = "ItemUid";
                DataGridJobCard.Columns[6].Visible = false;
                DataGridJobCard.Columns[7].Name = "IOLUid";
                DataGridJobCard.Columns[7].HeaderText = "IOLUid";
                DataGridJobCard.Columns[7].DataPropertyName = "IOLUid";
                DataGridJobCard.Columns[7].Visible = false;
                DataGridJobCard.Columns[8].Name = "JcUid";
                DataGridJobCard.Columns[8].HeaderText = "JcUid";
                DataGridJobCard.Columns[8].DataPropertyName = "JcUid";
                DataGridJobCard.Columns[8].Visible = false;
                DataGridJobCard.Columns[9].Name = "Remarks";
                DataGridJobCard.Columns[9].HeaderText = "Remarks";
                DataGridJobCard.Columns[9].DataPropertyName = "Remarks";
                DataGridJobCard.Columns[9].Visible = false;

                DataGridJobCard.Columns[10].Name = "TotalEnds";
                DataGridJobCard.Columns[10].HeaderText = "TotalEnds";
                DataGridJobCard.Columns[10].DataPropertyName = "TotalEnds";
                DataGridJobCard.Columns[10].Visible = false;
                DataGridJobCard.DataSource = bsJobCard;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void btnPrint_Click(object sender, EventArgs e)
        {
            try
            {
                int Index = DataGridJobCard.SelectedCells[0].RowIndex;
                int Uid = Convert.ToInt32(DataGridJobCard.Rows[Index].Cells[6].Value.ToString());
                Genclass.Dtype = 1000;
                Genclass.Prtid = Uid;
                Genclass.SortNo = DataGridJobCard.Rows[Index].Cells[2].Value.ToString();
                Genclass.Barcode = DataGridJobCard.Rows[Index].Cells[0].Value.ToString();
                Crviewer crv = new Crviewer();
                crv.Show();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void txtjobcardSearch_TextChanged(object sender, EventArgs e)
        {
            try
            {
                bsJobCard.Filter = string.Format("DocNo LIKE '%{0}%' or ItemName LIKE '%{1}%'", txtjobcardSearch.Text, txtjobcardSearch.Text);

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                if(checkBox1.Checked == true)
                {
                    string Query = @"select a.JcUid,a.DocNo,a.DocDate,a.ReqDate,c.SortNo as ItemName,d.DocNo as IODocNo,a.Qty,a.ItemUid,b.IOLUid,a.Remarks,a.TotalEnds from JobCard a inner join InternalorderList b on a.IOLuid = b.IOLUid
                                inner join SortDet c on b.ItemUid = c.Uid
                                inner join InternalOrder d on b.IOUid = d.IOUId
                                Where a.IsClosed=1
                                order by JcUid desc";
                    DataTable table = db.GetDataWithoutParam(CommandType.Text, Query, conn);
                    bsJobCard.DataSource = table;
                    fillGrid(table);
                }
                else
                {
                    LoadDataGrid();
                }
               
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }
    }
}
