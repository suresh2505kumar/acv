﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Drawing.Imaging;
using System.Configuration;
using System.Data.SqlClient;

namespace Subcontract
{
    public partial class Frmparty : Form
    {
        public Frmparty()
        {
            CenterToParent();
            this.BackColor = Color.White;
            InitializeComponent();
        }
        string uid = "";
        int mode = 0;

        //SqlCommand cmd;
        SqlConnection conn = new SqlConnection("Data Source=" + Genclass.data2 + "; Initial Catalog=" + Genclass.data5 + ";User id=" + Genclass.data3 + ";Password=" + Genclass.data4 + "");

        //SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["connstr"].ConnectionString);
        SqlCommand qur = new SqlCommand();

        private void Frmparty_Load(object sender, EventArgs e)
        {
            qur.Connection = conn;
            
            Loadtype();
            //this.Size = new Size(SystemInformation.VirtualScreen.Width, SystemInformation.VirtualScreen.Height);
            //Left = (MdiParent.ClientRectangle.Width - Width) / 8;
            //Top = (MdiParent.ClientRectangle.Height - Height) / 8;
            Left = (MdiParent.ClientRectangle.Width - Width) / 3;
            Top = (MdiParent.ClientRectangle.Height - Height) / 3;
            //label1.Location = new Point(60,50);

            HFGP.RowHeadersVisible = false;
            HFG3.RowHeadersVisible = false;

            chkact.Checked = true;
            //Genpan.Location = new Point(50,70);
            //Editpnl.Location = new Point(50, 70);
            cbotype.Text = "Dealer";
            cbocr.Text = "Customer";
            fraitem.Visible = false;
            Editpnl.Visible = false;
          
        }
        public void Loadtype()
        {
            conn.Open();
            string qur = "select * from generalm where TypeM_Uid=3 and companyid=" + Genclass.data1 + "";
            SqlCommand cmd = new SqlCommand(qur, conn);
            SqlDataAdapter apt = new SqlDataAdapter(cmd);
            DataTable tab = new DataTable();
            apt.Fill(tab);
            cbotype.DataSource = null;
            cbotype.DataSource = tab;
            cbotype.DisplayMember = "Generalname";
            cbotype.ValueMember = "uid";
            cbotype.SelectedIndex = -1;
            conn.Close();
        }
        private void Loadgrid()
        {
            try
            {
                conn.Open();

                Genclass.StrSrch = "";


                Genclass.FSSQLSortStr = "Name";
                Genclass.FSSQLSortStr1 = "contactp";
                Genclass.FSSQLSortStr2 = "phone";
       


                if (txtscr1.Text != "")
                {
                    if (Genclass.StrSrch == "")
                    {
                        Genclass.StrSrch = Genclass.FSSQLSortStr + " like '%" + txtscr1.Text + "%'";
                    }
                    else
                    {
                        Genclass.StrSrch = Genclass.StrSrch + " and " + Genclass.FSSQLSortStr + " like '%" + txtscr1.Text + "%'";
                    }

                }

                if (Txtscr2.Text != "")
                {
                    if (Genclass.StrSrch == "")
                    {
                        Genclass.StrSrch = Genclass.FSSQLSortStr1 + " like '%" + Txtscr2.Text + "%'";
                    }
                    else
                    {
                        Genclass.StrSrch = Genclass.StrSrch + " and " + Genclass.FSSQLSortStr1 + " like '%" + Txtscr2.Text + "%'";
                    }

                }

                if (Txtscr3.Text != "")
                {
                    if (Genclass.StrSrch == "")
                    {
                        Genclass.StrSrch = Genclass.FSSQLSortStr2 + " like '%" + Txtscr3.Text + "%'";
                    }
                    else
                    {
                        Genclass.StrSrch = Genclass.StrSrch + " and " + Genclass.FSSQLSortStr2 + " like '%" + Txtscr3.Text + "%'";
                    }

                }



                if (txtscr1.Text != "")
                {
                    Genclass.StrSrch = " " + Genclass.StrSrch;
                }
                else if (Txtscr2.Text != "")
                {
                    Genclass.StrSrch = " " + Genclass.StrSrch;
                }
                else if (Txtscr3.Text != "")
                {
                    Genclass.StrSrch = " " + Genclass.StrSrch;
                }
                else
                {
                    Genclass.StrSrch = "uid <> 0";
                }

                if (chkact.Checked == true)
                {
                    string quy = "select Uid,Name,Contactp as ContactPerson,Phone,active from Partym where active=1 and companyid=" + Genclass.data1 + " and Ptype<>1 and " + Genclass.StrSrch + "";
                    Genclass.cmd = new SqlCommand(quy, conn);
                }
                else
                {
                    string quy = "select Uid,Name,Contactp as ContactPerson,Phone,active from Partym where active=0 and companyid=" + Genclass.data1 + " and Ptype<>1 and " + Genclass.StrSrch + "";
                    Genclass.cmd = new SqlCommand(quy, conn);
                }
           


                SqlDataAdapter aptr = new SqlDataAdapter(Genclass.cmd);
                DataTable tap = new DataTable();
                aptr.Fill(tap);

                this.HFGP.DefaultCellStyle.Font = new Font("Microsoft Sans Serif", 10);
                //dgv.ColumnHeadersDefaultCellStyle.Font = new Font(dgv.ColumnHeadersDefaultCellStyle.Font, FontStyle.Bold);
                this.HFGP.ColumnHeadersDefaultCellStyle.Font = new Font("Microsoft Sans Serif", 11, FontStyle.Bold);
                HFGP.AutoGenerateColumns = false;
                HFGP.Refresh();
                HFGP.DataSource = null;
                HFGP.Rows.Clear();


                HFGP.ColumnCount = tap.Columns.Count;
                Genclass.i = 0;
                foreach (DataColumn column in tap.Columns)
                {
                    HFGP.Columns[Genclass.i].Name = column.ColumnName;
                    HFGP.Columns[Genclass.i].HeaderText = column.ColumnName;
                    HFGP.Columns[Genclass.i].DataPropertyName = column.ColumnName;
                    Genclass.i = Genclass.i + 1;
                }



                HFGP.Columns[0].Visible = false;

                HFGP.Columns[1].Width = 400;
                HFGP.Columns[2].Width = 200;
                HFGP.Columns[3].Width = 200;
                HFGP.Columns[4].Visible = false;



                HFGP.DataSource = tap;

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return;
            }
            finally
            {
                conn.Close();
                Genclass.cmd.Dispose();
            }
        }

        private void btnadd_Click(object sender, EventArgs e)
        {
            uid = "";
            mode = 1;
            Genpan.Visible = false;
            fraitem.Visible = false;
            Editpnl.Visible = true;
            chkact.Checked = true;
            Chkedtact.Checked = true;
            Module.ClearTextBox(this, Editpnl);
            txtname.Focus();

      
        }

        private void txtstate_KeyDown(object sender, KeyEventArgs e)
        {
        }

        private void btnaddrcan_Click(object sender, EventArgs e)
        {
            Module.ClearTextBox(this,Editpnl);
            Genpan.Visible = true;
            Loadgrid();
        }

        private void btnsave_Click(object sender, EventArgs e)
        {
         
   
        }

        private void btnexit_Click(object sender, EventArgs e)
        {
            this.Dispose();
            //Editpnl.Visible = true;
        }

        private void txtscr1_TextChanged(object sender, EventArgs e)
        {
            Loadgrid();
        }

        private void Txtscr2_TextChanged(object sender, EventArgs e)
        {
            Loadgrid();
        }

        private void Txtscr3_TextChanged(object sender, EventArgs e)
        {
            Loadgrid();
        }

        private void btnedit_Click(object sender, EventArgs e)
        {
            mode = 2;
            fraitem.Visible = false;
            Genpan.Visible = false;
            Editpnl.Visible = true;
            int i = HFGP.SelectedCells[0].RowIndex;
            txtname.Text = HFGP.Rows[i].Cells[1].Value.ToString();
            Txtctper.Text = HFGP.Rows[i].Cells[2].Value.ToString();
            txtphone    .Text = HFGP.Rows[i].Cells[3].Value.ToString();
            uid = HFGP.Rows[i].Cells[0].Value.ToString();
            if (HFGP.Rows[i].Cells[4].Value.ToString() == "True")
            {
                Chkedtact.Checked = true;
            }
            else
            {
                Chkedtact.Checked = false;
            }
            conn.Open();


            Genclass.strsql = "select a.*,b.generalname as state from partym a inner join generalm b on a.stateuid=b.uid where a.uid=" + uid + "";
           Genclass.cmd = new SqlCommand(Genclass.strsql, conn);
            SqlDataAdapter aptr = new SqlDataAdapter(Genclass.cmd);
            DataTable tap = new DataTable();
            aptr.Fill(tap);

            txtcode.Text = tap.Rows[0]["Code"].ToString();
            txtadd1.Text = tap.Rows[0]["address1"].ToString();
            txtadd2.Text = tap.Rows[0]["address2"].ToString();
            txtcity.Text = tap.Rows[0]["City"].ToString();
            txtstate.Text = tap.Rows[0]["state"].ToString();
            Txtstid.Text = tap.Rows[0]["stateuid"].ToString();
            txtpin.Text = tap.Rows[0]["pin"].ToString();
            txtcereg.Text = tap.Rows[0]["CERegno"].ToString();
            txtcrange.Text = tap.Rows[0]["CERange"].ToString();
            txtecc.Text = tap.Rows[0]["ECCNo"].ToString();
            txtdiv.Text = tap.Rows[0]["Division"].ToString();
            txtcoll.Text = tap.Rows[0]["Collectorate"].ToString();
            tngno.Text = tap.Rows[0]["TNGST"].ToString();
            txtcst.Text = tap.Rows[0]["CST"].ToString();
            txtcrdlmt.Text = tap.Rows[0]["CreditLimit"].ToString();
            txtcrddays.Text = tap.Rows[0]["CreditDays"].ToString();
            cbotype.Text = tap.Rows[0]["PartyType"].ToString();
            txtmail.Text = tap.Rows[0]["Emailid"].ToString();
            cbocr.Text = tap.Rows[0]["Type"].ToString();
      
          
          

            conn.Close();

        }

        private void Genpan_Paint(object sender, PaintEventArgs e)
        {

        }

        private void CmdItem1_Click(object sender, EventArgs e)
        {
           

        }
    

        private void cmdcancel_Click(object sender, EventArgs e)
        {
            fraitem.Visible = false;

        }

        private void btnsave_Click_1(object sender, EventArgs e)
        {
            if (txtname.Text == "")
            {
                MessageBox.Show("Enter the Name");
                txtname.Focus();
                return;
            }
            if (txtstate.Text == "")
            {
                MessageBox.Show("Select the State");
                txtstate.Focus();
                return;
            }
            if (cbotype.Text == "")
            {
                MessageBox.Show("Enter the Party Type");
                cbotype.Focus();
                return;
            }

            conn.Open();
            //if (mode == 1)
            //{
            //    string quy = "select  Code from partym where Code='" + txtcode.Text + "'";
            //    Genclass.cmd = new SqlCommand(quy, conn);

            //    SqlDataAdapter aptr = new SqlDataAdapter(Genclass.cmd);
            //    DataTable tap = new DataTable();
            //    aptr.Fill(tap);
            //    if (tap.Rows.Count > 0)
            //    {
            //        MessageBox.Show("Record Already Exist");
            //        txtcode.Text = "";
            //        txtcode.Focus();
            //        conn.Close();
            //        return;
            //    }

            //    string quy1 = "select  Name from partym where Name='" + txtname.Text + "'";
            //    Genclass.cmd = new SqlCommand(quy1, conn);

            //    SqlDataAdapter aptr1 = new SqlDataAdapter(Genclass.cmd);
            //    DataTable tap1 = new DataTable();
            //    aptr1.Fill(tap1);
            //    if (tap.Rows.Count > 0)
            //    {
            //        MessageBox.Show("Record Already Exist");
            //        txtname.Text = "";
            //        txtname.Focus();
            //        conn.Close();
            //        return;
            //    }

            qur.CommandText = "exec sp_party '" + txtname.Text + "','" + txtcode.Text + "','" + cbotype.Text + "','" + txtadd1.Text + "','" + txtadd2.Text + "','" + txtcity.Text + "'," + Txtstid.Text + ",'" + txtpin.Text + "','" + txtcereg.Text + "','" + txtcrange.Text + "','" + txtecc.Text + "','" + txtdiv.Text + "','" + txtcoll.Text + "','" + tngno.Text + "','" + txtcst.Text + "'," + txtcrddays.Text + "," + txtcrdlmt.Text + "," + Chkedtact.Checked + ",'" + txtphone.Text + "','" + txtmail.Text + "','" + cbocr.Text + "','" + Txtctper.Text + "',0,'" + uid + "'," + mode + "," + Genclass.data1 + "";

                //qur.CommandText = "insert into partym values ('" + txtname.Text + "','" + txtcode.Text + "','" + cbotype.Text + "','" + txtadd1.Text + "','" + txtadd2.Text + "','" + txtcity.Text + "'," + Txtstid.Text + ",'" + txtpin.Text + "','" + txtcereg.Text + "','" + txtcrange.Text + "','" + txtecc.Text + "','" + txtdiv.Text + "','" + txtcoll.Text + "','" + tngno.Text + "','" + txtcst.Text + "'," + txtcrddays.Text + "," + txtcrdlmt.Text + ",1,'" + txtphone.Text + "','" + txtmail.Text + "','" + cbocr.Text + "','" + Txtctper.Text + "',0)";
                qur.ExecuteNonQuery();
                MessageBox.Show("Record has been saved", "Save", MessageBoxButtons.OK);
            //}

            //if (mode == 2)
            //{

            //    qur.CommandText = "Update partym set Name='" + txtname.Text + "',Code='" + txtcode.Text + "',PartyType='" + cbotype.Text + "',Address1='" + txtadd1.Text + "',Address2='" + txtadd2.Text + "',City='" + txtcity.Text + "',StateUid=" + Txtstid.Text + ",Pin=" + txtpin.Text + ",CERegno='" + txtcereg.Text + "',CERange='" + txtcrange.Text + "',ECCNo='" + txtecc.Text + "',Division='" + txtdiv.Text + "',Collectorate='" + txtcoll.Text + "',TNGST='" + tngno.Text + "',CST='" + txtcst.Text + "',CreditLimit=" + txtcrdlmt.Text + ",CreditDays=" + txtcrddays.Text + ",Phone='" + txtphone.Text + "',Emailid='" + txtmail.Text + "',Type='" + cbocr.Text + "',ContactP='" + Txtctper.Text + "' where uid=" + uid + "";

            //    qur.ExecuteNonQuery();

            //    MessageBox.Show("Record has been Updated", "Save", MessageBoxButtons.OK);
            //}



            conn.Close();
            Loadgrid();
            Genpan.Visible = true;
        }

        private void btnaddrcan_Click_1(object sender, EventArgs e)
        {

            fraitem.Visible = false;
            Editpnl.Visible = false;
            Genpan.Visible = true;
            //Module.ClearTextBox(this, Editpnl);

            Loadgrid();
        }

        private void CmdItem1_Click_1(object sender, EventArgs e)
        {
            if (mode == 1)
            {
                return;
            }

            else
            {
                fraitem.Visible = true;
                conn.Open();
                string quy = "select a.Uid,b.itemname from Partym  A  INNER JOIN itemm b on a.uid=b.partyuid where a.active=1 ";
                Genclass.cmd = new SqlCommand(quy, conn);



                SqlDataAdapter aptr = new SqlDataAdapter(Genclass.cmd);
                DataTable tap = new DataTable();
                aptr.Fill(tap);

                this.HFG3.DefaultCellStyle.Font = new Font("Arial", 10);
                //dgv.ColumnHeadersDefaultCellStyle.Font = new Font(dgv.ColumnHeadersDefaultCellStyle.Font, FontStyle.Bold);
                this.HFG3.ColumnHeadersDefaultCellStyle.Font = new Font("Arial", 10, FontStyle.Bold);
                HFG3.AutoGenerateColumns = false;
                HFG3.Refresh();
                HFG3.DataSource = null;
                HFG3.Rows.Clear();


                HFG3.ColumnCount = tap.Columns.Count;
                Genclass.i = 0;
                foreach (DataColumn column in tap.Columns)
                {
                    HFG3.Columns[Genclass.i].Name = column.ColumnName;
                    HFG3.Columns[Genclass.i].HeaderText = column.ColumnName;
                    HFG3.Columns[Genclass.i].DataPropertyName = column.ColumnName;
                    Genclass.i = Genclass.i + 1;
                }



                HFG3.Columns[0].Visible = false;

                HFG3.Columns[1].Width = 400;





                conn.Close();
                HFG3.DataSource = tap;

            }
        }

        private void label6_Click(object sender, EventArgs e)
        {

        }

        private void label4_Click(object sender, EventArgs e)
        {

        }

        private void label7_Click(object sender, EventArgs e)
        {

        }

        private void label8_Click(object sender, EventArgs e)
        {

        }

        private void label10_Click(object sender, EventArgs e)
        {

        }

        private void Editpnl_Paint(object sender, PaintEventArgs e)
        {

        }

        private void txtstate_KeyDown_1(object sender, KeyEventArgs e)
        {

            if (e.KeyCode == Keys.Enter)
            {
                conn.Open();

                Module.Partylistviewcont("uid", "Generalname", Genclass.strsql, this, Txtstid, txtstate, Editpnl);
                Genclass.strsql = "select uid,Generalname as State from Generalm where active=1 and TypeM_Uid=2";
                Genclass.FSSQLSortStr = "Generalname";
                Genclass.cmd = new SqlCommand(Genclass.strsql, conn);
                SqlDataAdapter aptr = new SqlDataAdapter(Genclass.cmd);
                DataTable tap = new DataTable();
                aptr.Fill(tap);
                Frmlookup contc = new Frmlookup();
                DataGridView dt = (DataGridView)contc.Controls["HFGP"];
                dt.Refresh();
                dt.ColumnCount = tap.Columns.Count;
                dt.Columns[0].Visible = false;
                dt.Columns[1].Width = 200;


                dt.DefaultCellStyle.Font = new Font("Arial", 10);

                dt.ColumnHeadersDefaultCellStyle.Font = new Font("Arial", 10, FontStyle.Bold);
                dt.AutoGenerateColumns = false;

                Genclass.i = 0;
                foreach (DataColumn column in tap.Columns)
                {
                    dt.Columns[Genclass.i].Name = column.ColumnName;
                    dt.Columns[Genclass.i].HeaderText = column.ColumnName;
                    dt.Columns[Genclass.i].DataPropertyName = column.ColumnName;
                    Genclass.i = Genclass.i + 1;
                }

                dt.DataSource = tap;
                contc.Show();
                conn.Close();
            }
        }

        private void btndelete_Click(object sender, EventArgs e)
        {
            conn.Open();

            int i = HFGP.SelectedCells[0].RowIndex;
         
            uid = HFGP.Rows[i].Cells[0].Value.ToString();

            mode = 3;
            qur.CommandText = "";

            qur.CommandText = "exec sp_party '" + txtname.Text + "','" + txtcode.Text + "','" + cbotype.Text + "','" + txtadd1.Text + "','" + txtadd2.Text + "','" + txtcity.Text + "'," + Txtstid.Text + ",'" + txtpin.Text + "','" + txtcereg.Text + "','" + txtcrange.Text + "','" + txtecc.Text + "','" + txtdiv.Text + "','" + txtcoll.Text + "','" + tngno.Text + "','" + txtcst.Text + "'," + txtcrddays.Text + "," + txtcrdlmt.Text + ",1,'" + txtphone.Text + "','" + txtmail.Text + "','" + cbocr.Text + "','" + Txtctper.Text + "',0,'" + uid + "'," + mode + "," + Genclass.data1 + "";
            qur.ExecuteNonQuery();


            string quy = "select * from Msgdes";
            Genclass.cmd = new SqlCommand(quy, conn);

            SqlDataAdapter aptr = new SqlDataAdapter(Genclass.cmd);
            DataTable tap = new DataTable();
            aptr.Fill(tap);

            MessageBox.Show(tap.Rows[0]["Msg"].ToString(), "Save", MessageBoxButtons.OK);
        }

        private void HFG3_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void chkact_CheckedChanged(object sender, EventArgs e)
        {
            Loadgrid();
        }

        private void txtstate_TextChanged(object sender, EventArgs e)
        {

        }

    }

      
}
