﻿namespace Subcontract
{
    partial class FrmMix
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.HFG3 = new System.Windows.Forms.DataGridView();
            this.txtrubber = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.txtdcodate = new System.Windows.Forms.DateTimePicker();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.txtmix = new System.Windows.Forms.TextBox();
            this.txtche = new System.Windows.Forms.TextBox();
            this.txtrb = new System.Windows.Forms.TextBox();
            this.txtwt = new System.Windows.Forms.TextBox();
            this.txtopname = new System.Windows.Forms.TextBox();
            this.fromdt = new System.Windows.Forms.DateTimePicker();
            this.todt = new System.Windows.Forms.DateTimePicker();
            this.cmdok = new System.Windows.Forms.Button();
            this.txtrubberid = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.HFG3)).BeginInit();
            this.SuspendLayout();
            // 
            // HFG3
            // 
            this.HFG3.BackgroundColor = System.Drawing.Color.White;
            this.HFG3.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.HFG3.Location = new System.Drawing.Point(25, 160);
            this.HFG3.Name = "HFG3";
            this.HFG3.Size = new System.Drawing.Size(842, 336);
            this.HFG3.TabIndex = 5;
            // 
            // txtrubber
            // 
            this.txtrubber.Location = new System.Drawing.Point(121, 133);
            this.txtrubber.Margin = new System.Windows.Forms.Padding(4);
            this.txtrubber.Name = "txtrubber";
            this.txtrubber.Size = new System.Drawing.Size(119, 20);
            this.txtrubber.TabIndex = 4;
            this.txtrubber.TextChanged += new System.EventHandler(this.txtrubber_TextChanged);
            this.txtrubber.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtrubber_KeyDown);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Bookman Old Style", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(115, 60);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(62, 14);
            this.label1.TabIndex = 6;
            this.label1.Text = "DocDate";
            // 
            // txtdcodate
            // 
            this.txtdcodate.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.txtdcodate.Location = new System.Drawing.Point(118, 77);
            this.txtdcodate.Name = "txtdcodate";
            this.txtdcodate.Size = new System.Drawing.Size(97, 20);
            this.txtdcodate.TabIndex = 157;
            this.txtdcodate.Value = new System.DateTime(2017, 3, 25, 16, 16, 31, 0);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Bookman Old Style", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(240, 60);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(41, 14);
            this.label2.TabIndex = 158;
            this.label2.Text = "From";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Bookman Old Style", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(349, 60);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(23, 14);
            this.label3.TabIndex = 159;
            this.label3.Text = "To";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Bookman Old Style", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(118, 115);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(131, 14);
            this.label4.TabIndex = 160;
            this.label4.Text = "Rubber  Compound";
            this.label4.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.label4.Click += new System.EventHandler(this.label4_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Bookman Old Style", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(268, 115);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(53, 14);
            this.label5.TabIndex = 161;
            this.label5.Text = "Mixing";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Bookman Old Style", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(337, 115);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(115, 14);
            this.label6.TabIndex = 162;
            this.label6.Text = "Chemical Wt(Kg)";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Bookman Old Style", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(458, 115);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(108, 14);
            this.label7.TabIndex = 163;
            this.label7.Text = "Rubber  Wt (Kg)";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Bookman Old Style", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(572, 115);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(109, 14);
            this.label8.TabIndex = 164;
            this.label8.Text = "Weight loss(Kg)";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Bookman Old Style", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(687, 115);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(120, 14);
            this.label9.TabIndex = 165;
            this.label9.Text = "Operatios\'s Name";
            // 
            // txtmix
            // 
            this.txtmix.Location = new System.Drawing.Point(260, 133);
            this.txtmix.Margin = new System.Windows.Forms.Padding(4);
            this.txtmix.Name = "txtmix";
            this.txtmix.Size = new System.Drawing.Size(63, 20);
            this.txtmix.TabIndex = 166;
            // 
            // txtche
            // 
            this.txtche.Location = new System.Drawing.Point(340, 133);
            this.txtche.Margin = new System.Windows.Forms.Padding(4);
            this.txtche.Name = "txtche";
            this.txtche.Size = new System.Drawing.Size(105, 20);
            this.txtche.TabIndex = 167;
            // 
            // txtrb
            // 
            this.txtrb.Location = new System.Drawing.Point(461, 133);
            this.txtrb.Margin = new System.Windows.Forms.Padding(4);
            this.txtrb.Name = "txtrb";
            this.txtrb.Size = new System.Drawing.Size(102, 20);
            this.txtrb.TabIndex = 168;
            this.txtrb.TextChanged += new System.EventHandler(this.txtrb_TextChanged);
            // 
            // txtwt
            // 
            this.txtwt.Location = new System.Drawing.Point(575, 133);
            this.txtwt.Margin = new System.Windows.Forms.Padding(4);
            this.txtwt.Name = "txtwt";
            this.txtwt.Size = new System.Drawing.Size(93, 20);
            this.txtwt.TabIndex = 169;
            // 
            // txtopname
            // 
            this.txtopname.Location = new System.Drawing.Point(690, 133);
            this.txtopname.Margin = new System.Windows.Forms.Padding(4);
            this.txtopname.Name = "txtopname";
            this.txtopname.Size = new System.Drawing.Size(108, 20);
            this.txtopname.TabIndex = 170;
            // 
            // fromdt
            // 
            this.fromdt.Format = System.Windows.Forms.DateTimePickerFormat.Time;
            this.fromdt.Location = new System.Drawing.Point(243, 77);
            this.fromdt.Name = "fromdt";
            this.fromdt.Size = new System.Drawing.Size(80, 20);
            this.fromdt.TabIndex = 171;
            // 
            // todt
            // 
            this.todt.Format = System.Windows.Forms.DateTimePickerFormat.Time;
            this.todt.Location = new System.Drawing.Point(329, 77);
            this.todt.Name = "todt";
            this.todt.Size = new System.Drawing.Size(80, 20);
            this.todt.TabIndex = 172;
            // 
            // cmdok
            // 
            this.cmdok.Location = new System.Drawing.Point(823, 131);
            this.cmdok.Name = "cmdok";
            this.cmdok.Size = new System.Drawing.Size(43, 21);
            this.cmdok.TabIndex = 173;
            this.cmdok.Text = "OK";
            this.cmdok.UseVisualStyleBackColor = true;
            this.cmdok.Click += new System.EventHandler(this.cmdok_Click);
            // 
            // txtrubberid
            // 
            this.txtrubberid.Location = new System.Drawing.Point(158, 133);
            this.txtrubberid.Margin = new System.Windows.Forms.Padding(4);
            this.txtrubberid.Name = "txtrubberid";
            this.txtrubberid.Size = new System.Drawing.Size(42, 20);
            this.txtrubberid.TabIndex = 174;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Bookman Old Style", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(22, 19);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(132, 18);
            this.label10.TabIndex = 175;
            this.label10.Text = "Mixing Register";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(823, 60);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(43, 21);
            this.button1.TabIndex = 176;
            this.button1.Text = "Exit";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // FrmMix
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(915, 553);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.cmdok);
            this.Controls.Add(this.todt);
            this.Controls.Add(this.fromdt);
            this.Controls.Add(this.txtopname);
            this.Controls.Add(this.txtwt);
            this.Controls.Add(this.txtrb);
            this.Controls.Add(this.txtche);
            this.Controls.Add(this.txtmix);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txtdcodate);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.HFG3);
            this.Controls.Add(this.txtrubber);
            this.Controls.Add(this.txtrubberid);
            this.Name = "FrmMix";
            this.Text = "FrmMix";
            this.Load += new System.EventHandler(this.FrmMix_Load);
            ((System.ComponentModel.ISupportInitialize)(this.HFG3)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView HFG3;
        private System.Windows.Forms.TextBox txtrubber;
        private System.Windows.Forms.Label label1;
        internal System.Windows.Forms.DateTimePicker txtdcodate;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox txtmix;
        private System.Windows.Forms.TextBox txtche;
        private System.Windows.Forms.TextBox txtrb;
        private System.Windows.Forms.TextBox txtwt;
        private System.Windows.Forms.TextBox txtopname;
        private System.Windows.Forms.DateTimePicker fromdt;
        private System.Windows.Forms.DateTimePicker todt;
        private System.Windows.Forms.Button cmdok;
        private System.Windows.Forms.TextBox txtrubberid;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Button button1;
    }
}