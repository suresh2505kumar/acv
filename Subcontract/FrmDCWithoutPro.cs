﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Drawing.Imaging;
using System.Configuration;
using System.Data.SqlClient;
using System.Security.Cryptography;
using excel = Microsoft.Office.Interop.Excel;
using CrystalDecisions.Shared;
using CrystalDecisions.CrystalReports.Engine;


namespace ACV
{
    public partial class FrmDCWithoutPro : Form
    {
        public FrmDCWithoutPro()
        {
            CenterToParent();
            this.BackColor = Color.White;
            InitializeComponent();
        }
        string uid = "";
        int mode = 0;

        //SqlCommand cmd;
        //SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["connstr"].ConnectionString);
        SqlConnection conn = new SqlConnection("Data Source=" + Genclass.data2 + "; Initial Catalog=" + Genclass.data5 + ";User id=" + Genclass.data3 + ";Password=" + Genclass.data4 + "");
        SqlCommand qur = new SqlCommand();
        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void HFIT_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void FrmDCWithoutPro_Load(object sender, EventArgs e)
        {

            qur.Connection = conn;
            Left = (MdiParent.ClientRectangle.Width - Width) / 3;
            Top = (MdiParent.ClientRectangle.Height - Height) / 3;
           

            this.HFGP.DefaultCellStyle.Font = new Font("Arial", 10);
            this.HFGP.ColumnHeadersDefaultCellStyle.Font = new Font("Arial", 10, FontStyle.Bold);
            this.HFIT.DefaultCellStyle.Font = new Font("Arial", 10);
            this.HFIT.ColumnHeadersDefaultCellStyle.Font = new Font("Arial", 10, FontStyle.Bold);

            HFGP.RowHeadersVisible = false;

            HFIT.RowHeadersVisible = false;


            //DTPDOCDT = DateTime.Now;
            Genpan.Visible = true;
            Editpan.Visible = false;
            if (Genclass.Dtype == 20)
            {
                label1.Text = "DCWithoutProcess";
                txtnar.Width = 463;
                label11.Visible = true;
                label3.Text = "Party";
                label9.Visible = false;
                txtcusid.Visible = false;
                txtcusorderno.Visible = false;
            }
            else if (Genclass.Dtype == 30)
            {
                label1.Text = "DCProcess";
                txtnar.Width = 463;
                label11.Visible = true;
                label3.Text = "Party";
                label9.Visible = true;
                label9.Visible = true;
                txtcusid.Visible = true;
                txtcusorderno.Visible = true;
            }
            else if (Genclass.Dtype == 60)
            {
                label1.Text = "Stock In";
                txtnar.Width = 550;
                txtname.Width = 674;
                txtcusorderno.Visible = false;
                label9.Visible = false;
                label11.Visible = false;
                label3.Text = "Naration";
            }
            else if (Genclass.Dtype == 70)
            {
                label1.Text = "Stock Out";
                txtnar.Width = 550;
                txtname.Width = 674;
                txtcusorderno.Visible = false;
                label9.Visible = false;
                label11.Visible = false;
                label3.Text = "Naration";

            }

            Titlep();
            chkact.Checked = true;
            chkedtact.Checked = true;
            Genclass.Module.buttonstyleform(this);
            Genclass.Module.buttonstylepanel(panadd);
            panadd.Visible = true;
            HFGP.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            HFGP.EnableHeadersVisualStyles = false;
            HFGP.ColumnHeadersDefaultCellStyle.BackColor = SystemColors.GradientActiveCaption;
        }

        private void Titlep()
        {

            //DataTable dt = new DataTable();
            //dt.Columns.Add("Item");
            //dt.Columns.Add("Qty");
            //dt.Columns.Add("itid");

            if (Genclass.Dtype == 20)
            {
                HFIT.ColumnCount = 5;
                HFIT.Columns[0].Name = "Itemname";
                HFIT.Columns[1].Name = "Qty";

                HFIT.Columns[2].Name = "itemid";
                HFIT.Columns[3].Name = "Refid";

                HFIT.Columns[0].Width = 550;
                HFIT.Columns[1].Width = 90;

                HFIT.Columns[2].Visible = false;
                HFIT.Columns[4].Name = "Grnno";
                HFIT.Columns[4].Width = 120;
                HFIT.Columns[3].Visible = false;
            }
            else if (Genclass.Dtype == 30)
            {
                HFIT.ColumnCount = 5;
                HFIT.Columns[0].Name = "Itemname";
                HFIT.Columns[1].Name = "Qty";

                HFIT.Columns[2].Name = "itemid";
                HFIT.Columns[3].Name = "Refid";

                HFIT.Columns[0].Width = 550;
                HFIT.Columns[1].Width = 90;

                HFIT.Columns[2].Visible = false;
                HFIT.Columns[4].Name = "PRno";
                HFIT.Columns[4].Width = 120;
                HFIT.Columns[3].Visible = false;
            }
            else if (Genclass.Dtype == 60 || Genclass.Dtype == 70)
            {
                HFIT.ColumnCount = 3;
                HFIT.Columns[0].Name = "Itemname";
                HFIT.Columns[1].Name = "Qty";
                HFIT.Columns[0].Width = 550;
                HFIT.Columns[1].Width = 90;
                HFIT.Columns[2].Name = "itemid";
                HFIT.Columns[2].Visible = false;
            }

            //HFIT.DataSource = dt.DefaultView.Table;

        }

        private void textBox1_TextChanged_1(object sender, EventArgs e)
        {

        }

        private void txtgrnno_KeyDown(object sender, KeyEventArgs e)
        {


        }
        private void loadput()
        {
            conn.Open();

            Genclass.fieldone = "";
            Genclass.fieldtwo = "";
            Genclass.fieldthree = "";
            Genclass.fieldFour = "";
            Genclass.fieldFive = "";

            if (Genclass.type == 1)
            {
                Genclass.Module.Partylistviewcont("uid", "Name", Genclass.strsql, this, txtpuid, txtname, Editpan);
                //Genclass.strsql = "select uid,Name as Party from Partym where active=1 and  companyid=" + Genclass.data1 + "";
                Genclass.strsql = "select distinct  c.uid,c.name from transactionsp a inner join transactionsplist b on  a.uid=b.transactionspuid and a.doctypeid=10 and a.companyid=" + Genclass.data1 + " inner join partym c on a.partyuid=c.uid left join transactionsplist d on  b.uid=d.refuid  and b.itemuid=d.itemuid and d.doctypeid=20  left join transactionsplist e on  b.uid=e.refuid and e.doctypeid=30 group by c.uid,c.name,b.pqty  having b.pqty-isnull(sum(d.pqty),0)-isnull(sum(e.pqty),0) >0";

                Genclass.FSSQLSortStr = "Name";

            }
            else if (Genclass.type == 3)
            {
                Genclass.Module.Partylistviewcont2("uid", "Docno", "Qty", "Qty", Genclass.strsql, this, txtgrnid, txtgrnno, txtfqty, txtqty, Editpan);
                //Genclass.strsql = "select distinct a.uid,a.docno as grnno from transactionsp a inner join  transactionsplist b on  a.uid=b.transactionspuid and a.doctypeid=10 inner join partym c on a.partyuid=c.uid left join   transactionsplist d on  b.uid=d.refuid  and b.itemuid=d.itemuid and d.doctypeid=20  left join  transactionsplist   f on  b.uid=f.refuid   and f.doctypeid=30  left join itemsmbom g on b.itemuid=g.itemm_uid inner join itemm e on g.useditem_uid=e.uid    where a.uid=1 group by a.uid,a.docno,b.pqty having b.pqty-isnull(sum(d.pqty),0)-isnull(sum(f.pqty),0) >0 ";
                Genclass.strsql = "select uid,docno,isnull(sum(a.dcqty),0)-isnull(sum(b.PQty),0) as qty,isnull(sum(a.dcqty),0)-isnull(sum(b.PQty),0) as qty from (select distinct a.uid,a.docno,b.DCQty,c.Uid as puid,c.name from transactionsp a inner join  transactionsplist b on  a.uid=b.transactionspuid and a.doctypeid=50  inner join partym c  on a.partyuid=c.uid inner join itemm d on a.adddutyuid=d.uid where c.uid=" + txtpuid.Text + " and d.uid=" + txtnrid.Text + ") a left join (select refuid,isnull(PQty,0) as pqty from transactionsplist) b on a.uid=b.Refuid group by uid,docno";
                Genclass.FSSQLSortStr = "docno";
            }
            else if (Genclass.type == 2)
            {
                Genclass.Module.Partylistviewcont2("uid", "Docno", "Qty", "Qty", Genclass.strsql, this, txtgrnid, txtgrnno, txtfqty, txtqty, Editpan);
                Genclass.strsql = "select distinct  b.uid,a.docno as grnno,b.pqty-isnull(sum(d.pqty),0)-isnull(sum(e.pqty),0) as qty,b.pqty-isnull(sum(d.pqty),0)-isnull(sum(e.pqty),0) from transactionsp a inner join transactionsplist b on  a.uid=b.transactionspuid and a.doctypeid=10 and a.companyid=" + Genclass.data1 + " inner join partym c on a.partyuid=c.uid left join transactionsplist d on  b.uid=d.refuid  and b.itemuid=d.itemuid and d.doctypeid=20  left join transactionsplist e on  b.uid=e.refuid and e.doctypeid=50 where a.partyuid=" + txtpuid.Text + " and b.itemuid=" + txtnrid.Text + " group by b.uid,a.docno,b.pqty having b.pqty-isnull(sum(d.pqty),0)-isnull(sum(e.pqty),0) >0";
                Genclass.FSSQLSortStr = "docno";
            }
            else if (Genclass.type == 4)
            {
                Genclass.Module.Partylistviewcont("uid", "Item", Genclass.strsql, this, txtnrid, txtnar, Editpan);
                Genclass.strsql = "select distinct  Z.uid,Z.itemname from transactionsp a inner join transactionsplist b on  a.uid=b.transactionspuid and a.doctypeid=10 and a.companyid=" + Genclass.data1 + " inner join itemm z on b.itemuid=z.uid inner join partym c on a.partyuid=c.uid left join transactionsplist d on  b.uid=d.refuid  and b.itemuid=d.itemuid and d.doctypeid=20  left join transactionsplist e on  b.uid=e.refuid and e.doctypeid=30 where a.partyuid=" + txtpuid.Text + " group by Z.uid,Z.itemname,b.pqty having b.pqty-isnull(sum(d.pqty),0)-isnull(sum(e.pqty),0) >0";

                Genclass.FSSQLSortStr = "itemname";

            }
            else if (Genclass.type == 5)
            {
                Genclass.Module.Partylistviewcont("uid", "Item", Genclass.strsql, this, txtpuid, txtname, Editpan);
                Genclass.strsql = " select distinct uid,name from (select  c.Uid,c.name from transactionsp a inner join  transactionsplist b on  a.uid=b.transactionspuid and a.doctypeid=50  inner join partym c  on a.partyuid=c.uid inner join itemm d on a.adddutyuid=d.uid) a left join (select refuid,isnull(PQty,0) as pqty from transactionsplist) b  on a.uid=b.Refuid";

                Genclass.FSSQLSortStr = "itemname";
            }
            else if (Genclass.type == 6)
            {
                Genclass.Module.Partylistviewcont("uid", "Item", Genclass.strsql, this, txtnrid, txtnar, Editpan);
                Genclass.strsql = " select distinct uid,itemname from (select  d.Uid,d.itemname  from transactionsp a inner join  transactionsplist b on  a.uid=b.transactionspuid and a.doctypeid=50  inner join partym c  on a.partyuid=c.uid inner join itemm d on a.adddutyuid=d.uid) a left join (select refuid,isnull(PQty,0) as pqty from transactionsplist) b  on a.uid=b.Refuid";

                Genclass.FSSQLSortStr = "itemname";
            }
            else if (Genclass.type == 7)
            {
                Genclass.Module.Partylistviewcont("uid", "Item", Genclass.strsql, this, txtnrid, txtnar, Editpan);
                Genclass.strsql = " select uid,itemname from itemm where active=1 and companyid=" + Genclass.data1 + "";

                Genclass.FSSQLSortStr = "itemname";
            }
            else if (Genclass.type == 8)
            {
                Genclass.Module.Partylistviewcont("uid", "docno", Genclass.strsql, this, txtcusid, txtcusorderno, Editpan);
                Genclass.strsql = "select a.uid, a.docno  from stransactionsp a inner join stransactionsplist b on  a.uid=b.transactionspuid  and a.doctypeid=80 and a.companyid=" + Genclass.data1 + " inner join partym c on a.partyuid=c.uid   left join transactionsplist e on     b.uid=e.refuid and e.doctypeid=30 group by  a.uid, a.docno, b.pqty  having     b.pqty-isnull(sum(e.pqty),0) >0";

                Genclass.FSSQLSortStr = "docno";
            }

            Genclass.cmd = new SqlCommand(Genclass.strsql, conn);
            SqlDataAdapter aptr = new SqlDataAdapter(Genclass.cmd);
            DataTable tap = new DataTable();
            aptr.Fill(tap);
            Frmlookup contc = new Frmlookup();
            DataGridView dt = (DataGridView)contc.Controls["HFGP"];
            dt.Refresh();
            dt.ColumnCount = tap.Columns.Count;
            dt.Columns[0].Visible = false;
            dt.Columns[1].Width = 600;

            if (Genclass.type == 2 || Genclass.type == 3)
            {
                dt.Columns[2].Width = 100;
                dt.Columns[3].Visible = false;
            }


            dt.DefaultCellStyle.Font = new Font("Arial", 10);

            dt.ColumnHeadersDefaultCellStyle.Font = new Font("Arial", 10, FontStyle.Bold);
            dt.AutoGenerateColumns = false;

            Genclass.i = 0;
            foreach (DataColumn column in tap.Columns)
            {
                dt.Columns[Genclass.i].Name = column.ColumnName;
                dt.Columns[Genclass.i].HeaderText = column.ColumnName;
                dt.Columns[Genclass.i].DataPropertyName = column.ColumnName;
                Genclass.i = Genclass.i + 1;
            }

            dt.DataSource = tap;
            contc.Show();
            conn.Close();


        }
        private void Loadgrid()
        {
            try
            {
                conn.Open();

                Genclass.StrSrch = "";


                Genclass.FSSQLSortStr = "Docno";
                Genclass.FSSQLSortStr1 = "Docdate";
                Genclass.FSSQLSortStr2 = "Dcno";
                Genclass.FSSQLSortStr3 = "Dcdate";

                if (Genclass.Dtype == 60 || Genclass.Dtype == 70)
                {
                    Genclass.FSSQLSortStr4 = "narration";
                }
                else
                {
                    Genclass.FSSQLSortStr4 = "Name";
                }




                if (txtscr1.Text != "")
                {
                    if (Genclass.StrSrch == "")
                    {
                        Genclass.StrSrch = Genclass.FSSQLSortStr + " like '%" + txtscr1.Text + "%'";
                    }
                    else
                    {
                        Genclass.StrSrch = Genclass.StrSrch + " and " + Genclass.FSSQLSortStr + " like '%" + txtscr1.Text + "%'";
                    }

                }

                if (Txtscr2.Text != "")
                {
                    if (Genclass.StrSrch == "")
                    {
                        Genclass.StrSrch = Genclass.FSSQLSortStr1 + " like '%" + Txtscr2.Text + "%'";
                    }
                    else
                    {
                        Genclass.StrSrch = Genclass.StrSrch + " and " + Genclass.FSSQLSortStr1 + " like '%" + Txtscr2.Text + "%'";
                    }

                }

                if (Txtscr3.Text != "")
                {
                    if (Genclass.StrSrch == "")
                    {
                        Genclass.StrSrch = Genclass.FSSQLSortStr2 + " like '%" + Txtscr3.Text + "%'";
                    }
                    else
                    {
                        Genclass.StrSrch = Genclass.StrSrch + " and " + Genclass.FSSQLSortStr2 + " like '%" + Txtscr3.Text + "%'";
                    }

                }

                if (txtscr4.Text != "")
                {
                    if (Genclass.StrSrch == "")
                    {
                        Genclass.StrSrch = Genclass.FSSQLSortStr3 + " like '%" + txtscr4.Text + "%'";
                    }
                    else
                    {
                        Genclass.StrSrch = Genclass.StrSrch + " and " + Genclass.FSSQLSortStr3 + " like '%" + txtscr4.Text + "%'";
                    }

                }

                if (txtscr5.Text != "")
                {
                    if (Genclass.StrSrch == "")
                    {
                        Genclass.StrSrch = Genclass.FSSQLSortStr4 + " like '%" + txtscr5.Text + "%'";
                    }
                    else
                    {
                        Genclass.StrSrch = Genclass.StrSrch + " and " + Genclass.FSSQLSortStr4 + " like '%" + txtscr5.Text + "%'";
                    }

                }





                if (txtscr1.Text != "")
                {
                    Genclass.StrSrch = " " + Genclass.StrSrch;
                }
                else if (Txtscr2.Text != "")
                {
                    Genclass.StrSrch = " " + Genclass.StrSrch;
                }

                else if (Txtscr3.Text != "")
                {
                    Genclass.StrSrch = " " + Genclass.StrSrch;
                }
                else if (txtscr4.Text != "")
                {
                    Genclass.StrSrch = " " + Genclass.StrSrch;
                }
                else if (txtscr5.Text != "")
                {
                    Genclass.StrSrch = " " + Genclass.StrSrch;
                }
                else
                {
                    if (Genclass.Dtype == 60 || Genclass.Dtype == 70)
                    {
                        Genclass.StrSrch = "uid <> 0";
                    }
                    else
                    {
                        Genclass.StrSrch = "a.uid <> 0";
                    }
                }

                if (chkact.Checked == true)
                {
                    if (Genclass.Dtype == 60 || Genclass.Dtype == 70)
                    {
                        string quy = "select UId,DocNo,DocDate,DcNo,DcDate,narration,active from TransactionsP  where active=1 and doctypeid=" + Genclass.Dtype + " and companyid=" + Genclass.data1 + " and  " + Genclass.StrSrch + "";
                        Genclass.cmd = new SqlCommand(quy, conn);
                    }
                    else
                    {
                        string quy = "select a.UId,DocNo,DocDate,DcNo,DcDate,Name as Party,a.partyuid,a.narration,a.active from TransactionsP a inner join PartyM b on a.PartyUid=b.uid where a.active=1 and a.doctypeid=" + Genclass.Dtype + " and a.companyid=" + Genclass.data1 + " and  " + Genclass.StrSrch + "";
                        Genclass.cmd = new SqlCommand(quy, conn);
                    }
                }
                else
                {
                    if (Genclass.Dtype == 60 || Genclass.Dtype == 70)
                    {
                        string quy = "select UId,DocNo,DocDate,DcNo,DcDate,narration,active from TransactionsP  where active=0 and doctypeid=" + Genclass.Dtype + " and companyid=" + Genclass.data1 + " and  " + Genclass.StrSrch + "";
                        Genclass.cmd = new SqlCommand(quy, conn);
                    }
                    else
                    {
                        string quy = "select a.UId,DocNo,DocDate,DcNo,DcDate,Name as Party,a.partyuid,a.narration,a.active from TransactionsP a inner join PartyM b on a.PartyUid=b.uid where a.active=0 and a.doctypeid=" + Genclass.Dtype + " and a.companyid=" + Genclass.data1 + " and  " + Genclass.StrSrch + "";
                        Genclass.cmd = new SqlCommand(quy, conn);
                    }
                }

                SqlDataAdapter aptr = new SqlDataAdapter(Genclass.cmd);
                DataTable tap = new DataTable();
                aptr.Fill(tap);

                this.HFGP.DefaultCellStyle.Font = new Font("Arial", 10);
                //dgv.ColumnHeadersDefaultCellStyle.Font = new Font(dgv.ColumnHeadersDefaultCellStyle.Font, FontStyle.Bold);
                this.HFGP.ColumnHeadersDefaultCellStyle.Font = new Font("Arial", 10, FontStyle.Bold);
                HFGP.AutoGenerateColumns = false;
                HFGP.Refresh();
                HFGP.DataSource = null;
                HFGP.Rows.Clear();


                HFGP.ColumnCount = tap.Columns.Count;
                Genclass.i = 0;
                foreach (DataColumn column in tap.Columns)
                {
                    HFGP.Columns[Genclass.i].Name = column.ColumnName;
                    HFGP.Columns[Genclass.i].HeaderText = column.ColumnName;
                    HFGP.Columns[Genclass.i].DataPropertyName = column.ColumnName;
                    Genclass.i = Genclass.i + 1;
                }



                HFGP.Columns[0].Visible = false;

                HFGP.Columns[1].Width = 88;
                HFGP.Columns[2].Width = 90;
                HFGP.Columns[3].Width = 90;
                HFGP.Columns[4].Width = 90;
                HFGP.Columns[5].Width = 469;
                HFGP.Columns[6].Visible = false;


                if (Genclass.Dtype == 20 || Genclass.Dtype == 30)
                {
                    HFGP.Columns[7].Visible = false;
                    HFGP.Columns[8].Visible = false;
                }





                HFGP.DataSource = tap;
                //int ct = tap.Rows.Count;
                HFGP.Columns[2].DefaultCellStyle.Format = "yyyy'/'MM'/'dd";
                HFGP.Columns[4].DefaultCellStyle.Format = "yyyy'/'MM'/'dd";


                int index = HFGP.Rows.Count - 1;

                if (HFGP.Rows[0].Cells[1].Value.ToString() == "" || HFGP.Rows[0].Cells[1].Value == null)
                {
                    lblno1.Text = "0";
                }
                else
                {
                    lblno1.Text = "1";
                }


                lblno2.Text = "of " + index.ToString();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return;
            }
            finally
            {
                conn.Close();
                Genclass.cmd.Dispose();
            }
        }




        private void button1_Click(object sender, EventArgs e)
        {
      
                mode = 1;

                Genpan.Visible = false;
                Genclass.Module.ClearTextBox(this, Editpan);
                Genclass.Module.Gendocno();
                Genpan.Visible = false;
                Editpan.Visible = true;

                txtgrn.Text = Genclass.ST;
                DateTime d = new DateTime();
                d = DateTime.Now;
                txtgrndt.Text = d.ToString("dd.MM.yyyy");
                txtdcno.Focus();

          
        }


        private void btnaddrcan_Click(object sender, EventArgs e)
        {
            //Genclass.Module.ClearTextBox(this);
            Genpan.Visible = true;
            //Loadgrid();
        }
        private void Loadgrid1()
        {
            try
            {
                conn.Open();

                string quy = " select b.uid,e.itemname,b.pqty-isnull(sum(d.pqty),0) as qty ,b.addnotes,b.itemuid from transactionsp a inner join transactionsplist b on  a.uid=b.transactionspuid and a.doctypeid=125 and a.companyid=" + Genclass.data1 + " inner join partym c on a.partyuid=c.uid left join transactionsplist d on  a.docno=d.refno  and b.itemuid=d.itemuid and d.doctypeid=130 inner join itemm e on b.itemuid=e.uid  group by e.itemname,b.pqty, b.addnotes,b.itemuid  having b.pqty-isnull(sum(d.pqty),0) >0";


                Genclass.cmd = new SqlCommand(quy, conn);



                SqlDataAdapter aptr = new SqlDataAdapter(Genclass.cmd);
                DataTable tap = new DataTable();
                aptr.Fill(tap);

                this.HFIT.DefaultCellStyle.Font = new Font("Arial", 10);
                //dgv.ColumnHeadersDefaultCellStyle.Font = new Font(dgv.ColumnHeadersDefaultCellStyle.Font, FontStyle.Bold);
                this.HFIT.ColumnHeadersDefaultCellStyle.Font = new Font("Arial", 10, FontStyle.Bold);
                HFIT.AutoGenerateColumns = false;
                HFIT.Refresh();
                HFIT.DataSource = null;
                HFIT.Rows.Clear();


                HFIT.ColumnCount = tap.Columns.Count;
                Genclass.i = 0;
                foreach (DataColumn column in tap.Columns)
                {
                    HFIT.Columns[Genclass.i].Name = column.ColumnName;
                    HFIT.Columns[Genclass.i].HeaderText = column.ColumnName;
                    HFIT.Columns[Genclass.i].DataPropertyName = column.ColumnName;
                    Genclass.i = Genclass.i + 1;
                }



                HFIT.Columns[0].Visible = false;

                HFIT.Columns[1].Width = 120;
                HFIT.Columns[2].Width = 120;
                HFIT.Columns[3].Width = 120;
                HFIT.Columns[4].Visible = false;
                //HFIT.Columns[5].Width = 400;




                HFIT.DataSource = tap;

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return;
            }
            finally
            {
                conn.Close();
                Genclass.cmd.Dispose();
            }
        }


        private void btnexit_Click(object sender, EventArgs e)
        {
            this.Dispose();
        }



        private void txtgrnno_KeyDown_1(object sender, KeyEventArgs e)
        {
            if (Genclass.Dtype == 20)
            {
                if (e.KeyCode == Keys.Enter)
                {
                    Genclass.type = 2;
                    loadput();

                    foreach (DataGridViewColumn dc in HFIT.Columns)
                    {
                        if (dc.Name == "Qty")
                        {
                            dc.ReadOnly = false;
                        }
                        else
                        {
                            dc.ReadOnly = true;
                        }
                    }
                }
                else if (e.KeyCode == Keys.Escape)
                {
                    txtgrnno.Text = "";
                    txtgrnno.Focus();
                }
            }
            else
            {
                if (e.KeyCode == Keys.Enter)
                {
                    Genclass.type = 3;
                    loadput();

                    foreach (DataGridViewColumn dc in HFIT.Columns)
                    {
                        if (dc.Name == "Qty")
                        {
                            dc.ReadOnly = false;
                        }
                        else
                        {
                            dc.ReadOnly = true;
                        }
                    }

                }
                else if (e.KeyCode == Keys.Escape)
                {
                    txtgrnno.Text = "";
                    txtgrnno.Focus();
                }
            }
        }

        private void btnaddrcan_Click_1(object sender, EventArgs e)
        {
            Editpan.Visible = false;
            Genpan.Visible = true;
            panadd.Visible = true;
            Genclass.Module.ClearTextBox(this, Editpan);
            HFIT.Refresh();
            HFIT.DataSource = null;
            HFIT.Rows.Clear();
            chkact.Checked = true;
            Loadgrid();
        }

        private void txtitem_TextChanged(object sender, EventArgs e)
        {

        }

        private void btnsave_Click_1(object sender, EventArgs e)
        {
            if (txtgrn.Text == "")
            {
                MessageBox.Show("Enter the Grnno");
                txtname.Focus();
                return;
            }

            //if (txtname.Text == "")
            //{
            //    MessageBox.Show("Enter the Party");
            //    txtname.Focus();
            //    return;
            //}

            conn.Open();




            //qur.CommandText = "insert into TransactionsP (DocNo,DocDate, DcNo, DcDate, PartyUid,Narration,doctypeid) values ('" + txtgrn.Text + "','" + txtgrndt.Text + "','" + txtdcno.Text + "','" + Dtpdt.Text + "'," + txtpuid.Text + ",'" + txtnar.Text + "'," + Genclass.Dtype + ")";
            //qur.CommandText = "insert into TransactionsPlist (doctypeid,TransactionsPUId,ItemUId,PQty,active,refuid)  values (" + Genclass.Dtype + ", " + uid + "," + HFIT.Rows[i].Cells[2].Value + "," + HFIT.Rows[i].Cells[1].Value + ",1,'" + HFIT.Rows[i].Cells[3].Value + "')";

            for (int i = 0; i < HFIT.Rows.Count - 1; i++)
            {
                if (mode == 1)
                {
                    if (Genclass.Dtype == 60 || Genclass.Dtype == 70)
                    {
                        qur.CommandText = "Exec Sp_Transactions " + Genclass.Dtype + ",'" + txtgrn.Text + "','" + DTPDOCDT.Value + "','" + txtdcno.Text + "','" + Dtpdt.Value + "',0,'" + txtname.Text + "',0,0,0,'" + chkedtact.Checked + "',0,0,''," + Genclass.data1 + "," + Genclass.Yearid + "," + HFIT.Rows[i].Cells[2].Value + "," + HFIT.Rows[i].Cells[1].Value + ",0,0,'',0,0,0,0,0,0,0,0,0,0,0," + i + "," + mode + ",null";
                        qur.ExecuteNonQuery();
                    }
                    else if (Genclass.Dtype == 30)
                    {
                        qur.CommandText = "Exec Sp_Transactions " + Genclass.Dtype + ",'" + txtgrn.Text + "','" + DTPDOCDT.Value + "','" + txtdcno.Text + "','" + Dtpdt.Value + "'," + txtpuid.Text + ",'" + txtnar.Text + "',0,0,0,'" + chkedtact.Checked + "',0,0,''," + Genclass.data1 + "," + Genclass.Yearid + "," + HFIT.Rows[i].Cells[2].Value + "," + HFIT.Rows[i].Cells[1].Value + ",0,0,'',0," + HFIT.Rows[i].Cells[3].Value + ",0,0,0,0,0,0,0,0,0," + i + "," + mode + ",null";
                        qur.ExecuteNonQuery();

                        string quy = "select * from stransactionsp where docno='" + txtgrn.Text + "'";
                        Genclass.cmd = new SqlCommand(quy, conn);
                        SqlDataAdapter aptr = new SqlDataAdapter(Genclass.cmd);
                        DataTable tap = new DataTable();
                        aptr.Fill(tap);
                        if (tap.Rows.Count > 0)
                        {
                            uid = tap.Rows[0]["uid"].ToString();
                            qur.CommandText = "insert into custOrdetails values (" + uid + "," + txtcusid.Text + "," + HFIT.Rows[i].Cells[2].Value + "," + txtbqty.Text + ")";
                            qur.ExecuteNonQuery();
                        }


                    }
                    else
                    {
                        qur.CommandText = "Exec Sp_Transactions " + Genclass.Dtype + ",'" + txtgrn.Text + "','" + DTPDOCDT.Value + "','" + txtdcno.Text + "','" + Dtpdt.Value + "'," + txtpuid.Text + ",'" + txtnar.Text + "',0,0,0,'" + chkedtact.Checked + "',0,0,''," + Genclass.data1 + "," + Genclass.Yearid + "," + HFIT.Rows[i].Cells[2].Value + "," + HFIT.Rows[i].Cells[1].Value + ",0,0,'',0," + HFIT.Rows[i].Cells[3].Value + ",0,0,0,0,0,0,0,0,0," + i + "," + mode + ",null";
                        qur.ExecuteNonQuery();

                    }
                }

                
                else
                {
                    if (Genclass.Dtype == 60 || Genclass.Dtype == 70)
                    {
                        qur.CommandText = "Exec Sp_Transactions " + Genclass.Dtype + ",'" + txtgrn.Text + "','" + DTPDOCDT.Value + "','" + txtdcno.Text + "','" + Dtpdt.Value + "',0,'" + txtname.Text + "',0,0,0,'" + chkedtact.Checked + "',0,0,''," + Genclass.data1 + "," + Genclass.Yearid + "," + HFIT.Rows[i].Cells[2].Value + "," + HFIT.Rows[i].Cells[1].Value + ",0,0,'',0,0,0,0,0,0,0,0,0," + uid + ",0," + i + "," + mode + ",null";
                        qur.ExecuteNonQuery();
                    }
                    else if (Genclass.Dtype == 30)
                    {
                        qur.CommandText = "delete from custOrdetails where Tranid=" + uid + "";
                        qur.ExecuteNonQuery();

                        qur.CommandText = "Exec Sp_Transactions " + Genclass.Dtype + ",'" + txtgrn.Text + "','" + DTPDOCDT.Value + "','" + txtdcno.Text + "','" + Dtpdt.Value + "'," + txtpuid.Text + ",'" + txtnar.Text + "',0,0,0,'" + chkedtact.Checked + "',0,0,''," + Genclass.data1 + "," + Genclass.Yearid + "," + HFIT.Rows[i].Cells[2].Value + "," + HFIT.Rows[i].Cells[1].Value + ",0,0,'',0," + HFIT.Rows[i].Cells[3].Value + ",0,0,0,0,0,0,0,0,0," + i + "," + mode + ",null";
                        qur.ExecuteNonQuery();

                        string quy = "select * from stransactionsp where docno='" + txtgrn.Text + "'";
                        Genclass.cmd = new SqlCommand(quy, conn);
                        SqlDataAdapter aptr = new SqlDataAdapter(Genclass.cmd);
                        DataTable tap = new DataTable();
                        aptr.Fill(tap);
                        if (tap.Rows.Count > 0)
                        {
                            uid = tap.Rows[0]["uid"].ToString();
                            qur.CommandText = "insert into custOrdetails values (" + uid + "," + txtcusid.Text + "," + HFIT.Rows[i].Cells[2].Value + "," + txtbqty.Text + ")";
                            qur.ExecuteNonQuery();
                        }
                    }
                    else
                    {
                        qur.CommandText = "Exec Sp_Transactions " + Genclass.Dtype + ",'" + txtgrn.Text + "','" + DTPDOCDT.Value + "','" + txtdcno.Text + "','" + Dtpdt.Value + "'," + txtpuid.Text + ",'" + txtnar.Text + "',0,0,0,'" + chkedtact.Checked + "',0,0,''," + Genclass.data1 + "," + Genclass.Yearid + "," + HFIT.Rows[i].Cells[2].Value + "," + HFIT.Rows[i].Cells[1].Value + ",0,0,'',0," + HFIT.Rows[i].Cells[3].Value + ",0,0,0,0,0,0,0," + uid + ",0," + i + "," + mode + ",null";
                        qur.ExecuteNonQuery();
                    }
                }
            }


            if (mode == 1)
            {
                qur.CommandText = "update doctypem set lastno=lastno+1 where doctypeid=" + Genclass.Dtype + "";
                qur.ExecuteNonQuery();
            }


            MessageBox.Show("Record has been saved", "Save", MessageBoxButtons.OK);



            conn.Close();
            btnaddrcan_Click_1(sender, e);
            Loadgrid();
            Genpan.Visible = true;
            panadd.Visible = true;
        }

        private void btnedit_Click(object sender, EventArgs e)
        {
            mode = 2;
            Editpan.Visible = true;
            Genpan.Visible = false;
            int i = HFGP.SelectedCells[0].RowIndex;

            txtgrn.Text = HFGP.Rows[i].Cells[1].Value.ToString();
            DTPDOCDT.Text = HFGP.Rows[i].Cells[2].Value.ToString();
            txtdcno.Text = HFGP.Rows[i].Cells[3].Value.ToString();
            Dtpdt.Text = HFGP.Rows[i].Cells[4].Value.ToString();
            if (Genclass.Dtype == 60 || Genclass.Dtype == 70)
            {
                txtname.Text = HFGP.Rows[i].Cells[5].Value.ToString();


                if (HFGP.Rows[i].Cells[6].Value.ToString() == "True")
                {
                    chkedtact.Checked = true;
                }
                else
                {
                    chkedtact.Checked = false;
                }


            }
            else
            {
                txtname.Text = HFGP.Rows[i].Cells[5].Value.ToString();
                txtpuid.Text = HFGP.Rows[i].Cells[6].Value.ToString();
                txtnar.Text = HFGP.Rows[i].Cells[7].Value.ToString();

                if (HFGP.Rows[i].Cells[8].Value.ToString() == "True")
                {
                    chkedtact.Checked = true;
                }
                else
                {
                    chkedtact.Checked = false;
                }

            }

            uid = HFGP.Rows[i].Cells[0].Value.ToString();
            Loadgrid2();
        }


        private void Loadgrid2()
        {
            try
            {
                conn.Open();
              
         
                    string quy = "select c.itemname,b.pqty,b.itemuid,refuid from transactionsp a inner join transactionsplist b on a.uid=b.transactionspuid and a.companyid=" + Genclass.data1 + " inner join itemm c on b.itemuid=c.uid where a.uid=" + uid + "";
                    Genclass.cmd = new SqlCommand(quy, conn);
         
               



                SqlDataAdapter aptr = new SqlDataAdapter(Genclass.cmd);
                DataTable tap = new DataTable();
                aptr.Fill(tap);

                this.HFIT.DefaultCellStyle.Font = new Font("Arial", 10);
                //dgv.ColumnHeadersDefaultCellStyle.Font = new Font(dgv.ColumnHeadersDefaultCellStyle.Font, FontStyle.Bold);
                this.HFIT.ColumnHeadersDefaultCellStyle.Font = new Font("Arial", 10, FontStyle.Bold);
                HFIT.AutoGenerateColumns = false;
                HFIT.Refresh();
                HFIT.DataSource = null;
                HFIT.Rows.Clear();


                HFIT.ColumnCount = tap.Columns.Count;
                Genclass.i = 0;
                foreach (DataColumn column in tap.Columns)
                {
                    HFIT.Columns[Genclass.i].Name = column.ColumnName;
                    HFIT.Columns[Genclass.i].HeaderText = column.ColumnName;
                    HFIT.Columns[Genclass.i].DataPropertyName = column.ColumnName;
                    Genclass.i = Genclass.i + 1;
                }



                HFIT.Columns[0].Width = 450;

                HFIT.Columns[1].Width = 75;
                HFIT.Columns[2].Visible = false;
                HFIT.Columns[3].Visible = false;

                //HFIT.Columns[5].Width = 400;




                HFIT.DataSource = tap;

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return;
            }
            finally
            {
                conn.Close();
                Genclass.cmd.Dispose();
            }
        }

        private void txtname_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if (Genclass.Dtype == 20)
                {
                    Genclass.type = 1;
                    loadput();
                }
                else if (Genclass.Dtype == 30)
                {
                    Genclass.type = 5;
                    loadput();
                }


            }
            else if (e.KeyCode == Keys.Escape)
            {
                txtname.Text = "";
                txtname.Focus();
            }

        }


        private void cmdprt_Click(object sender, EventArgs e)
        {
            conn.Open();


            Genclass.Gbtxtid = 101;

            qur.CommandText = "delete from  Printtable";
            qur.ExecuteNonQuery();

            int i = HFGP.SelectedCells[0].RowIndex;

            qur.CommandText = "insert into Printtable select * from vw_TransactionsP where uid=" + HFGP.Rows[i].Cells[0].Value.ToString() + "";
            qur.ExecuteNonQuery();

            Genclass.strsql = "select * from Printtable";

            //Crviewer crv = new Crviewer();
            //crv.Show();
            //conn.Close();
        }


        private void chkact_CheckedChanged(object sender, EventArgs e)
        {
            Loadgrid();
        }

        private void btndelete_Click(object sender, EventArgs e)
        {
            conn.Open();
            mode = 3;
            int i = HFGP.SelectedCells[0].RowIndex;
            qur.CommandText = "Exec Sp_Transactions " + Genclass.Dtype + ",'" + txtgrn.Text + "','" + DTPDOCDT.Value + "','" + txtdcno.Text + "','" + Dtpdt.Value + "',0,'" + txtnar.Text + "',0,0,0,0,0,0,''," + Genclass.data1 + "," + Genclass.Yearid + ",0,0,0,0,'',0,0,0,0,0,0,0,0,0," + HFGP.Rows[i].Cells[0].Value + ",0," + i + "," + mode + ",null";
            qur.ExecuteNonQuery();
            conn.Close();
            Loadgrid();
        }


        public static string Encrypt(string clearText)
        {
            string EncryptionKey = "myla123";
            byte[] clearBytes = Encoding.Unicode.GetBytes(clearText);
            using (Aes encryptor = Aes.Create())
            {
                Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes(EncryptionKey, new byte[] { 0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 });
                encryptor.Key = pdb.GetBytes(32);
                encryptor.IV = pdb.GetBytes(16);
                using (MemoryStream ms = new MemoryStream())
                {
                    using (CryptoStream cs = new CryptoStream(ms, encryptor.CreateEncryptor(), CryptoStreamMode.Write))
                    {
                        cs.Write(clearBytes, 0, clearBytes.Length);
                        cs.Close();
                    }
                    clearText = Convert.ToBase64String(ms.ToArray());
                }
            }
            return clearText;

        }

        //public static string Decrypt(string cipherText)
        //{
        //    string EncryptionKey = "myla123";
        //    cipherText = cipherText.Replace(" ", "+");
        //    byte[] cipherBytes = Convert.FromBase64String(cipherText);
        //    using (Aes encryptor = Aes.Create())
        //    {
        //        Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes(EncryptionKey, new byte[] { 0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 });
        //        encryptor.Key = pdb.GetBytes(32);
        //        encryptor.IV = pdb.GetBytes(16);
        //        using (MemoryStream ms = new MemoryStream())
        //        {
        //            using (CryptoStream cs = new CryptoStream(ms, encryptor.CreateDecryptor(), CryptoStreamMode.Write))
        //            {
        //                cs.Write(cipherBytes, 0, cipherBytes.Length);
        //                cs.Close();
        //            }
        //            cipherText = Encoding.Unicode.GetString(ms.ToArray());
        //        }
        //    }
        //    return cipherText;
        //}

        //private void ClearText_TextChanged(object sender, EventArgs e)
        //{

        //}

        //private void btnEncrypt_Click(object sender, EventArgs e)
        //{
        //    txtCipherText.Text = Encrypt(ClearText.Text);
        //}

        //private void btnDecrypt_Click(object sender, EventArgs e)
        //{
        //    txtDecryptedText.Text = Decrypt(txtCipherText.Text);
        //}


        private void txtnar_KeyDown(object sender, KeyEventArgs e)
        {
            //if (txtname.Text == "")
            //{
            //    MessageBox.Show("Select the party");
            //    return;
            //}

            if (e.KeyCode == Keys.Enter)
            {
                if (Genclass.Dtype == 20)
                {
                    Genclass.type = 4;
                    loadput();
                }
                else if (Genclass.Dtype == 30)
                {
                    Genclass.type = 6;
                    loadput();
                }
                else
                {
                    Genclass.type = 7;
                    loadput();
                }

            }


        }

        //private void txtgrnid_TextChanged_1(object sender, EventArgs e)
        //{

        //    if (Genclass.Dtype == 20)
        //    {
        //        if (txtgrnid.Text == "")
        //        {
        //            return;

        //        }
        //        else
        //        {
        //            //Genclass.strsql = "select distinct itemname,b.pqty-isnull(sum(d.pqty),0) as pqty,b.itemuid,b.uid from transactionsp a inner join transactionsplist b on  a.uid=b.transactionspuid and a.doctypeid=10 inner join partym c on a.partyuid=c.uid left join transactionsplist d on  a.docno=d.refno  and b.itemuid=d.itemuid and d.doctypeid=20 inner join itemm e on b.itemuid=e.uid where a.uid=" + txtgrnid.Text + " group by itemname,b.pqty,b.itemuid,b.uid  having b.pqty-isnull(sum(d.pqty),0) >0 ";
        //            Genclass.strsql = "select distinct itemname,b.pqty-isnull(sum(d.pqty),0)-isnull(sum(f.pqty),0) as pqty,b.itemuid,b.uid,a.docno from transactionsp a inner join  transactionsplist b on  a.uid=b.transactionspuid and a.doctypeid=10 inner join partym c on a.partyuid=c.uid left join  transactionsplist d on  b.uid=d.refuid  and b.itemuid=d.itemuid and d.doctypeid=20 inner join itemm e on b.itemuid=e.uid left join transactionsplist f on  b.uid=f.refuid and f.doctypeid=50 where a.uid=" + txtgrnid.Text + " and b.itemuid=" + txtnrid.Text + " group by itemname,b.pqty,b.itemuid,b.uid,a.docno  having b.pqty-isnull(sum(d.pqty),0)-isnull(sum(f.pqty),0) >0 ";
        //            Genclass.cmd = new SqlCommand(Genclass.strsql, conn);
        //            SqlDataAdapter aptr1 = new SqlDataAdapter(Genclass.cmd);
        //            DataTable tap1 = new DataTable();
        //            aptr1.Fill(tap1);
        //            //for (int i = 0; i < tap1.Rows.Count; i++)
        //            //{
        //            var index = HFIT.Rows.Add();
        //            HFIT.Rows[index].Cells[0].Value = tap1.Rows[0]["itemname"].ToString();
        //            HFIT.Rows[index].Cells[1].Value = tap1.Rows[0]["pqty"].ToString();
        //            HFIT.Rows[index].Cells[2].Value = tap1.Rows[0]["itemuid"].ToString();
        //            HFIT.Rows[index].Cells[3].Value = tap1.Rows[0]["uid"].ToString();
        //            HFIT.Rows[index].Cells[4].Value = tap1.Rows[0]["docno"].ToString();
        //            //}

        //        }
        //    }
        //    else if (Genclass.Dtype == 30)
        //    {
        //        if (txtgrnid.Text == "")
        //        {
        //            return;

        //        }
        //        else
        //        {
        //            //Genclass.strsql = "select distinct itemname,b.pqty-isnull(sum(d.pqty),0) as pqty,b.itemuid,b.uid from transactionsp a inner join transactionsplist b on  a.uid=b.transactionspuid and a.doctypeid=10 inner join partym c on a.partyuid=c.uid left join transactionsplist d on  a.docno=d.refno  and b.itemuid=d.itemuid and d.doctypeid=20 inner join itemm e on b.itemuid=e.uid where a.uid=" + txtgrnid.Text + " group by itemname,b.pqty,b.itemuid,b.uid  having b.pqty-isnull(sum(d.pqty),0) >0 ";
        //            Genclass.strsql = "Select itemname,DCQty-isnull(sum(pqty),0) as pqty,uid,itid,docno from (select distinct a.uid,a.docno,b.DCQty,c.Uid as puid,c.name,d.itemname,d.uid as itid from transactionsp a inner join  transactionsplist b on  a.uid=b.transactionspuid and a.doctypeid=50  inner join partym c  on a.partyuid=c.uid inner join itemm d on a.adddutyuid=d.uid) a left join (select refuid,isnull(PQty,0) as pqty from transactionsplist) b on a.uid=b.Refuid group by DCQty,itemname,uid,itid,docno";
        //            Genclass.cmd = new SqlCommand(Genclass.strsql, conn);
        //            SqlDataAdapter aptr1 = new SqlDataAdapter(Genclass.cmd);
        //            DataTable tap1 = new DataTable();
        //            aptr1.Fill(tap1);
        //            //for (int i = 0; i < tap1.Rows.Count; i++)
        //            //{
        //            var index = HFIT.Rows.Add();
        //            HFIT.Rows[index].Cells[0].Value = tap1.Rows[0]["itemname"].ToString();
        //            HFIT.Rows[index].Cells[1].Value = tap1.Rows[0]["pqty"].ToString();
        //            HFIT.Rows[index].Cells[2].Value = tap1.Rows[0]["itid"].ToString();
        //            HFIT.Rows[index].Cells[3].Value = tap1.Rows[0]["uid"].ToString();
        //            HFIT.Rows[index].Cells[4].Value = tap1.Rows[0]["docno"].ToString();
        //            //}

        //        }
        //    }

        //    txtnar.Text = "";
        //    //txtgrnno.Text = "";
        //    //txtgrnid.Text = "";
        //    txtnar.Focus();

        //}


        private void buttrqok_Click(object sender, EventArgs e)
        {
            if (Genclass.Dtype == 20 || Genclass.Dtype == 30 )
            {
                for (int i = 0; i < HFIT.Rows.Count - 1; i++)
                {
                    if (txtnrid.Text == HFIT.Rows[i].Cells[2].Value.ToString() && txtgrnid.Text == HFIT.Rows[i].Cells[3].Value.ToString())
                    {

                        MessageBox.Show("Item for this Grnno already Exist");
                        txtnar.Text = "";
                        txtfqty.Text = "";
                        txtnrid.Text = "";
                        txtgrnid.Text = "";
                        txtgrnno.Text = "";
                        txtitem.Focus();
                        return;
                    }
                }

                if (Convert.ToDouble(txtfqty.Text) > Convert.ToDouble(txtqty.Text))
                {
                    MessageBox.Show("Qty Exceeds");
                    txtfqty.Text = txtqty.Text;
                    txtfqty.Focus();
                    return;
                }
            }

            HFIT.AllowUserToAddRows = true;
            var index = HFIT.Rows.Add();
            if (Genclass.Dtype == 60 || Genclass.Dtype == 70)
            {
                HFIT.Rows[index].Cells[0].Value = txtnar.Text;
                HFIT.Rows[index].Cells[1].Value = txtfqty.Text;
                HFIT.Rows[index].Cells[2].Value = txtnrid.Text;
            }
            else if (Genclass.Dtype == 20 || Genclass.Dtype == 30)
            {
                HFIT.Rows[index].Cells[0].Value = txtnar.Text;
                HFIT.Rows[index].Cells[1].Value = txtfqty.Text;
                HFIT.Rows[index].Cells[2].Value = txtnrid.Text;
                if (txtgrnid.Text != "")
                {
                    HFIT.Rows[index].Cells[3].Value = txtgrnid.Text;
                }
                else
                {
                    txtgrnid.Text = "0";
                    HFIT.Rows[index].Cells[3].Value = txtgrnid.Text;
                }
                if (txtgrnno.Text != "")
                {
                    HFIT.Rows[index].Cells[4].Value = txtgrnno.Text;
                }

                else
                {
                    txtgrnno.Text = "0";
                    HFIT.Rows[index].Cells[4].Value = txtgrnno.Text;
                }
            }
            //DataTable dt = HFIT.DataSource as DataTable;
            //if (Genclass.Dtype == 20 || Genclass.Dtype == 30)
            //{
            //    dt.Rows.Add(txtnar.Text, txtfqty.Text, txtnrid.Text, txtgrnid.Text, txtgrnno.Text);
            //}
            //else
            //{
            //        dt.Rows.Add(txtnar.Text,txtfqty.Text,txtnrid.Text);
            //}

            //HFIT.DataSource = dt;


            //dt.Columns.Add("Item");
            //dt.Columns.Add("Qty");
            //dt.Columns.Add("itid");



            //DataRow dr = dt.NewRow();
            //dr["Item"] = txtnar.Text;

            //dr["Qty"] = txtfqty.Text;
            //dr["itid"] = txtnrid.Text;

            //dt.Rows.Add(dr);

            //HFIT.DataSource = dt.DefaultView.Table;


            txtnar.Text = "";
            txtfqty.Text = "";
            txtnrid.Text = "";
            txtgrnid.Text = "";
            txtgrnno.Text = "";
            txtitem.Focus();

        }

        private void txtname_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtgrnno_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtscr1_TextChanged(object sender, EventArgs e)
        {
            Loadgrid();
        }

        private void Txtscr2_TextChanged(object sender, EventArgs e)
        {
            Loadgrid();
        }

        private void Txtscr3_TextChanged(object sender, EventArgs e)
        {
            Loadgrid();
        }

        private void txtscr4_TextChanged(object sender, EventArgs e)
        {
            Loadgrid();
        }

        private void txtscr5_TextChanged(object sender, EventArgs e)
        {
            Loadgrid();
        }

        private void chkact_CheckedChanged_1(object sender, EventArgs e)
        {
            Loadgrid();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            mode = 1;

            Genpan.Visible = false;
            Genclass.Module.ClearTextBox(this, Editpan);
            Genclass.Module.Gendocno();
            Genpan.Visible = false;
            Editpan.Visible = true;
            panadd.Visible = false;
            txtgrn.Text = Genclass.ST;
            DateTime d = new DateTime();
            d = DateTime.Now;
            txtgrndt.Text = d.ToString("dd.MM.yyyy");
            txtdcno.Focus();
        }

        private void butedit_Click(object sender, EventArgs e)
        {
            mode = 2;
            Editpan.Visible = true;
            Genpan.Visible = false;
            panadd.Visible = false;
            
            int i = HFGP.SelectedCells[0].RowIndex;

            txtgrn.Text = HFGP.Rows[i].Cells[1].Value.ToString();
            DTPDOCDT.Text = HFGP.Rows[i].Cells[2].Value.ToString();
            txtdcno.Text = HFGP.Rows[i].Cells[3].Value.ToString();
            Dtpdt.Text = HFGP.Rows[i].Cells[4].Value.ToString();
            if (Genclass.Dtype == 60 || Genclass.Dtype == 70)
            {
                txtname.Text = HFGP.Rows[i].Cells[5].Value.ToString();


                if (HFGP.Rows[i].Cells[6].Value.ToString() == "True")
                {
                    chkedtact.Checked = true;
                }
                else
                {
                    chkedtact.Checked = false;
                }


            }
            else
            {
                txtname.Text = HFGP.Rows[i].Cells[5].Value.ToString();
                txtpuid.Text = HFGP.Rows[i].Cells[6].Value.ToString();
                txtnar.Text = HFGP.Rows[i].Cells[7].Value.ToString();

                if (HFGP.Rows[i].Cells[8].Value.ToString() == "True")
                {
                    chkedtact.Checked = true;
                }
                else
                {
                    chkedtact.Checked = false;
                }

            }

            uid = HFGP.Rows[i].Cells[0].Value.ToString();
            Loadgrid2();
        }

        private void butexit_Click(object sender, EventArgs e)
        {
            conn.Open();


            Genclass.Gbtxtid = 101;

            qur.CommandText = "delete from  Printtable";
            qur.ExecuteNonQuery();

            int i = HFGP.SelectedCells[0].RowIndex;

            qur.CommandText = "insert into Printtable select * from vw_TransactionsP where uid=" + HFGP.Rows[i].Cells[0].Value.ToString() + "";
            qur.ExecuteNonQuery();

            Genclass.strsql = "select * from Printtable";

            //Crviewer crv = new Crviewer();
            //crv.Show();
            //conn.Close();
        }

        private void buttnnvfst_Click(object sender, EventArgs e)
        {
            int index = HFGP.SelectedRows[0].Index;
            int index1 = 0;
            int index2 = HFGP.Rows.Count - 1;

            HFGP.Rows[index].Selected = false;
            HFGP.Rows[index1].Selected = true;
            index1 = index1 + 1;
            lblno1.Text = index1.ToString();
            lblno2.Text = "of " + index2.ToString();
        }

        private void buttnnxtlft_Click(object sender, EventArgs e)
        {
            int index = HFGP.SelectedRows[0].Index;
            int index1 = HFGP.Rows.Count - 1; ;
            if (index > 0)
            {
                HFGP.Rows[index].Selected = false;
                HFGP.Rows[index - 1].Selected = true;
                lblno1.Text = index.ToString();
                lblno2.Text = "of " + index1.ToString();
            }
        }

        private void buttrnxt_Click(object sender, EventArgs e)
        {
            int index = HFGP.SelectedRows[0].Index;
            int index2 = HFGP.SelectedRows[0].Index + 2;
            int index1 = HFGP.Rows.Count - 1; ;
            if (index < HFGP.Rows.Count - 2)
            {
                HFGP.Rows[index].Selected = false;
                HFGP.Rows[index + 1].Selected = true;
                lblno1.Text = index2.ToString();
                lblno2.Text = "of " + index1.ToString();

            }
        }

        private void btnfinnxt_Click(object sender, EventArgs e)
        {
            int index = HFGP.SelectedRows[0].Index;
            int index1 = HFGP.Rows.Count - 1;


            HFGP.Rows[index].Selected = false;

            HFGP.Rows[index1 - 1].Selected = true;
            lblno1.Text = index1.ToString();
            lblno2.Text = "of " + index1.ToString();
        }

        private void HFGP_CellMouseUp(object sender, DataGridViewCellMouseEventArgs e)
        {
            if (HFGP.Rows[0].Cells[0].Value.ToString() != "")
            {
                int index = HFGP.SelectedRows[0].Index;
                if (index < HFGP.Rows.Count - 1)
                {
                    index = index + 1;
                    int index1 = HFGP.Rows.Count - 1;
                    lblno1.Text = index.ToString();
                    lblno2.Text = "of " + index1.ToString();
                }
            }
        }

        private void HFGP_KeyUp(object sender, KeyEventArgs e)
        {
            if (HFGP.Rows[0].Cells[0].Value.ToString() != "")
            {
                int index = HFGP.SelectedRows[0].Index;
                if (index < HFGP.Rows.Count - 1)
                {
                    index = index + 1;
                    int index1 = HFGP.Rows.Count - 1;
                    lblno1.Text = index.ToString();
                    lblno2.Text = "of " + index1.ToString();
                }
            }
        }

        private void buttnext1_Click(object sender, EventArgs e)
        {
            this.Dispose();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            this.Dispose();
        }

        private void txtcusorderno_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if (Genclass.Dtype == 30)
                {
                    Genclass.type = 8;
                    loadput();
                }
            }
        }

        private void txtcusorderno_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtcusid_TextChanged(object sender, EventArgs e)
        {

        }
        public string Rupees(Int64 rup)
        {
            string result = "";
            Int64 res;
            if ((rup / 10000000) > 0)
            {
                res = rup / 10000000;
                rup = rup % 10000000;
                result = result + ' ' + RupeesToWords(res) + " Crore";
            }
            if ((rup / 100000) > 0)
            {
                res = rup / 100000;
                rup = rup % 100000;
                result = result + ' ' + RupeesToWords(res) + " Lakh";
            }
            if ((rup / 1000) > 0)
            {
                res = rup / 1000;
                rup = rup % 1000;
                result = result + ' ' + RupeesToWords(res) + " Thousand";
            }


            if ((rup / 100) > 0)
            {
                res = rup / 100;
                rup = rup % 100;
                if (rup > 0)
                {
                    result = result + ' ' + RupeesToWords(res) + " Hundred and";
                }
                else
                {
                    result = result + ' ' + RupeesToWords(res) + " Hundred";
                }
            }
            if ((rup % 10) >= 0)
            {
                res = rup % 100;
                result = result + " " + RupeesToWords(res);
            }
            result = "Rupees " + result + ' ' + "only";
            return result;
        }

        public string RupeesToWords(Int64 rup)
        {
            string result = "";
            if ((rup >= 1) && (rup <= 10))
            {
                if ((rup % 10) == 1) result = "One";
                if ((rup % 10) == 2) result = "Two";
                if ((rup % 10) == 3) result = "Three";
                if ((rup % 10) == 4) result = "Four";
                if ((rup % 10) == 5) result = "Five";
                if ((rup % 10) == 6) result = "Six";
                if ((rup % 10) == 7) result = "Seven";
                if ((rup % 10) == 8) result = "Eight";
                if ((rup % 10) == 9) result = "Nine";
                if ((rup % 10) == 0) result = "Ten";
            }
            if (rup > 9 && rup < 20)
            {
                if (rup == 11) result = "Eleven";
                if (rup == 12) result = "Twelve";
                if (rup == 13) result = "Thirteen";
                if (rup == 14) result = "Forteen";
                if (rup == 15) result = "Fifteen";
                if (rup == 16) result = "Sixteen";
                if (rup == 17) result = "Seventeen";
                if (rup == 18) result = "Eighteen";
                if (rup == 19) result = "Nineteen";
            }

            if (rup >= 20 && (rup / 10) == 2 && (rup % 10) == 0) result = "Twenty";
            if (rup > 20 && (rup / 10) == 3 && (rup % 10) == 0) result = "Thirty";
            if (rup > 20 && (rup / 10) == 4 && (rup % 10) == 0) result = "Forty";
            if (rup > 20 && (rup / 10) == 5 && (rup % 10) == 0) result = "Fifty";
            if (rup > 20 && (rup / 10) == 6 && (rup % 10) == 0) result = "Sixty";
            if (rup > 20 && (rup / 10) == 7 && (rup % 10) == 0) result = "Seventy";
            if (rup > 20 && (rup / 10) == 8 && (rup % 10) == 0) result = "Eighty";
            if (rup > 20 && (rup / 10) == 9 && (rup % 10) == 0) result = "Ninty";

            if (rup > 20 && (rup / 10) == 2 && (rup % 10) != 0)
            {
                if ((rup % 10) == 1) result = "Twenty One";
                if ((rup % 10) == 2) result = "Twenty Two";
                if ((rup % 10) == 3) result = "Twenty Three";
                if ((rup % 10) == 4) result = "Twenty Four";
                if ((rup % 10) == 5) result = "Twenty Five";
                if ((rup % 10) == 6) result = "Twenty Six";
                if ((rup % 10) == 7) result = "Twenty Seven";
                if ((rup % 10) == 8) result = "Twenty Eight";
                if ((rup % 10) == 9) result = "Twenty Nine";
            }
            if (rup > 20 && (rup / 10) == 3 && (rup % 10) != 0)
            {
                if ((rup % 10) == 1) result = "Thirty One";
                if ((rup % 10) == 2) result = "Thirty Two";
                if ((rup % 10) == 3) result = "Thirty Three";
                if ((rup % 10) == 4) result = "Thirty Four";
                if ((rup % 10) == 5) result = "Thirty Five";
                if ((rup % 10) == 6) result = "Thirty Six";
                if ((rup % 10) == 7) result = "Thirty Seven";
                if ((rup % 10) == 8) result = "Thirty Eight";
                if ((rup % 10) == 9) result = "Thirty Nine";
            }
            if (rup > 20 && (rup / 10) == 4 && (rup % 10) != 0)
            {
                if ((rup % 10) == 1) result = "Forty One";
                if ((rup % 10) == 2) result = "Forty Two";
                if ((rup % 10) == 3) result = "Forty Three";
                if ((rup % 10) == 4) result = "Forty Four";
                if ((rup % 10) == 5) result = "Forty Five";
                if ((rup % 10) == 6) result = "Forty Six";
                if ((rup % 10) == 7) result = "Forty Seven";
                if ((rup % 10) == 8) result = "Forty Eight";
                if ((rup % 10) == 9) result = "Forty Nine";
            }
            if (rup > 20 && (rup / 10) == 5 && (rup % 10) != 0)
            {
                if ((rup % 10) == 1) result = "Fifty One";
                if ((rup % 10) == 2) result = "Fifty Two";
                if ((rup % 10) == 3) result = "Fifty Three";
                if ((rup % 10) == 4) result = "Fifty Four";
                if ((rup % 10) == 5) result = "Fifty Five";
                if ((rup % 10) == 6) result = "Fifty Six";
                if ((rup % 10) == 7) result = "Fifty Seven";
                if ((rup % 10) == 8) result = "Fifty Eight";
                if ((rup % 10) == 9) result = "Fifty Nine";
            }
            if (rup > 20 && (rup / 10) == 6 && (rup % 10) != 0)
            {
                if ((rup % 10) == 1) result = "Sixty One";
                if ((rup % 10) == 2) result = "Sixty Two";
                if ((rup % 10) == 3) result = "Sixty Three";
                if ((rup % 10) == 4) result = "Sixty Four";
                if ((rup % 10) == 5) result = "Sixty Five";
                if ((rup % 10) == 6) result = "Sixty Six";
                if ((rup % 10) == 7) result = "Sixty Seven";
                if ((rup % 10) == 8) result = "Sixty Eight";
                if ((rup % 10) == 9) result = "Sixty Nine";
            }
            if (rup > 20 && (rup / 10) == 7 && (rup % 10) != 0)
            {
                if ((rup % 10) == 1) result = "Seventy One";
                if ((rup % 10) == 2) result = "Seventy Two";
                if ((rup % 10) == 3) result = "Seventy Three";
                if ((rup % 10) == 4) result = "Seventy Four";
                if ((rup % 10) == 5) result = "Seventy Five";
                if ((rup % 10) == 6) result = "Seventy Six";
                if ((rup % 10) == 7) result = "Seventy Seven";
                if ((rup % 10) == 8) result = "Seventy Eight";
                if ((rup % 10) == 9) result = "Seventy Nine";
            }
            if (rup > 20 && (rup / 10) == 8 && (rup % 10) != 0)
            {
                if ((rup % 10) == 1) result = "Eighty One";
                if ((rup % 10) == 2) result = "Eighty Two";
                if ((rup % 10) == 3) result = "Eighty Three";
                if ((rup % 10) == 4) result = "Eighty Four";
                if ((rup % 10) == 5) result = "Eighty Five";
                if ((rup % 10) == 6) result = "Eighty Six";
                if ((rup % 10) == 7) result = "Eighty Seven";
                if ((rup % 10) == 8) result = "Eighty Eight";
                if ((rup % 10) == 9) result = "Eighty Nine";
            }
            if (rup > 20 && (rup / 10) == 9 && (rup % 10) != 0)
            {
                if ((rup % 10) == 1) result = "Ninty One";
                if ((rup % 10) == 2) result = "Ninty Two";
                if ((rup % 10) == 3) result = "Ninty Three";
                if ((rup % 10) == 4) result = "Ninty Four";
                if ((rup % 10) == 5) result = "Ninty Five";
                if ((rup % 10) == 6) result = "Ninty Six";
                if ((rup % 10) == 7) result = "Ninty Seven";
                if ((rup % 10) == 8) result = "Ninty Eight";
                if ((rup % 10) == 9) result = "Ninty Nine";
            }
            return result;
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            //conn.Open();

            //qur.CommandText = "delete from  NotoWords";
            //qur.ExecuteNonQuery();

            //Int64 NumVal = Convert.ToInt64(HFGP.CurrentRow.Cells[6].Value);
            //string Nw = Rupees(NumVal);

            //qur.CommandText = "Insert into NotoWords values('" + Nw + "',1)";
            //qur.ExecuteNonQuery();

            //Genclass.Prtid = Convert.ToInt16(HFGP.CurrentRow.Cells[0].Value.ToString());


            //for (int i = 1; i < 5; i++)
            //{
            //    Genclass.slno = i;
            //    Crviewer crv = new Crviewer();

            //    Genclass.strsql = "select * from Vw_salprtexlfin   where muid=" + Genclass.Prtid + " and Companyid=" + Genclass.data1 + "";

            //    Genclass.cmd = new SqlCommand(Genclass.strsql, conn);
            //    SqlDataAdapter aptr3 = new SqlDataAdapter(Genclass.cmd);
            //    DataTable tap3 = new DataTable();
            //    aptr3.Fill(tap3);
            //    if (tap3.Rows.Count > 0)
            //    {
            //        if (tap3.Rows[0]["bstate"].ToString() == "Tamil Nadu 33")
            //        {
            //            if (tap3.Rows[0]["tag"].ToString() == "0")
            //            {

            //                SqlDataAdapter da = new SqlDataAdapter("sp_pdf", conn);
            //                da.SelectCommand.CommandType = CommandType.StoredProcedure;
            //                da.SelectCommand.Parameters.Add("@Muid", SqlDbType.Int).Value = Genclass.Prtid;
            //                da.SelectCommand.Parameters.Add("@slno", SqlDbType.Int).Value = Genclass.slno;
            //                da.SelectCommand.Parameters.Add("@companyid", SqlDbType.Int).Value = Genclass.data1;
            //                DataSet ds = new DataSet();
            //                da.Fill(ds, "salesinvoice");
            //                if (Genclass.data1 == 1)
            //                {
            //                    doc.Load(Application.StartupPath + "\\salesinvoice.rpt");
            //                }
            //                else
            //                {
            //                    doc.Load(Application.StartupPath + "\\salesinvoiceven.rpt");
            //                }
            //                //doc.Load(@"C:\Users\Admin\Desktop\Simta Trading\Subcontract\salesinvoice.rpt");
            //                doc.SetDataSource(ds);
            //            }
            //        }
            //    }
            //}
        }

        private void panadd_Paint(object sender, PaintEventArgs e)
        {

        }

        private void txtnar_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtnar_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                if (Genclass.Dtype == 20)
                {
                    Genclass.type = 4;
                    loadput();
                }
                else if (Genclass.Dtype == 30)
                {
                    Genclass.type = 6;
                    loadput();
                }
                else
                {
                    Genclass.type = 7;
                    loadput();
                }

            }
            else if (e.Button == MouseButtons.Left)
            {
                if (Genclass.Dtype == 20)
                {
                    Genclass.type = 4;
                    loadput();
                }
                else if (Genclass.Dtype == 30)
                {
                    Genclass.type = 6;
                    loadput();
                }
                else
                {
                    Genclass.type = 7;
                    loadput();
                }

            }
        }

        private void txtname_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                if (Genclass.Dtype == 20)
                {
                    Genclass.type = 1;
                    loadput();
                }
                else if (Genclass.Dtype == 30)
                {
                    Genclass.type = 5;
                    loadput();
                }


            }
            else if (e.Button == MouseButtons.Left)
            {
                if (Genclass.Dtype == 20)
                {
                    Genclass.type = 1;
                    loadput();
                }
                else if (Genclass.Dtype == 30)
                {
                    Genclass.type = 5;
                    loadput();
                }


            }
        }

        private void Editpan_Paint(object sender, PaintEventArgs e)
        {
        
        }


    }
}
