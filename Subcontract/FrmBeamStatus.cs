﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ACV
{
    public partial class FrmBeamStatus : Form
    {
        public FrmBeamStatus()
        {
            InitializeComponent();
            sfDataGrid1.Style.HeaderStyle.BackColor = Color.DeepSkyBlue;
            sfDataGrid2.Style.HeaderStyle.BackColor = Color.DeepSkyBlue;
        }
        SqlConnection conn = new SqlConnection("Data Source=" + Genclass.data2 + "; Initial Catalog=" + Genclass.data5 + ";User id=" + Genclass.data3 + ";Password=" + Genclass.data4 + "");
        BindingSource bsyarn = new BindingSource();
        BindingSource bsBeam = new BindingSource();
        SQLDBHelper db = new SQLDBHelper();
        int SelectedId = 0;
        int Fillid = 0;

        private void TxtYarn_Click(object sender, EventArgs e)
        {
            try
            {
                string Query = "Select YarnId,YarnName,YarnShortName from YarnMaster Where Active =1 order by YarnName";
                DataTable dt = db.GetDataWithoutParam(CommandType.Text, Query, conn);
                bsyarn.DataSource = dt;
                Point loc = FindLocation(txtYarn);
                grSearch.Location = new Point(loc.X, loc.Y + 20);
                LoadDataGrid(dt, 1);
                grSearch.Visible = true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private Point FindLocation(Control ctrl)
        {
            if (ctrl.Parent is Form)
                return ctrl.Location;
            else
            {
                Point p = FindLocation(ctrl.Parent);
                p.X += ctrl.Location.X;
                p.Y += ctrl.Location.Y;
                return p;
            }
        }

        private void LoadDataGrid(DataTable dt, int FillId)
        {
            try
            {
                SelectedId = 1;
                DataGridCommon.DataSource = null;
                if (FillId == 1)
                {
                    Fillid = 1;
                    DataGridCommon.AutoGenerateColumns = false;
                    DataGridCommon.ColumnCount = 3;
                    DataGridCommon.Columns[0].Name = "Uid";
                    DataGridCommon.Columns[0].HeaderText = "Uid";
                    DataGridCommon.Columns[0].DataPropertyName = "YarnId";
                    DataGridCommon.Columns[0].Visible = false;

                    DataGridCommon.Columns[1].Name = "YarnName";
                    DataGridCommon.Columns[1].HeaderText = "Yarn Count";
                    DataGridCommon.Columns[1].DataPropertyName = "YarnName";
                    DataGridCommon.Columns[1].Width = 200;

                    DataGridCommon.Columns[2].Name = "YarnShortName";
                    DataGridCommon.Columns[2].HeaderText = "ShortName";
                    DataGridCommon.Columns[2].DataPropertyName = "YarnShortName";
                    DataGridCommon.Columns[2].Width = 140;
                    DataGridCommon.DataSource = bsyarn;
                }
                else if (FillId == 2)
                {
                    Fillid = 2; 
                    DataGridCommon.Columns.Clear();
                    DataGridCommon.AutoGenerateColumns = false;
                    DataGridCommon.ColumnCount = 2;
                    DataGridCommon.Columns[0].Name = "Barcode";
                    DataGridCommon.Columns[0].HeaderText = "Barcode";
                    DataGridCommon.Columns[0].DataPropertyName = "Barcode";
                    DataGridCommon.Columns[0].Width = 200;

                    DataGridCommon.Columns[1].Name = "Beam";
                    DataGridCommon.Columns[1].HeaderText = "Beam";
                    DataGridCommon.Columns[1].DataPropertyName = "Beam";
                    DataGridCommon.Columns[1].Width = 140;
                    DataGridCommon.DataSource = bsBeam;
                }
                SelectedId = 0;
                grSearch.Visible = true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void btnSelect_Click(object sender, EventArgs e)
        {
            try
            {
                SelectedId = 1;
                if (Fillid == 1)
                {
                    int Index = DataGridCommon.SelectedCells[0].RowIndex;
                    txtYarn.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                    txtYarn.Tag = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                }
                else if (Fillid == 2)
                {
                    int Index = DataGridCommon.SelectedCells[0].RowIndex;
                    txtBeam.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                    txtBeam.Tag = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                }
                SelectedId = 0;
                grSearch.Visible = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void btnHide_Click(object sender, EventArgs e)
        {
            grSearch.Visible = false;
        }

        private void BtnExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void DataGridCommon_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            btnSelect_Click(sender, e);
        }

        private void BtnSts_Click(object sender, EventArgs e)
        {
            try
            {
                if(txtYarn.Text != string.Empty && txtBeam.Text != string.Empty)
                {
                    SqlParameter[] sqlParameters = {
                        new SqlParameter("@BEAMNO",txtBeam.Tag)
                    };
                    DataSet data = db.GetDataWithParamMultiple(CommandType.StoredProcedure, "PROC_RPTBEAMSTATUS", sqlParameters, conn);
                    sfDataGrid1.DataSource = null;
                    sfDataGrid1.DataSource = data.Tables[0];
                    sfDataGrid1.AutoSizeColumnsMode = Syncfusion.WinForms.DataGrid.Enums.AutoSizeColumnsMode.AllCells;

                    sfDataGrid2.DataSource = null;
                    sfDataGrid2.DataSource = data.Tables[1];
                    sfDataGrid2.AutoSizeColumnsMode = Syncfusion.WinForms.DataGrid.Enums.AutoSizeColumnsMode.AllCells;
                }
                else
                {
                    MessageBox.Show("Select Yarn", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    txtYarn.Focus();
                    return;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void TxtYarn_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if(SelectedId == 0)
                {
                    bsyarn.Filter = string.Format("YarnName Like '%{0}%' Or  YarnShortName Like '%{1}%'", txtYarn.Text, txtYarn.Text);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void TxtBeam_Click(object sender, EventArgs e)
        {
            try
            {
                if(txtYarn.Text != string.Empty)
                {
                    string Query = @"Select a.Barcode,b.GeneralName as Beam from WarbBooking a 
                                    inner join GeneralM b on a.BeamUid = b.Uid and TypeM_uid = 15 Where ItemUid =" + txtYarn.Tag + "";
                    DataTable dt = db.GetDataWithoutParam(CommandType.Text, Query, conn);
                    bsBeam.DataSource = dt;
                    Point loc = FindLocation(txtBeam);
                    grSearch.Location = new Point(loc.X, loc.Y + 20);
                    LoadDataGrid(dt, 2);
                    grSearch.Visible = true;
                }
                else
                {

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void txtBeam_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (SelectedId == 0)
                {
                    bsBeam.Filter = string.Format("Barcode Like '%{0}%' Or  Beam Like '%{1}%'", txtBeam.Text, txtBeam.Text);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void FrmBeamStatus_Load(object sender, EventArgs e)
        {

        }
    }
}
