﻿namespace ACV
{
    partial class FrmInvoice
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        //private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        //protected override void Dispose(bool disposing)
        //{
        //    if (disposing && (components != null))
        //    {
        //        components.Dispose();
        //    }
        //    base.Dispose(disposing);
        //}

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmInvoice));
            this.Genpan = new System.Windows.Forms.Panel();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.txtscr8 = new System.Windows.Forms.TextBox();
            this.txtscr7 = new System.Windows.Forms.TextBox();
            this.label57 = new System.Windows.Forms.Label();
            this.dtpfnt = new System.Windows.Forms.DateTimePicker();
            this.txtscr6 = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.txtscr5 = new System.Windows.Forms.TextBox();
            this.txtscr4 = new System.Windows.Forms.TextBox();
            this.Txtscr3 = new System.Windows.Forms.TextBox();
            this.Txtscr2 = new System.Windows.Forms.TextBox();
            this.txtscr1 = new System.Windows.Forms.TextBox();
            this.HFGP = new System.Windows.Forms.DataGridView();
            this.panel1 = new System.Windows.Forms.Panel();
            this.lblno1 = new System.Windows.Forms.Label();
            this.lblno2 = new System.Windows.Forms.Label();
            this.flowLayoutPanel3 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel2 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.buttnnvfst = new System.Windows.Forms.Button();
            this.buttnnxtlft = new System.Windows.Forms.Button();
            this.btnfinnxt = new System.Windows.Forms.Button();
            this.buttrnxt = new System.Windows.Forms.Button();
            this.panadd = new System.Windows.Forms.Panel();
            this.txtchargessum = new System.Windows.Forms.TextBox();
            this.txtbasicval = new System.Windows.Forms.TextBox();
            this.txttax = new System.Windows.Forms.TextBox();
            this.txttotamt = new System.Windows.Forms.TextBox();
            this.label58 = new System.Windows.Forms.Label();
            this.button13 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.buttnext1 = new System.Windows.Forms.Button();
            this.chkact = new System.Windows.Forms.CheckBox();
            this.butcan = new System.Windows.Forms.Button();
            this.butedit = new System.Windows.Forms.Button();
            this.button6 = new System.Windows.Forms.Button();
            this.Editpan = new System.Windows.Forms.Panel();
            this.termspan = new System.Windows.Forms.Panel();
            this.HFGT = new System.Windows.Forms.DataGridView();
            this.txttermset = new System.Windows.Forms.TextBox();
            this.button16 = new System.Windows.Forms.Button();
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.label61 = new System.Windows.Forms.Label();
            this.HFG9 = new System.Windows.Forms.DataGridView();
            this.button15 = new System.Windows.Forms.Button();
            this.label56 = new System.Windows.Forms.Label();
            this.button10 = new System.Windows.Forms.Button();
            this.txttotc = new System.Windows.Forms.TextBox();
            this.label35 = new System.Windows.Forms.Label();
            this.label36 = new System.Windows.Forms.Label();
            this.label33 = new System.Windows.Forms.Label();
            this.txtaddcharge = new System.Windows.Forms.TextBox();
            this.btnadd = new System.Windows.Forms.Button();
            this.txtaddid = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label34 = new System.Windows.Forms.Label();
            this.HFGTAX = new System.Windows.Forms.DataGridView();
            this.button9 = new System.Windows.Forms.Button();
            this.HFGA = new System.Windows.Forms.DataGridView();
            this.txtremde = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.txtterms = new System.Windows.Forms.TextBox();
            this.Terms = new System.Windows.Forms.Label();
            this.txttermid = new System.Windows.Forms.TextBox();
            this.button7 = new System.Windows.Forms.Button();
            this.txtcoucharge = new System.Windows.Forms.TextBox();
            this.label60 = new System.Windows.Forms.Label();
            this.label59 = new System.Windows.Forms.Label();
            this.label54 = new System.Windows.Forms.Label();
            this.txtttot = new System.Windows.Forms.TextBox();
            this.txtigval = new System.Windows.Forms.TextBox();
            this.txtigstp = new System.Windows.Forms.TextBox();
            this.label52 = new System.Windows.Forms.Label();
            this.label53 = new System.Windows.Forms.Label();
            this.txttsgval = new System.Windows.Forms.TextBox();
            this.txtsgstp = new System.Windows.Forms.TextBox();
            this.label50 = new System.Windows.Forms.Label();
            this.label51 = new System.Windows.Forms.Label();
            this.txttcgval = new System.Windows.Forms.TextBox();
            this.txttcgstp = new System.Windows.Forms.TextBox();
            this.label49 = new System.Windows.Forms.Label();
            this.label48 = new System.Windows.Forms.Label();
            this.label47 = new System.Windows.Forms.Label();
            this.txttprdval = new System.Windows.Forms.TextBox();
            this.label46 = new System.Windows.Forms.Label();
            this.txttdisc = new System.Windows.Forms.TextBox();
            this.txttbval = new System.Windows.Forms.TextBox();
            this.label45 = new System.Windows.Forms.Label();
            this.label43 = new System.Windows.Forms.Label();
            this.txttdis = new System.Windows.Forms.TextBox();
            this.txtexcise = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.TxtNetAmt = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.TxtRoff = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.txtcharges = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.txtpadd2 = new System.Windows.Forms.RichTextBox();
            this.txtpadd1 = new System.Windows.Forms.RichTextBox();
            this.label41 = new System.Windows.Forms.Label();
            this.txtveh = new System.Windows.Forms.TextBox();
            this.label40 = new System.Windows.Forms.Label();
            this.label37 = new System.Windows.Forms.Label();
            this.txtplace = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.button8 = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.button4 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.txtamt = new System.Windows.Forms.TextBox();
            this.button2 = new System.Windows.Forms.Button();
            this.DTPDOCDT = new System.Windows.Forms.DateTimePicker();
            this.HFIT = new System.Windows.Forms.DataGridView();
            this.Dtpdt = new System.Windows.Forms.DateTimePicker();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.txtdcno = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.txtname = new System.Windows.Forms.TextBox();
            this.Phone = new System.Windows.Forms.Label();
            this.txtgrn = new System.Windows.Forms.TextBox();
            this.txtpuid = new System.Windows.Forms.TextBox();
            this.txtgrnid = new System.Windows.Forms.TextBox();
            this.txtdcid = new System.Windows.Forms.TextBox();
            this.txtrem = new System.Windows.Forms.RichTextBox();
            this.txttitemid = new System.Windows.Forms.TextBox();
            this.txtpluid = new System.Windows.Forms.TextBox();
            this.label44 = new System.Windows.Forms.Label();
            this.txtuom = new System.Windows.Forms.TextBox();
            this.Dtprem = new System.Windows.Forms.DateTimePicker();
            this.label39 = new System.Windows.Forms.Label();
            this.Dtppre = new System.Windows.Forms.DateTimePicker();
            this.label38 = new System.Windows.Forms.Label();
            this.pantax = new System.Windows.Forms.Panel();
            this.txttitem = new System.Windows.Forms.RichTextBox();
            this.label30 = new System.Windows.Forms.Label();
            this.txttqty = new System.Windows.Forms.TextBox();
            this.label29 = new System.Windows.Forms.Label();
            this.Txtrate = new System.Windows.Forms.TextBox();
            this.label28 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.txtbasic = new System.Windows.Forms.TextBox();
            this.label23 = new System.Windows.Forms.Label();
            this.button5 = new System.Windows.Forms.Button();
            this.txttotal = new System.Windows.Forms.TextBox();
            this.label24 = new System.Windows.Forms.Label();
            this.txtigcst = new System.Windows.Forms.TextBox();
            this.cboigst = new System.Windows.Forms.ComboBox();
            this.label22 = new System.Windows.Forms.Label();
            this.txtsgst = new System.Windows.Forms.TextBox();
            this.SGST = new System.Windows.Forms.ComboBox();
            this.label21 = new System.Windows.Forms.Label();
            this.txtcgst = new System.Windows.Forms.TextBox();
            this.cbocgst = new System.Windows.Forms.ComboBox();
            this.label20 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.txtper = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.txttaxable = new System.Windows.Forms.TextBox();
            this.txthidqty = new System.Windows.Forms.TextBox();
            this.txttempadd2 = new System.Windows.Forms.TextBox();
            this.txttempadd1 = new System.Windows.Forms.TextBox();
            this.txttgstval = new System.Windows.Forms.TextBox();
            this.txttgstp = new System.Windows.Forms.TextBox();
            this.button12 = new System.Windows.Forms.Button();
            this.buttnnxt = new System.Windows.Forms.Button();
            this.button11 = new System.Windows.Forms.Button();
            this.btnaddrcan = new System.Windows.Forms.Button();
            this.label55 = new System.Windows.Forms.Label();
            this.Txttot = new System.Windows.Forms.TextBox();
            this.txtot = new System.Windows.Forms.TextBox();
            this.label42 = new System.Windows.Forms.Label();
            this.txtnotes = new System.Windows.Forms.TextBox();
            this.label32 = new System.Windows.Forms.Label();
            this.txtbval = new System.Windows.Forms.TextBox();
            this.label31 = new System.Windows.Forms.Label();
            this.txtqty = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.txtitemname = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.txtprice = new System.Windows.Forms.TextBox();
            this.buttcusok = new System.Windows.Forms.Button();
            this.txtgst = new System.Windows.Forms.TextBox();
            this.txtite = new System.Windows.Forms.TextBox();
            this.txtqtyp = new System.Windows.Forms.TextBox();
            this.txtitemcode = new System.Windows.Forms.TextBox();
            this.txtbuid = new System.Windows.Forms.TextBox();
            this.txt100 = new System.Windows.Forms.TextBox();
            this.txtlisid = new System.Windows.Forms.TextBox();
            this.txtsaleorderno = new System.Windows.Forms.TextBox();
            this.txtsalesdate = new System.Windows.Forms.TextBox();
            this.txtorderno = new System.Windows.Forms.DateTimePicker();
            this.txttrans = new System.Windows.Forms.TextBox();
            this.txtchk = new System.Windows.Forms.TextBox();
            this.addipan = new System.Windows.Forms.Panel();
            this.label13 = new System.Windows.Forms.Label();
            this.txttotaddd = new System.Windows.Forms.TextBox();
            this.Taxpan = new System.Windows.Forms.Panel();
            this.button14 = new System.Windows.Forms.Button();
            this.btnsave = new System.Windows.Forms.Button();
            this.buttnfinbk = new System.Windows.Forms.Button();
            this.button17 = new System.Windows.Forms.Button();
            this.Genpan.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.HFGP)).BeginInit();
            this.panel1.SuspendLayout();
            this.panadd.SuspendLayout();
            this.Editpan.SuspendLayout();
            this.termspan.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.HFGT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.HFG9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.HFGTAX)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.HFGA)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.HFIT)).BeginInit();
            this.pantax.SuspendLayout();
            this.Taxpan.SuspendLayout();
            this.SuspendLayout();
            // 
            // Genpan
            // 
            this.Genpan.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.Genpan.Controls.Add(this.textBox1);
            this.Genpan.Controls.Add(this.txtscr8);
            this.Genpan.Controls.Add(this.txtscr7);
            this.Genpan.Controls.Add(this.label57);
            this.Genpan.Controls.Add(this.dtpfnt);
            this.Genpan.Controls.Add(this.txtscr6);
            this.Genpan.Controls.Add(this.label7);
            this.Genpan.Controls.Add(this.txtscr5);
            this.Genpan.Controls.Add(this.txtscr4);
            this.Genpan.Controls.Add(this.Txtscr3);
            this.Genpan.Controls.Add(this.Txtscr2);
            this.Genpan.Controls.Add(this.txtscr1);
            this.Genpan.Controls.Add(this.HFGP);
            this.Genpan.Controls.Add(this.panel1);
            this.Genpan.Controls.Add(this.buttnnvfst);
            this.Genpan.Controls.Add(this.buttnnxtlft);
            this.Genpan.Controls.Add(this.btnfinnxt);
            this.Genpan.Controls.Add(this.buttrnxt);
            this.Genpan.Location = new System.Drawing.Point(-1, -1);
            this.Genpan.Margin = new System.Windows.Forms.Padding(4);
            this.Genpan.Name = "Genpan";
            this.Genpan.Size = new System.Drawing.Size(1178, 466);
            this.Genpan.TabIndex = 187;
            this.Genpan.Paint += new System.Windows.Forms.PaintEventHandler(this.Genpan_Paint);
            // 
            // textBox1
            // 
            this.textBox1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox1.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox1.Location = new System.Drawing.Point(848, 43);
            this.textBox1.Margin = new System.Windows.Forms.Padding(5);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(100, 22);
            this.textBox1.TabIndex = 226;
            this.textBox1.TextChanged += new System.EventHandler(this.textBox1_TextChanged_1);
            // 
            // txtscr8
            // 
            this.txtscr8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtscr8.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtscr8.Location = new System.Drawing.Point(947, 43);
            this.txtscr8.Margin = new System.Windows.Forms.Padding(5);
            this.txtscr8.Name = "txtscr8";
            this.txtscr8.Size = new System.Drawing.Size(100, 22);
            this.txtscr8.TabIndex = 225;
            this.txtscr8.TextChanged += new System.EventHandler(this.txtscr8_TextChanged);
            // 
            // txtscr7
            // 
            this.txtscr7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtscr7.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtscr7.Location = new System.Drawing.Point(1048, 43);
            this.txtscr7.Margin = new System.Windows.Forms.Padding(5);
            this.txtscr7.Name = "txtscr7";
            this.txtscr7.Size = new System.Drawing.Size(100, 22);
            this.txtscr7.TabIndex = 224;
            this.txtscr7.TextChanged += new System.EventHandler(this.txtscr7_TextChanged);
            // 
            // label57
            // 
            this.label57.AutoSize = true;
            this.label57.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label57.Location = new System.Drawing.Point(442, 17);
            this.label57.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label57.Name = "label57";
            this.label57.Size = new System.Drawing.Size(44, 19);
            this.label57.TabIndex = 223;
            this.label57.Text = "Date";
            this.label57.Click += new System.EventHandler(this.label57_Click);
            // 
            // dtpfnt
            // 
            this.dtpfnt.CustomFormat = "MMM/yyyy";
            this.dtpfnt.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpfnt.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpfnt.Location = new System.Drawing.Point(489, 14);
            this.dtpfnt.Margin = new System.Windows.Forms.Padding(4);
            this.dtpfnt.Name = "dtpfnt";
            this.dtpfnt.Size = new System.Drawing.Size(104, 22);
            this.dtpfnt.TabIndex = 222;
            this.dtpfnt.Value = new System.DateTime(2017, 7, 4, 0, 0, 0, 0);
            this.dtpfnt.ValueChanged += new System.EventHandler(this.dtpfnt_ValueChanged);
            // 
            // txtscr6
            // 
            this.txtscr6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtscr6.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtscr6.Location = new System.Drawing.Point(750, 43);
            this.txtscr6.Margin = new System.Windows.Forms.Padding(5);
            this.txtscr6.Name = "txtscr6";
            this.txtscr6.Size = new System.Drawing.Size(100, 22);
            this.txtscr6.TabIndex = 202;
            this.txtscr6.TextChanged += new System.EventHandler(this.txtscr6_TextChanged);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(2, 14);
            this.label7.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(110, 19);
            this.label7.TabIndex = 201;
            this.label7.Text = "Sales Invoice";
            // 
            // txtscr5
            // 
            this.txtscr5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtscr5.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtscr5.Location = new System.Drawing.Point(178, 43);
            this.txtscr5.Margin = new System.Windows.Forms.Padding(5);
            this.txtscr5.Name = "txtscr5";
            this.txtscr5.Size = new System.Drawing.Size(573, 22);
            this.txtscr5.TabIndex = 90;
            this.txtscr5.TextChanged += new System.EventHandler(this.txtscr5_TextChanged);
            // 
            // txtscr4
            // 
            this.txtscr4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtscr4.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtscr4.Location = new System.Drawing.Point(267, 43);
            this.txtscr4.Margin = new System.Windows.Forms.Padding(5);
            this.txtscr4.Name = "txtscr4";
            this.txtscr4.Size = new System.Drawing.Size(90, 22);
            this.txtscr4.TabIndex = 100;
            this.txtscr4.TextChanged += new System.EventHandler(this.txtscr4_TextChanged);
            // 
            // Txtscr3
            // 
            this.Txtscr3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Txtscr3.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txtscr3.Location = new System.Drawing.Point(178, 43);
            this.Txtscr3.Margin = new System.Windows.Forms.Padding(5);
            this.Txtscr3.Name = "Txtscr3";
            this.Txtscr3.Size = new System.Drawing.Size(90, 22);
            this.Txtscr3.TabIndex = 88;
            this.Txtscr3.TextChanged += new System.EventHandler(this.Txtscr3_TextChanged);
            // 
            // Txtscr2
            // 
            this.Txtscr2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Txtscr2.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txtscr2.Location = new System.Drawing.Point(89, 43);
            this.Txtscr2.Margin = new System.Windows.Forms.Padding(5);
            this.Txtscr2.Name = "Txtscr2";
            this.Txtscr2.Size = new System.Drawing.Size(90, 22);
            this.Txtscr2.TabIndex = 87;
            this.Txtscr2.TextChanged += new System.EventHandler(this.Txtscr2_TextChanged);
            // 
            // txtscr1
            // 
            this.txtscr1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtscr1.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtscr1.Location = new System.Drawing.Point(1, 43);
            this.txtscr1.Margin = new System.Windows.Forms.Padding(5);
            this.txtscr1.Name = "txtscr1";
            this.txtscr1.Size = new System.Drawing.Size(90, 22);
            this.txtscr1.TabIndex = 1;
            this.txtscr1.TextChanged += new System.EventHandler(this.txtscr1_TextChanged);
            // 
            // HFGP
            // 
            this.HFGP.BackgroundColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.HFGP.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.HFGP.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnF2;
            this.HFGP.Location = new System.Drawing.Point(1, 74);
            this.HFGP.Margin = new System.Windows.Forms.Padding(4);
            this.HFGP.Name = "HFGP";
            this.HFGP.ReadOnly = true;
            this.HFGP.Size = new System.Drawing.Size(1173, 392);
            this.HFGP.TabIndex = 3;
            this.HFGP.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.HFGP_CellContentClick);
            this.HFGP.CellMouseUp += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.HFGP_CellMouseUp);
            this.HFGP.KeyUp += new System.Windows.Forms.KeyEventHandler(this.HFGP_KeyUp);
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel1.Controls.Add(this.lblno1);
            this.panel1.Controls.Add(this.lblno2);
            this.panel1.Controls.Add(this.flowLayoutPanel3);
            this.panel1.Controls.Add(this.flowLayoutPanel2);
            this.panel1.Controls.Add(this.flowLayoutPanel1);
            this.panel1.Location = new System.Drawing.Point(66, 390);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(60, 30);
            this.panel1.TabIndex = 214;
            // 
            // lblno1
            // 
            this.lblno1.AutoSize = true;
            this.lblno1.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblno1.ForeColor = System.Drawing.Color.Black;
            this.lblno1.Location = new System.Drawing.Point(4, 5);
            this.lblno1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblno1.Name = "lblno1";
            this.lblno1.Size = new System.Drawing.Size(15, 16);
            this.lblno1.TabIndex = 163;
            this.lblno1.Text = "1";
            // 
            // lblno2
            // 
            this.lblno2.AutoSize = true;
            this.lblno2.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblno2.ForeColor = System.Drawing.Color.Black;
            this.lblno2.Location = new System.Drawing.Point(27, 5);
            this.lblno2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblno2.Name = "lblno2";
            this.lblno2.Size = new System.Drawing.Size(29, 16);
            this.lblno2.TabIndex = 162;
            this.lblno2.Text = "of 1";
            // 
            // flowLayoutPanel3
            // 
            this.flowLayoutPanel3.Location = new System.Drawing.Point(61, 30);
            this.flowLayoutPanel3.Name = "flowLayoutPanel3";
            this.flowLayoutPanel3.Size = new System.Drawing.Size(200, 100);
            this.flowLayoutPanel3.TabIndex = 2;
            // 
            // flowLayoutPanel2
            // 
            this.flowLayoutPanel2.Location = new System.Drawing.Point(38, 35);
            this.flowLayoutPanel2.Name = "flowLayoutPanel2";
            this.flowLayoutPanel2.Size = new System.Drawing.Size(200, 100);
            this.flowLayoutPanel2.TabIndex = 1;
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.Location = new System.Drawing.Point(62, -129);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(200, 100);
            this.flowLayoutPanel1.TabIndex = 0;
            // 
            // buttnnvfst
            // 
            this.buttnnvfst.BackColor = System.Drawing.Color.White;
            this.buttnnvfst.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.buttnnvfst.FlatAppearance.BorderSize = 0;
            this.buttnnvfst.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttnnvfst.Image = ((System.Drawing.Image)(resources.GetObject("buttnnvfst.Image")));
            this.buttnnvfst.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttnnvfst.Location = new System.Drawing.Point(19, 390);
            this.buttnnvfst.Name = "buttnnvfst";
            this.buttnnvfst.Size = new System.Drawing.Size(19, 31);
            this.buttnnvfst.TabIndex = 213;
            this.buttnnvfst.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.buttnnvfst.UseVisualStyleBackColor = false;
            this.buttnnvfst.Click += new System.EventHandler(this.buttnnvfst_Click);
            // 
            // buttnnxtlft
            // 
            this.buttnnxtlft.BackColor = System.Drawing.Color.White;
            this.buttnnxtlft.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.buttnnxtlft.FlatAppearance.BorderSize = 0;
            this.buttnnxtlft.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttnnxtlft.Image = ((System.Drawing.Image)(resources.GetObject("buttnnxtlft.Image")));
            this.buttnnxtlft.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttnnxtlft.Location = new System.Drawing.Point(44, 390);
            this.buttnnxtlft.Name = "buttnnxtlft";
            this.buttnnxtlft.Size = new System.Drawing.Size(18, 31);
            this.buttnnxtlft.TabIndex = 212;
            this.buttnnxtlft.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.buttnnxtlft.UseVisualStyleBackColor = false;
            this.buttnnxtlft.Click += new System.EventHandler(this.buttnnxtlft_Click);
            // 
            // btnfinnxt
            // 
            this.btnfinnxt.BackColor = System.Drawing.Color.White;
            this.btnfinnxt.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.btnfinnxt.FlatAppearance.BorderSize = 0;
            this.btnfinnxt.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnfinnxt.Image = ((System.Drawing.Image)(resources.GetObject("btnfinnxt.Image")));
            this.btnfinnxt.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnfinnxt.Location = new System.Drawing.Point(156, 390);
            this.btnfinnxt.Name = "btnfinnxt";
            this.btnfinnxt.Size = new System.Drawing.Size(19, 31);
            this.btnfinnxt.TabIndex = 211;
            this.btnfinnxt.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnfinnxt.UseVisualStyleBackColor = false;
            this.btnfinnxt.Click += new System.EventHandler(this.btnfinnxt_Click);
            // 
            // buttrnxt
            // 
            this.buttrnxt.BackColor = System.Drawing.Color.White;
            this.buttrnxt.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.buttrnxt.FlatAppearance.BorderSize = 0;
            this.buttrnxt.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttrnxt.Image = ((System.Drawing.Image)(resources.GetObject("buttrnxt.Image")));
            this.buttrnxt.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttrnxt.Location = new System.Drawing.Point(132, 390);
            this.buttrnxt.Name = "buttrnxt";
            this.buttrnxt.Size = new System.Drawing.Size(18, 31);
            this.buttrnxt.TabIndex = 210;
            this.buttrnxt.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.buttrnxt.UseVisualStyleBackColor = false;
            this.buttrnxt.Click += new System.EventHandler(this.buttrnxt_Click);
            // 
            // panadd
            // 
            this.panadd.BackColor = System.Drawing.Color.White;
            this.panadd.Controls.Add(this.txtchargessum);
            this.panadd.Controls.Add(this.txtbasicval);
            this.panadd.Controls.Add(this.txttax);
            this.panadd.Controls.Add(this.txttotamt);
            this.panadd.Controls.Add(this.label58);
            this.panadd.Controls.Add(this.button13);
            this.panadd.Controls.Add(this.button1);
            this.panadd.Controls.Add(this.buttnext1);
            this.panadd.Controls.Add(this.chkact);
            this.panadd.Controls.Add(this.butcan);
            this.panadd.Controls.Add(this.butedit);
            this.panadd.Controls.Add(this.button6);
            this.panadd.Location = new System.Drawing.Point(-1, 466);
            this.panadd.Name = "panadd";
            this.panadd.Size = new System.Drawing.Size(1177, 33);
            this.panadd.TabIndex = 235;
            this.panadd.Paint += new System.Windows.Forms.PaintEventHandler(this.panadd_Paint);
            // 
            // txtchargessum
            // 
            this.txtchargessum.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtchargessum.Enabled = false;
            this.txtchargessum.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtchargessum.Location = new System.Drawing.Point(852, 3);
            this.txtchargessum.Margin = new System.Windows.Forms.Padding(5);
            this.txtchargessum.Name = "txtchargessum";
            this.txtchargessum.Size = new System.Drawing.Size(100, 25);
            this.txtchargessum.TabIndex = 235;
            this.txtchargessum.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txtbasicval
            // 
            this.txtbasicval.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtbasicval.Enabled = false;
            this.txtbasicval.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtbasicval.Location = new System.Drawing.Point(753, 3);
            this.txtbasicval.Margin = new System.Windows.Forms.Padding(5);
            this.txtbasicval.Name = "txtbasicval";
            this.txtbasicval.Size = new System.Drawing.Size(100, 25);
            this.txtbasicval.TabIndex = 233;
            this.txtbasicval.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txttax
            // 
            this.txttax.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txttax.Enabled = false;
            this.txttax.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txttax.Location = new System.Drawing.Point(951, 3);
            this.txttax.Margin = new System.Windows.Forms.Padding(5);
            this.txttax.Name = "txttax";
            this.txttax.Size = new System.Drawing.Size(100, 25);
            this.txttax.TabIndex = 234;
            this.txttax.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txttotamt
            // 
            this.txttotamt.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txttotamt.Enabled = false;
            this.txttotamt.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txttotamt.Location = new System.Drawing.Point(1050, 3);
            this.txttotamt.Margin = new System.Windows.Forms.Padding(5);
            this.txttotamt.Name = "txttotamt";
            this.txttotamt.Size = new System.Drawing.Size(100, 25);
            this.txttotamt.TabIndex = 232;
            this.txttotamt.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label58
            // 
            this.label58.AutoSize = true;
            this.label58.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label58.Location = new System.Drawing.Point(697, 7);
            this.label58.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label58.Name = "label58";
            this.label58.Size = new System.Drawing.Size(46, 19);
            this.label58.TabIndex = 231;
            this.label58.Text = "Total";
            this.label58.Click += new System.EventHandler(this.label58_Click);
            // 
            // button13
            // 
            this.button13.BackColor = System.Drawing.Color.White;
            this.button13.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button13.Image = ((System.Drawing.Image)(resources.GetObject("button13.Image")));
            this.button13.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button13.Location = new System.Drawing.Point(378, 2);
            this.button13.Name = "button13";
            this.button13.Size = new System.Drawing.Size(105, 30);
            this.button13.TabIndex = 225;
            this.button13.Text = "Print Preview";
            this.button13.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button13.UseVisualStyleBackColor = false;
            this.button13.Click += new System.EventHandler(this.button13_Click);
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.White;
            this.button1.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.Image = ((System.Drawing.Image)(resources.GetObject("button1.Image")));
            this.button1.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button1.Location = new System.Drawing.Point(313, 2);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(65, 30);
            this.button1.TabIndex = 216;
            this.button1.Text = "Print";
            this.button1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click_1);
            // 
            // buttnext1
            // 
            this.buttnext1.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.buttnext1.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttnext1.Image = ((System.Drawing.Image)(resources.GetObject("buttnext1.Image")));
            this.buttnext1.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttnext1.Location = new System.Drawing.Point(483, 2);
            this.buttnext1.Name = "buttnext1";
            this.buttnext1.Size = new System.Drawing.Size(54, 30);
            this.buttnext1.TabIndex = 208;
            this.buttnext1.Text = "Exit";
            this.buttnext1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.buttnext1.UseVisualStyleBackColor = false;
            this.buttnext1.Click += new System.EventHandler(this.buttnext1_Click);
            // 
            // chkact
            // 
            this.chkact.AutoSize = true;
            this.chkact.BackColor = System.Drawing.Color.White;
            this.chkact.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkact.Location = new System.Drawing.Point(4, 8);
            this.chkact.Name = "chkact";
            this.chkact.Size = new System.Drawing.Size(57, 19);
            this.chkact.TabIndex = 187;
            this.chkact.Text = "Active";
            this.chkact.UseVisualStyleBackColor = false;
            this.chkact.CheckedChanged += new System.EventHandler(this.chkact_CheckedChanged);
            // 
            // butcan
            // 
            this.butcan.BackColor = System.Drawing.Color.White;
            this.butcan.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.butcan.Image = ((System.Drawing.Image)(resources.GetObject("butcan.Image")));
            this.butcan.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.butcan.Location = new System.Drawing.Point(202, 2);
            this.butcan.Name = "butcan";
            this.butcan.Size = new System.Drawing.Size(110, 30);
            this.butcan.TabIndex = 186;
            this.butcan.Text = "Invoice Cancel";
            this.butcan.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.butcan.UseVisualStyleBackColor = false;
            this.butcan.Click += new System.EventHandler(this.butcan_Click);
            // 
            // butedit
            // 
            this.butedit.BackColor = System.Drawing.Color.White;
            this.butedit.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.butedit.Image = ((System.Drawing.Image)(resources.GetObject("butedit.Image")));
            this.butedit.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.butedit.Location = new System.Drawing.Point(148, 2);
            this.butedit.Name = "butedit";
            this.butedit.Size = new System.Drawing.Size(55, 30);
            this.butedit.TabIndex = 185;
            this.butedit.Text = "Edit";
            this.butedit.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.butedit.UseVisualStyleBackColor = false;
            this.butedit.Click += new System.EventHandler(this.butedit_Click);
            // 
            // button6
            // 
            this.button6.BackColor = System.Drawing.Color.White;
            this.button6.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.button6.FlatAppearance.BorderSize = 0;
            this.button6.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button6.Image = ((System.Drawing.Image)(resources.GetObject("button6.Image")));
            this.button6.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button6.Location = new System.Drawing.Point(67, 2);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(82, 30);
            this.button6.TabIndex = 184;
            this.button6.Text = "Add new";
            this.button6.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button6.UseVisualStyleBackColor = false;
            this.button6.Click += new System.EventHandler(this.button6_Click_1);
            // 
            // Editpan
            // 
            this.Editpan.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.Editpan.Controls.Add(this.termspan);
            this.Editpan.Controls.Add(this.txtcoucharge);
            this.Editpan.Controls.Add(this.label60);
            this.Editpan.Controls.Add(this.label59);
            this.Editpan.Controls.Add(this.label54);
            this.Editpan.Controls.Add(this.txtttot);
            this.Editpan.Controls.Add(this.txtigval);
            this.Editpan.Controls.Add(this.txtigstp);
            this.Editpan.Controls.Add(this.label52);
            this.Editpan.Controls.Add(this.label53);
            this.Editpan.Controls.Add(this.txttsgval);
            this.Editpan.Controls.Add(this.txtsgstp);
            this.Editpan.Controls.Add(this.label50);
            this.Editpan.Controls.Add(this.label51);
            this.Editpan.Controls.Add(this.txttcgval);
            this.Editpan.Controls.Add(this.txttcgstp);
            this.Editpan.Controls.Add(this.label49);
            this.Editpan.Controls.Add(this.label48);
            this.Editpan.Controls.Add(this.label47);
            this.Editpan.Controls.Add(this.txttprdval);
            this.Editpan.Controls.Add(this.label46);
            this.Editpan.Controls.Add(this.txttdisc);
            this.Editpan.Controls.Add(this.txttbval);
            this.Editpan.Controls.Add(this.label45);
            this.Editpan.Controls.Add(this.label43);
            this.Editpan.Controls.Add(this.txttdis);
            this.Editpan.Controls.Add(this.txtexcise);
            this.Editpan.Controls.Add(this.label17);
            this.Editpan.Controls.Add(this.TxtNetAmt);
            this.Editpan.Controls.Add(this.label16);
            this.Editpan.Controls.Add(this.TxtRoff);
            this.Editpan.Controls.Add(this.label8);
            this.Editpan.Controls.Add(this.txtcharges);
            this.Editpan.Controls.Add(this.label12);
            this.Editpan.Controls.Add(this.txtpadd2);
            this.Editpan.Controls.Add(this.txtpadd1);
            this.Editpan.Controls.Add(this.label41);
            this.Editpan.Controls.Add(this.txtveh);
            this.Editpan.Controls.Add(this.label40);
            this.Editpan.Controls.Add(this.label37);
            this.Editpan.Controls.Add(this.txtplace);
            this.Editpan.Controls.Add(this.label4);
            this.Editpan.Controls.Add(this.button8);
            this.Editpan.Controls.Add(this.label1);
            this.Editpan.Controls.Add(this.button4);
            this.Editpan.Controls.Add(this.button3);
            this.Editpan.Controls.Add(this.txtamt);
            this.Editpan.Controls.Add(this.button2);
            this.Editpan.Controls.Add(this.DTPDOCDT);
            this.Editpan.Controls.Add(this.HFIT);
            this.Editpan.Controls.Add(this.Dtpdt);
            this.Editpan.Controls.Add(this.label6);
            this.Editpan.Controls.Add(this.label5);
            this.Editpan.Controls.Add(this.txtdcno);
            this.Editpan.Controls.Add(this.label2);
            this.Editpan.Controls.Add(this.label3);
            this.Editpan.Controls.Add(this.txtname);
            this.Editpan.Controls.Add(this.Phone);
            this.Editpan.Controls.Add(this.txtgrn);
            this.Editpan.Controls.Add(this.txtpuid);
            this.Editpan.Controls.Add(this.txtgrnid);
            this.Editpan.Controls.Add(this.txtdcid);
            this.Editpan.Controls.Add(this.txtrem);
            this.Editpan.Controls.Add(this.txttitemid);
            this.Editpan.Controls.Add(this.txtpluid);
            this.Editpan.Controls.Add(this.label44);
            this.Editpan.Controls.Add(this.txtuom);
            this.Editpan.Controls.Add(this.Dtprem);
            this.Editpan.Controls.Add(this.label39);
            this.Editpan.Controls.Add(this.Dtppre);
            this.Editpan.Controls.Add(this.label38);
            this.Editpan.Controls.Add(this.pantax);
            this.Editpan.Controls.Add(this.txttempadd2);
            this.Editpan.Controls.Add(this.txttempadd1);
            this.Editpan.Controls.Add(this.txttgstval);
            this.Editpan.Controls.Add(this.txttgstp);
            this.Editpan.Controls.Add(this.button12);
            this.Editpan.Controls.Add(this.buttnnxt);
            this.Editpan.Controls.Add(this.button11);
            this.Editpan.Controls.Add(this.btnaddrcan);
            this.Editpan.Controls.Add(this.label55);
            this.Editpan.Controls.Add(this.Txttot);
            this.Editpan.Controls.Add(this.txtot);
            this.Editpan.Controls.Add(this.label42);
            this.Editpan.Controls.Add(this.txtnotes);
            this.Editpan.Controls.Add(this.label32);
            this.Editpan.Controls.Add(this.txtbval);
            this.Editpan.Controls.Add(this.label31);
            this.Editpan.Controls.Add(this.txtqty);
            this.Editpan.Controls.Add(this.label15);
            this.Editpan.Controls.Add(this.txtitemname);
            this.Editpan.Controls.Add(this.label14);
            this.Editpan.Controls.Add(this.txtprice);
            this.Editpan.Controls.Add(this.buttcusok);
            this.Editpan.Controls.Add(this.txtgst);
            this.Editpan.Controls.Add(this.txtite);
            this.Editpan.Controls.Add(this.txtqtyp);
            this.Editpan.Controls.Add(this.txtitemcode);
            this.Editpan.Controls.Add(this.txtbuid);
            this.Editpan.Controls.Add(this.txt100);
            this.Editpan.Controls.Add(this.txtlisid);
            this.Editpan.Controls.Add(this.txtsaleorderno);
            this.Editpan.Controls.Add(this.txtsalesdate);
            this.Editpan.Controls.Add(this.txtorderno);
            this.Editpan.Controls.Add(this.txttrans);
            this.Editpan.Controls.Add(this.txtchk);
            this.Editpan.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Editpan.Location = new System.Drawing.Point(1, 0);
            this.Editpan.Margin = new System.Windows.Forms.Padding(4);
            this.Editpan.Name = "Editpan";
            this.Editpan.Size = new System.Drawing.Size(1175, 464);
            this.Editpan.TabIndex = 243;
            this.Editpan.ContextMenuStripChanged += new System.EventHandler(this.Editpan_ContextMenuStripChanged);
            this.Editpan.Click += new System.EventHandler(this.Editpan_Click);
            this.Editpan.Paint += new System.Windows.Forms.PaintEventHandler(this.Editpan_Paint);
            // 
            // termspan
            // 
            this.termspan.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.termspan.Controls.Add(this.HFGT);
            this.termspan.Controls.Add(this.txttermset);
            this.termspan.Controls.Add(this.button16);
            this.termspan.Controls.Add(this.checkBox1);
            this.termspan.Controls.Add(this.label61);
            this.termspan.Controls.Add(this.HFG9);
            this.termspan.Controls.Add(this.button15);
            this.termspan.Controls.Add(this.label56);
            this.termspan.Controls.Add(this.button10);
            this.termspan.Controls.Add(this.txttotc);
            this.termspan.Controls.Add(this.label35);
            this.termspan.Controls.Add(this.label36);
            this.termspan.Controls.Add(this.label33);
            this.termspan.Controls.Add(this.txtaddcharge);
            this.termspan.Controls.Add(this.btnadd);
            this.termspan.Controls.Add(this.txtaddid);
            this.termspan.Controls.Add(this.label10);
            this.termspan.Controls.Add(this.label34);
            this.termspan.Controls.Add(this.HFGTAX);
            this.termspan.Controls.Add(this.button9);
            this.termspan.Controls.Add(this.HFGA);
            this.termspan.Controls.Add(this.txtremde);
            this.termspan.Controls.Add(this.label11);
            this.termspan.Controls.Add(this.txtterms);
            this.termspan.Controls.Add(this.Terms);
            this.termspan.Controls.Add(this.txttermid);
            this.termspan.Controls.Add(this.button7);
            this.termspan.Location = new System.Drawing.Point(6, 53);
            this.termspan.Name = "termspan";
            this.termspan.Size = new System.Drawing.Size(10, 431);
            this.termspan.TabIndex = 334;
            // 
            // HFGT
            // 
            this.HFGT.BackgroundColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.HFGT.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.HFGT.Location = new System.Drawing.Point(518, 90);
            this.HFGT.Name = "HFGT";
            this.HFGT.Size = new System.Drawing.Size(374, 296);
            this.HFGT.TabIndex = 317;
            // 
            // txttermset
            // 
            this.txttermset.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txttermset.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txttermset.Location = new System.Drawing.Point(14, 63);
            this.txttermset.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.txttermset.Name = "txttermset";
            this.txttermset.Size = new System.Drawing.Size(349, 22);
            this.txttermset.TabIndex = 316;
            // 
            // button16
            // 
            this.button16.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.button16.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button16.Image = global::ACV.Properties.Resources.arrow_35x35;
            this.button16.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.button16.Location = new System.Drawing.Point(440, 188);
            this.button16.Name = "button16";
            this.button16.Size = new System.Drawing.Size(39, 39);
            this.button16.TabIndex = 315;
            this.button16.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button16.UseVisualStyleBackColor = false;
            this.button16.Click += new System.EventHandler(this.button16_Click);
            // 
            // checkBox1
            // 
            this.checkBox1.AutoSize = true;
            this.checkBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBox1.Location = new System.Drawing.Point(413, 130);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new System.Drawing.Size(93, 20);
            this.checkBox1.TabIndex = 314;
            this.checkBox1.Text = "Select All";
            this.checkBox1.CheckedChanged += new System.EventHandler(this.checkBox1_CheckedChanged);
            this.checkBox1.Click += new System.EventHandler(this.checkBox1_Click);
            // 
            // label61
            // 
            this.label61.AutoSize = true;
            this.label61.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label61.Location = new System.Drawing.Point(11, 42);
            this.label61.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label61.Name = "label61";
            this.label61.Size = new System.Drawing.Size(68, 16);
            this.label61.TabIndex = 313;
            this.label61.Text = "TermsList";
            // 
            // HFG9
            // 
            this.HFG9.BackgroundColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.HFG9.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.HFG9.Location = new System.Drawing.Point(14, 90);
            this.HFG9.Name = "HFG9";
            this.HFG9.Size = new System.Drawing.Size(391, 296);
            this.HFG9.TabIndex = 312;
            // 
            // button15
            // 
            this.button15.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.button15.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button15.Image = ((System.Drawing.Image)(resources.GetObject("button15.Image")));
            this.button15.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button15.Location = new System.Drawing.Point(588, 305);
            this.button15.Name = "button15";
            this.button15.Size = new System.Drawing.Size(54, 30);
            this.button15.TabIndex = 311;
            this.button15.Text = "Exit";
            this.button15.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button15.UseVisualStyleBackColor = false;
            this.button15.Click += new System.EventHandler(this.button15_Click_1);
            // 
            // label56
            // 
            this.label56.AutoSize = true;
            this.label56.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label56.Location = new System.Drawing.Point(404, 12);
            this.label56.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label56.Name = "label56";
            this.label56.Size = new System.Drawing.Size(130, 16);
            this.label56.TabIndex = 310;
            this.label56.Text = "Terms && Conditions";
            // 
            // button10
            // 
            this.button10.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button10.Location = new System.Drawing.Point(342, 205);
            this.button10.Name = "button10";
            this.button10.Size = new System.Drawing.Size(15, 26);
            this.button10.TabIndex = 308;
            this.button10.Text = "Terms& Conditions";
            this.button10.UseVisualStyleBackColor = true;
            // 
            // txttotc
            // 
            this.txttotc.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txttotc.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txttotc.Location = new System.Drawing.Point(323, 212);
            this.txttotc.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.txttotc.Name = "txttotc";
            this.txttotc.Size = new System.Drawing.Size(79, 22);
            this.txttotc.TabIndex = 306;
            this.txttotc.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label35.Location = new System.Drawing.Point(333, 212);
            this.label35.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(66, 16);
            this.label35.TabIndex = 307;
            this.label35.Text = "HSN/SAC";
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label36.Location = new System.Drawing.Point(310, 183);
            this.label36.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(95, 16);
            this.label36.TabIndex = 303;
            this.label36.Text = "Total Charges";
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label33.Location = new System.Drawing.Point(264, 202);
            this.label33.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(129, 16);
            this.label33.TabIndex = 305;
            this.label33.Text = "Additional Charges";
            // 
            // txtaddcharge
            // 
            this.txtaddcharge.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtaddcharge.Location = new System.Drawing.Point(307, 222);
            this.txtaddcharge.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.txtaddcharge.Name = "txtaddcharge";
            this.txtaddcharge.Size = new System.Drawing.Size(92, 22);
            this.txtaddcharge.TabIndex = 304;
            // 
            // btnadd
            // 
            this.btnadd.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnadd.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnadd.Image = global::ACV.Properties.Resources.ok1_25x25;
            this.btnadd.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.btnadd.Location = new System.Drawing.Point(313, 202);
            this.btnadd.Name = "btnadd";
            this.btnadd.Size = new System.Drawing.Size(34, 32);
            this.btnadd.TabIndex = 300;
            this.btnadd.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnadd.UseVisualStyleBackColor = false;
            // 
            // txtaddid
            // 
            this.txtaddid.Location = new System.Drawing.Point(365, 205);
            this.txtaddid.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.txtaddid.Name = "txtaddid";
            this.txtaddid.Size = new System.Drawing.Size(25, 22);
            this.txtaddid.TabIndex = 301;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(333, 196);
            this.label10.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(57, 16);
            this.label10.TabIndex = 302;
            this.label10.Text = "Amount";
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label34.Location = new System.Drawing.Point(189, 214);
            this.label34.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(145, 16);
            this.label34.TabIndex = 295;
            this.label34.Text = "Terms and Conditions";
            // 
            // HFGTAX
            // 
            this.HFGTAX.BackgroundColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.HFGTAX.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.HFGTAX.Location = new System.Drawing.Point(173, 209);
            this.HFGTAX.Name = "HFGTAX";
            this.HFGTAX.Size = new System.Drawing.Size(220, 26);
            this.HFGTAX.TabIndex = 296;
            // 
            // button9
            // 
            this.button9.BackColor = System.Drawing.SystemColors.Control;
            this.button9.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button9.Location = new System.Drawing.Point(363, 222);
            this.button9.Name = "button9";
            this.button9.Size = new System.Drawing.Size(19, 25);
            this.button9.TabIndex = 297;
            this.button9.Text = "Additional Charges";
            this.button9.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button9.UseVisualStyleBackColor = false;
            // 
            // HFGA
            // 
            this.HFGA.BackgroundColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.HFGA.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.HFGA.Location = new System.Drawing.Point(146, 165);
            this.HFGA.Name = "HFGA";
            this.HFGA.Size = new System.Drawing.Size(223, 109);
            this.HFGA.TabIndex = 271;
            // 
            // txtremde
            // 
            this.txtremde.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtremde.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtremde.Location = new System.Drawing.Point(652, 258);
            this.txtremde.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.txtremde.Name = "txtremde";
            this.txtremde.Size = new System.Drawing.Size(236, 22);
            this.txtremde.TabIndex = 230;
            this.txtremde.TextChanged += new System.EventHandler(this.txtremde_TextChanged);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(761, 231);
            this.label11.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(121, 16);
            this.label11.TabIndex = 229;
            this.label11.Text = "Terms Description";
            // 
            // txtterms
            // 
            this.txtterms.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtterms.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtterms.Location = new System.Drawing.Point(518, 63);
            this.txtterms.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.txtterms.Name = "txtterms";
            this.txtterms.Size = new System.Drawing.Size(364, 22);
            this.txtterms.TabIndex = 228;
            // 
            // Terms
            // 
            this.Terms.AutoSize = true;
            this.Terms.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Terms.Location = new System.Drawing.Point(516, 43);
            this.Terms.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.Terms.Name = "Terms";
            this.Terms.Size = new System.Drawing.Size(46, 16);
            this.Terms.TabIndex = 227;
            this.Terms.Text = "Terms";
            // 
            // txttermid
            // 
            this.txttermid.Location = new System.Drawing.Point(256, 207);
            this.txttermid.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.txttermid.Name = "txttermid";
            this.txttermid.Size = new System.Drawing.Size(25, 22);
            this.txttermid.TabIndex = 232;
            // 
            // button7
            // 
            this.button7.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.button7.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button7.Image = global::ACV.Properties.Resources.ok1_25x25;
            this.button7.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.button7.Location = new System.Drawing.Point(890, 58);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(34, 32);
            this.button7.TabIndex = 239;
            this.button7.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button7.UseVisualStyleBackColor = false;
            this.button7.Click += new System.EventHandler(this.button7_Click_2);
            // 
            // txtcoucharge
            // 
            this.txtcoucharge.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtcoucharge.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtcoucharge.Location = new System.Drawing.Point(1053, 160);
            this.txtcoucharge.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.txtcoucharge.Name = "txtcoucharge";
            this.txtcoucharge.Size = new System.Drawing.Size(113, 22);
            this.txtcoucharge.TabIndex = 323;
            this.txtcoucharge.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtcoucharge.TextChanged += new System.EventHandler(this.txtcoucharge_TextChanged);
            // 
            // label60
            // 
            this.label60.AutoSize = true;
            this.label60.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label60.Location = new System.Drawing.Point(938, 192);
            this.label60.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label60.Name = "label60";
            this.label60.Size = new System.Drawing.Size(111, 16);
            this.label60.TabIndex = 322;
            this.label60.Text = "Courier Charges";
            // 
            // label59
            // 
            this.label59.AutoSize = true;
            this.label59.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label59.Location = new System.Drawing.Point(128, 99);
            this.label59.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label59.Name = "label59";
            this.label59.Size = new System.Drawing.Size(60, 16);
            this.label59.TabIndex = 320;
            this.label59.Text = "PO.Date";
            // 
            // label54
            // 
            this.label54.AutoSize = true;
            this.label54.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label54.Location = new System.Drawing.Point(964, 366);
            this.label54.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label54.Name = "label54";
            this.label54.Size = new System.Drawing.Size(79, 16);
            this.label54.TabIndex = 317;
            this.label54.Text = "Total Value";
            // 
            // txtttot
            // 
            this.txtttot.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtttot.Enabled = false;
            this.txtttot.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtttot.Location = new System.Drawing.Point(1053, 360);
            this.txtttot.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.txtttot.Name = "txtttot";
            this.txtttot.Size = new System.Drawing.Size(112, 22);
            this.txtttot.TabIndex = 316;
            this.txtttot.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txtigval
            // 
            this.txtigval.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtigval.Enabled = false;
            this.txtigval.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtigval.Location = new System.Drawing.Point(1053, 326);
            this.txtigval.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.txtigval.Name = "txtigval";
            this.txtigval.Size = new System.Drawing.Size(113, 22);
            this.txtigval.TabIndex = 315;
            this.txtigval.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txtigstp
            // 
            this.txtigstp.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtigstp.Enabled = false;
            this.txtigstp.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtigstp.Location = new System.Drawing.Point(997, 326);
            this.txtigstp.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.txtigstp.Name = "txtigstp";
            this.txtigstp.Size = new System.Drawing.Size(37, 22);
            this.txtigstp.TabIndex = 314;
            this.txtigstp.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label52
            // 
            this.label52.AutoSize = true;
            this.label52.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label52.Location = new System.Drawing.Point(1032, 328);
            this.label52.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label52.Name = "label52";
            this.label52.Size = new System.Drawing.Size(18, 16);
            this.label52.TabIndex = 313;
            this.label52.Text = "%";
            // 
            // label53
            // 
            this.label53.AutoSize = true;
            this.label53.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label53.Location = new System.Drawing.Point(952, 328);
            this.label53.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label53.Name = "label53";
            this.label53.Size = new System.Drawing.Size(39, 16);
            this.label53.TabIndex = 312;
            this.label53.Text = "IGST";
            // 
            // txttsgval
            // 
            this.txttsgval.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txttsgval.Enabled = false;
            this.txttsgval.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txttsgval.Location = new System.Drawing.Point(1053, 292);
            this.txttsgval.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.txttsgval.Name = "txttsgval";
            this.txttsgval.Size = new System.Drawing.Size(113, 22);
            this.txttsgval.TabIndex = 311;
            this.txttsgval.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txtsgstp
            // 
            this.txtsgstp.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtsgstp.Enabled = false;
            this.txtsgstp.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtsgstp.Location = new System.Drawing.Point(997, 292);
            this.txtsgstp.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.txtsgstp.Name = "txtsgstp";
            this.txtsgstp.Size = new System.Drawing.Size(37, 22);
            this.txtsgstp.TabIndex = 310;
            this.txtsgstp.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label50
            // 
            this.label50.AutoSize = true;
            this.label50.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label50.Location = new System.Drawing.Point(1032, 294);
            this.label50.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label50.Name = "label50";
            this.label50.Size = new System.Drawing.Size(18, 16);
            this.label50.TabIndex = 309;
            this.label50.Text = "%";
            // 
            // label51
            // 
            this.label51.AutoSize = true;
            this.label51.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label51.Location = new System.Drawing.Point(952, 294);
            this.label51.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label51.Name = "label51";
            this.label51.Size = new System.Drawing.Size(44, 16);
            this.label51.TabIndex = 308;
            this.label51.Text = "SGST";
            // 
            // txttcgval
            // 
            this.txttcgval.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txttcgval.Enabled = false;
            this.txttcgval.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txttcgval.Location = new System.Drawing.Point(1053, 258);
            this.txttcgval.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.txttcgval.Name = "txttcgval";
            this.txttcgval.Size = new System.Drawing.Size(113, 22);
            this.txttcgval.TabIndex = 307;
            this.txttcgval.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txttcgstp
            // 
            this.txttcgstp.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txttcgstp.Enabled = false;
            this.txttcgstp.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txttcgstp.Location = new System.Drawing.Point(997, 258);
            this.txttcgstp.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.txttcgstp.Name = "txttcgstp";
            this.txttcgstp.Size = new System.Drawing.Size(37, 22);
            this.txttcgstp.TabIndex = 306;
            this.txttcgstp.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label49
            // 
            this.label49.AutoSize = true;
            this.label49.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label49.Location = new System.Drawing.Point(1032, 260);
            this.label49.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label49.Name = "label49";
            this.label49.Size = new System.Drawing.Size(18, 16);
            this.label49.TabIndex = 305;
            this.label49.Text = "%";
            // 
            // label48
            // 
            this.label48.AutoSize = true;
            this.label48.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label48.Location = new System.Drawing.Point(952, 260);
            this.label48.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label48.Name = "label48";
            this.label48.Size = new System.Drawing.Size(44, 16);
            this.label48.TabIndex = 304;
            this.label48.Text = "CGST";
            // 
            // label47
            // 
            this.label47.AutoSize = true;
            this.label47.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label47.Location = new System.Drawing.Point(952, 128);
            this.label47.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label47.Name = "label47";
            this.label47.Size = new System.Drawing.Size(97, 16);
            this.label47.TabIndex = 303;
            this.label47.Text = "Product Value";
            // 
            // txttprdval
            // 
            this.txttprdval.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txttprdval.Enabled = false;
            this.txttprdval.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txttprdval.Location = new System.Drawing.Point(1053, 126);
            this.txttprdval.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.txttprdval.Name = "txttprdval";
            this.txttprdval.Size = new System.Drawing.Size(113, 22);
            this.txttprdval.TabIndex = 302;
            this.txttprdval.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label46
            // 
            this.label46.AutoSize = true;
            this.label46.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label46.Location = new System.Drawing.Point(947, 96);
            this.label46.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label46.Name = "label46";
            this.label46.Size = new System.Drawing.Size(102, 16);
            this.label46.TabIndex = 301;
            this.label46.Text = "Discount Value";
            // 
            // txttdisc
            // 
            this.txttdisc.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txttdisc.Enabled = false;
            this.txttdisc.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txttdisc.Location = new System.Drawing.Point(1054, 94);
            this.txttdisc.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.txttdisc.Name = "txttdisc";
            this.txttdisc.Size = new System.Drawing.Size(113, 22);
            this.txttdisc.TabIndex = 300;
            this.txttdisc.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txttbval
            // 
            this.txttbval.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txttbval.Enabled = false;
            this.txttbval.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txttbval.Location = new System.Drawing.Point(1054, 33);
            this.txttbval.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.txttbval.Name = "txttbval";
            this.txttbval.Size = new System.Drawing.Size(113, 22);
            this.txttbval.TabIndex = 299;
            this.txttbval.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label45
            // 
            this.label45.AutoSize = true;
            this.label45.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label45.Location = new System.Drawing.Point(967, 35);
            this.label45.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(82, 16);
            this.label45.TabIndex = 298;
            this.label45.Text = "Basic Value";
            // 
            // label43
            // 
            this.label43.AutoSize = true;
            this.label43.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label43.Location = new System.Drawing.Point(973, 65);
            this.label43.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(76, 16);
            this.label43.TabIndex = 297;
            this.label43.Text = "Discount %";
            // 
            // txttdis
            // 
            this.txttdis.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txttdis.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txttdis.Location = new System.Drawing.Point(1054, 63);
            this.txttdis.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.txttdis.Name = "txttdis";
            this.txttdis.Size = new System.Drawing.Size(113, 22);
            this.txttdis.TabIndex = 296;
            this.txttdis.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txttdis.TextChanged += new System.EventHandler(this.txttdis_TextChanged_1);
            // 
            // txtexcise
            // 
            this.txtexcise.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtexcise.Enabled = false;
            this.txtexcise.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtexcise.Location = new System.Drawing.Point(1053, 224);
            this.txtexcise.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.txtexcise.Name = "txtexcise";
            this.txtexcise.Size = new System.Drawing.Size(113, 22);
            this.txtexcise.TabIndex = 295;
            this.txtexcise.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.Location = new System.Drawing.Point(974, 429);
            this.label17.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(69, 16);
            this.label17.TabIndex = 294;
            this.label17.Text = "Net Value";
            // 
            // TxtNetAmt
            // 
            this.TxtNetAmt.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.TxtNetAmt.Enabled = false;
            this.TxtNetAmt.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtNetAmt.ForeColor = System.Drawing.Color.Red;
            this.TxtNetAmt.Location = new System.Drawing.Point(1053, 426);
            this.TxtNetAmt.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.TxtNetAmt.Name = "TxtNetAmt";
            this.TxtNetAmt.Size = new System.Drawing.Size(113, 25);
            this.TxtNetAmt.TabIndex = 293;
            this.TxtNetAmt.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(1004, 396);
            this.label16.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(39, 16);
            this.label16.TabIndex = 292;
            this.label16.Text = "R.Off";
            // 
            // TxtRoff
            // 
            this.TxtRoff.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.TxtRoff.Enabled = false;
            this.TxtRoff.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtRoff.Location = new System.Drawing.Point(1053, 394);
            this.TxtRoff.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.TxtRoff.Name = "TxtRoff";
            this.TxtRoff.Size = new System.Drawing.Size(112, 22);
            this.TxtRoff.TabIndex = 291;
            this.TxtRoff.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(950, 226);
            this.label8.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(99, 16);
            this.label8.TabIndex = 290;
            this.label8.Text = "Taxable Value";
            // 
            // txtcharges
            // 
            this.txtcharges.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtcharges.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtcharges.Location = new System.Drawing.Point(1053, 190);
            this.txtcharges.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.txtcharges.Name = "txtcharges";
            this.txtcharges.Size = new System.Drawing.Size(113, 22);
            this.txtcharges.TabIndex = 289;
            this.txtcharges.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtcharges.TextChanged += new System.EventHandler(this.txtcharges_TextChanged_1);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(948, 159);
            this.label12.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(102, 16);
            this.label12.TabIndex = 288;
            this.label12.Text = " P && F Charges";
            // 
            // txtpadd2
            // 
            this.txtpadd2.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtpadd2.Location = new System.Drawing.Point(585, 51);
            this.txtpadd2.MaxLength = 100;
            this.txtpadd2.Name = "txtpadd2";
            this.txtpadd2.Size = new System.Drawing.Size(328, 84);
            this.txtpadd2.TabIndex = 268;
            this.txtpadd2.Text = "";
            // 
            // txtpadd1
            // 
            this.txtpadd1.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtpadd1.Location = new System.Drawing.Point(250, 51);
            this.txtpadd1.MaxLength = 100;
            this.txtpadd1.Name = "txtpadd1";
            this.txtpadd1.Size = new System.Drawing.Size(330, 85);
            this.txtpadd1.TabIndex = 267;
            this.txtpadd1.Text = "";
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label41.Location = new System.Drawing.Point(583, 145);
            this.label41.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(121, 16);
            this.label41.TabIndex = 259;
            this.label41.Text = "Mode of Despatch";
            // 
            // txtveh
            // 
            this.txtveh.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtveh.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtveh.Location = new System.Drawing.Point(586, 165);
            this.txtveh.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.txtveh.Name = "txtveh";
            this.txtveh.Size = new System.Drawing.Size(325, 22);
            this.txtveh.TabIndex = 228;
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label40.Location = new System.Drawing.Point(6, 99);
            this.label40.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(50, 16);
            this.label40.TabIndex = 257;
            this.label40.Text = "P.O.No";
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label37.Location = new System.Drawing.Point(583, 11);
            this.label37.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(108, 16);
            this.label37.TabIndex = 250;
            this.label37.Text = "Place of Supply";
            // 
            // txtplace
            // 
            this.txtplace.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtplace.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtplace.Location = new System.Drawing.Point(586, 30);
            this.txtplace.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.txtplace.MaxLength = 100;
            this.txtplace.Name = "txtplace";
            this.txtplace.Size = new System.Drawing.Size(327, 22);
            this.txtplace.TabIndex = 249;
            this.txtplace.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtplace_KeyDown_1);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(836, 497);
            this.label4.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(46, 18);
            this.label4.TabIndex = 238;
            this.label4.Text = "Total";
            // 
            // button8
            // 
            this.button8.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.button8.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button8.Image = global::ACV.Properties.Resources.save;
            this.button8.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button8.Location = new System.Drawing.Point(819, 32);
            this.button8.Name = "button8";
            this.button8.Size = new System.Drawing.Size(90, 38);
            this.button8.TabIndex = 106;
            this.button8.Text = "Save";
            this.button8.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button8.UseVisualStyleBackColor = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(6, 144);
            this.label1.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(63, 16);
            this.label1.TabIndex = 233;
            this.label1.Text = "Remarks";
            // 
            // button4
            // 
            this.button4.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.button4.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button4.Image = ((System.Drawing.Image)(resources.GetObject("button4.Image")));
            this.button4.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button4.Location = new System.Drawing.Point(820, 79);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(91, 38);
            this.button4.TabIndex = 101;
            this.button4.Text = "Back";
            this.button4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button4.UseVisualStyleBackColor = false;
            // 
            // button3
            // 
            this.button3.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.button3.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button3.Image = ((System.Drawing.Image)(resources.GetObject("button3.Image")));
            this.button3.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button3.Location = new System.Drawing.Point(1012, 549);
            this.button3.Margin = new System.Windows.Forms.Padding(4);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(124, 47);
            this.button3.TabIndex = 227;
            this.button3.Text = "Back";
            this.button3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button3.UseVisualStyleBackColor = false;
            // 
            // txtamt
            // 
            this.txtamt.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtamt.Location = new System.Drawing.Point(775, 559);
            this.txtamt.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.txtamt.Name = "txtamt";
            this.txtamt.Size = new System.Drawing.Size(161, 22);
            this.txtamt.TabIndex = 226;
            // 
            // button2
            // 
            this.button2.BackColor = System.Drawing.Color.White;
            this.button2.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button2.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button2.Location = new System.Drawing.Point(1144, 550);
            this.button2.Margin = new System.Windows.Forms.Padding(4);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(113, 46);
            this.button2.TabIndex = 224;
            this.button2.Text = "Tax";
            this.button2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button2.UseVisualStyleBackColor = false;
            // 
            // DTPDOCDT
            // 
            this.DTPDOCDT.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DTPDOCDT.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.DTPDOCDT.Location = new System.Drawing.Point(130, 30);
            this.DTPDOCDT.Margin = new System.Windows.Forms.Padding(4);
            this.DTPDOCDT.Name = "DTPDOCDT";
            this.DTPDOCDT.Size = new System.Drawing.Size(113, 22);
            this.DTPDOCDT.TabIndex = 221;
            // 
            // HFIT
            // 
            this.HFIT.BackgroundColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.HFIT.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.HFIT.Location = new System.Drawing.Point(8, 268);
            this.HFIT.Margin = new System.Windows.Forms.Padding(4);
            this.HFIT.Name = "HFIT";
            this.HFIT.ReadOnly = true;
            this.HFIT.Size = new System.Drawing.Size(933, 184);
            this.HFIT.TabIndex = 214;
            this.HFIT.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.HFIT_CellContentClick);
            this.HFIT.KeyDown += new System.Windows.Forms.KeyEventHandler(this.HFIT_KeyDown);
            // 
            // Dtpdt
            // 
            this.Dtpdt.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Dtpdt.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.Dtpdt.Location = new System.Drawing.Point(131, 73);
            this.Dtpdt.Margin = new System.Windows.Forms.Padding(4);
            this.Dtpdt.Name = "Dtpdt";
            this.Dtpdt.Size = new System.Drawing.Size(113, 22);
            this.Dtpdt.TabIndex = 224;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(127, 10);
            this.label6.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(87, 16);
            this.label6.TabIndex = 204;
            this.label6.Text = "Invoice.Date";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(4, 55);
            this.label5.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(45, 16);
            this.label5.TabIndex = 203;
            this.label5.Text = "Dc.No";
            // 
            // txtdcno
            // 
            this.txtdcno.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtdcno.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtdcno.Location = new System.Drawing.Point(8, 73);
            this.txtdcno.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.txtdcno.Name = "txtdcno";
            this.txtdcno.Size = new System.Drawing.Size(119, 22);
            this.txtdcno.TabIndex = 223;
            this.txtdcno.TextChanged += new System.EventHandler(this.txtdcno_TextChanged_1);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(128, 53);
            this.label2.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(57, 16);
            this.label2.TabIndex = 201;
            this.label2.Text = "Dc.Date";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(248, 10);
            this.label3.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(82, 16);
            this.label3.TabIndex = 195;
            this.label3.Text = "Party Name";
            // 
            // txtname
            // 
            this.txtname.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtname.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtname.Location = new System.Drawing.Point(250, 30);
            this.txtname.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.txtname.MaxLength = 100;
            this.txtname.Name = "txtname";
            this.txtname.Size = new System.Drawing.Size(331, 22);
            this.txtname.TabIndex = 222;
            this.txtname.MouseClick += new System.Windows.Forms.MouseEventHandler(this.txtname_MouseClick);
            this.txtname.TextChanged += new System.EventHandler(this.txtname_TextChanged);
            this.txtname.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtname_KeyDown);
            this.txtname.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtname_KeyPress);
            this.txtname.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txtname_KeyUp);
            this.txtname.MouseDown += new System.Windows.Forms.MouseEventHandler(this.txtname_MouseDown);
            this.txtname.MouseEnter += new System.EventHandler(this.txtname_MouseEnter);
            // 
            // Phone
            // 
            this.Phone.AutoSize = true;
            this.Phone.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Phone.Location = new System.Drawing.Point(3, 10);
            this.Phone.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.Phone.Name = "Phone";
            this.Phone.Size = new System.Drawing.Size(75, 16);
            this.Phone.TabIndex = 199;
            this.Phone.Text = "Invoice.No";
            // 
            // txtgrn
            // 
            this.txtgrn.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtgrn.Enabled = false;
            this.txtgrn.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtgrn.Location = new System.Drawing.Point(6, 30);
            this.txtgrn.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.txtgrn.Name = "txtgrn";
            this.txtgrn.Size = new System.Drawing.Size(119, 22);
            this.txtgrn.TabIndex = 198;
            // 
            // txtpuid
            // 
            this.txtpuid.Location = new System.Drawing.Point(64, 291);
            this.txtpuid.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.txtpuid.Name = "txtpuid";
            this.txtpuid.Size = new System.Drawing.Size(87, 22);
            this.txtpuid.TabIndex = 217;
            this.txtpuid.TextChanged += new System.EventHandler(this.txtpuid_TextChanged);
            // 
            // txtgrnid
            // 
            this.txtgrnid.Location = new System.Drawing.Point(344, 332);
            this.txtgrnid.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.txtgrnid.Name = "txtgrnid";
            this.txtgrnid.Size = new System.Drawing.Size(28, 22);
            this.txtgrnid.TabIndex = 220;
            // 
            // txtdcid
            // 
            this.txtdcid.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtdcid.Location = new System.Drawing.Point(651, 336);
            this.txtdcid.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.txtdcid.Name = "txtdcid";
            this.txtdcid.Size = new System.Drawing.Size(23, 22);
            this.txtdcid.TabIndex = 105;
            this.txtdcid.TextChanged += new System.EventHandler(this.txtdcid_TextChanged);
            // 
            // txtrem
            // 
            this.txtrem.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtrem.Location = new System.Drawing.Point(7, 164);
            this.txtrem.MaxLength = 100;
            this.txtrem.Name = "txtrem";
            this.txtrem.Size = new System.Drawing.Size(574, 46);
            this.txtrem.TabIndex = 227;
            this.txtrem.Text = "";
            this.txtrem.TextChanged += new System.EventHandler(this.txtrem_TextChanged);
            // 
            // txttitemid
            // 
            this.txttitemid.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txttitemid.Location = new System.Drawing.Point(586, 343);
            this.txttitemid.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.txttitemid.Name = "txttitemid";
            this.txttitemid.Size = new System.Drawing.Size(35, 22);
            this.txttitemid.TabIndex = 336;
            this.txttitemid.TextChanged += new System.EventHandler(this.txttitemid_TextChanged);
            // 
            // txtpluid
            // 
            this.txtpluid.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtpluid.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtpluid.Location = new System.Drawing.Point(677, 30);
            this.txtpluid.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.txtpluid.Name = "txtpluid";
            this.txtpluid.Size = new System.Drawing.Size(34, 22);
            this.txtpluid.TabIndex = 251;
            this.txtpluid.TextChanged += new System.EventHandler(this.txtpluid_TextChanged);
            // 
            // label44
            // 
            this.label44.AutoSize = true;
            this.label44.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label44.Location = new System.Drawing.Point(321, 321);
            this.label44.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(36, 16);
            this.label44.TabIndex = 263;
            this.label44.Text = "UoM";
            // 
            // txtuom
            // 
            this.txtuom.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtuom.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtuom.Location = new System.Drawing.Point(178, 329);
            this.txtuom.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.txtuom.Name = "txtuom";
            this.txtuom.Size = new System.Drawing.Size(59, 22);
            this.txtuom.TabIndex = 262;
            // 
            // Dtprem
            // 
            this.Dtprem.CustomFormat = "dd-MMM-yyyy  hh:mm tt";
            this.Dtprem.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Dtprem.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.Dtprem.Location = new System.Drawing.Point(414, 353);
            this.Dtprem.Margin = new System.Windows.Forms.Padding(4);
            this.Dtprem.Name = "Dtprem";
            this.Dtprem.Size = new System.Drawing.Size(176, 22);
            this.Dtprem.TabIndex = 255;
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label39.Location = new System.Drawing.Point(412, 301);
            this.label39.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(110, 16);
            this.label39.TabIndex = 254;
            this.label39.Text = "D&&T of Removal";
            // 
            // Dtppre
            // 
            this.Dtppre.AllowDrop = true;
            this.Dtppre.CustomFormat = "dd-MMM-yyyy  hh:mm tt";
            this.Dtppre.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Dtppre.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.Dtppre.Location = new System.Drawing.Point(144, 341);
            this.Dtppre.Margin = new System.Windows.Forms.Padding(4);
            this.Dtppre.Name = "Dtppre";
            this.Dtppre.Size = new System.Drawing.Size(175, 22);
            this.Dtppre.TabIndex = 253;
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label38.Location = new System.Drawing.Point(250, 317);
            this.label38.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(129, 16);
            this.label38.TabIndex = 252;
            this.label38.Text = "D&&T of Preparation";
            // 
            // pantax
            // 
            this.pantax.BackColor = System.Drawing.Color.Silver;
            this.pantax.Controls.Add(this.txttitem);
            this.pantax.Controls.Add(this.label30);
            this.pantax.Controls.Add(this.txttqty);
            this.pantax.Controls.Add(this.label29);
            this.pantax.Controls.Add(this.Txtrate);
            this.pantax.Controls.Add(this.label28);
            this.pantax.Controls.Add(this.label27);
            this.pantax.Controls.Add(this.label26);
            this.pantax.Controls.Add(this.label25);
            this.pantax.Controls.Add(this.txtbasic);
            this.pantax.Controls.Add(this.label23);
            this.pantax.Controls.Add(this.button5);
            this.pantax.Controls.Add(this.txttotal);
            this.pantax.Controls.Add(this.label24);
            this.pantax.Controls.Add(this.txtigcst);
            this.pantax.Controls.Add(this.cboigst);
            this.pantax.Controls.Add(this.label22);
            this.pantax.Controls.Add(this.txtsgst);
            this.pantax.Controls.Add(this.SGST);
            this.pantax.Controls.Add(this.label21);
            this.pantax.Controls.Add(this.txtcgst);
            this.pantax.Controls.Add(this.cbocgst);
            this.pantax.Controls.Add(this.label20);
            this.pantax.Controls.Add(this.label19);
            this.pantax.Controls.Add(this.textBox2);
            this.pantax.Controls.Add(this.label18);
            this.pantax.Controls.Add(this.txtper);
            this.pantax.Controls.Add(this.label9);
            this.pantax.Controls.Add(this.txttaxable);
            this.pantax.Controls.Add(this.txthidqty);
            this.pantax.Location = new System.Drawing.Point(514, 293);
            this.pantax.Name = "pantax";
            this.pantax.Size = new System.Drawing.Size(68, 10);
            this.pantax.TabIndex = 232;
            this.pantax.Visible = false;
            // 
            // txttitem
            // 
            this.txttitem.Enabled = false;
            this.txttitem.Location = new System.Drawing.Point(8, 36);
            this.txttitem.Name = "txttitem";
            this.txttitem.Size = new System.Drawing.Size(315, 51);
            this.txttitem.TabIndex = 247;
            this.txttitem.Text = "";
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label30.Location = new System.Drawing.Point(5, 16);
            this.label30.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(40, 18);
            this.label30.TabIndex = 246;
            this.label30.Text = "Item";
            // 
            // txttqty
            // 
            this.txttqty.Location = new System.Drawing.Point(216, 102);
            this.txttqty.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.txttqty.Name = "txttqty";
            this.txttqty.Size = new System.Drawing.Size(88, 22);
            this.txttqty.TabIndex = 244;
            this.txttqty.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label29.Location = new System.Drawing.Point(26, 102);
            this.label29.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(34, 18);
            this.label29.TabIndex = 243;
            this.label29.Text = "Qty";
            // 
            // Txtrate
            // 
            this.Txtrate.Enabled = false;
            this.Txtrate.Location = new System.Drawing.Point(216, 136);
            this.Txtrate.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.Txtrate.Name = "Txtrate";
            this.Txtrate.Size = new System.Drawing.Size(88, 22);
            this.Txtrate.TabIndex = 242;
            this.Txtrate.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label28.Location = new System.Drawing.Point(26, 136);
            this.label28.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(43, 18);
            this.label28.TabIndex = 241;
            this.label28.Text = "Rate";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label27.Location = new System.Drawing.Point(181, 352);
            this.label27.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(22, 18);
            this.label27.TabIndex = 240;
            this.label27.Text = "%";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label26.Location = new System.Drawing.Point(181, 314);
            this.label26.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(22, 18);
            this.label26.TabIndex = 239;
            this.label26.Text = "%";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label25.Location = new System.Drawing.Point(181, 277);
            this.label25.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(22, 18);
            this.label25.TabIndex = 238;
            this.label25.Text = "%";
            // 
            // txtbasic
            // 
            this.txtbasic.Enabled = false;
            this.txtbasic.Location = new System.Drawing.Point(216, 170);
            this.txtbasic.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.txtbasic.Name = "txtbasic";
            this.txtbasic.Size = new System.Drawing.Size(88, 22);
            this.txtbasic.TabIndex = 237;
            this.txtbasic.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.Location = new System.Drawing.Point(26, 170);
            this.label23.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(96, 18);
            this.label23.TabIndex = 236;
            this.label23.Text = "Basic Value";
            // 
            // button5
            // 
            this.button5.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.button5.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button5.Image = global::ACV.Properties.Resources.Add;
            this.button5.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button5.Location = new System.Drawing.Point(120, 425);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(83, 41);
            this.button5.TabIndex = 235;
            this.button5.Text = "Ok";
            this.button5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button5.UseVisualStyleBackColor = false;
            // 
            // txttotal
            // 
            this.txttotal.Enabled = false;
            this.txttotal.Location = new System.Drawing.Point(216, 386);
            this.txttotal.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.txttotal.Name = "txttotal";
            this.txttotal.Size = new System.Drawing.Size(88, 22);
            this.txttotal.TabIndex = 228;
            this.txttotal.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.Location = new System.Drawing.Point(25, 388);
            this.label24.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(92, 18);
            this.label24.TabIndex = 227;
            this.label24.Text = "Total Value";
            // 
            // txtigcst
            // 
            this.txtigcst.Enabled = false;
            this.txtigcst.Location = new System.Drawing.Point(216, 352);
            this.txtigcst.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.txtigcst.Name = "txtigcst";
            this.txtigcst.Size = new System.Drawing.Size(88, 22);
            this.txtigcst.TabIndex = 225;
            this.txtigcst.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // cboigst
            // 
            this.cboigst.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboigst.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboigst.FormattingEnabled = true;
            this.cboigst.Location = new System.Drawing.Point(137, 350);
            this.cboigst.Margin = new System.Windows.Forms.Padding(4);
            this.cboigst.Name = "cboigst";
            this.cboigst.Size = new System.Drawing.Size(39, 24);
            this.cboigst.TabIndex = 224;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.Location = new System.Drawing.Point(26, 352);
            this.label22.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(46, 18);
            this.label22.TabIndex = 223;
            this.label22.Text = "IGST";
            // 
            // txtsgst
            // 
            this.txtsgst.Enabled = false;
            this.txtsgst.Location = new System.Drawing.Point(216, 313);
            this.txtsgst.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.txtsgst.Name = "txtsgst";
            this.txtsgst.Size = new System.Drawing.Size(88, 22);
            this.txtsgst.TabIndex = 222;
            this.txtsgst.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // SGST
            // 
            this.SGST.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.SGST.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.SGST.FormattingEnabled = true;
            this.SGST.Location = new System.Drawing.Point(137, 311);
            this.SGST.Margin = new System.Windows.Forms.Padding(4);
            this.SGST.Name = "SGST";
            this.SGST.Size = new System.Drawing.Size(39, 24);
            this.SGST.TabIndex = 221;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.Location = new System.Drawing.Point(26, 313);
            this.label21.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(53, 18);
            this.label21.TabIndex = 220;
            this.label21.Text = "SGST";
            // 
            // txtcgst
            // 
            this.txtcgst.Enabled = false;
            this.txtcgst.Location = new System.Drawing.Point(216, 276);
            this.txtcgst.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.txtcgst.Name = "txtcgst";
            this.txtcgst.Size = new System.Drawing.Size(88, 22);
            this.txtcgst.TabIndex = 219;
            this.txtcgst.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // cbocgst
            // 
            this.cbocgst.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cbocgst.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cbocgst.FormattingEnabled = true;
            this.cbocgst.Location = new System.Drawing.Point(137, 274);
            this.cbocgst.Margin = new System.Windows.Forms.Padding(4);
            this.cbocgst.Name = "cbocgst";
            this.cbocgst.Size = new System.Drawing.Size(39, 24);
            this.cbocgst.TabIndex = 218;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.Location = new System.Drawing.Point(26, 276);
            this.label20.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(54, 18);
            this.label20.TabIndex = 217;
            this.label20.Text = "CGST";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.Location = new System.Drawing.Point(25, 241);
            this.label19.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(112, 18);
            this.label19.TabIndex = 216;
            this.label19.Text = "Taxable Value";
            // 
            // textBox2
            // 
            this.textBox2.Enabled = false;
            this.textBox2.Location = new System.Drawing.Point(216, 204);
            this.textBox2.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(88, 22);
            this.textBox2.TabIndex = 215;
            this.textBox2.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.Location = new System.Drawing.Point(181, 205);
            this.label18.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(22, 18);
            this.label18.TabIndex = 214;
            this.label18.Text = "%";
            // 
            // txtper
            // 
            this.txtper.Location = new System.Drawing.Point(137, 204);
            this.txtper.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.txtper.Name = "txtper";
            this.txtper.Size = new System.Drawing.Size(36, 22);
            this.txtper.TabIndex = 205;
            this.txtper.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(25, 205);
            this.label9.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(62, 16);
            this.label9.TabIndex = 204;
            this.label9.Text = "Discount";
            // 
            // txttaxable
            // 
            this.txttaxable.Enabled = false;
            this.txttaxable.Location = new System.Drawing.Point(216, 240);
            this.txttaxable.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.txttaxable.Name = "txttaxable";
            this.txttaxable.Size = new System.Drawing.Size(88, 22);
            this.txttaxable.TabIndex = 200;
            this.txttaxable.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txthidqty
            // 
            this.txthidqty.Location = new System.Drawing.Point(216, 101);
            this.txthidqty.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.txthidqty.Name = "txthidqty";
            this.txthidqty.Size = new System.Drawing.Size(36, 22);
            this.txthidqty.TabIndex = 245;
            // 
            // txttempadd2
            // 
            this.txttempadd2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txttempadd2.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txttempadd2.Location = new System.Drawing.Point(467, 75);
            this.txttempadd2.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.txttempadd2.Name = "txttempadd2";
            this.txttempadd2.Size = new System.Drawing.Size(45, 22);
            this.txttempadd2.TabIndex = 270;
            // 
            // txttempadd1
            // 
            this.txttempadd1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txttempadd1.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txttempadd1.Location = new System.Drawing.Point(414, 75);
            this.txttempadd1.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.txttempadd1.Name = "txttempadd1";
            this.txttempadd1.Size = new System.Drawing.Size(45, 22);
            this.txttempadd1.TabIndex = 269;
            // 
            // txttgstval
            // 
            this.txttgstval.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txttgstval.Enabled = false;
            this.txttgstval.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txttgstval.Location = new System.Drawing.Point(784, 88);
            this.txttgstval.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.txttgstval.Name = "txttgstval";
            this.txttgstval.Size = new System.Drawing.Size(36, 22);
            this.txttgstval.TabIndex = 319;
            this.txttgstval.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txttgstp
            // 
            this.txttgstp.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txttgstp.Enabled = false;
            this.txttgstp.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txttgstp.Location = new System.Drawing.Point(741, 88);
            this.txttgstp.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.txttgstp.Name = "txttgstp";
            this.txttgstp.Size = new System.Drawing.Size(36, 22);
            this.txttgstp.TabIndex = 318;
            this.txttgstp.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // button12
            // 
            this.button12.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.button12.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button12.Image = ((System.Drawing.Image)(resources.GetObject("button12.Image")));
            this.button12.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button12.Location = new System.Drawing.Point(809, 332);
            this.button12.Margin = new System.Windows.Forms.Padding(4);
            this.button12.Name = "button12";
            this.button12.Size = new System.Drawing.Size(60, 30);
            this.button12.TabIndex = 237;
            this.button12.Text = "Back";
            this.button12.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button12.UseVisualStyleBackColor = false;
            this.button12.Click += new System.EventHandler(this.button12_Click_2);
            // 
            // buttnnxt
            // 
            this.buttnnxt.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.buttnnxt.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttnnxt.Image = ((System.Drawing.Image)(resources.GetObject("buttnnxt.Image")));
            this.buttnnxt.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttnnxt.Location = new System.Drawing.Point(677, 321);
            this.buttnnxt.Margin = new System.Windows.Forms.Padding(4);
            this.buttnnxt.Name = "buttnnxt";
            this.buttnnxt.Size = new System.Drawing.Size(73, 30);
            this.buttnnxt.TabIndex = 239;
            this.buttnnxt.Text = "Next";
            this.buttnnxt.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.buttnnxt.UseVisualStyleBackColor = false;
            this.buttnnxt.Click += new System.EventHandler(this.buttnnxt_Click);
            // 
            // button11
            // 
            this.button11.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.button11.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button11.Image = ((System.Drawing.Image)(resources.GetObject("button11.Image")));
            this.button11.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button11.Location = new System.Drawing.Point(796, 321);
            this.button11.Margin = new System.Windows.Forms.Padding(4);
            this.button11.Name = "button11";
            this.button11.Size = new System.Drawing.Size(73, 30);
            this.button11.TabIndex = 236;
            this.button11.Text = "Next";
            this.button11.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button11.UseVisualStyleBackColor = false;
            this.button11.Click += new System.EventHandler(this.button11_Click_2);
            // 
            // btnaddrcan
            // 
            this.btnaddrcan.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnaddrcan.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnaddrcan.Image = ((System.Drawing.Image)(resources.GetObject("btnaddrcan.Image")));
            this.btnaddrcan.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnaddrcan.Location = new System.Drawing.Point(719, 359);
            this.btnaddrcan.Margin = new System.Windows.Forms.Padding(4);
            this.btnaddrcan.Name = "btnaddrcan";
            this.btnaddrcan.Size = new System.Drawing.Size(60, 30);
            this.btnaddrcan.TabIndex = 100;
            this.btnaddrcan.Text = "Back";
            this.btnaddrcan.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnaddrcan.UseVisualStyleBackColor = false;
            this.btnaddrcan.Click += new System.EventHandler(this.btnaddrcan_Click_1);
            // 
            // label55
            // 
            this.label55.AutoSize = true;
            this.label55.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label55.Location = new System.Drawing.Point(430, 439);
            this.label55.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label55.Name = "label55";
            this.label55.Size = new System.Drawing.Size(79, 16);
            this.label55.TabIndex = 264;
            this.label55.Text = "Total Value";
            // 
            // Txttot
            // 
            this.Txttot.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Txttot.Enabled = false;
            this.Txttot.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txttot.Location = new System.Drawing.Point(536, 402);
            this.Txttot.Margin = new System.Windows.Forms.Padding(5);
            this.Txttot.Name = "Txttot";
            this.Txttot.Size = new System.Drawing.Size(101, 22);
            this.Txttot.TabIndex = 109;
            this.Txttot.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txtot
            // 
            this.txtot.Location = new System.Drawing.Point(699, 429);
            this.txtot.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.txtot.Name = "txtot";
            this.txtot.Size = new System.Drawing.Size(121, 22);
            this.txtot.TabIndex = 237;
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label42.Location = new System.Drawing.Point(626, 224);
            this.label42.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(67, 16);
            this.label42.TabIndex = 261;
            this.label42.Text = "Addnotes";
            // 
            // txtnotes
            // 
            this.txtnotes.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtnotes.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtnotes.Location = new System.Drawing.Point(621, 242);
            this.txtnotes.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.txtnotes.Name = "txtnotes";
            this.txtnotes.Size = new System.Drawing.Size(275, 22);
            this.txtnotes.TabIndex = 232;
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label32.Location = new System.Drawing.Point(513, 225);
            this.label32.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(78, 16);
            this.label32.TabIndex = 246;
            this.label32.Text = "BasicValue";
            // 
            // txtbval
            // 
            this.txtbval.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtbval.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtbval.Location = new System.Drawing.Point(516, 241);
            this.txtbval.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.txtbval.Name = "txtbval";
            this.txtbval.Size = new System.Drawing.Size(99, 22);
            this.txtbval.TabIndex = 245;
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label31.Location = new System.Drawing.Point(452, 224);
            this.label31.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(29, 16);
            this.label31.TabIndex = 244;
            this.label31.Text = "Qty";
            // 
            // txtqty
            // 
            this.txtqty.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtqty.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtqty.Location = new System.Drawing.Point(454, 241);
            this.txtqty.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.txtqty.Name = "txtqty";
            this.txtqty.Size = new System.Drawing.Size(57, 22);
            this.txtqty.TabIndex = 231;
            this.txtqty.TextChanged += new System.EventHandler(this.txtqty_TextChanged_2);
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(8, 221);
            this.label15.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(73, 16);
            this.label15.TabIndex = 242;
            this.label15.Text = "ItemName";
            // 
            // txtitemname
            // 
            this.txtitemname.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtitemname.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtitemname.Location = new System.Drawing.Point(8, 241);
            this.txtitemname.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.txtitemname.Name = "txtitemname";
            this.txtitemname.Size = new System.Drawing.Size(364, 22);
            this.txtitemname.TabIndex = 229;
            this.txtitemname.Click += new System.EventHandler(this.txtitemname_Click);
            this.txtitemname.MouseClick += new System.Windows.Forms.MouseEventHandler(this.txtitemname_MouseClick);
            this.txtitemname.TextChanged += new System.EventHandler(this.txtitemname_TextChanged);
            this.txtitemname.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtitemname_KeyDown);
            this.txtitemname.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtitemname_KeyPress);
            this.txtitemname.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txtitemname_KeyUp);
            this.txtitemname.MouseDown += new System.Windows.Forms.MouseEventHandler(this.txtitemname_MouseDown);
            this.txtitemname.MouseEnter += new System.EventHandler(this.txtitemname_MouseEnter);
            this.txtitemname.MouseMove += new System.Windows.Forms.MouseEventHandler(this.txtitemname_MouseMove);
            this.txtitemname.MouseUp += new System.Windows.Forms.MouseEventHandler(this.txtitemname_MouseUp);
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(380, 224);
            this.label14.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(41, 16);
            this.label14.TabIndex = 240;
            this.label14.Text = "Price";
            // 
            // txtprice
            // 
            this.txtprice.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtprice.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtprice.Location = new System.Drawing.Point(375, 241);
            this.txtprice.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.txtprice.Name = "txtprice";
            this.txtprice.Size = new System.Drawing.Size(68, 22);
            this.txtprice.TabIndex = 230;
            this.txtprice.TextChanged += new System.EventHandler(this.txtprice_TextChanged_1);
            // 
            // buttcusok
            // 
            this.buttcusok.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.buttcusok.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttcusok.Image = global::ACV.Properties.Resources.ok1_25x25;
            this.buttcusok.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.buttcusok.Location = new System.Drawing.Point(904, 236);
            this.buttcusok.Name = "buttcusok";
            this.buttcusok.Size = new System.Drawing.Size(34, 32);
            this.buttcusok.TabIndex = 232;
            this.buttcusok.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.buttcusok.UseVisualStyleBackColor = false;
            this.buttcusok.Click += new System.EventHandler(this.buttcusok_Click);
            // 
            // txtgst
            // 
            this.txtgst.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtgst.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtgst.Location = new System.Drawing.Point(178, 292);
            this.txtgst.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.txtgst.Name = "txtgst";
            this.txtgst.Size = new System.Drawing.Size(265, 22);
            this.txtgst.TabIndex = 324;
            // 
            // txtite
            // 
            this.txtite.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtite.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtite.Location = new System.Drawing.Point(553, 315);
            this.txtite.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.txtite.Name = "txtite";
            this.txtite.Size = new System.Drawing.Size(68, 22);
            this.txtite.TabIndex = 326;
            // 
            // txtqtyp
            // 
            this.txtqtyp.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtqtyp.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtqtyp.Location = new System.Drawing.Point(375, 306);
            this.txtqtyp.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.txtqtyp.Name = "txtqtyp";
            this.txtqtyp.Size = new System.Drawing.Size(68, 22);
            this.txtqtyp.TabIndex = 325;
            // 
            // txtitemcode
            // 
            this.txtitemcode.Location = new System.Drawing.Point(690, 296);
            this.txtitemcode.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.txtitemcode.Name = "txtitemcode";
            this.txtitemcode.Size = new System.Drawing.Size(87, 22);
            this.txtitemcode.TabIndex = 327;
            // 
            // txtbuid
            // 
            this.txtbuid.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtbuid.Location = new System.Drawing.Point(645, 308);
            this.txtbuid.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.txtbuid.Name = "txtbuid";
            this.txtbuid.Size = new System.Drawing.Size(23, 22);
            this.txtbuid.TabIndex = 328;
            // 
            // txt100
            // 
            this.txt100.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt100.Location = new System.Drawing.Point(505, 385);
            this.txt100.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.txt100.Name = "txt100";
            this.txt100.Size = new System.Drawing.Size(35, 22);
            this.txt100.TabIndex = 335;
            // 
            // txtlisid
            // 
            this.txtlisid.Location = new System.Drawing.Point(384, 366);
            this.txtlisid.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.txtlisid.Name = "txtlisid";
            this.txtlisid.Size = new System.Drawing.Size(28, 22);
            this.txtlisid.TabIndex = 247;
            this.txtlisid.TextChanged += new System.EventHandler(this.txtlisid_TextChanged);
            // 
            // txtsaleorderno
            // 
            this.txtsaleorderno.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtsaleorderno.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtsaleorderno.Location = new System.Drawing.Point(8, 116);
            this.txtsaleorderno.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.txtsaleorderno.Name = "txtsaleorderno";
            this.txtsaleorderno.Size = new System.Drawing.Size(121, 22);
            this.txtsaleorderno.TabIndex = 337;
            // 
            // txtsalesdate
            // 
            this.txtsalesdate.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtsalesdate.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtsalesdate.Location = new System.Drawing.Point(130, 116);
            this.txtsalesdate.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.txtsalesdate.Name = "txtsalesdate";
            this.txtsalesdate.Size = new System.Drawing.Size(113, 22);
            this.txtsalesdate.TabIndex = 338;
            // 
            // txtorderno
            // 
            this.txtorderno.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtorderno.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.txtorderno.Location = new System.Drawing.Point(131, 116);
            this.txtorderno.Margin = new System.Windows.Forms.Padding(4);
            this.txtorderno.Name = "txtorderno";
            this.txtorderno.Size = new System.Drawing.Size(113, 22);
            this.txtorderno.TabIndex = 226;
            // 
            // txttrans
            // 
            this.txttrans.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txttrans.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txttrans.Location = new System.Drawing.Point(9, 116);
            this.txttrans.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.txttrans.Name = "txttrans";
            this.txttrans.Size = new System.Drawing.Size(118, 22);
            this.txttrans.TabIndex = 225;
            this.txttrans.TextChanged += new System.EventHandler(this.txttrans_TextChanged);
            this.txttrans.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txttrans_KeyDown);
            // 
            // txtchk
            // 
            this.txtchk.Location = new System.Drawing.Point(335, 370);
            this.txtchk.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.txtchk.Name = "txtchk";
            this.txtchk.Size = new System.Drawing.Size(28, 22);
            this.txtchk.TabIndex = 339;
            // 
            // addipan
            // 
            this.addipan.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.addipan.Location = new System.Drawing.Point(13, 18);
            this.addipan.Name = "addipan";
            this.addipan.Size = new System.Drawing.Size(1151, 440);
            this.addipan.TabIndex = 229;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(454, 468);
            this.label13.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(149, 18);
            this.label13.TabIndex = 232;
            this.label13.Text = "Additional Charges";
            // 
            // txttotaddd
            // 
            this.txttotaddd.Location = new System.Drawing.Point(621, 467);
            this.txttotaddd.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.txttotaddd.Name = "txttotaddd";
            this.txttotaddd.Size = new System.Drawing.Size(161, 22);
            this.txttotaddd.TabIndex = 233;
            this.txttotaddd.TextChanged += new System.EventHandler(this.txttotaddd_TextChanged);
            // 
            // Taxpan
            // 
            this.Taxpan.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.Taxpan.Controls.Add(this.txttotaddd);
            this.Taxpan.Controls.Add(this.label13);
            this.Taxpan.Controls.Add(this.addipan);
            this.Taxpan.Location = new System.Drawing.Point(-1, -2);
            this.Taxpan.Margin = new System.Windows.Forms.Padding(4);
            this.Taxpan.Name = "Taxpan";
            this.Taxpan.Size = new System.Drawing.Size(4, 466);
            this.Taxpan.TabIndex = 201;
            this.Taxpan.Paint += new System.Windows.Forms.PaintEventHandler(this.Taxpan_Paint);
            // 
            // button14
            // 
            this.button14.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.button14.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button14.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.button14.Location = new System.Drawing.Point(954, 467);
            this.button14.Name = "button14";
            this.button14.Size = new System.Drawing.Size(62, 30);
            this.button14.TabIndex = 330;
            this.button14.Text = "Terms";
            this.button14.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button14.UseVisualStyleBackColor = false;
            this.button14.Click += new System.EventHandler(this.button14_Click_1);
            // 
            // btnsave
            // 
            this.btnsave.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnsave.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnsave.Image = ((System.Drawing.Image)(resources.GetObject("btnsave.Image")));
            this.btnsave.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnsave.Location = new System.Drawing.Point(1023, 467);
            this.btnsave.Margin = new System.Windows.Forms.Padding(4);
            this.btnsave.Name = "btnsave";
            this.btnsave.Size = new System.Drawing.Size(69, 30);
            this.btnsave.TabIndex = 332;
            this.btnsave.Text = "Save";
            this.btnsave.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnsave.UseVisualStyleBackColor = false;
            this.btnsave.Click += new System.EventHandler(this.btnsave_Click_2);
            // 
            // buttnfinbk
            // 
            this.buttnfinbk.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.buttnfinbk.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttnfinbk.Image = ((System.Drawing.Image)(resources.GetObject("buttnfinbk.Image")));
            this.buttnfinbk.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttnfinbk.Location = new System.Drawing.Point(1100, 467);
            this.buttnfinbk.Margin = new System.Windows.Forms.Padding(4);
            this.buttnfinbk.Name = "buttnfinbk";
            this.buttnfinbk.Size = new System.Drawing.Size(60, 30);
            this.buttnfinbk.TabIndex = 331;
            this.buttnfinbk.Text = "Back";
            this.buttnfinbk.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.buttnfinbk.UseVisualStyleBackColor = false;
            this.buttnfinbk.Click += new System.EventHandler(this.buttnfinbk_Click_1);
            // 
            // button17
            // 
            this.button17.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.button17.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button17.Image = ((System.Drawing.Image)(resources.GetObject("button17.Image")));
            this.button17.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button17.Location = new System.Drawing.Point(887, 469);
            this.button17.Margin = new System.Windows.Forms.Padding(4);
            this.button17.Name = "button17";
            this.button17.Size = new System.Drawing.Size(60, 30);
            this.button17.TabIndex = 333;
            this.button17.Text = "Back";
            this.button17.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button17.UseVisualStyleBackColor = false;
            this.button17.Click += new System.EventHandler(this.button17_Click);
            // 
            // FrmInvoice
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1175, 497);
            this.Controls.Add(this.panadd);
            this.Controls.Add(this.button14);
            this.Controls.Add(this.btnsave);
            this.Controls.Add(this.buttnfinbk);
            this.Controls.Add(this.button17);
            this.Controls.Add(this.Taxpan);
            this.Controls.Add(this.Editpan);
            this.Controls.Add(this.Genpan);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Margin = new System.Windows.Forms.Padding(4);
            this.MaximizeBox = false;
            this.Name = "FrmInvoice";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "         ";
            this.Load += new System.EventHandler(this.FrmInvoice_Load);
            this.Genpan.ResumeLayout(false);
            this.Genpan.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.HFGP)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panadd.ResumeLayout(false);
            this.panadd.PerformLayout();
            this.Editpan.ResumeLayout(false);
            this.Editpan.PerformLayout();
            this.termspan.ResumeLayout(false);
            this.termspan.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.HFGT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.HFG9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.HFGTAX)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.HFGA)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.HFIT)).EndInit();
            this.pantax.ResumeLayout(false);
            this.pantax.PerformLayout();
            this.Taxpan.ResumeLayout(false);
            this.Taxpan.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel Genpan;
        private System.Windows.Forms.DataGridView HFGP;
        private System.Windows.Forms.Button btnaddrcan;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Panel panadd;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label lblno1;
        private System.Windows.Forms.Label lblno2;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel3;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel2;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private System.Windows.Forms.Button buttnnvfst;
        private System.Windows.Forms.Button buttnnxtlft;
        private System.Windows.Forms.Button btnfinnxt;
        private System.Windows.Forms.Button buttrnxt;
        private System.Windows.Forms.Button buttnext1;
        private System.Windows.Forms.CheckBox chkact;
        private System.Windows.Forms.Button butcan;
        private System.Windows.Forms.Button butedit;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.TextBox txtscr5;
        private System.Windows.Forms.TextBox txtscr4;
        private System.Windows.Forms.TextBox Txtscr3;
        private System.Windows.Forms.TextBox Txtscr2;
        private System.Windows.Forms.TextBox txtscr1;
        private System.Windows.Forms.TextBox txtscr6;
        private System.Windows.Forms.Button button11;
        private System.Windows.Forms.Button button12;
        private System.Windows.Forms.Button buttnnxt;
        private System.Windows.Forms.Panel Editpan;
        private System.Windows.Forms.Label label42;
        private System.Windows.Forms.TextBox txtnotes;
        private System.Windows.Forms.Label label41;
        private System.Windows.Forms.TextBox txtveh;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.TextBox txttrans;
        private System.Windows.Forms.DateTimePicker Dtprem;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.DateTimePicker Dtppre;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.TextBox txtplace;
        private System.Windows.Forms.Button buttcusok;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.TextBox txtbval;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.TextBox Txttot;
        private System.Windows.Forms.TextBox txtqty;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox txtitemname;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox txtprice;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtot;
        private System.Windows.Forms.Button button8;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel pantax;
        private System.Windows.Forms.RichTextBox txttitem;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.TextBox txttqty;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.TextBox Txtrate;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.TextBox txtbasic;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.TextBox txttotal;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.TextBox txtigcst;
        private System.Windows.Forms.ComboBox cboigst;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.TextBox txtsgst;
        private System.Windows.Forms.ComboBox SGST;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.TextBox txtcgst;
        private System.Windows.Forms.ComboBox cbocgst;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.TextBox txtper;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox txttaxable;
        private System.Windows.Forms.TextBox txthidqty;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.TextBox txtamt;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.DateTimePicker DTPDOCDT;
        private System.Windows.Forms.DataGridView HFIT;
        private System.Windows.Forms.DateTimePicker Dtpdt;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtdcno;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtname;
        private System.Windows.Forms.Label Phone;
        private System.Windows.Forms.TextBox txtgrn;
        private System.Windows.Forms.TextBox txtpuid;
        private System.Windows.Forms.TextBox txtgrnid;
        private System.Windows.Forms.TextBox txtdcid;
        private System.Windows.Forms.RichTextBox txtrem;
        private System.Windows.Forms.TextBox txttitemid;
        private System.Windows.Forms.TextBox txtpluid;
        private System.Windows.Forms.Label label44;
        private System.Windows.Forms.TextBox txtuom;
        private System.Windows.Forms.Label label55;
        private System.Windows.Forms.RichTextBox txtpadd2;
        private System.Windows.Forms.RichTextBox txtpadd1;
        private System.Windows.Forms.TextBox txttempadd2;
        private System.Windows.Forms.TextBox txttempadd1;
        private System.Windows.Forms.Label label57;
        private System.Windows.Forms.DateTimePicker dtpfnt;
        private System.Windows.Forms.Label label54;
        private System.Windows.Forms.TextBox txtttot;
        private System.Windows.Forms.TextBox txtigval;
        private System.Windows.Forms.TextBox txtigstp;
        private System.Windows.Forms.Label label52;
        private System.Windows.Forms.Label label53;
        private System.Windows.Forms.TextBox txttsgval;
        private System.Windows.Forms.TextBox txtsgstp;
        private System.Windows.Forms.Label label50;
        private System.Windows.Forms.Label label51;
        private System.Windows.Forms.TextBox txttcgval;
        private System.Windows.Forms.TextBox txttcgstp;
        private System.Windows.Forms.Label label49;
        private System.Windows.Forms.Label label48;
        private System.Windows.Forms.Label label47;
        private System.Windows.Forms.TextBox txttprdval;
        private System.Windows.Forms.Label label46;
        private System.Windows.Forms.TextBox txttdisc;
        private System.Windows.Forms.TextBox txttbval;
        private System.Windows.Forms.Label label45;
        private System.Windows.Forms.Label label43;
        private System.Windows.Forms.TextBox txttdis;
        private System.Windows.Forms.TextBox txtexcise;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox TxtNetAmt;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.TextBox TxtRoff;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txtcharges;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox txttgstval;
        private System.Windows.Forms.TextBox txttgstp;
        private System.Windows.Forms.DateTimePicker txtorderno;
        private System.Windows.Forms.Label label59;
        private System.Windows.Forms.TextBox txtcoucharge;
        private System.Windows.Forms.Label label60;
        private System.Windows.Forms.TextBox txtgst;
        private System.Windows.Forms.TextBox txtite;
        private System.Windows.Forms.TextBox txtqtyp;
        private System.Windows.Forms.TextBox txtitemcode;
        private System.Windows.Forms.TextBox txtbuid;
        private System.Windows.Forms.Button button13;
        private System.Windows.Forms.Panel addipan;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox txttotaddd;
        private System.Windows.Forms.Panel Taxpan;
        private System.Windows.Forms.Panel termspan;
        private System.Windows.Forms.Button button15;
        private System.Windows.Forms.Label label56;
        private System.Windows.Forms.Button button10;
        private System.Windows.Forms.TextBox txttotc;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.TextBox txtaddcharge;
        private System.Windows.Forms.Button btnadd;
        private System.Windows.Forms.TextBox txtaddid;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.DataGridView HFGTAX;
        private System.Windows.Forms.Button button9;
        private System.Windows.Forms.DataGridView HFGA;
        private System.Windows.Forms.TextBox txtremde;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox txtterms;
        private System.Windows.Forms.Label Terms;
        private System.Windows.Forms.TextBox txttermid;
        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.Button button14;
        private System.Windows.Forms.Button buttnfinbk;
        private System.Windows.Forms.Button btnsave;
        private System.Windows.Forms.TextBox txt100;
        private System.Windows.Forms.TextBox txtlisid;
        private System.Windows.Forms.DataGridView HFG9;
        private System.Windows.Forms.Label label61;
        private System.Windows.Forms.Button button16;
        private System.Windows.Forms.CheckBox checkBox1;
        private System.Windows.Forms.TextBox txttermset;
        private System.Windows.Forms.DataGridView HFGT;
        private System.Windows.Forms.Button button17;
        private System.Windows.Forms.TextBox txtsaleorderno;
        private System.Windows.Forms.TextBox txtsalesdate;
        private System.Windows.Forms.TextBox txtscr8;
        private System.Windows.Forms.TextBox txtscr7;
        private System.Windows.Forms.TextBox txtbasicval;
        private System.Windows.Forms.TextBox txttax;
        private System.Windows.Forms.TextBox txttotamt;
        private System.Windows.Forms.Label label58;
        private System.Windows.Forms.TextBox txtchargessum;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.TextBox txtchk;
    }
}