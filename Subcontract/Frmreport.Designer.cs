﻿namespace ACV
{
    partial class Frmreport
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Frmreport));
            this.cbotype = new System.Windows.Forms.ComboBox();
            this.label14 = new System.Windows.Forms.Label();
            this.dtpfrom = new System.Windows.Forms.DateTimePicker();
            this.label6 = new System.Windows.Forms.Label();
            this.dtpto = new System.Windows.Forms.DateTimePicker();
            this.label1 = new System.Windows.Forms.Label();
            this.buttnshow = new System.Windows.Forms.Button();
            this.buttnext = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // cbotype
            // 
            this.cbotype.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cbotype.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cbotype.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbotype.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbotype.FormattingEnabled = true;
            this.cbotype.Items.AddRange(new object[] {
            "Sales Register",
            "Hsncode Register"});
            this.cbotype.Location = new System.Drawing.Point(24, 57);
            this.cbotype.Name = "cbotype";
            this.cbotype.Size = new System.Drawing.Size(286, 26);
            this.cbotype.TabIndex = 184;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.ForeColor = System.Drawing.Color.Black;
            this.label14.Location = new System.Drawing.Point(21, 38);
            this.label14.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(82, 18);
            this.label14.TabIndex = 185;
            this.label14.Text = "Report Type";
            // 
            // dtpfrom
            // 
            this.dtpfrom.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpfrom.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpfrom.Location = new System.Drawing.Point(43, 123);
            this.dtpfrom.Margin = new System.Windows.Forms.Padding(4);
            this.dtpfrom.Name = "dtpfrom";
            this.dtpfrom.Size = new System.Drawing.Size(113, 26);
            this.dtpfrom.TabIndex = 223;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(40, 103);
            this.label6.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(72, 18);
            this.label6.TabIndex = 222;
            this.label6.Text = "From Date";
            // 
            // dtpto
            // 
            this.dtpto.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpto.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpto.Location = new System.Drawing.Point(164, 123);
            this.dtpto.Margin = new System.Windows.Forms.Padding(4);
            this.dtpto.Name = "dtpto";
            this.dtpto.Size = new System.Drawing.Size(113, 26);
            this.dtpto.TabIndex = 225;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(161, 103);
            this.label1.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(54, 18);
            this.label1.TabIndex = 224;
            this.label1.Text = "To Date";
            // 
            // buttnshow
            // 
            this.buttnshow.BackColor = System.Drawing.Color.White;
            this.buttnshow.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttnshow.Image = ((System.Drawing.Image)(resources.GetObject("buttnshow.Image")));
            this.buttnshow.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttnshow.Location = new System.Drawing.Point(93, 186);
            this.buttnshow.Name = "buttnshow";
            this.buttnshow.Size = new System.Drawing.Size(65, 30);
            this.buttnshow.TabIndex = 226;
            this.buttnshow.Text = "Show";
            this.buttnshow.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.buttnshow.UseVisualStyleBackColor = false;
            this.buttnshow.Click += new System.EventHandler(this.buttnshow_Click);
            // 
            // buttnext
            // 
            this.buttnext.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.buttnext.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttnext.Image = ((System.Drawing.Image)(resources.GetObject("buttnext.Image")));
            this.buttnext.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttnext.Location = new System.Drawing.Point(164, 186);
            this.buttnext.Name = "buttnext";
            this.buttnext.Size = new System.Drawing.Size(54, 30);
            this.buttnext.TabIndex = 227;
            this.buttnext.Text = "Exit";
            this.buttnext.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.buttnext.UseVisualStyleBackColor = false;
            this.buttnext.Click += new System.EventHandler(this.buttnext_Click);
            // 
            // Frmreport
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.ClientSize = new System.Drawing.Size(340, 262);
            this.Controls.Add(this.buttnext);
            this.Controls.Add(this.buttnshow);
            this.Controls.Add(this.dtpto);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.dtpfrom);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.cbotype);
            this.Controls.Add(this.label14);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.Name = "Frmreport";
            this.Text = "Register";
            this.Load += new System.EventHandler(this.Frmreport_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox cbotype;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.DateTimePicker dtpfrom;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.DateTimePicker dtpto;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button buttnshow;
        private System.Windows.Forms.Button buttnext;
    }
}