﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Windows.Forms;

namespace ACV
{
    public partial class FrmDispatchPackingSlip : Form
    {
        public FrmDispatchPackingSlip()
        {
            InitializeComponent();
        }
        BindingSource bsDipatchNo = new BindingSource();
        SqlConnection conn = new SqlConnection("Data Source=" + Genclass.data2 + "; Initial Catalog=" + Genclass.data5 + ";User id=" + Genclass.data3 + ";Password=" + Genclass.data4 + "");
        SQLDBHelper db = new SQLDBHelper();
        int Fillid = 0;
        int SelectedId = 0;
        DataTable dtM;
        DataTable dtD;

        private void FrmDispatchPackingSlip_Load(object sender, EventArgs e)
        {
            LoadTo();
            LoadPurpose();
            LoadButton(0);
            LoadDataGridSortRoll();
            LoadDataGridSortSummary();
            LoadFrontGrid();
        }

        private void LoadButton(int id)
        {
            try
            {
                if (id == 0)//Load
                {
                    btnAdd.Visible = true;
                    btnEdit.Visible = true;
                    btnDelete.Visible = true;
                    btnExit.Visible = true;
                    btnPrint.Visible = true;
                    btnsave.Visible = false;
                    btnaddrcan.Visible = false;
                    grFront.Visible = true;
                    grBack.Visible = false;
                    btnPrint.Visible = true;
                    btnPackingSlipPrint.Visible = true;
                }
                else if (id == 1)//add
                {
                    btnAdd.Visible = false;
                    btnEdit.Visible = false;
                    btnDelete.Visible = false;
                    btnExit.Visible = false;
                    btnPrint.Visible = false;
                    btnsave.Visible = true;
                    btnaddrcan.Visible = true;
                    grFront.Visible = false;
                    grBack.Visible = true;
                    btnsave.Text = "Save";
                    btnPrint.Visible = false;
                    btnPackingSlipPrint.Visible = false;
                }
                else if (id == 2)//Edit
                {
                    btnAdd.Visible = false;
                    btnEdit.Visible = false;
                    btnDelete.Visible = false;
                    btnExit.Visible = false;
                    btnPrint.Visible = false;
                    btnsave.Visible = true;
                    btnaddrcan.Visible = true;
                    grFront.Visible = false;
                    grBack.Visible = true;
                    btnsave.Text = "Update";
                    btnPrint.Visible = false;
                    btnPackingSlipPrint.Visible = false;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void BtnAdd_Click(object sender, EventArgs e)
        {
            try
            {
                LoadButton(1);
                txtDocNo.Tag = "0";
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void BtnExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void Btnaddrcan_Click(object sender, EventArgs e)
        {
            try
            {
                LoadButton(0);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        protected void LoadTo()
        {
            try
            {
                DataTable dt = db.GetDataWithoutParam(CommandType.StoredProcedure, "SP_getParty", conn);
                CmbTo.DataSource = null;
                CmbTo.DisplayMember = "Name";
                CmbTo.ValueMember = "Uid";
                CmbTo.DataSource = dt;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        protected void LoadPurpose()
        {
            try
            {
                SqlParameter[] parameters = { new SqlParameter("@TypeM_Uid", 32), new SqlParameter("@Active", 1) };
                DataTable dt = db.GetDataWithParam(CommandType.StoredProcedure, "SP_GetGeneralM", parameters, conn);
                CmbPurpose.DataSource = null;
                CmbPurpose.DisplayMember = "GeneralName";
                CmbPurpose.ValueMember = "Uid";
                CmbPurpose.DataSource = dt;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void TxtDispatchSlipNo_Click(object sender, EventArgs e)
        {
            try
            {
                DataTable dt = db.GetDataWithoutParam(CommandType.StoredProcedure, "Proc_GetDispatchPackingSlip", conn);
                bsDipatchNo.DataSource = dt;
                Point loc = FindLocation(txtDispatchSlipNo);
                grSearch.Location = new Point(loc.X, loc.Y + 20);
                grSearch.Visible = true;
                LoadDataGrid(dt, 1);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private Point FindLocation(Control ctrl)
        {
            if (ctrl.Parent is Form)
                return ctrl.Location;
            else
            {
                Point p = FindLocation(ctrl.Parent);
                p.X += ctrl.Location.X;
                p.Y += ctrl.Location.Y;
                return p;
            }
        }

        private void LoadDataGrid(DataTable dt, int FillId)
        {
            try
            {
                SelectedId = 1;
                DataGridCommon.DataSource = null;
                if (FillId == 1)
                {
                    Fillid = 1;
                    DataGridCommon.AutoGenerateColumns = false;
                    DataGridCommon.ColumnCount = 3;
                    DataGridCommon.Columns[0].Name = "Uid";
                    DataGridCommon.Columns[0].HeaderText = "Uid";
                    DataGridCommon.Columns[0].DataPropertyName = "Uid";
                    DataGridCommon.Columns[0].Visible = false;

                    DataGridCommon.Columns[1].Name = "DispatchNo";
                    DataGridCommon.Columns[1].HeaderText = "Dispatch No";
                    DataGridCommon.Columns[1].DataPropertyName = "DispatchNo";
                    DataGridCommon.Columns[1].Width = 150;

                    DataGridCommon.Columns[2].Name = "VehicleNo";
                    DataGridCommon.Columns[2].HeaderText = "Vehicle No";
                    DataGridCommon.Columns[2].DataPropertyName = "VehicleNo";
                    DataGridCommon.Columns[2].Width = 200;
                    DataGridCommon.DataSource = bsDipatchNo;
                }
                SelectedId = 0;
                grSearch.Visible = true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void BtnSelect_Click(object sender, EventArgs e)
        {
            try
            {
                SelectedId = 1;
                if (Fillid == 1)
                {
                    int Index = DataGridCommon.SelectedCells[0].RowIndex;
                    txtDispatchSlipNo.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                    txtDispatchSlipNo.Tag = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                    txtVechileNo.Text = DataGridCommon.Rows[Index].Cells[2].Value.ToString();
                    string Query = "Select ISNULL(Count(*),0) + 1 as Cnt from DispatachPackingSlipM Where DispatchSlipUid = " + txtDispatchSlipNo.Tag + "";
                    DataTable dt = db.GetDataWithoutParam(CommandType.Text, Query, conn);
                    txtDocNo.Text = txtVechileNo.Text + "-" + dt.Rows[0]["Cnt"].ToString();
                    LoadSortNo(Convert.ToDecimal(txtDispatchSlipNo.Tag));
                }
                SelectedId = 0;
                grSearch.Visible = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        protected void LoadSortNo(decimal DispatchSlipMUid)
        {
            try
            {
                SqlParameter[] sqlParameters = { new SqlParameter("@DispatchSlipMUid", DispatchSlipMUid) };
                DataSet dataSet = db.GetDataWithParamMultiple(CommandType.StoredProcedure, "Proc_GetDetForDispatchSlip", sqlParameters, conn);
                dtM = dataSet.Tables[0];
                dtD = dataSet.Tables[1];
                DataTable dataTable = dtD.DefaultView.ToTable(true, "SortNo");
                CmbSortNo.DataSource = null;
                CmbSortNo.DisplayMember = "SortNo";
                CmbSortNo.ValueMember = "SortNo";
                CmbSortNo.DataSource = dataTable;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }

        }

        private void BtnHide_Click(object sender, EventArgs e)
        {
            try
            {
                grSearch.Visible = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void TxtDispatchSlipNo_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (SelectedId == 0)
                {
                    bsDipatchNo.Filter = string.Format("DispatchNo Like '%{0}%'", txtDispatchSlipNo.Text);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void DataGridCommon_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            try
            {
                SelectedId = 1;
                if (Fillid == 1)
                {
                    int Index = DataGridCommon.SelectedCells[0].RowIndex;
                    txtDispatchSlipNo.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                    txtDispatchSlipNo.Tag = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                    txtVechileNo.Text = DataGridCommon.Rows[Index].Cells[2].Value.ToString();
                    string Query = "Select ISNULL(Count(*),0) + 1 as Cnt from DispatachPackingSlipM Where DispatchSlipUid = " + txtDispatchSlipNo.Tag + "";
                    DataTable dt = db.GetDataWithoutParam(CommandType.Text, Query, conn);
                    txtDocNo.Text = txtVechileNo.Text + "-" + dt.Rows[0]["Cnt"].ToString();
                    LoadSortNo(Convert.ToDecimal(txtDispatchSlipNo.Tag));
                }
                SelectedId = 0;
                grSearch.Visible = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void CmbSortNo_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (CmbSortNo.SelectedIndex != -1)
                {
                    DataGridSortRoll.Rows.Clear();
                    txtFrTotalMtr.Text = "0";
                    txtTotalFrWght.Text = "0";
                    DataTable dt = dtD.Select("SortNo='" + CmbSortNo.Text + "'", null).CopyToDataTable();
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        int Index = DataGridSortRoll.Rows.Add();
                        DataGridViewRow row = (DataGridViewRow)DataGridSortRoll.Rows[Index];
                        row.Cells[1].Value = CmbSortNo.Text;
                        row.Cells[2].Value = dt.Rows[i]["RollNo"].ToString();
                        row.Cells[3].Value = dt.Rows[i]["Meter"].ToString();
                        row.Cells[4].Value = dt.Rows[i]["Weight"].ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        protected void LoadDataGridSortRoll()
        {
            try
            {
                DataGridSortRoll.AutoGenerateColumns = false;
                DataGridSortRoll.ColumnCount = 4;

                DataGridViewCheckBoxColumn dataGridViewCheckBoxColumn = new DataGridViewCheckBoxColumn();
                dataGridViewCheckBoxColumn.HeaderText = "Chck";
                dataGridViewCheckBoxColumn.Name = "Chck";
                dataGridViewCheckBoxColumn.Width = 40;
                DataGridSortRoll.Columns.Insert(0, dataGridViewCheckBoxColumn);

                DataGridSortRoll.Columns[1].Name = "SortNo";
                DataGridSortRoll.Columns[1].HeaderText = "SortNo";
                DataGridSortRoll.Columns[1].DataPropertyName = "SortNo";
                DataGridSortRoll.Columns[1].Visible = false;

                DataGridSortRoll.Columns[2].Name = "RollNo";
                DataGridSortRoll.Columns[2].HeaderText = "RollNo";
                DataGridSortRoll.Columns[2].DataPropertyName = "RollNo";
                DataGridSortRoll.Columns[2].Width = 130;

                DataGridSortRoll.Columns[3].Name = "Meter";
                DataGridSortRoll.Columns[3].HeaderText = "Meter";
                DataGridSortRoll.Columns[3].DataPropertyName = "Meter";

                DataGridSortRoll.Columns[4].Name = "Weight";
                DataGridSortRoll.Columns[4].HeaderText = "Weight";
                DataGridSortRoll.Columns[4].DataPropertyName = "Weight";

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        protected void LoadDataGridSortSummary()
        {
            try
            {
                DataGridSortSummary.AutoGenerateColumns = false;
                DataGridSortSummary.ColumnCount = 6;

                DataGridViewCheckBoxColumn dataGridViewCheckBoxColumn = new DataGridViewCheckBoxColumn();
                dataGridViewCheckBoxColumn.HeaderText = "Chck";
                dataGridViewCheckBoxColumn.Name = "Chck";
                dataGridViewCheckBoxColumn.Width = 40;
                DataGridSortSummary.Columns.Insert(0, dataGridViewCheckBoxColumn);

                DataGridSortSummary.Columns[1].Name = "SlNo";
                DataGridSortSummary.Columns[1].HeaderText = "SlNo";
                DataGridSortSummary.Columns[1].Width = 60;

                DataGridSortSummary.Columns[2].Name = "SortNo";
                DataGridSortSummary.Columns[2].HeaderText = "SortNo";
                DataGridSortSummary.Columns[2].Width = 140;

                DataGridSortSummary.Columns[3].Name = "RollNo";
                DataGridSortSummary.Columns[3].HeaderText = "RollNo";
                DataGridSortSummary.Columns[3].Width = 110;

                DataGridSortSummary.Columns[4].Name = "Meter";
                DataGridSortSummary.Columns[4].HeaderText = "Meter";

                DataGridSortSummary.Columns[5].Name = "Weight";
                DataGridSortSummary.Columns[5].HeaderText = "Weight";

                DataGridSortSummary.Columns[6].HeaderText = "Duid";
                DataGridSortSummary.Columns[6].Name = "Duid";
                DataGridSortSummary.Columns[6].Visible = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void BtnAddBags_Click(object sender, EventArgs e)
        {
            try
            {
                CmbToSortNo.Items.Add(CmbSortNo.Text);
                foreach (DataGridViewRow row in DataGridSortRoll.Rows)
                {
                    if (row.Cells["Chck"].Value != null && (bool)row.Cells["Chck"].Value)
                    {
                        bool checkBagNo = CheckGridValue(row.Cells[2].Value.ToString());
                        if (checkBagNo == false)
                        {
                            int Index = DataGridSortSummary.Rows.Add();
                            DataGridViewRow dsrow = (DataGridViewRow)DataGridSortSummary.Rows[Index];
                            dsrow.Cells[1].Value = DataGridSortSummary.Rows.Count;
                            dsrow.Cells[2].Value = CmbSortNo.Text;
                            dsrow.Cells[3].Value = row.Cells[2].Value.ToString();
                            dsrow.Cells[4].Value = row.Cells[3].Value.ToString();
                            dsrow.Cells[5].Value = row.Cells[4].Value.ToString();
                            dsrow.Cells[6].Value = 0;
                        }
                        else
                        {
                            MessageBox.Show("RollNo already exists", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            return;
                        }
                    }
                }
                decimal Weight = 0;
                decimal Meter = 0;
                decimal TotalMeter = 0;
                decimal TotalWeight = 0;
                for (int i = 0; i < DataGridSortSummary.Rows.Count; i++)
                {
                    Weight = Convert.ToDecimal(DataGridSortSummary.Rows[i].Cells[5].Value.ToString());
                    Meter = Convert.ToDecimal(DataGridSortSummary.Rows[i].Cells[4].Value.ToString());
                    TotalMeter += Meter;
                    TotalWeight += Weight;
                }
                txtTotalMeters.Text = TotalMeter.ToString("0.000");
                txtTotalWeight.Text = TotalWeight.ToString("0.000");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void Btnsave_Click(object sender, EventArgs e)
        {
            try
            {
                if (txtDocNo.Text != string.Empty)
                {
                    SqlParameter[] sqlParameters = {
                        new SqlParameter("@Uid",txtDocNo.Tag),
                        new SqlParameter("@DocDate",Convert.ToDateTime(dtpDocDate.Text)),
                        new SqlParameter("@DocNo",txtDocNo.Text),
                        new SqlParameter("@DispatchSlipUid",txtDispatchSlipNo.Tag),
                        new SqlParameter("@DispatchTo",CmbTo.SelectedValue),
                        new SqlParameter("@Purpose",CmbPurpose.SelectedValue),
                        new SqlParameter("@ReurnId",SqlDbType.Decimal)
                    };
                    sqlParameters[6].Direction = ParameterDirection.Output;
                    int Uid = db.ExecuteNonQuery(CommandType.StoredProcedure, "Proc_DispatachPackingSlipM", sqlParameters, conn, 6);

                    for (int i = 0; i < DataGridSortSummary.Rows.Count; i++)
                    {
                        SqlParameter[] parameters = {
                            new SqlParameter("@DPSlipDUid",DataGridSortSummary.Rows[i].Cells[6].Value.ToString()),
                            new SqlParameter("@DPSlipMUid",Uid),
                            new SqlParameter("@SortNo",DataGridSortSummary.Rows[i].Cells[2].Value.ToString()),
                            new SqlParameter("@RollNo",DataGridSortSummary.Rows[i].Cells[3].Value.ToString()),
                            new SqlParameter("@Meter",DataGridSortSummary.Rows[i].Cells[4].Value.ToString()),
                            new SqlParameter("@Weight",DataGridSortSummary.Rows[i].Cells[5].Value.ToString()),
                            new SqlParameter("@VechileNo",txtVechileNo.Text)
                        };
                        db.ExecuteNonQuery(CommandType.StoredProcedure, "Proc_DispatachPackingSlipD", parameters, conn);
                    }
                }
                MessageBox.Show("Record Saved Successfully", "Infromation", MessageBoxButtons.OK, MessageBoxIcon.Information);
                DataGridSortRoll.Rows.Clear();
                DataGridSortSummary.Rows.Clear();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.StackTrace);
                return;
            }
        }

        protected DataTable GetDtata(decimal uid)
        {
            DataTable dt = new DataTable();
            try
            {
                SqlParameter[] sqlParameters = {
                    new SqlParameter("@Uid",uid)
                };
                dt = db.GetDataWithParam(CommandType.StoredProcedure, "Proc_GetDispatachPackingSlip", sqlParameters, conn);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.StackTrace);
            }
            return dt;
        }

        protected void LoadFrontGrid()
        {
            try
            {
                DataTable dt = GetDtata(0);
                DataGridDispatchPakSlip.AutoGenerateColumns = false;
                DataGridDispatchPakSlip.ColumnCount = 9;
                DataGridDispatchPakSlip.Columns[0].Name = "Uid";
                DataGridDispatchPakSlip.Columns[0].HeaderText = "Uid";
                DataGridDispatchPakSlip.Columns[0].DataPropertyName = "Uid";
                DataGridDispatchPakSlip.Columns[0].Visible = false;

                DataGridDispatchPakSlip.Columns[1].Name = "DocNo";
                DataGridDispatchPakSlip.Columns[1].HeaderText = "DocNo";
                DataGridDispatchPakSlip.Columns[1].DataPropertyName = "DocNo";
                DataGridDispatchPakSlip.Columns[1].Width = 150;

                DataGridDispatchPakSlip.Columns[2].Name = "DocDate";
                DataGridDispatchPakSlip.Columns[2].HeaderText = "DocDate";
                DataGridDispatchPakSlip.Columns[2].DataPropertyName = "DocDate";

                DataGridDispatchPakSlip.Columns[3].Name = "DispatchNo";
                DataGridDispatchPakSlip.Columns[3].HeaderText = "DispatchNo";
                DataGridDispatchPakSlip.Columns[3].DataPropertyName = "DispatchNo";

                DataGridDispatchPakSlip.Columns[4].Name = "Name";
                DataGridDispatchPakSlip.Columns[4].HeaderText = "Name";
                DataGridDispatchPakSlip.Columns[4].DataPropertyName = "Name";
                DataGridDispatchPakSlip.Columns[4].Width = 200;

                DataGridDispatchPakSlip.Columns[5].Name = "Job";
                DataGridDispatchPakSlip.Columns[5].HeaderText = "Job";
                DataGridDispatchPakSlip.Columns[5].DataPropertyName = "GeneralName";
                DataGridDispatchPakSlip.Columns[5].Width = 190;

                DataGridDispatchPakSlip.Columns[6].Name = "Roll";
                DataGridDispatchPakSlip.Columns[6].HeaderText = "Roll";
                DataGridDispatchPakSlip.Columns[6].DataPropertyName = "Roll";

                DataGridDispatchPakSlip.Columns[7].Name = "Meter";
                DataGridDispatchPakSlip.Columns[7].HeaderText = "Meter";
                DataGridDispatchPakSlip.Columns[7].DataPropertyName = "Meter";

                DataGridDispatchPakSlip.Columns[8].Name = "Weight";
                DataGridDispatchPakSlip.Columns[8].HeaderText = "Weight";
                DataGridDispatchPakSlip.Columns[8].DataPropertyName = "Weight";
                DataGridDispatchPakSlip.DataSource = dt;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.StackTrace);
                return;
            }
        }

        protected bool CheckGridValue(string RollNo)
        {
            bool entryFound = false;
            try
            {
                foreach (DataGridViewRow row in DataGridSortSummary.Rows)
                {
                    object val2 = row.Cells[3].Value;
                    if (val2 != null && val2.ToString() == RollNo)
                    {
                        entryFound = true;
                        break;
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            return entryFound;
        }

        private void BtnReverse_Click(object sender, EventArgs e)
        {
            try
            {
                foreach (DataGridViewRow row in DataGridSortSummary.Rows)
                {
                    if (row.Cells["Chck"].Value != null && (bool)row.Cells["Chck"].Value)
                    {
                        bool checkBagNo = CheckGridValue(row.Cells[2].Value.ToString());
                        if (checkBagNo == false)
                        {
                            int Index = DataGridSortRoll.Rows.Add();
                            DataGridViewRow dsrow = (DataGridViewRow)DataGridSortRoll.Rows[Index];
                            dsrow.Cells[1].Value = row.Cells[1].Value.ToString();
                            dsrow.Cells[2].Value = row.Cells[2].Value.ToString();
                            dsrow.Cells[3].Value = row.Cells[3].Value.ToString();
                            dsrow.Cells[4].Value = row.Cells[4].Value.ToString();
                            DataGridSortSummary.Rows.Remove(row);
                            DataGridSortSummary.ClearSelection();
                        }
                        else
                        {
                            MessageBox.Show("RollNo already exists", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            return;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void DataGridSortSummary_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Delete)
                {
                    int Index = DataGridSortSummary.SelectedCells[0].RowIndex;
                    DataGridSortSummary.Rows.RemoveAt(Index);
                    DataGridSortSummary.ClearSelection();
                    decimal Weight = 0;
                    decimal Meter = 0;
                    decimal TotalMeter = 0;
                    decimal TotalWeight = 0;
                    for (int i = 0; i < DataGridSortSummary.Rows.Count; i++)
                    {
                        Weight = Convert.ToDecimal(DataGridSortSummary.Rows[i].Cells[5].Value.ToString());
                        Meter = Convert.ToDecimal(DataGridSortSummary.Rows[i].Cells[4].Value.ToString());
                        TotalMeter += Meter;
                        TotalWeight += Weight;
                    }
                    txtTotalMeters.Text = TotalMeter.ToString("0.000");
                    txtTotalWeight.Text = TotalWeight.ToString("0.000");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void BtnPackingSlipPrint_Click(object sender, EventArgs e)
        {
            try
            {

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void DataGridSortRoll_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void DataGridSortRoll_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                LoadTotal();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void ChckAll_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                if (ChckAll.Checked == true)
                {
                    for (int i = 0; i < DataGridSortRoll.RowCount; i++)
                    {
                        DataGridSortRoll[0, i].Value = true;
                    }
                }
                else
                {
                    for (int i = 0; i < DataGridSortRoll.RowCount; i++)
                    {
                        DataGridSortRoll[0, i].Value = false;
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        protected void LoadTotal()
        {
            try
            {
                if (txtTotalFrWght.Text == string.Empty)
                {
                    txtTotalFrWght.Text = "0";
                }
                if (txtFrTotalMtr.Text == string.Empty)
                {
                    txtFrTotalMtr.Text = "0";
                }
                decimal totalMtr = Convert.ToDecimal(txtTotalFrWght.Text);
                decimal totalWght = Convert.ToDecimal(txtFrTotalMtr.Text);
                decimal Weight = 0;
                decimal Meter = 0;
                decimal TotalMeter = 0;
                decimal TotalWeight = 0;
                for (int i = 0; i < DataGridSortRoll.Rows.Count; ++i)
                {
                    if (DataGridSortRoll.Rows[i].Cells["Chck"].Value != null && (bool)DataGridSortRoll.Rows[i].Cells["Chck"].Value)
                    {
                        Weight = Convert.ToDecimal(DataGridSortRoll.Rows[i].Cells[4].Value.ToString());
                        Meter = Convert.ToDecimal(DataGridSortRoll.Rows[i].Cells[3].Value.ToString());
                        TotalMeter += Meter;
                        TotalWeight += Weight;
                    }
                }
                txtFrTotalMtr.Text = TotalMeter.ToString("0.00");
                txtTotalFrWght.Text = TotalWeight.ToString("0.00");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void CmbToSortNo_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                //var bindData = (BindingSource)DataGridSortSummary.DataSource;
                //var dataTable = bindData.DataSource as DataTable;
                //var rows = dataTable.Select(string.Format("SortNo LIKE '%{0}%'", CmbToSortNo.Text));

                //DataGridSortSummary.DataSource = rows.CopyToDataTable();
                //DataGridSortSummary.Refresh();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }
    }
}
