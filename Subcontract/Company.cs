﻿using System;
using System.Data;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace ACV
{
    public partial class Company : Form
    {
        public Company()
        {
            InitializeComponent();
        }
        SqlConnection conn = new SqlConnection("Data Source=" + Genclass.data2 + "; Initial Catalog=" + Genclass.data5 + ";User id=" + Genclass.data3 + ";Password=" + Genclass.data4 + "");
        SqlCommand qur = new SqlCommand();
        private void butnsave_Click(object sender, EventArgs e)
        {
            if (txtname.Text == "")
            {
                MessageBox.Show("Enter the Name");
                txtname.Focus();
                return;
            }
            if (txtstate.Text == "")
            {
                MessageBox.Show("Select the State");
                txtstate.Focus();
                return;
            }
            conn.Open();
            if (Genclass.data1 == 1)
            {
                qur.CommandText = "update partym set name= '" + txtname.Text + "',address1='" + txtadd1.Text + "',address2='" + txtadd2.Text + "',city='" + txtcity.Text + "',pin='" + txtpin.Text + "',tngst='" + tngno.Text + "',phone='" + txtphone.Text + "',emailid='" + txtmail.Text + "' Where companyid=" + Genclass.data1 + "";
            }
            else
            {
                qur.CommandText = "update partym set name= '" + txtname.Text + "',address1='" + txtadd1.Text + "',address2='" + txtadd2.Text + "',city='" + txtcity.Text + "',pin='" + txtpin.Text + "',tngst='" + tngno.Text + "',phone='" + txtphone.Text + "',emailid='" + txtmail.Text + " Where companyid=" + Genclass.data1 + " ";
            }
            qur.ExecuteNonQuery();
            MessageBox.Show("Record has been saved", "Save", MessageBoxButtons.OK);
            conn.Close();
        }

        private void Company_Load(object sender, System.EventArgs e)
        {
            qur.Connection = conn;
            Left = (MdiParent.ClientRectangle.Width - Width) / 3;
            Top = (MdiParent.ClientRectangle.Height - Height) / 3;
            conn.Open();
            if (Genclass.data1 == 1)
            {
                Genclass.strsql = "select  * from partym a inner join generalm b on a.stateuid=b.uid  where a.uid=1210 and a.companyid=" + Genclass.data1 + "";
            }
            else
            {
                Genclass.strsql = "select  * from partym a inner join generalm b on a.stateuid=b.uid  where a.uid=1212 and a.companyid=" + Genclass.data1 + "";
            }
            Genclass.cmd = new SqlCommand(Genclass.strsql, conn);
            SqlDataAdapter aptr = new SqlDataAdapter(Genclass.cmd);
            DataTable tap = new DataTable();
            aptr.Fill(tap);
            if (tap.Rows.Count > 0)
            {
                txtname.Text = tap.Rows[0]["name"].ToString();
                txtadd1.Text = tap.Rows[0]["address1"].ToString();
                txtadd2.Text = tap.Rows[0]["address2"].ToString();
                txtcity.Text = tap.Rows[0]["City"].ToString();
                txtstate.Text = tap.Rows[0]["generalname"].ToString();
                txtpin.Text = tap.Rows[0]["pin"].ToString();
                tngno.Text = tap.Rows[0]["TNGST"].ToString();
                txtmail.Text = tap.Rows[0]["Emailid"].ToString();
                txtphone.Text = tap.Rows[0]["phone"].ToString();
            }
            conn.Close();
        }

        private void buttnext2_Click(object sender, System.EventArgs e)
        {
            this.Close();
        }
    }
}
