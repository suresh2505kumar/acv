﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Windows.Forms;

namespace ACV
{
    public partial class FrmInternalOrder : Form
    {
        public FrmInternalOrder()
        {
            InitializeComponent();
        }
        SQLDBHelper db = new SQLDBHelper();
        BindingSource bsItem = new BindingSource();
        SqlConnection conn = new SqlConnection("Data Source=" + Genclass.data2 + "; Initial Catalog=" + Genclass.data5 + ";User id=" + Genclass.data3 + ";Password=" + Genclass.data4 + "");
        int Fillid = 0;
        int SelectId = 0;
        int DocTypeUid = 0;
        BindingSource bsInternal = new BindingSource();
        private void FrmInternalOrder_Load(object sender, EventArgs e)
        {
            DocTypeUid = 230;
            LoadDatagrid();
            LoadDataInternalGrid();
            Genclass.Module.buttonstyleform(this);
            Genclass.Module.buttonstylepanel(panadd);
            LoadButtons(0);
        }

        protected bool CheckGridValue()
        {
            bool entryFound = false;
            try
            {
                foreach (DataGridViewRow row in DataGridIO.Rows)
                {
                    object val2 = row.Cells[0].Value;
                    if (val2 != null && val2.ToString() == txtItemName.Text)
                    {
                        entryFound = true;
                        break;
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            return entryFound;
        }

        protected void LoadDatagrid()
        {
            try
            {
                DataGridIO.DataSource = null;
                DataGridIO.AutoGenerateColumns = false;
                DataGridIO.ColumnCount = 9;
                DataGridIO.Columns[0].Name = "ItemName";
                DataGridIO.Columns[0].HeaderText = "ItemName";
                DataGridIO.Columns[0].Width = 300;

                DataGridIO.Columns[1].Name = "TO";
                DataGridIO.Columns[1].HeaderText = "To";

                DataGridIO.Columns[2].Name = "SalesOrderNo";
                DataGridIO.Columns[2].HeaderText = "SalesOrderNo";
                DataGridIO.Columns[2].Width = 150;

                DataGridIO.Columns[3].Name = "Quantity";
                DataGridIO.Columns[3].HeaderText = "Quantity";

                DataGridIO.Columns[4].Name = "Req Date";
                DataGridIO.Columns[4].HeaderText = "Req Date";

                DataGridIO.Columns[5].Name = "Notes";
                DataGridIO.Columns[5].HeaderText = "Notes";
                DataGridIO.Columns[5].Width = 150;

                DataGridIO.Columns[6].Name = "ItemUid";
                DataGridIO.Columns[6].HeaderText = "ItemUid";
                DataGridIO.Columns[6].Visible = false;

                DataGridIO.Columns[7].Name = "SoUid";
                DataGridIO.Columns[7].HeaderText = "SoUid";
                DataGridIO.Columns[7].Visible = false;

                DataGridIO.Columns[8].Name = "IOLUid";
                DataGridIO.Columns[8].HeaderText = "IOLUid";
                DataGridIO.Columns[8].Visible = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        protected DataTable getItem()
        {
            DataTable dt = new DataTable();
            try
            {
                dt = db.GetDataWithoutParam(CommandType.StoredProcedure, "SP_GetSorDet", conn);
                bsItem.DataSource = dt;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            return dt;
        }

        protected void FillGrid(DataTable dt, int FillId)
        {
            try
            {
                DataGridCommon.DataSource = null;
                DataGridCommon.AutoGenerateColumns = false;
                if (FillId == 1)
                {
                    Fillid = 1;
                    DataGridCommon.ColumnCount = 3;
                    DataGridCommon.Columns[0].Name = "Uid";
                    DataGridCommon.Columns[0].HeaderText = "Uid";
                    DataGridCommon.Columns[0].DataPropertyName = "Uid";
                    DataGridCommon.Columns[1].Name = "Name";
                    DataGridCommon.Columns[1].HeaderText = "Name";
                    DataGridCommon.Columns[1].DataPropertyName = "SortNo";
                    DataGridCommon.Columns[1].Width = 360;
                    DataGridCommon.DataSource = bsItem;
                    DataGridCommon.Columns[0].Visible = false;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void txtItemName_MouseClick(object sender, MouseEventArgs e)
        {
            try
            {
                DataTable dt = getItem();
                FillGrid(dt, 1);
                Point p = FindLocation(txtItemName);
                panelSearch.Location = new Point(p.X, p.Y + 20);
                panelSearch.Visible = true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private Point FindLocation(Control ctrl)
        {
            if (ctrl.Parent is Form)
                return ctrl.Location;
            else
            {
                Point p = FindLocation(ctrl.Parent);
                p.X += ctrl.Location.X;
                p.Y += ctrl.Location.Y;
                return p;
            }
        }

        protected void LoadButtons(int id)
        {
            try
            {
                if (id == 0)
                {
                    PanelBack.Visible = false;
                    PanelFront.Visible = true;
                    btnAdd.Visible = true;
                    btnEdit.Visible = true;
                    //btnDelete.Visible = true;
                    btnExit.Visible = true;
                    btnPrint.Visible = true;
                    btnFirst.Visible = true;
                    btnBack.Visible = true;
                    btnNxt.Visible = true;
                    btnLast.Visible = true;
                    PanelgridNos.Visible = true;
                    btnSave.Visible = false;
                    btnAddCancel.Visible = false;
                    btnSave.Text = "Save";
                }
                else if (id == 1)
                {
                    PanelBack.Visible = true;
                    PanelFront.Visible = false;
                    btnAdd.Visible = false;
                    btnEdit.Visible = false;
                    //btnDelete.Visible = false;
                    btnExit.Visible = false;
                    btnPrint.Visible = false;
                    btnFirst.Visible = false;
                    btnBack.Visible = false;
                    btnNxt.Visible = false;
                    btnLast.Visible = false;
                    PanelgridNos.Visible = false;
                    btnSave.Visible = true;
                    btnAddCancel.Visible = true;
                    btnSave.Text = "Save";
                }

                else if (id == 2)
                {
                    PanelBack.Visible = true;
                    PanelFront.Visible = false;
                    btnAdd.Visible = false;
                    btnEdit.Visible = false;
                    //btnDelete.Visible = false;
                    btnExit.Visible = false;
                    btnPrint.Visible = false;
                    btnFirst.Visible = false;
                    btnBack.Visible = false;
                    btnNxt.Visible = false;
                    btnLast.Visible = false;
                    PanelgridNos.Visible = false;
                    btnSave.Visible = true;
                    btnAddCancel.Visible = true;
                    btnSave.Text = "Update";
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            LoadButtons(1);
        }

        private void DataGridCommon_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            try
            {
                SelectId = 1;
                int Index = DataGridCommon.SelectedCells[0].RowIndex;
                if (Fillid == 1)
                {
                    txtItemName.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                    txtItemName.Tag = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                    txtSalesOrderNo.Text = "Internal Order";
                    txtSalesOrderNo.Tag = "0";
                }
                panelSearch.Visible = false;
                SelectId = 0;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void btnAddCancel_Click(object sender, EventArgs e)
        {
            LoadButtons(0);
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                if (btnSave.Text == "Save")
                {
                    SqlParameter[] para = {
                        new SqlParameter("@IOUid","0"),
                        new SqlParameter("@DocNo",txtDocNo.Text),
                        new SqlParameter("@DocDate",Convert.ToDateTime(dtpDocDate.Text)),
                        new SqlParameter("@Remarks",txtRemarks.Text),
                        new SqlParameter("@Tag","Insert"),
                        new SqlParameter("@Returnid",SqlDbType.Int),
                        new SqlParameter("@DoctypeUid",DocTypeUid),
                    };
                    para[5].Direction = ParameterDirection.Output;
                    int ReturnId = db.ExecuteNonQuery(CommandType.StoredProcedure, "SP_InternalOrder", para, conn, 5);

                    for (int i = 0; i < DataGridIO.Rows.Count - 1; i++)
                    {
                        SqlParameter[] paraDet = {
                            new SqlParameter("@IOUid",ReturnId),
                            new SqlParameter("@RefUid",Convert.ToInt32(DataGridIO.Rows[i].Cells[7].Value.ToString())),
                            new SqlParameter("@ItemUid",Convert.ToInt32(DataGridIO.Rows[i].Cells[6].Value.ToString())),
                            new SqlParameter("@Remarks",DataGridIO.Rows[i].Cells[5].Value.ToString()),
                            new SqlParameter("@ReqDate",Convert.ToDateTime(DataGridIO.Rows[i].Cells[4].Value.ToString())),
                            new SqlParameter("@Qty",Convert.ToDecimal(DataGridIO.Rows[i].Cells[3].Value.ToString())),
                            new SqlParameter("@IOLUid",Convert.ToInt32(DataGridIO.Rows[i].Cells[8].Value.ToString())),
                            new SqlParameter("@OrderTo",DataGridIO.Rows[i].Cells[1].Value.ToString())
                        };
                        db.ExecuteNonQuery(CommandType.StoredProcedure, "SP_InternalOrderList", paraDet, conn);
                    }
                }
                else
                {
                    SqlParameter[] para = {
                        new SqlParameter("@IOUid",txtDocNo.Tag),
                        new SqlParameter("@DocNo",txtDocNo.Text),
                        new SqlParameter("@DocDate",Convert.ToDateTime(dtpDocDate.Text)),
                        new SqlParameter("@Remarks",txtRemarks.Text),
                        new SqlParameter("@Tag","Update"),
                        new SqlParameter("@Returnid",SqlDbType.Int),
                        new SqlParameter("@DoctypeUid",DocTypeUid),
                    };
                    para[5].Direction = ParameterDirection.Output;
                    int ReturnId = db.ExecuteNonQuery(CommandType.StoredProcedure, "SP_InternalOrder", para, conn, 5);

                    for (int i = 0; i < DataGridIO.Rows.Count - 1; i++)
                    {
                        SqlParameter[] paraDet = {
                            new SqlParameter("@IOUid",txtDocNo.Tag),
                            new SqlParameter("@RefUid",Convert.ToInt32(DataGridIO.Rows[i].Cells[7].Value.ToString())),
                            new SqlParameter("@ItemUid",Convert.ToInt32(DataGridIO.Rows[i].Cells[6].Value.ToString())),
                            new SqlParameter("@Remarks",DataGridIO.Rows[i].Cells[5].Value.ToString()),
                            new SqlParameter("@ReqDate",Convert.ToDateTime(DataGridIO.Rows[i].Cells[4].Value.ToString())),
                            new SqlParameter("@Qty",Convert.ToDecimal(DataGridIO.Rows[i].Cells[3].Value.ToString())),
                            new SqlParameter("@IOLUid",Convert.ToInt32(DataGridIO.Rows[i].Cells[8].Value.ToString())),
                            new SqlParameter("@OrderTo",DataGridIO.Rows[i].Cells[1].Value.ToString())
                        };
                        db.ExecuteNonQuery(CommandType.StoredProcedure, "SP_InternalOrderList", paraDet, conn);
                    }
                }
                MessageBox.Show("Record Has been Saved", "Infromation", MessageBoxButtons.OK, MessageBoxIcon.Information);
                Genclass.Module.ClearTextBox(this, PanelBack);
                DataGridIO.Rows.Clear();
                LoadDataInternalGrid();
                LoadButtons(0);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            try
            {
                txtTotalQty.Text = "0";
                int Index = DataGridInOrder.SelectedCells[0].RowIndex;
                int IOUid = Convert.ToInt32(DataGridInOrder.Rows[Index].Cells[0].Value.ToString());
                SqlParameter[] para = {
                    new SqlParameter("@Tag","Edit"),
                    new SqlParameter("@IOUid",IOUid)
                };
                DataSet ds = db.GetDataWithParamMultiple(CommandType.StoredProcedure, "SP_GetInternalOrder", para, conn);
                DataTable dt = ds.Tables[0];
                DataTable dt1 = ds.Tables[1];
                txtDocNo.Text = dt.Rows[0]["DocNo"].ToString();
                txtDocNo.Tag = dt.Rows[0]["IOUid"].ToString();
                dtpDocDate.Text = dt.Rows[0]["DocDate"].ToString();
                txtRemarks.Text = dt.Rows[0]["Remarks"].ToString();
                if (txtTotalQty.Text == string.Empty)
                {
                    txtTotalQty.Text = "0";
                }
                decimal totalQty = Convert.ToDecimal(txtTotalQty.Text);
                DataGridIO.Rows.Clear();
                for (int j = 0; j < dt1.Rows.Count; j++)
                {
                    DataGridViewRow row = (DataGridViewRow)DataGridIO.Rows[0].Clone();
                    row.Cells[0].Value = dt1.Rows[j]["ItemName"].ToString();
                    row.Cells[1].Value = dt1.Rows[j]["OrderTo"].ToString();
                    row.Cells[2].Value = dt1.Rows[j]["SalesOrderNo"].ToString();
                    row.Cells[3].Value = dt1.Rows[j]["Qty"].ToString();
                    row.Cells[4].Value = Convert.ToDateTime(dt1.Rows[j]["ReqDate"].ToString()).ToString("dd-MMM-yyyy");
                    row.Cells[5].Value = dt1.Rows[j]["Notes"].ToString();
                    row.Cells[6].Value = dt1.Rows[j]["ItemUid"].ToString();
                    row.Cells[7].Value = dt1.Rows[j]["SOUid"].ToString();
                    row.Cells[8].Value = dt1.Rows[j]["IOLUid"].ToString();
                    DataGridIO.Rows.Add(row);
                }
                for (int i = 0; i < DataGridIO.Rows.Count - 1; i++)
                {
                    totalQty += Convert.ToDecimal(DataGridIO.Rows[i].Cells[3].Value.ToString());
                }
                txtTotalQty.Text = totalQty.ToString();
                LoadButtons(2);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void BtnSelect_Click(object sender, EventArgs e)
        {
            try
            {
                if (txtTotalQty.Text == string.Empty)
                {
                    txtTotalQty.Text = "0";
                }
                decimal totalQty = Convert.ToDecimal(txtTotalQty.Text);
                if (txtItemName.Text != string.Empty || txtQty.Text != string.Empty)
                {
                    DataGridViewRow row = (DataGridViewRow)DataGridIO.Rows[0].Clone();
                    row.Cells[0].Value = txtItemName.Text;
                    row.Cells[1].Value = comboBox1.Text;
                    row.Cells[2].Value = txtSalesOrderNo.Text;
                    row.Cells[3].Value = txtQty.Text;
                    row.Cells[4].Value = dtpReqDate.Text;
                    row.Cells[5].Value = txtNotes.Text;
                    row.Cells[6].Value = txtItemName.Tag;
                    row.Cells[7].Value = txtSalesOrderNo.Tag;
                    row.Cells[8].Value = "0";
                    DataGridIO.Rows.Add(row);
                    txtTotalQty.Text = (totalQty + Convert.ToDecimal(txtQty.Text)).ToString();
                    ClearTextbox();
                }
                else
                {
                    MessageBox.Show("Enter item and Quantity", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        protected void ClearTextbox()
        {
            SelectId = 1;
            txtItemName.Text = string.Empty;
            txtItemName.Tag = string.Empty;
            txtSalesOrderNo.Text = string.Empty;
            txtSalesOrderNo.Tag = string.Empty;
            txtQty.Text = string.Empty;
            txtNotes.Text = string.Empty;
            SelectId = 0;
        }

        private void TxtItemName_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (SelectId == 0)
                {
                    bsItem.Filter = string.Format("SortNo LIKE '%{0}%' ", txtItemName.Text);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void BtnHide_Click(object sender, EventArgs e)
        {
            panelSearch.Visible = false;
        }

        protected void LoadDataInternalGrid()
        {
            try
            {
                SqlParameter[] para = {
                    new SqlParameter("@Tag","ALL"),
                    new SqlParameter("@IOUid","0")
                };
                DataTable dt = db.GetDataWithParam(CommandType.StoredProcedure, "SP_GetInternalOrder", para, conn);
                bsInternal.DataSource = dt;
                DataGridInOrder.DataSource = null;
                DataGridInOrder.AutoGenerateColumns = false;
                DataGridInOrder.ColumnCount = 5;
                DataGridInOrder.Columns[0].Name = "IOUid";
                DataGridInOrder.Columns[0].HeaderText = "IOUid";
                DataGridInOrder.Columns[0].DataPropertyName = "IOUid";
                DataGridInOrder.Columns[0].Visible = false;
                DataGridInOrder.Columns[1].Name = "DocNo";
                DataGridInOrder.Columns[1].HeaderText = "DocNo";
                DataGridInOrder.Columns[1].DataPropertyName = "DocNo";
                DataGridInOrder.Columns[1].Width = 150;
                DataGridInOrder.Columns[2].Name = "DocDtae";
                DataGridInOrder.Columns[2].HeaderText = "DocDtae";
                DataGridInOrder.Columns[2].DataPropertyName = "DocDate";
                DataGridInOrder.Columns[2].Width = 150;
                DataGridInOrder.Columns[3].Name = "SortNo";
                DataGridInOrder.Columns[3].HeaderText = "SortNo";
                DataGridInOrder.Columns[3].DataPropertyName = "Remarks";
                DataGridInOrder.Columns[3].Width = 330;
                DataGridInOrder.Columns[4].Name = "Qty";
                DataGridInOrder.Columns[4].HeaderText = "Qty";
                DataGridInOrder.Columns[4].DataPropertyName = "Qty";
                DataGridInOrder.Columns[4].Width = 150;
                DataGridInOrder.Columns[4].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                DataGridInOrder.Columns[4].DefaultCellStyle.Format = "N4";
                DataGridInOrder.DataSource = bsInternal;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void BtnPrint_Click(object sender, EventArgs e)
        {

        }

        private void txtGridSearch_TextChanged(object sender, EventArgs e)
        {
            try
            {
                bsInternal.Filter = string.Format("DocNo LIKE '%{0}%' or Remarks LIKE '%{0}%' ", txtGridSearch.Text);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information",MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }
    }
}
