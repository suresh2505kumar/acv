﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ACV
{
    public partial class FrmKnottingStatus : Form
    {
        public FrmKnottingStatus()
        {
            InitializeComponent();
            sfDataGridRunout.Style.HeaderStyle.BackColor = Color.DeepSkyBlue;
            sfDataGridKnotting.Style.HeaderStyle.BackColor = Color.DeepSkyBlue;
        }
        SqlConnection conn = new SqlConnection("Data Source=" + Genclass.data2 + "; Initial Catalog=" + Genclass.data5 + ";User id=" + Genclass.data3 + ";Password=" + Genclass.data4 + "");
        SQLDBHelper db = new SQLDBHelper();
        private void BtnOk_Click(object sender, EventArgs e)
        {
            try
            {
                SqlParameter[] sqlParameters = {
                    new SqlParameter("@DT", Convert.ToDateTime(DtpFromDate.Text).ToString("yyyy-MM-dd")),
                    new SqlParameter("@TDT", Convert.ToDateTime(DtpToDate.Text).ToString("yyyy-MM-dd"))
                };
                DataSet dataSet = db.GetDataWithParamMultiple(CommandType.StoredProcedure, "PROC_RPT_GETKNOTTINGDATA", sqlParameters, conn);
                sfDataGridRunout.DataSource = null;
                sfDataGridKnotting.DataSource = null;
                sfDataGridRunout.DataSource = dataSet.Tables[0];
                sfDataGridKnotting.DataSource = dataSet.Tables[1];
                sfDataGridRunout.AutoSizeColumnsMode = Syncfusion.WinForms.DataGrid.Enums.AutoSizeColumnsMode.AllCells;
                sfDataGridKnotting.AutoSizeColumnsMode = Syncfusion.WinForms.DataGrid.Enums.AutoSizeColumnsMode.AllCells;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.StackTrace, MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void BtnExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void sfDataGridRunout_QueryCellStyle(object sender, Syncfusion.WinForms.DataGrid.Events.QueryCellStyleEventArgs e)
        {
            try
            {
                //if(e.Column.MappingName == "")
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.StackTrace, MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }
    }
}
