﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Configuration;
using System.Data.SqlClient;
using System.Data.OleDb;


namespace ACV
{
    public partial class FrmlookupItem : Form
    {

        public FrmlookupItem()
        {
            InitializeComponent();
        }
        SqlConnection conn = new SqlConnection("Data Source=" + Genclass.data2 + "; Initial Catalog=" + Genclass.data5 + ";User id=" + Genclass.data3 + ";Password=" + Genclass.data4 + "");
        SqlCommand qur = new SqlCommand();
        private void tabPage2_Click(object sender, EventArgs e)
        {
            FrmInvoice cont = new FrmInvoice();
            TextBox cont1 = (TextBox)cont.Controls["txtdcid"];

            Genclass.fieldtwo = cont1;
            txtscr3.Focus();
        }

        private void btnadd_Click(object sender, EventArgs e)
        {
            Genclass.ty = 1;
            txtinp();


        }

        private void btnaddrcan_Click(object sender, EventArgs e)
        {
            Genclass.canclclik = true;
            this.Dispose();

        }

        private void FrmlookupItem_Load(object sender, EventArgs e)
        {
            qur.Connection = conn;
            this.Hfgp.DefaultCellStyle.Font = new Font("calibri", 10);
            this.Hfgp.ColumnHeadersDefaultCellStyle.Font = new Font("calibri", 10, FontStyle.Bold);
            this.HFGP2.DefaultCellStyle.Font = new Font("calibri", 10);
            this.HFGP2.ColumnHeadersDefaultCellStyle.Font = new Font("calibri", 10, FontStyle.Bold);
            this.HFGP3.DefaultCellStyle.Font = new Font("calibri", 10);
            this.HFGP3.ColumnHeadersDefaultCellStyle.Font = new Font("calibri", 10, FontStyle.Bold);
            this.HFGP4.DefaultCellStyle.Font = new Font("calibri", 10);
            this.HFGP4.ColumnHeadersDefaultCellStyle.Font = new Font("calibri", 10, FontStyle.Bold);
            HFGP4.RowHeadersVisible = false;

            //Titlep1();
            Genclass.canclclik = false;
            this.CenterToScreen();
            Hfgp.RowHeadersVisible = false;
            HFGP2.RowHeadersVisible = false;
            HFGP3.RowHeadersVisible = false;
            HFGP4.RowHeadersVisible = false;
            Genclass.Module.buttonstyleform(this);
            //tabC.Visible = true;


            //txtscr1.Focus();
        }
        public void Loadgrp()
        {
            conn.Open();
            string qur = "select distinct b.uid,b.ItemGroup from itemm a inner join ItemGroup b on a.ItemGroup_UId=b.UId";
            SqlCommand cmd = new SqlCommand(qur, conn);
            SqlDataAdapter apt = new SqlDataAdapter(cmd);
            DataTable tab = new DataTable();
            apt.Fill(tab);
            cbogrp.DataSource = null;
            cbogrp.DataSource = tab;
            cbogrp.DisplayMember = "Itemgroup";
            cbogrp.ValueMember = "uid";
            //cbogrp.SelectedIndex = -1;
            conn.Close();
        }



        private void txtinp()
        {
            //int columnIndex = Hfgp.CurrentCell.ColumnIndex;
            //string columnName = Hfgp.Columns[columnIndex].Name;
            ////if (Hfgp.CurrentCell.ColumnIndex == 0)

            ////{
            ////}    

            foreach (Form Frm in Application.OpenForms)
            {
                if (Frm.Name == Genclass.frmwhat)
                {
                    foreach (Control ccontrol in Frm.Controls)
                    {

                        if (ccontrol is Panel)
                        {

                            foreach (Control c in Genclass.Parent.Controls)
                            {
                                if (c is TextBox)
                                {

                                    if (c.Name == (string)Genclass.fieldone)
                                    {

                                        int i = Hfgp.SelectedCells[0].RowIndex;
                                        c.Text = Hfgp.Rows[i].Cells[0].Value.ToString();
                                    }
                                    else if (c.Name == (string)Genclass.fieldtwo)
                                    {
                                        int i = Hfgp.SelectedCells[0].RowIndex;
                                        c.Text = Hfgp.Rows[i].Cells[1].Value.ToString();
                                    }
                                    else if (c.Name == (string)Genclass.fieldthree)
                                    {
                                        int i = Hfgp.SelectedCells[0].RowIndex;
                                        c.Text = Hfgp.Rows[i].Cells[2].Value.ToString();
                                    }
                                    else if (c.Name == (string)Genclass.fieldFour)
                                    {
                                        int i = Hfgp.SelectedCells[0].RowIndex;
                                        c.Text = Hfgp.Rows[i].Cells[3].Value.ToString();
                                    }
                                    else if (c.Name == (string)Genclass.fieldFive)
                                    {
                                        int i = Hfgp.SelectedCells[0].RowIndex;
                                        c.Text = Hfgp.Rows[i].Cells[4].Value.ToString();
                                    }

                                }
                            }
                        }

                    }
                }

            }

            this.Dispose();
        }

        private void txtinp1()
        {
            foreach (Form Frm in Application.OpenForms)
            {
                if (Frm.Name == Genclass.frmwhat)
                {
                    foreach (Control ccontrol in Frm.Controls)
                    {

                        if (ccontrol is Panel)
                        {

                            foreach (Control c in Genclass.Parent.Controls)
                            {
                                if (c is TextBox)
                                {

                                    if (c.Name == (string)Genclass.fieldone)
                                    {

                                        int i = HFGP4.SelectedCells[0].RowIndex;
                                        c.Text = HFGP4.Rows[i].Cells[0].Value.ToString();
                                    }
                                    else if (c.Name == (string)Genclass.fieldtwo)
                                    {
                                        int i = HFGP4.SelectedCells[0].RowIndex;
                                        c.Text = HFGP4.Rows[i].Cells[1].Value.ToString();
                                    }
                                    else if (c.Name == (string)Genclass.fieldthree)
                                    {
                                        int i = HFGP4.SelectedCells[0].RowIndex;
                                        c.Text = HFGP4.Rows[i].Cells[2].Value.ToString();
                                    }
                                    else if (c.Name == (string)Genclass.fieldFour)
                                    {
                                        int i = HFGP4.SelectedCells[0].RowIndex;
                                        c.Text = HFGP4.Rows[i].Cells[3].Value.ToString();
                                    }
                                    else if (c.Name == (string)Genclass.fieldFive)
                                    {
                                        int i = HFGP4.SelectedCells[0].RowIndex;
                                        c.Text = HFGP4.Rows[i].Cells[4].Value.ToString();
                                    }
                                    else if (c.Name == (string)Genclass.fieldSix)
                                    {
                                        int i = HFGP4.SelectedCells[0].RowIndex;
                                        c.Text = HFGP4.Rows[i].Cells[5].Value.ToString();
                                    }

                                }
                            }
                        }

                    }
                }

            }

            this.Dispose();
        }

        private void txtinp2()
        {
            foreach (Form Frm in Application.OpenForms)
            {
                if (Frm.Name == Genclass.frmwhat)
                {
                    foreach (Control ccontrol in Frm.Controls)
                    {

                        if (ccontrol is Panel)
                        {

                            foreach (Control c in Genclass.Parent.Controls)
                            {
                                if (c is TextBox)
                                {

                                    if (c.Name == (string)Genclass.fieldone)
                                    {

                                        int i = HFGP2.SelectedCells[0].RowIndex;
                                        c.Text = HFGP2.Rows[i].Cells[0].Value.ToString();
                                    }
                                    else if (c.Name == (string)Genclass.fieldtwo)
                                    {
                                        int i = HFGP2.SelectedCells[0].RowIndex;
                                        c.Text = HFGP2.Rows[i].Cells[1].Value.ToString();
                                    }
                                    else if (c.Name == (string)Genclass.fieldthree)
                                    {
                                        int i = HFGP2.SelectedCells[0].RowIndex;
                                        c.Text = HFGP2.Rows[i].Cells[2].Value.ToString();
                                    }
                                    else if (c.Name == (string)Genclass.fieldFour)
                                    {
                                        int i = HFGP2.SelectedCells[0].RowIndex;
                                        c.Text = HFGP2.Rows[i].Cells[3].Value.ToString();
                                    }
                                    else if (c.Name == (string)Genclass.fieldFive)
                                    {
                                        int i = HFGP2.SelectedCells[0].RowIndex;
                                        c.Text = HFGP2.Rows[i].Cells[4].Value.ToString();
                                    }

                                }
                            }
                        }

                    }
                }

            }

            this.Dispose();
        }

        private void txtinp3()
        {
            foreach (Form Frm in Application.OpenForms)
            {
                if (Frm.Name == Genclass.frmwhat)
                {
                    foreach (Control ccontrol in Frm.Controls)
                    {

                        if (ccontrol is Panel)
                        {

                            foreach (Control c in Genclass.Parent.Controls)
                            {
                                if (c is TextBox)
                                {

                                    if (c.Name == (string)Genclass.fieldone)
                                    {

                                        int i = HFGP3.SelectedCells[0].RowIndex;
                                        c.Text = HFGP3.Rows[i].Cells[0].Value.ToString();
                                    }
                                    else if (c.Name == (string)Genclass.fieldtwo)
                                    {
                                        int i = HFGP3.SelectedCells[0].RowIndex;
                                        c.Text = HFGP3.Rows[i].Cells[1].Value.ToString();
                                    }
                                    else if (c.Name == (string)Genclass.fieldthree)
                                    {
                                        int i = HFGP3.SelectedCells[0].RowIndex;
                                        c.Text = HFGP3.Rows[i].Cells[2].Value.ToString();
                                    }
                                    else if (c.Name == (string)Genclass.fieldFour)
                                    {
                                        int i = HFGP3.SelectedCells[0].RowIndex;
                                        c.Text = HFGP3.Rows[i].Cells[3].Value.ToString();
                                    }
                                    else if (c.Name == (string)Genclass.fieldFive)
                                    {
                                        int i = HFGP3.SelectedCells[0].RowIndex;
                                        c.Text = HFGP3.Rows[i].Cells[4].Value.ToString();
                                    }

                                }
                            }
                        }

                    }
                }

            }

            this.Dispose();
        }
        private void lookupload()
        {
            try
            {

                //conn.Open();


                Genclass.strsqltmp = Genclass.strsql3;
                //Genclass.FSSQLSortStrtmp = Genclass.FSSQLSortStr;
                //Genclass.FSSQLSortStrtmp1 = Genclass.FSSQLSortStr1;
                Genclass.StrSrch = "";
                Genclass.StrSrch1 = "";



                //if (txtscr7.Text != "" || txtscr8.Text != "")
                //{
                if (Genclass.StrSrch == "")
                {
                    Genclass.StrSrch = Genclass.FSSQLSortStr6 + " like '%" + txtscr7.Text + "%'";
                    Genclass.StrSrch1 = Genclass.FSSQLSortStr7 + " like '%" + txtscr8.Text + "%'";
                }
                else
                {
                    Genclass.StrSrch = Genclass.StrSrch + " and " + Genclass.FSSQLSortStr6 + " like '%" + txtscr7.Text + "%'";
                    Genclass.StrSrch1 = Genclass.StrSrch1 + " and " + Genclass.FSSQLSortStr7 + " like '%" + txtscr8.Text + "%'";
                }

                //}


                if (txtscr7.Text != "" || txtscr8.Text != "")
                {
                    if (cbogrp.Visible == false)
                    {
                        if (txtscr7.Text != "")
                        {
                            Genclass.StrSrch = " " + Genclass.StrSrch;
                        }
                        else
                        {
                            Genclass.StrSrch1 = " " + Genclass.StrSrch1;
                        }
                    }
                    if (txtscr7.Text != "")
                    {
                        Genclass.StrSrch = " " + Genclass.StrSrch;
                    }
                    else
                    {
                        Genclass.StrSrch1 = " " + Genclass.StrSrch1;
                    }

                }

                //if (txtscr7.Text != "")
                //{

                if (Genclass.data1 == 1)
                {

                    Genclass.strsqltmp = "select distinct top 30  uid,c.itemname,c.itemcode,isnull(bb.price,0) as Price,isnull(bb.qty,0) as Qty FROM itemm c   left join Pgsotmp bb on c.uid=bb.itemuid where " + Genclass.StrSrch + " and  " + Genclass.StrSrch1 + " and active=1 and companyid=" + Genclass.data1 + "";

                    Genclass.cmd = new SqlCommand(Genclass.strsqltmp, conn);
                }

                else
                {
                    Genclass.strsqltmp = "select distinct top 30 uid,itemcode,c.itemname,isnull(bb.price,0) as Price,isnull(bb.qty,0) as Qty FROM itemm c left join sotmp bb on c.uid=bb.itemuid where " + Genclass.StrSrch + " and  " + Genclass.StrSrch1 + "  and active=1 and companyid=" + Genclass.data1 + "";

                    Genclass.cmd = new SqlCommand(Genclass.strsqltmp, conn);
                }




                //}
                //else if (txtscr7.Text == "")
                //{
                //    Genclass.strsqltmp = Genclass.strsqltmp +" order by " + Genclass.FSSQLSortStr + "";
                //    Genclass.cmd = new SqlCommand(Genclass.strsqltmp, conn);


                //}
                //else if (txtscr8.Text != "")
                //{
                //    Genclass.strsqltmp = Genclass.strsqltmp + " and  " + Genclass.StrSrch1 + " order by " + Genclass.FSSQLSortStr1 + "";
                //    Genclass.cmd = new SqlCommand(Genclass.strsqltmp, conn);


                //}
                //else
                //{
                //     Genclass.strsqltmp = Genclass.strsqltmp + " order by " + Genclass.FSSQLSortStr1 + "";
                //    Genclass.cmd = new SqlCommand(Genclass.strsqltmp, conn);


                //}

                SqlDataAdapter aptr = new SqlDataAdapter(Genclass.cmd);
                DataTable tap = new DataTable();
                aptr.Fill(tap);


                this.HFGP3.DefaultCellStyle.Font = new Font("Arial", 10);
                this.HFGP3.ColumnHeadersDefaultCellStyle.Font = new Font("Arial", 10, FontStyle.Bold);
                HFGP3.AutoGenerateColumns = false;
                HFGP3.DataSource = null;
                HFGP3.ColumnCount = tap.Columns.Count;
                Genclass.i = 0;
                foreach (DataColumn column in tap.Columns)
                {
                    HFGP3.Columns[Genclass.i].Name = column.ColumnName;
                    HFGP3.Columns[Genclass.i].HeaderText = column.ColumnName;
                    HFGP3.Columns[Genclass.i].DataPropertyName = column.ColumnName;
                    Genclass.i = Genclass.i + 1;
                }

                if (Genclass.data1 == 1)
                {
                    HFGP3.Columns[1].Width = 300;
                    HFGP3.Columns[2].Width = 130;
                }
                else
                {
                    HFGP3.Columns[1].Width = 130;
                    HFGP3.Columns[2].Width = 300;
                }
                HFGP3.Columns[3].Width = 100;
                HFGP3.Columns[4].Width = 100;


                HFGP3.DataSource = tap;
                //txtscr7.Focus();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return;
            }
            finally
            {
                conn.Close();
                Genclass.cmd.Dispose();
            }
        }
        private void HFGP3_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {


        }

        private void HFGP3_CellEnter(object sender, DataGridViewCellEventArgs e)
        {
            //txtscr7.Focus();
        }

        private void HFGP2_CellEnter(object sender, DataGridViewCellEventArgs e)
        {
            //txtscr5.Focus();
        }



        private void Hfgp_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void HFGP2_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void tabPage1_Click(object sender, EventArgs e)
        {
            //FrmInvoice cont = new FrmInvoice();

            //TextBox cont1 = (TextBox)cont.Controls["txtdcid"];

            //Genclass.fieldone = cont1;
            //txtscr1.Focus();

        }

        private void tabPage3_Click(object sender, EventArgs e)
        {
            txtscr5.Focus();
        }

        private void tabPage4_Click(object sender, EventArgs e)
        {
            txtscr7.Focus();
        }

        private void Hfgp_DoubleClick(object sender, EventArgs e)
        {
            txtinp();
        }



        private void HFGP2_DoubleClick(object sender, EventArgs e)
        {
            txtinp();
        }

        //private void HFGP3_DoubleClick(object sender, EventArgs e)
        //{
        //    txtinp();
        //}

        private void txtscr1_TextChanged(object sender, EventArgs e)
        {
            //lookupload1();
        }

        private void txtscr2_TextChanged(object sender, EventArgs e)
        {
            //lookupload1();
        }

        private void txtscr3_TextChanged(object sender, EventArgs e)
        {
            lookupload2();
        }

        private void txtscr4_TextChanged(object sender, EventArgs e)
        {
            lookupload2();
        }

        private void txtscr5_TextChanged(object sender, EventArgs e)
        {
            lookupload3();
        }

        private void txtscr6_TextChanged(object sender, EventArgs e)
        {
            lookupload3();
        }

        private void txtscr7_TextChanged(object sender, EventArgs e)
        {
            lookupload();
        }

        private void txtscr8_TextChanged(object sender, EventArgs e)
        {
            lookupload();
        }

        private void Hfgp_Click(object sender, EventArgs e)
        {
            Genclass.ty = 1;
            txtinp();
        }




        //private void HFGP2_Click(object sender, EventArgs e)
        //{

        //}

        private void HFGP3_Click(object sender, EventArgs e)
        {
            //Genclass.ty3 = 4;
            //txtinp3();
        }

        private void tabC_SelectedIndexChanged(object sender, EventArgs e)
        {
            //    Titlep1();
            //    Hfgp.RowHeadersVisible = false;

            //    if (Genclass.data1 == 1)
            //    {
            //        Genclass.Module.Partylistviewcont1("uid", "itemname", "itemcode", "docno", "docdate", Genclass.strsql, this, txttitemid, txtitemname, txtitemcode,txtsaleorderno , txtsalesdate, Editpan);
            //        Genclass.strsql = "select itemid,itemname,itemcode,docno,docdate from (select a.UId,a.DocNo,a.docdate,isnull(b.PQty,0)-isnull(c.PQty,0)-isnull(d.PQty,0) as balqty,f.uid as itemid,f.ItemName,f.ItemCode,b.uid as listid from sTransactionsP a inner join sTransactionsPList b on a.UId=b.TransactionsPUId   and b.DocTypeID = 80    left join Stransactionsplist  d on b.uid=  d.Refuid  and d.doctypeid=40 left join  TransactionsPList c on b.uid=c.Refuid  and c.DocTypeID=20 left join Stransactionsplist e on c.Uid=e.refuid and e.doctypeid=40  left join itemm f on b.ItemUId=f.Uid left join partym h on a.partyuid=h.uid  where a.partyuid=" + txtpuid.Text + " and a.companyid=" + Genclass.data1 + "  group  by  a.uid,a.docno,b.pqty,d.pqty,c.pqty,f.ItemName,f.ItemCode,a.docdate,f.uid,b.uid having isnull(b.PQty,0)-isnull(c.PQty,0)-isnull(d.PQty,0) >0 ) tab";
            //        Genclass.FSSQLSortStr = "itemname";
            //        Genclass.FSSQLSortStr1 = "itemcode";
            //    }

            //    else
            //    {
            //        Genclass.Module.Partylistviewcont7("itemcode", "itemname", "Pono", "Podate", "Qty", "Listid", "uid", Genclass.strsql, this, txtitemcode, txtitemname, txtsaleorderno, txtsalesdate, txtqty, txtlisid, txttitemid, Editpan);
            //        if (Genclass.STR == "")
            //        {
            //            Genclass.strsql = "select distinct itemcode,itemname,Pono,Podate,Qty,listid,itemid from (select aa.*,bb.reqdate as Podate from (select a.UId,a.transp as Pono,isnull(b.PQty,0)-isnull(c.PQty,0)-isnull(d.PQty,0) as qty,f.uid as itemid,f.ItemName,f.ItemCode,b.uid as listid from sTransactionsP a inner join sTransactionsPList b on a.UId=b.TransactionsPUId   and b.DocTypeID = 80    left join Stransactionsplist  d on b.uid=  d.Refuid  and d.doctypeid=40 left join  TransactionsPList c on b.uid=c.Refuid  and c.DocTypeID=20 left join Stransactionsplist e on c.Uid=e.refuid and e.doctypeid=40  left join itemm f on b.ItemUId=f.Uid left join ordreqqty o on a.uid=o.tpuid where a.partyuid=" + txtpuid.Text + " and a.companyid=" + Genclass.data1 + "  group  by  a.uid,a.transp,b.pqty,d.pqty,c.pqty,f.ItemName,f.ItemCode,f.uid,b.uid having isnull(b.PQty,0)-isnull(c.PQty,0)-isnull(d.PQty,0) >0) aa inner join  (select c.tpuid,c.itid,c.reqdate from STransactionsP a inner join STransactionsPlist b on a.uid=b.TransactionsPUId and a.Doctypeid=80 and a.companyid=3 left join Ordreqqty c on a.uid=c.Tpuid and b.ItemUId=c.itid)bb on aa.uid=bb.tpuid and aa.itemid=bb.itid ) tab order by Podate";
            //         Genclass.cmd = new SqlCommand( Genclass.strsql, conn);
            //        }
            //        else
            //        {

            //            Genclass.strsql = "select distinct itemcode,itemname,Pono,Podate,Qty,listid,itemid from (select aa.*,bb.reqdate as Podate from (select a.UId,a.transp as Pono,isnull(b.PQty,0)-isnull(c.PQty,0)-isnull(d.PQty,0) as qty,f.uid as itemid,f.ItemName,f.ItemCode,b.uid as listid from sTransactionsP a inner join sTransactionsPList b on a.UId=b.TransactionsPUId   and b.DocTypeID = 80    left join Stransactionsplist  d on b.uid=  d.Refuid  and d.doctypeid=40 left join  TransactionsPList c on b.uid=c.Refuid  and c.DocTypeID=20 left join Stransactionsplist e on c.Uid=e.refuid and e.doctypeid=40  left join itemm f on b.ItemUId=f.Uid left join ordreqqty o on a.uid=o.tpuid where a.partyuid=" + txtpuid.Text + " and a.companyid=" + Genclass.data1 + " and b.uid not in (" + Genclass.STR + ")  group  by  a.uid,a.transp,b.pqty,d.pqty,c.pqty,f.ItemName,f.ItemCode,f.uid,b.uid having isnull(b.PQty,0)-isnull(c.PQty,0)-isnull(d.PQty,0) >0) aa inner join  (select c.tpuid,c.itid,c.reqdate from STransactionsP a inner join STransactionsPlist b on a.uid=b.TransactionsPUId and a.Doctypeid=80 and a.companyid=3 left join Ordreqqty c on a.uid=c.Tpuid and b.ItemUId=c.itid)bb on aa.uid=bb.tpuid and aa.itemid=bb.itid ) tab order by Podate";
            //         Genclass.cmd = new SqlCommand( Genclass.strsql, conn);
            //        }
            //        Genclass.FSSQLSortStr = "itemcode";
            //        Genclass.FSSQLSortStr1 = "itemname";



            //        SqlDataAdapter aptr = new SqlDataAdapter(Genclass.cmd);
            //        DataTable tap = new DataTable();
            //        aptr.Fill(tap);


            //        Hfgp.AutoGenerateColumns = false;
            //        Hfgp.Refresh();
            //        Hfgp.DataSource = null;
            //        Hfgp.Rows.Clear();


            //        Hfgp.ColumnCount = tap.Columns.Count+1;
            //        Genclass.i = 1;
            //        foreach (DataColumn column in tap.Columns)
            //        {
            //            Hfgp.Columns[Genclass.i].Name = column.ColumnName;
            //            Hfgp.Columns[Genclass.i].HeaderText = column.ColumnName;
            //            Hfgp.Columns[Genclass.i].DataPropertyName = column.ColumnName;
            //            Genclass.i = Genclass.i + 1;

            //        }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Dispose();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            this.Dispose();
        }

        private void button6_Click(object sender, EventArgs e)
        {
            this.Dispose();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            txtinp1();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            txtinp2();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            txtinp3();
        }

        private void tabPage13_Click(object sender, EventArgs e)
        {

        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            txtinp1();
        }

        private void button5_Click_1(object sender, EventArgs e)
        {
            Genclass.ty = 4;
            FrmInvoiceM master = (FrmInvoiceM)Application.OpenForms["FrmInvoiceM"];
            master.buttcusok.PerformClick();

        }

        private void button3_Click_1(object sender, EventArgs e)
        {
            Genclass.ty = 3;
            FrmInvoiceM master = (FrmInvoiceM)Application.OpenForms["FrmInvoiceM"];
            master.buttcusok.PerformClick();

        }

        private void button4_Click_1(object sender, EventArgs e)
        {
            this.Dispose();
        }

        private void button2_Click_1(object sender, EventArgs e)
        {
            this.Dispose();
        }

        private void button6_Click_1(object sender, EventArgs e)
        {
            this.Dispose();
        }

        private void button1_Click_2(object sender, EventArgs e)
        {
            Genclass.ty = 2;
            FrmInvoiceM master = (FrmInvoiceM)Application.OpenForms["FrmInvoiceM"];
            master.buttcusok.PerformClick();


        }

        private void loadput()
        {
            conn.Close();
            conn.Open();



            Genclass.strsql = "select * from sotmp";



            Genclass.cmd = new SqlCommand(Genclass.strsql, conn);
            SqlDataAdapter aptr = new SqlDataAdapter(Genclass.cmd);
            DataTable tap1 = new DataTable();
            aptr.Fill(tap1);

            FrmInvoice co = new FrmInvoice();

            //TabControl tab = (TabControl)contc.Controls["tapc"];
            DataGridView dtco = (DataGridView)co.Controls["HFIT"];
            //dtco.Refresh();
            //dtco.ColumnCount = tap1.Columns.Count;

            for (int i = 0; i < tap1.Rows.Count; i++)
            {



                var index = dtco.Rows.Add();
                dtco.Rows[index].Cells[0].Value = tap1.Rows[i]["itemname"].ToString();



                dtco.Rows[index].Cells[1].Value = tap1.Rows[i]["UOM"].ToString();

                double sump = Convert.ToDouble(tap1.Rows[i]["Price"].ToString());
                dtco.Rows[index].Cells[2].Value = sump.ToString("0.000");

                double sumq = Convert.ToDouble(tap1.Rows[i]["qty"].ToString());
                dtco.Rows[index].Cells[3].Value = sumq.ToString("0.00");

                double sumb = Convert.ToDouble(tap1.Rows[i]["BasicValue"].ToString());
                dtco.Rows[index].Cells[4].Value = sumb.ToString("0.00");



                dtco.Rows[index].Cells[6].Value = tap1.Rows[i]["itemuid"].ToString();

                dtco.Rows[index].Cells[7].Value = tap1.Rows[i]["gstper"].ToString();

                dtco.Rows[index].Cells[8].Value = tap1.Rows[i]["gstval"].ToString();


                dtco.Rows[index].Cells[9].Value = 0;



                dtco.Rows[index].Cells[10].Value = tap1.Rows[i]["listid"].ToString();

                //if (Genclass.STR == "")
                //{
                //    Genclass.STR = txtlisid.Text;
                //}
                //else
                //{
                //    Genclass.STR = Genclass.STR + "," + txtlisid.Text;
                //}

                //Genclass.STR = Genclass.STR + ")";

                dtco.Rows[index].Cells[11].Value = tap1.Rows[i]["hsnid"].ToString();
                dtco.Rows[index].Cells[12].Value = tap1.Rows[i]["total"].ToString();
                dtco.Rows[index].Cells[13].Value = tap1.Rows[i]["pono"].ToString();
                dtco.Rows[index].Cells[14].Value = tap1.Rows[i]["podate"].ToString();

                Genclass.sum1 = Genclass.sum1 + Convert.ToDouble(tap1.Rows[i]["BasicValue"].ToString());

            }

            this.Dispose();
            //co.Show();
            conn.Close();
        }

        private void button2_Click_2(object sender, EventArgs e)
        {
            this.Dispose();
        }



        private void lookupload1()
        {
            try
            {

                //conn.Open();


                Genclass.strsqltmp = Genclass.strsql;
                Genclass.FSSQLSortStrtmp = Genclass.FSSQLSortStr;
                Genclass.FSSQLSortStrtmp1 = Genclass.FSSQLSortStr1;
                Genclass.StrSrch = "";
                Genclass.StrSrch1 = "";



                //if (txtscr1.Text != "" || txtscr2.Text != "")
                //{
                if (Genclass.StrSrch == "")
                {
                    Genclass.StrSrch = Genclass.FSSQLSortStr + " like '%" + txtscr1.Text + "%'";
                    Genclass.StrSrch1 = Genclass.FSSQLSortStr1 + " like '%" + txtscr2.Text + "%'";
                }
                else
                {
                    Genclass.StrSrch = Genclass.StrSrch + " and " + Genclass.FSSQLSortStr + " like '%" + txtscr1.Text + "%'";
                    Genclass.StrSrch1 = Genclass.StrSrch1 + " and " + Genclass.FSSQLSortStr1 + " like '%" + txtscr2.Text + "%'";
                }

                //}


                if (txtscr1.Text != "" || txtscr2.Text != "")
                {
                    if (cbogrp.Visible == false)
                    {
                        if (txtscr1.Text != "")
                        {
                            Genclass.StrSrch = " " + Genclass.StrSrch;
                        }
                        else
                        {
                            Genclass.StrSrch1 = " " + Genclass.StrSrch1;
                        }
                    }
                    else if (txtscr1.Text != "")
                    {
                        Genclass.StrSrch = " " + Genclass.StrSrch;
                    }
                    else
                    {
                        Genclass.StrSrch1 = " " + Genclass.StrSrch1;
                    }

                }

                //if (txtscr1.Text != "")
                //{
                if (Genclass.data1 == 1)
                {
                    Genclass.strsqltmp = "select itemid,itemname ,itemcode,listid from (select a.UId,a.DocNo,isnull(b.PQty,0)-isnull(c.PQty,0)-isnull(e.PQty,0)  as  balqty,f.ItemName,f.ItemCode,f.uid as itemid,b.uid as listid from sTransactionsP a inner join sTransactionsPList b on a.UId=b.TransactionsPUId     and b.DocTypeID = 80   left join Stransactionsplist  d on b.uid=  d.Refuid  and d.doctypeid=40 left join    TransactionsPList c    on b.uid=c.Refuid  and c.DocTypeID=110  left join Stransactionsplist e on c.Uid=e.refuid    and e.doctypeid=40  left join     itemm f on c.ItemUId=f.Uid  where f.itemname is not null   and f.companyid=1 and a.suppuid=" + Genclass.cat + "   and  " + Genclass.StrSrch + " and  " + Genclass.StrSrch1 + "  group  by   a.uid,a.docno,b.pqty,e.pqty,c.pqty,f.ItemName,     f.ItemCode,f.uid,b.uid  having isnull(b.PQty,0)-isnull(c.PQty,0)-isnull(e.PQty,0) >0 ) tab  order by " + Genclass.FSSQLSortStr + "";
                    Genclass.cmd = new SqlCommand(Genclass.strsqltmp, conn);
                }
                else
                {
                    Genclass.strsqltmp = "select itemid,itemcode ,itemname,listid from (select a.UId,a.DocNo,isnull(b.PQty,0)-isnull(c.PQty,0)-isnull(e.PQty,0)  as  balqty,f.ItemName,f.ItemCode,f.uid as itemid,b.uid as listid from sTransactionsP a inner join sTransactionsPList b on a.UId=b.TransactionsPUId     and b.DocTypeID = 80   left join Stransactionsplist  d on b.uid=  d.Refuid  and d.doctypeid=40 left join    TransactionsPList c    on b.uid=c.Refuid  and c.DocTypeID=110  left join Stransactionsplist e on c.Uid=e.refuid    and e.doctypeid=40  left join     itemm f on c.ItemUId=f.Uid  where f.itemname is not null   and f.companyid=1 and a.suppuid=" + Genclass.cat + "    and  " + Genclass.StrSrch + " and  " + Genclass.StrSrch1 + "  group  by   a.uid,a.docno,b.pqty,e.pqty,c.pqty,f.ItemName,     f.ItemCode,f.uid,b.uid  having isnull(b.PQty,0)-isnull(c.PQty,0)-isnull(e.PQty,0) >0 ) tab  order by " + Genclass.FSSQLSortStr + "";
                    Genclass.cmd = new SqlCommand(Genclass.strsqltmp, conn);
                }


                //}
                //else if (txtscr1.Text == "")
                //{
                //    if (Genclass.data1 == 1)
                //    {
                //        Genclass.strsqltmp = "select itemid,itemname ,itemcode,listid from (select a.UId,a.DocNo,isnull(b.PQty,0)-isnull(c.PQty,0)-isnull(e.PQty,0)  as  balqty,f.ItemName,f.ItemCode,f.uid as itemid,b.uid as listid from sTransactionsP a inner join sTransactionsPList b on a.UId=b.TransactionsPUId     and b.DocTypeID = 80   left join Stransactionsplist  d on b.uid=  d.Refuid  and d.doctypeid=40 left join    TransactionsPList c    on b.uid=c.Refuid  and c.DocTypeID=110  left join Stransactionsplist e on c.Uid=e.refuid    and e.doctypeid=40  left join     itemm f on c.ItemUId=f.Uid  where f.itemname is not null   and f.companyid=1      group  by   a.uid,a.docno,b.pqty,e.pqty,c.pqty,f.ItemName,     f.ItemCode,f.uid,b.uid  having isnull(b.PQty,0)-isnull(c.PQty,0)-isnull(e.PQty,0) >0 ) tab  order by " + Genclass.FSSQLSortStr + "";
                //        Genclass.cmd = new SqlCommand(Genclass.strsqltmp, conn);
                //    }
                //    else
                //    {
                //        Genclass.strsqltmp = "select itemid,itemcode ,itemname,listid from (select a.UId,a.DocNo,isnull(b.PQty,0)-isnull(c.PQty,0)-isnull(e.PQty,0)  as  balqty,f.ItemName,f.ItemCode,f.uid as itemid,b.uid as listid from sTransactionsP a inner join sTransactionsPList b on a.UId=b.TransactionsPUId     and b.DocTypeID = 80   left join Stransactionsplist  d on b.uid=  d.Refuid  and d.doctypeid=40 left join    TransactionsPList c    on b.uid=c.Refuid  and c.DocTypeID=110  left join Stransactionsplist e on c.Uid=e.refuid    and e.doctypeid=40  left join     itemm f on c.ItemUId=f.Uid  where f.itemname is not null   and f.companyid=1      group  by   a.uid,a.docno,b.pqty,e.pqty,c.pqty,f.ItemName,     f.ItemCode,f.uid,b.uid  having isnull(b.PQty,0)-isnull(c.PQty,0)-isnull(e.PQty,0) >0 ) tab  order by " + Genclass.FSSQLSortStr + "";
                //        Genclass.cmd = new SqlCommand(Genclass.strsqltmp, conn);
                //    }


                //}
                //else if (txtscr2.Text != "")
                //{
                //    if (Genclass.data1 == 1)
                //    {
                //        Genclass.strsqltmp = "select itemid,itemname ,itemcode,listid from (select a.UId,a.DocNo,isnull(b.PQty,0)-isnull(c.PQty,0)-isnull(e.PQty,0)  as  balqty,f.ItemName,f.ItemCode,f.uid as itemid,b.uid as listid from sTransactionsP a inner join sTransactionsPList b on a.UId=b.TransactionsPUId     and b.DocTypeID = 80   left join Stransactionsplist  d on b.uid=  d.Refuid  and d.doctypeid=40 left join    TransactionsPList c    on b.uid=c.Refuid  and c.DocTypeID=110  left join Stransactionsplist e on c.Uid=e.refuid    and e.doctypeid=40  left join     itemm f on c.ItemUId=f.Uid  where f.itemname is not null   and f.companyid=1    and  " + Genclass.StrSrch1 + "  group  by   a.uid,a.docno,b.pqty,e.pqty,c.pqty,f.ItemName,     f.ItemCode,f.uid,b.uid  having isnull(b.PQty,0)-isnull(c.PQty,0)-isnull(e.PQty,0) >0 ) tab  order by " + Genclass.FSSQLSortStr1 + "";
                //        Genclass.cmd = new SqlCommand(Genclass.strsqltmp, conn);
                //    }
                //    else
                //    {
                //        Genclass.strsqltmp = "select itemid,itemcode ,itemname,listid from (select a.UId,a.DocNo,isnull(b.PQty,0)-isnull(c.PQty,0)-isnull(e.PQty,0)  as  balqty,f.ItemName,f.ItemCode,f.uid as itemid,b.uid as listid from sTransactionsP a inner join sTransactionsPList b on a.UId=b.TransactionsPUId     and b.DocTypeID = 80   left join Stransactionsplist  d on b.uid=  d.Refuid  and d.doctypeid=40 left join    TransactionsPList c    on b.uid=c.Refuid  and c.DocTypeID=110  left join Stransactionsplist e on c.Uid=e.refuid    and e.doctypeid=40  left join     itemm f on c.ItemUId=f.Uid  where f.itemname is not null   and f.companyid=1    and  " + Genclass.StrSrch1 + "  group  by   a.uid,a.docno,b.pqty,e.pqty,c.pqty,f.ItemName,     f.ItemCode,f.uid,b.uid  having isnull(b.PQty,0)-isnull(c.PQty,0)-isnull(e.PQty,0) >0 ) tab  order by " + Genclass.FSSQLSortStr1 + "";
                //        Genclass.cmd = new SqlCommand(Genclass.strsqltmp, conn);
                //    }


                //}
                //else if (txtscr2.Text == "")
                //{
                //    if (Genclass.data1 == 1)
                //    {
                //        Genclass.strsqltmp = "select itemid,itemname ,itemcode,listid from (select a.UId,a.DocNo,isnull(b.PQty,0)-isnull(c.PQty,0)-isnull(e.PQty,0)  as  balqty,f.ItemName,f.ItemCode,f.uid as itemid ,b.uid as listid from sTransactionsP a inner join sTransactionsPList b on a.UId=b.TransactionsPUId     and b.DocTypeID = 80   left join Stransactionsplist  d on b.uid=  d.Refuid  and d.doctypeid=40 left join    TransactionsPList c    on b.uid=c.Refuid  and c.DocTypeID=110  left join Stransactionsplist e on c.Uid=e.refuid    and e.doctypeid=40  left join     itemm f on c.ItemUId=f.Uid  where f.itemname is not null   and f.companyid=1     group  by   a.uid,a.docno,b.pqty,e.pqty,c.pqty,f.ItemName,     f.ItemCode,f.uid,b.uid  having isnull(b.PQty,0)-isnull(c.PQty,0)-isnull(e.PQty,0) >0 ) tab  order by " + Genclass.FSSQLSortStr1 + "";
                //        Genclass.cmd = new SqlCommand(Genclass.strsqltmp, conn);
                //    }
                //    else
                //    {
                //        Genclass.strsqltmp = "select itemid,itemcode ,itemname,listid from (select a.UId,a.DocNo,isnull(b.PQty,0)-isnull(c.PQty,0)-isnull(e.PQty,0)  as  balqty,f.ItemName,f.ItemCode,f.uid as itemid,b.uid as listid from sTransactionsP a inner join sTransactionsPList b on a.UId=b.TransactionsPUId     and b.DocTypeID = 80   left join Stransactionsplist  d on b.uid=  d.Refuid  and d.doctypeid=40 left join    TransactionsPList c    on b.uid=c.Refuid  and c.DocTypeID=110  left join Stransactionsplist e on c.Uid=e.refuid    and e.doctypeid=40  left join     itemm f on c.ItemUId=f.Uid  where f.itemname is not null   and f.companyid=1    and  " + Genclass.StrSrch1 + "  group  by   a.uid,a.docno,b.pqty,e.pqty,c.pqty,f.ItemName,     f.ItemCode,f.uid,b.uid  having isnull(b.PQty,0)-isnull(c.PQty,0)-isnull(e.PQty,0) >0 ) tab  order by " + Genclass.FSSQLSortStr1 + "";
                //        Genclass.cmd = new SqlCommand(Genclass.strsqltmp, conn);
                //    }


                //}
                SqlDataAdapter aptr = new SqlDataAdapter(Genclass.cmd);
                DataTable tap = new DataTable();
                aptr.Fill(tap);


                this.Hfgp.DefaultCellStyle.Font = new Font("Arial", 10);
                this.Hfgp.ColumnHeadersDefaultCellStyle.Font = new Font("Arial", 10, FontStyle.Bold);
                Hfgp.AutoGenerateColumns = false;
                Hfgp.DataSource = null;
                Hfgp.ColumnCount = tap.Columns.Count;
                Genclass.i = 0;
                foreach (DataColumn column in tap.Columns)
                {
                    Hfgp.Columns[Genclass.i].Name = column.ColumnName;
                    Hfgp.Columns[Genclass.i].HeaderText = column.ColumnName;
                    Hfgp.Columns[Genclass.i].DataPropertyName = column.ColumnName;
                    Genclass.i = Genclass.i + 1;
                }

                if (Genclass.data1 == 1)
                {
                    Hfgp.Columns[0].Visible = false;
                    Hfgp.Columns[1].Width = 300;


                    Hfgp.Columns[2].Width = 130;
                    Hfgp.Columns[3].Visible = false;
                    Hfgp.Columns[4].Visible = false;
                }

                else
                {
                    Hfgp.Columns[0].Visible = false;
                    Hfgp.Columns[1].Width = 130;


                    Hfgp.Columns[2].Width = 300;
                    Hfgp.Columns[3].Visible = false;
                }


                Hfgp.DataSource = tap;
                //txtscr1.Focus();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return;
            }
            finally
            {
                conn.Close();
                Genclass.cmd.Dispose();
            }
        }


        //private void Titlep1()
        //{
        //    DataGridViewCheckBoxColumn checkColumn = new DataGridViewCheckBoxColumn();
        //    checkColumn.Name = "CHK";
        //    checkColumn.HeaderText = "CHK";

        //    checkColumn.TrueValue = true;
        //    checkColumn.FalseValue = false;
        //    checkColumn.Width = 50;
        //    checkColumn.ReadOnly = false;
        //    checkColumn.FillWeight = 10; //if the datagridview is resized (on form resize) the checkbox won't take up too much; value is relative to the other columns' fill values
        //    Hfgp.Columns.Add(checkColumn);
        //}

        private void lookupload2()
        {
            try
            {

                //conn.Open();


                Genclass.strsqltmp = Genclass.strsql;
                //Genclass.FSSQLSortStrtmp = Genclass.FSSQLSortStr;
                //Genclass.FSSQLSortStrtmp1 = Genclass.FSSQLSortStr9;
                Genclass.StrSrch = "";
                Genclass.StrSrch1 = "";



                //if (txtscr3.Text != "" || txtscr4.Text != "")
                //{
                if (Genclass.StrSrch == "")
                {
                    Genclass.StrSrch = Genclass.FSSQLSortStr2 + " like '%" + txtscr3.Text + "%'";
                    Genclass.StrSrch1 = Genclass.FSSQLSortStr3 + " like '%" + txtscr4.Text + "%'";
                }
                else
                {
                    Genclass.StrSrch = Genclass.StrSrch + " and " + Genclass.FSSQLSortStr2 + " like '%" + txtscr3.Text + "%'";
                    Genclass.StrSrch1 = Genclass.StrSrch1 + " and " + Genclass.FSSQLSortStr3 + " like '%" + txtscr4.Text + "%'";
                }

                //}


                if (txtscr3.Text != "" || txtscr4.Text != "")
                {
                    if (cbogrp.Visible == false)
                    {
                        if (txtscr3.Text != "")
                        {
                            Genclass.StrSrch = " " + Genclass.StrSrch;
                        }
                        else
                        {
                            Genclass.StrSrch1 = " " + Genclass.StrSrch1;
                        }
                    }
                    else if (txtscr3.Text != "")
                    {
                        Genclass.StrSrch = " " + Genclass.StrSrch;
                    }
                    else
                    {
                        Genclass.StrSrch1 = " " + Genclass.StrSrch1;
                    }

                }


                if (Genclass.data1 == 1)
                {

                    Genclass.strsqltmp = "select distinct itemname,itemcode,Pono,convert(date,Podate,102) as Podate,Qty,listid,itemid,BQty from (select aa.*,bb.reqdate as Podate from (select a.UId,a.transp as Pono,isnull(b.PQty,0)-isnull(c.PQty,0)-isnull(d.PQty,0) as qty,f.uid as itemid,f.ItemName,f.ItemCode,b.uid as listid,isnull(z.qty,0) as Bqty from sTransactionsP a inner join sTransactionsPList b on a.UId=b.TransactionsPUId   and b.DocTypeID = 80    left join Stransactionsplist  d on b.uid=  d.Refuid  and d.doctypeid=40 left join  TransactionsPList c on b.uid=c.Refuid  and c.DocTypeID=20 left join Stransactionsplist e on c.Uid=e.refuid and e.doctypeid=40  left join itemm f on b.ItemUId=f.Uid left join ordreqqty o on a.uid=o.tpuid left join Pgsotmp z on b.uid=z.refid  where  a.partyuid=" + Genclass.cat + " and a.companyid=" + Genclass.data1 + " and  " + Genclass.StrSrch + " and  " + Genclass.StrSrch1 + "   group  by  a.uid,a.transp,b.pqty,d.pqty,c.pqty,f.ItemName,f.ItemCode,f.uid,b.uid,z.qty having isnull(b.PQty,0)-isnull(c.PQty,0)-isnull(d.PQty,0) >0) aa left join  (select c.tpuid,c.itid,c.reqdate from STransactionsP a inner join STransactionsPlist b on a.uid=b.TransactionsPUId and a.Doctypeid=80 and a.companyid=" + Genclass.data1 + " left join Ordreqqty c on a.uid=c.Tpuid and b.ItemUId=c.itid)bb on aa.uid=bb.tpuid and aa.itemid=bb.itid ) tab order by  Podate";
                    Genclass.cmd = new SqlCommand(Genclass.strsqltmp, conn);

                }
                else
                {
                    Genclass.strsqltmp = "select distinct itemcode,itemname,Pono,convert(date,Podate,102) as Podate,Qty,listid,itemid,BQty from (select aa.*,bb.reqdate as Podate from (select a.UId,a.transp as Pono,isnull(b.PQty,0)-isnull(c.PQty,0)-isnull(d.PQty,0) as qty,f.uid as itemid,f.ItemName,f.ItemCode,b.uid as listid,isnull(z.qty,0) as Bqty from sTransactionsP a inner join sTransactionsPList b on a.UId=b.TransactionsPUId   and b.DocTypeID = 80    left join Stransactionsplist  d on b.uid=  d.Refuid  and d.doctypeid=40 left join  TransactionsPList c on b.uid=c.Refuid  and c.DocTypeID=20 left join Stransactionsplist e on c.Uid=e.refuid and e.doctypeid=40  left join itemm f on b.ItemUId=f.Uid left join ordreqqty o on a.uid=o.tpuid left join sotmp z on b.uid=z.refid  where  a.partyuid=" + Genclass.cat + " and a.companyid=" + Genclass.data1 + " and  " + Genclass.StrSrch + " and  " + Genclass.StrSrch1 + "   group  by  a.uid,a.transp,b.pqty,d.pqty,c.pqty,f.ItemName,f.ItemCode,f.uid,b.uid,z.qty having isnull(b.PQty,0)-isnull(c.PQty,0)-isnull(d.PQty,0) >0) aa left join  (select c.tpuid,c.itid,c.reqdate from STransactionsP a inner join STransactionsPlist b on a.uid=b.TransactionsPUId and a.Doctypeid=80 and a.companyid=" + Genclass.data1 + " left join Ordreqqty c on a.uid=c.Tpuid and b.ItemUId=c.itid)bb on aa.uid=bb.tpuid and aa.itemid=bb.itid ) tab order by  Podate";

                    Genclass.cmd = new SqlCommand(Genclass.strsqltmp, conn);

                }

                //Genclass.strsqltmp = "select distinct itemcode,itemname,Pono,convert(date,Podate,102) as Podate,Qty,listid,itemid,BQty from (select aa.*,bb.reqdate as Podate from (select a.UId,a.transp as Pono,isnull(b.PQty,0)-isnull(c.PQty,0)-isnull(d.PQty,0) as qty,f.uid as itemid,f.ItemName,f.ItemCode,b.uid as listid,isnull(z.qty,0) as Bqty from sTransactionsP a inner join sTransactionsPList b on a.UId=b.TransactionsPUId   and b.DocTypeID = 80    left join Stransactionsplist  d on b.uid=  d.Refuid  and d.doctypeid=40 left join  TransactionsPList c on b.uid=c.Refuid  and c.DocTypeID=20 left join Stransactionsplist e on c.Uid=e.refuid and e.doctypeid=40  left join itemm f on b.ItemUId=f.Uid left join ordreqqty o on a.uid=o.tpuid left join sotmp z on b.uid=z.refid  where  a.partyuid=" + Genclass.cat + " and a.companyid=" + Genclass.data1 + " and  " + Genclass.StrSrch + " and  " + Genclass.StrSrch1 + "   group  by  a.uid,a.transp,b.pqty,d.pqty,c.pqty,f.ItemName,f.ItemCode,f.uid,b.uid,z.qty having isnull(b.PQty,0)-isnull(c.PQty,0)-isnull(d.PQty,0) >0) aa left join  (select c.tpuid,c.itid,c.reqdate from STransactionsP a inner join STransactionsPlist b on a.uid=b.TransactionsPUId and a.Doctypeid=80 and a.companyid=" + Genclass.data1 + " left join Ordreqqty c on a.uid=c.Tpuid and b.ItemUId=c.itid)bb on aa.uid=bb.tpuid and aa.itemid=bb.itid ) tab order by  Podate";
                //Genclass.cmd = new SqlCommand(Genclass.strsqltmp, conn);
                SqlDataAdapter aptr = new SqlDataAdapter(Genclass.cmd);
                DataTable tap = new DataTable();
                aptr.Fill(tap);




                this.HFGP4.DefaultCellStyle.Font = new Font("Arial", 10);
                this.HFGP4.ColumnHeadersDefaultCellStyle.Font = new Font("Arial", 10, FontStyle.Bold);
                HFGP4.AutoGenerateColumns = false;
                HFGP4.DataSource = null;
                HFGP4.ColumnCount = tap.Columns.Count;
                Genclass.i = 0;
                //foreach (DataColumn column in tap.Columns)
                //{
                //    HFGP4.Columns[Genclass.i].Name = column.ColumnName;
                //    HFGP4.Columns[Genclass.i].HeaderText = column.ColumnName;
                //    HFGP4.Columns[Genclass.i].DataPropertyName = column.ColumnName;
                //    Genclass.i = Genclass.i + 1;
                //}

                HFGP4.Columns[0].Width = 130;
                HFGP4.Columns[1].Width = 250;


                HFGP4.Columns[2].Width = 130;
                HFGP4.Columns[3].Width = 100;
                HFGP4.Columns[4].Width = 80;
                if (Genclass.data1 == 3)
                {
                    HFGP4.Columns[5].Visible = false;
                    HFGP4.Columns[6].Visible = false;
                    HFGP4.Columns[7].Width = 80;
                    //HFGP4.Columns[7].ReadOnly = false;
                }

                HFGP4.DataSource = tap;


            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return;
            }
            finally
            {
                conn.Close();
                Genclass.cmd.Dispose();
            }
        }

        private void lookupload3()
        {
            try
            {

                //conn.Open();


                Genclass.strsqltmp = Genclass.strsql2;
                //Genclass.FSSQLSortStrtmp = Genclass.FSSQLSortStr;
                //Genclass.FSSQLSortStrtmp1 = Genclass.FSSQLSortStr1;
                Genclass.StrSrch = "";
                Genclass.StrSrch1 = "";



                //if (txtscr5.Text != "" || txtscr6.Text != "")
                //{
                if (Genclass.StrSrch == "")
                {
                    Genclass.StrSrch = Genclass.FSSQLSortStr4 + " like '%" + txtscr5.Text + "%'";
                    Genclass.StrSrch1 = Genclass.FSSQLSortStr5 + " like '%" + txtscr6.Text + "%'";
                }
                else
                {
                    Genclass.StrSrch = Genclass.StrSrch + " and " + Genclass.FSSQLSortStr4 + " like '%" + txtscr5.Text + "%'";
                    Genclass.StrSrch1 = Genclass.StrSrch1 + " and " + Genclass.FSSQLSortStr5 + " like '%" + txtscr6.Text + "%'";
                }

                //}


                if (txtscr5.Text != "" || txtscr6.Text != "")
                {
                    if (cbogrp.Visible == false)
                    {
                        if (txtscr5.Text != "")
                        {
                            Genclass.StrSrch = " " + Genclass.StrSrch;
                        }
                        else
                        {
                            Genclass.StrSrch1 = " " + Genclass.StrSrch1;
                        }
                    }
                    else
                        if (txtscr5.Text != "")
                    {
                        Genclass.StrSrch = " " + Genclass.StrSrch;
                    }
                    else
                    {
                        Genclass.StrSrch1 = " " + Genclass.StrSrch1;
                    }

                }

                //if (txtscr5.Text != "")
                //{

                if (Genclass.data1 == 1)
                {
                    Genclass.strsqltmp = "select c.uid,c.itemname,c.itemcode,a.price,isnull(bb.qty,0) as Qty from pur_price_list  a inner join  PartyM b on b.Uid=a.suppuid inner join ItemM c on a.Itemuid=c.Uid   left join Pgsotmp bb on c.uid=bb.itemuid  where a.suppuid=" + Genclass.cat + " and a.companyid=" + Genclass.data1 + " and  " + Genclass.StrSrch + " and  " + Genclass.StrSrch1 + " order by " + Genclass.FSSQLSortStr + "";
                    Genclass.cmd = new SqlCommand(Genclass.strsqltmp, conn);
                }
                else

                {
                    Genclass.strsqltmp = "select c.uid,c.itemcode,c.itemname,a.price,isnull(bb.qty,0) as Qty from pur_price_list  a inner join  PartyM b on b.Uid=a.suppuid inner join ItemM c on a.Itemuid=c.Uid   left join sotmp bb on c.uid=bb.itemuid  where a.suppuid=" + Genclass.cat + " and a.companyid=" + Genclass.data1 + " and  " + Genclass.StrSrch + " and  " + Genclass.StrSrch1 + " order by " + Genclass.FSSQLSortStr + "";
                    Genclass.cmd = new SqlCommand(Genclass.strsqltmp, conn);
                }


                //}
                //else if (txtscr5.Text == "")
                //{
                //      if (Genclass.data1 == 1)
                //    {
                //    Genclass.strsqltmp = "select c.uid,c.itemname,c.itemcode,price,convert(nvarchar,eff_from,106) as eff_from from pur_price_list  a inner join  PartyM b on b.Uid=a.suppuid inner join ItemM c on a.Itemuid=c.Uid   where a.companyid=" + Genclass.data1 + "  order by " + Genclass.FSSQLSortStr + "";
                //    Genclass.cmd = new SqlCommand(Genclass.strsqltmp, conn);
                //               }
                //    else

                //        {
                //            Genclass.strsqltmp = "select c.uid,c.itemcode,c.itemname,price,convert(nvarchar,eff_from,106) as eff_from from pur_price_list  a inner join  PartyM b on b.Uid=a.suppuid inner join ItemM c on a.Itemuid=c.Uid   where a.companyid=" + Genclass.data1 + "  order by " + Genclass.FSSQLSortStr + "";
                //            Genclass.cmd = new SqlCommand(Genclass.strsqltmp, conn);
                //        }


                //}
                //else if (txtscr6.Text != "") 
                //{
                //    if (Genclass.data1 == 1)
                //    {
                //        Genclass.strsqltmp = "select c.uid,c.itemname,c.itemcode,price,convert(nvarchar,eff_from,106) as eff_from from pur_price_list  a inner join  PartyM b on b.Uid=a.suppuid inner join ItemM c on a.Itemuid=c.Uid   where a.companyid=" + Genclass.data1 + " and  " + Genclass.StrSrch1 + " order by " + Genclass.FSSQLSortStr1 + "";
                //        Genclass.cmd = new SqlCommand(Genclass.strsqltmp, conn);
                //    }
                //    else
                //    {
                //        Genclass.strsqltmp = "select c.uid,c.itemcode,c.itemname,price,convert(nvarchar,eff_from,106) as eff_from from pur_price_list  a inner join  PartyM b on b.Uid=a.suppuid inner join ItemM c on a.Itemuid=c.Uid   where a.companyid=" + Genclass.data1 + " and  " + Genclass.StrSrch1 + " order by " + Genclass.FSSQLSortStr1 + "";
                //        Genclass.cmd = new SqlCommand(Genclass.strsqltmp, conn);
                //    }


                //}
                //else if (txtscr6.Text == "")
                //{
                //    if (Genclass.data1 == 1)
                //    {
                //        Genclass.strsqltmp = "select c.uid,c.itemname,c.itemcode,price,convert(nvarchar,eff_from,106) as eff_from from pur_price_list  a inner join  PartyM b on b.Uid=a.suppuid inner join ItemM c on a.Itemuid=c.Uid   where a.companyid=" + Genclass.data1 + "  order by " + Genclass.FSSQLSortStr1 + "";
                //        Genclass.cmd = new SqlCommand(Genclass.strsqltmp, conn);
                //    }
                //    else
                //    {
                //        Genclass.strsqltmp = "select c.uid,c.itemcode,c.itemname,price,convert(nvarchar,eff_from,106) as eff_from from pur_price_list  a inner join  PartyM b on b.Uid=a.suppuid inner join ItemM c on a.Itemuid=c.Uid   where a.companyid=" + Genclass.data1 + "  order by " + Genclass.FSSQLSortStr1 + "";
                //        Genclass.cmd = new SqlCommand(Genclass.strsqltmp, conn);
                //    }


                //}

                SqlDataAdapter aptr = new SqlDataAdapter(Genclass.cmd);
                DataTable tap = new DataTable();
                aptr.Fill(tap);


                this.HFGP2.DefaultCellStyle.Font = new Font("Arial", 10);
                this.HFGP2.ColumnHeadersDefaultCellStyle.Font = new Font("Arial", 10, FontStyle.Bold);
                HFGP2.AutoGenerateColumns = false;
                HFGP2.DataSource = null;
                HFGP2.ColumnCount = tap.Columns.Count;
                Genclass.i = 0;
                foreach (DataColumn column in tap.Columns)
                {
                    HFGP2.Columns[Genclass.i].Name = column.ColumnName;
                    HFGP2.Columns[Genclass.i].HeaderText = column.ColumnName;
                    HFGP2.Columns[Genclass.i].DataPropertyName = column.ColumnName;
                    Genclass.i = Genclass.i + 1;
                }

                HFGP2.Columns[0].Visible = false;

                if (Genclass.data1 == 1)
                {
                    HFGP2.Columns[1].Width = 300;
                    HFGP2.Columns[2].Width = 130;
                }
                else
                {
                    HFGP2.Columns[1].Width = 130;
                    HFGP2.Columns[2].Width = 300;
                }

                HFGP2.Columns[3].Width = 100;
                HFGP2.Columns[4].Width = 120;


                HFGP2.DataSource = tap;
                //txtscr5.Focus();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return;
            }
            finally
            {
                conn.Close();
                Genclass.cmd.Dispose();
            }
        }

        private void Hfgp_CellEnter(object sender, DataGridViewCellEventArgs e)
        {
            txtscr1.Focus();
        }

        private void HFGP4_CellEnter(object sender, DataGridViewCellEventArgs e)
        {
            //txtscr3.Focus();
        }

        private void chkact_Click(object sender, EventArgs e)
        {

        }

        private void chkact_CheckedChanged(object sender, EventArgs e)
        {

            if (chkact.Checked == true)
            {
                for (int i = 0; i < HFGP4.RowCount; i++)
                {
                    HFGP4[8, i].Value = true;
                }
            }
            else
            {
                for (int i = 0; i < HFGP4.RowCount; i++)
                {
                    HFGP4[8, i].Value = false;
                }
            }
        }

        private void HFGP4_KeyDown(object sender, KeyEventArgs e)
        {

            //if (Hfgp.Rows[0].Cells[1].Value != null)
            //{
            //    if (e.KeyCode == Keys.Enter && HFGP4.CurrentCell.ColumnIndex == 7)
            //    {
            //        if (Convert.ToInt64(HFGP4.CurrentRow.Cells[7].Value) > 0)
            //        {
            //            qur.CommandText = "insert into sotmp select distinct itemid,0,itemname,uom,listid,Qty,PRate,convert(decimal(18,2),Qty*PRate,102),0,0,convert(decimal(18,2),Qty*PRate,102),f1,convert(decimal(18,2),((Qty*PRate)/100)*f1,102),0,Pono,Podate from (select aa.*,bb.reqdate as Podate from (select a.UId,a.transp as Pono,isnull(b.PQty,0)-isnull(c.PQty,0)-isnull(d.PQty,0) as qty,b.PRate,f.uid as itemid,f.ItemName,f.ItemCode,b.uid as listid,gg.GeneralName as uom,j.Sname as f1 from sTransactionsP a inner join sTransactionsPList b on a.UId=b.TransactionsPUId   and b.DocTypeID = 80    left join Stransactionsplist  d on b.uid=  d.Refuid  and d.doctypeid=40 left join  TransactionsPList c on b.uid=c.Refuid  and c.DocTypeID=20 left join Stransactionsplist e on c.Uid=e.refuid and e.doctypeid=40 left join itemm f on b.ItemUId=f.Uid left join generalm gg on f.UOM_UId=gg.Uid left join ItemGroup j on f.itemgroup_Uid=j.UId left join ordreqqty o on a.uid=o.tpuid where b.uid=" + HFGP4.CurrentRow.Cells[6].Value + " group  by  a.uid,a.transp,b.pqty,d.pqty,c.pqty,f.ItemName,f.ItemCode,f.uid,b.uid,gg.GeneralName,b.PRate,j.Sname having isnull(b.PQty,0)-isnull(c.PQty,0)-isnull(d.PQty,0) >0) aa inner join  (select c.tpuid,c.itid,c.reqdate from STransactionsP a inner join STransactionsPlist b on a.uid=b.TransactionsPUId and a.Doctypeid=80 and a.companyid=" + Genclass.data1 + " left join Ordreqqty c on a.uid=c.Tpuid and b.ItemUId=c.itid)bb on aa.uid=bb.tpuid and aa.itemid=bb.itid ) tab ";
            //            qur.ExecuteNonQuery();
            //        }
            //        else
            //        {
            //            if (HFGP4.CurrentRow.Cells[7].Value.ToString() == "")
            //            {
            //                HFGP4.CurrentRow.Cells[7].Value = 0;
            //                qur.CommandText = "delete from sotmp where refid=" + HFGP4.CurrentRow.Cells[6].Value + "";
            //                qur.ExecuteNonQuery();
            //            }
            //        }
            //    }
            //}
        }

        private void HFGP4_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {

        }



        //private void HFGP4_CellContentClick(object sender, DataGridViewCellEventArgs e)
        //{

        //}

        private void HFGP3_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {

            if (HFGP3.Rows[0].Cells[1].Value != null)
            {
                conn.Open();
                if (HFGP3.CurrentCell.ColumnIndex == 4 && HFGP3.CurrentRow.Cells[4].Value.ToString() != "0" || HFGP3.CurrentCell.ColumnIndex == 3 && HFGP3.CurrentRow.Cells[3].Value.ToString() != "0.0")
                {
                    if (HFGP3.CurrentRow.Cells[4].Value.ToString() != "0")
                    {
                        HFGP3.CurrentRow.Cells[4].Style.BackColor = Color.LawnGreen;

                        if (Genclass.data1 == 1)
                        {
                            qur.CommandText = "delete from Pgsotmp where itemuid=" + HFGP3.CurrentRow.Cells[0].Value + "";
                            qur.ExecuteNonQuery();

                            //qur.CommandText = "insert into Pgsotmp select f.itemname,gg.generalname," + HFGP2.CurrentRow.Cells[3].Value + "," + HFGP2.CurrentRow.Cells[4].Value + ",convert(decimal(18,2)," + HFGP2.CurrentRow.Cells[3].Value + "*" + HFGP2.CurrentRow.Cells[4].Value + ",102) as basicvalue,'',f.uid, j.sname, convert(decimal(18,2),((" + HFGP2.CurrentRow.Cells[3].Value + "*" + HFGP2.CurrentRow.Cells[4].Value + ")/100)*j.sname,102)   as gstval,'',0  from itemm f  left join generalm gg on f.UOM_UId=gg.Uid left join ItemGroup j on f.itemgroup_Uid=j.UId  where f.uid=" + HFGP2.CurrentRow.Cells[0].Value + "";
                            qur.CommandText = "insert into Pgsotmp select f.uid,0,f.itemname,gg.generalname,0," + HFGP3.CurrentRow.Cells[4].Value + "," + HFGP3.CurrentRow.Cells[3].Value + ",convert(decimal(18,2)," + HFGP3.CurrentRow.Cells[3].Value + "*" + HFGP3.CurrentRow.Cells[4].Value + ",102) as basicvalue,0,0,0, isnull(hh.f1,0), convert(decimal(18,2),((" + HFGP3.CurrentRow.Cells[3].Value + "*" + HFGP3.CurrentRow.Cells[4].Value + ")/100)*isnull(hh.f1,0),102)   as gstval,0,0,''  from itemm f  left join generalm gg on f.UOM_UId=gg.Uid left join ItemGroup j on f.itemgroup_Uid=j.UId left join generalm hh on f.tax=hh.Uid where f.uid=" + HFGP3.CurrentRow.Cells[0].Value + "";
                            qur.ExecuteNonQuery();
                        }
                        else
                        {
                            qur.CommandText = "delete from sotmp where itemuid=" + HFGP3.CurrentRow.Cells[0].Value + "";
                            qur.ExecuteNonQuery();
                            //qur.CommandText = "insert into Batmp select f.itemname,gg.generalname," + HFGP3.CurrentRow.Cells[3].Value + "," + HFGP3.CurrentRow.Cells[4].Value + ",convert(decimal(18,2)," + HFGP3.CurrentRow.Cells[3].Value + "*" + HFGP3.CurrentRow.Cells[4].Value + ",102) as basicvalue,'',f.uid,case " + Genclass.cat + " when 24 then convert(decimal(18,2),j.sname/2,102) else j.sname end as sname,case " + Genclass.cat + " when 24 then convert(decimal(18,2),(((" + HFGP3.CurrentRow.Cells[3].Value + "*" + HFGP3.CurrentRow.Cells[4].Value + ")/100)*j.sname)/2,102) else convert(decimal(18,2),((" + HFGP3.CurrentRow.Cells[3].Value + "*" + HFGP3.CurrentRow.Cells[4].Value + ")/100)*j.sname,102) end  as gstval,'',0  from itemm f  left join generalm gg on f.UOM_UId=gg.Uid left join ItemGroup j on f.itemgroup_Uid=j.UId  where f.uid=" + HFGP3.CurrentRow.Cells[0].Value + "";

                            qur.CommandText = "insert into sotmp select f.uid,0,f.itemname,gg.generalname,0," + HFGP3.CurrentRow.Cells[4].Value + "," + HFGP3.CurrentRow.Cells[3].Value + ",convert(decimal(18,2)," + HFGP3.CurrentRow.Cells[3].Value + "*" + HFGP3.CurrentRow.Cells[4].Value + ",102) as basicvalue,0,0,0, isnull(hh.f1,0), convert(decimal(18,2),((" + HFGP3.CurrentRow.Cells[3].Value + "*" + HFGP3.CurrentRow.Cells[4].Value + ")/100)*isnull(hh.f1,0),102)   as gstval,0,0,''  from itemm f  left join generalm gg on f.UOM_UId=gg.Uid left join ItemGroup j on f.itemgroup_Uid=j.UId left join generalm hh on f.tax=hh.Uid  where f.uid=" + HFGP3.CurrentRow.Cells[0].Value + "";
                            qur.ExecuteNonQuery();
                        }

                    }
                }
                else
                {
                    if (Genclass.data1 == 1)
                    {
                        qur.CommandText = "delete from Pgsotmp where itemuid=" + HFGP3.CurrentRow.Cells[0].Value + " and refid=0";
                        qur.ExecuteNonQuery();
                    }
                    else
                    {
                        qur.CommandText = "delete from sotmp where itemuid=" + HFGP3.CurrentRow.Cells[0].Value + " and refid=0";
                        qur.ExecuteNonQuery();
                    }

                }
                conn.Close();
            }
        }

        private void HFGP2_CellClick(object sender, DataGridViewCellEventArgs e)
        {

            if (HFGP2.CurrentRow.Cells[1].Value.ToString() != "")
            {
                DataGridViewCell cell = HFGP2.CurrentRow.Cells[4];
                HFGP2.CurrentCell = cell;
                HFGP2.BeginEdit(true);

            }
        }

        private void HFGP2_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {

            if (HFGP2.Rows[0].Cells[0].Value != null)
            {
                conn.Open();
                if (HFGP2.CurrentCell.ColumnIndex == 4 && HFGP2.CurrentRow.Cells[4].Value.ToString() != "0")
                {
                    if (HFGP2.CurrentRow.Cells[4].Value.ToString() != "0")
                    {
                        HFGP2.CurrentRow.Cells[4].Style.BackColor = Color.LawnGreen;

                        if (Genclass.data1 == 1)
                        {
                            qur.CommandText = "delete from Pgsotmp where itemuid=" + HFGP2.CurrentRow.Cells[0].Value + "";
                            qur.ExecuteNonQuery();

                            //qur.CommandText = "insert into Pgsotmp select f.itemname,gg.generalname," + HFGP2.CurrentRow.Cells[3].Value + "," + HFGP2.CurrentRow.Cells[4].Value + ",convert(decimal(18,2)," + HFGP2.CurrentRow.Cells[3].Value + "*" + HFGP2.CurrentRow.Cells[4].Value + ",102) as basicvalue,'',f.uid, j.sname, convert(decimal(18,2),((" + HFGP2.CurrentRow.Cells[3].Value + "*" + HFGP2.CurrentRow.Cells[4].Value + ")/100)*j.sname,102)   as gstval,'',0  from itemm f  left join generalm gg on f.UOM_UId=gg.Uid left join ItemGroup j on f.itemgroup_Uid=j.UId  where f.uid=" + HFGP2.CurrentRow.Cells[0].Value + "";
                            qur.CommandText = "insert into Pgsotmp select f.uid,0,f.itemname,gg.generalname,0," + HFGP2.CurrentRow.Cells[4].Value + "," + HFGP2.CurrentRow.Cells[3].Value + ",convert(decimal(18,2)," + HFGP2.CurrentRow.Cells[3].Value + "*" + HFGP2.CurrentRow.Cells[4].Value + ",102) as basicvalue,0,0,0, isnull(hh.f1,0) , convert(decimal(18,2),((" + HFGP2.CurrentRow.Cells[3].Value + "*" + HFGP2.CurrentRow.Cells[4].Value + ")/100)*isnull(hh.f1,0),102)   as gstval,0,0,''  from itemm f  left join generalm gg on f.UOM_UId=gg.Uid left join ItemGroup j on f.itemgroup_Uid=j.UId  left join generalm hh on f.tax=hh.Uid where f.uid=" + HFGP2.CurrentRow.Cells[0].Value + "  ";
                            qur.ExecuteNonQuery();
                        }
                        else
                        {
                            qur.CommandText = "delete from sotmp where itemuid=" + HFGP2.CurrentRow.Cells[0].Value + "";
                            qur.ExecuteNonQuery();
                            //qur.CommandText = "insert into Batmp select f.itemname,gg.generalname," + HFGP2.CurrentRow.Cells[3].Value + "," + HFGP2.CurrentRow.Cells[4].Value + ",convert(decimal(18,2)," + HFGP2.CurrentRow.Cells[3].Value + "*" + HFGP2.CurrentRow.Cells[4].Value + ",102) as basicvalue,'',f.uid,case " + Genclass.cat + " when 24 then convert(decimal(18,2),j.sname/2,102) else j.sname end as sname,case " + Genclass.cat + " when 24 then convert(decimal(18,2),(((" + HFGP2.CurrentRow.Cells[3].Value + "*" + HFGP2.CurrentRow.Cells[4].Value + ")/100)*j.sname)/2,102) else convert(decimal(18,2),((" + HFGP2.CurrentRow.Cells[3].Value + "*" + HFGP2.CurrentRow.Cells[4].Value + ")/100)*j.sname,102) end  as gstval,'',0  from itemm f  left join generalm gg on f.UOM_UId=gg.Uid left join ItemGroup j on f.itemgroup_Uid=j.UId  where f.uid=" + HFGP2.CurrentRow.Cells[0].Value + "";

                            qur.CommandText = "insert into sotmp select f.uid,0,f.itemname,gg.generalname,0," + HFGP2.CurrentRow.Cells[4].Value + "," + HFGP2.CurrentRow.Cells[3].Value + ",convert(decimal(18,2)," + HFGP2.CurrentRow.Cells[3].Value + "*" + HFGP2.CurrentRow.Cells[4].Value + ",102) as basicvalue,0,0,0, isnull(hh.f1,0), convert(decimal(18,2),((" + HFGP2.CurrentRow.Cells[3].Value + "*" + HFGP2.CurrentRow.Cells[4].Value + ")/100)*isnull(hh.f1,0),102)   as gstval,0,0,''  from itemm f  left join generalm gg on f.UOM_UId=gg.Uid left join ItemGroup j on f.itemgroup_Uid=j.UId  left join generalm hh on f.tax=hh.Uid  where f.uid=" + HFGP2.CurrentRow.Cells[0].Value + "";
                            qur.ExecuteNonQuery();
                        }

                    }
                }
                else
                {
                    if (Genclass.data1 == 1)
                    {
                        qur.CommandText = "delete from Pgsotmp where itid=" + HFGP2.CurrentRow.Cells[0].Value + " and refid=0";
                        qur.ExecuteNonQuery();
                    }
                    else
                    {
                        qur.CommandText = "delete from sotmp where itid=" + HFGP2.CurrentRow.Cells[0].Value + " and refid=0";
                        qur.ExecuteNonQuery();
                    }

                }


                conn.Close();
            }
        }

        private void HFGP4_CellContentClick_1(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void HFGP4_CellClick_1(object sender, DataGridViewCellEventArgs e)
        {
            if (HFGP4.CurrentRow.Cells[7].Value.ToString() == "0")
            {
                DataGridViewCell cell = HFGP4.CurrentRow.Cells[7];
                HFGP4.CurrentCell = cell;
                HFGP4.BeginEdit(true);
            }


        }

        private void HFGP4_CellValueChanged_1(object sender, DataGridViewCellEventArgs e)
        {

            if (HFGP4.Rows[0].Cells[1].Value != null)
            {
                conn.Close();
                conn.Open();
                if (HFGP4.CurrentCell.ColumnIndex == 7 && HFGP4.CurrentRow.Cells[7].Value.ToString() != "0.0")
                {
                    if (HFGP4.CurrentRow.Cells[4].Value.ToString() != "0")
                    {

                        HFGP4.CurrentRow.Cells[7].Style.BackColor = Color.LawnGreen;



                        if (Convert.ToInt64(HFGP4.CurrentRow.Cells[7].Value) > Convert.ToInt64(HFGP4.CurrentRow.Cells[4].Value))
                        {
                            MessageBox.Show("Qty Exeeds");
                            HFGP4.CurrentRow.Cells[7].Value = 0;
                            return;
                        }
                        else
                        {
                            if (Genclass.data1 == 1)
                            {
                                qur.CommandText = "delete from Pgsotmp where refid=" + HFGP4.CurrentRow.Cells[5].Value + "";
                                qur.ExecuteNonQuery();
                                qur.CommandText = "insert into Pgsotmp select distinct itemid,0,itemname,uom,listid," + HFGP4.CurrentRow.Cells[7].Value + ",PRate,convert(decimal(18,2)," + HFGP4.CurrentRow.Cells[7].Value + "*PRate,102),0,0,convert(decimal(18,2)," + HFGP4.CurrentRow.Cells[7].Value + "*PRate,102),f1,convert(decimal(18,2),((" + HFGP4.CurrentRow.Cells[7].Value + "*PRate)/100)*f1,102),0,Pono,Podate from (select aa.*,bb.reqdate as Podate from (select a.UId,a.transp as Pono,isnull(b.PQty,0)-isnull(c.PQty,0)-isnull(d.PQty,0) as qty,b.PRate,f.uid as itemid,f.ItemName,f.ItemCode,b.uid as listid,gg.GeneralName as uom,hh.f1 as f1 from sTransactionsP a inner join sTransactionsPList b on a.UId=b.TransactionsPUId   and b.DocTypeID = 80    left join Stransactionsplist  d on b.uid=  d.Refuid  and d.doctypeid=40 left join  TransactionsPList c on b.uid=c.Refuid  and c.DocTypeID=20 left join Stransactionsplist e on c.Uid=e.refuid and e.doctypeid=40 left join itemm f on b.ItemUId=f.Uid left join generalm gg on f.UOM_UId=gg.Uid left join ItemGroup j on f.itemgroup_Uid=j.UId left join ordreqqty o on a.uid=o.tpuid left join generalm hh on f.tax=hh.Uid where b.uid=" + HFGP4.CurrentRow.Cells[5].Value + " group  by  a.uid,a.transp,b.pqty,d.pqty,c.pqty,f.ItemName,f.ItemCode,f.uid,b.uid,gg.GeneralName,b.PRate,j.Sname,hh.f1 having isnull(b.PQty,0)-isnull(c.PQty,0)-isnull(d.PQty,0) >0) aa left join  (select c.tpuid,c.itid,c.reqdate from STransactionsP a inner join STransactionsPlist b on a.uid=b.TransactionsPUId and a.Doctypeid=80 and a.companyid=" + Genclass.data1 + " left join Ordreqqty c on a.uid=c.Tpuid and b.ItemUId=c.itid)bb on aa.uid=bb.tpuid and aa.itemid=bb.itid ) tab ";
                                qur.ExecuteNonQuery();
                            }
                            else
                            {
                                qur.CommandText = "delete from sotmp where refid=" + HFGP4.CurrentRow.Cells[5].Value + "";
                                qur.ExecuteNonQuery();
                                qur.CommandText = "insert into sotmp select distinct itemid,0,itemname,uom,listid," + HFGP4.CurrentRow.Cells[7].Value + ",PRate,convert(decimal(18,2)," + HFGP4.CurrentRow.Cells[7].Value + "*PRate,102),0,0,convert(decimal(18,2)," + HFGP4.CurrentRow.Cells[7].Value + "*PRate,102),f1,convert(decimal(18,2),((" + HFGP4.CurrentRow.Cells[7].Value + "*PRate)/100)*f1,102),0,Pono,Podate from (select aa.*,bb.reqdate as Podate from (select a.UId,a.transp as Pono,isnull(b.PQty,0)-isnull(c.PQty,0)-isnull(d.PQty,0) as qty,b.PRate,f.uid as itemid,f.ItemName,f.ItemCode,b.uid as listid,gg.GeneralName as uom,hh.f1 as f1 from sTransactionsP a inner join sTransactionsPList b on a.UId=b.TransactionsPUId   and b.DocTypeID = 80    left join Stransactionsplist  d on b.uid=  d.Refuid  and d.doctypeid=40 left join  TransactionsPList c on b.uid=c.Refuid  and c.DocTypeID=20 left join Stransactionsplist e on c.Uid=e.refuid and e.doctypeid=40 left join itemm f on b.ItemUId=f.Uid left join generalm gg on f.UOM_UId=gg.Uid left join ItemGroup j on f.itemgroup_Uid=j.UId left join ordreqqty o on a.uid=o.tpuid left join generalm hh on f.tax=hh.Uid where b.uid=" + HFGP4.CurrentRow.Cells[5].Value + " group  by  a.uid,a.transp,b.pqty,d.pqty,c.pqty,f.ItemName,f.ItemCode,f.uid,b.uid,gg.GeneralName,b.PRate,j.Sname,hh.f1 having isnull(b.PQty,0)-isnull(c.PQty,0)-isnull(d.PQty,0) >0) aa left join  (select c.tpuid,c.itid,c.reqdate from STransactionsP a inner join STransactionsPlist b on a.uid=b.TransactionsPUId and a.Doctypeid=80 and a.companyid=" + Genclass.data1 + " left join Ordreqqty c on a.uid=c.Tpuid and b.ItemUId=c.itid)bb on aa.uid=bb.tpuid and aa.itemid=bb.itid ) tab ";
                                //qur.CommandText = "insert into Batmp select distinct f.itemname,gg.generalname,b.prate," + HFGP4.CurrentRow.Cells[7].Value + ",convert(decimal(18,2),b.prate*" + HFGP4.CurrentRow.Cells[7].Value + ",102) as basicvalue,'" + HFGP4.CurrentRow.Cells[3].Value + "',f.uid,j.sname,(b.prate*" + HFGP4.CurrentRow.Cells[7].Value + ")/100*j.sname as gstval,'" + HFGP4.CurrentRow.Cells[6].Value + "',b.uid from sTransactionsP a inner join sTransactionsPList b on a.UId=b.TransactionsPUId   and b.DocTypeID = 80  left join Stransactionsplist  d on b.uid=  d.Refuid  and d.doctypeid=40   left join itemm f on b.ItemUId=f.Uid left join generalm gg on f.UOM_UId=gg.Uid left join ItemGroup j on f.itemgroup_Uid=j.UId left join partym pp on a.partyuid=PP.uid where b.uid=" + HFGP4.CurrentRow.Cells[5].Value + " group  by  a.uid,a.transp,b.pqty,d.pqty,f.ItemName,f.ItemCode,f.uid,b.uid,gg.GeneralName,b.PRate,j.Sname,pp.stateuid";
                                qur.ExecuteNonQuery();
                            }
                        }
                    }


                }
            }
            else
            {
                if (Genclass.data1 == 1)
                {
                    qur.CommandText = "delete from Pgsotmp where refid=" + HFGP4.CurrentRow.Cells[5].Value + "";
                    qur.ExecuteNonQuery();
                }
                else
                {
                    qur.CommandText = "delete from sotmp where refid=" + HFGP4.CurrentRow.Cells[5].Value + "";
                    qur.ExecuteNonQuery();
                }

            }

            conn.Close();
        }

        private void HFGP4_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (HFGP4.CurrentRow.Cells[1].Value.ToString() != "")
            {
                DataGridViewCell cell = HFGP4.CurrentRow.Cells[7];
                HFGP4.CurrentCell = cell;
                HFGP4.BeginEdit(true);
            }
        }

        private void HFGP4_CellValueChanged_2(object sender, DataGridViewCellEventArgs e)
        {
            if (HFGP4.Rows[0].Cells[1].Value != null)
            {
                conn.Open();
                if (HFGP4.CurrentCell.ColumnIndex == 7 && Convert.ToInt64(HFGP4.CurrentRow.Cells[7].Value) > 0)
                {
                    if (Convert.ToInt64(HFGP4.CurrentRow.Cells[4].Value) > 0)
                    {

                        HFGP4.CurrentRow.Cells[7].Style.BackColor = Color.LawnGreen;



                        if (Convert.ToInt64(HFGP4.CurrentRow.Cells[7].Value) > Convert.ToInt64(HFGP4.CurrentRow.Cells[4].Value))
                        {
                            MessageBox.Show("Qty Exeeds");
                            HFGP4.CurrentRow.Cells[7].Value = 0;
                            return;
                        }
                        else
                        {
                            if (Genclass.data1 == 1)
                            {
                                qur.CommandText = "delete from Pgsotmp where refid=" + HFGP4.CurrentRow.Cells[5].Value + "";
                                qur.ExecuteNonQuery();
                                qur.CommandText = "insert into Pgsotmp select distinct itemid,0,itemname,uom,listid," + HFGP4.CurrentRow.Cells[7].Value + ",PRate,convert(decimal(18,2)," + HFGP4.CurrentRow.Cells[7].Value + "*PRate,102),0,0,convert(decimal(18,2)," + HFGP4.CurrentRow.Cells[7].Value + "*PRate,102),f1,convert(decimal(18,2),((" + HFGP4.CurrentRow.Cells[7].Value + "*PRate)/100)*f1,102),0,Pono,Podate from (select aa.*,bb.reqdate as Podate from (select a.UId,a.transp as Pono,isnull(b.PQty,0)-isnull(c.PQty,0)-isnull(d.PQty,0) as qty,b.PRate,f.uid as itemid,f.ItemName,f.ItemCode,b.uid as listid,gg.GeneralName as uom,j.Sname as f1 from sTransactionsP a inner join sTransactionsPList b on a.UId=b.TransactionsPUId   and b.DocTypeID = 80    left join Stransactionsplist  d on b.uid=  d.Refuid  and d.doctypeid=40 left join  TransactionsPList c on b.uid=c.Refuid  and c.DocTypeID=20 left join Stransactionsplist e on c.Uid=e.refuid and e.doctypeid=40 left join itemm f on b.ItemUId=f.Uid left join generalm gg on f.UOM_UId=gg.Uid left join ItemGroup j on f.itemgroup_Uid=j.UId left join ordreqqty o on a.uid=o.tpuid where b.uid=" + HFGP4.CurrentRow.Cells[5].Value + " group  by  a.uid,a.transp,b.pqty,d.pqty,c.pqty,f.ItemName,f.ItemCode,f.uid,b.uid,gg.GeneralName,b.PRate,j.Sname having isnull(b.PQty,0)-isnull(c.PQty,0)-isnull(d.PQty,0) >0) aa left join  (select c.tpuid,c.itid,c.reqdate from STransactionsP a inner join STransactionsPlist b on a.uid=b.TransactionsPUId and a.Doctypeid=80 and a.companyid=" + Genclass.data1 + " left join Ordreqqty c on a.uid=c.Tpuid and b.ItemUId=c.itid)bb on aa.uid=bb.tpuid and aa.itemid=bb.itid ) tab ";
                                qur.ExecuteNonQuery();
                            }
                            else
                            {
                                qur.CommandText = "delete from sotmp where refid=" + HFGP4.CurrentRow.Cells[5].Value + "";
                                qur.ExecuteNonQuery();
                                qur.CommandText = "insert into sotmp select distinct itemid,0,itemname,uom,listid," + HFGP4.CurrentRow.Cells[7].Value + ",PRate,convert(decimal(18,2)," + HFGP4.CurrentRow.Cells[7].Value + "*PRate,102),0,0,convert(decimal(18,2)," + HFGP4.CurrentRow.Cells[7].Value + "*PRate,102),f1,convert(decimal(18,2),((" + HFGP4.CurrentRow.Cells[7].Value + "*PRate)/100)*f1,102),0,Pono,Podate from (select aa.*,bb.reqdate as Podate from (select a.UId,a.transp as Pono,isnull(b.PQty,0)-isnull(c.PQty,0)-isnull(d.PQty,0) as qty,b.PRate,f.uid as itemid,f.ItemName,f.ItemCode,b.uid as listid,gg.GeneralName as uom,j.Sname as f1 from sTransactionsP a inner join sTransactionsPList b on a.UId=b.TransactionsPUId   and b.DocTypeID = 80    left join Stransactionsplist  d on b.uid=  d.Refuid  and d.doctypeid=40 left join  TransactionsPList c on b.uid=c.Refuid  and c.DocTypeID=20 left join Stransactionsplist e on c.Uid=e.refuid and e.doctypeid=40 left join itemm f on b.ItemUId=f.Uid left join generalm gg on f.UOM_UId=gg.Uid left join ItemGroup j on f.itemgroup_Uid=j.UId left join ordreqqty o on a.uid=o.tpuid where b.uid=" + HFGP4.CurrentRow.Cells[5].Value + " group  by  a.uid,a.transp,b.pqty,d.pqty,c.pqty,f.ItemName,f.ItemCode,f.uid,b.uid,gg.GeneralName,b.PRate,j.Sname having isnull(b.PQty,0)-isnull(c.PQty,0)-isnull(d.PQty,0) >0) aa left join  (select c.tpuid,c.itid,c.reqdate from STransactionsP a inner join STransactionsPlist b on a.uid=b.TransactionsPUId and a.Doctypeid=80 and a.companyid=" + Genclass.data1 + " left join Ordreqqty c on a.uid=c.Tpuid and b.ItemUId=c.itid)bb on aa.uid=bb.tpuid and aa.itemid=bb.itid ) tab ";
                                //qur.CommandText = "insert into Batmp select distinct f.itemname,gg.generalname,b.prate," + HFGP4.CurrentRow.Cells[7].Value + ",convert(decimal(18,2),b.prate*" + HFGP4.CurrentRow.Cells[7].Value + ",102) as basicvalue,'" + HFGP4.CurrentRow.Cells[3].Value + "',f.uid,j.sname,(b.prate*" + HFGP4.CurrentRow.Cells[7].Value + ")/100*j.sname as gstval,'" + HFGP4.CurrentRow.Cells[6].Value + "',b.uid from sTransactionsP a inner join sTransactionsPList b on a.UId=b.TransactionsPUId   and b.DocTypeID = 80  left join Stransactionsplist  d on b.uid=  d.Refuid  and d.doctypeid=40   left join itemm f on b.ItemUId=f.Uid left join generalm gg on f.UOM_UId=gg.Uid left join ItemGroup j on f.itemgroup_Uid=j.UId left join partym pp on a.partyuid=PP.uid where b.uid=" + HFGP4.CurrentRow.Cells[5].Value + " group  by  a.uid,a.transp,b.pqty,d.pqty,f.ItemName,f.ItemCode,f.uid,b.uid,gg.GeneralName,b.PRate,j.Sname,pp.stateuid";
                                qur.ExecuteNonQuery();
                            }
                        }
                    }


                }
            }
            else
            {
                if (Genclass.data1 == 1)
                {
                    qur.CommandText = "delete from Pgsotmp where refid=" + HFGP4.CurrentRow.Cells[5].Value + "";
                    qur.ExecuteNonQuery();
                }
                else
                {
                    qur.CommandText = "delete from sotmp where refid=" + HFGP4.CurrentRow.Cells[5].Value + "";
                    qur.ExecuteNonQuery();
                }

            }

            conn.Close();
        }

        private void HFGP4_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void HFGP4_CellClick_2(object sender, DataGridViewCellEventArgs e)
        {
            DataGridViewCell cell = HFGP4.CurrentRow.Cells[7];
            HFGP4.CurrentCell = cell;
            HFGP4.BeginEdit(true);
        }

        private void HFGP4_CellValueChanged_3(object sender, DataGridViewCellEventArgs e)
        {
            HFGP4_CellValueChanged_1(sender, e);
        }

        private void HFGP3_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (HFGP3.CurrentRow.Cells[1].Value.ToString() != "" && HFGP3.CurrentCell.ColumnIndex == 3)
            {
                DataGridViewCell cell = HFGP3.CurrentRow.Cells[3];
                HFGP3.CurrentCell = cell;
                HFGP3.BeginEdit(true);
            }
            if (HFGP3.CurrentRow.Cells[1].Value.ToString() != "" && HFGP3.CurrentCell.ColumnIndex == 4)
            {
                DataGridViewCell cell1 = HFGP3.CurrentRow.Cells[4];
                HFGP3.CurrentCell = cell1;
                HFGP3.BeginEdit(true);

            }
        }

    }
}