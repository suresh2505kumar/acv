﻿namespace ACV
{
    partial class Frmsalret
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Frmsalret));
            this.Genpan = new System.Windows.Forms.Panel();
            this.label60 = new System.Windows.Forms.Label();
            this.dtpfnt = new System.Windows.Forms.DateTimePicker();
            this.txtscr9 = new System.Windows.Forms.TextBox();
            this.txtscr7 = new System.Windows.Forms.TextBox();
            this.txtscr6 = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.txtscr4 = new System.Windows.Forms.TextBox();
            this.Txtscr2 = new System.Windows.Forms.TextBox();
            this.txtscr1 = new System.Windows.Forms.TextBox();
            this.HFGP = new System.Windows.Forms.DataGridView();
            this.txtscr5 = new System.Windows.Forms.TextBox();
            this.txtscr8 = new System.Windows.Forms.TextBox();
            this.Txtscr3 = new System.Windows.Forms.TextBox();
            this.dateTimePicker1 = new System.Windows.Forms.DateTimePicker();
            this.panadd = new System.Windows.Forms.Panel();
            this.txtchargessum = new System.Windows.Forms.TextBox();
            this.txtbasicval = new System.Windows.Forms.TextBox();
            this.txttax = new System.Windows.Forms.TextBox();
            this.txttotamt = new System.Windows.Forms.TextBox();
            this.label58 = new System.Windows.Forms.Label();
            this.button13 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.buttnext1 = new System.Windows.Forms.Button();
            this.chkact = new System.Windows.Forms.CheckBox();
            this.butcan = new System.Windows.Forms.Button();
            this.butedit = new System.Windows.Forms.Button();
            this.button6 = new System.Windows.Forms.Button();
            this.Editpan = new System.Windows.Forms.Panel();
            this.txtdcdate = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.HFGST = new System.Windows.Forms.DataGridView();
            this.dcdate = new System.Windows.Forms.DateTimePicker();
            this.txtprice = new System.Windows.Forms.TextBox();
            this.txtgen1 = new System.Windows.Forms.TextBox();
            this.button7 = new System.Windows.Forms.Button();
            this.label34 = new System.Windows.Forms.Label();
            this.label35 = new System.Windows.Forms.Label();
            this.label36 = new System.Windows.Forms.Label();
            this.label57 = new System.Windows.Forms.Label();
            this.label59 = new System.Windows.Forms.Label();
            this.txtexcise = new System.Windows.Forms.TextBox();
            this.label54 = new System.Windows.Forms.Label();
            this.txtttot = new System.Windows.Forms.TextBox();
            this.label47 = new System.Windows.Forms.Label();
            this.txttprdval = new System.Windows.Forms.TextBox();
            this.label46 = new System.Windows.Forms.Label();
            this.txttdisc = new System.Windows.Forms.TextBox();
            this.txttbval = new System.Windows.Forms.TextBox();
            this.label45 = new System.Windows.Forms.Label();
            this.label43 = new System.Windows.Forms.Label();
            this.txttdis = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.TxtNetAmt = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.TxtRoff = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.txtcharges = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.txtpadd1 = new System.Windows.Forms.RichTextBox();
            this.label55 = new System.Windows.Forms.Label();
            this.label40 = new System.Windows.Forms.Label();
            this.txttrans = new System.Windows.Forms.TextBox();
            this.Txttot = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.button3 = new System.Windows.Forms.Button();
            this.txtamt = new System.Windows.Forms.TextBox();
            this.button2 = new System.Windows.Forms.Button();
            this.DTPDOCDT = new System.Windows.Forms.DateTimePicker();
            this.HFIT = new System.Windows.Forms.DataGridView();
            this.label6 = new System.Windows.Forms.Label();
            this.txtdcno = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtname = new System.Windows.Forms.TextBox();
            this.Phone = new System.Windows.Forms.Label();
            this.txtgrn = new System.Windows.Forms.TextBox();
            this.txtrem = new System.Windows.Forms.RichTextBox();
            this.Dtprem = new System.Windows.Forms.DateTimePicker();
            this.label39 = new System.Windows.Forms.Label();
            this.txttempadd2 = new System.Windows.Forms.TextBox();
            this.txttempadd1 = new System.Windows.Forms.TextBox();
            this.txtitemname = new System.Windows.Forms.TextBox();
            this.txtnotes = new System.Windows.Forms.TextBox();
            this.txtbval = new System.Windows.Forms.TextBox();
            this.txtqty = new System.Windows.Forms.TextBox();
            this.dtpdc = new System.Windows.Forms.DateTimePicker();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.txtpuid = new System.Windows.Forms.TextBox();
            this.txtdcid = new System.Windows.Forms.TextBox();
            this.txttitemid = new System.Windows.Forms.TextBox();
            this.txtlisid = new System.Windows.Forms.TextBox();
            this.txtitemcode = new System.Windows.Forms.TextBox();
            this.txttgstp = new System.Windows.Forms.TextBox();
            this.txtgrnid = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txtuom = new System.Windows.Forms.TextBox();
            this.txtpluid = new System.Windows.Forms.TextBox();
            this.txttgstval = new System.Windows.Forms.TextBox();
            this.txtigval = new System.Windows.Forms.TextBox();
            this.txtigstp = new System.Windows.Forms.TextBox();
            this.label52 = new System.Windows.Forms.Label();
            this.label53 = new System.Windows.Forms.Label();
            this.txttsgval = new System.Windows.Forms.TextBox();
            this.txtsgstp = new System.Windows.Forms.TextBox();
            this.label50 = new System.Windows.Forms.Label();
            this.label51 = new System.Windows.Forms.Label();
            this.txttcgval = new System.Windows.Forms.TextBox();
            this.txttcgstp = new System.Windows.Forms.TextBox();
            this.label49 = new System.Windows.Forms.Label();
            this.label48 = new System.Windows.Forms.Label();
            this.Dtpdt = new System.Windows.Forms.DateTimePicker();
            this.Dtppre = new System.Windows.Forms.DateTimePicker();
            this.txtot = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.textBox3 = new System.Windows.Forms.TextBox();
            this.btnsave = new System.Windows.Forms.Button();
            this.buttnfinbk = new System.Windows.Forms.Button();
            this.Genpan.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.HFGP)).BeginInit();
            this.panadd.SuspendLayout();
            this.Editpan.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.HFGST)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.HFIT)).BeginInit();
            this.SuspendLayout();
            // 
            // Genpan
            // 
            this.Genpan.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.Genpan.Controls.Add(this.label60);
            this.Genpan.Controls.Add(this.dtpfnt);
            this.Genpan.Controls.Add(this.txtscr9);
            this.Genpan.Controls.Add(this.txtscr7);
            this.Genpan.Controls.Add(this.txtscr6);
            this.Genpan.Controls.Add(this.label7);
            this.Genpan.Controls.Add(this.txtscr4);
            this.Genpan.Controls.Add(this.Txtscr2);
            this.Genpan.Controls.Add(this.txtscr1);
            this.Genpan.Controls.Add(this.HFGP);
            this.Genpan.Controls.Add(this.txtscr5);
            this.Genpan.Controls.Add(this.txtscr8);
            this.Genpan.Controls.Add(this.Txtscr3);
            this.Genpan.Controls.Add(this.dateTimePicker1);
            this.Genpan.Location = new System.Drawing.Point(-1, -1);
            this.Genpan.Margin = new System.Windows.Forms.Padding(4);
            this.Genpan.Name = "Genpan";
            this.Genpan.Size = new System.Drawing.Size(1229, 515);
            this.Genpan.TabIndex = 188;
            // 
            // label60
            // 
            this.label60.AutoSize = true;
            this.label60.Font = new System.Drawing.Font("Calibri", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label60.Location = new System.Drawing.Point(519, 11);
            this.label60.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label60.Name = "label60";
            this.label60.Size = new System.Drawing.Size(58, 21);
            this.label60.TabIndex = 225;
            this.label60.Text = "Month";
            // 
            // dtpfnt
            // 
            this.dtpfnt.CustomFormat = "MMM/yyyy";
            this.dtpfnt.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpfnt.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpfnt.Location = new System.Drawing.Point(586, 10);
            this.dtpfnt.Margin = new System.Windows.Forms.Padding(4);
            this.dtpfnt.Name = "dtpfnt";
            this.dtpfnt.Size = new System.Drawing.Size(104, 26);
            this.dtpfnt.TabIndex = 224;
            this.dtpfnt.Value = new System.DateTime(2017, 7, 4, 0, 0, 0, 0);
            this.dtpfnt.ValueChanged += new System.EventHandler(this.dtpfnt_ValueChanged);
            // 
            // txtscr9
            // 
            this.txtscr9.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtscr9.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtscr9.Location = new System.Drawing.Point(1105, 43);
            this.txtscr9.Margin = new System.Windows.Forms.Padding(5);
            this.txtscr9.Name = "txtscr9";
            this.txtscr9.Size = new System.Drawing.Size(112, 26);
            this.txtscr9.TabIndex = 205;
            // 
            // txtscr7
            // 
            this.txtscr7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtscr7.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtscr7.Location = new System.Drawing.Point(944, 43);
            this.txtscr7.Margin = new System.Windows.Forms.Padding(5);
            this.txtscr7.Name = "txtscr7";
            this.txtscr7.Size = new System.Drawing.Size(60, 26);
            this.txtscr7.TabIndex = 203;
            // 
            // txtscr6
            // 
            this.txtscr6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtscr6.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtscr6.Location = new System.Drawing.Point(847, 43);
            this.txtscr6.Margin = new System.Windows.Forms.Padding(5);
            this.txtscr6.Name = "txtscr6";
            this.txtscr6.Size = new System.Drawing.Size(98, 26);
            this.txtscr6.TabIndex = 202;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Calibri", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(-3, 11);
            this.label7.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(96, 21);
            this.label7.TabIndex = 201;
            this.label7.Text = "Sales Return";
            // 
            // txtscr4
            // 
            this.txtscr4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtscr4.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtscr4.Location = new System.Drawing.Point(312, 43);
            this.txtscr4.Margin = new System.Windows.Forms.Padding(5);
            this.txtscr4.Name = "txtscr4";
            this.txtscr4.Size = new System.Drawing.Size(96, 26);
            this.txtscr4.TabIndex = 100;
            // 
            // Txtscr2
            // 
            this.Txtscr2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Txtscr2.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txtscr2.Location = new System.Drawing.Point(89, 43);
            this.Txtscr2.Margin = new System.Windows.Forms.Padding(5);
            this.Txtscr2.Name = "Txtscr2";
            this.Txtscr2.Size = new System.Drawing.Size(90, 26);
            this.Txtscr2.TabIndex = 87;
            // 
            // txtscr1
            // 
            this.txtscr1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtscr1.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtscr1.Location = new System.Drawing.Point(1, 43);
            this.txtscr1.Margin = new System.Windows.Forms.Padding(5);
            this.txtscr1.Name = "txtscr1";
            this.txtscr1.Size = new System.Drawing.Size(90, 26);
            this.txtscr1.TabIndex = 1;
            // 
            // HFGP
            // 
            this.HFGP.BackgroundColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.HFGP.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.HFGP.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnF2;
            this.HFGP.Location = new System.Drawing.Point(1, 74);
            this.HFGP.Margin = new System.Windows.Forms.Padding(4);
            this.HFGP.Name = "HFGP";
            this.HFGP.ReadOnly = true;
            this.HFGP.Size = new System.Drawing.Size(1213, 440);
            this.HFGP.TabIndex = 3;
            // 
            // txtscr5
            // 
            this.txtscr5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtscr5.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtscr5.Location = new System.Drawing.Point(408, 43);
            this.txtscr5.Margin = new System.Windows.Forms.Padding(5);
            this.txtscr5.Name = "txtscr5";
            this.txtscr5.Size = new System.Drawing.Size(441, 26);
            this.txtscr5.TabIndex = 90;
            // 
            // txtscr8
            // 
            this.txtscr8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtscr8.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtscr8.Location = new System.Drawing.Point(1003, 43);
            this.txtscr8.Margin = new System.Windows.Forms.Padding(5);
            this.txtscr8.Name = "txtscr8";
            this.txtscr8.Size = new System.Drawing.Size(103, 26);
            this.txtscr8.TabIndex = 204;
            // 
            // Txtscr3
            // 
            this.Txtscr3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Txtscr3.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txtscr3.Location = new System.Drawing.Point(178, 43);
            this.Txtscr3.Margin = new System.Windows.Forms.Padding(5);
            this.Txtscr3.Name = "Txtscr3";
            this.Txtscr3.Size = new System.Drawing.Size(134, 26);
            this.Txtscr3.TabIndex = 88;
            // 
            // dateTimePicker1
            // 
            this.dateTimePicker1.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dateTimePicker1.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dateTimePicker1.Location = new System.Drawing.Point(127, 101);
            this.dateTimePicker1.Margin = new System.Windows.Forms.Padding(4);
            this.dateTimePicker1.Name = "dateTimePicker1";
            this.dateTimePicker1.Size = new System.Drawing.Size(113, 22);
            this.dateTimePicker1.TabIndex = 334;
            // 
            // panadd
            // 
            this.panadd.BackColor = System.Drawing.Color.White;
            this.panadd.Controls.Add(this.txtchargessum);
            this.panadd.Controls.Add(this.txtbasicval);
            this.panadd.Controls.Add(this.txttax);
            this.panadd.Controls.Add(this.txttotamt);
            this.panadd.Controls.Add(this.label58);
            this.panadd.Controls.Add(this.button13);
            this.panadd.Controls.Add(this.button1);
            this.panadd.Controls.Add(this.buttnext1);
            this.panadd.Controls.Add(this.chkact);
            this.panadd.Controls.Add(this.butcan);
            this.panadd.Controls.Add(this.butedit);
            this.panadd.Controls.Add(this.button6);
            this.panadd.Location = new System.Drawing.Point(-1, 517);
            this.panadd.Name = "panadd";
            this.panadd.Size = new System.Drawing.Size(1233, 53);
            this.panadd.TabIndex = 236;
            // 
            // txtchargessum
            // 
            this.txtchargessum.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtchargessum.Enabled = false;
            this.txtchargessum.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtchargessum.Location = new System.Drawing.Point(904, 17);
            this.txtchargessum.Margin = new System.Windows.Forms.Padding(5);
            this.txtchargessum.Name = "txtchargessum";
            this.txtchargessum.Size = new System.Drawing.Size(100, 26);
            this.txtchargessum.TabIndex = 235;
            this.txtchargessum.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txtbasicval
            // 
            this.txtbasicval.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtbasicval.Enabled = false;
            this.txtbasicval.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtbasicval.Location = new System.Drawing.Point(805, 17);
            this.txtbasicval.Margin = new System.Windows.Forms.Padding(5);
            this.txtbasicval.Name = "txtbasicval";
            this.txtbasicval.Size = new System.Drawing.Size(100, 26);
            this.txtbasicval.TabIndex = 233;
            this.txtbasicval.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txttax
            // 
            this.txttax.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txttax.Enabled = false;
            this.txttax.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txttax.Location = new System.Drawing.Point(1003, 17);
            this.txttax.Margin = new System.Windows.Forms.Padding(5);
            this.txttax.Name = "txttax";
            this.txttax.Size = new System.Drawing.Size(100, 26);
            this.txttax.TabIndex = 234;
            this.txttax.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txttotamt
            // 
            this.txttotamt.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txttotamt.Enabled = false;
            this.txttotamt.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txttotamt.Location = new System.Drawing.Point(1102, 17);
            this.txttotamt.Margin = new System.Windows.Forms.Padding(5);
            this.txttotamt.Name = "txttotamt";
            this.txttotamt.Size = new System.Drawing.Size(100, 26);
            this.txttotamt.TabIndex = 232;
            this.txttotamt.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label58
            // 
            this.label58.AutoSize = true;
            this.label58.Font = new System.Drawing.Font("Calibri", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label58.Location = new System.Drawing.Point(749, 21);
            this.label58.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label58.Name = "label58";
            this.label58.Size = new System.Drawing.Size(43, 21);
            this.label58.TabIndex = 231;
            this.label58.Text = "Total";
            // 
            // button13
            // 
            this.button13.BackColor = System.Drawing.Color.White;
            this.button13.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button13.Image = ((System.Drawing.Image)(resources.GetObject("button13.Image")));
            this.button13.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button13.Location = new System.Drawing.Point(422, 13);
            this.button13.Name = "button13";
            this.button13.Size = new System.Drawing.Size(117, 30);
            this.button13.TabIndex = 225;
            this.button13.Text = "Print Preview";
            this.button13.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button13.UseVisualStyleBackColor = false;
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.White;
            this.button1.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.Image = ((System.Drawing.Image)(resources.GetObject("button1.Image")));
            this.button1.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button1.Location = new System.Drawing.Point(358, 13);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(65, 30);
            this.button1.TabIndex = 216;
            this.button1.Text = "Print";
            this.button1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button1.UseVisualStyleBackColor = false;
            // 
            // buttnext1
            // 
            this.buttnext1.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.buttnext1.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttnext1.Image = ((System.Drawing.Image)(resources.GetObject("buttnext1.Image")));
            this.buttnext1.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttnext1.Location = new System.Drawing.Point(541, 13);
            this.buttnext1.Name = "buttnext1";
            this.buttnext1.Size = new System.Drawing.Size(60, 30);
            this.buttnext1.TabIndex = 208;
            this.buttnext1.Text = "Exit";
            this.buttnext1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.buttnext1.UseVisualStyleBackColor = false;
            this.buttnext1.Click += new System.EventHandler(this.buttnext1_Click);
            // 
            // chkact
            // 
            this.chkact.AutoSize = true;
            this.chkact.BackColor = System.Drawing.Color.White;
            this.chkact.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkact.Location = new System.Drawing.Point(3, 19);
            this.chkact.Name = "chkact";
            this.chkact.Size = new System.Drawing.Size(65, 22);
            this.chkact.TabIndex = 187;
            this.chkact.Text = "Active";
            this.chkact.UseVisualStyleBackColor = false;
            // 
            // butcan
            // 
            this.butcan.BackColor = System.Drawing.Color.White;
            this.butcan.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.butcan.Image = ((System.Drawing.Image)(resources.GetObject("butcan.Image")));
            this.butcan.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.butcan.Location = new System.Drawing.Point(231, 13);
            this.butcan.Name = "butcan";
            this.butcan.Size = new System.Drawing.Size(121, 30);
            this.butcan.TabIndex = 186;
            this.butcan.Text = "Invoice Cancel";
            this.butcan.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.butcan.UseVisualStyleBackColor = false;
            // 
            // butedit
            // 
            this.butedit.BackColor = System.Drawing.Color.White;
            this.butedit.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.butedit.Image = ((System.Drawing.Image)(resources.GetObject("butedit.Image")));
            this.butedit.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.butedit.Location = new System.Drawing.Point(170, 13);
            this.butedit.Name = "butedit";
            this.butedit.Size = new System.Drawing.Size(55, 30);
            this.butedit.TabIndex = 185;
            this.butedit.Text = "Edit";
            this.butedit.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.butedit.UseVisualStyleBackColor = false;
            this.butedit.Click += new System.EventHandler(this.butedit_Click);
            // 
            // button6
            // 
            this.button6.BackColor = System.Drawing.Color.White;
            this.button6.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.button6.FlatAppearance.BorderSize = 0;
            this.button6.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button6.Image = ((System.Drawing.Image)(resources.GetObject("button6.Image")));
            this.button6.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button6.Location = new System.Drawing.Point(74, 13);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(93, 30);
            this.button6.TabIndex = 184;
            this.button6.Text = "Add new";
            this.button6.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button6.UseVisualStyleBackColor = false;
            this.button6.Click += new System.EventHandler(this.button6_Click);
            // 
            // Editpan
            // 
            this.Editpan.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.Editpan.Controls.Add(this.txtdcdate);
            this.Editpan.Controls.Add(this.label9);
            this.Editpan.Controls.Add(this.HFGST);
            this.Editpan.Controls.Add(this.dcdate);
            this.Editpan.Controls.Add(this.txtprice);
            this.Editpan.Controls.Add(this.txtgen1);
            this.Editpan.Controls.Add(this.button7);
            this.Editpan.Controls.Add(this.label34);
            this.Editpan.Controls.Add(this.label35);
            this.Editpan.Controls.Add(this.label36);
            this.Editpan.Controls.Add(this.label57);
            this.Editpan.Controls.Add(this.label59);
            this.Editpan.Controls.Add(this.txtexcise);
            this.Editpan.Controls.Add(this.label54);
            this.Editpan.Controls.Add(this.txtttot);
            this.Editpan.Controls.Add(this.label47);
            this.Editpan.Controls.Add(this.txttprdval);
            this.Editpan.Controls.Add(this.label46);
            this.Editpan.Controls.Add(this.txttdisc);
            this.Editpan.Controls.Add(this.txttbval);
            this.Editpan.Controls.Add(this.label45);
            this.Editpan.Controls.Add(this.label43);
            this.Editpan.Controls.Add(this.txttdis);
            this.Editpan.Controls.Add(this.label17);
            this.Editpan.Controls.Add(this.TxtNetAmt);
            this.Editpan.Controls.Add(this.label16);
            this.Editpan.Controls.Add(this.TxtRoff);
            this.Editpan.Controls.Add(this.label8);
            this.Editpan.Controls.Add(this.txtcharges);
            this.Editpan.Controls.Add(this.label12);
            this.Editpan.Controls.Add(this.txtpadd1);
            this.Editpan.Controls.Add(this.label55);
            this.Editpan.Controls.Add(this.label40);
            this.Editpan.Controls.Add(this.txttrans);
            this.Editpan.Controls.Add(this.Txttot);
            this.Editpan.Controls.Add(this.label1);
            this.Editpan.Controls.Add(this.button3);
            this.Editpan.Controls.Add(this.txtamt);
            this.Editpan.Controls.Add(this.button2);
            this.Editpan.Controls.Add(this.DTPDOCDT);
            this.Editpan.Controls.Add(this.HFIT);
            this.Editpan.Controls.Add(this.label6);
            this.Editpan.Controls.Add(this.txtdcno);
            this.Editpan.Controls.Add(this.label3);
            this.Editpan.Controls.Add(this.txtname);
            this.Editpan.Controls.Add(this.Phone);
            this.Editpan.Controls.Add(this.txtgrn);
            this.Editpan.Controls.Add(this.txtrem);
            this.Editpan.Controls.Add(this.Dtprem);
            this.Editpan.Controls.Add(this.label39);
            this.Editpan.Controls.Add(this.txttempadd2);
            this.Editpan.Controls.Add(this.txttempadd1);
            this.Editpan.Controls.Add(this.txtitemname);
            this.Editpan.Controls.Add(this.txtnotes);
            this.Editpan.Controls.Add(this.txtbval);
            this.Editpan.Controls.Add(this.txtqty);
            this.Editpan.Controls.Add(this.dtpdc);
            this.Editpan.Controls.Add(this.label10);
            this.Editpan.Controls.Add(this.label11);
            this.Editpan.Controls.Add(this.txtpuid);
            this.Editpan.Controls.Add(this.txtdcid);
            this.Editpan.Controls.Add(this.txttitemid);
            this.Editpan.Controls.Add(this.txtlisid);
            this.Editpan.Controls.Add(this.txtitemcode);
            this.Editpan.Controls.Add(this.txttgstp);
            this.Editpan.Controls.Add(this.txtgrnid);
            this.Editpan.Controls.Add(this.label2);
            this.Editpan.Controls.Add(this.textBox2);
            this.Editpan.Controls.Add(this.label5);
            this.Editpan.Controls.Add(this.txtuom);
            this.Editpan.Controls.Add(this.txtpluid);
            this.Editpan.Controls.Add(this.txttgstval);
            this.Editpan.Controls.Add(this.txtigval);
            this.Editpan.Controls.Add(this.txtigstp);
            this.Editpan.Controls.Add(this.label52);
            this.Editpan.Controls.Add(this.label53);
            this.Editpan.Controls.Add(this.txttsgval);
            this.Editpan.Controls.Add(this.txtsgstp);
            this.Editpan.Controls.Add(this.label50);
            this.Editpan.Controls.Add(this.label51);
            this.Editpan.Controls.Add(this.txttcgval);
            this.Editpan.Controls.Add(this.txttcgstp);
            this.Editpan.Controls.Add(this.label49);
            this.Editpan.Controls.Add(this.label48);
            this.Editpan.Controls.Add(this.Dtpdt);
            this.Editpan.Controls.Add(this.Dtppre);
            this.Editpan.Controls.Add(this.txtot);
            this.Editpan.Controls.Add(this.label4);
            this.Editpan.Controls.Add(this.textBox3);
            this.Editpan.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Editpan.Location = new System.Drawing.Point(0, -1);
            this.Editpan.Margin = new System.Windows.Forms.Padding(4);
            this.Editpan.Name = "Editpan";
            this.Editpan.Size = new System.Drawing.Size(1232, 517);
            this.Editpan.TabIndex = 244;
            this.Editpan.Paint += new System.Windows.Forms.PaintEventHandler(this.Editpan_Paint);
            // 
            // txtdcdate
            // 
            this.txtdcdate.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtdcdate.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtdcdate.Location = new System.Drawing.Point(131, 76);
            this.txtdcdate.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.txtdcdate.Name = "txtdcdate";
            this.txtdcdate.Size = new System.Drawing.Size(117, 26);
            this.txtdcdate.TabIndex = 354;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Calibri", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(1026, 214);
            this.label9.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(79, 21);
            this.label9.TabIndex = 350;
            this.label9.Text = "GST Value";
            // 
            // HFGST
            // 
            this.HFGST.BackgroundColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.HFGST.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.HFGST.Location = new System.Drawing.Point(849, 390);
            this.HFGST.Margin = new System.Windows.Forms.Padding(4);
            this.HFGST.Name = "HFGST";
            this.HFGST.Size = new System.Drawing.Size(373, 120);
            this.HFGST.TabIndex = 349;
            this.HFGST.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.HFGST_CellContentClick);
            // 
            // dcdate
            // 
            this.dcdate.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dcdate.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dcdate.Location = new System.Drawing.Point(131, 75);
            this.dcdate.Margin = new System.Windows.Forms.Padding(4);
            this.dcdate.Name = "dcdate";
            this.dcdate.Size = new System.Drawing.Size(113, 22);
            this.dcdate.TabIndex = 334;
            // 
            // txtprice
            // 
            this.txtprice.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtprice.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtprice.Location = new System.Drawing.Point(357, 181);
            this.txtprice.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.txtprice.Name = "txtprice";
            this.txtprice.Size = new System.Drawing.Size(68, 26);
            this.txtprice.TabIndex = 333;
            this.txtprice.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txtgen1
            // 
            this.txtgen1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtgen1.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtgen1.Location = new System.Drawing.Point(7, 76);
            this.txtgen1.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.txtgen1.Name = "txtgen1";
            this.txtgen1.Size = new System.Drawing.Size(117, 26);
            this.txtgen1.TabIndex = 331;
            this.txtgen1.Click += new System.EventHandler(this.txtgen1_Click);
            this.txtgen1.TextChanged += new System.EventHandler(this.txtgen1_TextChanged);
            // 
            // button7
            // 
            this.button7.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.button7.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button7.Image = global::ACV.Properties.Resources.ok1_25x25;
            this.button7.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.button7.Location = new System.Drawing.Point(802, 173);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(34, 32);
            this.button7.TabIndex = 330;
            this.button7.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button7.UseVisualStyleBackColor = false;
            this.button7.Click += new System.EventHandler(this.button7_Click);
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Font = new System.Drawing.Font("Calibri", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label34.Location = new System.Drawing.Point(599, 157);
            this.label34.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(77, 21);
            this.label34.TabIndex = 329;
            this.label34.Text = "Addnotes";
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Font = new System.Drawing.Font("Calibri", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label35.Location = new System.Drawing.Point(501, 157);
            this.label35.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(83, 21);
            this.label35.TabIndex = 328;
            this.label35.Text = "BasicValue";
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Font = new System.Drawing.Font("Calibri", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label36.Location = new System.Drawing.Point(432, 157);
            this.label36.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(35, 21);
            this.label36.TabIndex = 326;
            this.label36.Text = "Qty";
            // 
            // label57
            // 
            this.label57.AutoSize = true;
            this.label57.Font = new System.Drawing.Font("Calibri", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label57.Location = new System.Drawing.Point(6, 154);
            this.label57.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label57.Name = "label57";
            this.label57.Size = new System.Drawing.Size(83, 21);
            this.label57.TabIndex = 325;
            this.label57.Text = "ItemName";
            // 
            // label59
            // 
            this.label59.AutoSize = true;
            this.label59.Font = new System.Drawing.Font("Calibri", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label59.Location = new System.Drawing.Point(367, 157);
            this.label59.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label59.Name = "label59";
            this.label59.Size = new System.Drawing.Size(44, 21);
            this.label59.TabIndex = 324;
            this.label59.Text = "Price";
            // 
            // txtexcise
            // 
            this.txtexcise.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtexcise.Enabled = false;
            this.txtexcise.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtexcise.Location = new System.Drawing.Point(1110, 182);
            this.txtexcise.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.txtexcise.Name = "txtexcise";
            this.txtexcise.Size = new System.Drawing.Size(112, 26);
            this.txtexcise.TabIndex = 317;
            this.txtexcise.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label54
            // 
            this.label54.AutoSize = true;
            this.label54.Font = new System.Drawing.Font("Calibri", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label54.Location = new System.Drawing.Point(1021, 246);
            this.label54.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label54.Name = "label54";
            this.label54.Size = new System.Drawing.Size(85, 21);
            this.label54.TabIndex = 316;
            this.label54.Text = "Total Value";
            // 
            // txtttot
            // 
            this.txtttot.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtttot.Enabled = false;
            this.txtttot.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtttot.Location = new System.Drawing.Point(1110, 240);
            this.txtttot.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.txtttot.Name = "txtttot";
            this.txtttot.Size = new System.Drawing.Size(111, 26);
            this.txtttot.TabIndex = 315;
            this.txtttot.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label47
            // 
            this.label47.AutoSize = true;
            this.label47.Font = new System.Drawing.Font("Calibri", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label47.Location = new System.Drawing.Point(1002, 119);
            this.label47.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label47.Name = "label47";
            this.label47.Size = new System.Drawing.Size(107, 21);
            this.label47.TabIndex = 302;
            this.label47.Text = "Product Value";
            // 
            // txttprdval
            // 
            this.txttprdval.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txttprdval.Enabled = false;
            this.txttprdval.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txttprdval.Location = new System.Drawing.Point(1109, 117);
            this.txttprdval.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.txttprdval.Name = "txttprdval";
            this.txttprdval.Size = new System.Drawing.Size(112, 26);
            this.txttprdval.TabIndex = 301;
            this.txttprdval.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label46
            // 
            this.label46.AutoSize = true;
            this.label46.Font = new System.Drawing.Font("Calibri", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label46.Location = new System.Drawing.Point(997, 87);
            this.label46.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label46.Name = "label46";
            this.label46.Size = new System.Drawing.Size(113, 21);
            this.label46.TabIndex = 300;
            this.label46.Text = "Discount Value";
            // 
            // txttdisc
            // 
            this.txttdisc.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txttdisc.Enabled = false;
            this.txttdisc.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txttdisc.Location = new System.Drawing.Point(1110, 85);
            this.txttdisc.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.txttdisc.Name = "txttdisc";
            this.txttdisc.Size = new System.Drawing.Size(112, 26);
            this.txttdisc.TabIndex = 299;
            this.txttdisc.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txttbval
            // 
            this.txttbval.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txttbval.Enabled = false;
            this.txttbval.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txttbval.Location = new System.Drawing.Point(1110, 26);
            this.txttbval.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.txttbval.Name = "txttbval";
            this.txttbval.Size = new System.Drawing.Size(112, 26);
            this.txttbval.TabIndex = 298;
            this.txttbval.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label45
            // 
            this.label45.AutoSize = true;
            this.label45.Font = new System.Drawing.Font("Calibri", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label45.Location = new System.Drawing.Point(1023, 28);
            this.label45.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(87, 21);
            this.label45.TabIndex = 297;
            this.label45.Text = "Basic Value";
            // 
            // label43
            // 
            this.label43.AutoSize = true;
            this.label43.Font = new System.Drawing.Font("Calibri", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label43.Location = new System.Drawing.Point(1023, 58);
            this.label43.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(87, 21);
            this.label43.TabIndex = 296;
            this.label43.Text = "Discount %";
            // 
            // txttdis
            // 
            this.txttdis.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txttdis.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txttdis.Location = new System.Drawing.Point(1110, 56);
            this.txttdis.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.txttdis.Name = "txttdis";
            this.txttdis.Size = new System.Drawing.Size(112, 26);
            this.txttdis.TabIndex = 295;
            this.txttdis.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txttdis.TextChanged += new System.EventHandler(this.txttdis_TextChanged);
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Calibri", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.Location = new System.Drawing.Point(1031, 309);
            this.label17.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(77, 21);
            this.label17.TabIndex = 294;
            this.label17.Text = "Net Value";
            // 
            // TxtNetAmt
            // 
            this.TxtNetAmt.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.TxtNetAmt.Enabled = false;
            this.TxtNetAmt.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtNetAmt.ForeColor = System.Drawing.Color.Red;
            this.TxtNetAmt.Location = new System.Drawing.Point(1110, 306);
            this.TxtNetAmt.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.TxtNetAmt.Name = "TxtNetAmt";
            this.TxtNetAmt.Size = new System.Drawing.Size(112, 26);
            this.TxtNetAmt.TabIndex = 293;
            this.TxtNetAmt.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Calibri", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(1061, 276);
            this.label16.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(44, 21);
            this.label16.TabIndex = 292;
            this.label16.Text = "R.Off";
            // 
            // TxtRoff
            // 
            this.TxtRoff.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.TxtRoff.Enabled = false;
            this.TxtRoff.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtRoff.Location = new System.Drawing.Point(1110, 274);
            this.TxtRoff.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.TxtRoff.Name = "TxtRoff";
            this.TxtRoff.Size = new System.Drawing.Size(111, 26);
            this.TxtRoff.TabIndex = 291;
            this.TxtRoff.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Calibri", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(1006, 183);
            this.label8.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(103, 21);
            this.label8.TabIndex = 290;
            this.label8.Text = "Taxable Value";
            // 
            // txtcharges
            // 
            this.txtcharges.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtcharges.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtcharges.Location = new System.Drawing.Point(1109, 148);
            this.txtcharges.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.txtcharges.Name = "txtcharges";
            this.txtcharges.Size = new System.Drawing.Size(112, 26);
            this.txtcharges.TabIndex = 289;
            this.txtcharges.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtcharges.TextChanged += new System.EventHandler(this.txtcharges_TextChanged);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Calibri", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(1041, 150);
            this.label12.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(69, 21);
            this.label12.TabIndex = 288;
            this.label12.Text = " Charges";
            // 
            // txtpadd1
            // 
            this.txtpadd1.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtpadd1.Location = new System.Drawing.Point(250, 60);
            this.txtpadd1.MaxLength = 100;
            this.txtpadd1.Name = "txtpadd1";
            this.txtpadd1.Size = new System.Drawing.Size(447, 91);
            this.txtpadd1.TabIndex = 267;
            this.txtpadd1.Text = "";
            this.txtpadd1.TextChanged += new System.EventHandler(this.txtpadd1_TextChanged);
            // 
            // label55
            // 
            this.label55.AutoSize = true;
            this.label55.Font = new System.Drawing.Font("Calibri", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label55.Location = new System.Drawing.Point(534, 490);
            this.label55.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label55.Name = "label55";
            this.label55.Size = new System.Drawing.Size(85, 21);
            this.label55.TabIndex = 264;
            this.label55.Text = "Total Value";
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Font = new System.Drawing.Font("Calibri", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label40.Location = new System.Drawing.Point(4, 58);
            this.label40.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(54, 21);
            this.label40.TabIndex = 257;
            this.label40.Text = "Inv.No";
            // 
            // txttrans
            // 
            this.txttrans.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txttrans.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txttrans.Location = new System.Drawing.Point(303, 62);
            this.txttrans.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.txttrans.Name = "txttrans";
            this.txttrans.Size = new System.Drawing.Size(118, 22);
            this.txttrans.TabIndex = 226;
            // 
            // Txttot
            // 
            this.Txttot.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Txttot.Enabled = false;
            this.Txttot.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txttot.Location = new System.Drawing.Point(624, 488);
            this.Txttot.Margin = new System.Windows.Forms.Padding(5);
            this.Txttot.Name = "Txttot";
            this.Txttot.Size = new System.Drawing.Size(101, 26);
            this.Txttot.TabIndex = 202;
            this.Txttot.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Calibri", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(708, 2);
            this.label1.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(70, 21);
            this.label1.TabIndex = 233;
            this.label1.Text = "Remarks";
            // 
            // button3
            // 
            this.button3.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.button3.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button3.Image = ((System.Drawing.Image)(resources.GetObject("button3.Image")));
            this.button3.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button3.Location = new System.Drawing.Point(1012, 549);
            this.button3.Margin = new System.Windows.Forms.Padding(4);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(124, 47);
            this.button3.TabIndex = 227;
            this.button3.Text = "Back";
            this.button3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button3.UseVisualStyleBackColor = false;
            // 
            // txtamt
            // 
            this.txtamt.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtamt.Location = new System.Drawing.Point(775, 559);
            this.txtamt.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.txtamt.Name = "txtamt";
            this.txtamt.Size = new System.Drawing.Size(161, 22);
            this.txtamt.TabIndex = 226;
            // 
            // button2
            // 
            this.button2.BackColor = System.Drawing.Color.White;
            this.button2.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button2.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button2.Location = new System.Drawing.Point(1144, 550);
            this.button2.Margin = new System.Windows.Forms.Padding(4);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(113, 46);
            this.button2.TabIndex = 224;
            this.button2.Text = "Tax";
            this.button2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button2.UseVisualStyleBackColor = false;
            // 
            // DTPDOCDT
            // 
            this.DTPDOCDT.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DTPDOCDT.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.DTPDOCDT.Location = new System.Drawing.Point(130, 29);
            this.DTPDOCDT.Margin = new System.Windows.Forms.Padding(4);
            this.DTPDOCDT.Name = "DTPDOCDT";
            this.DTPDOCDT.Size = new System.Drawing.Size(113, 26);
            this.DTPDOCDT.TabIndex = 221;
            // 
            // HFIT
            // 
            this.HFIT.BackgroundColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.HFIT.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.HFIT.Location = new System.Drawing.Point(5, 214);
            this.HFIT.Margin = new System.Windows.Forms.Padding(4);
            this.HFIT.Name = "HFIT";
            this.HFIT.Size = new System.Drawing.Size(828, 263);
            this.HFIT.TabIndex = 214;
            this.HFIT.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.HFIT_CellContentClick);
            this.HFIT.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.HFIT_CellValueChanged);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Calibri", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(127, 4);
            this.label6.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(72, 21);
            this.label6.TabIndex = 204;
            this.label6.Text = "Doc.Date";
            // 
            // txtdcno
            // 
            this.txtdcno.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtdcno.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtdcno.Location = new System.Drawing.Point(303, 107);
            this.txtdcno.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.txtdcno.Name = "txtdcno";
            this.txtdcno.Size = new System.Drawing.Size(119, 22);
            this.txtdcno.TabIndex = 223;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Calibri", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(248, 10);
            this.label3.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(92, 21);
            this.label3.TabIndex = 195;
            this.label3.Text = "Party Name";
            // 
            // txtname
            // 
            this.txtname.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtname.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtname.Location = new System.Drawing.Point(250, 31);
            this.txtname.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.txtname.MaxLength = 100;
            this.txtname.Name = "txtname";
            this.txtname.Size = new System.Drawing.Size(448, 26);
            this.txtname.TabIndex = 222;
            this.txtname.Click += new System.EventHandler(this.txtname_Click);
            this.txtname.MouseClick += new System.Windows.Forms.MouseEventHandler(this.txtname_MouseClick);
            this.txtname.TextChanged += new System.EventHandler(this.txtname_TextChanged);
            this.txtname.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.txtname_MouseDoubleClick);
            // 
            // Phone
            // 
            this.Phone.AutoSize = true;
            this.Phone.Font = new System.Drawing.Font("Calibri", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Phone.Location = new System.Drawing.Point(3, 4);
            this.Phone.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.Phone.Name = "Phone";
            this.Phone.Size = new System.Drawing.Size(60, 21);
            this.Phone.TabIndex = 199;
            this.Phone.Text = "Doc.No";
            // 
            // txtgrn
            // 
            this.txtgrn.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtgrn.Enabled = false;
            this.txtgrn.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtgrn.Location = new System.Drawing.Point(6, 29);
            this.txtgrn.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.txtgrn.Name = "txtgrn";
            this.txtgrn.Size = new System.Drawing.Size(119, 26);
            this.txtgrn.TabIndex = 198;
            // 
            // txtrem
            // 
            this.txtrem.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtrem.Location = new System.Drawing.Point(704, 30);
            this.txtrem.MaxLength = 100;
            this.txtrem.Name = "txtrem";
            this.txtrem.Size = new System.Drawing.Size(290, 121);
            this.txtrem.TabIndex = 225;
            this.txtrem.Text = "";
            // 
            // Dtprem
            // 
            this.Dtprem.CustomFormat = "dd-MMM-yyyy  hh:mm tt";
            this.Dtprem.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Dtprem.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.Dtprem.Location = new System.Drawing.Point(563, 70);
            this.Dtprem.Margin = new System.Windows.Forms.Padding(4);
            this.Dtprem.Name = "Dtprem";
            this.Dtprem.Size = new System.Drawing.Size(112, 22);
            this.Dtprem.TabIndex = 255;
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Font = new System.Drawing.Font("Calibri", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label39.Location = new System.Drawing.Point(128, 58);
            this.label39.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(66, 21);
            this.label39.TabIndex = 254;
            this.label39.Text = "Inv.Date";
            // 
            // txttempadd2
            // 
            this.txttempadd2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txttempadd2.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txttempadd2.Location = new System.Drawing.Point(467, 75);
            this.txttempadd2.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.txttempadd2.Name = "txttempadd2";
            this.txttempadd2.Size = new System.Drawing.Size(45, 22);
            this.txttempadd2.TabIndex = 270;
            // 
            // txttempadd1
            // 
            this.txttempadd1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txttempadd1.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txttempadd1.Location = new System.Drawing.Point(414, 75);
            this.txttempadd1.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.txttempadd1.Name = "txttempadd1";
            this.txttempadd1.Size = new System.Drawing.Size(45, 22);
            this.txttempadd1.TabIndex = 269;
            // 
            // txtitemname
            // 
            this.txtitemname.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtitemname.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtitemname.Location = new System.Drawing.Point(6, 181);
            this.txtitemname.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.txtitemname.Name = "txtitemname";
            this.txtitemname.Size = new System.Drawing.Size(350, 26);
            this.txtitemname.TabIndex = 241;
            this.txtitemname.Click += new System.EventHandler(this.txtitemname_Click);
            this.txtitemname.TextChanged += new System.EventHandler(this.txtitemname_TextChanged);
            // 
            // txtnotes
            // 
            this.txtnotes.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtnotes.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtnotes.Location = new System.Drawing.Point(602, 181);
            this.txtnotes.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.txtnotes.Name = "txtnotes";
            this.txtnotes.Size = new System.Drawing.Size(200, 26);
            this.txtnotes.TabIndex = 260;
            // 
            // txtbval
            // 
            this.txtbval.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtbval.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtbval.Location = new System.Drawing.Point(494, 181);
            this.txtbval.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.txtbval.Name = "txtbval";
            this.txtbval.Size = new System.Drawing.Size(102, 26);
            this.txtbval.TabIndex = 245;
            this.txtbval.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txtqty
            // 
            this.txtqty.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtqty.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtqty.Location = new System.Drawing.Point(427, 181);
            this.txtqty.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.txtqty.Name = "txtqty";
            this.txtqty.Size = new System.Drawing.Size(65, 26);
            this.txtqty.TabIndex = 243;
            this.txtqty.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // dtpdc
            // 
            this.dtpdc.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpdc.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpdc.Location = new System.Drawing.Point(428, 107);
            this.dtpdc.Margin = new System.Windows.Forms.Padding(4);
            this.dtpdc.Name = "dtpdc";
            this.dtpdc.Size = new System.Drawing.Size(113, 22);
            this.dtpdc.TabIndex = 224;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(302, 92);
            this.label10.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(75, 16);
            this.label10.TabIndex = 273;
            this.label10.Text = "Invoice No";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(427, 92);
            this.label11.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(87, 16);
            this.label11.TabIndex = 271;
            this.label11.Text = "Invoice Date";
            // 
            // txtpuid
            // 
            this.txtpuid.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtpuid.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtpuid.Location = new System.Drawing.Point(282, 70);
            this.txtpuid.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.txtpuid.Name = "txtpuid";
            this.txtpuid.Size = new System.Drawing.Size(11, 22);
            this.txtpuid.TabIndex = 335;
            this.txtpuid.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtpuid.TextChanged += new System.EventHandler(this.txtpuid_TextChanged);
            // 
            // txtdcid
            // 
            this.txtdcid.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtdcid.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtdcid.Location = new System.Drawing.Point(97, 251);
            this.txtdcid.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.txtdcid.Name = "txtdcid";
            this.txtdcid.Size = new System.Drawing.Size(11, 22);
            this.txtdcid.TabIndex = 336;
            this.txtdcid.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtdcid.TextChanged += new System.EventHandler(this.txtdcid_TextChanged);
            // 
            // txttitemid
            // 
            this.txttitemid.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txttitemid.Location = new System.Drawing.Point(690, 313);
            this.txttitemid.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.txttitemid.Name = "txttitemid";
            this.txttitemid.Size = new System.Drawing.Size(35, 22);
            this.txttitemid.TabIndex = 340;
            // 
            // txtlisid
            // 
            this.txtlisid.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtlisid.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtlisid.Location = new System.Drawing.Point(650, 261);
            this.txtlisid.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.txtlisid.Name = "txtlisid";
            this.txtlisid.Size = new System.Drawing.Size(106, 22);
            this.txtlisid.TabIndex = 339;
            // 
            // txtitemcode
            // 
            this.txtitemcode.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtitemcode.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtitemcode.Location = new System.Drawing.Point(536, 344);
            this.txtitemcode.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.txtitemcode.Name = "txtitemcode";
            this.txtitemcode.Size = new System.Drawing.Size(119, 22);
            this.txtitemcode.TabIndex = 338;
            // 
            // txttgstp
            // 
            this.txttgstp.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txttgstp.Enabled = false;
            this.txttgstp.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txttgstp.Location = new System.Drawing.Point(257, 255);
            this.txttgstp.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.txttgstp.Name = "txttgstp";
            this.txttgstp.Size = new System.Drawing.Size(36, 22);
            this.txttgstp.TabIndex = 346;
            this.txttgstp.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txtgrnid
            // 
            this.txtgrnid.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtgrnid.Enabled = false;
            this.txtgrnid.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtgrnid.Location = new System.Drawing.Point(547, 306);
            this.txtgrnid.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.txtgrnid.Name = "txtgrnid";
            this.txtgrnid.Size = new System.Drawing.Size(36, 22);
            this.txtgrnid.TabIndex = 345;
            this.txtgrnid.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(362, 295);
            this.label2.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(44, 16);
            this.label2.TabIndex = 344;
            this.label2.Text = "SGST";
            // 
            // textBox2
            // 
            this.textBox2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox2.Enabled = false;
            this.textBox2.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox2.Location = new System.Drawing.Point(476, 404);
            this.textBox2.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(36, 22);
            this.textBox2.TabIndex = 343;
            this.textBox2.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.textBox2.TextChanged += new System.EventHandler(this.textBox2_TextChanged);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(183, 286);
            this.label5.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(44, 16);
            this.label5.TabIndex = 342;
            this.label5.Text = "CGST";
            // 
            // txtuom
            // 
            this.txtuom.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtuom.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtuom.Location = new System.Drawing.Point(479, 265);
            this.txtuom.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.txtuom.Name = "txtuom";
            this.txtuom.Size = new System.Drawing.Size(59, 22);
            this.txtuom.TabIndex = 341;
            // 
            // txtpluid
            // 
            this.txtpluid.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtpluid.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtpluid.Location = new System.Drawing.Point(585, 263);
            this.txtpluid.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.txtpluid.Name = "txtpluid";
            this.txtpluid.Size = new System.Drawing.Size(34, 22);
            this.txtpluid.TabIndex = 347;
            this.txtpluid.TextChanged += new System.EventHandler(this.txtpluid_TextChanged);
            // 
            // txttgstval
            // 
            this.txttgstval.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txttgstval.Enabled = false;
            this.txttgstval.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txttgstval.Location = new System.Drawing.Point(583, 286);
            this.txttgstval.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.txttgstval.Name = "txttgstval";
            this.txttgstval.Size = new System.Drawing.Size(36, 22);
            this.txttgstval.TabIndex = 348;
            this.txttgstval.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txtigval
            // 
            this.txtigval.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtigval.Enabled = false;
            this.txtigval.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtigval.Location = new System.Drawing.Point(1109, 212);
            this.txtigval.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.txtigval.Name = "txtigval";
            this.txtigval.Size = new System.Drawing.Size(112, 26);
            this.txtigval.TabIndex = 314;
            this.txtigval.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txtigstp
            // 
            this.txtigstp.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtigstp.Enabled = false;
            this.txtigstp.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtigstp.Location = new System.Drawing.Point(346, 323);
            this.txtigstp.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.txtigstp.Name = "txtigstp";
            this.txtigstp.Size = new System.Drawing.Size(36, 22);
            this.txtigstp.TabIndex = 313;
            this.txtigstp.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label52
            // 
            this.label52.AutoSize = true;
            this.label52.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label52.Location = new System.Drawing.Point(381, 325);
            this.label52.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label52.Name = "label52";
            this.label52.Size = new System.Drawing.Size(18, 16);
            this.label52.TabIndex = 312;
            this.label52.Text = "%";
            // 
            // label53
            // 
            this.label53.AutoSize = true;
            this.label53.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label53.Location = new System.Drawing.Point(301, 325);
            this.label53.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label53.Name = "label53";
            this.label53.Size = new System.Drawing.Size(39, 16);
            this.label53.TabIndex = 311;
            this.label53.Text = "IGST";
            // 
            // txttsgval
            // 
            this.txttsgval.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txttsgval.Enabled = false;
            this.txttsgval.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txttsgval.Location = new System.Drawing.Point(311, 357);
            this.txttsgval.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.txttsgval.Name = "txttsgval";
            this.txttsgval.Size = new System.Drawing.Size(112, 22);
            this.txttsgval.TabIndex = 310;
            this.txttsgval.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txtsgstp
            // 
            this.txtsgstp.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtsgstp.Enabled = false;
            this.txtsgstp.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtsgstp.Location = new System.Drawing.Point(346, 289);
            this.txtsgstp.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.txtsgstp.Name = "txtsgstp";
            this.txtsgstp.Size = new System.Drawing.Size(36, 22);
            this.txtsgstp.TabIndex = 309;
            this.txtsgstp.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label50
            // 
            this.label50.AutoSize = true;
            this.label50.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label50.Location = new System.Drawing.Point(381, 291);
            this.label50.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label50.Name = "label50";
            this.label50.Size = new System.Drawing.Size(18, 16);
            this.label50.TabIndex = 308;
            this.label50.Text = "%";
            // 
            // label51
            // 
            this.label51.AutoSize = true;
            this.label51.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label51.Location = new System.Drawing.Point(301, 291);
            this.label51.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label51.Name = "label51";
            this.label51.Size = new System.Drawing.Size(44, 16);
            this.label51.TabIndex = 307;
            this.label51.Text = "SGST";
            // 
            // txttcgval
            // 
            this.txttcgval.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txttcgval.Enabled = false;
            this.txttcgval.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txttcgval.Location = new System.Drawing.Point(402, 255);
            this.txttcgval.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.txttcgval.Name = "txttcgval";
            this.txttcgval.Size = new System.Drawing.Size(112, 22);
            this.txttcgval.TabIndex = 306;
            this.txttcgval.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txttcgstp
            // 
            this.txttcgstp.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txttcgstp.Enabled = false;
            this.txttcgstp.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txttcgstp.Location = new System.Drawing.Point(346, 255);
            this.txttcgstp.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.txttcgstp.Name = "txttcgstp";
            this.txttcgstp.Size = new System.Drawing.Size(36, 22);
            this.txttcgstp.TabIndex = 305;
            this.txttcgstp.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label49
            // 
            this.label49.AutoSize = true;
            this.label49.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label49.Location = new System.Drawing.Point(381, 257);
            this.label49.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label49.Name = "label49";
            this.label49.Size = new System.Drawing.Size(18, 16);
            this.label49.TabIndex = 304;
            this.label49.Text = "%";
            // 
            // label48
            // 
            this.label48.AutoSize = true;
            this.label48.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label48.Location = new System.Drawing.Point(301, 257);
            this.label48.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label48.Name = "label48";
            this.label48.Size = new System.Drawing.Size(44, 16);
            this.label48.TabIndex = 303;
            this.label48.Text = "CGST";
            // 
            // Dtpdt
            // 
            this.Dtpdt.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Dtpdt.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.Dtpdt.Location = new System.Drawing.Point(414, 320);
            this.Dtpdt.Margin = new System.Windows.Forms.Padding(4);
            this.Dtpdt.Name = "Dtpdt";
            this.Dtpdt.Size = new System.Drawing.Size(113, 22);
            this.Dtpdt.TabIndex = 352;
            // 
            // Dtppre
            // 
            this.Dtppre.AllowDrop = true;
            this.Dtppre.CustomFormat = "dd-MMM-yyyy  hh:mm tt";
            this.Dtppre.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Dtppre.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.Dtppre.Location = new System.Drawing.Point(479, 239);
            this.Dtppre.Margin = new System.Windows.Forms.Padding(4);
            this.Dtppre.Name = "Dtppre";
            this.Dtppre.Size = new System.Drawing.Size(175, 22);
            this.Dtppre.TabIndex = 353;
            // 
            // txtot
            // 
            this.txtot.Location = new System.Drawing.Point(663, 344);
            this.txtot.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.txtot.Name = "txtot";
            this.txtot.Size = new System.Drawing.Size(121, 26);
            this.txtot.TabIndex = 237;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(679, 372);
            this.label4.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(46, 18);
            this.label4.TabIndex = 238;
            this.label4.Text = "Total";
            // 
            // textBox3
            // 
            this.textBox3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox3.Enabled = false;
            this.textBox3.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox3.Location = new System.Drawing.Point(509, 295);
            this.textBox3.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new System.Drawing.Size(112, 22);
            this.textBox3.TabIndex = 351;
            this.textBox3.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.textBox3.TextChanged += new System.EventHandler(this.textBox3_TextChanged);
            // 
            // btnsave
            // 
            this.btnsave.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnsave.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnsave.Image = ((System.Drawing.Image)(resources.GetObject("btnsave.Image")));
            this.btnsave.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnsave.Location = new System.Drawing.Point(1100, 529);
            this.btnsave.Margin = new System.Windows.Forms.Padding(4);
            this.btnsave.Name = "btnsave";
            this.btnsave.Size = new System.Drawing.Size(60, 30);
            this.btnsave.TabIndex = 245;
            this.btnsave.Text = "Save";
            this.btnsave.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnsave.UseVisualStyleBackColor = false;
            this.btnsave.Click += new System.EventHandler(this.btnsave_Click);
            // 
            // buttnfinbk
            // 
            this.buttnfinbk.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.buttnfinbk.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttnfinbk.Image = ((System.Drawing.Image)(resources.GetObject("buttnfinbk.Image")));
            this.buttnfinbk.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttnfinbk.Location = new System.Drawing.Point(1168, 527);
            this.buttnfinbk.Margin = new System.Windows.Forms.Padding(4);
            this.buttnfinbk.Name = "buttnfinbk";
            this.buttnfinbk.Size = new System.Drawing.Size(60, 30);
            this.buttnfinbk.TabIndex = 246;
            this.buttnfinbk.Text = "Back";
            this.buttnfinbk.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.buttnfinbk.UseVisualStyleBackColor = false;
            this.buttnfinbk.Click += new System.EventHandler(this.buttnfinbk_Click);
            // 
            // Frmsalret
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1231, 562);
            this.Controls.Add(this.panadd);
            this.Controls.Add(this.btnsave);
            this.Controls.Add(this.buttnfinbk);
            this.Controls.Add(this.Genpan);
            this.Controls.Add(this.Editpan);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MinimizeBox = false;
            this.Name = "Frmsalret";
            this.Text = "Sales Retrurn";
            this.Load += new System.EventHandler(this.Frmsalret_Load);
            this.Genpan.ResumeLayout(false);
            this.Genpan.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.HFGP)).EndInit();
            this.panadd.ResumeLayout(false);
            this.panadd.PerformLayout();
            this.Editpan.ResumeLayout(false);
            this.Editpan.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.HFGST)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.HFIT)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel Genpan;
        private System.Windows.Forms.Label label60;
        private System.Windows.Forms.DateTimePicker dtpfnt;
        private System.Windows.Forms.TextBox txtscr9;
        private System.Windows.Forms.TextBox txtscr7;
        private System.Windows.Forms.TextBox txtscr6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtscr4;
        private System.Windows.Forms.TextBox Txtscr2;
        private System.Windows.Forms.TextBox txtscr1;
        private System.Windows.Forms.DataGridView HFGP;
        private System.Windows.Forms.TextBox txtscr5;
        private System.Windows.Forms.TextBox txtscr8;
        private System.Windows.Forms.TextBox Txtscr3;
        private System.Windows.Forms.DateTimePicker dateTimePicker1;
        private System.Windows.Forms.Panel panadd;
        private System.Windows.Forms.TextBox txtchargessum;
        private System.Windows.Forms.TextBox txtbasicval;
        private System.Windows.Forms.TextBox txttax;
        private System.Windows.Forms.TextBox txttotamt;
        private System.Windows.Forms.Label label58;
        private System.Windows.Forms.Button button13;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button buttnext1;
        private System.Windows.Forms.CheckBox chkact;
        private System.Windows.Forms.Button butcan;
        private System.Windows.Forms.Button butedit;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.Panel Editpan;
        private System.Windows.Forms.DateTimePicker dcdate;
        private System.Windows.Forms.TextBox txtprice;
        private System.Windows.Forms.TextBox txtgen1;
        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.Label label57;
        private System.Windows.Forms.Label label59;
        private System.Windows.Forms.TextBox txtexcise;
        private System.Windows.Forms.Label label54;
        private System.Windows.Forms.TextBox txtttot;
        private System.Windows.Forms.TextBox txtigval;
        private System.Windows.Forms.TextBox txtigstp;
        private System.Windows.Forms.Label label52;
        private System.Windows.Forms.Label label53;
        private System.Windows.Forms.TextBox txttsgval;
        private System.Windows.Forms.TextBox txtsgstp;
        private System.Windows.Forms.Label label50;
        private System.Windows.Forms.Label label51;
        private System.Windows.Forms.TextBox txttcgval;
        private System.Windows.Forms.TextBox txttcgstp;
        private System.Windows.Forms.Label label49;
        private System.Windows.Forms.Label label48;
        private System.Windows.Forms.Label label47;
        private System.Windows.Forms.TextBox txttprdval;
        private System.Windows.Forms.Label label46;
        private System.Windows.Forms.TextBox txttdisc;
        private System.Windows.Forms.TextBox txttbval;
        private System.Windows.Forms.Label label45;
        private System.Windows.Forms.Label label43;
        private System.Windows.Forms.TextBox txttdis;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox TxtNetAmt;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.TextBox TxtRoff;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txtcharges;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.RichTextBox txtpadd1;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.TextBox txttrans;
        private System.Windows.Forms.TextBox Txttot;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtot;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.TextBox txtamt;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.DateTimePicker DTPDOCDT;
        private System.Windows.Forms.DataGridView HFIT;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtdcno;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtname;
        private System.Windows.Forms.Label Phone;
        private System.Windows.Forms.TextBox txtgrn;
        private System.Windows.Forms.RichTextBox txtrem;
        private System.Windows.Forms.DateTimePicker Dtprem;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.TextBox txttempadd2;
        private System.Windows.Forms.TextBox txttempadd1;
        private System.Windows.Forms.TextBox txtitemname;
        private System.Windows.Forms.TextBox txtnotes;
        private System.Windows.Forms.TextBox txtbval;
        private System.Windows.Forms.TextBox txtqty;
        private System.Windows.Forms.DateTimePicker dtpdc;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label55;
        private System.Windows.Forms.TextBox txtpuid;
        private System.Windows.Forms.TextBox txtdcid;
        private System.Windows.Forms.TextBox txtitemcode;
        private System.Windows.Forms.TextBox txtlisid;
        private System.Windows.Forms.TextBox txttitemid;
        private System.Windows.Forms.Button btnsave;
        private System.Windows.Forms.Button buttnfinbk;
        private System.Windows.Forms.TextBox txtuom;
        private System.Windows.Forms.TextBox txtgrnid;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txttgstp;
        private System.Windows.Forms.TextBox txtpluid;
        private System.Windows.Forms.TextBox txttgstval;
        private System.Windows.Forms.DataGridView HFGST;
        private System.Windows.Forms.TextBox textBox3;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.DateTimePicker Dtpdt;
        private System.Windows.Forms.DateTimePicker Dtppre;
        private System.Windows.Forms.TextBox txtdcdate;
    }
}