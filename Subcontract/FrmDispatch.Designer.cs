﻿namespace ACV
{
    partial class FrmDispatch
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.grBack = new System.Windows.Forms.GroupBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.DataGridRollNo = new System.Windows.Forms.DataGridView();
            this.DataGridSortNo = new System.Windows.Forms.DataGridView();
            this.txtVechileNo = new System.Windows.Forms.TextBox();
            this.txtPackingSlipNo = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.grFront = new System.Windows.Forms.GroupBox();
            this.txtSearch = new System.Windows.Forms.TextBox();
            this.dtpDate = new System.Windows.Forms.DateTimePicker();
            this.DataGridDispatch = new System.Windows.Forms.DataGridView();
            this.btnView = new System.Windows.Forms.Button();
            this.panadd = new System.Windows.Forms.Panel();
            this.grBack.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridRollNo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridSortNo)).BeginInit();
            this.grFront.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridDispatch)).BeginInit();
            this.panadd.SuspendLayout();
            this.SuspendLayout();
            // 
            // grBack
            // 
            this.grBack.Controls.Add(this.label4);
            this.grBack.Controls.Add(this.label3);
            this.grBack.Controls.Add(this.DataGridRollNo);
            this.grBack.Controls.Add(this.DataGridSortNo);
            this.grBack.Controls.Add(this.txtVechileNo);
            this.grBack.Controls.Add(this.txtPackingSlipNo);
            this.grBack.Controls.Add(this.label2);
            this.grBack.Controls.Add(this.label1);
            this.grBack.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grBack.Location = new System.Drawing.Point(8, 2);
            this.grBack.Name = "grBack";
            this.grBack.Size = new System.Drawing.Size(780, 412);
            this.grBack.TabIndex = 0;
            this.grBack.TabStop = false;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(424, 58);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(53, 18);
            this.label4.TabIndex = 7;
            this.label4.Text = "Roll No";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(49, 60);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(51, 18);
            this.label3.TabIndex = 6;
            this.label3.Text = "SortNo";
            // 
            // DataGridRollNo
            // 
            this.DataGridRollNo.AllowUserToAddRows = false;
            this.DataGridRollNo.BackgroundColor = System.Drawing.Color.White;
            this.DataGridRollNo.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DataGridRollNo.Location = new System.Drawing.Point(424, 81);
            this.DataGridRollNo.Name = "DataGridRollNo";
            this.DataGridRollNo.ReadOnly = true;
            this.DataGridRollNo.RowHeadersVisible = false;
            this.DataGridRollNo.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.DataGridRollNo.Size = new System.Drawing.Size(319, 311);
            this.DataGridRollNo.TabIndex = 5;
            // 
            // DataGridSortNo
            // 
            this.DataGridSortNo.AllowUserToAddRows = false;
            this.DataGridSortNo.BackgroundColor = System.Drawing.Color.White;
            this.DataGridSortNo.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DataGridSortNo.Location = new System.Drawing.Point(49, 81);
            this.DataGridSortNo.Name = "DataGridSortNo";
            this.DataGridSortNo.ReadOnly = true;
            this.DataGridSortNo.RowHeadersVisible = false;
            this.DataGridSortNo.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.DataGridSortNo.Size = new System.Drawing.Size(347, 311);
            this.DataGridSortNo.TabIndex = 4;
            // 
            // txtVechileNo
            // 
            this.txtVechileNo.Location = new System.Drawing.Point(427, 22);
            this.txtVechileNo.Name = "txtVechileNo";
            this.txtVechileNo.Size = new System.Drawing.Size(158, 26);
            this.txtVechileNo.TabIndex = 3;
            // 
            // txtPackingSlipNo
            // 
            this.txtPackingSlipNo.Location = new System.Drawing.Point(173, 22);
            this.txtPackingSlipNo.Name = "txtPackingSlipNo";
            this.txtPackingSlipNo.Size = new System.Drawing.Size(162, 26);
            this.txtPackingSlipNo.TabIndex = 2;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(346, 26);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(75, 18);
            this.label2.TabIndex = 1;
            this.label2.Text = "Vechile No";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(65, 26);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(102, 18);
            this.label1.TabIndex = 0;
            this.label1.Text = "Packing Slip No";
            // 
            // grFront
            // 
            this.grFront.Controls.Add(this.txtSearch);
            this.grFront.Controls.Add(this.dtpDate);
            this.grFront.Controls.Add(this.DataGridDispatch);
            this.grFront.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grFront.Location = new System.Drawing.Point(9, 1);
            this.grFront.Name = "grFront";
            this.grFront.Size = new System.Drawing.Size(780, 412);
            this.grFront.TabIndex = 1;
            this.grFront.TabStop = false;
            // 
            // txtSearch
            // 
            this.txtSearch.Location = new System.Drawing.Point(6, 15);
            this.txtSearch.Name = "txtSearch";
            this.txtSearch.Size = new System.Drawing.Size(648, 26);
            this.txtSearch.TabIndex = 2;
            // 
            // dtpDate
            // 
            this.dtpDate.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpDate.Location = new System.Drawing.Point(655, 14);
            this.dtpDate.Name = "dtpDate";
            this.dtpDate.Size = new System.Drawing.Size(118, 26);
            this.dtpDate.TabIndex = 1;
            // 
            // DataGridDispatch
            // 
            this.DataGridDispatch.AllowUserToAddRows = false;
            this.DataGridDispatch.BackgroundColor = System.Drawing.Color.White;
            this.DataGridDispatch.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DataGridDispatch.Location = new System.Drawing.Point(6, 42);
            this.DataGridDispatch.Name = "DataGridDispatch";
            this.DataGridDispatch.ReadOnly = true;
            this.DataGridDispatch.RowHeadersVisible = false;
            this.DataGridDispatch.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.DataGridDispatch.Size = new System.Drawing.Size(767, 365);
            this.DataGridDispatch.TabIndex = 0;
            // 
            // btnView
            // 
            this.btnView.BackColor = System.Drawing.Color.White;
            this.btnView.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnView.Image = global::ACV.Properties.Resources.ok;
            this.btnView.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnView.Location = new System.Drawing.Point(710, 4);
            this.btnView.Name = "btnView";
            this.btnView.Size = new System.Drawing.Size(67, 28);
            this.btnView.TabIndex = 227;
            this.btnView.Text = "View";
            this.btnView.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnView.UseVisualStyleBackColor = false;
            // 
            // panadd
            // 
            this.panadd.BackColor = System.Drawing.Color.White;
            this.panadd.Controls.Add(this.btnView);
            this.panadd.Location = new System.Drawing.Point(8, 416);
            this.panadd.Name = "panadd";
            this.panadd.Size = new System.Drawing.Size(783, 36);
            this.panadd.TabIndex = 225;
            // 
            // FrmDispatch
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.ClientSize = new System.Drawing.Size(800, 452);
            this.Controls.Add(this.panadd);
            this.Controls.Add(this.grFront);
            this.Controls.Add(this.grBack);
            this.Name = "FrmDispatch";
            this.Text = "Dispatch";
            this.Load += new System.EventHandler(this.FrmDispatch_Load);
            this.grBack.ResumeLayout(false);
            this.grBack.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridRollNo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridSortNo)).EndInit();
            this.grFront.ResumeLayout(false);
            this.grFront.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridDispatch)).EndInit();
            this.panadd.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox grBack;
        private System.Windows.Forms.GroupBox grFront;
        private System.Windows.Forms.TextBox txtSearch;
        private System.Windows.Forms.DateTimePicker dtpDate;
        private System.Windows.Forms.DataGridView DataGridDispatch;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.DataGridView DataGridRollNo;
        private System.Windows.Forms.DataGridView DataGridSortNo;
        private System.Windows.Forms.TextBox txtVechileNo;
        private System.Windows.Forms.TextBox txtPackingSlipNo;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnView;
        private System.Windows.Forms.Panel panadd;
    }
}