﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Drawing.Imaging;
using System.Configuration;
using System.Data.SqlClient;

namespace ACV
{
    public partial class Frmopstock : Form
    {
        public Frmopstock()
        {
            
            this.BackColor = Color.White; ;
            InitializeComponent();
        }
                   //int uid = 0;
        int mode = 0;
        //string tpuid = "";
        SqlConnection conn = new SqlConnection("Data Source=" + Genclass.data2 + "; Initial Catalog=" + Genclass.data5 + ";User id=" + Genclass.data3 + ";Password=" + Genclass.data4 + "");

        //SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["connstr"].ConnectionString);
        SqlCommand qur = new SqlCommand();

        private void Frmopstock_Load(object sender, EventArgs e)
        {
            qur.Connection = conn;
 
            Left = (MdiParent.ClientRectangle.Width - Width) / 3;
            Top = (MdiParent.ClientRectangle.Height - Height) / 3;

            this.HFGP.DefaultCellStyle.Font = new Font("Arial", 10);
            this.HFGP.ColumnHeadersDefaultCellStyle.Font = new Font("Arial", 10, FontStyle.Bold);
            HFGP.RowHeadersVisible = false;
            HFGP.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            HFGP.EnableHeadersVisualStyles = false;
            HFGP.ColumnHeadersDefaultCellStyle.BackColor = SystemColors.GradientActiveCaption;
            Genclass.Module.ClearTextBox(this, Genpan);
            txtname.Visible = true;
            txtitem.Visible = true;
            txtitcode.Visible = true;
            txtop.Visible = true;
            txtscr1.Visible = false;
            txtscr2.Visible = false;
            txtscr3.Visible = false;
            txtscr4.Visible = false;
            Loadgrid();
            txtname.Focus();
        }


        private void loadput()
        {

            conn.Open();

            Genclass.fieldone = "";
            Genclass.fieldtwo = "";
            Genclass.fieldthree = "";
            Genclass.fieldFour = "";
            Genclass.fieldFive = "";

            if (Genclass.type == 1)
            {
                Genclass.Module.Partylistviewcont("uid", "Name", Genclass.strsql, this, txtpuid, txtname, Genpan);
                Genclass.strsql = "select uid,Name as Party from Partym where active=1 and companyid=" + Genclass.data1 + "";
                Genclass.FSSQLSortStr = "Name";

            }
            else if (Genclass.type == 2)
            {
                Genclass.Module.Partylistviewcont3("uid", "Item", "Itemcode", Genclass.strsql, this, txtitid, txtitem, txtitcode, Genpan);
                Genclass.strsql = "select uid,Itemname,Itemcode from Itemm where active=1 and companyid=" + Genclass.data1 + " and partyuid=" + txtpuid.Text + "";
                Genclass.FSSQLSortStr = "Itemname";

            }
            Genclass.cmd = new SqlCommand(Genclass.strsql, conn);
            SqlDataAdapter aptr = new SqlDataAdapter(Genclass.cmd);
            DataTable tap = new DataTable();
            aptr.Fill(tap);
            //Frmlookup contc = new Frmlookup() { Width = 400, Height = 300 };
            Frmlookup contc = new Frmlookup();
            DataGridView dt = (DataGridView)contc.Controls["HFGP"];
            dt.Refresh();
            dt.ColumnCount = tap.Columns.Count;
            dt.Columns[0].Visible = false;
            dt.Columns[1].Width = 400;


            dt.DefaultCellStyle.Font = new Font("Arial", 10);

            dt.ColumnHeadersDefaultCellStyle.Font = new Font("Arial", 10, FontStyle.Bold);
            dt.AutoGenerateColumns = false;

            Genclass.i = 0;
            foreach (DataColumn column in tap.Columns)
            {
                dt.Columns[Genclass.i].Name = column.ColumnName;
                dt.Columns[Genclass.i].HeaderText = column.ColumnName;
                dt.Columns[Genclass.i].DataPropertyName = column.ColumnName;
                Genclass.i = Genclass.i + 1;
            }

            dt.DataSource = tap;

            contc.Show();


            conn.Close();
        }


        private void Loadgrid()
        {
            try
            {
                conn.Open();

                if (txtscr1.Visible == true)
                {
                    Genclass.StrSrch = "";


                    Genclass.FSSQLSortStr = "name";
                    Genclass.FSSQLSortStr1 = "itemname";
                    Genclass.FSSQLSortStr2 = "itemcode";
                    Genclass.FSSQLSortStr3 = "adduty";



                    if (txtscr1.Text != "")
                    {
                        if (Genclass.StrSrch == "")
                        {
                            Genclass.StrSrch = Genclass.FSSQLSortStr + " like '%" + txtscr1.Text + "%'";
                        }
                        else
                        {
                            Genclass.StrSrch = Genclass.StrSrch + " and " + Genclass.FSSQLSortStr + " like '%" + txtscr1.Text + "%'";
                        }

                    }

                    if (txtscr2.Text != "")
                    {
                        if (Genclass.StrSrch == "")
                        {
                            Genclass.StrSrch = Genclass.FSSQLSortStr1 + " like '%" + txtscr2.Text + "%'";
                        }
                        else
                        {
                            Genclass.StrSrch = Genclass.StrSrch + " and " + Genclass.FSSQLSortStr1 + " like '%" + txtscr2.Text + "%'";
                        }

                    }

                    if (txtscr3.Text != "")
                    {
                        if (Genclass.StrSrch == "")
                        {
                            Genclass.StrSrch = Genclass.FSSQLSortStr2 + " like '%" + txtscr3.Text + "%'";
                        }
                        else
                        {
                            Genclass.StrSrch = Genclass.StrSrch + " and " + Genclass.FSSQLSortStr2 + " like '%" + txtscr3.Text + "%'";
                        }

                    }

                    if (txtscr4.Text != "")
                    {
                        if (Genclass.StrSrch == "")
                        {
                            Genclass.StrSrch = Genclass.FSSQLSortStr3 + " like '%" + txtscr4.Text + "%'";
                        }
                        else
                        {
                            Genclass.StrSrch = Genclass.StrSrch + " and " + Genclass.FSSQLSortStr3 + " like '%" + txtscr4.Text + "%'";
                        }

                    }



                    if (txtscr1.Text != "")
                    {
                        Genclass.StrSrch = " " + Genclass.StrSrch;
                    }
                    else if (txtscr2.Text != "")
                    {
                        Genclass.StrSrch = " " + Genclass.StrSrch;
                    }

                    else if (txtscr3.Text != "")
                    {
                        Genclass.StrSrch = " " + Genclass.StrSrch;
                    }
                    else if (txtscr4.Text != "")
                    {
                        Genclass.StrSrch = " " + Genclass.StrSrch;
                    }
                    else
                    {
                        Genclass.StrSrch = "a.uid <> 0";
                    }



                    string quy = " select b.uid,b.name as Party,c.itemname as Item,c.itemcode as Itemcode,a.adduty as Qty,c.uid from transactionsp a inner join partym b on a.partyuid=b.uid inner join itemm c on a.adddutyuid=c.uid  where a.doctypeid=0 and a.companyid=" + Genclass.data1 + " and  " + Genclass.StrSrch + "";
                    Genclass.cmd = new SqlCommand(quy, conn);
                }
                else
                {
                    string quy = " select b.uid,b.name as Party,c.itemname as Item,c.itemcode as Itemcode,a.adduty as Qty,c.uid from transactionsp a inner join partym b on a.partyuid=b.uid inner join itemm c on a.adddutyuid=c.uid  where a.doctypeid=0 and a.companyid=" + Genclass.data1 + "";
                    Genclass.cmd = new SqlCommand(quy, conn);

                }
                  
           

                SqlDataAdapter aptr = new SqlDataAdapter(Genclass.cmd);
                DataTable tap = new DataTable();
                aptr.Fill(tap);



                HFGP.AutoGenerateColumns = true;
                HFGP.Refresh();
                HFGP.DataSource = null;
                HFGP.Rows.Clear();


                HFGP.ColumnCount = tap.Columns.Count;
                Genclass.i = 0;
                foreach (DataColumn column in tap.Columns)
                {
                    HFGP.Columns[Genclass.i].Name = column.ColumnName;
                    HFGP.Columns[Genclass.i].HeaderText = column.ColumnName;
                    HFGP.Columns[Genclass.i].DataPropertyName = column.ColumnName;
                    Genclass.i = Genclass.i + 1;
                }




                HFGP.Columns[0].Visible = false;
         
                HFGP.Columns[1].Width = 380;
                HFGP.Columns[2].Width = 380;
                HFGP.Columns[3].Width = 200;
                HFGP.Columns[4].Width = 90;
                HFGP.Columns[5].Visible = false;

        


                HFGP.DataSource = tap;

                int index = HFGP.Rows.Count - 1;

                if (HFGP.Rows[0].Cells[1].Value.ToString() == "" || HFGP.Rows[0].Cells[1].Value == null)
                {
                    lblno1.Text = "0";
                }
                else
                {
                    lblno1.Text = "1";
                }


                lblno2.Text = "of " + index.ToString();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return;
            }
            finally
            {
                conn.Close();
                Genclass.cmd.Dispose();
            }
        }

        private void txtname_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {

                Genclass.type = 1;
                loadput();
            }
            else if (e.KeyCode == Keys.Escape)
            {
                txtname.Text = "";
                txtname.Focus();
            }
           
        }

        private void txtitem_KeyDown(object sender, KeyEventArgs e)

        {
            if (txtpuid.Text == "")
            {
                MessageBox.Show("Select the Party");
                return;
            }
            if (e.KeyCode == Keys.Enter)
            {

                Genclass.type = 2;
                loadput();
            }
            else if (e.KeyCode == Keys.Escape)
            {
                txtitem.Text = "";
                txtitem.Focus();
            }
           
        }

        private void buttrqok_Click(object sender, EventArgs e)
        {
            conn.Open();
            if (txtname.Text == "")
            {
                MessageBox.Show("Enter the Party");
                txtname.Focus();
            }

            if (txtitem.Text == "")
            {
                MessageBox.Show("Enter the Item");
                txtitem.Focus();
            }


            Genclass.strsql = "select uid from transactionsp where PartyUid='" + txtpuid.Text + "' and adddutyuid=" + txtitid.Text + " and doctypeid=0";
            Genclass.cmd = new SqlCommand(Genclass.strsql, conn);
            SqlDataAdapter aptr3 = new SqlDataAdapter(Genclass.cmd);
            DataTable tap3 = new DataTable();
            aptr3.Fill(tap3);
            
            if (tap3.Rows.Count==0)
            {
                mode=1;
            }
            else
            {
                 mode=2;
            }
            if (mode == 1)
            {
                Genclass.Module.Gendocno();
                qur.CommandText = "Insert into transactionsp values  (0,'" + Genclass.ST + "','" + DateTime.Now + "','',''," + txtpuid.Text + ",'',0,0,0,0," + txtitid.Text + "," + txtop.Text + ",''," + Genclass.data1 + "," + Genclass.Yearid + ")";
                qur.ExecuteNonQuery();
                qur.CommandText = "Insert into Mfg_stock_ledger values ('" + Genclass.ST + "','" + DateTime.Now + "',0,'Receipt'," + txtitid.Text + "," + txtop.Text + "," + Genclass.Yearid + ",'" + DateTime.Now + "',0,'OPSTOCK'," + txtpuid.Text + "," + Genclass.data1 + ",1,0)";
                qur.ExecuteNonQuery();
                MessageBox.Show("Opening Stock Updated");
            }
            else
            {
                string message = "Opening Stock already Updated for this Item,Do you want to change?";
                string caption = "Dilama";
                MessageBoxButtons buttons = MessageBoxButtons.YesNo;
                DialogResult result;

                result = MessageBox.Show(message, caption, buttons);

                if (result == System.Windows.Forms.DialogResult.Yes)
                {
                    qur.CommandText = "update TransactionsP set adduty=" + txtop.Text + ",dcdate='" + DateTime.Now + "' where PartyUid=" + txtpuid.Text + " and adddutyuid=" + txtitid.Text + " and doctypeid=0";
                    qur.ExecuteNonQuery();
                    qur.CommandText = "update Mfg_stock_ledger set qty=" + txtop.Text + ",logdate='" + DateTime.Now + "' where PartyUid=" + txtpuid.Text + " and itemid=" + txtitid.Text + " and doctypeid=0";
                    qur.ExecuteNonQuery();
                    MessageBox.Show("Opening Stock Updated");
                }

          
            }
  

            conn.Close();
           
            Genclass.Module.ClearTextBox(this, Genpan);
            Loadgrid();
            
            txtname.Focus();
        }

        private void btnexit_Click(object sender, EventArgs e)
        {
            this.Dispose();
        }

        private void txtname_TextChanged(object sender, EventArgs e)
        {
            txtitem.Focus();
        }

        private void txtitem_TextChanged(object sender, EventArgs e)
        {
            txtop.Focus();
        }

        private void txtop_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {

                Genclass.type = 2;
                buttrqok_Click(sender,e);
            }
            else if (e.KeyCode == Keys.Escape)
            {
                txtop.Text = "";
                txtop.Focus();
            }
        }

      

        private void txtscr1_TextChanged(object sender, EventArgs e)
        {
            Loadgrid();
        }

        private void txtscr2_TextChanged(object sender, EventArgs e)
        {
            Loadgrid();
        }

        private void txtscr3_TextChanged(object sender, EventArgs e)
        {
            Loadgrid();
        }

        private void txtscr4_TextChanged(object sender, EventArgs e)
        {
            Loadgrid();
        }

        private void button1_Click_1(object sender, EventArgs e)
        {

            Genclass.Module.ClearTextBox(this, Genpan);

            if (button1.Text == "Search")
            {
                button1.Text = "Refresh";
                txtname.Visible = false;
                txtitem.Visible = false;
                txtitcode.Visible = false;
                txtop.Visible = false;
                txtscr1.Visible = true;
                txtscr2.Visible = true;
                txtscr3.Visible = true;
                txtscr4.Visible = true;
            }
            else
            {
                button1.Text = "Search";
                txtname.Visible = true;
                txtitem.Visible = true;
                txtitcode.Visible = true;
                txtop.Visible = true;
                txtscr1.Visible = false;
                txtscr2.Visible = false;
                txtscr3.Visible = false;
                txtscr4.Visible = false;
            }
        }

        private void buttnext1_Click(object sender, EventArgs e)
        {
            this.Dispose();
        }

        private void buttnnvfst_Click(object sender, EventArgs e)
        {
            int index = HFGP.SelectedRows[0].Index;
            int index1 = 0;
            int index2 = HFGP.Rows.Count - 1;

            HFGP.Rows[index].Selected = false;
            HFGP.Rows[index1].Selected = true;
            index1 = index1 + 1;
            lblno1.Text = index1.ToString();
            lblno2.Text = "of " + index2.ToString();
        }

        private void buttnnxtlft_Click(object sender, EventArgs e)
        {
            int index = HFGP.SelectedRows[0].Index;
            int index1 = HFGP.Rows.Count - 1; ;
            if (index > 0)
            {
                HFGP.Rows[index].Selected = false;
                HFGP.Rows[index - 1].Selected = true;
                lblno1.Text = index.ToString();
                lblno2.Text = "of " + index1.ToString();
            }
        }

        private void buttrnxt_Click(object sender, EventArgs e)
        {
            int index = HFGP.SelectedRows[0].Index;
            int index2 = HFGP.SelectedRows[0].Index + 2;
            int index1 = HFGP.Rows.Count - 1; ;
            if (index < HFGP.Rows.Count - 2)
            {
                HFGP.Rows[index].Selected = false;
                HFGP.Rows[index + 1].Selected = true;
                lblno1.Text = index2.ToString();
                lblno2.Text = "of " + index1.ToString();

            }

        }

        private void btnfinnxt_Click(object sender, EventArgs e)
        {
            int index = HFGP.SelectedRows[0].Index;
            int index1 = HFGP.Rows.Count - 1;


            HFGP.Rows[index].Selected = false;

            HFGP.Rows[index1 - 1].Selected = true;
            lblno1.Text = index1.ToString();
            lblno2.Text = "of " + index1.ToString();
        }

        private void HFGP_KeyUp(object sender, KeyEventArgs e)
        {

            if (HFGP.Rows[0].Cells[0].Value.ToString() != "")
            {
                int index = HFGP.SelectedRows[0].Index;
                if (index < HFGP.Rows.Count - 1)
                {
                    index = index + 1;
                    int index1 = HFGP.Rows.Count - 1;
                    lblno1.Text = index.ToString();
                    lblno2.Text = "of " + index1.ToString();
                }
            }
        }

        private void HFGP_CellMouseUp(object sender, DataGridViewCellMouseEventArgs e)
        {

            if (HFGP.Rows[0].Cells[0].Value.ToString() != "")
            {
                int index = HFGP.SelectedRows[0].Index;
                if (index < HFGP.Rows.Count - 1)
                {
                    index = index + 1;
                    int index1 = HFGP.Rows.Count - 1;
                    lblno1.Text = index.ToString();
                    lblno2.Text = "of " + index1.ToString();
                }
            }
        }

      
    }
}
