﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace ACV
{
    public partial class FrmRollReport : Form
    {
        public FrmRollReport()
        {
            InitializeComponent();
        }
        SQLDBHelper db = new SQLDBHelper();
        SqlConnection conn = new SqlConnection("Data Source=" + Genclass.data2 + "; Initial Catalog=" + Genclass.data5 + ";User id=" + Genclass.data3 + ";Password=" + Genclass.data4 + "");
        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                string Query = "Select * from RollM Where ShiftId = 985";
                DataTable dt = db.GetDataWithoutParam(CommandType.Text, Query, conn);
                if (dt.Rows.Count > 0)
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        int ShiftId = 0;
                        DateTime dte = Convert.ToDateTime((DateTime)dt.Rows[i]["rolldate"]);

                        string timeString24Hour = dte.ToString("HH:mm", CultureInfo.CurrentCulture);
                        TimeSpan tme = dte.TimeOfDay;

                        string Time = tme.ToString("HH:mm");
                        string[] t2 = Time.Split(':');
                        int t1 = Convert.ToInt32(t2[0]);
                        if (t1 >= 8 && t1 < 16)
                        {
                            ShiftId = 984;
                        }
                        else if (t1 >= 16 && t1 <= 24)
                        {
                            ShiftId = 985;
                        }
                        else if (t1 >= 0 && t1 < 8)
                        {
                            ShiftId = 986;
                        }
                        int ID = Convert.ToInt32(dt.Rows[i]["ID"].ToString());
                        string Qury2 = "Update RollM Set ShiftId =" + ShiftId + " Where Id =" + ID + " ";
                        db.ExecuteNonQuery(CommandType.Text, Qury2, conn);
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void LoadShift()
        {
            try
            {
                string Query = "Select Uid,GeneralName as ShiftName from GeneralM Where TypeM_Uid = 27";
                SqlDataAdapter da = new SqlDataAdapter(Query, conn);
                DataTable dt = new DataTable();
                da.Fill(dt);
                cmbShift.DataSource = null;
                cmbShift.DisplayMember = "ShiftName";
                cmbShift.ValueMember = "Uid";
                cmbShift.DataSource = dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void FrmRollReport_Load(object sender, EventArgs e)
        {
            LoadShift();
        }

        private void btnPrint_Click(object sender, EventArgs e)
        {
            try
            {
                Genclass.Dtype = 9;
                Genclass.ReportType = "RollReort";
                Genclass.ReortDate = Convert.ToDateTime(dtpQCDate.Text);
                Genclass.parameter = cmbShift.SelectedValue.ToString();
                FrmReprtViwer rpt = new FrmReprtViwer();
                rpt.MdiParent = this.MdiParent;
                rpt.WindowState = FormWindowState.Maximized;
                rpt.Show();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }
}
