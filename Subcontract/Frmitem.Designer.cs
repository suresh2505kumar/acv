﻿namespace ACV
{
    partial class Frmitem
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Frmitem));
            this.Genpan = new System.Windows.Forms.Panel();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.Txtscr5 = new System.Windows.Forms.TextBox();
            this.Txtscr4 = new System.Windows.Forms.TextBox();
            this.Txtscr3 = new System.Windows.Forms.TextBox();
            this.Txtscr2 = new System.Windows.Forms.TextBox();
            this.txtscr1 = new System.Windows.Forms.TextBox();
            this.HFGP = new System.Windows.Forms.DataGridView();
            this.Editpnl = new System.Windows.Forms.Panel();
            this.txttax = new System.Windows.Forms.ComboBox();
            this.label32 = new System.Windows.Forms.Label();
            this.txthsncode = new System.Windows.Forms.TextBox();
            this.txtuom = new System.Windows.Forms.ComboBox();
            this.label31 = new System.Windows.Forms.Label();
            this.label30 = new System.Windows.Forms.Label();
            this.txtcat = new System.Windows.Forms.ComboBox();
            this.Txtitgrp = new System.Windows.Forms.TextBox();
            this.txtitname = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.Txtitcode = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.txtspec = new System.Windows.Forms.RichTextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.txttax1 = new System.Windows.Forms.TextBox();
            this.txtuom1 = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.txtuomid = new System.Windows.Forms.TextBox();
            this.txtcatid = new System.Windows.Forms.TextBox();
            this.txtitgrpid = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.txtparty = new System.Windows.Forms.TextBox();
            this.txtpartyid = new System.Windows.Forms.TextBox();
            this.txtgrp = new System.Windows.Forms.TextBox();
            this.label24 = new System.Windows.Forms.Label();
            this.txtgrpid = new System.Windows.Forms.TextBox();
            this.txthsn = new System.Windows.Forms.TextBox();
            this.txthsnid = new System.Windows.Forms.TextBox();
            this.label28 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.txtcusitname = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtcusitcode = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.panadd = new System.Windows.Forms.Panel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.lblno1 = new System.Windows.Forms.Label();
            this.lblno2 = new System.Windows.Forms.Label();
            this.flowLayoutPanel3 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel2 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.buttnnvfst = new System.Windows.Forms.Button();
            this.buttnnxtlft = new System.Windows.Forms.Button();
            this.btnfinnxt = new System.Windows.Forms.Button();
            this.buttrnxt = new System.Windows.Forms.Button();
            this.buttnext1 = new System.Windows.Forms.Button();
            this.chkact = new System.Windows.Forms.CheckBox();
            this.butexit = new System.Windows.Forms.Button();
            this.butedit = new System.Windows.Forms.Button();
            this.btnadd = new System.Windows.Forms.Button();
            this.Chkedtact = new System.Windows.Forms.CheckBox();
            this.bompro = new System.Windows.Forms.Panel();
            this.label29 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.txtptparty = new System.Windows.Forms.TextBox();
            this.label25 = new System.Windows.Forms.Label();
            this.Txtptitem = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.txthour = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.label12 = new System.Windows.Forms.Label();
            this.txtprocessqty = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.txtsettime = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.txtprocestime = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.txtrate = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.txtsqgno = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.txtproceesname = new System.Windows.Forms.TextBox();
            this.HFG4 = new System.Windows.Forms.DataGridView();
            this.txtprocessid = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.button2 = new System.Windows.Forms.Button();
            this.buttrqok = new System.Windows.Forms.Button();
            this.label19 = new System.Windows.Forms.Label();
            this.txtperqty = new System.Windows.Forms.TextBox();
            this.label20 = new System.Windows.Forms.Label();
            this.txtusedqty = new System.Windows.Forms.TextBox();
            this.label21 = new System.Windows.Forms.Label();
            this.txbomuom = new System.Windows.Forms.TextBox();
            this.label22 = new System.Windows.Forms.Label();
            this.txtbomcode = new System.Windows.Forms.TextBox();
            this.label23 = new System.Windows.Forms.Label();
            this.txtbomitem = new System.Windows.Forms.TextBox();
            this.hfg3 = new System.Windows.Forms.DataGridView();
            this.txtalterid = new System.Windows.Forms.TextBox();
            this.txtbomuomid = new System.Windows.Forms.TextBox();
            this.buttnbomext = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.btnaddrcan = new System.Windows.Forms.Button();
            this.butnsave = new System.Windows.Forms.Button();
            this.buttnext2 = new System.Windows.Forms.Button();
            this.Genpan.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.HFGP)).BeginInit();
            this.Editpnl.SuspendLayout();
            this.panadd.SuspendLayout();
            this.panel1.SuspendLayout();
            this.bompro.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.HFG4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.hfg3)).BeginInit();
            this.SuspendLayout();
            // 
            // Genpan
            // 
            this.Genpan.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.Genpan.Controls.Add(this.comboBox1);
            this.Genpan.Controls.Add(this.label1);
            this.Genpan.Controls.Add(this.Txtscr5);
            this.Genpan.Controls.Add(this.Txtscr4);
            this.Genpan.Controls.Add(this.Txtscr3);
            this.Genpan.Controls.Add(this.Txtscr2);
            this.Genpan.Controls.Add(this.txtscr1);
            this.Genpan.Controls.Add(this.HFGP);
            this.Genpan.Location = new System.Drawing.Point(0, 0);
            this.Genpan.Name = "Genpan";
            this.Genpan.Size = new System.Drawing.Size(1001, 467);
            this.Genpan.TabIndex = 94;
            this.Genpan.Paint += new System.Windows.Forms.PaintEventHandler(this.Genpan_Paint);
            // 
            // comboBox1
            // 
            this.comboBox1.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.comboBox1.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.comboBox1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox1.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Items.AddRange(new object[] {
            "Mapped Items",
            "Unmapped Items"});
            this.comboBox1.Location = new System.Drawing.Point(694, 10);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(276, 26);
            this.comboBox1.TabIndex = 145;
            this.comboBox1.SelectedIndexChanged += new System.EventHandler(this.comboBox1_SelectedIndexChanged);
            this.comboBox1.Click += new System.EventHandler(this.comboBox1_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Calibri", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(3, 14);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(105, 23);
            this.label1.TabIndex = 94;
            this.label1.Text = "Item Master";
            // 
            // Txtscr5
            // 
            this.Txtscr5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Txtscr5.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txtscr5.Location = new System.Drawing.Point(864, 42);
            this.Txtscr5.Name = "Txtscr5";
            this.Txtscr5.Size = new System.Drawing.Size(110, 26);
            this.Txtscr5.TabIndex = 90;
            this.Txtscr5.TextChanged += new System.EventHandler(this.Txtscr5_TextChanged);
            // 
            // Txtscr4
            // 
            this.Txtscr4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Txtscr4.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txtscr4.Location = new System.Drawing.Point(696, 42);
            this.Txtscr4.Name = "Txtscr4";
            this.Txtscr4.Size = new System.Drawing.Size(170, 26);
            this.Txtscr4.TabIndex = 89;
            this.Txtscr4.TextChanged += new System.EventHandler(this.Txtscr4_TextChanged);
            // 
            // Txtscr3
            // 
            this.Txtscr3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Txtscr3.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txtscr3.Location = new System.Drawing.Point(497, 42);
            this.Txtscr3.Name = "Txtscr3";
            this.Txtscr3.Size = new System.Drawing.Size(201, 26);
            this.Txtscr3.TabIndex = 88;
            this.Txtscr3.TextChanged += new System.EventHandler(this.Txtscr3_TextChanged);
            // 
            // Txtscr2
            // 
            this.Txtscr2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Txtscr2.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txtscr2.Location = new System.Drawing.Point(199, 42);
            this.Txtscr2.Name = "Txtscr2";
            this.Txtscr2.Size = new System.Drawing.Size(299, 26);
            this.Txtscr2.TabIndex = 87;
            this.Txtscr2.TextChanged += new System.EventHandler(this.Txtscr2_TextChanged);
            // 
            // txtscr1
            // 
            this.txtscr1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtscr1.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtscr1.Location = new System.Drawing.Point(0, 42);
            this.txtscr1.Name = "txtscr1";
            this.txtscr1.Size = new System.Drawing.Size(201, 26);
            this.txtscr1.TabIndex = 1;
            this.txtscr1.TextChanged += new System.EventHandler(this.txtscr1_TextChanged);
            // 
            // HFGP
            // 
            this.HFGP.BackgroundColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.HFGP.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.HFGP.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.HFGP.Location = new System.Drawing.Point(0, 70);
            this.HFGP.Name = "HFGP";
            this.HFGP.Size = new System.Drawing.Size(1001, 398);
            this.HFGP.TabIndex = 3;
            this.HFGP.CellMouseUp += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.HFGP_CellMouseUp);
            this.HFGP.KeyUp += new System.Windows.Forms.KeyEventHandler(this.HFGP_KeyUp);
            // 
            // Editpnl
            // 
            this.Editpnl.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.Editpnl.Controls.Add(this.txttax);
            this.Editpnl.Controls.Add(this.label32);
            this.Editpnl.Controls.Add(this.txthsncode);
            this.Editpnl.Controls.Add(this.txtuom);
            this.Editpnl.Controls.Add(this.label31);
            this.Editpnl.Controls.Add(this.label30);
            this.Editpnl.Controls.Add(this.txtcat);
            this.Editpnl.Controls.Add(this.Txtitgrp);
            this.Editpnl.Controls.Add(this.txtitname);
            this.Editpnl.Controls.Add(this.label3);
            this.Editpnl.Controls.Add(this.Txtitcode);
            this.Editpnl.Controls.Add(this.label2);
            this.Editpnl.Controls.Add(this.label6);
            this.Editpnl.Controls.Add(this.txtspec);
            this.Editpnl.Controls.Add(this.label8);
            this.Editpnl.Controls.Add(this.txttax1);
            this.Editpnl.Controls.Add(this.txtuom1);
            this.Editpnl.Controls.Add(this.label7);
            this.Editpnl.Controls.Add(this.txtuomid);
            this.Editpnl.Controls.Add(this.txtcatid);
            this.Editpnl.Controls.Add(this.txtitgrpid);
            this.Editpnl.Controls.Add(this.label9);
            this.Editpnl.Controls.Add(this.txtparty);
            this.Editpnl.Controls.Add(this.txtpartyid);
            this.Editpnl.Controls.Add(this.txtgrp);
            this.Editpnl.Controls.Add(this.label24);
            this.Editpnl.Controls.Add(this.txtgrpid);
            this.Editpnl.Controls.Add(this.txthsn);
            this.Editpnl.Controls.Add(this.txthsnid);
            this.Editpnl.Controls.Add(this.label28);
            this.Editpnl.Controls.Add(this.label27);
            this.Editpnl.Controls.Add(this.txtcusitname);
            this.Editpnl.Controls.Add(this.label4);
            this.Editpnl.Controls.Add(this.txtcusitcode);
            this.Editpnl.Controls.Add(this.label5);
            this.Editpnl.Location = new System.Drawing.Point(-3, -3);
            this.Editpnl.Name = "Editpnl";
            this.Editpnl.Size = new System.Drawing.Size(1004, 470);
            this.Editpnl.TabIndex = 113;
            this.Editpnl.Paint += new System.Windows.Forms.PaintEventHandler(this.Editpnl_Paint);
            // 
            // txttax
            // 
            this.txttax.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.txttax.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.txttax.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.txttax.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txttax.FormattingEnabled = true;
            this.txttax.Location = new System.Drawing.Point(346, 188);
            this.txttax.Name = "txttax";
            this.txttax.Size = new System.Drawing.Size(143, 26);
            this.txttax.TabIndex = 150;
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Font = new System.Drawing.Font("Calibri", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label32.Location = new System.Drawing.Point(211, 166);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(78, 21);
            this.label32.TabIndex = 149;
            this.label32.Text = "HSNCODE";
            // 
            // txthsncode
            // 
            this.txthsncode.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txthsncode.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txthsncode.Location = new System.Drawing.Point(213, 188);
            this.txthsncode.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.txthsncode.Name = "txthsncode";
            this.txthsncode.Size = new System.Drawing.Size(128, 26);
            this.txthsncode.TabIndex = 148;
            this.txthsncode.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txthsncode_KeyDown);
            // 
            // txtuom
            // 
            this.txtuom.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.txtuom.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.txtuom.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.txtuom.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtuom.FormattingEnabled = true;
            this.txtuom.Location = new System.Drawing.Point(17, 138);
            this.txtuom.Name = "txtuom";
            this.txtuom.Size = new System.Drawing.Size(189, 26);
            this.txtuom.TabIndex = 147;
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Font = new System.Drawing.Font("Calibri", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label31.Location = new System.Drawing.Point(213, 116);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(72, 21);
            this.label31.TabIndex = 146;
            this.label31.Text = "Category";
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Font = new System.Drawing.Font("Calibri", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label30.Location = new System.Drawing.Point(346, 166);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(32, 21);
            this.label30.TabIndex = 145;
            this.label30.Text = "Tax";
            // 
            // txtcat
            // 
            this.txtcat.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.txtcat.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.txtcat.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.txtcat.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtcat.FormattingEnabled = true;
            this.txtcat.Location = new System.Drawing.Point(213, 138);
            this.txtcat.Name = "txtcat";
            this.txtcat.Size = new System.Drawing.Size(276, 26);
            this.txtcat.TabIndex = 144;
            // 
            // Txtitgrp
            // 
            this.Txtitgrp.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Txtitgrp.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txtitgrp.Location = new System.Drawing.Point(498, 138);
            this.Txtitgrp.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.Txtitgrp.Name = "Txtitgrp";
            this.Txtitgrp.Size = new System.Drawing.Size(246, 26);
            this.Txtitgrp.TabIndex = 122;
            this.Txtitgrp.Click += new System.EventHandler(this.Txtitgrp_Click);
            this.Txtitgrp.TextChanged += new System.EventHandler(this.Txtitgrp_TextChanged);
            this.Txtitgrp.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Txtitgrp_KeyDown_1);
            // 
            // txtitname
            // 
            this.txtitname.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtitname.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtitname.Location = new System.Drawing.Point(213, 91);
            this.txtitname.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.txtitname.MaxLength = 100;
            this.txtitname.Name = "txtitname";
            this.txtitname.Size = new System.Drawing.Size(530, 26);
            this.txtitname.TabIndex = 114;
            this.txtitname.TextChanged += new System.EventHandler(this.txtitname_TextChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Calibri", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(213, 70);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(83, 21);
            this.label3.TabIndex = 114;
            this.label3.Text = "ItemName";
            // 
            // Txtitcode
            // 
            this.Txtitcode.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Txtitcode.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txtitcode.Location = new System.Drawing.Point(17, 91);
            this.Txtitcode.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.Txtitcode.MaxLength = 20;
            this.Txtitcode.Name = "Txtitcode";
            this.Txtitcode.Size = new System.Drawing.Size(187, 26);
            this.Txtitcode.TabIndex = 113;
            this.Txtitcode.TextChanged += new System.EventHandler(this.Txtitcode_TextChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Calibri", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(13, 67);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(75, 21);
            this.label2.TabIndex = 112;
            this.label2.Text = "Itemcode";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Calibri", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(15, 235);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(97, 21);
            this.label6.TabIndex = 120;
            this.label6.Text = "Specification";
            // 
            // txtspec
            // 
            this.txtspec.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtspec.Location = new System.Drawing.Point(17, 252);
            this.txtspec.Name = "txtspec";
            this.txtspec.Size = new System.Drawing.Size(956, 166);
            this.txtspec.TabIndex = 123;
            this.txtspec.Text = "";
            this.txtspec.TextChanged += new System.EventHandler(this.txtspec_TextChanged);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Calibri", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(493, 117);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(51, 21);
            this.label8.TabIndex = 128;
            this.label8.Text = "Brand";
            // 
            // txttax1
            // 
            this.txttax1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txttax1.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txttax1.Location = new System.Drawing.Point(314, 280);
            this.txttax1.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.txttax1.Name = "txttax1";
            this.txttax1.Size = new System.Drawing.Size(138, 22);
            this.txttax1.TabIndex = 120;
            this.txttax1.Click += new System.EventHandler(this.txtcat_Click);
            this.txttax1.TextChanged += new System.EventHandler(this.txtcat_TextChanged_1);
            this.txttax1.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtcat_KeyDown_1);
            // 
            // txtuom1
            // 
            this.txtuom1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtuom1.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtuom1.Location = new System.Drawing.Point(262, 280);
            this.txtuom1.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.txtuom1.Name = "txtuom1";
            this.txtuom1.Size = new System.Drawing.Size(191, 22);
            this.txtuom1.TabIndex = 119;
            this.txtuom1.Click += new System.EventHandler(this.txtuom_Click);
            this.txtuom1.TextChanged += new System.EventHandler(this.txtuom_TextChanged_1);
            this.txtuom1.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtuom_KeyDown_1);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Calibri", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(15, 119);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(45, 21);
            this.label7.TabIndex = 122;
            this.label7.Text = "UoM";
            // 
            // txtuomid
            // 
            this.txtuomid.Location = new System.Drawing.Point(101, 286);
            this.txtuomid.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.txtuomid.Name = "txtuomid";
            this.txtuomid.Size = new System.Drawing.Size(63, 21);
            this.txtuomid.TabIndex = 124;
            // 
            // txtcatid
            // 
            this.txtcatid.Location = new System.Drawing.Point(360, 286);
            this.txtcatid.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.txtcatid.Name = "txtcatid";
            this.txtcatid.Size = new System.Drawing.Size(47, 21);
            this.txtcatid.TabIndex = 127;
            // 
            // txtitgrpid
            // 
            this.txtitgrpid.Location = new System.Drawing.Point(701, 265);
            this.txtitgrpid.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.txtitgrpid.Name = "txtitgrpid";
            this.txtitgrpid.Size = new System.Drawing.Size(59, 21);
            this.txtitgrpid.TabIndex = 111;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(62, 297);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(78, 16);
            this.label9.TabIndex = 131;
            this.label9.Text = "PartyName";
            // 
            // txtparty
            // 
            this.txtparty.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtparty.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtparty.Location = new System.Drawing.Point(66, 317);
            this.txtparty.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.txtparty.MaxLength = 100;
            this.txtparty.Name = "txtparty";
            this.txtparty.Size = new System.Drawing.Size(605, 22);
            this.txtparty.TabIndex = 115;
            this.txtparty.Click += new System.EventHandler(this.txtparty_Click);
            this.txtparty.TextChanged += new System.EventHandler(this.txtparty_TextChanged_1);
            this.txtparty.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtparty_KeyDown_1);
            // 
            // txtpartyid
            // 
            this.txtpartyid.Location = new System.Drawing.Point(486, 327);
            this.txtpartyid.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.txtpartyid.Name = "txtpartyid";
            this.txtpartyid.Size = new System.Drawing.Size(28, 21);
            this.txtpartyid.TabIndex = 133;
            // 
            // txtgrp
            // 
            this.txtgrp.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtgrp.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtgrp.Location = new System.Drawing.Point(14, 188);
            this.txtgrp.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.txtgrp.Name = "txtgrp";
            this.txtgrp.Size = new System.Drawing.Size(191, 26);
            this.txtgrp.TabIndex = 121;
            this.txtgrp.Click += new System.EventHandler(this.txtgrp_Click);
            this.txtgrp.TextChanged += new System.EventHandler(this.txtgrp_TextChanged);
            this.txtgrp.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtgrp_KeyDown);
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("Calibri", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.Location = new System.Drawing.Point(10, 167);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(90, 21);
            this.label24.TabIndex = 137;
            this.label24.Text = "Item Group";
            // 
            // txtgrpid
            // 
            this.txtgrpid.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtgrpid.Location = new System.Drawing.Point(486, 286);
            this.txtgrpid.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.txtgrpid.Name = "txtgrpid";
            this.txtgrpid.Size = new System.Drawing.Size(28, 22);
            this.txtgrpid.TabIndex = 138;
            // 
            // txthsn
            // 
            this.txthsn.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txthsn.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txthsn.Location = new System.Drawing.Point(393, 303);
            this.txthsn.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.txthsn.Name = "txthsn";
            this.txthsn.Size = new System.Drawing.Size(191, 22);
            this.txthsn.TabIndex = 140;
            this.txthsn.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txthsn_KeyDown);
            // 
            // txthsnid
            // 
            this.txthsnid.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txthsnid.Location = new System.Drawing.Point(759, 286);
            this.txthsnid.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.txthsnid.Name = "txthsnid";
            this.txthsnid.Size = new System.Drawing.Size(31, 22);
            this.txthsnid.TabIndex = 142;
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Font = new System.Drawing.Font("Calibri", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label28.Location = new System.Drawing.Point(15, 22);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(105, 23);
            this.label28.TabIndex = 143;
            this.label28.Text = "Item Master";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label27.Location = new System.Drawing.Point(626, 286);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(68, 16);
            this.label27.TabIndex = 141;
            this.label27.Text = "Hsn Code";
            // 
            // txtcusitname
            // 
            this.txtcusitname.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtcusitname.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtcusitname.Location = new System.Drawing.Point(262, 348);
            this.txtcusitname.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.txtcusitname.MaxLength = 100;
            this.txtcusitname.Name = "txtcusitname";
            this.txtcusitname.Size = new System.Drawing.Size(407, 22);
            this.txtcusitname.TabIndex = 118;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(262, 327);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(137, 16);
            this.label4.TabIndex = 118;
            this.label4.Text = "Customer ItemName";
            // 
            // txtcusitcode
            // 
            this.txtcusitcode.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtcusitcode.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtcusitcode.Location = new System.Drawing.Point(66, 348);
            this.txtcusitcode.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.txtcusitcode.MaxLength = 20;
            this.txtcusitcode.Name = "txtcusitcode";
            this.txtcusitcode.Size = new System.Drawing.Size(187, 22);
            this.txtcusitcode.TabIndex = 117;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(64, 327);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(131, 16);
            this.label5.TabIndex = 116;
            this.label5.Text = "Customer Itemcode";
            // 
            // panadd
            // 
            this.panadd.BackColor = System.Drawing.Color.White;
            this.panadd.Controls.Add(this.panel1);
            this.panadd.Controls.Add(this.buttnnvfst);
            this.panadd.Controls.Add(this.buttnnxtlft);
            this.panadd.Controls.Add(this.btnfinnxt);
            this.panadd.Controls.Add(this.buttrnxt);
            this.panadd.Controls.Add(this.buttnext1);
            this.panadd.Controls.Add(this.chkact);
            this.panadd.Controls.Add(this.butexit);
            this.panadd.Controls.Add(this.butedit);
            this.panadd.Controls.Add(this.btnadd);
            this.panadd.Location = new System.Drawing.Point(0, 468);
            this.panadd.Name = "panadd";
            this.panadd.Size = new System.Drawing.Size(1001, 31);
            this.panadd.TabIndex = 208;
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel1.Controls.Add(this.lblno1);
            this.panel1.Controls.Add(this.lblno2);
            this.panel1.Controls.Add(this.flowLayoutPanel3);
            this.panel1.Controls.Add(this.flowLayoutPanel2);
            this.panel1.Controls.Add(this.flowLayoutPanel1);
            this.panel1.Location = new System.Drawing.Point(64, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(74, 31);
            this.panel1.TabIndex = 214;
            // 
            // lblno1
            // 
            this.lblno1.AutoSize = true;
            this.lblno1.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblno1.ForeColor = System.Drawing.Color.Black;
            this.lblno1.Location = new System.Drawing.Point(3, 5);
            this.lblno1.Name = "lblno1";
            this.lblno1.Size = new System.Drawing.Size(15, 18);
            this.lblno1.TabIndex = 163;
            this.lblno1.Text = "1";
            // 
            // lblno2
            // 
            this.lblno2.AutoSize = true;
            this.lblno2.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblno2.ForeColor = System.Drawing.Color.Black;
            this.lblno2.Location = new System.Drawing.Point(27, 5);
            this.lblno2.Name = "lblno2";
            this.lblno2.Size = new System.Drawing.Size(31, 18);
            this.lblno2.TabIndex = 162;
            this.lblno2.Text = "of 1";
            // 
            // flowLayoutPanel3
            // 
            this.flowLayoutPanel3.Location = new System.Drawing.Point(59, 30);
            this.flowLayoutPanel3.Name = "flowLayoutPanel3";
            this.flowLayoutPanel3.Size = new System.Drawing.Size(199, 100);
            this.flowLayoutPanel3.TabIndex = 2;
            // 
            // flowLayoutPanel2
            // 
            this.flowLayoutPanel2.Location = new System.Drawing.Point(38, 35);
            this.flowLayoutPanel2.Name = "flowLayoutPanel2";
            this.flowLayoutPanel2.Size = new System.Drawing.Size(199, 100);
            this.flowLayoutPanel2.TabIndex = 1;
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.Location = new System.Drawing.Point(62, -130);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(199, 100);
            this.flowLayoutPanel1.TabIndex = 0;
            // 
            // buttnnvfst
            // 
            this.buttnnvfst.BackColor = System.Drawing.Color.White;
            this.buttnnvfst.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.buttnnvfst.FlatAppearance.BorderSize = 0;
            this.buttnnvfst.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttnnvfst.Image = ((System.Drawing.Image)(resources.GetObject("buttnnvfst.Image")));
            this.buttnnvfst.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttnnvfst.Location = new System.Drawing.Point(6, 0);
            this.buttnnvfst.Name = "buttnnvfst";
            this.buttnnvfst.Size = new System.Drawing.Size(17, 31);
            this.buttnnvfst.TabIndex = 213;
            this.buttnnvfst.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.buttnnvfst.UseVisualStyleBackColor = false;
            this.buttnnvfst.Click += new System.EventHandler(this.buttnnvfst_Click);
            // 
            // buttnnxtlft
            // 
            this.buttnnxtlft.BackColor = System.Drawing.Color.White;
            this.buttnnxtlft.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.buttnnxtlft.FlatAppearance.BorderSize = 0;
            this.buttnnxtlft.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttnnxtlft.Image = ((System.Drawing.Image)(resources.GetObject("buttnnxtlft.Image")));
            this.buttnnxtlft.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttnnxtlft.Location = new System.Drawing.Point(38, 0);
            this.buttnnxtlft.Name = "buttnnxtlft";
            this.buttnnxtlft.Size = new System.Drawing.Size(17, 31);
            this.buttnnxtlft.TabIndex = 212;
            this.buttnnxtlft.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.buttnnxtlft.UseVisualStyleBackColor = false;
            this.buttnnxtlft.Click += new System.EventHandler(this.buttnnxtlft_Click);
            // 
            // btnfinnxt
            // 
            this.btnfinnxt.BackColor = System.Drawing.Color.White;
            this.btnfinnxt.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.btnfinnxt.FlatAppearance.BorderSize = 0;
            this.btnfinnxt.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnfinnxt.Image = ((System.Drawing.Image)(resources.GetObject("btnfinnxt.Image")));
            this.btnfinnxt.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnfinnxt.Location = new System.Drawing.Point(178, 0);
            this.btnfinnxt.Name = "btnfinnxt";
            this.btnfinnxt.Size = new System.Drawing.Size(17, 31);
            this.btnfinnxt.TabIndex = 211;
            this.btnfinnxt.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnfinnxt.UseVisualStyleBackColor = false;
            this.btnfinnxt.Click += new System.EventHandler(this.btnfinnxt_Click);
            // 
            // buttrnxt
            // 
            this.buttrnxt.BackColor = System.Drawing.Color.White;
            this.buttrnxt.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.buttrnxt.FlatAppearance.BorderSize = 0;
            this.buttrnxt.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttrnxt.Image = ((System.Drawing.Image)(resources.GetObject("buttrnxt.Image")));
            this.buttrnxt.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttrnxt.Location = new System.Drawing.Point(143, 0);
            this.buttrnxt.Name = "buttrnxt";
            this.buttrnxt.Size = new System.Drawing.Size(17, 31);
            this.buttrnxt.TabIndex = 210;
            this.buttrnxt.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.buttrnxt.UseVisualStyleBackColor = false;
            this.buttrnxt.Click += new System.EventHandler(this.buttrnxt_Click);
            // 
            // buttnext1
            // 
            this.buttnext1.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.buttnext1.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttnext1.Image = ((System.Drawing.Image)(resources.GetObject("buttnext1.Image")));
            this.buttnext1.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttnext1.Location = new System.Drawing.Point(528, 1);
            this.buttnext1.Name = "buttnext1";
            this.buttnext1.Size = new System.Drawing.Size(56, 30);
            this.buttnext1.TabIndex = 208;
            this.buttnext1.Text = "Exit";
            this.buttnext1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.buttnext1.UseVisualStyleBackColor = false;
            this.buttnext1.Click += new System.EventHandler(this.buttnext1_Click);
            // 
            // chkact
            // 
            this.chkact.AutoSize = true;
            this.chkact.BackColor = System.Drawing.Color.White;
            this.chkact.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkact.Location = new System.Drawing.Point(227, 7);
            this.chkact.Name = "chkact";
            this.chkact.Size = new System.Drawing.Size(65, 22);
            this.chkact.TabIndex = 187;
            this.chkact.Text = "Active";
            this.chkact.UseVisualStyleBackColor = false;
            this.chkact.CheckedChanged += new System.EventHandler(this.chkact_CheckedChanged_1);
            // 
            // butexit
            // 
            this.butexit.BackColor = System.Drawing.Color.White;
            this.butexit.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.butexit.Image = ((System.Drawing.Image)(resources.GetObject("butexit.Image")));
            this.butexit.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.butexit.Location = new System.Drawing.Point(458, 1);
            this.butexit.Name = "butexit";
            this.butexit.Size = new System.Drawing.Size(71, 30);
            this.butexit.TabIndex = 186;
            this.butexit.Text = "Delete";
            this.butexit.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.butexit.UseVisualStyleBackColor = false;
            this.butexit.Click += new System.EventHandler(this.butexit_Click);
            // 
            // butedit
            // 
            this.butedit.BackColor = System.Drawing.Color.White;
            this.butedit.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.butedit.Image = ((System.Drawing.Image)(resources.GetObject("butedit.Image")));
            this.butedit.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.butedit.Location = new System.Drawing.Point(402, 1);
            this.butedit.Name = "butedit";
            this.butedit.Size = new System.Drawing.Size(55, 30);
            this.butedit.TabIndex = 185;
            this.butedit.Text = "Edit";
            this.butedit.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.butedit.UseVisualStyleBackColor = false;
            this.butedit.Click += new System.EventHandler(this.butedit_Click);
            // 
            // btnadd
            // 
            this.btnadd.BackColor = System.Drawing.Color.White;
            this.btnadd.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.btnadd.FlatAppearance.BorderSize = 0;
            this.btnadd.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnadd.Image = ((System.Drawing.Image)(resources.GetObject("btnadd.Image")));
            this.btnadd.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnadd.Location = new System.Drawing.Point(311, 1);
            this.btnadd.Name = "btnadd";
            this.btnadd.Size = new System.Drawing.Size(87, 30);
            this.btnadd.TabIndex = 184;
            this.btnadd.Text = "Add new";
            this.btnadd.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnadd.UseVisualStyleBackColor = false;
            this.btnadd.Click += new System.EventHandler(this.btnadd_Click_1);
            // 
            // Chkedtact
            // 
            this.Chkedtact.AutoSize = true;
            this.Chkedtact.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Chkedtact.Location = new System.Drawing.Point(24, 473);
            this.Chkedtact.Name = "Chkedtact";
            this.Chkedtact.Size = new System.Drawing.Size(65, 22);
            this.Chkedtact.TabIndex = 211;
            this.Chkedtact.Text = "Active";
            this.Chkedtact.UseVisualStyleBackColor = true;
            // 
            // bompro
            // 
            this.bompro.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.bompro.Controls.Add(this.label29);
            this.bompro.Controls.Add(this.label26);
            this.bompro.Controls.Add(this.txtptparty);
            this.bompro.Controls.Add(this.label25);
            this.bompro.Controls.Add(this.Txtptitem);
            this.bompro.Controls.Add(this.label10);
            this.bompro.Controls.Add(this.label11);
            this.bompro.Controls.Add(this.txthour);
            this.bompro.Controls.Add(this.button1);
            this.bompro.Controls.Add(this.label12);
            this.bompro.Controls.Add(this.txtprocessqty);
            this.bompro.Controls.Add(this.label13);
            this.bompro.Controls.Add(this.txtsettime);
            this.bompro.Controls.Add(this.label14);
            this.bompro.Controls.Add(this.txtprocestime);
            this.bompro.Controls.Add(this.label15);
            this.bompro.Controls.Add(this.txtrate);
            this.bompro.Controls.Add(this.label16);
            this.bompro.Controls.Add(this.txtsqgno);
            this.bompro.Controls.Add(this.label17);
            this.bompro.Controls.Add(this.txtproceesname);
            this.bompro.Controls.Add(this.HFG4);
            this.bompro.Controls.Add(this.txtprocessid);
            this.bompro.Controls.Add(this.label18);
            this.bompro.Controls.Add(this.button2);
            this.bompro.Controls.Add(this.buttrqok);
            this.bompro.Controls.Add(this.label19);
            this.bompro.Controls.Add(this.txtperqty);
            this.bompro.Controls.Add(this.label20);
            this.bompro.Controls.Add(this.txtusedqty);
            this.bompro.Controls.Add(this.label21);
            this.bompro.Controls.Add(this.txbomuom);
            this.bompro.Controls.Add(this.label22);
            this.bompro.Controls.Add(this.txtbomcode);
            this.bompro.Controls.Add(this.label23);
            this.bompro.Controls.Add(this.txtbomitem);
            this.bompro.Controls.Add(this.hfg3);
            this.bompro.Controls.Add(this.txtalterid);
            this.bompro.Controls.Add(this.txtbomuomid);
            this.bompro.Location = new System.Drawing.Point(-3, 0);
            this.bompro.Name = "bompro";
            this.bompro.Size = new System.Drawing.Size(1008, 467);
            this.bompro.TabIndex = 219;
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Font = new System.Drawing.Font("Calibri", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label29.Location = new System.Drawing.Point(10, 12);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(96, 21);
            this.label29.TabIndex = 223;
            this.label29.Text = "Item Master";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Font = new System.Drawing.Font("Calibri", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label26.Location = new System.Drawing.Point(467, 52);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(47, 21);
            this.label26.TabIndex = 222;
            this.label26.Text = "Party";
            // 
            // txtptparty
            // 
            this.txtptparty.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtptparty.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtptparty.Location = new System.Drawing.Point(514, 50);
            this.txtptparty.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.txtptparty.Name = "txtptparty";
            this.txtptparty.Size = new System.Drawing.Size(376, 22);
            this.txtptparty.TabIndex = 221;
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Font = new System.Drawing.Font("Calibri", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label25.Location = new System.Drawing.Point(18, 50);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(42, 21);
            this.label25.TabIndex = 220;
            this.label25.Text = "Item";
            // 
            // Txtptitem
            // 
            this.Txtptitem.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Txtptitem.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txtptitem.Location = new System.Drawing.Point(66, 50);
            this.Txtptitem.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.Txtptitem.Name = "Txtptitem";
            this.Txtptitem.Size = new System.Drawing.Size(401, 22);
            this.Txtptitem.TabIndex = 219;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Calibri", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(-1, 295);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(63, 21);
            this.label10.TabIndex = 216;
            this.label10.Text = "Process";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Calibri", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(808, 270);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(65, 21);
            this.label11.TabIndex = 215;
            this.label11.Text = "Rate/Hr";
            // 
            // txthour
            // 
            this.txthour.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txthour.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txthour.Location = new System.Drawing.Point(811, 293);
            this.txthour.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.txthour.Name = "txthour";
            this.txthour.Size = new System.Drawing.Size(82, 22);
            this.txthour.TabIndex = 207;
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.button1.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.Image = ((System.Drawing.Image)(resources.GetObject("button1.Image")));
            this.button1.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.button1.Location = new System.Drawing.Point(899, 287);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(34, 32);
            this.button1.TabIndex = 208;
            this.button1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button1.UseVisualStyleBackColor = false;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Calibri", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(729, 271);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(74, 21);
            this.label12.TabIndex = 211;
            this.label12.Text = "Proc. Qty";
            // 
            // txtprocessqty
            // 
            this.txtprocessqty.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtprocessqty.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtprocessqty.Location = new System.Drawing.Point(724, 293);
            this.txtprocessqty.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.txtprocessqty.Name = "txtprocessqty";
            this.txtprocessqty.Size = new System.Drawing.Size(86, 22);
            this.txtprocessqty.TabIndex = 206;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Calibri", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(556, 271);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(69, 21);
            this.label13.TabIndex = 208;
            this.label13.Text = "Set.Time";
            // 
            // txtsettime
            // 
            this.txtsettime.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtsettime.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtsettime.Location = new System.Drawing.Point(553, 293);
            this.txtsettime.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.txtsettime.Name = "txtsettime";
            this.txtsettime.Size = new System.Drawing.Size(86, 22);
            this.txtsettime.TabIndex = 204;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Calibri", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(643, 271);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(78, 21);
            this.label14.TabIndex = 209;
            this.label14.Text = "Proc.Time";
            // 
            // txtprocestime
            // 
            this.txtprocestime.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtprocestime.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtprocestime.Location = new System.Drawing.Point(640, 293);
            this.txtprocestime.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.txtprocestime.Name = "txtprocestime";
            this.txtprocestime.Size = new System.Drawing.Size(86, 22);
            this.txtprocestime.TabIndex = 205;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Calibri", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(493, 271);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(41, 21);
            this.label15.TabIndex = 205;
            this.label15.Text = "Rate";
            // 
            // txtrate
            // 
            this.txtrate.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtrate.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtrate.Location = new System.Drawing.Point(493, 293);
            this.txtrate.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.txtrate.Name = "txtrate";
            this.txtrate.Size = new System.Drawing.Size(61, 22);
            this.txtrate.TabIndex = 203;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Calibri", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(321, 271);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(55, 21);
            this.label16.TabIndex = 203;
            this.label16.Text = "SeqNo";
            // 
            // txtsqgno
            // 
            this.txtsqgno.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtsqgno.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtsqgno.Location = new System.Drawing.Point(325, 293);
            this.txtsqgno.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.txtsqgno.Name = "txtsqgno";
            this.txtsqgno.Size = new System.Drawing.Size(166, 22);
            this.txtsqgno.TabIndex = 202;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Calibri", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.Location = new System.Drawing.Point(63, 271);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(102, 21);
            this.label17.TabIndex = 201;
            this.label17.Text = "Processname";
            // 
            // txtproceesname
            // 
            this.txtproceesname.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtproceesname.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtproceesname.Location = new System.Drawing.Point(66, 293);
            this.txtproceesname.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.txtproceesname.Name = "txtproceesname";
            this.txtproceesname.Size = new System.Drawing.Size(257, 22);
            this.txtproceesname.TabIndex = 200;
            // 
            // HFG4
            // 
            this.HFG4.BackgroundColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.HFG4.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.HFG4.Location = new System.Drawing.Point(3, 323);
            this.HFG4.Name = "HFG4";
            this.HFG4.Size = new System.Drawing.Size(1001, 136);
            this.HFG4.TabIndex = 199;
            // 
            // txtprocessid
            // 
            this.txtprocessid.Location = new System.Drawing.Point(108, 365);
            this.txtprocessid.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.txtprocessid.Name = "txtprocessid";
            this.txtprocessid.Size = new System.Drawing.Size(26, 21);
            this.txtprocessid.TabIndex = 213;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Calibri", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.Location = new System.Drawing.Point(17, 107);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(45, 21);
            this.label18.TabIndex = 198;
            this.label18.Text = "BOM";
            // 
            // button2
            // 
            this.button2.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.button2.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button2.Image = ((System.Drawing.Image)(resources.GetObject("button2.Image")));
            this.button2.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button2.Location = new System.Drawing.Point(349, 360);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(90, 38);
            this.button2.TabIndex = 197;
            this.button2.Text = "Save";
            this.button2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button2.UseVisualStyleBackColor = false;
            // 
            // buttrqok
            // 
            this.buttrqok.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.buttrqok.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttrqok.Image = ((System.Drawing.Image)(resources.GetObject("buttrqok.Image")));
            this.buttrqok.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.buttrqok.Location = new System.Drawing.Point(899, 102);
            this.buttrqok.Name = "buttrqok";
            this.buttrqok.Size = new System.Drawing.Size(34, 32);
            this.buttrqok.TabIndex = 193;
            this.buttrqok.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.buttrqok.UseVisualStyleBackColor = false;
            this.buttrqok.Click += new System.EventHandler(this.buttrqok_Click_1);
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Calibri", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.Location = new System.Drawing.Point(794, 79);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(68, 21);
            this.label19.TabIndex = 194;
            this.label19.Text = "UsedPer";
            // 
            // txtperqty
            // 
            this.txtperqty.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtperqty.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtperqty.Location = new System.Drawing.Point(792, 105);
            this.txtperqty.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.txtperqty.Name = "txtperqty";
            this.txtperqty.Size = new System.Drawing.Size(101, 22);
            this.txtperqty.TabIndex = 192;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Calibri", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.Location = new System.Drawing.Point(703, 79);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(70, 21);
            this.label20.TabIndex = 192;
            this.label20.Text = "UsedQty";
            // 
            // txtusedqty
            // 
            this.txtusedqty.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtusedqty.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtusedqty.Location = new System.Drawing.Point(700, 105);
            this.txtusedqty.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.txtusedqty.Name = "txtusedqty";
            this.txtusedqty.Size = new System.Drawing.Size(92, 22);
            this.txtusedqty.TabIndex = 191;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("Calibri", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.Location = new System.Drawing.Point(636, 79);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(44, 21);
            this.label21.TabIndex = 190;
            this.label21.Text = "Uom";
            // 
            // txbomuom
            // 
            this.txbomuom.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txbomuom.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txbomuom.Location = new System.Drawing.Point(629, 105);
            this.txbomuom.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.txbomuom.Name = "txbomuom";
            this.txbomuom.Size = new System.Drawing.Size(72, 22);
            this.txbomuom.TabIndex = 189;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Calibri", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.Location = new System.Drawing.Point(463, 77);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(77, 21);
            this.label22.TabIndex = 188;
            this.label22.Text = "ItemCode";
            // 
            // txtbomcode
            // 
            this.txtbomcode.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtbomcode.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtbomcode.Location = new System.Drawing.Point(461, 105);
            this.txtbomcode.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.txtbomcode.Name = "txtbomcode";
            this.txtbomcode.Size = new System.Drawing.Size(166, 22);
            this.txtbomcode.TabIndex = 187;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("Calibri", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.Location = new System.Drawing.Point(66, 77);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(42, 21);
            this.label23.TabIndex = 186;
            this.label23.Text = "Item";
            // 
            // txtbomitem
            // 
            this.txtbomitem.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtbomitem.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtbomitem.Location = new System.Drawing.Point(66, 105);
            this.txtbomitem.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.txtbomitem.Name = "txtbomitem";
            this.txtbomitem.Size = new System.Drawing.Size(397, 22);
            this.txtbomitem.TabIndex = 185;
            // 
            // hfg3
            // 
            this.hfg3.BackgroundColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.hfg3.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.hfg3.Location = new System.Drawing.Point(3, 135);
            this.hfg3.Name = "hfg3";
            this.hfg3.Size = new System.Drawing.Size(1001, 133);
            this.hfg3.TabIndex = 184;
            // 
            // txtalterid
            // 
            this.txtalterid.Location = new System.Drawing.Point(272, 106);
            this.txtalterid.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.txtalterid.Name = "txtalterid";
            this.txtalterid.Size = new System.Drawing.Size(19, 21);
            this.txtalterid.TabIndex = 196;
            // 
            // txtbomuomid
            // 
            this.txtbomuomid.Location = new System.Drawing.Point(591, 106);
            this.txtbomuomid.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.txtbomuomid.Name = "txtbomuomid";
            this.txtbomuomid.Size = new System.Drawing.Size(26, 21);
            this.txtbomuomid.TabIndex = 218;
            // 
            // buttnbomext
            // 
            this.buttnbomext.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.buttnbomext.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttnbomext.Image = ((System.Drawing.Image)(resources.GetObject("buttnbomext.Image")));
            this.buttnbomext.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttnbomext.Location = new System.Drawing.Point(458, 468);
            this.buttnbomext.Name = "buttnbomext";
            this.buttnbomext.Size = new System.Drawing.Size(59, 29);
            this.buttnbomext.TabIndex = 218;
            this.buttnbomext.Text = "Back";
            this.buttnbomext.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.buttnbomext.UseVisualStyleBackColor = false;
            this.buttnbomext.Click += new System.EventHandler(this.buttnbomext_Click);
            // 
            // button4
            // 
            this.button4.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.button4.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button4.Image = ((System.Drawing.Image)(resources.GetObject("button4.Image")));
            this.button4.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button4.Location = new System.Drawing.Point(157, 468);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(129, 30);
            this.button4.TabIndex = 213;
            this.button4.Text = "Bom && Process";
            this.button4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button4.UseVisualStyleBackColor = false;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // btnaddrcan
            // 
            this.btnaddrcan.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnaddrcan.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnaddrcan.Image = ((System.Drawing.Image)(resources.GetObject("btnaddrcan.Image")));
            this.btnaddrcan.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnaddrcan.Location = new System.Drawing.Point(290, 468);
            this.btnaddrcan.Name = "btnaddrcan";
            this.btnaddrcan.Size = new System.Drawing.Size(66, 30);
            this.btnaddrcan.TabIndex = 210;
            this.btnaddrcan.Text = "Back";
            this.btnaddrcan.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnaddrcan.UseVisualStyleBackColor = false;
            this.btnaddrcan.Click += new System.EventHandler(this.btnaddrcan_Click_2);
            // 
            // butnsave
            // 
            this.butnsave.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.butnsave.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.butnsave.Image = ((System.Drawing.Image)(resources.GetObject("butnsave.Image")));
            this.butnsave.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.butnsave.Location = new System.Drawing.Point(90, 468);
            this.butnsave.Name = "butnsave";
            this.butnsave.Size = new System.Drawing.Size(64, 30);
            this.butnsave.TabIndex = 209;
            this.butnsave.Text = "Save";
            this.butnsave.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.butnsave.UseVisualStyleBackColor = false;
            this.butnsave.Click += new System.EventHandler(this.butnsave_Click);
            // 
            // buttnext2
            // 
            this.buttnext2.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.buttnext2.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttnext2.Image = ((System.Drawing.Image)(resources.GetObject("buttnext2.Image")));
            this.buttnext2.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttnext2.Location = new System.Drawing.Point(362, 468);
            this.buttnext2.Name = "buttnext2";
            this.buttnext2.Size = new System.Drawing.Size(60, 30);
            this.buttnext2.TabIndex = 212;
            this.buttnext2.Text = "Exit";
            this.buttnext2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.buttnext2.UseVisualStyleBackColor = false;
            this.buttnext2.Click += new System.EventHandler(this.buttnext2_Click);
            // 
            // Frmitem
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1001, 498);
            this.Controls.Add(this.panadd);
            this.Controls.Add(this.buttnbomext);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.Chkedtact);
            this.Controls.Add(this.btnaddrcan);
            this.Controls.Add(this.butnsave);
            this.Controls.Add(this.buttnext2);
            this.Controls.Add(this.Editpnl);
            this.Controls.Add(this.Genpan);
            this.Controls.Add(this.bompro);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "Frmitem";
            this.Text = "Item Master";
            this.Load += new System.EventHandler(this.Frmitem_Load);
            this.Genpan.ResumeLayout(false);
            this.Genpan.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.HFGP)).EndInit();
            this.Editpnl.ResumeLayout(false);
            this.Editpnl.PerformLayout();
            this.panadd.ResumeLayout(false);
            this.panadd.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.bompro.ResumeLayout(false);
            this.bompro.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.HFG4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.hfg3)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel Genpan;
        private System.Windows.Forms.TextBox Txtscr5;
        private System.Windows.Forms.TextBox Txtscr4;
        private System.Windows.Forms.TextBox Txtscr3;
        private System.Windows.Forms.TextBox Txtscr2;
        private System.Windows.Forms.DataGridView HFGP;
        private System.Windows.Forms.TextBox txtscr1;
        private System.Windows.Forms.Panel Editpnl;
        private System.Windows.Forms.TextBox Txtitgrp;
        private System.Windows.Forms.TextBox txtcusitname;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtcusitcode;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtitname;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox Txtitcode;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.RichTextBox txtspec;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txttax1;
        //private System.Windows.Forms.Label Category;
        private System.Windows.Forms.TextBox txtuom1;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtuomid;
        private System.Windows.Forms.TextBox txtcatid;
        private System.Windows.Forms.TextBox txtitgrpid;
        private System.Windows.Forms.TextBox txtpartyid;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox txtparty;
        private System.Windows.Forms.TextBox txtgrp;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.TextBox txtgrpid;
        private System.Windows.Forms.TextBox txthsn;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.TextBox txthsnid;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panadd;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label lblno1;
        private System.Windows.Forms.Label lblno2;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel3;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel2;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private System.Windows.Forms.Button buttnnvfst;
        private System.Windows.Forms.Button buttnnxtlft;
        private System.Windows.Forms.Button btnfinnxt;
        private System.Windows.Forms.Button buttrnxt;
        private System.Windows.Forms.Button buttnext1;
        private System.Windows.Forms.CheckBox chkact;
        private System.Windows.Forms.Button butexit;
        private System.Windows.Forms.Button butedit;
        private System.Windows.Forms.Button btnadd;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.CheckBox Chkedtact;
        private System.Windows.Forms.Button btnaddrcan;
        private System.Windows.Forms.Button butnsave;
        private System.Windows.Forms.Button buttnext2;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button buttnbomext;
        private System.Windows.Forms.Panel bompro;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.TextBox txtptparty;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.TextBox Txtptitem;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox txthour;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox txtprocessqty;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox txtsettime;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox txtprocestime;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox txtrate;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.TextBox txtsqgno;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox txtproceesname;
        private System.Windows.Forms.DataGridView HFG4;
        private System.Windows.Forms.TextBox txtprocessid;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button buttrqok;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.TextBox txtperqty;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.TextBox txtusedqty;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.TextBox txbomuom;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.TextBox txtbomcode;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.TextBox txtbomitem;
        private System.Windows.Forms.DataGridView hfg3;
        private System.Windows.Forms.TextBox txtalterid;
        private System.Windows.Forms.TextBox txtbomuomid;
        private System.Windows.Forms.ComboBox txtcat;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.ComboBox txtuom;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.TextBox txthsncode;
        private System.Windows.Forms.ComboBox txttax;
        private System.Windows.Forms.ComboBox comboBox1;
    }
}