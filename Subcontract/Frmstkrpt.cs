﻿using System;
using System.Data;
using System.Drawing;
using System.Windows.Forms;
using System.IO;
using System.Data.SqlClient;
using excel = Microsoft.Office.Interop.Excel;
using CrystalDecisions.CrystalReports.Engine;

namespace ACV
{

    public partial class Frmstkrpt : Form
    {
        private CrystalDecisions.Windows.Forms.CrystalReportViewer Cryview = new CrystalDecisions.Windows.Forms.CrystalReportViewer();
        //private Container components = null;
        ReportDocument doc = new ReportDocument();
        public Frmstkrpt()
        {
            CenterToParent();
            this.BackColor = Color.White;
            InitializeComponent();
        }
        //int uid = 0;
        //int mode = 0;
        //string tpuid = "";

        private static Microsoft.Office.Interop.Excel.Workbook mWorkBook;
        private static Microsoft.Office.Interop.Excel.Sheets mWorkSheets;
        private static Microsoft.Office.Interop.Excel.Worksheet mWSheet1;
        private static Microsoft.Office.Interop.Excel.Application oXL;
        SqlConnection conn = new SqlConnection("Data Source=" + Genclass.data2 + "; Initial Catalog=" + Genclass.data5 + ";User id=" + Genclass.data3 + ";Password=" + Genclass.data4 + "");
        SqlCommand qur = new SqlCommand();



        private void Titlep1()
        {
            DataGridViewCheckBoxColumn checkColumn = new DataGridViewCheckBoxColumn();
            checkColumn.Name = "CHK";
            checkColumn.HeaderText = "CHK";

            checkColumn.TrueValue = true;
            checkColumn.FalseValue = false;
            checkColumn.Width = 50;
            checkColumn.ReadOnly = false;
            checkColumn.FillWeight = 10; //if the datagridview is resized (on form resize) the checkbox won't take up too much; value is relative to the other columns' fill values
            HFGP.Columns.Add(checkColumn);
        }


        private void Titlep2()
        {
            DataGridViewCheckBoxColumn checkColumn = new DataGridViewCheckBoxColumn();
            checkColumn.Name = "CHK";
            checkColumn.HeaderText = "CHK";
            checkColumn.Width = 50;
            checkColumn.ReadOnly = false;
            checkColumn.FillWeight = 10; //if the datagridview is resized (on form resize) the checkbox won't take up too much; value is relative to the other columns' fill values
            HFIT.Columns.Add(checkColumn);


        }

        private void Frmstkrpt_Load(object sender, EventArgs e)
        {
            qur.Connection = conn;

            Left = (MdiParent.ClientRectangle.Width - Width) / 3;
            Top = (MdiParent.ClientRectangle.Height - Height) / 3;

            this.HFGP.DefaultCellStyle.Font = new Font("calibri", 10);
            this.HFGP.ColumnHeadersDefaultCellStyle.Font = new Font("calibri", 11, FontStyle.Bold);
            this.HFIT.DefaultCellStyle.Font = new Font("calibri", 10);
            this.HFIT.ColumnHeadersDefaultCellStyle.Font = new Font("calibri", 11, FontStyle.Bold);
            Genclass.Module.buttonstyleform(this);

            HFIT.RowHeadersVisible = false;

            HFGP.RowHeadersVisible = false;


            //qur.CommandText = "Create table #tmpitid (Itemid bigint)";
            //qur.ExecuteNonQuery();

            Titlep1();
            Titlep2();

            if (Genclass.Gbtxtid == 210)
            {
                cbogrp.Visible = false;
                label4.Visible = false;
                label1.Text = "Stock Register";
                loadParty();

            }
            else if (Genclass.Gbtxtid == 230)
            {
                cbogrp.Visible = false;
                label4.Visible = false;
                button1_Click(sender, e);
                loadParty();
                cbogrp.Text = "Sales Order Outstaing Register";
                //buttonview.Visible = false;
            }

            else 
            {
                loaditem();
                button1_Click(sender, e);
                cbogrp.Text = "Stock Statement";
                //buttonview.Visible = false;
            }

        }

        private void loaditem()
        {
            conn.Open();

            Genclass.StrSrch = "";
            Genclass.FSSQLSortStr = "Itemname";

            if (Txtitem.Text != "")
            {
                if (Genclass.StrSrch == "")
                {
                    Genclass.StrSrch = Genclass.FSSQLSortStr + " like '%" + Txtitem.Text + "%'";
                }
                else
                {
                    Genclass.StrSrch = Genclass.StrSrch + " and " + Genclass.FSSQLSortStr + " like '%" + Txtitem.Text + "%'";
                }
            }

            if (Txtitem.Text != "")
            {
                Genclass.StrSrch = " " + Genclass.StrSrch;
            }
            else
            {
                Genclass.StrSrch = "uid <> 0";
            }

            string quy = "select Itemname as Item,uid from Itemm where active=1 and companyid=" + Genclass.data1 + " and " + Genclass.StrSrch + "";
            Genclass.cmd = new SqlCommand(quy, conn);

            SqlDataAdapter aptr = new SqlDataAdapter(Genclass.cmd);
            DataTable tap = new DataTable();
            aptr.Fill(tap);


            HFGP.AutoGenerateColumns = false;
            HFGP.Refresh();
            HFGP.DataSource = null;
            HFGP.Rows.Clear();


            HFGP.ColumnCount = tap.Columns.Count + 1;
            Genclass.i = 1;
            foreach (DataColumn column in tap.Columns)
            {
                HFGP.Columns[Genclass.i].Name = column.ColumnName;
                HFGP.Columns[Genclass.i].HeaderText = column.ColumnName;
                HFGP.Columns[Genclass.i].DataPropertyName = column.ColumnName;
                Genclass.i = Genclass.i + 1;
            }

            HFGP.Columns[1].Width = 350;
            HFGP.Columns[2].Visible = false;
            HFGP.DataSource = tap;
        }

        private void loadParty()
        {
            conn.Close();
            conn.Open();

            Genclass.StrSrch = "";
            Genclass.FSSQLSortStr = "Name";

            if (Txtitem.Text != "")
            {
                if (Genclass.StrSrch == "")
                {
                    Genclass.StrSrch = Genclass.FSSQLSortStr + " like '%" + Txtitem.Text + "%'";
                }
                else
                {
                    Genclass.StrSrch = Genclass.StrSrch + " and " + Genclass.FSSQLSortStr + " like '%" + Txtitem.Text + "%'";
                }
            }

            if (Txtitem.Text != "")
            {
                Genclass.StrSrch = " " + Genclass.StrSrch;
            }
            else
            {
                Genclass.StrSrch = "uid <> 0";
            }

            string quy = "select Name as Party,Address1,uid from partym where active=1 and companyid=" + Genclass.data1 + " and " + Genclass.StrSrch + " order by name";
            Genclass.cmd = new SqlCommand(quy, conn);

            SqlDataAdapter aptr = new SqlDataAdapter(Genclass.cmd);
            DataTable tap = new DataTable();
            aptr.Fill(tap);


            HFGP.AutoGenerateColumns = false;
            HFGP.Refresh();
            HFGP.DataSource = null;
            HFGP.Rows.Clear();


            HFGP.ColumnCount = tap.Columns.Count + 1;
            Genclass.i = 1;
            foreach (DataColumn column in tap.Columns)
            {
                HFGP.Columns[Genclass.i].Name = column.ColumnName;
                HFGP.Columns[Genclass.i].HeaderText = column.ColumnName;
                HFGP.Columns[Genclass.i].DataPropertyName = column.ColumnName;
                Genclass.i = Genclass.i + 1;
            }
            HFGP.Columns[3].Visible = false;
            HFGP.Columns[2].Width = 350;
            HFGP.Columns[1].Width = 250;
            HFGP.DataSource = tap;
        }



        private void chkact_Click(object sender, EventArgs e)
        {
            //foreach (DataGridViewRow row in HFGP.Rows)
            //{
            //    DataGridViewCheckBoxCell chk = (DataGridViewCheckBoxCell)row.Cells[0];

            //    if (chk.Value == chk.FalseValue || chk.Value == null)
            //    {
            //        chk.Value = chk.TrueValue;
            //    }
            //    else
            //    {
            //        chk.Value = chk.FalseValue;
            //    }

            //}

            //HFGP.EndEdit();

            if (chkact.Checked == true)
            {
                for (int i = 0; i < HFGP.RowCount; i++)
                {
                    HFGP[0, i].Value = true;
                }
            }
            else
            {
                for (int i = 0; i < HFGP.RowCount; i++)
                {
                    HFGP[0, i].Value = false;
                }
            }

        }

        private void HFGP_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            DataGridViewCheckBoxCell ch1 = new DataGridViewCheckBoxCell();
            ch1 = (DataGridViewCheckBoxCell)HFGP.Rows[HFGP.CurrentRow.Index].Cells[0];

            if (ch1.Value == null)
                ch1.Value = false;
            switch (ch1.Value.ToString())
            {
                case "True":
                    {
                        ch1.Value = false;

                        break;
                    }
                case "False":
                    {
                        ch1.Value = true;
                        //Where should I put the selected cell here?
                        break;
                    }
            }
        }

        private void chkact1_Click(object sender, EventArgs e)
        {
            if (chkact1.Checked == true)
            {
                for (int i = 0; i < HFIT.RowCount; i++)
                {
                    if (HFGP.Rows[i].Cells[1].Value.ToString() != "")
                    {
                        HFIT[0, i].Value = true;
                    }
                }
            }
            else
            {
                for (int i = 0; i < HFIT.RowCount; i++)
                {
                    if (HFGP.Rows[i].Cells[1].Value.ToString() != "")
                    {
                        HFIT[0, i].Value = false;
                    }
                }
            }
        }

        private void HFIT_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            DataGridViewCheckBoxCell ch1 = new DataGridViewCheckBoxCell();
            ch1 = (DataGridViewCheckBoxCell)HFIT.Rows[HFIT.CurrentRow.Index].Cells[0];

            if (ch1.Value == null)
                ch1.Value = false;
            switch (ch1.Value.ToString())
            {
                case "True":
                    {
                        ch1.Value = false;

                        break;
                    }
                case "False":
                    {
                        ch1.Value = true;
                        //Where should I put the selected cell here?
                        break;
                    }
            }
        }

        private void btnexit_Click(object sender, EventArgs e)
        {
            //qur.CommandText = "drop table #tmpitid";
            //qur.ExecuteNonQuery();
            this.Dispose();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            int i = 0;
            conn.Close();
            conn.Open();
            qur.CommandText = "delete from tmpitid";
            qur.ExecuteNonQuery();

            foreach (DataGridViewRow row in HFGP.Rows)
            {
                DataGridViewCheckBoxCell chk = (DataGridViewCheckBoxCell)row.Cells[0];
                if (cbogrp.Text == "Stock Statement"||cbogrp.Text == "Stock Ledger")
                {
                      if (Convert.ToBoolean(row.Cells[0].Value) == true)
                {
                    qur.CommandText = "delete from tmpitid where itemid=" + HFGP.Rows[i].Cells[2].Value.ToString() + "";
                    qur.ExecuteNonQuery();
                    qur.CommandText = "insert into tmpitid values (" + HFGP.Rows[i].Cells[2].Value.ToString() + ")";
                    qur.ExecuteNonQuery();
                }
                }
             
                else

                {
                if (Convert.ToBoolean(row.Cells[0].Value) == true)
                {
                    qur.CommandText = "delete from tmpitid where itemid=" + HFGP.Rows[i].Cells[3].Value.ToString() + "";
                    qur.ExecuteNonQuery();
                    qur.CommandText = "insert into tmpitid values (" + HFGP.Rows[i].Cells[3].Value.ToString() + ")";
                    qur.ExecuteNonQuery();
                }
                }

                i = i + 1;

            }
            if (Genclass.Gbtxtid == 210 || Genclass.Gbtxtid == 230)
            {
                Loadfitparty();
            }

            else
            {
                Loadhfit();
            }



        }

        private void button2_Click(object sender, EventArgs e)
        {
            int i = 0;

            foreach (DataGridViewRow row in HFIT.Rows)
            {
                DataGridViewCheckBoxCell chk = (DataGridViewCheckBoxCell)row.Cells[0];

                if (Convert.ToBoolean(row.Cells[0].Value) == true)
                {
                    qur.CommandText = "delete from tmpitid where itemid=" + HFIT.Rows[i].Cells[3].Value.ToString() + "";
                    qur.ExecuteNonQuery();

                }

                i = i + 1;

            }

            if (Genclass.Gbtxtid == 210)
            {
                Loadfitparty();
            }

            else
            {
                Loadhfit();
            }
        }


        private void Loadhfit()
        {
            HFIT.Refresh();
            HFIT.DataSource = null;
            HFIT.Rows.Clear();

            string quy = "select b.itemname,a.itemid from tmpitid a inner join itemm b on a.itemid=b.uid ";
            Genclass.cmd = new SqlCommand(quy, conn);

            SqlDataAdapter aptr = new SqlDataAdapter(Genclass.cmd);
            DataTable tap = new DataTable();
            aptr.Fill(tap);

            HFIT.DataSource = tap;


            HFIT.Columns[1].Name = "Selected Item";
            HFIT.Columns[2].Name = "uid";

            HFIT.Columns[1].Width = 350;
            HFIT.Columns[2].Visible = false;

            HFIT.AutoGenerateColumns = false;
        }
        private void Loadfitparty()
        {
            HFIT.Refresh();
            HFIT.DataSource = null;
            HFIT.Rows.Clear();

            string quy = "select b.name,a.itemid from tmpitid a inner join partym b on a.itemid=b.uid ";
            Genclass.cmd = new SqlCommand(quy, conn);

            SqlDataAdapter aptr = new SqlDataAdapter(Genclass.cmd);
            DataTable tap = new DataTable();
            aptr.Fill(tap);

            HFIT.DataSource = tap;


            HFIT.Columns[1].Name = "Selected Party";
            HFIT.Columns[2].Name = "uid";

            HFIT.Columns[1].Width = 310;
            HFIT.Columns[2].Visible = false;

            HFIT.AutoGenerateColumns = false;
        }
        private void Txtitem_TextChanged(object sender, EventArgs e)
        {
            conn.Close();
            loadParty();
        }

        private void cmdprt_Click(object sender, EventArgs e)
        {
            if (Genclass.Gbtxtid == 210)
            {
                conn.Close();
                conn.Open();
                Crviewer crv = new Crviewer();

                SqlDataAdapter da = new SqlDataAdapter("sp_moneyreceipt", conn);
                da.SelectCommand.CommandType = CommandType.StoredProcedure;


                da.SelectCommand.Parameters.Add("@companyid", SqlDbType.Int).Value = Genclass.data1;

                da.SelectCommand.Parameters.Add("@fromdate", SqlDbType.Date).Value = Fromdt.Text;
                da.SelectCommand.Parameters.Add("@todate", SqlDbType.Date).Value = Todt.Text;

                DataSet ds = new DataSet();
                da.Fill(ds, "MoneyReceipt");

                doc.Load(Application.StartupPath + "\\MoneyRecRpt.rpt");

                doc.SetDataSource(ds);
                Cryview.ReportSource = doc;
                doc.PrintToPrinter(1, false, 0, 0);

            }
            else if (Genclass.Gbtxtid == 230)
            {

                conn.Close();
                conn.Open();
                string strgp = "";
                for (int k = 0; k < HFIT.Rows.Count - 1; k++)
                {
                    if (strgp == "")
                    {
                        strgp = Convert.ToString(HFIT.Rows[k].Cells[2].Value);
                    }
                    else
                    {
                        strgp = strgp + "," + Convert.ToString(HFIT.Rows[k].Cells[2].Value);
                    }
                }

               string path = Application.StartupPath + "\\SalesOrder Outstanding.xls";
                if (!File.Exists(path))
                {
                    File.Create(path);
                }
                oXL = new excel.Application();
                oXL.Visible = true;
                oXL.DisplayAlerts = false;

                mWorkBook = oXL.Workbooks.Open(path, 0, false, 5, "", "", false, Microsoft.Office.Interop.Excel.XlPlatform.xlWindows, "", true, false, 0, true, false, false);
                mWorkSheets = mWorkBook.Worksheets;
                mWSheet1 = (excel.Worksheet)mWorkSheets.get_Item("SalesOrder Outstanding");
                excel.Range range = mWSheet1.UsedRange;
                Genclass.strsql = "select b.Name as Partyname ,b.address1 as Address,Docno,convert(date,Docdate,102) as Docdate,Ordno as Orderno,orddate as Orderdate,ItemCode,ItemName,reqqty as OrderQty,recqty as Invoiceqty,Pendingqty,totqty as Totalqty ,convert(date,reqdate,102) as RequiredDate,invno as InvoiceNo,convert(date,invdate,102) as InvoiceDate from salesorout a inner join partym b on a.partyuid=b.uid where a.partyuid in (" + strgp + ") and convert(date,Docdate,102)>=convert(date,'" + Fromdt.Value + "',102)  and convert(date,Docdate,102)<=convert(date,'" + Todt.Value + "',102) order by docno,Docdate  ";
                Genclass.cmd = new SqlCommand(Genclass.strsql, conn);
                SqlDataAdapter aptr15 = new SqlDataAdapter(Genclass.cmd);
                DataTable tap15 = new DataTable();
                aptr15.Fill(tap15);

                if (tap15.Rows.Count > 0)
                {
                    Genclass.h = 1;
                    foreach (DataColumn column in tap15.Columns)
                    {
                        mWSheet1.Cells[1, Genclass.h] = column.ColumnName;
                        Genclass.h = Genclass.h + 1;
                    }
                }

                int h = 2;
                for (int j = 0; j < tap15.Rows.Count; j++)
                {
                    mWSheet1.Cells[h, 1] = tap15.Rows[j]["Partyname"].ToString();
                    mWSheet1.Cells[h, 2] = tap15.Rows[j]["Address"].ToString();
                    mWSheet1.Cells[h, 3] = tap15.Rows[j]["Docno"].ToString();
                    mWSheet1.Cells[h, 4] = tap15.Rows[j]["Docdate"].ToString();
                    mWSheet1.Cells[h, 5] = tap15.Rows[j]["Orderno"].ToString();
                    mWSheet1.Cells[h, 6] = tap15.Rows[j]["Orderdate"].ToString();
                    mWSheet1.Cells[h, 7] = tap15.Rows[j]["Itemcode"].ToString();
                    mWSheet1.Cells[h, 8] = tap15.Rows[j]["itemname"].ToString();
                    mWSheet1.Cells[h, 9] = tap15.Rows[j]["OrderQty"].ToString();
                    mWSheet1.Cells[h, 10] = tap15.Rows[j]["Invoiceqty"].ToString();
                    mWSheet1.Cells[h, 11] = tap15.Rows[j]["Pendingqty"].ToString();
                    mWSheet1.Cells[h, 12] = tap15.Rows[j]["Totalqty"].ToString();
                    mWSheet1.Cells[h, 13] = tap15.Rows[j]["RequiredDate"].ToString();
                    mWSheet1.Cells[h, 14] = tap15.Rows[j]["InvoiceNo"].ToString();
                    mWSheet1.Cells[h, 15] = tap15.Rows[j]["InvoiceDate"].ToString();
                    h = h + 1;
                }
            }
            else
            {
                conn.Close();
                conn.Open();
                if (cbogrp.Text == "Stock Statement")
                {
                    Genclass.Dtype = 1;

                    int i = HFGP.SelectedCells[0].RowIndex;

                    qur.CommandText = "Exec SP_stkstmt '" + Fromdt.Text + "','" + Todt.Text + "',"+ Genclass.data1 +"";
                    qur.ExecuteNonQuery();

                    textBox1.Text = Fromdt.Text;
                    textBox2.Text = Todt.Text;
                    Genclass.prtfrmdate = Convert.ToDateTime(textBox1.Text);
                    Genclass.prttodate = Convert.ToDateTime(textBox2.Text);
                    Crviewer crv = new Crviewer();
                    crv.Show();
                }
                else
                {
                    Genclass.Dtype = 2;


                    int i = HFGP.SelectedCells[0].RowIndex;

                    qur.CommandText = "Exec SP_stkledger '" + Fromdt.Text + "','" + Todt.Text + "'," + Genclass.data1 + "";
                    qur.ExecuteNonQuery();

                    textBox1.Text = Fromdt.Text;
                    textBox2.Text = Todt.Text;
                    Genclass.prtfrmdate = Convert.ToDateTime(textBox1.Text);
                    Genclass.prttodate = Convert.ToDateTime(textBox2.Text);
                    Crviewer crv = new Crviewer();
                    crv.Show();
                }
            

            }

          
            conn.Close();
        }





        private void chkact_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void HFGP_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

    }


}