﻿using System;
using System.Data;
using System.Drawing;
using System.Windows.Forms;
using System.Data.SqlClient;


namespace ACV
{
    public partial class FrmMoneyReceipt : Form
    {
        public FrmMoneyReceipt()
        {
            CenterToParent();
            this.BackColor = Color.White;
            InitializeComponent();
        }
        string uid = "";
        int mode = 0;
        //SqlCommand cmd;
        //SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["connstr"].ConnectionString);
        SqlConnection conn = new SqlConnection("Data Source=" + Genclass.data2 + "; Initial Catalog=" + Genclass.data5 + ";User id=" + Genclass.data3 + ";Password=" + Genclass.data4 + "");
        SqlCommand qur = new SqlCommand();
        private void cboexcise_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cboexcise.Text == "CASH")
            {
                txtdcno.Enabled = false;
                Dtpdt.Enabled = false;
                cbrbank.Enabled = false;
                cbrdbbank.Enabled = false;
            }
            else
            {
                txtdcno.Enabled = true;
                Dtpdt.Enabled = true;
                cbrbank.Enabled = true;
                cbrdbbank.Enabled = true;
            }
            //loadBill();
            //
        }

        private void fun2()
        {
            Genclass.sum3 = 0;
            for (int i = 0; i < HFIT.Rows.Count - 1; i++)
            {

                Genclass.sum3 = Genclass.sum3 + Convert.ToDouble(HFIT.Rows[i].Cells[3].Value);
                txtbilltot.Text = Genclass.sum3.ToString("0.00");
            }
        }
        private void loadBill()
        {
            conn.Close();
            conn.Open();
            if (txtpuid.Text != "")
            {
                if (Genclass.Dtype == 180)
                {
                    string quy = "select Date,BillNo,Billamount,AdjustedAmount,uid, amount  from  (select distinct Convert(nvarchar,docdate,105) as Date, Docno as BillNo,Netvalue-ISNULL(b.amount,0) As Billamount,'' as AdjustedAmount,a.uid,ISNULL(b.amount,0) as amount from Stransactionsp  a left join MTransactionslist  b on a.uid=b.STRefuid and Netvalue<>amount   where  a.Doctypeid=40 and companyid=" + Genclass.data1 + " and partyuid=" + txtpuid.Text + " )tab order by billno ";
                    Genclass.cmd = new SqlCommand(quy, conn);
                }

                else
                {
                    string quy = "select Date,BillNo,Billamount,AdjustedAmount,uid, amount  from  (select distinct Convert(nvarchar,docdate,105) as Date, Docno as BillNo,Netvalue-ISNULL(b.amount,0) As Billamount,'' as AdjustedAmount,a.uid,ISNULL(b.amount,0) as amount from Stransactionsp  a left join MTransactionslist  b on a.uid=b.STRefuid and Netvalue<>amount   where  a.Doctypeid=100 and companyid=" + Genclass.data1 + " and partyuid=" + txtpuid.Text + " )tab order by billno ";
                    Genclass.cmd = new SqlCommand(quy, conn);

                }



                SqlDataAdapter aptr = new SqlDataAdapter(Genclass.cmd);
                DataTable tap = new DataTable();
                aptr.Fill(tap);
                if (tap.Rows.Count > 0)
                {
                    if (Genclass.Dtype == 180)
                    {
                        string quy1 = "select Date,BillNo,Billamount,AdjustedAmount,uid  from  (select distinct Convert(nvarchar,docdate,105) as Date,  Docno as BillNo,ISNULL(Netvalue,0)-ISNULL(sum(b.amount),0) As Billamount,'' as AdjustedAmount,a.uid from Stransactionsp  a left join  MTransactionslist  b on a.uid=b.STRefuid    where  Doctypeid=40 and companyid=" + Genclass.data1 + " and partyuid=" + txtpuid.Text + "  group by docdate,Docno,Netvalue,a.uid having isnull(Netvalue,0)<>ISNULL(sum(b.amount),0))  tab order by billno";
                        Genclass.cmd = new SqlCommand(quy1, conn);
                    }

                    else
                    {
                        string quy1 = "select Date,BillNo,Billamount,AdjustedAmount,uid  from  (select distinct Convert(nvarchar,docdate,105) as Date,  Docno as BillNo,ISNULL(Netvalue,0)-ISNULL(sum(b.amount),0) As Billamount,'' as AdjustedAmount,a.uid from Stransactionsp  a left join  MTransactionslist  b on a.uid=b.STRefuid    where  Doctypeid=100 and companyid=" + Genclass.data1 + " and partyuid=" + txtpuid.Text + "  group by docdate,Docno,Netvalue,a.uid having isnull(Netvalue,0)<>ISNULL(sum(b.amount),0))  tab order by billno";
                        Genclass.cmd = new SqlCommand(quy1, conn);

                    }

                }
                SqlDataAdapter aptr1 = new SqlDataAdapter(Genclass.cmd);
                DataTable tap1 = new DataTable();
                aptr1.Fill(tap1);


                if (tap1.Rows.Count == 0)
                {
                }

                HFIT.AutoGenerateColumns = false;
                HFIT.Refresh();
                HFIT.DataSource = null;
                HFIT.Rows.Clear();


                HFIT.ColumnCount = tap1.Columns.Count + 1;
                Genclass.i = 1;
                foreach (DataColumn column in tap1.Columns)
                {
                    HFIT.Columns[Genclass.i].Name = column.ColumnName;
                    HFIT.Columns[Genclass.i].HeaderText = column.ColumnName;
                    HFIT.Columns[Genclass.i].DataPropertyName = column.ColumnName;
                    Genclass.i = Genclass.i + 1;
                }
                HFIT.Columns[0].Width = 50;
                HFIT.Columns[1].Width = 201;
                HFIT.Columns[2].Width = 201;
                HFIT.Columns[3].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                HFIT.Columns[3].Width = 150;
                HFIT.Columns[4].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                HFIT.Columns[4].Width = 201;
                HFIT.Columns[5].Visible = false;
                HFIT.DataSource = tap1;
            }
        }

        private void loadBill1()
        {
            conn.Close();
            conn.Open();

            if (Genclass.Dtype == 180)
            {
                string quy = "select Date,BillNo,Billamount,AdjustedAmount,uid  from (select distinct Convert(nvarchar,c.docdate,105) as Date,c.Docno as BillNo, Netvalue As Billamount,b.amount as AdjustedAmount,a.uid from MTransactions  a inner join  MTransactionslist  b on a.uid=b.Mtuid left join Stransactionsp c on  b.STRefuid=c.uid   where a.uid=" + txtgrnid.Text + ")tab order by billno";
                Genclass.cmd = new SqlCommand(quy, conn);
            }
            else
            {
                string quy = "select Date,BillNo,Billamount,AdjustedAmount,uid  from (select distinct Convert(nvarchar,c.docdate,105) as Date,c.Docno as BillNo, Netvalue As Billamount,b.amount as AdjustedAmount,a.uid from MTransactions  a inner join  MTransactionslist  b on a.uid=b.Mtuid left join Stransactionsp c on  b.STRefuid=c.uid   where a.uid=" + txtgrnid.Text + ")tab order by billno";
                Genclass.cmd = new SqlCommand(quy, conn);

            }


            SqlDataAdapter aptr = new SqlDataAdapter(Genclass.cmd);
            DataTable tap = new DataTable();
            aptr.Fill(tap);


            HFIT.AutoGenerateColumns = false;
            HFIT.Refresh();
            HFIT.DataSource = null;
            HFIT.Rows.Clear();


            HFIT.ColumnCount = tap.Columns.Count + 1;
            Genclass.i = 1;
            foreach (DataColumn column in tap.Columns)
            {
                HFIT.Columns[Genclass.i].Name = column.ColumnName;
                HFIT.Columns[Genclass.i].HeaderText = column.ColumnName;
                HFIT.Columns[Genclass.i].DataPropertyName = column.ColumnName;
                Genclass.i = Genclass.i + 1;
            }
            HFIT.Columns[0].Width = 50;
            HFIT.Columns[1].Width = 201;
            HFIT.Columns[2].Width = 201;
            HFIT.Columns[3].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            HFIT.Columns[3].Width = 150;
            HFIT.Columns[4].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            HFIT.Columns[4].Width = 201;
            HFIT.Columns[5].Visible = false;
            HFIT.DataSource = tap;
        }
        private void Loadgrid()
        {
            try
            {
                conn.Open();

                Genclass.StrSrch = "";


                Genclass.FSSQLSortStr = "Docno";
                Genclass.FSSQLSortStr1 = "Docdate";
                Genclass.FSSQLSortStr2 = "Party";
                Genclass.FSSQLSortStr3 = "Mode";
                Genclass.FSSQLSortStr4 = "Total";






                if (txtscr1.Text != "")
                {
                    if (Genclass.StrSrch == "")
                    {
                        Genclass.StrSrch = Genclass.FSSQLSortStr + " like '%" + txtscr1.Text + "%'";
                    }
                    else
                    {
                        Genclass.StrSrch = Genclass.StrSrch + " and " + Genclass.FSSQLSortStr + " like '%" + txtscr1.Text + "%'";
                    }

                }

                if (Txtscr2.Text != "")
                {
                    if (Genclass.StrSrch == "")
                    {
                        Genclass.StrSrch = Genclass.FSSQLSortStr1 + " like '%" + Txtscr2.Text + "%'";
                    }
                    else
                    {
                        Genclass.StrSrch = Genclass.StrSrch + " and " + Genclass.FSSQLSortStr1 + " like '%" + Txtscr2.Text + "%'";
                    }

                }

                if (Txtscr3.Text != "")
                {
                    if (Genclass.StrSrch == "")
                    {
                        Genclass.StrSrch = Genclass.FSSQLSortStr2 + " like '%" + Txtscr3.Text + "%'";
                    }
                    else
                    {
                        Genclass.StrSrch = Genclass.StrSrch + " and " + Genclass.FSSQLSortStr2 + " like '%" + Txtscr3.Text + "%'";
                    }

                }

                if (txtscr4.Text != "")
                {
                    if (Genclass.StrSrch == "")
                    {
                        Genclass.StrSrch = Genclass.FSSQLSortStr3 + " like '%" + txtscr4.Text + "%'";
                    }
                    else
                    {
                        Genclass.StrSrch = Genclass.StrSrch + " and " + Genclass.FSSQLSortStr3 + " like '%" + txtscr4.Text + "%'";
                    }

                }

                if (txtscr5.Text != "")
                {
                    if (Genclass.StrSrch == "")
                    {
                        Genclass.StrSrch = Genclass.FSSQLSortStr4 + " like '%" + txtscr5.Text + "%'";
                    }
                    else
                    {
                        Genclass.StrSrch = Genclass.StrSrch + " and " + Genclass.FSSQLSortStr4 + " like '%" + txtscr5.Text + "%'";
                    }

                }





                if (txtscr1.Text != "")
                {
                    Genclass.StrSrch = " " + Genclass.StrSrch;
                }
                else if (Txtscr2.Text != "")
                {
                    Genclass.StrSrch = " " + Genclass.StrSrch;
                }

                else if (Txtscr3.Text != "")
                {
                    Genclass.StrSrch = " " + Genclass.StrSrch;
                }
                else if (txtscr4.Text != "")
                {
                    Genclass.StrSrch = " " + Genclass.StrSrch;
                }
                else if (txtscr5.Text != "")
                {
                    Genclass.StrSrch = " " + Genclass.StrSrch;
                }




                DateTime str9 = Convert.ToDateTime(dtpfnt.Text);
                if (chkact.Checked == true)
                {

                    if (Genclass.Dtype == 180)
                    {
                        string quy = "select a.uid,Docno,Docdate,name as Customer,Transtype as Type,Amount,ISNULL(a.Refno,0) as Refno,refdate,c.bankname,d.bankname,a.amount,a.partyuid from MTransactions a  inner join partym b on a.partyuid=b.uid and a.doctypeid=180  left join bank c on a.bankuid=c.uid left join bank d on a.Tobankuid=d.uid where a.active=1 and a.companyid=" + Genclass.data1 + " and month(docdate)=" + str9.Month + "  and year(docdate)=" + str9.Year + " ";
                        Genclass.cmd = new SqlCommand(quy, conn);
                    }
                    else
                    {
                        string quy = "select a.uid,Docno,Docdate,name as Customer,Transtype as Type,Amount,ISNULL(a.Refno,0) as Refno,refdate,c.bankname,d.bankname,a.amount,a.partyuid from MTransactions a  inner join partym b on a.partyuid=b.uid and a.doctypeid=190 left join bank c on a.bankuid=c.uid left join bank d on a.Tobankuid=d.uid where a.active=1 and a.companyid=" + Genclass.data1 + " and month(docdate)=" + str9.Month + "  and year(docdate)=" + str9.Year + " ";
                        Genclass.cmd = new SqlCommand(quy, conn);
                    }
                }

                else if (chkact.Checked == false)
                {
                    if (Genclass.Gbtxtid == 180)
                    {
                        string quy = "select a.uid,docno,docdate,name as partym,transtype,Amount,a.partyuid from MTransactions a inner join partym b on a.partyuid=b.uid where a.active=0 and a.companyid=" + Genclass.data1 + " and a.doctypeid=180 and month(docdate)=" + str9.Month + "  and year(docdate)=" + str9.Year + " ";
                        Genclass.cmd = new SqlCommand(quy, conn);
                    }
                    else
                    {
                        string quy = "select a.uid,docno,docdate,name as partym,transtype,Amount,a.partyuid from MTransactions a inner join partym b on a.partyuid=b.uid where a.active=0 and a.companyid=" + Genclass.data1 + " and a.doctypeid=190 and month(docdate)=" + str9.Month + "  and year(docdate)=" + str9.Year + " ";
                        Genclass.cmd = new SqlCommand(quy, conn);
                    }
                }




                SqlDataAdapter aptr = new SqlDataAdapter(Genclass.cmd);
                DataTable tap = new DataTable();
                aptr.Fill(tap);

                this.HFGP.DefaultCellStyle.Font = new Font("Arial", 10);
                //dgv.ColumnHeadersDefaultCellStyle.Font = new Font(dgv.ColumnHeadersDefaultCellStyle.Font, FontStyle.Bold);
                this.HFGP.ColumnHeadersDefaultCellStyle.Font = new Font("Arial", 10, FontStyle.Bold);
                HFGP.AutoGenerateColumns = false;
                HFGP.Refresh();
                HFGP.DataSource = null;
                HFGP.Rows.Clear();


                HFGP.ColumnCount = tap.Columns.Count;
                Genclass.i = 0;
                foreach (DataColumn column in tap.Columns)
                {
                    HFGP.Columns[Genclass.i].Name = column.ColumnName;
                    HFGP.Columns[Genclass.i].HeaderText = column.ColumnName;
                    HFGP.Columns[Genclass.i].DataPropertyName = column.ColumnName;
                    Genclass.i = Genclass.i + 1;
                }



                HFGP.Columns[0].Visible = false;

                HFGP.Columns[1].Width = 90;
                HFGP.Columns[2].Width = 90;
                HFGP.Columns[3].Width = 450;
                HFGP.Columns[4].Width = 90;
                HFGP.Columns[5].Width = 100;
                HFGP.Columns[6].Visible = false;
                HFGP.Columns[7].Visible = false;
                HFGP.Columns[8].Visible = false;
                HFGP.Columns[9].Visible = false;
                HFGP.Columns[10].Visible = false;
                HFGP.Columns[11].Visible = false;

                HFGP.DataSource = tap;
                //int ct = tap.Rows.Count;
                HFGP.Columns[2].DefaultCellStyle.Format = "yyyy'/'MM'/'dd";



            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return;
            }
            finally
            {
                conn.Close();
                Genclass.cmd.Dispose();
            }
            textBox3.Text = "";
            totalamount();
        }
        private void totalamount()
        {
            conn.Close();
            conn.Open();
            DateTime str9 = Convert.ToDateTime(dtpfnt.Text);

            if (chkact.Checked == true)
            {
                textBox3.Text = "0";
                if (Genclass.Dtype == 180)
                {
                    Genclass.strsql = "Select  isnull(sum(a.amount),0) as totamt from Mtransactions a inner join  partym b on a.partyuid=b.uid  where a.active=1 and a.doctypeid=180  and companyid=" + Genclass.data1 + " and month(docdate)=" + str9.Month + "  and year(docdate)=" + str9.Year + " ";
                }
                else
                {
                    Genclass.strsql = "Select  isnull(sum(a.amount),0) as totamt from Mtransactions a inner join  partym b on a.partyuid=b.uid  where a.active=1 and a.doctypeid=190  and companyid=" + Genclass.data1 + "  and month(docdate)=" + str9.Month + "  and year(docdate)=" + str9.Year + " ";

                }

                Genclass.cmd = new SqlCommand(Genclass.strsql, conn);
                SqlDataAdapter aptr2 = new SqlDataAdapter(Genclass.cmd);
                DataTable tap2 = new DataTable();
                aptr2.Fill(tap2);
                textBox3.Text = tap2.Rows[0]["totamt"].ToString();
            }
            conn.Close();
        }
        private void txtname_TextChanged(object sender, System.EventArgs e)
        {

        }

        private void txtname_Click(object sender, System.EventArgs e)
        {
            Genclass.type = 1;
            loadput();
        }
        private void Titlep1()
        {
            DataGridViewCheckBoxColumn checkColumn = new DataGridViewCheckBoxColumn();
            checkColumn.Name = "CHK";
            checkColumn.HeaderText = "CHK";

            checkColumn.TrueValue = true;
            checkColumn.FalseValue = false;
            checkColumn.Width = 50;
            checkColumn.ReadOnly = false;
            checkColumn.FillWeight = 10; //if the datagridview is resized (on form resize) the checkbox won't take up too much; value is relative to the other columns' fill values
            HFIT.Columns.Add(checkColumn);
        }
        private void loadput()
        {
            conn.Close();
            conn.Open();

            Genclass.fieldone = "";
            Genclass.fieldtwo = "";
            Genclass.fieldthree = "";
            Genclass.fieldFour = "";
            Genclass.fieldFive = "";

            if (Genclass.type == 1)
            {
                Genclass.Module.Partylistviewcont("uid", "Name", Genclass.strsql, this, txtpuid, txtname, Editpan);
                //Genclass.strsql = "select uid,Name as Party from Partym where active=1 and  companyid=" + Genclass.data1 + "";
                //Genclass.strsql = " select distinct c.Uid as puid,c.name   from stransactionsp a inner join  stransactionsplist b   on  a.uid=b.transactionspuid and a.doctypeid=80 left join transactionsplist f on b.uid=f.refuid   and f.DocTypeID=110  left join  stransactionsplist g on f.Uid=g.Refuid and g.DocTypeID=40    inner join  partym c  on a.partyuid=c.uid group by a.uid,docno,b.pqty,c.Uid,c.name      having isnull(b.pqty,0)-ISNULL(SUM(f.pqty),0)-ISNULL(sum(g.pqty),0)>0";
                if (Genclass.Dtype == 180)
                {

                    Genclass.strsql = "select distinct puid,Party  from  (select distinct C.uid as Puid,c.name as Party,Convert(nvarchar,docdate,105) as Date,  Docno as BillNo,ISNULL(Netvalue,0)-ISNULL(sum(b.amount),0) As Billamount,'' as AdjustedAmount,a.uid from Stransactionsp  a left join MTransactionslist  b on a.uid=b.STRefuid inner join partym c on a.partyuid=c.uid   where  a.Doctypeid=40 and a.companyid=" + Genclass.data1 + "  group by c.uid,name,docdate,Docno,Netvalue,a.uid having isnull(Netvalue,0)<>ISNULL(sum(b.amount),0))  tab";
                }
                else
                {
                    Genclass.strsql = "select distinct puid,Party  from  (select distinct C.uid as Puid,c.name as Party,Convert(nvarchar,docdate,105) as Date,  Docno as BillNo,ISNULL(Netvalue,0)-ISNULL(sum(b.amount),0) As Billamount,'' as AdjustedAmount,a.uid from Stransactionsp  a left join MTransactionslist  b on a.uid=b.STRefuid inner join partym c on a.partyuid=c.uid   where  a.Doctypeid=100 and a.companyid=" + Genclass.data1 + "  group by c.uid,name,docdate,Docno,Netvalue,a.uid having isnull(Netvalue,0)<>ISNULL(sum(b.amount),0))  tab ";

                }
                Genclass.FSSQLSortStr = "Name";

            }
            Genclass.cmd = new SqlCommand(Genclass.strsql, conn);
            SqlDataAdapter aptr = new SqlDataAdapter(Genclass.cmd);
            DataTable tap = new DataTable();
            aptr.Fill(tap);
            Frmlookup contc = new Frmlookup();
            DataGridView dt = (DataGridView)contc.Controls["HFGP"];
            dt.Refresh();
            dt.ColumnCount = tap.Columns.Count;
            dt.Columns[0].Visible = false;

            dt.Columns[1].Width = 340;





            dt.DefaultCellStyle.Font = new Font("Arial", 10);

            dt.ColumnHeadersDefaultCellStyle.Font = new Font("Arial", 10, FontStyle.Bold);
            dt.AutoGenerateColumns = false;

            Genclass.i = 0;
            foreach (DataColumn column in tap.Columns)
            {
                dt.Columns[Genclass.i].Name = column.ColumnName;
                dt.Columns[Genclass.i].HeaderText = column.ColumnName;
                dt.Columns[Genclass.i].DataPropertyName = column.ColumnName;
                Genclass.i = Genclass.i + 1;
            }

            dt.DataSource = tap;
            contc.Show();
            conn.Close();

        }

        private void FrmMoneyReceipt_Load(object sender, System.EventArgs e)
        {
            qur.Connection = conn;
            Left = (MdiParent.ClientRectangle.Width - Width) / 3;
            Top = (MdiParent.ClientRectangle.Height - Height) / 3;
            chkact.Checked = true;
            chkedtact.Checked = true;
            if (Genclass.Dtype == 180)
            {
                label14.Text = "Money Receipt";
                this.Text = "Money Receipt";
            }
            else
            {
                label14.Text = "Money Payment";
                this.Text = "Money Payment";
            }
            this.HFIT.DefaultCellStyle.Font = new Font("Arial", 10);
            this.HFIT.ColumnHeadersDefaultCellStyle.Font = new Font("Arial", 10, FontStyle.Bold);
            string da = DateTime.Today.ToString("MMM/yyyy");
            dtpfnt.Text = da;

            DateTime str9 = Convert.ToDateTime(dtpfnt.Text);

            HFGP.RowHeadersVisible = false;
            HFIT.RowHeadersVisible = false;
            Genpan.Visible = true;
            panadd.Visible = true;
            Editpan.Visible = false;



            Genclass.Module.buttonstyleform(this);
            Genclass.Module.buttonstylepanel(panadd);
            Genclass.Module.buttonstylepanel(Editpan);
            HFGP.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            HFGP.EnableHeadersVisualStyles = false;
            HFGP.ColumnHeadersDefaultCellStyle.BackColor = SystemColors.GradientActiveCaption;
            //cboexcise.Items.Clear();
            cbrbank.Items.Clear();
            cbrdbbank.Items.Clear();
            loadbank();
            loadbankparty();

            Loadgrid();
            Genclass.sum2 = 0;
            Genclass.sum1 = 0;


            textBox4.ForeColor = Color.DarkRed;
            totalamount();

        }
        private void loadbank()
        {

            conn.Open();
            string qur = "select bankname,UId from  bank order by bankname ";
            SqlCommand cmd = new SqlCommand(qur, conn);
            SqlDataAdapter apt = new SqlDataAdapter(cmd);
            DataTable tab = new DataTable();
            apt.Fill(tab);
            cbrbank.DataSource = null;
            cbrbank.DataSource = tab;
            cbrbank.DisplayMember = "bankname";
            cbrbank.ValueMember = "uid";
            cbrbank.SelectedIndex = -1;
            conn.Close();



        }
        private void loadbankparty()
        {

            conn.Open();
            string qur = "select bankname,UId from  bank  where uid= 22 order by bankname  ";
            SqlCommand cmd = new SqlCommand(qur, conn);
            SqlDataAdapter apt = new SqlDataAdapter(cmd);
            DataTable tab = new DataTable();
            apt.Fill(tab);
            cbrdbbank.DataSource = null;
            cbrdbbank.DataSource = tab;
            cbrdbbank.DisplayMember = "bankname";
            cbrdbbank.ValueMember = "uid";
            cbrdbbank.SelectedIndex = -1;
            conn.Close();



        }
        private void button2_Click(object sender, System.EventArgs e)
        {

        }

        private void button2_Click_1(object sender, System.EventArgs e)
        {
            mode = 1;
            Genpan.Visible = false;
            Editpan.Visible = true;
            panadd.Visible = false;
            chkedtact.Checked = true;
            Genclass.Module.ClearTextBox(this, Editpan);

            Genclass.Module.Gendocno();
            txtgrn.Text = Genclass.ST;
            HFIT.Refresh();
            HFIT.DataSource = null;
            HFIT.Rows.Clear();
            txttotamt.Text = "0";
            textBox4.Enabled = false;
            txttotamt.Enabled = false;
        }

        private void button3_Click(object sender, System.EventArgs e)
        {

            Genpan.Visible = true;
            Editpan.Visible = false;
            panadd.Visible = true;

        }

        private void HFIT_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (textBox4.Text == "0")
            {
                if (HFIT.CurrentRow.Cells[4].Value.ToString() == "")
                {
                    return;
                }

            }
            if (HFIT.CurrentRow.Cells[0].Selected == true)
            {
                DataGridViewCheckBoxCell ch1 = new DataGridViewCheckBoxCell();
                ch1 = (DataGridViewCheckBoxCell)HFIT.Rows[HFIT.CurrentRow.Index].Cells[0];



                if (ch1.Value == null)
                    ch1.Value = false;
                switch (ch1.Value.ToString())
                {
                    case "True":
                        {
                            ch1.Value = false;
                            if (HFIT.CurrentRow.Cells[3].Value.ToString() != "")
                            {
                                Genclass.sum2 = Genclass.sum2 - Convert.ToDouble(HFIT.CurrentRow.Cells[3].Value.ToString());
                                HFIT.CurrentRow.Cells[4].Value = "";

                                txttotamt.Text = "";
                                Genclass.sum4 = 0;
                                for (int j = 0; j < HFIT.RowCount - 1; j++)
                                {
                                    if (HFIT.Rows[j].Cells[4].Value.ToString() != "")
                                    {
                                        Genclass.sum4 = Genclass.sum4 + Convert.ToDouble(HFIT.Rows[j].Cells[4].Value.ToString());
                                        txttotamt.Text = Convert.ToString(Genclass.sum4);
                                        textBox4.Text = txttotamt.Text;
                                    }

                                }
                            }

                            break;
                        }
                    case "False":
                        {

                            ch1.Value = true;
                            if (txtcusorderno.Text != "")
                            {
                                if (HFIT.CurrentRow.Cells[3].Value.ToString() != "")
                                {

                                    Double grdval, txtval, txtbal, txtbbval;
                                    grdval = Convert.ToDouble(HFIT.CurrentRow.Cells[3].Value.ToString());
                                    txtval = Convert.ToDouble(txtcusorderno.Text);




                                    Genclass.sum2 = Genclass.sum2 + Convert.ToDouble(grdval);
                                    txtbal = Convert.ToDouble(txtval) - Convert.ToDouble(Genclass.sum2);
                                    Double txtbal1 = grdval - (-(txtbal));
                                    //txtbal1 = -(txtbal1);
                                    txtbbval = txtbal1 - grdval;
                                    if (Genclass.sum2 < txtval)
                                    {


                                        HFIT.CurrentRow.Cells[4].Value = Convert.ToString(grdval);

                                    }

                                    else if (Genclass.sum2 > txtval && txtval != Convert.ToDouble(txttotamt.Text))
                                    {
                                        HFIT.CurrentRow.Cells[4].Value = Convert.ToString(txtbal1);
                                    }
                                    else if (grdval == Convert.ToDouble(HFIT.CurrentRow.Cells[3].Value))
                                    {
                                        HFIT.CurrentRow.Cells[4].Value = grdval;
                                    }
                                    else
                                    {
                                        //string vall9;
                                        //vall9 = txtcusorderno.Text;

                                        MessageBox.Show("Amount Exceeds");
                                        return;
                                    }
                                }

                            }
                            else
                            {
                                ch1.Value = false;
                                MessageBox.Show("Enter the amount");
                            }
                            //Where should I put the selected cell here?
                            break;
                        }
                }
                Genclass.sum1 = 0;
                for (int k = 0; k < HFIT.RowCount - 1; k++)
                {
                    if (HFIT.Rows[k].Cells[4].Value.ToString() != "")
                    {
                        Genclass.sum1 = Genclass.sum1 + Convert.ToDouble(HFIT.Rows[k].Cells[4].Value.ToString());
                        txttotamt.Text = Convert.ToString(Genclass.sum1);

                    }
                }
                Double val8, val9, val10;
                if (txtcusorderno.Text != "")
                {
                    val8 = Convert.ToDouble(txtcusorderno.Text);

                    if (txttotamt.Text != "")
                    {
                        val9 = Convert.ToDouble(txttotamt.Text);
                    }
                    else
                    {
                        txttotamt.Text = "0";
                        val9 = Convert.ToDouble(txttotamt.Text);

                    }

                    val10 = val8 - val9;
                    textBox4.Text = val10.ToString();
                }
            }
        }

        private void HFIT_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void btnsave_Click(object sender, EventArgs e)
        {
            conn.Close();
            conn.Open();


            if (cboexcise.Text == "")
            {
                MessageBox.Show("Enter the Mode of Transfer");
                cboexcise.Focus();
                return;
            }


            for (int i = 0; i < HFIT.RowCount - 1; i++)
            {
                if (txtdcno.Text == "")
                {
                    txtdcno.Text = "0";
                }

                if (mode == 1)
                {

                    if (i == 0)
                    {



                        if (cbrdbbank.SelectedValue == null || cbrbank.SelectedValue == null)
                        {
                            if (Genclass.Dtype == 180)
                            {
                                qur.CommandText = "insert into MTransactions values('" + txtgrn.Text + "','" + DTPDOCDT.Value + "','Money Receipt'," + txtpuid.Text + "," + txttotamt.Text + ",'" + cboexcise.Text + "','" + txtdcno.Text + "','" + Dtpdt.Value + "', 0,  0,1 ," + Genclass.Dtype + ")";
                                qur.ExecuteNonQuery();
                            }
                            else if (Genclass.Dtype == 190)
                            {
                                qur.CommandText = "insert into MTransactions values('" + txtgrn.Text + "','" + DTPDOCDT.Value + "','Money payment'," + txtpuid.Text + "," + txttotamt.Text + ",'" + cboexcise.Text + "','" + txtdcno.Text + "','" + Dtpdt.Value + "', 0,  0,1 ," + Genclass.Dtype + ")";
                                qur.ExecuteNonQuery();
                            }


                        }
                        else
                        {
                            if (Genclass.Dtype == 180)
                            {
                                qur.CommandText = "insert into MTransactions values('" + txtgrn.Text + "','" + DTPDOCDT.Value + "','Money Receipt'," + txtpuid.Text + "," + txttotamt.Text + ",'" + cboexcise.Text + "','" + txtdcno.Text + "','" + Dtpdt.Value + "', " + cbrbank.SelectedValue + ",  " + cbrdbbank.SelectedValue + ",1 ," + Genclass.Dtype + " )";
                                qur.ExecuteNonQuery();
                            }
                            else if (Genclass.Dtype == 190)
                            {
                                qur.CommandText = "insert into MTransactions values('" + txtgrn.Text + "','" + DTPDOCDT.Value + "','Money Payment'," + txtpuid.Text + "," + txttotamt.Text + ",'" + cboexcise.Text + "','" + txtdcno.Text + "','" + Dtpdt.Value + "', " + cbrbank.SelectedValue + ",  " + cbrdbbank.SelectedValue + ",1 ," + Genclass.Dtype + " )";
                                qur.ExecuteNonQuery();
                            }
                        }

                    }

                    string quy = "select uid from MTransactions where  docno='" + txtgrn.Text + "' and doctypeid=" + Genclass.Dtype + " ";
                    Genclass.cmd = new SqlCommand(quy, conn);
                    SqlDataAdapter aptr = new SqlDataAdapter(Genclass.cmd);
                    DataTable tap = new DataTable();
                    aptr.Fill(tap);

                    txtgrnid.Text = tap.Rows[0]["uid"].ToString();
                    if (HFIT.Rows[i].Cells[4].Value.ToString() != "")
                    {
                        qur.CommandText = "insert into MTransactionslist values(" + txtgrnid.Text + "," + HFIT.Rows[i].Cells[5].Value + "," + HFIT.Rows[i].Cells[4].Value + ")";
                        qur.ExecuteNonQuery();
                    }
                    //conn.Open();

                }


                else if (mode == 2)
                {
                    string quy = "select uid from MTransactions where  docno='" + txtgrn.Text + "' ";
                    Genclass.cmd = new SqlCommand(quy, conn);
                    SqlDataAdapter aptr = new SqlDataAdapter(Genclass.cmd);
                    DataTable tap = new DataTable();
                    aptr.Fill(tap);

                    txtgrnid.Text = tap.Rows[0]["uid"].ToString();
                    if (cbrdbbank.SelectedValue == null || cbrbank.SelectedValue == null)
                    {
                        qur.CommandText = "update MTransactions set docdate='" + DTPDOCDT.Text + "',Amount=" + txttotamt.Text + ",Transtype='" + cboexcise.Text + "',PartyUid=" + txtpuid.Text + ",Refno='" + txtdcno.Text + "',active=1,refdate='" + Dtpdt.Text + "',Bankuid=0,Tobankuid=0 where UId=" + txtgrnid.Text + "";
                        qur.ExecuteNonQuery();
                    }
                    else
                    {
                        qur.CommandText = "update MTransactions set docdate='" + DTPDOCDT.Text + "',Amount=" + txttotamt.Text + ",Transtype='" + cboexcise.Text + "',PartyUid=" + txtpuid.Text + ",Refno='" + txtdcno.Text + "',active=1,refdate='" + Dtpdt.Text + "',Bankuid=" + cbrbank.SelectedValue + ",Tobankuid='" + cbrdbbank.SelectedValue + "' where UId=" + txtgrnid.Text + "";
                        qur.ExecuteNonQuery();
                    }
                    qur.CommandText = "insert into MTransactionslist values(" + txtgrnid.Text + "," + HFIT.Rows[i].Cells[5].Value + "," + HFIT.Rows[i].Cells[4].Value + ")";
                    qur.ExecuteNonQuery();
                }
            }

            if (mode == 1)
            {
                qur.CommandText = "update doctypem set lastno= lastno + 1 where doctypeid=" + Genclass.Dtype + " and companyid=" + Genclass.data1 + "";
                qur.ExecuteNonQuery();
            }


            MessageBox.Show("Record has been saved", "Save", MessageBoxButtons.OK);


            conn.Close();
            Loadgrid();
            totalamount();
            panadd.Visible = true;
            Editpan.Visible = false;
            Genpan.Visible = true;


        }

        private void butedit_Click(object sender, System.EventArgs e)
        {
            mode = 2;
            panadd.Visible = false;
            Genpan.Visible = false;
            Editpan.Visible = true;

            int i = HFGP.SelectedCells[0].RowIndex;
            uid = HFGP.Rows[i].Cells[0].Value.ToString();
            txtgrnid.Text = HFGP.Rows[i].Cells[0].Value.ToString();
            txtgrn.Text = HFGP.Rows[i].Cells[1].Value.ToString();
            DTPDOCDT.Text = HFGP.Rows[i].Cells[2].Value.ToString();
            txtname.Text = HFGP.Rows[i].Cells[3].Value.ToString();
            txtpuid.Text = HFGP.Rows[i].Cells[11].Value.ToString();
            cboexcise.Text = HFGP.Rows[i].Cells[4].Value.ToString();
            //int sis=Convert.ToInt16(HFGP.Rows[i].Cells[5].Value);
            txttotamt.Text = HFGP.Rows[i].Cells[5].Value.ToString();

            txtdcno.Text = HFGP.Rows[i].Cells[6].Value.ToString();
            Dtpdt.Text = HFGP.Rows[i].Cells[7].Value.ToString();
            cbrbank.Text = HFGP.Rows[i].Cells[8].Value.ToString();
            cbrdbbank.Text = HFGP.Rows[i].Cells[9].Value.ToString();
            int sis = Convert.ToInt32(HFGP.Rows[i].Cells[10].Value);
            txtcusorderno.Text = sis.ToString();

            //conn.Close();
            //conn.Open();
            textBox4.Enabled = false;
            txttotamt.Enabled = false;

            HFIT.Refresh();
            HFIT.DataSource = null;
            HFIT.Rows.Clear();
            Titlep1();
            loadBill1();
            fun2();
            Double val8, val9, val10;
            val8 = Convert.ToDouble(txtcusorderno.Text);
            val9 = Convert.ToDouble(txttotamt.Text);

            val10 = val8 - val9;
            textBox4.Text = val10.ToString();



        }

        private void buttnext1_Click(object sender, System.EventArgs e)
        {
            this.Dispose();
        }

        private void txtpuid_TextChanged(object sender, System.EventArgs e)
        {
            Genclass.sum2 = 0;
            Titlep1();
            loadBill();
            fun2();
        }

        private void txtcusorderno_TextChanged(object sender, System.EventArgs e)
        {
            if (System.Text.RegularExpressions.Regex.IsMatch(txtcusorderno.Text, "[^0-9]"))
            {
                MessageBox.Show("Please enter only numbers.");
                txtcusorderno.Text = txtcusorderno.Text.Remove(txtcusorderno.Text.Length - 1);
            }
        }

        private void Dtpdt_ValueChanged(object sender, System.EventArgs e)
        {

        }

        private void label6_Click(object sender, System.EventArgs e)
        {

        }

        private void textBox4_TextChanged(object sender, System.EventArgs e)
        {

        }

        private void dtpfnt_ValueChanged(object sender, System.EventArgs e)
        {
            Loadgrid();
            totalamount();
        }

    }
}

