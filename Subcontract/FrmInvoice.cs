﻿using System;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Windows.Forms;
using System.Data.SqlClient;
using CrystalDecisions.CrystalReports.Engine;

namespace ACV
{
    public partial class FrmInvoice : Form
    {
        private CrystalDecisions.Windows.Forms.CrystalReportViewer Cryview = new CrystalDecisions.Windows.Forms.CrystalReportViewer();
        private Container components = new Container();
        ReportDocument doc = new ReportDocument();
        public FrmInvoice()
        {
            CenterToParent();
            this.BackColor = Color.White;
            InitializeComponent();
        }
        string uid = "";
        int mode = 0;
        //private static Microsoft.Office.Interop.Excel.Workbook mWorkBook;
        //private static Microsoft.Office.Interop.Excel.Sheets mWorkSheets;
        //private static Microsoft.Office.Interop.Excel.Worksheet mWSheet1;
        //private static Microsoft.Office.Interop.Excel.Application oXL;
        SqlConnection conn = new SqlConnection("Data Source=" + Genclass.data2 + "; Initial Catalog=" + Genclass.data5 + ";User id=" + Genclass.data3 + ";Password=" + Genclass.data4 + "");
        SqlCommand qur = new SqlCommand();
        private void FrmInvoice_Load(object sender, EventArgs e)
        {
            qur.Connection = conn;
            Left = (MdiParent.ClientRectangle.Width - Width) / 3;
            Top = (MdiParent.ClientRectangle.Height - Height) / 3;
            HFGP.RowHeadersVisible = false;
            HFIT.RowHeadersVisible = false;
            HFGA.RowHeadersVisible = false;
            HFG9.RowHeadersVisible = false;
            HFGT.RowHeadersVisible = false;
            HFGTAX.RowHeadersVisible = false;
            Genpan.Visible = true;
            Taxpan.Visible = false;
            Editpan.Visible = false;
            TitleAdd();
            Titleterm();
            Titlep();

            this.HFGP.DefaultCellStyle.Font = new Font("Arial", 10);
            this.HFGP.ColumnHeadersDefaultCellStyle.Font = new Font("Arial", 10, FontStyle.Bold);
            this.HFG9.DefaultCellStyle.Font = new Font("Arial", 10);
            this.HFG9.ColumnHeadersDefaultCellStyle.Font = new Font("Arial", 10, FontStyle.Bold);
            this.HFIT.DefaultCellStyle.Font = new Font("Arial", 10);
            this.HFIT.ColumnHeadersDefaultCellStyle.Font = new Font("Arial", 10, FontStyle.Bold);
            this.HFGA.DefaultCellStyle.Font = new Font("Arial", 10);
            this.HFGA.ColumnHeadersDefaultCellStyle.Font = new Font("Arial", 10, FontStyle.Bold);
            this.HFGT.DefaultCellStyle.Font = new Font("Arial", 10);
            this.HFGT.ColumnHeadersDefaultCellStyle.Font = new Font("Arial", 10, FontStyle.Bold);
            this.HFGTAX.DefaultCellStyle.Font = new Font("Arial", 10);
            this.HFGTAX.ColumnHeadersDefaultCellStyle.Font = new Font("Arial", 10, FontStyle.Bold);

            txtname.ReadOnly = true;
            txtplace.ReadOnly = true;
            txtitemname.ReadOnly = true;
            loadtax();
            chkact.Checked = true;
            txtexcise.Text = "";

            string da = DateTime.Today.ToString("MMM/yyyy");
            dtpfnt.Text = da;

            DateTime str9 = Convert.ToDateTime(dtpfnt.Text);


            chkact.Checked = true;
            Genclass.Module.buttonstyleform(this);
            Genclass.Module.buttonstylepanel(panadd);
            panadd.Visible = true;
            HFGP.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            HFGP.EnableHeadersVisualStyles = false;
            HFGP.ColumnHeadersDefaultCellStyle.BackColor = SystemColors.GradientActiveCaption;
            HFIT.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            HFIT.EnableHeadersVisualStyles = false;
            HFIT.ColumnHeadersDefaultCellStyle.BackColor = SystemColors.GradientActiveCaption;
            HFGA.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            HFGA.EnableHeadersVisualStyles = false;
            HFGA.ColumnHeadersDefaultCellStyle.BackColor = SystemColors.GradientActiveCaption;
            HFG9.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            HFG9.EnableHeadersVisualStyles = false;
            HFG9.ColumnHeadersDefaultCellStyle.BackColor = SystemColors.GradientActiveCaption;
            HFGT.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            HFGT.EnableHeadersVisualStyles = false;
            HFGT.ColumnHeadersDefaultCellStyle.BackColor = SystemColors.GradientActiveCaption;
            HFGTAX.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            HFGTAX.EnableHeadersVisualStyles = false;
            HFGTAX.ColumnHeadersDefaultCellStyle.BackColor = SystemColors.GradientActiveCaption;
            HFGP.Focus();
            Genclass.sum1 = 0;

            Titterms();
            loadterms();
            TotalAmount();


        }


        private void loadterms()
        {
            conn.Close();
            conn.Open();

            Genclass.StrSrch = "";
            Genclass.FSSQLSortStr = "Generalname";

            if (txttermset.Text != "")
            {
                if (Genclass.StrSrch == "")
                {
                    Genclass.StrSrch = Genclass.FSSQLSortStr + " like '%" + txttermset.Text + "%'";
                }
                else
                {
                    Genclass.StrSrch = Genclass.StrSrch + " and " + Genclass.FSSQLSortStr + " like '%" + txttermset.Text + "%'";
                }
            }

            if (txttermset.Text != "")
            {
                Genclass.StrSrch = " " + Genclass.StrSrch;
            }
            else
            {
                Genclass.StrSrch = "uid <> 0";
            }

            string quy = "select Generalname as Terms,uid from Generalm where active=1 and typem_uid=14 and companyid=" + Genclass.data1 + " and " + Genclass.StrSrch + "";
            Genclass.cmd = new SqlCommand(quy, conn);

            SqlDataAdapter aptr = new SqlDataAdapter(Genclass.cmd);
            DataTable tap = new DataTable();
            aptr.Fill(tap);


            HFG9.AutoGenerateColumns = false;
            HFG9.Refresh();
            HFG9.DataSource = null;
            HFG9.Rows.Clear();


            HFG9.ColumnCount = tap.Columns.Count + 1;
            Genclass.i = 1;
            foreach (DataColumn column in tap.Columns)
            {
                HFG9.Columns[Genclass.i].Name = column.ColumnName;
                HFG9.Columns[Genclass.i].HeaderText = column.ColumnName;
                HFG9.Columns[Genclass.i].DataPropertyName = column.ColumnName;
                Genclass.i = Genclass.i + 1;
            }

            HFG9.Columns[1].Width = 370;
            HFG9.Columns[2].Visible = false;
            HFG9.DataSource = tap;
        }

        private void Titterms()
        {
            DataGridViewCheckBoxColumn checkColumn = new DataGridViewCheckBoxColumn();
            checkColumn.Name = "CHK";
            checkColumn.HeaderText = "CHK";

            checkColumn.TrueValue = true;
            checkColumn.FalseValue = false;
            checkColumn.Width = 50;
            checkColumn.ReadOnly = false;
            checkColumn.FillWeight = 10; //if the datagridview is resized (on form resize) the checkbox won't take up too much; value is relative to the other columns' fill values
            HFG9.Columns.Add(checkColumn);
        }
        //private void loadexcise()
        //{
        //    conn.Open();
        //    string qur = "Select UId,GeneralName from GENERALM  where TypeM_UId =5 and active=1";
        //    SqlCommand cmd = new SqlCommand(qur, conn);
        //    SqlDataAdapter apt = new SqlDataAdapter(cmd);
        //    DataTable tab = new DataTable();
        //    apt.Fill(tab);
        //    cboexcise.DataSource = null;
        //    cboexcise.DataSource = tab;
        //    cboexcise.DisplayMember = "GeneralName";
        //    cboexcise.ValueMember = "uid";
        //    cboexcise.SelectedIndex = -1;
        //    conn.Close();

        //}


        private void loadtax()
        {

            conn.Open();
            string qur = "select a.UId,a.GeneralName from  GENERALM a inner join typem b  on a.TypeM_Uid=b.UId where a.TypeM_Uid in (4,5,6,7,8,9) and Active=1 ";
            SqlCommand cmd = new SqlCommand(qur, conn);
            SqlDataAdapter apt = new SqlDataAdapter(cmd);
            DataTable tab = new DataTable();
            apt.Fill(tab);
            //cbotax.DataSource = null;
            //cbotax.DataSource = tab;
            //cbotax.DisplayMember = "GeneralName";
            //cbotax.ValueMember = "uid";
            //cbotax.SelectedIndex = -1;
            conn.Close();



        }

        //private void loadtax()
        //{

        //    conn.Open();
        //    string qur = "select a.UId,a.GeneralName from  GENERALM a inner join typem b  on a.TypeM_Uid=b.UId where a.TypeM_Uid in (4,5,6,7,8,9) and Active=1 ";
        //    SqlCommand cmd = new SqlCommand(qur, conn);
        //    SqlDataAdapter apt = new SqlDataAdapter(cmd);
        //    DataTable tab = new DataTable();
        //    apt.Fill(tab);
        //    //cbotax.DataSource = null;
        //    //cbotax.DataSource = tab;
        //    //cbotax.DisplayMember = "GeneralName";
        //    //cbotax.ValueMember = "uid";
        //    //cbotax.SelectedIndex = -1;
        //    conn.Close();



        //}
        private void Loadgrid()
        {
            try
            {
                conn.Close();
                conn.Open();

                Genclass.StrSrch = "";


                Genclass.FSSQLSortStr = "Docno";
                Genclass.FSSQLSortStr1 = "Docdate";
                Genclass.FSSQLSortStr2 = "dcno";
                Genclass.FSSQLSortStr3 = "Dcdate";
                Genclass.FSSQLSortStr4 = "b.Name";
                Genclass.FSSQLSortStr5 = "Basicvalue";
                Genclass.FSSQLSortStr6 = "igstval";
                Genclass.FSSQLSortStr7 = "Netvalue";
                Genclass.FSSQLSortStr8 = "charges";



                if (txtscr1.Text != "")
                {
                    if (Genclass.StrSrch == "")
                    {
                        Genclass.StrSrch = Genclass.FSSQLSortStr + " like '%" + txtscr1.Text + "%'";
                    }
                    else
                    {
                        Genclass.StrSrch = Genclass.StrSrch + " and " + Genclass.FSSQLSortStr + " like '%" + txtscr1.Text + "%'";
                    }

                }

                if (Txtscr2.Text != "")
                {
                    if (Genclass.StrSrch == "")
                    {
                        Genclass.StrSrch = Genclass.FSSQLSortStr1 + " like '%" + Txtscr2.Text + "%'";
                    }
                    else
                    {
                        Genclass.StrSrch = Genclass.StrSrch + " and " + Genclass.FSSQLSortStr1 + " like '%" + Txtscr2.Text + "%'";
                    }

                }

                if (Txtscr3.Text != "")
                {
                    if (Genclass.StrSrch == "")
                    {
                        Genclass.StrSrch = Genclass.FSSQLSortStr2 + " like '%" + Txtscr3.Text + "%'";
                    }
                    else
                    {
                        Genclass.StrSrch = Genclass.StrSrch + " and " + Genclass.FSSQLSortStr2 + " like '%" + Txtscr3.Text + "%'";
                    }

                }

                if (txtscr4.Text != "")
                {
                    if (Genclass.StrSrch == "")
                    {
                        Genclass.StrSrch = Genclass.FSSQLSortStr3 + " like '%" + txtscr4.Text + "%'";
                    }
                    else
                    {
                        Genclass.StrSrch = Genclass.StrSrch + " and " + Genclass.FSSQLSortStr3 + " like '%" + txtscr4.Text + "%'";
                    }

                }

                if (txtscr5.Text != "")
                {
                    if (Genclass.StrSrch == "")
                    {
                        Genclass.StrSrch = Genclass.FSSQLSortStr4 + " like '%" + txtscr5.Text + "%'";
                    }
                    else
                    {
                        Genclass.StrSrch = Genclass.StrSrch + " and " + Genclass.FSSQLSortStr4 + " like '%" + txtscr5.Text + "%'";
                    }

                }

                if (txtscr6.Text != "")
                {
                    if (Genclass.StrSrch == "")
                    {
                        Genclass.StrSrch = Genclass.FSSQLSortStr5 + " like '%" + txtscr6.Text + "%'";
                    }
                    else
                    {
                        Genclass.StrSrch = Genclass.StrSrch + " and " + Genclass.FSSQLSortStr5 + " like '%" + txtscr6.Text + "%'";
                    }

                }
                if (txtscr7.Text != "")
                {
                    if (Genclass.StrSrch == "")
                    {
                        Genclass.StrSrch = Genclass.FSSQLSortStr6 + " like '%" + txtscr7.Text + "%'";
                    }
                    else
                    {
                        Genclass.StrSrch = Genclass.StrSrch + " and " + Genclass.FSSQLSortStr6 + " like '%" + txtscr7.Text + "%'";
                    }

                }

                if (txtscr8.Text != "")
                {
                    if (Genclass.StrSrch == "")
                    {
                        Genclass.StrSrch = Genclass.FSSQLSortStr7 + " like '%" + txtscr8.Text + "%'";
                    }
                    else
                    {
                        Genclass.StrSrch = Genclass.StrSrch + " and " + Genclass.FSSQLSortStr7 + " like '%" + txtscr8.Text + "%'";
                    }

                }


                if (textBox1.Text != "")
                {
                    if (Genclass.StrSrch == "")
                    {
                        Genclass.StrSrch = Genclass.FSSQLSortStr8 + " like '%" + textBox1.Text + "%'";
                    }
                    else
                    {
                        Genclass.StrSrch = Genclass.StrSrch + " and " + Genclass.FSSQLSortStr8 + " like '%" + textBox1.Text + "%'";
                    }

                }




                if (txtscr1.Text != "")
                {
                    Genclass.StrSrch = " " + Genclass.StrSrch;
                }
                else if (Txtscr2.Text != "")
                {
                    Genclass.StrSrch = " " + Genclass.StrSrch;
                }

                else if (Txtscr3.Text != "")
                {
                    Genclass.StrSrch = " " + Genclass.StrSrch;
                }
                else if (txtscr4.Text != "")
                {
                    Genclass.StrSrch = " " + Genclass.StrSrch;
                }
                else if (txtscr5.Text != "")
                {
                    Genclass.StrSrch = " " + Genclass.StrSrch;
                }
                else if (txtscr6.Text != "")
                {
                    Genclass.StrSrch = " " + Genclass.StrSrch;
                }
                else if (txtscr7.Text != "")
                {
                    Genclass.StrSrch = " " + Genclass.StrSrch;
                }
                else if (txtscr8.Text != "")
                {
                    Genclass.StrSrch = " " + Genclass.StrSrch;
                }
                else
                {
                    Genclass.StrSrch = "a.uid <> 0";
                }
                DateTime str9 = Convert.ToDateTime(dtpfnt.Text);


                if (Genclass.Dtype == 40)
                {
                    if (chkact.Checked == true)
                    {
                        //string quy = "Select distinct a.Uid,Docno,Convert(nvarchar,docdate,106) as DocDate,Dcno,Convert(nvarchar,dcdate,106) as Dcdate,b.Name,a.Netvalue,a.partyuid,a.placeuid,isnull(c.name,'') as Placename,dtpre,dtrem,transp,vehno,a.remarks from stransactionsp a inner join  partym b on a.partyuid=b.uid left join  partym c on a.placeuid=c.uid where a.active=1 and a.doctypeid=40 and a.companyid=" + Genclass.data1 + " and  " + Genclass.StrSrch + " and month(docdate)=" + str9.Month + "  and year(docdate)=" + str9.Year + "";
                        string quy = "Select distinct a.Uid,DocNo,convert(nvarchar,docdate,106) as  DocDate,Dcno as PONo, convert(nvarchar,docdate,106) as  POdate ,b.Name,isnull(SUM(d.basicvalue),0) as Basicvalue,isnull(e.totalcharges,0) as charges,Convert(decimal(18,2),(isnull(SUM(d.basicvalue),0)*ISNULL(i.f1,0))/100) as Taxval,a.Netvalue, a.partyuid,a.placeuid,isnull(c.name,'') as Placename, dtpre,dtrem,transp,vehno,a.remarks from stransactionsp a inner join  partym b on a.partyuid=b.uid  left join  partym c on a.placeuid=c.uid left join  stransactionsplist d on a.uid=d.transactionspuid left join STransactionsPCharges e on a.uid=e.STransactionsPUid  inner join  itemm g on d.ItemUId=g.uid left join generalm h on g.uom_uid=h.uid left join ItemGroup x on g.itemgroup_Uid=x.uid left join hsndet f  on x.hsnid=f.uid  left join generalm i on f.Sgid=i.uid  where  a.active=1 and a.doctypeid=40 and a.companyid=" + Genclass.data1 + " and  " + Genclass.StrSrch + " and month(docdate)=" + str9.Month + "  and year(docdate)=" + str9.Year + " group by a.Uid,DocNo,docdate,Dcno,b.Name,Netvalue,igstval,a.partyuid,a.placeuid,i.f1, c.name,dtpre,dtrem,transp,vehno,a.remarks,e.totalcharges";
                        Genclass.cmd = new SqlCommand(quy, conn);



                    }
                    else
                    {
                        string quy = "Select distinct a.Uid,DocNo,convert(nvarchar,docdate,106) as  DocDate,Dcno as PONo, convert(nvarchar,docdate,106) as  POdate ,b.Name,isnull(SUM(d.basicvalue),0) as Basicvalue,isnull(e.totalcharges,0) as charges,Convert(decimal(18,2),(isnull(SUM(d.basicvalue),0)*ISNULL(i.f1,0))/100) as Taxval,a.Netvalue, a.partyuid,a.placeuid,isnull(c.name,'') as Placename, dtpre,dtrem,transp,vehno,a.remarks from stransactionsp a inner join  partym b on a.partyuid=b.uid  left join  partym c on a.placeuid=c.uid left join  stransactionsplist d on a.uid=d.transactionspuid left join STransactionsPCharges e on a.uid=e.STransactionsPUid  inner join  itemm g on d.ItemUId=g.uid left join generalm h on g.uom_uid=h.uid left join ItemGroup x on g.itemgroup_Uid=x.uid left join hsndet f  on x.hsnid=f.uid  left join generalm i on f.Sgid=i.uid  where  a.active=0 and a.doctypeid=40 and a.companyid=" + Genclass.data1 + " and  " + Genclass.StrSrch + " and month(docdate)=" + str9.Month + "  and year(docdate)=" + str9.Year + " group by a.Uid,DocNo,docdate,Dcno,b.Name,Netvalue,igstval,a.partyuid,a.placeuid,i.f1 ,c.name,dtpre,dtrem,transp,vehno,a.remarks,e.totalcharges";
                        Genclass.cmd = new SqlCommand(quy, conn);

                    }

                }
                else if (Genclass.Dtype == 80)
                {
                    if (chkact.Checked == true)
                    {
                        string quy = "Select distinct a.Uid,DocNo, Convert(nvarchar,docdate,106) as  DocDate,Dcno, Convert(nvarchar,dcdate,106) as Dcdate,Name,a.Netvalue,a.partyuid from stransactionsp a inner join  partym b on a.partyuid=b.uid  where a.active=1 and a.doctypeid=80  and  " + Genclass.StrSrch + " and month(docdate)=" + str9.Month + "  and year(docdate)=" + str9.Year + "";
                        Genclass.cmd = new SqlCommand(quy, conn);

                    }
                    else
                    {
                        string quy = "Select distinct a.Uid,DocNo, Convert(nvarchar,docdate,106) as  DocDate,Dcno, Convert(nvarchar,dcdate,106) as Dcdate,Name,a.Netvalue,a.partyuid from stransactionsp a inner join  partym b on a.partyuid=b.uid  where a.active=0 and a.doctypeid=80  and  " + Genclass.StrSrch + " and month(docdate)=" + str9.Month + "  and year(docdate)=" + str9.Year + " ";
                        Genclass.cmd = new SqlCommand(quy, conn);

                    }
                }

                SqlDataAdapter aptr = new SqlDataAdapter(Genclass.cmd);
                DataTable tap = new DataTable();
                aptr.Fill(tap);




                HFGP.AutoGenerateColumns = false;
                HFGP.Refresh();
                HFGP.DataSource = null;
                HFGP.Rows.Clear();


                HFGP.ColumnCount = tap.Columns.Count;
                Genclass.i = 0;

                foreach (DataColumn column in tap.Columns)
                {
                    HFGP.Columns[Genclass.i].Name = column.ColumnName;
                    HFGP.Columns[Genclass.i].HeaderText = column.ColumnName;
                    HFGP.Columns[Genclass.i].DataPropertyName = column.ColumnName;
                    Genclass.i = Genclass.i + 1;
                }



                HFGP.Columns[0].Visible = false;

                HFGP.Columns[1].Width = 87;
                HFGP.Columns[2].Width = 91;
                //HFGP.Columns[3].Width = 89;
                //HFGP.Columns[4].Width = 91;
                HFGP.Columns[3].Visible = false;
                HFGP.Columns[4].Visible = false;
                HFGP.Columns[5].Width = 570;


                HFGP.Columns[6].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                HFGP.Columns[6].Width = 100;
                HFGP.Columns[7].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                HFGP.Columns[7].Width = 100;
                HFGP.Columns[8].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                HFGP.Columns[8].Width = 100;
                HFGP.Columns[9].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                HFGP.Columns[9].Width = 100;
                HFGP.Columns[10].Visible = false;
                HFGP.Columns[11].Visible = false;
                HFGP.Columns[12].Visible = false;
                HFGP.Columns[13].Visible = false;
                HFGP.Columns[14].Visible = false;
                HFGP.Columns[15].Visible = false;
                HFGP.Columns[16].Visible = false;
                HFGP.Columns[17].Visible = false;



                HFGP.DataSource = tap;
                HFGP.Columns[2].DefaultCellStyle.Format = "yyyy'/'MM'/'dd";
                HFGP.Columns[4].DefaultCellStyle.Format = "yyyy'/'MM'/'dd";


                //int index = HFGP.Rows.Count - 1;

                //if (HFGP.Rows[0].Cells[1].Value == "" || HFGP.Rows[0].Cells[1].Value == null)
                //{
                //    lblno1.Text = "0";
                //}
                //else
                //{
                //    lblno1.Text = "1";
                //}


                //lblno2.Text = "of " + index.ToString();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return;
            }
            finally
            {
                conn.Close();
                Genclass.cmd.Dispose();
            }
        }



        private void Loadgrid3()
        {
            try
            {
                conn.Open();

                Genclass.StrSrch = "";


                Genclass.FSSQLSortStr = "Docno";
                Genclass.FSSQLSortStr1 = "Docdate";
                Genclass.FSSQLSortStr2 = "Dcno";
                Genclass.FSSQLSortStr3 = "Dcdate";
                Genclass.FSSQLSortStr4 = "Name";
                Genclass.FSSQLSortStr5 = "Netvalue";



                if (txtscr1.Text != "")
                {
                    if (Genclass.StrSrch == "")
                    {
                        Genclass.StrSrch = Genclass.FSSQLSortStr + " like '%" + txtscr1.Text + "%'";
                    }
                    else
                    {
                        Genclass.StrSrch = Genclass.StrSrch + " and " + Genclass.FSSQLSortStr + " like '%" + txtscr1.Text + "%'";
                    }

                }

                if (Txtscr2.Text != "")
                {
                    if (Genclass.StrSrch == "")
                    {
                        Genclass.StrSrch = Genclass.FSSQLSortStr1 + " like '%" + Txtscr2.Text + "%'";
                    }
                    else
                    {
                        Genclass.StrSrch = Genclass.StrSrch + " and " + Genclass.FSSQLSortStr1 + " like '%" + Txtscr2.Text + "%'";
                    }

                }

                if (Txtscr3.Text != "")
                {
                    if (Genclass.StrSrch == "")
                    {
                        Genclass.StrSrch = Genclass.FSSQLSortStr2 + " like '%" + Txtscr3.Text + "%'";
                    }
                    else
                    {
                        Genclass.StrSrch = Genclass.StrSrch + " and " + Genclass.FSSQLSortStr2 + " like '%" + Txtscr3.Text + "%'";
                    }

                }

                if (txtscr4.Text != "")
                {
                    if (Genclass.StrSrch == "")
                    {
                        Genclass.StrSrch = Genclass.FSSQLSortStr3 + " like '%" + txtscr4.Text + "%'";
                    }
                    else
                    {
                        Genclass.StrSrch = Genclass.StrSrch + " and " + Genclass.FSSQLSortStr3 + " like '%" + txtscr4.Text + "%'";
                    }

                }

                if (txtscr5.Text != "")
                {
                    if (Genclass.StrSrch == "")
                    {
                        Genclass.StrSrch = Genclass.FSSQLSortStr4 + " like '%" + txtscr5.Text + "%'";
                    }
                    else
                    {
                        Genclass.StrSrch = Genclass.StrSrch + " and " + Genclass.FSSQLSortStr4 + " like '%" + txtscr5.Text + "%'";
                    }

                }

                if (txtscr6.Text != "")
                {
                    if (Genclass.StrSrch == "")
                    {
                        Genclass.StrSrch = Genclass.FSSQLSortStr5 + " like '%" + txtscr6.Text + "%'";
                    }
                    else
                    {
                        Genclass.StrSrch = Genclass.StrSrch + " and " + Genclass.FSSQLSortStr5 + " like '%" + txtscr6.Text + "%'";
                    }

                }





                if (txtscr1.Text != "")
                {
                    Genclass.StrSrch = " " + Genclass.StrSrch;
                }
                else if (Txtscr2.Text != "")
                {
                    Genclass.StrSrch = " " + Genclass.StrSrch;
                }

                else if (Txtscr3.Text != "")
                {
                    Genclass.StrSrch = " " + Genclass.StrSrch;
                }
                else if (txtscr4.Text != "")
                {
                    Genclass.StrSrch = " " + Genclass.StrSrch;
                }
                else if (txtscr5.Text != "")
                {
                    Genclass.StrSrch = " " + Genclass.StrSrch;
                }
                else if (txtscr6.Text != "")
                {
                    Genclass.StrSrch = " " + Genclass.StrSrch;
                }
                else
                {
                    Genclass.StrSrch = "a.uid <> 0";
                }


                DateTime str9 = Convert.ToDateTime(dtpfnt.Text);


                if (Genclass.Dtype == 40)
                {
                    if (chkact.Checked == true)
                    {
                        string quy = "Select distinct a.Uid,Docno,Convert(nvarchar,docdate,106) as DocDate,Dcno,Convert(nvarchar,dcdate,106) as Dcdate,b.Name,a.Netvalue,a.partyuid,a.placeuid,isnull(c.name,'') as Placename,dtpre,dtrem,transp,vehno,a.remarks from stransactionsp a inner join  partym b on a.partyuid=b.uid left join  partym c on a.placeuid=c.uid where a.active=1 and a.doctypeid=40 and a.companyid=" + Genclass.data1 + " and  " + Genclass.StrSrch + " and month(docdate)=" + str9.Month + "  and year(docdate)=" + str9.Year + "";

                        Genclass.cmd = new SqlCommand(quy, conn);
                    }
                    else
                    {

                        string quy = "Select distinct a.Uid,Docno,Convert(nvarchar,docdate,106) as DocDate,Dcno, Convert(nvarchar,dcdate,106) as Dcdate,b.Name,a.Netvalue,a.partyuid,a.placeuid,isnull(c.name,'') as Placename,dtpre,dtrem,transp,vehno,a.remarks from stransactionsp a inner join  partym b on a.partyuid=b.uid left join  partym c on a.placeuid=c.uid where a.active=0 and a.doctypeid=40 and a.companyid=" + Genclass.data1 + "  and  " + Genclass.StrSrch + "and month(docdate)=" + str9.Month + "  and year(docdate)=" + str9.Year + "";
                        Genclass.cmd = new SqlCommand(quy, conn);
                    }

                }
                else if (Genclass.Dtype == 80)
                {
                    if (chkact.Checked == true)
                    {
                        string quy = "Select distinct a.Uid,DocNo, Convert(nvarchar,docdate,106) as  DocDate,Dcno, Convert(nvarchar,dcdate,106) as Dcdate,Name,a.Netvalue,a.partyuid from stransactionsp a inner join  partym b on a.partyuid=b.uid  where a.active=1 and a.doctypeid=80  and  " + Genclass.StrSrch + " and month(docdate)=" + str9.Month + "  and year(docdate)=" + str9.Year + "";
                        Genclass.cmd = new SqlCommand(quy, conn);
                    }
                    else
                    {
                        string quy = "Select distinct a.Uid,DocNo, Convert(nvarchar,docdate,106) as  DocDate,Dcno, Convert(nvarchar,dcdate,106) as Dcdate,Name,a.Netvalue,a.partyuid from stransactionsp a inner join  partym b on a.partyuid=b.uid  where a.active=0 and a.doctypeid=80  and  " + Genclass.StrSrch + " and month(docdate)=" + str9.Month + "  and year(docdate)=" + str9.Year + " ";
                        Genclass.cmd = new SqlCommand(quy, conn);
                    }
                }

                SqlDataAdapter aptr = new SqlDataAdapter(Genclass.cmd);
                DataTable tap = new DataTable();
                aptr.Fill(tap);




                HFGP.AutoGenerateColumns = false;
                HFGP.Refresh();
                HFGP.DataSource = null;
                HFGP.Rows.Clear();


                HFGP.ColumnCount = tap.Columns.Count;
                Genclass.i = 0;

                foreach (DataColumn column in tap.Columns)
                {
                    HFGP.Columns[Genclass.i].Name = column.ColumnName;
                    HFGP.Columns[Genclass.i].HeaderText = column.ColumnName;
                    HFGP.Columns[Genclass.i].DataPropertyName = column.ColumnName;
                    Genclass.i = Genclass.i + 1;
                }



                HFGP.Columns[0].Visible = false;

                HFGP.Columns[1].Width = 87;
                HFGP.Columns[2].Width = 89;
                HFGP.Columns[3].Width = 89;
                HFGP.Columns[4].Width = 89;
                HFGP.Columns[5].Width = 429;
                HFGP.Columns[6].Width = 100;
                HFGP.Columns[7].Visible = false;
                HFGP.Columns[8].Visible = false;
                HFGP.Columns[9].Visible = false;
                HFGP.Columns[10].Visible = false;
                HFGP.Columns[11].Visible = false;
                HFGP.Columns[12].Visible = false;
                HFGP.Columns[13].Visible = false;
                HFGP.Columns[14].Visible = false;



                HFGP.DataSource = tap;
                HFGP.Columns[2].DefaultCellStyle.Format = "yyyy'/'MM'/'dd";
                HFGP.Columns[4].DefaultCellStyle.Format = "yyyy'/'MM'/'dd";


                int index = HFGP.Rows.Count - 1;

                if (HFGP.Rows[0].Cells[1].Value.ToString() == "" || HFGP.Rows[0].Cells[1].Value == null)
                {
                    lblno1.Text = "0";
                }
                else
                {
                    lblno1.Text = "1";
                }


                lblno2.Text = "of " + index.ToString();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return;
            }
            finally
            {
                conn.Close();
                Genclass.cmd.Dispose();
            }
        }


        private void Titlep()
        {
            HFIT.ColumnCount = 15;

            HFIT.Columns[0].Name = "Itemname";
            HFIT.Columns[1].Name = "UoM";

            HFIT.Columns[2].Name = "Price";
            HFIT.Columns[3].Name = "Qty";
            HFIT.Columns[4].Name = "Value";
            HFIT.Columns[5].Name = "narration";
            HFIT.Columns[6].Name = "Itemuid";
            HFIT.Columns[7].Name = "GST%";
            HFIT.Columns[8].Name = "GSTval";
            HFIT.Columns[9].Name = "D.A.No";
            HFIT.Columns[10].Name = "Refuid";
            HFIT.Columns[11].Name = "Hsnuid";
            HFIT.Columns[12].Name = "Total";
            HFIT.Columns[13].Name = "Pono";
            HFIT.Columns[14].Name = "Podate";



            HFIT.Columns[0].Width = 364;
            HFIT.Columns[1].Width = 60;
            HFIT.Columns[2].Width = 70;
            HFIT.Columns[3].Width = 80;

            HFIT.Columns[4].Width = 100;

            HFIT.Columns[6].Visible = false;
            HFIT.Columns[5].Visible = false;

            HFIT.Columns[7].Visible = false;
            HFIT.Columns[8].Visible = false;

            HFIT.Columns[9].Visible = false;
            HFIT.Columns[10].Visible = false;
            HFIT.Columns[11].Visible = false;
            HFIT.Columns[12].Visible = false;

            //if (mode == 1)
            //{

            HFIT.Columns[13].Visible = true;
            HFIT.Columns[14].Visible = true;
            HFIT.Columns[13].Width = 140;

            HFIT.Columns[14].Width = 100;
            //}
            //else
            //{
            //    HFIT.Columns[13].Visible = false;
            //    HFIT.Columns[14].Visible = false;
            //}
        }

        private void Titletax()
        {
            HFGTAX.ColumnCount = 6;
            HFGTAX.Columns[0].Name = "Taxable Value";
            HFGTAX.Columns[1].Name = "SGST%";
            HFGTAX.Columns[2].Name = "SGST Value";
            HFGTAX.Columns[3].Name = "CGST%";
            HFGTAX.Columns[4].Name = "CGST Value";
            HFGTAX.Columns[0].Width = 100;
            HFGTAX.Columns[1].Width = 60;
            HFGTAX.Columns[2].Width = 100;
            HFGTAX.Columns[3].Width = 60;
            HFGTAX.Columns[4].Width = 100;
            HFGTAX.Columns[5].Name = "hsnid";
            HFGTAX.Columns[5].Visible = false;
        }

        private void Loadgrid1()
        {
            try
            {
                conn.Open();

                string quy = "select c.itemname,b.pqty,b.itemuid,refuid from transactionsp a inner join transactionsplist b on a.uid=b.transactionspuid and a.companyid=" + Genclass.data1 + " inner join itemm c on b.itemuid=c.uid where a.uid=" + uid + "";


                Genclass.cmd = new SqlCommand(quy, conn);



                SqlDataAdapter aptr = new SqlDataAdapter(Genclass.cmd);
                DataTable tap = new DataTable();
                aptr.Fill(tap);

                this.HFIT.DefaultCellStyle.Font = new Font("Arial", 10);
                //dgv.ColumnHeadersDefaultCellStyle.Font = new Font(dgv.ColumnHeadersDefaultCellStyle.Font, FontStyle.Bold);
                this.HFIT.ColumnHeadersDefaultCellStyle.Font = new Font("Arial", 10, FontStyle.Bold);
                HFIT.AutoGenerateColumns = false;
                HFIT.Refresh();
                HFIT.DataSource = null;
                HFIT.Rows.Clear();


                HFIT.ColumnCount = tap.Columns.Count;
                Genclass.i = 0;
                foreach (DataColumn column in tap.Columns)
                {
                    HFIT.Columns[Genclass.i].Name = column.ColumnName;
                    HFIT.Columns[Genclass.i].HeaderText = column.ColumnName;
                    HFIT.Columns[Genclass.i].DataPropertyName = column.ColumnName;
                    Genclass.i = Genclass.i + 1;
                }



                HFIT.Columns[0].Width = 450;

                HFIT.Columns[1].Width = 75;
                HFIT.Columns[2].Visible = false;
                HFIT.Columns[3].Visible = false;

                //HFIT.Columns[5].Width = 400;




                HFIT.DataSource = tap;

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return;
            }
            finally
            {
                conn.Close();
                Genclass.cmd.Dispose();
            }
        }
        private void button1_Click(object sender, System.EventArgs e)
        {

        }

        private void loadput()
        {
            conn.Close();
            conn.Open();

            if (Genclass.type == 1)
            {
                if (Genclass.Gbtxtid == 130)
                {
                    Genclass.Module.Partylistviewcont1("uid", "Name", "GSTIN", "Address1", "State", Genclass.strsql, this, txtpuid, txtname, txttempadd1, txtgst, txtbuid, Editpan);
                    //Genclass.Module.Partylistviewcont2("uid", "Name", "Address1", "city", Genclass.strsql, this, txtpuid, txtname, txttempadd1, txtgst, Editpan);
                    //Genclass.strsql = "select top 25 uid,Name as Party,Address1  from Partym where active=1 and ptype<>1  and  companyid=" + Genclass.data1 + "";
                    //Genclass.strsql = "select distinct  c.uid,c.name from transactionsp a inner join transactionsplist b on  a.uid=b.transactionspuid and a.doctypeid=20 and a.companyid=" + Genclass.data1 + " inner join partym c on a.partyuid=c.uid left join stransactionsplist e on  b.uid=e.refuid and e.doctypeid=40 group by c.uid,c.name,b.pqty  having b.pqty-isnull(sum(e.pqty),0) >0";
                    Genclass.strsql = "select top 25 a.uid,Name as Party,tngst as GSTIN,Address1,city,generalname as State from Partym a inner join generalm b on a.stateuid=b.uid where a.active=1 and ptype<>1  and  a.companyid=" + Genclass.data1 + "  order by name ";

                    Genclass.FSSQLSortStr = "Name";

                }


            }

            else if (Genclass.type == 2)
            {
                Genclass.Module.Partylistviewcont("uid", "Docno", Genclass.strsql, this, txtdcid, txtdcno, Editpan);
                Genclass.strsql = "select distinct  a.uid,a.docno as Dcno from transactionsp a inner join transactionsplist b on  a.uid=b.transactionspuid and a.doctypeid=20 and a.companyid=" + Genclass.data1 + " inner join partym c on a.partyuid=c.uid   left join stransactionsplist e on  b.uid=e.refuid and e.doctypeid=40 group by a.uid,a.docno,b.pqty  having b.pqty-isnull(sum(e.pqty),0) >0";
                Genclass.FSSQLSortStr = "docno";
            }

            else if (Genclass.type == 3)
            {
                Genclass.Module.Partylistviewcont("uid", "AddtionalCharges", Genclass.strsql, this, txtaddid, txtaddcharge, addipan);
                Genclass.strsql = "select Uid ,GeneralName   from Generalm where TypeM_UId =13 and active=1 and companyid==" + Genclass.data1 + " ";
                Genclass.FSSQLSortStr = "GeneralName";
            }
            else if (Genclass.type == 4)
            {
                Genclass.Module.Partylistviewcont("uid", "Terms", Genclass.strsql, this, txttermid, txtterms, termspan);
                Genclass.strsql = "select Uid ,GeneralName   from Generalm where TypeM_UId =14 and active=1 and companyid==" + Genclass.data1 + " ";
                Genclass.FSSQLSortStr = "GeneralName";
            }

            else if (Genclass.type == 5)
            {

                //Genclass.Module.Partylistviewcont3("uid", "Item","Itemcode", Genclass.strsql, this, txttitemid, txtitemname,txtdcid, Editpan);

                //Genclass.strsql = "select distinct b.uid,itemname,price from pur_price_list a inner join itemm b on a.itemuid=b.uid where a.suppuid=" + txtpuid.Text + " and b.active=1 and a.companyid=1 and a.Eff_to is null";
                if (Genclass.data1 == 1)
                {
                    Genclass.Module.Partylistviewcont("uid", "Item", Genclass.strsql, this, txttitemid, txtitemname, Editpan);
                    Genclass.strsql = "select distinct uid,itemname,itemcode FROM itemm  where  active=1 and companyid=" + Genclass.data1 + "";
                    Genclass.FSSQLSortStr = "itemname";

                }
                else
                {
                    Genclass.Module.Partylistviewcont2("uid", "Itemcode", "itemname", "Price", Genclass.strsql, this, txttitemid, txtdcid, txtitemname, txtprice, Editpan);
                    Genclass.strsql = "select distinct a.uid,itemcode,itemname,b.price FROM itemm a  inner join pur_price_list b on a.uid=b.itemuid  where  a.active=1 and a.companyid=" + Genclass.data1 + " and suppuid=" + txtpuid.Text + " and eff_to is null ";
                    Genclass.FSSQLSortStr = "itemcode";
                    Genclass.FSSQLSortStr1 = "itemname";
                }

            }
            else if (Genclass.type == 6)
            {
                Genclass.Module.Partylistviewcont("uid", "Name", Genclass.strsql, this, txtpuid, txtname, Editpan);
                Genclass.strsql = "select top 50 uid,Name as Party from Partym where active=1 and ptype<>1 and  companyid=" + Genclass.data1 + " ";
                //Genclass.strsql = "    select distinct b.uid,name from pur_price_list a inner join partym b on a.Suppuid=b.uid where  active=1 and a.companyid=1 and a.Eff_to is null";

                Genclass.FSSQLSortStr = "Name";
            }

            else if (Genclass.type == 7)
            {
                Genclass.Module.Partylistviewcont3("uid", "Name", "Address1", Genclass.strsql, this, txtpluid, txtplace, txttempadd2, Editpan);
                Genclass.strsql = "select top 25 uid,Name as Party,Address1  from Partym where active=1 and ptype<>1  and  companyid=" + Genclass.data1 + "";

                Genclass.FSSQLSortStr = "Name";
            }
            else if (Genclass.type == 8)
            {
                Genclass.Module.Partylistviewcont3("uid", "docno", "docdate", Genclass.strsql, this, txtdcid, txttrans, txtite, Editpan);
                Genclass.strsql = "select distinct Tuid,docno,docdate  from ( select m.uid as Puid,m.name,m.address1,Docno, convert(varchar,Docdate,105) as Docdate,c.hsnid,c.itemname,c.uid,a.uid as Tuid, b.uid as listuid,b.pqty-isnull(sum(z.pqty),0) as qty,b.PRate as  Price,b.BasicValue,i.f1  as gstper,convert(decimal(18,2),(b.BasicValue /100 * i.f1),105) as gstval from   stransactionsp a   inner join stransactionsplist b on a.UId=b.TransactionsPUId and b.doctypeid=80  LEFT join stransactionsplist z on b.Uid=z.Refuid and  z.doctypeid=40   inner join   itemm c on b.ItemUId=c.uid  left join   ItemGroup j on c.itemgroup_Uid=j.UId left join Hsndet f on j.hsnid=f.uid left join   generalm i   on f.sgid=i.uid inner join partym m on a.partyuid=m.uid  where m.uid=" + txtpuid.Text + " group by m.uid,m.name,m.address1,Docno,    Docdate,c.hsnid,c.itemname,c.uid,i.f1,b.pqty,b.PRate,b.BasicValue,i.f1,  a.uid,b.uid  having    b.pqty-isnull(sum(z.pqty),0)>0)tab";

                Genclass.FSSQLSortStr = "Name";
            }
            else if (Genclass.type == 9)
            {
                Genclass.Module.Partylistviewcont("uid", "Terms", Genclass.strsql, this, txttermid, txtterms, termspan);
                Genclass.strsql = "";

                Genclass.FSSQLSortStr = "Name";
            }
            Genclass.cmd = new SqlCommand(Genclass.strsql, conn);
            SqlDataAdapter aptr = new SqlDataAdapter(Genclass.cmd);
            DataTable tap = new DataTable();
            aptr.Fill(tap);

            Frmlookup contc = new Frmlookup();

            //TabControl tab = (TabControl)contc.Controls["tapc"];
            DataGridView dt = (DataGridView)contc.Controls["Hfgp"];
            dt.Refresh();
            dt.ColumnCount = tap.Columns.Count;



            dt.DefaultCellStyle.Font = new Font("Arial", 10);

            dt.ColumnHeadersDefaultCellStyle.Font = new Font("Arial", 10, FontStyle.Bold);
            dt.AutoGenerateColumns = false;

            Genclass.i = 0;
            foreach (DataColumn column in tap.Columns)
            {
                dt.Columns[Genclass.i].Name = column.ColumnName;
                dt.Columns[Genclass.i].HeaderText = column.ColumnName;
                dt.Columns[Genclass.i].DataPropertyName = column.ColumnName;
                Genclass.i = Genclass.i + 1;
            }

            dt.DataSource = tap;

            dt.Columns[0].Visible = false;
            dt.Columns[1].Width = 350;

            if (Genclass.type == 7 || Genclass.type == 5)
            {

                dt.Columns[2].Width = 250;

            }
            else if (Genclass.type == 1)
            {
                dt.Columns[2].Width = 150;
                dt.Columns[3].Width = 250;
                dt.Columns[4].Width = 150;
                dt.Columns[5].Width = 150;
                //dt.Columns[6].Width = 80;
            }
            else
            {
                dt.Columns[2].Visible = false;
            }
            contc.StartPosition = FormStartPosition.CenterScreen;
            contc.MdiParent = this.MdiParent;
            contc.Show();
            conn.Close();
        }


        //private void HFIT_CellEnter(object sender, DataGridViewCellEventArgs e)
        //{
        //    int i = HFIT.SelectedCells[0].RowIndex;
        //    HFIT.Rows[i].Cells[4].Value = Convert.ToDouble(HFIT.Rows[i].Cells[2].Value) * Convert.ToDouble(HFIT.Rows[i].Cells[3].Value);
        //}

        //private void HFIT_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        //{
        //    if (HFIT.RowCount - 2 > 0)
        //    {
        //        Genclass.sum = 0;
        //        txtamt.Text = null;
        //        txtamttot.Text = null;
        //        if (HFIT.RowCount > 2)
        //        {
        //            for (int i = 0; i < HFIT.RowCount - 1; i++)
        //            {
        //                if (HFIT.Rows[i].Cells[4].Value.ToString() == "" || HFIT.Rows[i].Cells[4].Value.ToString() == null)
        //                {
        //                    return;
        //                }

        //                else
        //                {
        //                    //int sum1 = (int.TryParse(HFIT.Rows[i].Cells[2].Value.ToString(), out quantity) && int.TryParse(HFIT.Rows[i].Cells[4].Value.ToString(), out rate));
        //                    Genclass.sum = Genclass.sum + Convert.ToInt16(HFIT.Rows[i].Cells[4].Value.ToString());

        //                    txtamttot.Text = Genclass.sum.ToString();
        //                    txtamt.Text = txtamttot.Text;
        //                }
        //            }
        //        }
        //    }
        //}

        private void loadput1()
        {
            conn.Close();
            conn.Open();
            //txttitemid.Text = "";
            txtprice.Text = "";

            if (Genclass.type == 5)
            {

                //Genclass.Module.Partylistviewcont3("uid", "Item","Itemcode", Genclass.strsql, this, txttitemid, txtitemname,txtdcid, Editpan);

                //Genclass.strsql = "select distinct b.uid,itemname,price from pur_price_list a inner join itemm b on a.itemuid=b.uid where a.suppuid=" + txtpuid.Text + " and b.active=1 and a.companyid=1 and a.Eff_to is null";
                if (Genclass.data1 == 1)
                {
                    Genclass.Module.Partylistviewcont1("uid", "itemname", "itemcode", "listid", "id", Genclass.strsql, this, txttitemid, txtitemname, txtitemcode, txtsaleorderno, txtsalesdate, Editpan);

                    //Genclass.strsql = "select uid,itemname ,itemcode from (select a.UId,a.DocNo,isnull(b.PQty,0)-isnull(c.PQty,0)-isnull(d.PQty,0) as balqty,f.ItemName,f.ItemCode from sTransactionsP a inner join sTransactionsPList b on a.UId=b.TransactionsPUId   and b.DocTypeID = 80   left join Stransactionsplist  d on b.uid=  d.Refuid  and d.doctypeid=40 left join  TransactionsPList c on b.uid=c.Refuid  and c.DocTypeID=20  left join Stransactionsplist e on c.Uid=e.refuid and e.doctypeid=40  left join itemm f on b.ItemUId=f.Uid group  by   a.uid,a.docno,b.pqty,d.pqty,c.pqty,f.ItemName,f.ItemCode having isnull(b.PQty,0)-isnull(c.PQty,0)-isnull(d.PQty,0) >0 ) tab";
                    Genclass.strsql = "select itemid,itemname ,itemcode,listid,'' as id from (select a.UId,a.DocNo,a.docdate,isnull(b.PQty,0)-isnull(c.PQty,0)-isnull(e.PQty,0)  as  balqty,f.ItemName,f.ItemCode,f.uid as itemid,b.uid as listid from sTransactionsP a inner join sTransactionsPList b on a.UId=b.TransactionsPUId     and b.DocTypeID = 80   left join Stransactionsplist  d on b.uid=  d.Refuid  and d.doctypeid=40 left join    TransactionsPList c    on b.uid=c.Refuid  and c.DocTypeID=110  left join Stransactionsplist e on c.Uid=e.refuid    and e.doctypeid=40  left join     itemm f on c.ItemUId=f.Uid  left join transactionsp  g on c.transactionspuid=g.uid left join partym h on g.partyuid=h.uid  where f.itemname is not null   and f.companyid=1  and h.uid=" + txtpuid.Text + "     group  by   a.uid,a.docno,b.pqty,e.pqty,c.pqty,f.ItemName,a.docdate,f.ItemCode,f.uid,b.uid having isnull(b.PQty,0)-isnull(c.PQty,0)-isnull(e.PQty,0) >0 ) tab     ";
                    Genclass.FSSQLSortStr = "itemname";
                    Genclass.FSSQLSortStr1 = "itemcode";

                }
                else
                {
                    Genclass.Module.Partylistviewcont2("uid", "Itemcode", "itemname", "listid", Genclass.strsql, this, txttitemid, txtdcid, txtitemname, txtlisid, Editpan);
                    Genclass.strsql = "select itemid,itemcode,itemname ,listid,'' as id from (select a.UId,a.DocNo,a.docdate,isnull(b.PQty,0)-isnull(c.PQty,0)-isnull(e.PQty,0)  as  balqty,f.ItemName,f.ItemCode,f.uid as itemid,b.uid as listid from sTransactionsP a inner join sTransactionsPList b on a.UId=b.TransactionsPUId     and b.DocTypeID = 80   left join Stransactionsplist  d on b.uid=  d.Refuid  and d.doctypeid=40 left join    TransactionsPList c    on b.uid=c.Refuid  and c.DocTypeID=110  left join Stransactionsplist e on c.Uid=e.refuid    and e.doctypeid=40  left join     itemm f on c.ItemUId=f.Uid  left join transactionsp  g on c.transactionspuid=g.uid left join partym h on g.partyuid=h.uid  where f.itemname is not null   and f.companyid=3  and h.uid=" + txtpuid.Text + "     group  by   a.uid,a.docno,b.pqty,e.pqty,c.pqty,f.ItemName,a.docdate,f.ItemCode,f.uid,b.uid having isnull(b.PQty,0)-isnull(c.PQty,0)-isnull(e.PQty,0) >0 ) tab ";

                    Genclass.FSSQLSortStr = "itemcode";

                }
                Genclass.cmd = new SqlCommand(Genclass.strsql, conn);
                SqlDataAdapter aptr = new SqlDataAdapter(Genclass.cmd);
                DataTable tap = new DataTable();
                aptr.Fill(tap);


                FrmlookupItem contc = new FrmlookupItem();
                TabControl tab = (TabControl)contc.Controls["tabC"];


                TabPage tab1 = (TabPage)tab.Controls["tabPage1"];

                DataGridView grid = (DataGridView)tab1.Controls["hfgp"];
                grid.Refresh();

                grid.ColumnCount = tap.Columns.Count;

                if (Genclass.data1 == 1)
                {
                    grid.Columns[0].Visible = false;
                    grid.Columns[1].Width = 300;


                    grid.Columns[2].Width = 130;
                    grid.Columns[3].Visible = false;
                    grid.Columns[4].Visible = false;
                }

                else

                {
                    grid.Columns[0].Visible = false;
                    grid.Columns[1].Width = 130;


                    grid.Columns[2].Width = 300;
                    grid.Columns[3].Visible = false;
                }




                grid.DefaultCellStyle.Font = new Font("Arial", 10);

                grid.ColumnHeadersDefaultCellStyle.Font = new Font("Arial", 10, FontStyle.Bold);
                grid.AutoGenerateColumns = false;

                Genclass.i = 0;
                foreach (DataColumn column in tap.Columns)
                {
                    grid.Columns[Genclass.i].Name = column.ColumnName;
                    grid.Columns[Genclass.i].HeaderText = column.ColumnName;
                    grid.Columns[Genclass.i].DataPropertyName = column.ColumnName;
                    Genclass.i = Genclass.i + 1;
                }

                grid.DataSource = tap;





                TabPage tab2 = (TabPage)tab.Controls["tabPage2"];
                DataGridView grid1 = (DataGridView)tab2.Controls["HFGP4"];
                //grid1.RowHeadersVisible = false;
                grid1.DataSource = null;
                grid1.DefaultCellStyle.Font = new Font("Arial", 10);

                grid1.ColumnHeadersDefaultCellStyle.Font = new Font("Arial", 10, FontStyle.Bold);

                //DataGridViewCheckBoxColumn checkColumn = new DataGridViewCheckBoxColumn();
                //checkColumn.Name = "CHK";
                //checkColumn.HeaderText = "CHK";

                //checkColumn.TrueValue = true;
                //checkColumn.FalseValue = false;
                //checkColumn.Width = 50;
                //checkColumn.ReadOnly = false;
                //checkColumn.FillWeight = 10; //if the datagridview is resized (on form resize) the checkbox won't take up too much; value is relative to the other columns' fill values
                //grid1.Columns.Add(checkColumn);

                if (Genclass.data1 == 1)
                {
                    //Genclass.Module.Partylistviewcont1("uid", "itemname", "itemcode", "docno", "docdate", Genclass.strsql, this, txttitemid, txtitemname, txtitemcode, txtsaleorderno, txtsalesdate, Editpan);
                    //Genclass.strsql = "select itemid,itemname,itemcode,docno,docdate from (select a.UId,a.DocNo,a.docdate,isnull(b.PQty,0)-isnull(c.PQty,0)-isnull(d.PQty,0) as balqty,f.uid as itemid,f.ItemName,f.ItemCode,b.uid as listid from sTransactionsP a inner join sTransactionsPList b on a.UId=b.TransactionsPUId   and b.DocTypeID = 80    left join Stransactionsplist  d on b.uid=  d.Refuid  and d.doctypeid=40 left join  TransactionsPList c on b.uid=c.Refuid  and c.DocTypeID=20 left join Stransactionsplist e on c.Uid=e.refuid and e.doctypeid=40  left join itemm f on b.ItemUId=f.Uid left join partym h on a.partyuid=h.uid  where a.partyuid=" + txtpuid.Text + " and a.companyid=" + Genclass.data1 + "  group  by  a.uid,a.docno,b.pqty,d.pqty,c.pqty,f.ItemName,f.ItemCode,a.docdate,f.uid,b.uid having isnull(b.PQty,0)-isnull(c.PQty,0)-isnull(d.PQty,0) >0 ) tab";

                    //if (Genclass.STR == "")
                    //{
                    Genclass.strsql = "select distinct itemname,itemcode,Pono,convert(date,Podate,102) as Podate,Qty,listid,itemid,0 as BQty  from (select aa.*,bb.reqdate as Podate from (select a.UId,a.transp as Pono,isnull(b.PQty,0)-isnull(c.PQty,0)-isnull(d.PQty,0) as qty,f.uid as itemid,f.ItemName,f.ItemCode,b.uid as listid from sTransactionsP a inner join sTransactionsPList b on a.UId=b.TransactionsPUId   and b.DocTypeID = 80    left join Stransactionsplist  d on b.uid=  d.Refuid  and d.doctypeid=40 left join  TransactionsPList c on b.uid=c.Refuid  and c.DocTypeID=20 left join Stransactionsplist e on c.Uid=e.refuid and e.doctypeid=40  left join itemm f on b.ItemUId=f.Uid left join ordreqqty o on a.uid=o.tpuid where a.partyuid=" + txtpuid.Text + " and a.companyid=" + Genclass.data1 + "  group  by  a.uid,a.transp,b.pqty,d.pqty,c.pqty,f.ItemName,f.ItemCode,f.uid,b.uid having isnull(b.PQty,0)-isnull(c.PQty,0)-isnull(d.PQty,0) >0) aa left join  (select c.tpuid,c.itid,c.reqdate from STransactionsP a inner join STransactionsPlist b on a.uid=b.TransactionsPUId and a.Doctypeid=80 and a.companyid=3 left join Ordreqqty c on a.uid=c.Tpuid and b.ItemUId=c.itid)bb on aa.uid=bb.tpuid and aa.itemid=bb.itid ) tab order by Podate";
                    //}
                    //else
                    //{

                    //    Genclass.strsql = "select distinct itemname,itemcode,Pono,convert(date,Podate,102) as Podate,Qty,listid,itemid,0 as BQty from (select aa.*,bb.reqdate as Podate from (select a.UId,a.transp as Pono,isnull(b.PQty,0)-isnull(c.PQty,0)-isnull(d.PQty,0) as qty,f.uid as itemid,f.ItemName,f.ItemCode,b.uid as listid from sTransactionsP a inner join sTransactionsPList b on a.UId=b.TransactionsPUId   and b.DocTypeID = 80    left join Stransactionsplist  d on b.uid=  d.Refuid  and d.doctypeid=40 left join  TransactionsPList c on b.uid=c.Refuid  and c.DocTypeID=20 left join Stransactionsplist e on c.Uid=e.refuid and e.doctypeid=40  left join itemm f on b.ItemUId=f.Uid left join ordreqqty o on a.uid=o.tpuid where a.partyuid=" + txtpuid.Text + " and a.companyid=" + Genclass.data1 + " and b.uid not in (" + Genclass.STR + ")  group  by  a.uid,a.transp,b.pqty,d.pqty,c.pqty,f.ItemName,f.ItemCode,f.uid,b.uid having isnull(b.PQty,0)-isnull(c.PQty,0)-isnull(d.PQty,0) >0) aa left join  (select c.tpuid,c.itid,c.reqdate from STransactionsP a inner join STransactionsPlist b on a.uid=b.TransactionsPUId and a.Doctypeid=80 and a.companyid=3 left join Ordreqqty c on a.uid=c.Tpuid and b.ItemUId=c.itid)bb on aa.uid=bb.tpuid and aa.itemid=bb.itid ) tab order by Podate";
                    //}
                    Genclass.FSSQLSortStr = "itemname";
                    Genclass.FSSQLSortStr1 = "itemcode";
                }

                else
                {

                    //if (Genclass.STR == "")
                    //{
                    Genclass.strsql = "select distinct itemcode,itemname,Pono,convert(date,Podate,102) as Podate,Qty,listid,itemid,0 as BQty  from (select aa.*,bb.reqdate as Podate from (select a.UId,a.transp as Pono,isnull(b.PQty,0)-isnull(c.PQty,0)-isnull(d.PQty,0) as qty,f.uid as itemid,f.ItemName,f.ItemCode,b.uid as listid from sTransactionsP a inner join sTransactionsPList b on a.UId=b.TransactionsPUId   and b.DocTypeID = 80    left join Stransactionsplist  d on b.uid=  d.Refuid  and d.doctypeid=40 left join  TransactionsPList c on b.uid=c.Refuid  and c.DocTypeID=20 left join Stransactionsplist e on c.Uid=e.refuid and e.doctypeid=40  left join itemm f on b.ItemUId=f.Uid left join ordreqqty o on a.uid=o.tpuid where a.partyuid=" + txtpuid.Text + " and a.companyid=" + Genclass.data1 + "  group  by  a.uid,a.transp,b.pqty,d.pqty,c.pqty,f.ItemName,f.ItemCode,f.uid,b.uid having isnull(b.PQty,0)-isnull(c.PQty,0)-isnull(d.PQty,0) >0) aa left join  (select c.tpuid,c.itid,c.reqdate from STransactionsP a inner join STransactionsPlist b on a.uid=b.TransactionsPUId and a.Doctypeid=80 and a.companyid=3 left join Ordreqqty c on a.uid=c.Tpuid and b.ItemUId=c.itid)bb on aa.uid=bb.tpuid and aa.itemid=bb.itid ) tab order by Podate";
                    //}
                    //else
                    //{

                    //    Genclass.strsql = "select distinct itemcode,itemname,Pono,convert(date,Podate,102) as Podate,Qty,listid,itemid,0 as BQty from (select aa.*,bb.reqdate as Podate from (select a.UId,a.transp as Pono,isnull(b.PQty,0)-isnull(c.PQty,0)-isnull(d.PQty,0) as qty,f.uid as itemid,f.ItemName,f.ItemCode,b.uid as listid from sTransactionsP a inner join sTransactionsPList b on a.UId=b.TransactionsPUId   and b.DocTypeID = 80    left join Stransactionsplist  d on b.uid=  d.Refuid  and d.doctypeid=40 left join  TransactionsPList c on b.uid=c.Refuid  and c.DocTypeID=20 left join Stransactionsplist e on c.Uid=e.refuid and e.doctypeid=40  left join itemm f on b.ItemUId=f.Uid left join ordreqqty o on a.uid=o.tpuid where a.partyuid=" + txtpuid.Text + " and a.companyid=" + Genclass.data1 + " and b.uid not in (" + Genclass.STR + ")  group  by  a.uid,a.transp,b.pqty,d.pqty,c.pqty,f.ItemName,f.ItemCode,f.uid,b.uid having isnull(b.PQty,0)-isnull(c.PQty,0)-isnull(d.PQty,0) >0) aa inner join  (select c.tpuid,c.itid,c.reqdate from STransactionsP a inner join STransactionsPlist b on a.uid=b.TransactionsPUId and a.Doctypeid=80 and a.companyid=3 left join Ordreqqty c on a.uid=c.Tpuid and b.ItemUId=c.itid)bb on aa.uid=bb.tpuid and aa.itemid=bb.itid ) tab order by Podate";
                    //}
                    Genclass.FSSQLSortStr = "itemcode";
                    Genclass.FSSQLSortStr9 = "f.itemname";
                }



                Genclass.cmd = new SqlCommand(Genclass.strsql, conn);
                SqlDataAdapter aptr1 = new SqlDataAdapter(Genclass.cmd);
                DataTable tap1 = new DataTable();
                aptr1.Fill(tap1);


                grid1.Refresh();
                grid1.DataSource = null;
                grid1.Rows.Clear();

                grid1.ColumnCount = tap1.Columns.Count;

                Genclass.i = 0;
                foreach (DataColumn column in tap1.Columns)
                {
                    grid1.Columns[Genclass.i].Name = column.ColumnName;
                    grid1.Columns[Genclass.i].HeaderText = column.ColumnName;
                    grid1.Columns[Genclass.i].DataPropertyName = column.ColumnName;
                    Genclass.i = Genclass.i + 1;
                }

                if (Genclass.data1 == 1)
                {
                    grid1.Columns[0].Width = 300;
                    grid1.Columns[1].Width = 130;
                }
                else
                {
                    grid1.Columns[0].Width = 130;
                    grid1.Columns[1].Width = 300;
                }


                grid1.Columns[2].Width = 130;
                grid1.Columns[3].Width = 100;
                grid1.Columns[4].Width = 80;

                grid1.Columns[5].Visible = false;
                grid1.Columns[6].Visible = false;
                grid1.Columns[7].Width = 80;
                grid1.Columns[7].ReadOnly = false;





                grid1.DataSource = tap1;




                if (Genclass.data1 == 1)
                {

                    Genclass.Module.Partylistviewcont1("uid", "itemname", "itemcode", "price", "Effdate", Genclass.strsql, this, txttitemid, txtitemname, txtitemcode, txtprice, txtsalesdate, Editpan);

                    Genclass.strsql = "select distinct c.uid,c.itemname,c.itemcode,price,convert(nvarchar,eff_from,106) as eff_from from pur_price_list  a inner join  PartyM b on b.Uid=a.suppuid inner join ItemM c on a.Itemuid=c.Uid   where a.companyid=" + Genclass.data1 + " and a.suppuid=" + txtpuid.Text + " and a.eff_to is null";
                    Genclass.FSSQLSortStr = "itemname";
                    Genclass.FSSQLSortStr1 = "itemcode";
                }

                else
                {
                    Genclass.Module.Partylistviewcont1("uid", "itemcode", "itemname", "price", "Effdate", Genclass.strsql, this, txttitemid, txtitemcode, txtitemname, txtprice, txtsalesdate, Editpan);

                    Genclass.strsql = "select distinct c.uid,c.itemcode,c.itemname,price,convert(nvarchar,eff_from,106) as eff_from from pur_price_list  a inner join  PartyM b on b.Uid=a.suppuid inner join ItemM c on a.Itemuid=c.Uid   where a.companyid=" + Genclass.data1 + " and a.suppuid=" + txtpuid.Text + " and a.eff_to is null";
                    Genclass.FSSQLSortStr = "isnull(itemcode,'')";
                    Genclass.FSSQLSortStr1 = "itemname";
                }

                Genclass.cmd = new SqlCommand(Genclass.strsql, conn);
                SqlDataAdapter aptr2 = new SqlDataAdapter(Genclass.cmd);
                DataTable tap2 = new DataTable();
                aptr2.Fill(tap2);

                TabPage tab3 = (TabPage)tab.Controls["tabPage3"];
                DataGridView grid2 = (DataGridView)tab3.Controls["HFGP2"];
                grid2.Refresh();
                grid2.ColumnCount = tap2.Columns.Count;
                grid2.Columns[0].Visible = false;
                if (Genclass.data1 == 1)
                {
                    grid2.Columns[1].Width = 300;


                    grid2.Columns[2].Width = 130;
                }
                else
                {
                    grid2.Columns[1].Width = 130;


                    grid2.Columns[2].Width = 300;
                }
                grid2.Columns[3].Width = 100;
                grid2.Columns[4].Width = 120;



                grid2.DefaultCellStyle.Font = new Font("Arial", 10);

                grid2.ColumnHeadersDefaultCellStyle.Font = new Font("Arial", 10, FontStyle.Bold);
                grid2.AutoGenerateColumns = false;

                Genclass.i = 0;
                foreach (DataColumn column in tap2.Columns)
                {
                    grid2.Columns[Genclass.i].Name = column.ColumnName;
                    grid2.Columns[Genclass.i].HeaderText = column.ColumnName;
                    grid2.Columns[Genclass.i].DataPropertyName = column.ColumnName;
                    Genclass.i = Genclass.i + 1;
                }


                grid2.DataSource = tap2;

                if (Genclass.data1 == 1)
                {

                    Genclass.Module.Partylistviewcont1("uid", "itemname", "itemcode", "listid", "id", Genclass.strsql, this, txttitemid, txtitemname, txtitemcode, txtprice, txtsalesdate, Editpan);

                    Genclass.strsql = "select distinct uid,itemname,itemcode,0 as listid,0 as id FROM itemm  where  active=1 and companyid=" + Genclass.data1 + "";
                    Genclass.FSSQLSortStr = "itemname";
                    Genclass.FSSQLSortStr1 = "itemcode";
                }

                else
                {
                    Genclass.Module.Partylistviewcont1("uid", "itemcode", "itemname", "listid", "id", Genclass.strsql, this, txttitemid, txtitemcode, txtitemname, txtprice, txtsalesdate, Editpan);

                    Genclass.strsql = "select distinct uid,itemcode,itemname,0 as listid,0 as id FROM itemm  where  active=1 and companyid=" + Genclass.data1 + "";
                    Genclass.FSSQLSortStr = "isnull(itemcode,'')";
                    Genclass.FSSQLSortStr1 = "itemname";
                }

                Genclass.cmd = new SqlCommand(Genclass.strsql, conn);
                SqlDataAdapter aptr3 = new SqlDataAdapter(Genclass.cmd);
                DataTable tap3 = new DataTable();
                aptr3.Fill(tap3);

                TabPage tab4 = (TabPage)tab.Controls["tabPage4"];
                DataGridView grid3 = (DataGridView)tab4.Controls["HFGP3"];
                grid3.Refresh();
                grid3.ColumnCount = tap3.Columns.Count;
                grid3.Columns[0].Visible = false;
                if (Genclass.data1 == 1)
                {
                    grid3.Columns[1].Width = 300;
                    grid3.Columns[2].Width = 130;
                }
                else
                {
                    grid3.Columns[1].Width = 130;
                    grid3.Columns[2].Width = 300;
                }
                grid3.Columns[3].Visible = false;
                grid3.Columns[4].Visible = false;


                grid3.DefaultCellStyle.Font = new Font("Arial", 10);

                grid3.ColumnHeadersDefaultCellStyle.Font = new Font("Arial", 10, FontStyle.Bold);
                grid3.AutoGenerateColumns = false;

                Genclass.i = 0;
                foreach (DataColumn column in tap3.Columns)
                {
                    grid3.Columns[Genclass.i].Name = column.ColumnName;
                    grid3.Columns[Genclass.i].HeaderText = column.ColumnName;
                    grid3.Columns[Genclass.i].DataPropertyName = column.ColumnName;
                    Genclass.i = Genclass.i + 1;
                }

                grid3.DataSource = tap3;


                contc.StartPosition = FormStartPosition.CenterScreen;
                contc.MdiParent = this.MdiParent;
                contc.Show();
                conn.Close();
            }
        }

        private void loadput2()
        {
            conn.Open();
            //txttitemid.Text = "";

            if (Genclass.type == 5)
            {

                //Genclass.Module.Partylistviewcont3("uid", "Item","Itemcode", Genclass.strsql, this, txttitemid, txtitemname,txtdcid, Editpan);

                //Genclass.strsql = "select distinct b.uid,itemname,price from pur_price_list a inner join itemm b on a.itemuid=b.uid where a.suppuid=" + txtpuid.Text + " and b.active=1 and a.companyid=1 and a.Eff_to is null";
                if (Genclass.data1 == 1)
                {
                    Genclass.Module.Partylistviewcont2("uid", "itemname", "itemcode", "listid", Genclass.strsql, this, txttitemid, txtitemname, txtitemcode, txtlisid, Editpan);

                    //Genclass.strsql = "select uid,itemname ,itemcode from (select a.UId,a.DocNo,isnull(b.PQty,0)-isnull(c.PQty,0)-isnull(d.PQty,0) as balqty,f.ItemName,f.ItemCode from sTransactionsP a inner join sTransactionsPList b on a.UId=b.TransactionsPUId   and b.DocTypeID = 80   left join Stransactionsplist  d on b.uid=  d.Refuid  and d.doctypeid=40 left join  TransactionsPList c on b.uid=c.Refuid  and c.DocTypeID=20  left join Stransactionsplist e on c.Uid=e.refuid and e.doctypeid=40  left join itemm f on b.ItemUId=f.Uid group  by   a.uid,a.docno,b.pqty,d.pqty,c.pqty,f.ItemName,f.ItemCode having isnull(b.PQty,0)-isnull(c.PQty,0)-isnull(d.PQty,0) >0 ) tab";
                    Genclass.strsql = "select itemid,itemname ,itemcode,listid from (select a.UId,a.DocNo,isnull(b.PQty,0)-isnull(c.PQty,0)-isnull(e.PQty,0)  as  balqty,f.ItemName,f.ItemCode,f.uid as itemid,b.uid as listid from sTransactionsP a inner join sTransactionsPList b on a.UId=b.TransactionsPUId     and b.DocTypeID = 80   left join Stransactionsplist  d on b.uid=  d.Refuid  and d.doctypeid=40 left join    TransactionsPList c    on b.uid=c.Refuid  and c.DocTypeID=110  left join Stransactionsplist e on c.Uid=e.refuid    and e.doctypeid=40  left join     itemm f on c.ItemUId=f.Uid  left join transactionsp  g on c.transactionspuid=g.uid left join partym h on g.partyuid=h.uid  where f.itemname is not null   and f.companyid=1  and h.uid=" + txtpuid.Text + "     group  by   a.uid,a.docno,b.pqty,e.pqty,c.pqty,f.ItemName,     f.ItemCode,f.uid,b.uid having isnull(b.PQty,0)-isnull(c.PQty,0)-isnull(e.PQty,0) >0 ) tab     ";
                    Genclass.FSSQLSortStr = "itemname";
                    Genclass.FSSQLSortStr1 = "itemcode";

                }
                else
                {
                    Genclass.Module.Partylistviewcont2("uid", "Itemcode", "itemname", "listid", Genclass.strsql, this, txttitemid, txtdcid, txtitemname, txtlisid, Editpan);
                    Genclass.strsql = "select distinct uid,itemcode,itemname,'' as listid FROM itemm  where  active=1 and companyid=" + Genclass.data1 + "";

                    Genclass.FSSQLSortStr = "itemcode";

                }
                Genclass.cmd = new SqlCommand(Genclass.strsql, conn);
                SqlDataAdapter aptr = new SqlDataAdapter(Genclass.cmd);
                DataTable tap = new DataTable();
                aptr.Fill(tap);


                FrmlookupItem contc = new FrmlookupItem();
                TabControl tab = (TabControl)contc.Controls["tabC"];


                TabPage tab1 = (TabPage)tab.Controls["tabPage1"];

                DataGridView grid = (DataGridView)tab1.Controls["hfgp"];
                grid.Refresh();
                grid.ColumnCount = tap.Columns.Count;
                grid.Columns[0].Visible = false;
                grid.Columns[1].Width = 350;


                grid.Columns[2].Width = 250;
                grid.Columns[3].Visible = false;


                grid.DefaultCellStyle.Font = new Font("Arial", 10);

                grid.ColumnHeadersDefaultCellStyle.Font = new Font("Arial", 10, FontStyle.Bold);
                grid.AutoGenerateColumns = false;

                Genclass.i = 0;
                foreach (DataColumn column in tap.Columns)
                {
                    grid.Columns[Genclass.i].Name = column.ColumnName;
                    grid.Columns[Genclass.i].HeaderText = column.ColumnName;
                    grid.Columns[Genclass.i].DataPropertyName = column.ColumnName;
                    Genclass.i = Genclass.i + 1;
                }

                grid.DataSource = tap;




                if (Genclass.data1 == 3)
                {

                    Genclass.Module.Partylistviewcont2("uid", "itemname", "itemcode", "listid", Genclass.strsql, this, txttitemid, txtitemname, txtitemcode, txtlisid, Editpan);
                    Genclass.strsql = "select itemid,itemname,itemcode,listid from (select a.UId,a.DocNo,isnull(b.PQty,0)-isnull(c.PQty,0)-isnull(d.PQty,0) as balqty,f.uid as itemid,f.ItemName,f.ItemCode,b.uid as listid from sTransactionsP a inner join sTransactionsPList b on a.UId=b.TransactionsPUId   and b.DocTypeID = 80    left join Stransactionsplist  d on b.uid=  d.Refuid  and d.doctypeid=40 left join  TransactionsPList c on b.uid=c.Refuid  and c.DocTypeID=20 left join Stransactionsplist e on c.Uid=e.refuid and e.doctypeid=40  left join itemm f on b.ItemUId=f.Uid left join partym h on a.partyuid=h.uid  where a.partyuid=" + txtpuid.Text + " group  by  a.uid,a.docno,b.pqty,d.pqty,c.pqty,f.ItemName,f.ItemCode,f.uid,b.uid having isnull(b.PQty,0)-isnull(c.PQty,0)-isnull(d.PQty,0) >0 ) tab";
                    Genclass.FSSQLSortStr = "itemname";
                    Genclass.FSSQLSortStr1 = "itemcode";


                    Genclass.cmd = new SqlCommand(Genclass.strsql, conn);
                    SqlDataAdapter aptr1 = new SqlDataAdapter(Genclass.cmd);
                    DataTable tap1 = new DataTable();
                    aptr1.Fill(tap1);

                    TabPage tab2 = (TabPage)tab.Controls["tabPage2"];
                    DataGridView grid1 = (DataGridView)tab2.Controls["HFGP4"];
                    grid1.Refresh();
                    grid1.ColumnCount = tap1.Columns.Count;
                    grid1.Columns[0].Visible = false;
                    grid1.Columns[1].Width = 350;


                    grid1.Columns[2].Width = 250;
                    grid1.Columns[3].Visible = false;



                    grid1.DefaultCellStyle.Font = new Font("Arial", 10);

                    grid1.ColumnHeadersDefaultCellStyle.Font = new Font("Arial", 10, FontStyle.Bold);
                    grid1.AutoGenerateColumns = false;

                    Genclass.i = 0;
                    foreach (DataColumn column in tap1.Columns)
                    {
                        grid1.Columns[Genclass.i].Name = column.ColumnName;
                        grid1.Columns[Genclass.i].HeaderText = column.ColumnName;
                        grid1.Columns[Genclass.i].DataPropertyName = column.ColumnName;
                        Genclass.i = Genclass.i + 1;
                    }
                    grid1.DataSource = tap1;


                    Genclass.Module.Partylistviewcont2("uid", "itemname", "itemcode", "listid", Genclass.strsql, this, txttitemid, txtitemname, txtitemcode, txtlisid, Editpan);

                    Genclass.strsql = "select c.uid,c.itemname,c.itemcode,0 as listid from pur_price_list  a inner join  PartyM b on b.Uid=a.suppuid inner join ItemM c on a.Itemuid=c.Uid   where a.companyid=" + Genclass.data1 + " and a.suppuid=" + txtpuid.Text + "";
                    Genclass.FSSQLSortStr = "itemname";
                    Genclass.FSSQLSortStr1 = "itemcode";

                    Genclass.cmd = new SqlCommand(Genclass.strsql, conn);
                    SqlDataAdapter aptr2 = new SqlDataAdapter(Genclass.cmd);
                    DataTable tap2 = new DataTable();
                    aptr2.Fill(tap2);

                    TabPage tab3 = (TabPage)tab.Controls["tabPage3"];
                    DataGridView grid2 = (DataGridView)tab3.Controls["HFGP2"];
                    grid2.Refresh();
                    grid2.ColumnCount = tap2.Columns.Count;
                    grid2.Columns[0].Visible = false;
                    grid2.Columns[1].Width = 350;


                    grid2.Columns[2].Width = 250;
                    grid2.Columns[3].Visible = false;



                    grid2.DefaultCellStyle.Font = new Font("Arial", 10);

                    grid2.ColumnHeadersDefaultCellStyle.Font = new Font("Arial", 10, FontStyle.Bold);
                    grid2.AutoGenerateColumns = false;

                    Genclass.i = 0;
                    foreach (DataColumn column in tap2.Columns)
                    {
                        grid2.Columns[Genclass.i].Name = column.ColumnName;
                        grid2.Columns[Genclass.i].HeaderText = column.ColumnName;
                        grid2.Columns[Genclass.i].DataPropertyName = column.ColumnName;
                        Genclass.i = Genclass.i + 1;
                    }


                    grid2.DataSource = tap2;


                    Genclass.Module.Partylistviewcont2("uid", "itemname", "itemcode", "listid", Genclass.strsql, this, txttitemid, txtitemname, txtitemcode, txtlisid, Editpan);

                    Genclass.strsql = "select distinct uid,itemname,itemcode,0 as listid FROM itemm  where  active=1 and companyid=" + Genclass.data1 + "";
                    Genclass.FSSQLSortStr = "itemname";
                    Genclass.FSSQLSortStr1 = "itemcode";

                    Genclass.cmd = new SqlCommand(Genclass.strsql, conn);
                    SqlDataAdapter aptr3 = new SqlDataAdapter(Genclass.cmd);
                    DataTable tap3 = new DataTable();
                    aptr3.Fill(tap3);

                    TabPage tab4 = (TabPage)tab.Controls["tabPage4"];
                    DataGridView grid3 = (DataGridView)tab4.Controls["HFGP3"];
                    grid3.Refresh();
                    grid3.ColumnCount = tap3.Columns.Count;
                    grid3.Columns[0].Visible = false;
                    grid3.Columns[1].Width = 350;


                    grid3.Columns[2].Width = 250;
                    grid3.Columns[3].Visible = false;



                    grid3.DefaultCellStyle.Font = new Font("Arial", 10);

                    grid3.ColumnHeadersDefaultCellStyle.Font = new Font("Arial", 10, FontStyle.Bold);
                    grid3.AutoGenerateColumns = false;

                    Genclass.i = 0;
                    foreach (DataColumn column in tap3.Columns)
                    {
                        grid3.Columns[Genclass.i].Name = column.ColumnName;
                        grid3.Columns[Genclass.i].HeaderText = column.ColumnName;
                        grid3.Columns[Genclass.i].DataPropertyName = column.ColumnName;
                        Genclass.i = Genclass.i + 1;
                    }

                    grid3.DataSource = tap3;

                }
                contc.StartPosition = FormStartPosition.CenterScreen;
                contc.MdiParent = this.MdiParent;
                contc.Show();
                conn.Close();
            }
        }

        private void btnexit_Click(object sender, System.EventArgs e)
        {
            this.Dispose();
        }

        private void txtper_TextChanged(object sender, System.EventArgs e)
        {
            int val1;
            int val2;
            int val3;
            val1 = Convert.ToInt16(txttaxable.Text);
            val2 = Convert.ToInt16(txtper.Text);
            val3 = (val1 * val2) / 100;
            //txtdis.Text = Convert.ToString(val3);
            CalcNetAmt();

        }

        //private void textBox2_TextChanged(object sender, System.EventArgs e)
        //{

        //}
        private void CalcNetAmt()
        {

            //int val4;
            //int val5;
            //int roff;
            int totamt;

            totamt = Convert.ToInt16(txttaxable.Text);

            //int dis = Convert.ToInt16(txtdis.Text);
            //if (txtper.Text == "")
            //{
            //    TxtNetAmt.Text = txttaxable.Text + txtexcise.Text + txttax.Text + txtpf.Text;
            //    val4 = Convert.ToInt16(TxtNetAmt.Text);
            //    val5 = val4 - Convert.ToInt16(txtdis.Text);
            //    // Genclass.strfin = totamt + Convert.ToInt16(txtpf.Text) - dis;

            //    TxtNetAmt.Text = Convert.ToString(val5);
            //}
            //else
            //{
            //    TxtNetAmt.Text = "0.00";


            //    TxtNetAmt.Text = txttaxable.Text + txtexcise.Text + txttax.Text + txtpf.Text;
            //    int cal5 = Convert.ToInt16(TxtNetAmt.Text);
            //    val4 = Convert.ToInt16(TxtNetAmt.Text) - Convert.ToInt16(txtdis.Text);
            //    //val5 = val4 - Convert.ToInt16(txtdis.Text);
            //    //txtpf.Text = "0.00";
            //    string cal1 = txtpf.Text;


            //TxtNetAmt.Text = Convert.ToString(val4);
            Genclass.strfin = TxtNetAmt.Text;
        }

        //TxtRoff.Text =val4 -Convert.ToString(TxtNetAmt.Text);

        //}

        //private void cboexcise_SelectedIndexChanged(object sender, System.EventArgs e)
        //{

        //    }
        //private void exciseduty()
        //{

        // if(cboexcise.SelectedValue==null)
        //    {
        //    return;

        //    }
        //    else
        //    {
        //    conn.Open();
        //        {
        //         Genclass.strsql = "Select * from GeneralM where TypeM_Uid=5 and Uid= "+ cboexcise.SelectedValue +" ";
        //        Genclass.cmd = new SqlCommand(Genclass.strsql, conn);
        //        SqlDataAdapter aptr1 = new SqlDataAdapter(Genclass.cmd);
        //        DataTable tap1 = new DataTable();
        //        aptr1.Fill(tap1);
        //        LblED1.Text = tap1.Rows[0]["F1"].ToString();
        //        LblCess.Text = tap1.Rows[0]["F2"].ToString();
        //        LblHECess.Text = tap1.Rows[0]["F3"].ToString();

        //        }
        //}



        //        }


        private void Taxduty()
        {

            //if (cbotax.SelectedValue == null)
            //{
            //    return;

            //}
            //else
            //{
            //    conn.Open();
            //    {
            //        Genclass.strsql = "Select * from GeneralM where  Uid= " + cbotax.SelectedValue + " ";
            //        Genclass.cmd = new SqlCommand(Genclass.strsql, conn);
            //        SqlDataAdapter aptr1 = new SqlDataAdapter(Genclass.cmd);
            //        DataTable tap1 = new DataTable();
            //        aptr1.Fill(tap1);
            //        lbltax.Text = tap1.Rows[0]["F1"].ToString();


            //    }
            //    conn.Close();
            //}



        }

        //private void cboexcise_Click(object sender, System.EventArgs e)
        //{
        //    if (cboexcise.SelectedValue != null)
        //    {
        //        exciseduty();
        //        txtted.Text = "";
        //        CalcNetAmt();
        //        int ed;
        //        int Cess;
        //        int HECess;
        //        int excise;
        //        int lbled;

        //        //val3 = (val1 * val2) / 100;
        //        //txtdis.Text = Convert.ToString(val3);
        //        lbled = Convert.ToInt16(LblED1.Text);
        //        ed = (Convert.ToInt16(Genclass.strfin) * lbled) / 100;
        //        txtted.Text = Convert.ToString(ed);
        //        Cess = ed * Convert.ToInt16(LblCess.Text) / 100;
        //        txtcess.Text = Convert.ToString(Cess);
        //        HECess = ed * Convert.ToInt16(LblHECess.Text) / 100;
        //        txthecess.Text = Convert.ToString(HECess);
        //        excise = ed + Cess + HECess;
        //        txtexcise.Text = Convert.ToString(excise);

        //    }   
        //    else
        //    {
        //        txtted.Text = "0.00";
        //        txtcess.Text = "0.00";
        //        txthecess.Text = "0.00";
        //        txtexcise.Text = "";
        //        LblED1.Text = "";
        //        LblCess.Text = "";
        //        LblHECess.Text = "";



        //    }

        //}

        private void btnsave_Click(object sender, System.EventArgs e)
        {


        }



        private void cbotax_Click(object sender, System.EventArgs e)
        {
            //if (cbotax.SelectedValue != null)
            //{
            //    int ed;
            //    int Cess;
            //    int HECess;
            //    int excise;
            //    int lbled;
            //    int tax;
            //    int vat;

            //    lbled = Convert.ToInt16(LblED1.Text);
            //    ed = (Convert.ToInt16(Genclass.strfin) * lbled) / 100;
            //    txtted.Text = Convert.ToString(ed);
            //    Cess = ed * Convert.ToInt16(LblCess.Text) / 100;
            //    txtcess.Text = Convert.ToString(Cess);
            //    HECess = ed * Convert.ToInt16(LblHECess.Text) / 100;
            //    txthecess.Text = Convert.ToString(HECess);
            //    excise = ed + Cess + HECess;
            //    txtexcise.Text = Convert.ToString(excise);
            //    CalcNetAmt();
            //    Taxduty();
            //    tax = Convert.ToInt16(lbltax.Text);
            //    vat = Convert.ToInt16(TxtNetAmt.Text) * tax / 100;
            //    txttax.Text = Convert.ToString(vat);
            CalcNetAmt();
            //}
        }


        private void txtdcno_TextChanged(object sender, EventArgs e)
        {


            //if (Genclass.Dtype == 40)
            //{

            //    if (txtdcno.Text == "")
            //    {
            //        return;

            //    }
            //    else
            //    {
            //        Genclass.strsql = "select Itemname,UoM,itemuid,refid,qty,PRICE,convert(decimal(18,2),BasicValue,105) AS BasicValue,disper,convert(decimal(18,2),Disvalue,105) AS Disvalue,convert(decimal(18,2),Taxablevalue,105) AS Taxablevalue,convert(decimal(18,2),Cgstval,105) AS EDVAL,convert(decimal(18,2),sgstval,105) AS VATVAL,convert(decimal(18,2),igstval,105) AS igstval,CG as ED,sg as VAT,ig, convert(decimal(18,2),(BasicValue-(isnull(Disvalue,0))+ (isnull(Cgstval,0)+isnull(sgstval,0))),105) as total from (select distinct   c.itemname,d.generalname as uom,b.itemuid,b.uid as refid,b.pqty-isnull(sum(e.pqty),0) as qty,f.Price ,(b.pqty-isnull(sum(e.pqty),0)) * f.Price as BasicValue,disper,((b.pqty-isnull(sum(e.pqty),0)) * f.Price/100)* disper as Disvalue,((b.pqty-isnull(sum(e.pqty),0)) * f.Price)-(((b.pqty-isnull(sum(e.pqty),0)) * f.Price/100)* disper) as Taxablevalue,g.f1 as CG,(((b.pqty-isnull(sum(e.pqty),0)) * f.Price)-(((b.pqty-isnull(sum(e.pqty),0)) * f.Price/100)* disper))/100 * g.f1 as Cgstval,h.f1 as SG,(((b.pqty-isnull(sum(e.pqty),0)) * f.Price)-(((b.pqty-isnull(sum(e.pqty),0)) * f.Price/100)* disper))/100 * h.f1 as sgstval,i.f1 as ig,(((b.pqty-isnull(sum(e.pqty),0)) * f.Price)-(((b.pqty-isnull(sum(e.pqty),0)) * f.Price/100)* disper))/100 * i.f1 as igstval from transactionsp a inner join transactionsplist b on  a.uid=b.transactionspuid and a.doctypeid=30 and a.companyid=1 inner join itemm c on b.itemuid=c.uid left join generalm d on c.uom_uid=d.uid  left join transactionsplist e on  b.uid=e.refuid and e.doctypeid=40 inner join pur_price_list f on c.uid=f.itemuid and c.partyuid=f.suppuid inner join generalm g on f.cgstid=g.uid inner join generalm h on f.sgstid=h.uid inner join generalm i on f.igstid=i.uid where a.docno='0001/17-18' group by c.itemname,d.generalname,b.itemuid,b.uid,b.pqty,f.price,disper,g.f1,h.f1,i.f1 having b.pqty-isnull(sum(e.pqty),0) >0) tab";

            //        Genclass.cmd = new SqlCommand(Genclass.strsql, conn);
            //        SqlDataAdapter aptr1 = new SqlDataAdapter(Genclass.cmd);
            //        DataTable tap1 = new DataTable();
            //        aptr1.Fill(tap1);
            //        //txtname.Text = tap1.Rows[0]["Name"].ToString();
            //        //txtpuid.Text = tap1.Rows[0]["Partyuid"].ToString();
            //        for (int i = 0; i < tap1.Rows.Count; i++)
            //        {
            //            var index = HFIT.Rows.Add();
            //            HFIT.Rows[index].Cells[0].Value = tap1.Rows[i]["itemname"].ToString();
            //            HFIT.Rows[index].Cells[1].Value = tap1.Rows[i]["UOM"].ToString();
            //            HFIT.Rows[index].Cells[2].Value = tap1.Rows[i]["Price"].ToString();
            //            HFIT.Rows[index].Cells[3].Value = tap1.Rows[i]["qty"].ToString();
            //            HFIT.Rows[index].Cells[4].Value = tap1.Rows[i]["BasicValue"].ToString();
            //            HFIT.Rows[index].Cells[5].Value = tap1.Rows[i]["itemuid"].ToString();
            //            HFIT.Rows[index].Cells[6].Value = tap1.Rows[i]["refid"].ToString();
            //            HFIT.Rows[index].Cells[7].Value = tap1.Rows[i]["disper"].ToString();
            //            HFIT.Rows[index].Cells[8].Value = tap1.Rows[i]["Disvalue"].ToString();
            //            HFIT.Rows[index].Cells[9].Value = tap1.Rows[i]["Taxablevalue"].ToString();
            //            HFIT.Rows[index].Cells[10].Value = tap1.Rows[i]["ED"].ToString();
            //            HFIT.Rows[index].Cells[11].Value = tap1.Rows[i]["EDVAL"].ToString();
            //            HFIT.Rows[index].Cells[12].Value = tap1.Rows[i]["VAT"].ToString();
            //            HFIT.Rows[index].Cells[13].Value = tap1.Rows[i]["VATVAL"].ToString();
            //            HFIT.Rows[index].Cells[14].Value = tap1.Rows[i]["Total"].ToString();
            //            HFIT.Rows[index].Cells[15].Value = index;

            //            Genclass.sum1 = Genclass.sum1 + Convert.ToDouble(HFIT.Rows[i].Cells[14].Value);
            //            Txttot.Text = Genclass.sum1.ToString();




            //            //Titlep();



            //        }

            //    }


            //}
            //else
            //{
            //    txtitemname.Focus();

            //}
        }



        private void button4_Click(object sender, EventArgs e)
        {
            Editpan.Visible = false;
            Genpan.Visible = true;
        }

        private void HFIT_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            for (int k = 0; k < HFIT.Rows.Count - 1; k++)
            {
                if (HFIT.Rows[k].Cells[2].Value.ToString() == "" || HFIT.Rows[k].Cells[2].Value == null || HFIT.Rows[k].Cells[3].Value == null || HFIT.Rows[k].Cells[3].Value.ToString() == "" || HFIT.Rows[k].Cells[7].Value.ToString() == "" || HFIT.Rows[k].Cells[7].Value == null)
                {
                    return;

                }
                else
                {
                    //Genclass.sum1 = 0;
                    //Genclass.sum4 = 0;
                    if (HFIT.Rows[k].Cells[2].Value.ToString() != "0.000" || HFIT.Rows[k].Cells[3].Value.ToString() != "0.000" || HFIT.Rows[k].Cells[7].Value.ToString() != "0.000")
                    {
                        //if (HFIT.Rows[k].Cells[2].Value.ToString() != "0.000")

                        txtprice.Text = HFIT.Rows[k].Cells[2].Value.ToString();
                        double str2 = Convert.ToDouble(HFIT.Rows[k].Cells[2].Value.ToString()) * Convert.ToDouble(HFIT.Rows[k].Cells[3].Value.ToString());
                        HFIT.Rows[k].Cells[4].Value = str2.ToString();
                        double str3 = Convert.ToDouble(HFIT.Rows[k].Cells[4].Value.ToString()) * (Convert.ToDouble(HFIT.Rows[k].Cells[7].Value.ToString()) / 100);
                        HFIT.Rows[k].Cells[8].Value = str3.ToString();
                        double str4 = Convert.ToDouble(HFIT.Rows[k].Cells[4].Value.ToString()) + Convert.ToDouble(HFIT.Rows[k].Cells[8].Value.ToString());
                        HFIT.Rows[k].Cells[12].Value = str4.ToString();

                        //Genclass.sum1 = Genclass.sum1 + Convert.ToDouble(HFIT.Rows[k].Cells[4].Value);
                        //Txttot.Text = Genclass.sum1.ToString("0.00");





                    }

                }

                fun2();


            }


        }
        private void fun2()
        {
            if (mode == 1)
            {
                txtbval.Text = "0.00";
                Txttot.Text = "0.00";
                Genclass.sum1 = 0;
                Genclass.sum4 = 0;

                for (int i = 0; i < HFIT.Rows.Count - 1; i++)
                {

                    if (HFIT.Rows[i].Cells[12].Value.ToString() != "" || HFIT.Rows[i].Cells[12].Value.ToString() != "0.00" || HFIT.Rows[i].Cells[4].Value.ToString() != "" || HFIT.Rows[i].Cells[4].Value.ToString() != "0.00")
                    {
                        Genclass.sum1 = Genclass.sum1 + Convert.ToDouble(HFIT.Rows[i].Cells[4].Value);
                        Txttot.Text = Genclass.sum1.ToString();


                        //Genclass.sum4 = Genclass.sum4 + Convert.ToDouble(HFIT.Rows[i].Cells[4].Value);
                        //txtbval.Text = Genclass.sum4.ToString();
                    }

                }
            }
        }

        private void button6_Click(object sender, EventArgs e)
        {

        }

        private void button8_Click(object sender, EventArgs e)
        {
            Editpan.Visible = false;
            Taxpan.Visible = true;
            txtexcise.Text = Txttot.Text;
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void button9_Click(object sender, EventArgs e)
        {
            //termspan.Visible = false;
            addipan.Visible = true;
            button9.Visible = false;
            button10.Visible = true;

        }

        private void button10_Click(object sender, EventArgs e)
        {

        }

        private void txtexcise_TextChanged(object sender, EventArgs e)
        {

        }

        private void Taxpan_Paint(object sender, PaintEventArgs e)
        {

        }

        private void button10_Click_1(object sender, EventArgs e)
        {

        }

        //private void panel1_Paint(object sender, PaintEventArgs e)
        //{

        //}
        private void TitleAdd()
        {
            HFGA.ColumnCount = 3;
            HFGA.Columns[0].Name = "Additional Charge";
            HFGA.Columns[1].Name = "Amount";
            HFGA.Columns[2].Name = "uid";

            HFGA.Columns[0].Width = 334;
            HFGA.Columns[1].Width = 90;
            HFGA.Columns[2].Visible = false;
        }

        private void Titleterm()
        {
            HFGT.ColumnCount = 1;
            HFGT.Columns[0].Name = "Terms";
            //HFGT.Columns[1].Name = "Term Description";
            //HFGT.Columns[2].Name = "";


            HFGT.Columns[0].Width = 350;

            //HFGT.Columns[1].Width = 230;
            //HFGT.Columns[2].Width = 250;




        }

        private void txtterms_KeyDown(object sender, KeyEventArgs e)
        {

        }

        private void button11_Click(object sender, EventArgs e)
        {

            HFGT.AllowUserToAddRows = true;
            HFGT.AutoGenerateColumns = true;
            var index = HFGT.Rows.Add();
            HFGT.Rows[index].Cells[0].Value = txtterms.Text;
            HFGT.Rows[index].Cells[1].Value = txtremde.Text;
            HFGT.Rows[index].Cells[2].Value = txttermid.Text;

        }

        private void button12_Click(object sender, EventArgs e)
        {

        }

        private void button10_Click_2(object sender, EventArgs e)
        {
            addipan.Visible = false;
            termspan.Visible = true;
            button9.Visible = true;
            button10.Visible = false;
        }

        private void txtaddcharge_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtaddcharge_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                Genclass.fieldone = "";
                Genclass.fieldtwo = "";
                Genclass.fieldthree = "";
                Genclass.fieldFour = "";
                Genclass.fieldFive = "";
                Genclass.type = 3;
                loadput();
            }
        }

        private void txttotaddd_TextChanged(object sender, EventArgs e)
        {
            if (txttotaddd.Text == "0" || txttotaddd.Text == "")
            {
                TxtNetAmt.Text = txtexcise.Text;
            }
            else
            {
                TxtNetAmt.Text = "0";
                double val2 = Convert.ToDouble(txtexcise.Text) + Convert.ToDouble(txttotaddd.Text);
                TxtNetAmt.Text = val2.ToString();

            }
        }

        private void btnedit_Click(object sender, EventArgs e)
        {
            HFIT.AutoGenerateColumns = false;
            HFIT.Refresh();
            HFIT.DataSource = null;
            HFIT.Rows.Clear();
            HFGA.AutoGenerateColumns = false;
            HFGA.Refresh();
            HFGA.DataSource = null;
            HFGA.Rows.Clear();
            HFGT.AutoGenerateColumns = false;
            HFGT.Refresh();
            HFGT.DataSource = null;
            HFGT.Rows.Clear();
            mode = 2;

            Genpan.Visible = false;
            Editpan.Visible = true;
            int i = HFGP.SelectedCells[0].RowIndex;
            txtgrnid.Text = HFGP.Rows[i].Cells[0].Value.ToString();
            txtgrn.Text = HFGP.Rows[i].Cells[1].Value.ToString();
            DTPDOCDT.Text = HFGP.Rows[i].Cells[2].Value.ToString();
            txtdcno.Text = HFGP.Rows[i].Cells[3].Value.ToString();
            Dtpdt.Text = HFGP.Rows[i].Cells[4].Value.ToString();
            txtname.Text = HFGP.Rows[i].Cells[5].Value.ToString();
            txtpuid.Text = HFGP.Rows[i].Cells[7].Value.ToString();
            //txtrem.Text = HFGP.Rows[i].Cells[8].Value.ToString();
            conn.Open();
            {
                Genclass.strsql = " select b.uid,c.ItemName,d.GeneralName as uom,PRate,pqty,BasicValue,ItemUId,b.refuid,disp,disval,Taxableval,Cgstid,Cgstval,Sgstid,Sgstval,totvalue from stransactionsp a inner join Stransactionsplist b on a.uid=b.transactionspuid  left join itemm c  on b.itemuid=c.uid left join generalm d on c.UOM_UId=d.Uid  where b.transactionspuid=" + txtgrnid.Text + " ";

                Genclass.cmd = new SqlCommand(Genclass.strsql, conn);
                SqlDataAdapter aptr1 = new SqlDataAdapter(Genclass.cmd);
                DataTable tap1 = new DataTable();
                aptr1.Fill(tap1);
                //txtname.Text = tap1.Rows[0]["Name"].ToString();
                //txtpuid.Text = tap1.Rows[0]["Partyuid"].ToString();
                Genclass.sum1 = 0;

                for (int k = 0; k < tap1.Rows.Count; k++)
                {
                    var index = HFIT.Rows.Add();
                    HFIT.Rows[index].Cells[0].Value = tap1.Rows[k]["itemname"].ToString();
                    HFIT.Rows[index].Cells[1].Value = tap1.Rows[k]["UOM"].ToString();
                    HFIT.Rows[index].Cells[2].Value = tap1.Rows[k]["PRate"].ToString();
                    HFIT.Rows[index].Cells[3].Value = tap1.Rows[k]["pqty"].ToString();
                    HFIT.Rows[index].Cells[4].Value = tap1.Rows[k]["BasicValue"].ToString();
                    HFIT.Rows[index].Cells[5].Value = tap1.Rows[k]["itemuid"].ToString();
                    HFIT.Rows[index].Cells[6].Value = tap1.Rows[k]["refuid"].ToString();
                    HFIT.Rows[index].Cells[7].Value = tap1.Rows[k]["disp"].ToString();
                    HFIT.Rows[index].Cells[8].Value = tap1.Rows[k]["disval"].ToString();
                    HFIT.Rows[index].Cells[9].Value = tap1.Rows[k]["Taxableval"].ToString();
                    HFIT.Rows[index].Cells[10].Value = tap1.Rows[k]["Cgstid"].ToString();
                    HFIT.Rows[index].Cells[11].Value = tap1.Rows[k]["Cgstval"].ToString();
                    HFIT.Rows[index].Cells[12].Value = tap1.Rows[k]["Sgstid"].ToString();
                    HFIT.Rows[index].Cells[13].Value = tap1.Rows[k]["Sgstval"].ToString();
                    HFIT.Rows[index].Cells[14].Value = tap1.Rows[k]["totvalue"].ToString();
                    HFIT.Rows[index].Cells[15].Value = tap1.Rows[i]["uid"].ToString();

                    Genclass.sum1 = Genclass.sum1 + Convert.ToDouble(HFIT.Rows[k].Cells[14].Value);
                    Txttot.Text = Genclass.sum1.ToString();
                    //txtexcise.Text = Genclass.sum1.ToString();



                }
                HFIT.Columns[0].Width = 300;
                HFIT.Columns[1].Visible = false;
                HFIT.Columns[2].Width = 60;
                HFIT.Columns[3].Width = 60;
                HFIT.Columns[4].Width = 100;

                HFIT.Columns[5].Visible = false;
                HFIT.Columns[6].Visible = false;
                HFIT.Columns[9].Visible = false;

                HFIT.Columns[7].Width = 50;
                HFIT.Columns[8].Width = 100;

                HFIT.Columns[10].Width = 50;
                HFIT.Columns[11].Width = 50;
                HFIT.Columns[12].Width = 50;
                HFIT.Columns[13].Width = 50;
                HFIT.Columns[14].Width = 100;
                HFIT.Columns[15].Visible = false;
                conn.Close();
            }
            conn.Open();
            {
                Genclass.strsql = "select * from Stransactionspcharges a left join generalm b on a.chargesuid=b.uid  where a.stransactionspuid=" + txtgrnid.Text + " ";

                Genclass.cmd = new SqlCommand(Genclass.strsql, conn);
                SqlDataAdapter aptr2 = new SqlDataAdapter(Genclass.cmd);
                DataTable tap2 = new DataTable();
                aptr2.Fill(tap2);
                Genclass.sum1 = 0;
                for (int j = 0; j < tap2.Rows.Count; j++)
                {
                    var index = HFGA.Rows.Add();
                    HFGA.Rows[index].Cells[0].Value = tap2.Rows[j]["generalname"].ToString();
                    HFGA.Rows[index].Cells[1].Value = tap2.Rows[j]["chargeamount"].ToString();
                    HFGA.Rows[index].Cells[2].Value = tap2.Rows[j]["chargesuid"].ToString();


                    Genclass.sum1 = Genclass.sum1 + Convert.ToDouble(HFGA.Rows[j].Cells[1].Value);
                    txttotaddd.Text = Genclass.sum1.ToString();
                }
                conn.Close();
            }
            conn.Open();
            {
                Genclass.strsql = "select * from STransactionsPTerms a left join generalm b on a.termsuid=b.uid  where a.stransactionspuid=" + txtgrnid.Text + " ";

                Genclass.cmd = new SqlCommand(Genclass.strsql, conn);
                SqlDataAdapter aptr3 = new SqlDataAdapter(Genclass.cmd);
                DataTable tap3 = new DataTable();
                aptr3.Fill(tap3);

                for (int g = 0; g < tap3.Rows.Count; g++)
                {
                    var index = HFGT.Rows.Add();
                    HFGT.Rows[index].Cells[0].Value = tap3.Rows[g]["generalname"].ToString();
                    HFGT.Rows[index].Cells[1].Value = tap3.Rows[g]["chargesamount"].ToString();
                    HFGT.Rows[index].Cells[2].Value = tap3.Rows[g]["chargesuid"].ToString();


                }
                conn.Close();
                double val2 = Convert.ToDouble(txtexcise.Text) + Convert.ToDouble(txttotaddd.Text);
                TxtNetAmt.Text = val2.ToString();
            }

        }



        private void txtscr1_TextChanged(object sender, EventArgs e)
        {
            Loadgrid();
        }

        private void Txtscr2_TextChanged(object sender, EventArgs e)
        {
            Loadgrid();
        }

        private void Txtscr3_TextChanged(object sender, EventArgs e)
        {
            Loadgrid();
        }

        private void txtscr4_TextChanged(object sender, EventArgs e)
        {
            Loadgrid();
        }

        private void txtscr5_TextChanged(object sender, EventArgs e)
        {
            Loadgrid();
        }

        private void HFGP_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void chkact_CheckedChanged(object sender, EventArgs e)
        {
            Loadgrid();
        }

        private void button6_Click_1(object sender, EventArgs e)
        {
            mode = 1;
            Genpan.Visible = false;
            panadd.Visible = false;
            Genclass.Module.ClearTextBox(this, Editpan);
            Genclass.STR = "";
            Genclass.Module.Gendocno();
            txtgrn.Text = Genclass.ST;
            label15.Visible = true;
            txtitemname.Visible = true;
            label14.Visible = true;
            txtprice.Visible = true;
            label31.Visible = true;
            txtqty.Visible = true;
            label32.Visible = true;
            txtbval.Visible = true;
            buttcusok.Visible = true;
            //}
            Editpan.Visible = true;
            btnsave.Visible = true;
            buttnfinbk.Visible = true;
            buttnnxt.Visible = true;
            termspan.Visible = false;
            button17.Visible = false;
            //buttnfinbk.Visible = false;
            //button11.Visible = true;
            //button12.Visible = true;
            DTPDOCDT.Focus();
            HFIT.Refresh();
            HFIT.DataSource = null;
            HFIT.Rows.Clear();
            Genclass.sum = 1;
            //HFIT.Columns[0].Width = 300;
            //HFIT.Columns[1].Visible = false;
            //HFIT.Columns[2].Width = 60;
            //HFIT.Columns[3].Width = 60;
            //HFIT.Columns[4].Width = 90;

            //HFIT.Columns[5].Visible = false;
            //HFIT.Columns[6].Visible = false;
            //HFIT.Columns[9].Visible = false;

            //HFIT.Columns[7].Width = 40;
            //HFIT.Columns[8].Width = 60;

            //HFIT.Columns[10].Width = 40;
            //HFIT.Columns[11].Width = 60;
            //HFIT.Columns[12].Width = 43;
            //HFIT.Columns[13].Width = 60;
            //HFIT.Columns[14].Width = 92;
            //HFIT.Columns[15].Visible = false;
            Titlep();
            HFIT.Columns[0].ReadOnly = true;
            HFIT.Columns[1].ReadOnly = true;
            HFIT.Columns[2].ReadOnly = true;
            //HFIT.Columns[3].ReadOnly = true;
            HFIT.Columns[4].ReadOnly = true;
            HFIT.Columns[5].ReadOnly = true;
            HFIT.Columns[6].ReadOnly = true;
            HFIT.Columns[7].ReadOnly = true;
            HFIT.Columns[8].ReadOnly = true;
            HFIT.Columns[9].ReadOnly = true;
            HFIT.Columns[10].ReadOnly = true;
            HFIT.Columns[11].ReadOnly = true;
            HFIT.Columns[12].ReadOnly = true;
            conn.Open();
            qur.CommandText = "delete from sotmp";
            qur.ExecuteNonQuery();
            conn.Close();

        }

        private void butedit_Click(object sender, EventArgs e)
        {

            mode = 2;
            panadd.Visible = false;
            Genpan.Visible = false;
            Taxpan.Visible = false;
            Editpan.Visible = true;


            label15.Visible = true;
            txtitemname.Visible = true;
            label14.Visible = true;
            txtprice.Visible = true;
            label31.Visible = true;
            txtqty.Visible = true;
            label32.Visible = true;
            txtbval.Visible = true;
            buttcusok.Visible = true;
            btnsave.Visible = true;
            termspan.Visible = false;
            button17.Visible = false;

            conn.Open();
            qur.CommandText = "delete from sotmp";
            qur.ExecuteNonQuery();
            conn.Close();

            int i = HFGP.SelectedCells[0].RowIndex;
            uid = HFGP.Rows[i].Cells[0].Value.ToString();
            txtgrnid.Text = HFGP.Rows[i].Cells[0].Value.ToString();
            txtgrn.Text = HFGP.Rows[i].Cells[1].Value.ToString();
            DTPDOCDT.Text = HFGP.Rows[i].Cells[2].Value.ToString();
            txtdcno.Text = HFGP.Rows[i].Cells[3].Value.ToString();
            Dtpdt.Text = HFGP.Rows[i].Cells[4].Value.ToString();
            txtname.Text = HFGP.Rows[i].Cells[5].Value.ToString();
            txtpuid.Text = HFGP.Rows[i].Cells[10].Value.ToString();
            txtpluid.Text = HFGP.Rows[i].Cells[11].Value.ToString();
            txtplace.Text = HFGP.Rows[i].Cells[12].Value.ToString();
            Dtppre.Text = HFGP.Rows[i].Cells[13].Value.ToString();
            Dtprem.Text = HFGP.Rows[i].Cells[14].Value.ToString();
            txttrans.Text = HFGP.Rows[i].Cells[15].Value.ToString();
            txtveh.Text = HFGP.Rows[i].Cells[16].Value.ToString();
            txtrem.Text = HFGP.Rows[i].Cells[17].Value.ToString();
            TxtNetAmt.Text = HFGP.Rows[i].Cells[10].Value.ToString();
            conn.Close();
            conn.Open();
            //HFIT.AutoGenerateColumns = false;

            HFIT.Refresh();
            HFIT.DataSource = null;
            HFIT.Rows.Clear();

            //HFGA.AutoGenerateColumns = false;
            HFGA.Refresh();
            HFGA.DataSource = null;
            HFGA.Rows.Clear();
            //HFGT.AutoGenerateColumns = false;
            HFGT.Refresh();
            HFGT.DataSource = null;
            HFGT.Rows.Clear();
            Titleterm();

            {
                Genclass.strsql = "select a.*,b.reqdate from (select distinct a.uid as hdid,b.uid,c.ItemName,d.generalname as uom,b.PRate,b.pqty,b.BasicValue,b.ItemUId, b.refuid,b.disp,b.disval,b.Taxableval,b.igstid,b.igstval,b.totvalue,b.addnotes,a.transp,isnull(chargeamount,0) as chargeamount,isnull(z.Transp,'') as dcno from stransactionsp a inner join Stransactionsplist b   on a.uid=b.transactionspuid  left join ItemM c on b.ItemUId=c.uid left join generalm d on c.uom_uid=d.uid  left join sTransactionsPcharges e on a.uid=e.stransactionspuid left join Stransactionsplist x on b.refuid=x.uid left join Stransactionsp z on x.transactionspuid=z.uid where b.transactionspuid=" + uid + " group by a.uid,b.uid,c.ItemName,generalname,b.PRate,b.pqty,  b.BasicValue,b.ItemUId,b.refuid,b.disp,b.disval,b.Taxableval,b.igstid,b.igstval,b.totvalue,b.addnotes,a.transp,chargeamount,z.Transp) a left join (select c.tpuid,c.itid,c.reqdate from STransactionsP a inner join STransactionsPlist b on a.uid=b.TransactionsPUId and a.Doctypeid=80 and a.companyid=3 left join Ordreqqty c on a.uid=c.Tpuid and b.ItemUId=c.itid)b on a.hdid=b.tpuid and a.ItemUId=b.itid ";
                //Genclass.strsql = " select distinct b.uid,c.ItemName,d.generalname as uom,b.PRate,b.pqty,b.BasicValue,b.ItemUId, b.refuid,b.disp,b.disval,b.Taxableval,b.igstid,b.igstval,b.totvalue,b.addnotes,a.transp,isnull(chargeamount,0) as chargeamount,z. from stransactionsp a inner join Stransactionsplist b   on a.uid=b.transactionspuid  left join ItemM c on b.ItemUId=c.uid left join generalm d on c.uom_uid=d.uid  left join sTransactionsPcharges e on a.uid=e.stransactionspuid left join Stransactionsplist x on b.refid=x.uid inner join Stransactionsp z on x.transactionspuid=z.uid where b.transactionspuid=" + uid + "   group by b.uid,c.ItemName,generalname,b.PRate,b.pqty,  b.BasicValue,b.ItemUId,b.refuid,b.disp,b.disval,b.Taxableval,b.igstid,b.igstval,b.totvalue,b.addnotes,a.transp,chargeamount";

                Genclass.cmd = new SqlCommand(Genclass.strsql, conn);
                SqlDataAdapter aptr1 = new SqlDataAdapter(Genclass.cmd);
                DataTable tap1 = new DataTable();
                aptr1.Fill(tap1);
                //txtname.Text = tap1.Rows[0]["Name"].ToString();
                //txtpuid.Text = tap1.Rows[0]["Partyuid"].ToString();
                Genclass.sum1 = 0;
                Txttot.Text = "";
                for (int k = 0; k < tap1.Rows.Count; k++)
                {
                    var index = HFIT.Rows.Add();
                    HFIT.Rows[index].Cells[0].Value = tap1.Rows[k]["itemname"].ToString();
                    HFIT.Rows[index].Cells[1].Value = tap1.Rows[k]["UOM"].ToString();
                    HFIT.Rows[index].Cells[2].Value = tap1.Rows[k]["PRate"].ToString();
                    HFIT.Rows[index].Cells[3].Value = tap1.Rows[k]["pqty"].ToString();
                    HFIT.Rows[index].Cells[4].Value = tap1.Rows[k]["BasicValue"].ToString();
                    HFIT.Rows[index].Cells[5].Value = tap1.Rows[k]["addnotes"].ToString();
                    HFIT.Rows[index].Cells[6].Value = tap1.Rows[k]["itemuid"].ToString();
                    //HFIT.Rows[index].Cells[7].Value = tap1.Rows[k]["disp"].ToString();
                    //HFIT.Rows[index].Cells[8].Value = tap1.Rows[k]["disval"].ToString();

                    HFIT.Rows[index].Cells[7].Value = tap1.Rows[k]["igstid"].ToString();
                    HFIT.Rows[index].Cells[8].Value = tap1.Rows[k]["igstval"].ToString();
                    HFIT.Rows[index].Cells[9].Value = tap1.Rows[k]["transp"].ToString();
                    //HFIT.Rows[index].Cells[12].Value = tap1.Rows[k]["Sgstid"].ToString();
                    //HFIT.Rows[index].Cells[13].Value = tap1.Rows[k]["Sgstval"].ToString();
                    HFIT.Rows[index].Cells[10].Value = tap1.Rows[k]["refuid"].ToString();
                    HFIT.Rows[index].Cells[12].Value = tap1.Rows[k]["totvalue"].ToString();
                    HFIT.Rows[index].Cells[13].Value = tap1.Rows[k]["dcno"].ToString();
                    HFIT.Rows[index].Cells[14].Value = tap1.Rows[k]["reqdate"].ToString();

                    //HFIT.Rows[index].Cells[0].Value = tap1.Rows[i]["itemname"].ToString();
                    //// double sump1 =  Convert.ToDouble(tap1.Rows[i]["width"].ToString());
                    ////HFIT.Rows[index].Cells[1].Value = sump1.ToString("0.000");
                    ////  double sumq1 =  Convert.ToDouble(tap1.Rows[i]["length"].ToString());
                    ////  HFIT.Rows[index].Cells[2].Value = sumq1.ToString("0.00");


                    //HFIT.Rows[index].Cells[1].Value = tap1.Rows[i]["UOM"].ToString();

                    //double sump = Convert.ToDouble(tap1.Rows[i]["Price"].ToString());
                    //HFIT.Rows[index].Cells[2].Value = sump.ToString("0.000");
                    //double sumq = Convert.ToDouble(tap1.Rows[i]["qty"].ToString());
                    //HFIT.Rows[index].Cells[3].Value = sumq.ToString("0.00");
                    //double sumb = Convert.ToDouble(tap1.Rows[i]["BasicValue"].ToString());
                    //HFIT.Rows[index].Cells[4].Value = sumb.ToString("0.00");

                    //HFIT.Rows[index].Cells[5].Value = txtnotes.Text;

                    //HFIT.Rows[index].Cells[6].Value = tap1.Rows[i]["uid"].ToString();
                    //HFIT.Rows[index].Cells[7].Value = tap1.Rows[i]["gstper"].ToString();

                    //HFIT.Rows[index].Cells[8].Value = tap1.Rows[i]["gstval"].ToString();
                    //HFIT.Rows[index].Cells[9].Value = tap1.Rows[i]["docno"].ToString();

                    //HFIT.Rows[index].Cells[10].Value = tap1.Rows[i]["listuid"].ToString();
                    //HFIT.Rows[index].Cells[11].Value = tap1.Rows[i]["hsnid"].ToString();
                    //HFIT.Rows[index].Cells[12].Value = tap1.Rows[i]["total"].ToString();

                    Genclass.sum1 = Genclass.sum1 + Convert.ToDouble(HFIT.Rows[k].Cells[4].Value);
                    Txttot.Text = Genclass.sum1.ToString();
                    //txtexcise.Text = Genclass.sum1.ToString();
                }

                txttbval.Text = Txttot.Text;
                if (tap1.Rows.Count > 0)
                {
                    txttdis.Text = tap1.Rows[0]["disp"].ToString();

                    txttdisc.Text = tap1.Rows[0]["disval"].ToString();
                    txtcharges.Text = tap1.Rows[0]["chargeamount"].ToString();
                }





                Titlep();

                btnaddrcan.Visible = false;
                buttnfinbk.Visible = true;

                conn.Close();
            }
            conn.Open();
            {
                Genclass.strsql = "select * from Stransactionspcharges a left join generalm b on a.chargesuid=b.uid  where a.stransactionspuid=" + txtgrnid.Text + " ";

                Genclass.cmd = new SqlCommand(Genclass.strsql, conn);
                SqlDataAdapter aptr2 = new SqlDataAdapter(Genclass.cmd);
                DataTable tap2 = new DataTable();
                aptr2.Fill(tap2);
                if (tap2.Rows.Count > 0)
                {
                    txtcharges.Text = tap2.Rows[0]["chargeamount"].ToString();

                    double cour = Convert.ToDouble(tap2.Rows[0]["totalcharges"].ToString()) - Convert.ToDouble(tap2.Rows[0]["chargeamount"].ToString());
                    txtcoucharge.Text = cour.ToString();
                }
                //Genclass.sum1 = 0;
                //for (int j = 0; j < tap2.Rows.Count; j++)
                //{
                //    var index = HFGA.Rows.Add();
                //    HFGA.Rows[index].Cells[0].Value = tap2.Rows[j]["generalname"].ToString();
                //    HFGA.Rows[index].Cells[1].Value = tap2.Rows[j]["chargeamount"].ToString();
                //    HFGA.Rows[index].Cells[2].Value = tap2.Rows[j]["chargesuid"].ToString();


                //    Genclass.sum1 = Genclass.sum1 + Convert.ToDouble(HFGA.Rows[j].Cells[1].Value);
                //    txttotaddd.Text = Genclass.sum1.ToString();
                //}
                conn.Close();
            }
            conn.Open();
            {
                Genclass.strsql = "select * from STransactionsPterms   where stransactionspuid=" + txtgrnid.Text + " ";

                Genclass.cmd = new SqlCommand(Genclass.strsql, conn);
                SqlDataAdapter aptr3 = new SqlDataAdapter(Genclass.cmd);
                DataTable tap3 = new DataTable();
                aptr3.Fill(tap3);

                for (int g = 0; g < tap3.Rows.Count; g++)
                {
                    var index = HFGT.Rows.Add();
                    HFGT.Rows[index].Cells[0].Value = tap3.Rows[g]["termsuid"].ToString();
                    //HFGT.Rows[index].Cells[1].Value = tap3.Rows[g]["termsdesc"].ToString();
                    //HFGT.Rows[index].Cells[2].Value = tap3.Rows[g]["termsuid"].ToString();


                }
                conn.Close();
                double val2 = Convert.ToDouble(txtexcise.Text) + Convert.ToDouble(txtcharges.Text);
                TxtNetAmt.Text = val2.ToString();
                Double net1 = Convert.ToDouble(txtttot.Text);
                TxtNetAmt.Text = net1.ToString("0.00");
                double someInt = (int)net1;

                double rof = Math.Round(net1 - someInt, 2);
                TxtRoff.Text = rof.ToString("0.00");

                if (Convert.ToDouble(TxtRoff.Text) < 0.49)
                {
                    Double rof1 = -1 * Convert.ToDouble(TxtRoff.Text);
                    TxtRoff.Text = rof1.ToString("0.00");
                }
                else
                {
                    Double rof2 = 1 - Convert.ToDouble(TxtRoff.Text);
                    TxtRoff.Text = rof2.ToString("0.00");
                }

                Double net = Convert.ToDouble(TxtNetAmt.Text) + Convert.ToDouble(TxtRoff.Text);
                //int ne=Convert.ToInt16(net);
                TxtNetAmt.Text = net.ToString("0.00");
            }
        }

        private void buttnext1_Click(object sender, EventArgs e)
        {
            this.Dispose();
        }

        private void HFGP_CellMouseUp(object sender, DataGridViewCellMouseEventArgs e)
        {
            if (HFGP.Rows[0].Cells[0].Value.ToString() != "")
            {
                int index = HFGP.SelectedRows[0].Index;
                if (index < HFGP.Rows.Count - 1)
                {
                    index = index + 1;
                    int index1 = HFGP.Rows.Count - 1;
                    lblno1.Text = index.ToString();
                    lblno2.Text = "of " + index1.ToString();
                }
            }
        }

        private void HFGP_KeyUp(object sender, KeyEventArgs e)
        {
            if (HFGP.Rows[0].Cells[0].Value.ToString() != "")
            {
                int index = HFGP.SelectedRows[0].Index;
                if (index < HFGP.Rows.Count - 1)
                {
                    index = index + 1;
                    int index1 = HFGP.Rows.Count - 1;
                    lblno1.Text = index.ToString();
                    lblno2.Text = "of " + index1.ToString();
                }
            }
        }

        private void buttnnvfst_Click(object sender, EventArgs e)
        {
            int index = HFGP.SelectedRows[0].Index;
            int index1 = 0;
            int index2 = HFGP.Rows.Count - 1;

            HFGP.Rows[index].Selected = false;
            HFGP.Rows[index1].Selected = true;
            index1 = index1 + 1;
            lblno1.Text = index1.ToString();
            lblno2.Text = "of " + index2.ToString();
        }

        private void buttnnxtlft_Click(object sender, EventArgs e)
        {
            int index = HFGP.SelectedRows[0].Index;
            int index1 = HFGP.Rows.Count - 1; ;
            if (index > 0)
            {
                HFGP.Rows[index].Selected = false;
                HFGP.Rows[index - 1].Selected = true;
                lblno1.Text = index.ToString();
                lblno2.Text = "of " + index1.ToString();
            }
        }

        private void buttrnxt_Click(object sender, EventArgs e)
        {
            int index = HFGP.SelectedRows[0].Index;
            int index2 = HFGP.SelectedRows[0].Index + 2;
            int index1 = HFGP.Rows.Count - 1; ;
            if (index < HFGP.Rows.Count - 2)
            {
                HFGP.Rows[index].Selected = false;
                HFGP.Rows[index + 1].Selected = true;
                lblno1.Text = index2.ToString();
                lblno2.Text = "of " + index1.ToString();

            }
        }

        private void btnfinnxt_Click(object sender, EventArgs e)
        {
            int index = HFGP.SelectedRows[0].Index;
            int index1 = HFGP.Rows.Count - 1;


            HFGP.Rows[index].Selected = false;

            HFGP.Rows[index1 - 1].Selected = true;
            lblno1.Text = index1.ToString();
            lblno2.Text = "of " + index1.ToString();

        }

        private void txtscr6_TextChanged(object sender, EventArgs e)
        {
            Loadgrid();
        }
        private void TotalAmount()
        {
            conn.Close();
            conn.Open();
            DateTime str9 = Convert.ToDateTime(dtpfnt.Text);
            if (Genclass.Dtype == 40)
            {
                if (chkact.Checked == true)
                {
                    txttotamt.Text = "0";
                    txtbasicval.Text = "0";
                    txttax.Text = "0";
                    Genclass.strsql = "Select  isnull(sum(a.Netvalue),0) as totamt from stransactionsp a inner join  partym b on a.partyuid=b.uid left join  partym c on a.placeuid=c.uid where a.active=1 and a.doctypeid=40 and a.companyid=" + Genclass.data1 + "   and month(docdate)=" + str9.Month + "  and year(docdate)=" + str9.Year + " ";
                    //Genclass.strsql = "Select  isnull(sum(a.Netvalue),0) as totamt from stransactionsp a inner join  partym b on a.partyuid=b.uid  left join  partym c on a.placeuid=c.uid left join stransactionsplist d on a.uid=d.transactionspuid where a.active=1 and a.doctypeid=80 and a.companyid=" + Genclass.data1 + " and  " + Genclass.StrSrch + " and month(docdate)=" + str9.Month + "  and year(docdate)=" + str9.Year + "  group by a.Uid,DocNo,docdate,Dcno,b.Name,Netvalue,igstval,partyuid,a.placeuid,c.name,dtpre,dtrem,transp,vehno,a.remarks";
                    Genclass.cmd = new SqlCommand(Genclass.strsql, conn);
                    SqlDataAdapter aptr2 = new SqlDataAdapter(Genclass.cmd);
                    DataTable tap2 = new DataTable();
                    aptr2.Fill(tap2);
                    txttotamt.Text = tap2.Rows[0]["totamt"].ToString();


                    Genclass.strsql = "Select  isnull(SUM(d.basicvalue),0) as Basicvalue from stransactionsp a  left join stransactionsplist d on a.uid=d.transactionspuid where a.active=1 and a.doctypeid=40 and a.companyid=" + Genclass.data1 + " and month(docdate)=" + str9.Month + "  and year(docdate)=" + str9.Year + " ";
                    Genclass.cmd = new SqlCommand(Genclass.strsql, conn);
                    SqlDataAdapter aptr3 = new SqlDataAdapter(Genclass.cmd);
                    DataTable tap3 = new DataTable();
                    aptr3.Fill(tap3);

                    txtbasicval.Text = tap3.Rows[0]["Basicvalue"].ToString();


                    //Genclass.strsql = "Select  isnull(d.igstval,0) as Taxval from stransactionsp a inner join  partym b on a.partyuid=b.uid  left join  partym c on a.placeuid=c.uid left join stransactionsplist d on a.uid=d.transactionspuid where a.active=1 and a.doctypeid=80 and a.companyid=" + Genclass.data1 + " and  " + Genclass.StrSrch + " and month(docdate)=" + str9.Month + "  and year(docdate)=" + str9.Year + "  group by a.Uid,DocNo,docdate,Dcno,b.Name,Netvalue,igstval,partyuid,a.placeuid,c.name,dtpre,dtrem,transp,vehno,a.remarks";
                    Genclass.strsql = "select sum(igstval) as Taxval from (Select distinct Convert(decimal(18,2),(isnull(SUM(d.basicvalue),0)*ISNULL(i.f1,0))/100) as igstval,a.uid from stransactionsp a  left join stransactionsplist d on a.uid=d.transactionspuid inner join  itemm g on d.ItemUId=g.uid left join generalm h on g.uom_uid=h.uid left join ItemGroup x on g.itemgroup_Uid=x.uid left join hsndet f on x.hsnid=f.uid  left join generalm i on f.Sgid=i.uid  where a.active=1 and a.doctypeid=40 and a.companyid=" + Genclass.data1 + "  and month(docdate)=" + str9.Month + "  and year(docdate)=" + str9.Year + " group by i.f1,d.basicvalue,a.uid) tab ";

                    Genclass.cmd = new SqlCommand(Genclass.strsql, conn);
                    SqlDataAdapter aptr4 = new SqlDataAdapter(Genclass.cmd);
                    DataTable tap4 = new DataTable();
                    aptr4.Fill(tap4);

                    txttax.Text = tap4.Rows[0]["Taxval"].ToString();

                    Genclass.strsql = "Select  isnull(SUM(e.totalcharges),0) as charges from stransactionsp a  left join stransactionsplist d on a.uid=d.transactionspuid  left join  STransactionsPCharges e on a.uid=e.STransactionsPUid   where a.active=1 and a.doctypeid=40 and a.companyid=" + Genclass.data1 + "  and month(docdate)=" + str9.Month + "  and year(docdate)=" + str9.Year + " ";

                    Genclass.cmd = new SqlCommand(Genclass.strsql, conn);
                    SqlDataAdapter aptr5 = new SqlDataAdapter(Genclass.cmd);
                    DataTable tap5 = new DataTable();
                    aptr5.Fill(tap5);
                    if (tap5.Rows.Count > 0)
                    {
                        txtchargessum.Text = tap5.Rows[0]["charges"].ToString();
                    }

                    else
                    {
                        txtchargessum.Text = "0.00";
                    }



                }
                else
                {

                    txttotamt.Text = "0";
                    Genclass.strsql = "Select  isnull(sum(a.Netvalue),0) as totamt,isnull(SUM(d.basicvalue),0) as Basicvalue,isnull(d.igstval,0) as Taxval from stransactionsp a inner join  partym b on a.partyuid=b.uid left join  partym c on a.placeuid=c.uid where a.active=0 and a.doctypeid=40 and a.companyid=" + Genclass.data1 + "  and  " + Genclass.StrSrch + " and month(docdate)=" + str9.Month + "  and year(docdate)=" + str9.Year + "";
                    Genclass.cmd = new SqlCommand(Genclass.strsql, conn);
                    SqlDataAdapter aptr2 = new SqlDataAdapter(Genclass.cmd);
                    DataTable tap2 = new DataTable();
                    aptr2.Fill(tap2);
                    txttotamt.Text = tap2.Rows[0]["totamt"].ToString();
                    txtbasicval.Text = tap2.Rows[0]["Basicvalue"].ToString();
                    txttax.Text = tap2.Rows[0]["Taxval"].ToString();
                }
            }
            conn.Close();
        }
        private void btnadd_Click(object sender, EventArgs e)
        {

            for (int i = 0; i < HFGA.RowCount - 1; i++)
            {
                if (HFGA.Rows[i].Cells[2].Value.ToString() == txtaddid.Text)
                {
                    MessageBox.Show("Charge Already Added");
                    txtaddcharge.Text = "";
                    txtcharges.Text = "";
                    txtaddcharge.Focus();
                    return;
                }
            }

            HFGA.AllowUserToAddRows = true;
            HFGA.AutoGenerateColumns = true;
            var index = HFGA.Rows.Add();
            HFGA.Rows[index].Cells[0].Value = txtaddcharge.Text;
            HFGA.Rows[index].Cells[1].Value = txtcharges.Text;
            HFGA.Rows[index].Cells[2].Value = txtaddid.Text;




            Genclass.sum1 = Genclass.sum1 + Convert.ToDouble(txtcharges.Text);
            txttotaddd.Text = Genclass.sum1.ToString();
            txttotc.Text = Genclass.sum1.ToString();
            splittax();
            txtaddcharge.Text = "";
            txtcharges.Text = "";
            txtaddcharge.Focus();
        }



        private void button11_Click_2(object sender, EventArgs e)
        {

            if (Txttot.Text == "" || Txttot.Text == null)
            {
                //MessageBox.Show("Enter the item to proceed");
                //return;
            }

            Taxpan.Visible = true;
            btnsave.Visible = false;
            btnaddrcan.Visible = false;

            txtexcise.Text = Txttot.Text;
            //button9.Visible = false;
            addipan.Visible = false;
            //termspan.Visible = true;
            button11.Visible = false;
            button12.Visible = false;
            btnsave.Visible = true;
            buttnnxt.Visible = false;
            buttnfinbk.Visible = true;
            HFGT.Visible = false;
            label34.Visible = false;
            Editpan.Visible = true;
            txtexcise.Text = Txttot.Text;
            //Titletax();
            Genclass.sum1 = 0;
            conn.Open();
            //qur.CommandText = "delete from salestaxsplit ";
            //qur.ExecuteNonQuery();

            //for (int i = 0; i < HFIT.RowCount - 1; i++)
            //{
            //    qur.CommandText = "insert into salestaxsplit values(" + HFIT.Rows[i].Cells[15].Value + "," + HFIT.Rows[i].Cells[9].Value + "," + HFIT.Rows[i].Cells[10].Value + "," + HFIT.Rows[i].Cells[11].Value + ")";
            //    qur.ExecuteNonQuery();
            //}

            //Genclass.strsql = "select hsnid,SUM(taxableval) as taxableval,convert(decimal(18,2),gstper/2,105) as cgstper,convert(decimal(18,2),gstval/2,105) as cgstval,convert(decimal(18,2),gstper/2,105) as sgstper,convert(decimal(18,2),gstval/2,105) as sgstval from salestaxsplit group by hsnid,gstper,gstval";

            //Genclass.cmd = new SqlCommand(Genclass.strsql, conn);
            //SqlDataAdapter aptr1 = new SqlDataAdapter(Genclass.cmd);
            //DataTable tap1 = new DataTable();
            //aptr1.Fill(tap1);


            //for (int j = 0; j < tap1.Rows.Count; j++)
            //{
            //    var index = HFGTAX.Rows.Add();
            //    HFGTAX.Rows[index].Cells[0].Value = tap1.Rows[j]["taxableval"].ToString();
            //    HFGTAX.Rows[index].Cells[1].Value = tap1.Rows[j]["cgstper"].ToString();
            //    HFGTAX.Rows[index].Cells[2].Value = tap1.Rows[j]["cgstval"].ToString();
            //    HFGTAX.Rows[index].Cells[3].Value = tap1.Rows[j]["sgstper"].ToString();
            //    HFGTAX.Rows[index].Cells[4].Value = tap1.Rows[j]["Sgstval"].ToString();
            //    HFGTAX.Rows[index].Cells[5].Value = tap1.Rows[j]["hsnid"].ToString();
            //}



            //Titleterm();
            //if (HFGT.Rows[0].Cells[0].Value == "" || HFGT.Rows[0].Cells[0].Value == null)
            //{
            //    var index = HFGT.Rows.Add();
            //    HFGT.Rows[index].Cells[0].Value = "No.of Cases";

            //    var index1 = HFGT.Rows.Add();
            //    HFGT.Rows[index1].Cells[0].Value = "Box No/Size";
            //    var index2 = HFGT.Rows.Add();
            //    HFGT.Rows[index2].Cells[0].Value = "Gross Weight";
            //    var index3 = HFGT.Rows.Add();
            //    HFGT.Rows[index3].Cells[0].Value = "Carrier";
            //    var index4 = HFGT.Rows.Add();
            //    HFGT.Rows[index4].Cells[0].Value = "Destination";
            //    var index5 = HFGT.Rows.Add();
            //    HFGT.Rows[index5].Cells[0].Value = "LR/RR/RPPNO./DT";
            //}
            //HFGT.Columns[0].ReadOnly = true;

            //Txttot.Text = "4352.86";
            txttbval.Text = Txttot.Text;
            if (txttbval.Text != "")
            {
                splittax();
            }


            conn.Close();
        }

        private void splittax()
        {
            if (txttdis.Text == "" || txttdis.Text == null || txttdis.Text == "0")
            {
                txttprdval.Text = txttbval.Text;

            }
            else
            {
                double dis6 = Convert.ToDouble(txttbval.Text) / 100 * Convert.ToDouble(txttdis.Text);
                txttdisc.Text = dis6.ToString("0.00");

                double dis4 = Convert.ToDouble(txttbval.Text) - Convert.ToDouble(txttdisc.Text);
                txttprdval.Text = dis4.ToString("0.00");
            }

            if (txtcharges.Text == "" || txtcharges.Text == null)
            {

                if (txtcoucharge.Text == "" || txtcoucharge.Text == null)
                {
                    txtexcise.Text = txttprdval.Text;
                }
                else
                {
                    if (txtcharges.Text == "")
                    {
                        txtcharges.Text = "0";
                    }
                    if (txtcoucharge.Text == "")
                    {
                        txtcoucharge.Text = "0";
                    }
                    double dis4 = Convert.ToDouble(txttprdval.Text) + Convert.ToDouble(txtcharges.Text) + Convert.ToDouble(txtcoucharge.Text);
                    txtexcise.Text = dis4.ToString("0.00");
                }
            }

            else
            {
                if (txtcoucharge.Text == "")
                {
                    txtcoucharge.Text = "0";
                }

                if (txttprdval.Text == "")
                {
                    txttprdval.Text = "0";
                }
                double dis4 = Convert.ToDouble(txttprdval.Text) + Convert.ToDouble(txtcharges.Text) + Convert.ToDouble(txtcoucharge.Text);
                txtexcise.Text = dis4.ToString("0.00");
            }



            if (txtpuid.Text != "")
            {
                Genclass.strsql = "select generalname from partym a inner join generalm b on a.stateuid=b.uid where a.companyid=" + Genclass.data1 + " and a.uid=" + txtpuid.Text + "";

                Genclass.cmd = new SqlCommand(Genclass.strsql, conn);
                SqlDataAdapter aptr1 = new SqlDataAdapter(Genclass.cmd);
                DataTable tap1 = new DataTable();
                aptr1.Fill(tap1);

                //MessageBox.Show(tap1.Rows[0]["generalname"].ToString());
                if (tap1.Rows[0]["generalname"].ToString() == "Tamil Nadu 33")
                {
                    txttgstp.Text = HFIT.Rows[0].Cells[7].Value.ToString();
                    txttgstval.Text = HFIT.Rows[0].Cells[8].Value.ToString();
                    double dis4 = Convert.ToDouble(HFIT.Rows[0].Cells[7].Value.ToString()) / 2;
                    txttcgstp.Text = dis4.ToString("0.00");
                    txtsgstp.Text = dis4.ToString("0.00");
                    double dis3 = (Convert.ToDouble(txtexcise.Text) / 100) * (Convert.ToDouble(HFIT.Rows[0].Cells[7].Value.ToString()));
                    dis3 = dis3 / 2;
                    txttcgval.Text = dis3.ToString("0.00");
                    txttsgval.Text = dis3.ToString("0.00");
                    double dis5 = Convert.ToDouble(txtexcise.Text) + dis3 + dis3;
                    txtttot.Text = dis5.ToString("0.00");


                }
                else
                {
                    txttgstp.Text = HFIT.Rows[0].Cells[7].Value.ToString();
                    txttgstval.Text = HFIT.Rows[0].Cells[8].Value.ToString();
                    txtigstp.Text = HFIT.Rows[0].Cells[7].Value.ToString();
                    double dis3 = (Convert.ToDouble(txtexcise.Text) / 100) * (Convert.ToDouble(HFIT.Rows[0].Cells[7].Value.ToString()));
                    txtigval.Text = dis3.ToString("0.00");
                    double dis5 = Convert.ToDouble(txtexcise.Text) + dis3;
                    txtttot.Text = dis5.ToString("0.00");
                }




                Double net1 = Convert.ToDouble(txtttot.Text);
                TxtNetAmt.Text = net1.ToString("0.00");
                double someInt = (int)net1;

                double rof = Math.Round(net1 - someInt, 2);
                TxtRoff.Text = rof.ToString("0.00");

                if (Convert.ToDouble(TxtRoff.Text) < 0.49)
                {
                    Double rof1 = -1 * Convert.ToDouble(TxtRoff.Text);
                    TxtRoff.Text = rof1.ToString("0.00");
                }
                else
                {
                    Double rof2 = 1 - Convert.ToDouble(TxtRoff.Text);
                    TxtRoff.Text = rof2.ToString("0.00");
                }

                Double net = Convert.ToDouble(TxtNetAmt.Text) + Convert.ToDouble(TxtRoff.Text);
                //int ne=Convert.ToInt16(net);
                TxtNetAmt.Text = net.ToString("0.00");

                //TxtNetAmt.Text = TxtNetAmt.Text.Replace(",", "");
                //txttcgval.Text = txttcgval.Text.Replace(",", "");
                //txttsgval.Text = txttsgval.Text.Replace(",", "");
                //txtigval.Text = txtigval.Text.Replace(",", "");
                //txtttot.Text = txtttot.Text.Replace(",", "");
                //txttprdval.Text = txttprdval.Text.Replace(",", "");
                //txtexcise.Text = txtexcise.Text.Replace(",", "");
                //txttdisc.Text = txttdisc.Text.Replace(",", "");

            }
        }

        private void button12_Click_2(object sender, EventArgs e)
        {
            Editpan.Visible = false;
            Genpan.Visible = true;
            panadd.Visible = true;
            Loadgrid();
        }

        private void button7_Click(object sender, EventArgs e)
        {

        }

        private void btnaddrcan_Click_1(object sender, EventArgs e)
        {
            addipan.Visible = true;
            //termspan.Visible = false;
            Editpan.Visible = false;
            //button1.Visible = false;
            btnaddrcan.Visible = false;
            btnsave.Visible = false;
            buttnfinbk.Visible = true;
            buttnnxt.Visible = true;
        }


        private void txtqty_TextChanged(object sender, EventArgs e)
        {
            if (txtqty.Text != "")
            {
                double bval = Convert.ToDouble(txtqty.Text) * Convert.ToDouble(txtprice.Text);
                txtbval.Text = bval.ToString();
            }

        }



        private void buttnnxt_Click(object sender, EventArgs e)
        {
            //addipan.Visible = false;
            termspan.Visible = true;
            //buttnfinbk.Visible = false;
            //buttnnxt.Visible = false;
            //btnaddrcan.Visible = true;
            //btnsave.Visible = true;

            HFGT.Visible = true;

            button17.Visible = true;
            button14.Visible = false;
            buttnfinbk.Visible = false;
            btnsave.Visible = false;


            //termspan.Visible = true;
            //if (HFGT.Rows[0].Cells[0].Value == "" || HFGT.Rows[0].Cells[0].Value == null)
            //{
            //    var index = HFGT.Rows.Add();
            //    HFGT.Rows[index].Cells[0].Value = "No.of Cases";

            //    var index1 = HFGT.Rows.Add();
            //    HFGT.Rows[index1].Cells[0].Value = "Box No/Size";
            //    var index2 = HFGT.Rows.Add();
            //    HFGT.Rows[index2].Cells[0].Value = "Gross Weight";
            //    var index3 = HFGT.Rows.Add();
            //    HFGT.Rows[index3].Cells[0].Value = "Carrier";
            //    var index4 = HFGT.Rows.Add();
            //    HFGT.Rows[index4].Cells[0].Value = "Destination";
            //    var index5 = HFGT.Rows.Add();
            //    HFGT.Rows[index5].Cells[0].Value = "LR/RR/RPPNO./DT";
            //}
            //HFGT.Columns[0].ReadOnly = true;
        }

        private void buttnfinbk_Click(object sender, EventArgs e)
        {
            addipan.Visible = false;
            Taxpan.Visible = false;

            buttnfinbk.Visible = false;
            buttnnxt.Visible = false;

            panadd.Visible = true;
            Editpan.Visible = false;
            Genpan.Visible = true;

            TxtNetAmt.Text = Txttot.Text;


        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            conn.Close();
            conn.Open();

            qur.CommandText = "delete from  NotoWords";
            qur.ExecuteNonQuery();

            Int64 NumVal = Convert.ToInt64(HFGP.CurrentRow.Cells[9].Value);
            string Nw = Rupees(NumVal);

            qur.CommandText = "Insert into NotoWords values('" + Nw + "',1)";
            qur.ExecuteNonQuery();
            int ptid = 0;
            if (Genclass.data1 == 3)
            {
                ptid = 5;
            }
            else
            {
                ptid = 4;
            }
            Genclass.Prtid = Convert.ToInt16(HFGP.CurrentRow.Cells[0].Value.ToString());


            for (int i = 1; i < ptid; i++)
            {
                Genclass.slno = i;
                Crviewer crv = new Crviewer();

                Genclass.strsql = "select * from Vw_salprtexlfin   where muid=" + Genclass.Prtid + " and Companyid=" + Genclass.data1 + "";

                Genclass.cmd = new SqlCommand(Genclass.strsql, conn);
                SqlDataAdapter aptr3 = new SqlDataAdapter(Genclass.cmd);
                DataTable tap3 = new DataTable();
                aptr3.Fill(tap3);
                if (tap3.Rows.Count > 0)
                {
                    if (tap3.Rows[0]["bstate"].ToString() == "Tamil Nadu 33")
                    {
                        if (tap3.Rows[0]["tag"].ToString() == "0")
                        {
                            if (Genclass.data1 == 3 && Convert.ToInt32(tap3.Rows[0]["partyuid"].ToString()) == 1367)
                            {

                                Genclass.strsql = "Exec sp_pdffincsven  " + Genclass.Prtid + " ," + Genclass.slno + "," + Genclass.data1 + ",40";

                                Genclass.cmd = new SqlCommand(Genclass.strsql, conn);
                                SqlDataAdapter aptr5 = new SqlDataAdapter(Genclass.cmd);
                                DataTable tap5 = new DataTable();
                                aptr5.Fill(tap5);
                                if (tap5.Rows.Count > 0)
                                {

                                    SqlDataAdapter da = new SqlDataAdapter("sp_pdffincs", conn);
                                    da.SelectCommand.CommandType = CommandType.StoredProcedure;
                                    da.SelectCommand.Parameters.Add("@Muid", SqlDbType.Int).Value = Genclass.Prtid;
                                    da.SelectCommand.Parameters.Add("@slno", SqlDbType.Int).Value = Genclass.slno;
                                    da.SelectCommand.Parameters.Add("@companyid", SqlDbType.Int).Value = Genclass.data1;
                                    da.SelectCommand.Parameters.Add("@doctypeid", SqlDbType.Int).Value = 40;
                                    DataSet ds = new DataSet();
                                    da.Fill(ds, "salesinvoice");


                                    doc.Load(Application.StartupPath + "\\salesinvoiceven.rpt");

                                    SqlDataAdapter da1 = new SqlDataAdapter("terms", conn);
                                    da1.SelectCommand.CommandType = CommandType.StoredProcedure;
                                    da1.SelectCommand.Parameters.Add("@uid", SqlDbType.Int).Value = Genclass.Prtid;
                                    DataTable ds1 = new DataTable("Terms");
                                    da1.Fill(ds1);
                                    ds.Tables.Add(ds1);

                                    doc.Subreports["Termrpt1.rpt"].SetDataSource(ds1);
                                    doc.SetDataSource(ds);
                                    Cryview.ReportSource = doc;
                                }
                                else
                                {
                                    SqlDataAdapter da = new SqlDataAdapter("sp_pdf", conn);
                                    da.SelectCommand.CommandType = CommandType.StoredProcedure;
                                    da.SelectCommand.Parameters.Add("@Muid", SqlDbType.Int).Value = Genclass.Prtid;
                                    da.SelectCommand.Parameters.Add("@slno", SqlDbType.Int).Value = Genclass.slno;
                                    da.SelectCommand.Parameters.Add("@companyid", SqlDbType.Int).Value = Genclass.data1;
                                    da.SelectCommand.Parameters.Add("@doctypeid", SqlDbType.Int).Value = 40;
                                    DataSet ds = new DataSet();
                                    da.Fill(ds, "salesinvoice");

                                    if (Genclass.data1 == 1)
                                    {
                                        doc.Load(Application.StartupPath + "\\salesinvoice.rpt");
                                    }
                                    else
                                    {
                                        doc.Load(Application.StartupPath + "\\salesinvoiceven.rpt");
                                    }



                                    SqlDataAdapter da1 = new SqlDataAdapter("terms", conn);
                                    da1.SelectCommand.CommandType = CommandType.StoredProcedure;
                                    da1.SelectCommand.Parameters.Add("@uid", SqlDbType.Int).Value = Genclass.Prtid;
                                    DataTable ds1 = new DataTable("Terms");
                                    da1.Fill(ds1);
                                    ds.Tables.Add(ds1);

                                    doc.Subreports["Termrpt1.rpt"].SetDataSource(ds1);
                                    doc.SetDataSource(ds);
                                    Cryview.ReportSource = doc;
                                }


                            }
                            else
                            {

                                SqlDataAdapter da = new SqlDataAdapter("sp_pdf", conn);
                                da.SelectCommand.CommandType = CommandType.StoredProcedure;
                                da.SelectCommand.Parameters.Add("@Muid", SqlDbType.Int).Value = Genclass.Prtid;
                                da.SelectCommand.Parameters.Add("@slno", SqlDbType.Int).Value = Genclass.slno;
                                da.SelectCommand.Parameters.Add("@companyid", SqlDbType.Int).Value = Genclass.data1;
                                da.SelectCommand.Parameters.Add("@doctypeid", SqlDbType.Int).Value = 40;
                                DataSet ds = new DataSet();
                                da.Fill(ds, "salesinvoice");
                                //if (Genclass.data1 == 1)
                                //{
                                //    SqlDataAdapter da1 = new SqlDataAdapter("terms", conn);
                                //    da1.SelectCommand.CommandType = CommandType.StoredProcedure;
                                //    da1.SelectCommand.Parameters.Add("@uid", SqlDbType.Int).Value = Genclass.Prtid;
                                //    DataSet ds1 = new DataSet();
                                //    da1.Fill(ds1, "terms");
                                //    //doc.SetDataSource(ds1);
                                //}
                                if (Genclass.data1 == 1)
                                {
                                    doc.Load(Application.StartupPath + "\\salesinvoice.rpt");
                                }
                                else
                                {
                                    doc.Load(Application.StartupPath + "\\salesinvoiceven.rpt");
                                }
                                //doc.Load(@"C:\Users\Admin\Desktop\Simta Trading\Subcontract\salesinvoice.rpt");
                                SqlDataAdapter da1 = new SqlDataAdapter("terms", conn);
                                da1.SelectCommand.CommandType = CommandType.StoredProcedure;
                                da1.SelectCommand.Parameters.Add("@uid", SqlDbType.Int).Value = Genclass.Prtid;
                                DataTable ds1 = new DataTable("Terms");
                                da1.Fill(ds1);
                                ds.Tables.Add(ds1);




                                doc.Subreports["Termrpt1.rpt"].SetDataSource(ds1);
                                doc.SetDataSource(ds);



                            }
                        }
                        else if (tap3.Rows[0]["tag"].ToString() == "1")
                        {
                            SqlDataAdapter da = new SqlDataAdapter("sp_pdfcgsg", conn);
                            da.SelectCommand.CommandType = CommandType.StoredProcedure;
                            da.SelectCommand.Parameters.Add("@Muid", SqlDbType.Int).Value = Genclass.Prtid;
                            da.SelectCommand.Parameters.Add("@slno", SqlDbType.Int).Value = Genclass.slno;
                            da.SelectCommand.Parameters.Add("@companyid", SqlDbType.Int).Value = Genclass.data1;
                            da.SelectCommand.Parameters.Add("@doctypeid", SqlDbType.Int).Value = 40;
                            DataSet ds = new DataSet();
                            da.Fill(ds, "salesinvoice");
                            if (Genclass.data1 == 1)
                            {
                                doc.Load(Application.StartupPath + "\\salesinvoicecgsg.rpt");
                            }
                            else
                            {
                                doc.Load(Application.StartupPath + "\\salesinvoiceven.rpt");
                            }
                            //d
                            //doc.Load(@"C:\Users\Admin\Desktop\Simta Trading\Subcontract\salesinvoice.rpt");
                            SqlDataAdapter da1 = new SqlDataAdapter("terms", conn);
                            da1.SelectCommand.CommandType = CommandType.StoredProcedure;
                            da1.SelectCommand.Parameters.Add("@uid", SqlDbType.Int).Value = Genclass.Prtid;
                            DataTable ds1 = new DataTable("Terms");
                            da1.Fill(ds1);
                            ds.Tables.Add(ds1);




                            doc.Subreports["Termrpt1.rpt"].SetDataSource(ds1);
                            doc.SetDataSource(ds);


                        }
                    }
                    else
                    {
                        if (tap3.Rows[0]["tag"].ToString() == "0")
                        {
                            SqlDataAdapter da = new SqlDataAdapter("sp_pdffincs", conn);
                            da.SelectCommand.CommandType = CommandType.StoredProcedure;
                            da.SelectCommand.Parameters.Add("@Muid", SqlDbType.Int).Value = Genclass.Prtid;
                            //da.SelectCommand.Parameters.Add("@type", SqlDbType.Int).Value = 2;
                            da.SelectCommand.Parameters.Add("@slno", SqlDbType.Int).Value = Genclass.slno;
                            da.SelectCommand.Parameters.Add("@companyid", SqlDbType.Int).Value = Genclass.data1;
                            da.SelectCommand.Parameters.Add("@doctypeid", SqlDbType.Int).Value = 40;
                            DataSet ds = new DataSet();
                            da.Fill(ds, "salesinvoice");
                            if (Genclass.data1 == 1)
                            {
                                doc.Load(Application.StartupPath + "\\salesinvoice.rpt");
                            }
                            else
                            {
                                doc.Load(Application.StartupPath + "\\salesinvoiceven.rpt");
                            }
                            //doc.Load(@"C:\Users\Admin\Desktop\Simta Trading\Subcontract\salesinvoice.rpt");
                            SqlDataAdapter da1 = new SqlDataAdapter("terms", conn);
                            da1.SelectCommand.CommandType = CommandType.StoredProcedure;
                            da1.SelectCommand.Parameters.Add("@uid", SqlDbType.Int).Value = Genclass.Prtid;
                            DataTable ds1 = new DataTable("Terms");
                            da1.Fill(ds1);
                            ds.Tables.Add(ds1);




                            doc.Subreports["Termrpt1.rpt"].SetDataSource(ds1);
                            doc.SetDataSource(ds);


                        }
                        else if (tap3.Rows[0]["tag"].ToString() == "1")
                        {
                            SqlDataAdapter da = new SqlDataAdapter("Sp_salprtexlfinigst", conn);
                            da.SelectCommand.CommandType = CommandType.StoredProcedure;
                            da.SelectCommand.Parameters.Add("@Muid", SqlDbType.Int).Value = Genclass.Prtid;
                            //da.SelectCommand.Parameters.Add("@type", SqlDbType.Int).Value = 2;
                            da.SelectCommand.Parameters.Add("@slno", SqlDbType.Int).Value = Genclass.slno;
                            da.SelectCommand.Parameters.Add("@companyid", SqlDbType.Int).Value = Genclass.data1;
                            da.SelectCommand.Parameters.Add("@doctypeid", SqlDbType.Int).Value = 40;
                            DataSet ds = new DataSet();
                            da.Fill(ds, "salesinvoice");
                            doc.Load(Application.StartupPath + "\\salesinvoiceIgst.rpt");
                            //doc.Load(@"C:\Users\Admin\Desktop\Simta Trading\Subcontract\salesinvoice.rpt");
                            SqlDataAdapter da1 = new SqlDataAdapter("terms", conn);
                            da1.SelectCommand.CommandType = CommandType.StoredProcedure;
                            da1.SelectCommand.Parameters.Add("@uid", SqlDbType.Int).Value = Genclass.Prtid;
                            DataTable ds1 = new DataTable("Terms");
                            da1.Fill(ds1);
                            ds.Tables.Add(ds1);




                            doc.Subreports["Termrpt1.rpt"].SetDataSource(ds1);
                            doc.SetDataSource(ds);


                        }
                    }






                    doc.PrintToPrinter(1, false, 0, 0);
                }
            }



            conn.Close();
        }



        public string Rupees(Int64 rup)
        {
            string result = "";
            Int64 res;
            if ((rup / 10000000) > 0)
            {
                res = rup / 10000000;
                rup = rup % 10000000;
                result = result + ' ' + RupeesToWords(res) + " Crore";
            }
            if ((rup / 100000) > 0)
            {
                res = rup / 100000;
                rup = rup % 100000;
                result = result + ' ' + RupeesToWords(res) + " Lakh";
            }
            if ((rup / 1000) > 0)
            {
                res = rup / 1000;
                rup = rup % 1000;
                result = result + ' ' + RupeesToWords(res) + " Thousand";
            }


            if ((rup / 100) > 0)
            {
                res = rup / 100;
                rup = rup % 100;
                if (rup > 0)
                {
                    result = result + ' ' + RupeesToWords(res) + " Hundred and";
                }
                else
                {
                    result = result + ' ' + RupeesToWords(res) + " Hundred";
                }
            }
            if ((rup % 10) >= 0)
            {
                res = rup % 100;
                result = result + " " + RupeesToWords(res);
            }
            result = "Rupees " + result + ' ' + "only";
            return result;
        }

        public string RupeesToWords(Int64 rup)
        {
            string result = "";
            if ((rup >= 1) && (rup <= 10))
            {
                if ((rup % 10) == 1) result = "One";
                if ((rup % 10) == 2) result = "Two";
                if ((rup % 10) == 3) result = "Three";
                if ((rup % 10) == 4) result = "Four";
                if ((rup % 10) == 5) result = "Five";
                if ((rup % 10) == 6) result = "Six";
                if ((rup % 10) == 7) result = "Seven";
                if ((rup % 10) == 8) result = "Eight";
                if ((rup % 10) == 9) result = "Nine";
                if ((rup % 10) == 0) result = "Ten";
            }
            if (rup > 9 && rup < 20)
            {
                if (rup == 11) result = "Eleven";
                if (rup == 12) result = "Twelve";
                if (rup == 13) result = "Thirteen";
                if (rup == 14) result = "Forteen";
                if (rup == 15) result = "Fifteen";
                if (rup == 16) result = "Sixteen";
                if (rup == 17) result = "Seventeen";
                if (rup == 18) result = "Eighteen";
                if (rup == 19) result = "Nineteen";
            }

            if (rup >= 20 && (rup / 10) == 2 && (rup % 10) == 0) result = "Twenty";
            if (rup > 20 && (rup / 10) == 3 && (rup % 10) == 0) result = "Thirty";
            if (rup > 20 && (rup / 10) == 4 && (rup % 10) == 0) result = "Forty";
            if (rup > 20 && (rup / 10) == 5 && (rup % 10) == 0) result = "Fifty";
            if (rup > 20 && (rup / 10) == 6 && (rup % 10) == 0) result = "Sixty";
            if (rup > 20 && (rup / 10) == 7 && (rup % 10) == 0) result = "Seventy";
            if (rup > 20 && (rup / 10) == 8 && (rup % 10) == 0) result = "Eighty";
            if (rup > 20 && (rup / 10) == 9 && (rup % 10) == 0) result = "Ninty";

            if (rup > 20 && (rup / 10) == 2 && (rup % 10) != 0)
            {
                if ((rup % 10) == 1) result = "Twenty One";
                if ((rup % 10) == 2) result = "Twenty Two";
                if ((rup % 10) == 3) result = "Twenty Three";
                if ((rup % 10) == 4) result = "Twenty Four";
                if ((rup % 10) == 5) result = "Twenty Five";
                if ((rup % 10) == 6) result = "Twenty Six";
                if ((rup % 10) == 7) result = "Twenty Seven";
                if ((rup % 10) == 8) result = "Twenty Eight";
                if ((rup % 10) == 9) result = "Twenty Nine";
            }
            if (rup > 20 && (rup / 10) == 3 && (rup % 10) != 0)
            {
                if ((rup % 10) == 1) result = "Thirty One";
                if ((rup % 10) == 2) result = "Thirty Two";
                if ((rup % 10) == 3) result = "Thirty Three";
                if ((rup % 10) == 4) result = "Thirty Four";
                if ((rup % 10) == 5) result = "Thirty Five";
                if ((rup % 10) == 6) result = "Thirty Six";
                if ((rup % 10) == 7) result = "Thirty Seven";
                if ((rup % 10) == 8) result = "Thirty Eight";
                if ((rup % 10) == 9) result = "Thirty Nine";
            }
            if (rup > 20 && (rup / 10) == 4 && (rup % 10) != 0)
            {
                if ((rup % 10) == 1) result = "Forty One";
                if ((rup % 10) == 2) result = "Forty Two";
                if ((rup % 10) == 3) result = "Forty Three";
                if ((rup % 10) == 4) result = "Forty Four";
                if ((rup % 10) == 5) result = "Forty Five";
                if ((rup % 10) == 6) result = "Forty Six";
                if ((rup % 10) == 7) result = "Forty Seven";
                if ((rup % 10) == 8) result = "Forty Eight";
                if ((rup % 10) == 9) result = "Forty Nine";
            }
            if (rup > 20 && (rup / 10) == 5 && (rup % 10) != 0)
            {
                if ((rup % 10) == 1) result = "Fifty One";
                if ((rup % 10) == 2) result = "Fifty Two";
                if ((rup % 10) == 3) result = "Fifty Three";
                if ((rup % 10) == 4) result = "Fifty Four";
                if ((rup % 10) == 5) result = "Fifty Five";
                if ((rup % 10) == 6) result = "Fifty Six";
                if ((rup % 10) == 7) result = "Fifty Seven";
                if ((rup % 10) == 8) result = "Fifty Eight";
                if ((rup % 10) == 9) result = "Fifty Nine";
            }
            if (rup > 20 && (rup / 10) == 6 && (rup % 10) != 0)
            {
                if ((rup % 10) == 1) result = "Sixty One";
                if ((rup % 10) == 2) result = "Sixty Two";
                if ((rup % 10) == 3) result = "Sixty Three";
                if ((rup % 10) == 4) result = "Sixty Four";
                if ((rup % 10) == 5) result = "Sixty Five";
                if ((rup % 10) == 6) result = "Sixty Six";
                if ((rup % 10) == 7) result = "Sixty Seven";
                if ((rup % 10) == 8) result = "Sixty Eight";
                if ((rup % 10) == 9) result = "Sixty Nine";
            }
            if (rup > 20 && (rup / 10) == 7 && (rup % 10) != 0)
            {
                if ((rup % 10) == 1) result = "Seventy One";
                if ((rup % 10) == 2) result = "Seventy Two";
                if ((rup % 10) == 3) result = "Seventy Three";
                if ((rup % 10) == 4) result = "Seventy Four";
                if ((rup % 10) == 5) result = "Seventy Five";
                if ((rup % 10) == 6) result = "Seventy Six";
                if ((rup % 10) == 7) result = "Seventy Seven";
                if ((rup % 10) == 8) result = "Seventy Eight";
                if ((rup % 10) == 9) result = "Seventy Nine";
            }
            if (rup > 20 && (rup / 10) == 8 && (rup % 10) != 0)
            {
                if ((rup % 10) == 1) result = "Eighty One";
                if ((rup % 10) == 2) result = "Eighty Two";
                if ((rup % 10) == 3) result = "Eighty Three";
                if ((rup % 10) == 4) result = "Eighty Four";
                if ((rup % 10) == 5) result = "Eighty Five";
                if ((rup % 10) == 6) result = "Eighty Six";
                if ((rup % 10) == 7) result = "Eighty Seven";
                if ((rup % 10) == 8) result = "Eighty Eight";
                if ((rup % 10) == 9) result = "Eighty Nine";
            }
            if (rup > 20 && (rup / 10) == 9 && (rup % 10) != 0)
            {
                if ((rup % 10) == 1) result = "Ninty One";
                if ((rup % 10) == 2) result = "Ninty Two";
                if ((rup % 10) == 3) result = "Ninty Three";
                if ((rup % 10) == 4) result = "Ninty Four";
                if ((rup % 10) == 5) result = "Ninty Five";
                if ((rup % 10) == 6) result = "Ninty Six";
                if ((rup % 10) == 7) result = "Ninty Seven";
                if ((rup % 10) == 8) result = "Ninty Eight";
                if ((rup % 10) == 9) result = "Ninty Nine";
            }
            return result;
        }

        //public string convertToString(int number)
        //{
        //    string[] numbers = { "Zero", "One", "Two", "Three", "Four", "Five", "Six", "Seven", "Eight", "Nine", "Ten", "Eleven", "Twelve", "Thirteen", "Fourteen", "Fifteen", "Sixteen", "Seventeen", "Eighteen", "Nineteen" };
        //    string[] tens = { "", "Ten", "Twenty", "Thirty", "Forty", "Fifty", "Sixty", "Seventy", "Eighty", "Ninety" };

        //    char[] digits = number.ToString().ToCharArray();
        //    string words = null;

        //    if (number >= 0 && number <= 19)
        //    {
        //        words = words + numbers[number];
        //    }
        //    else if (number >= 20 && number <= 99)
        //    {
        //        int firstDigit = (int)Char.GetNumericValue(digits[0]);
        //        int secondPart = number % 10;

        //        words = words + tens[firstDigit];

        //        if (secondPart > 0)
        //        {
        //            words = words + " " + convertToString(secondPart);
        //        }
        //    }
        //    else if (number >= 100 && number <= 999)
        //    {
        //        int firstDigit = (int)Char.GetNumericValue(digits[0]);
        //        int secondPart = number % 100;

        //        words = words + numbers[firstDigit] + " Hundred";

        //        if (secondPart > 0)
        //        {
        //            words = words + " and " + convertToString(secondPart);
        //        }
        //    }
        //    else if (number >= 1000 && number <= 19999)
        //    {
        //        int firstPart = (int)Char.GetNumericValue(digits[0]);
        //        if (number >= 10000)
        //        {
        //            string twoDigits = digits[0].ToString() + digits[1].ToString();
        //            firstPart = Convert.ToInt16(twoDigits);
        //        }
        //        int secondPart = number % 1000;

        //        words = words + numbers[firstPart] + " Thousand";

        //        if (secondPart > 0 && secondPart <= 99)
        //        {
        //            words = words + " and " + convertToString(secondPart);
        //        }
        //        else if (secondPart >= 100)
        //        {
        //            words = words + " " + convertToString(secondPart);
        //        }
        //    }
        //    else if (number >= 20000 && number <= 999999)
        //    {
        //        string firstStringPart = Char.GetNumericValue(digits[0]).ToString() + Char.GetNumericValue(digits[1]).ToString();

        //        if (number >= 100000)
        //        {
        //            firstStringPart = firstStringPart + Char.GetNumericValue(digits[2]).ToString();
        //        }

        //        int firstPart = Convert.ToInt16(firstStringPart);
        //        int secondPart = number - (firstPart * 1000);

        //        words = words + convertToString(firstPart) + " Thousand";

        //        if (secondPart > 0 && secondPart <= 99)
        //        {
        //            words = words + " and " + convertToString(secondPart);
        //        }
        //        else if (secondPart >= 100)
        //        {
        //            words = words + " " + convertToString(secondPart);
        //        }

        //    }
        //    else if (number == 100000)
        //    {
        //        words = words + "One Lakh";
        //    }
        //    return words;
        //}


        private void txtname_KeyDown_1(object sender, KeyEventArgs e)
        {

        }

        private void txtdcno_KeyDown_1(object sender, KeyEventArgs e)
        {
            //if (Genclass.Dtype == 40)
            //{
            //    if (e.KeyCode == Keys.Enter)
            //    {
            //        Genclass.type = 2;
            //        loadput();
            //    }
            //    else if (e.KeyCode == Keys.Escape)
            //    {
            //        txtdcno.Text = "";
            //        txtdcno.Focus();
            //    }
            //}

        }

        private void txtdcid_TextChanged_1(object sender, EventArgs e)
        {

            if (Genclass.Dtype == 40)
            {

                if (txtdcno.Text == "")
                {
                    return;

                }
                else
                {
                    Genclass.strsql = "select Itemname,UoM,itemuid,refid,qty,PRICE,convert(decimal(18,2),BasicValue,105) AS BasicValue,disper,convert(decimal(18,2),Disvalue,105) AS Disvalue,convert(decimal(18,2),Taxablevalue,105) AS Taxablevalue,convert(decimal(18,2),igstval,105) AS igstval,ig,0 as gid,0 as gt,convert(decimal(18,2),(BasicValue-(isnull(Disvalue,0))+ (isnull(igstval,0))),105) as total,hsnid from (select distinct   c.itemname,d.generalname as uom,b.itemuid,b.uid as refid,b.pqty-isnull(sum(e.pqty),0) as qty,f.Price ,(b.pqty-isnull(sum(e.pqty),0)) * f.Price as BasicValue,disper,((b.pqty-isnull(sum(e.pqty),0)) * f.Price/100)* disper as Disvalue,((b.pqty-isnull(sum(e.pqty),0)) * f.Price)-(((b.pqty-isnull(sum(e.pqty),0)) * f.Price/100)* disper) as Taxablevalue,i.f1 as ig,(((b.pqty-isnull(sum(e.pqty),0)) * f.Price)-(((b.pqty-isnull(sum(e.pqty),0)) * f.Price/100)* disper))/100 * i.f1 as igstval,c.hsnid from transactionsp a inner join transactionsplist b on  a.uid=b.transactionspuid and a.doctypeid=20 and a.companyid=1 inner join itemm c on b.itemuid=c.uid left join generalm d on c.uom_uid=d.uid  left join transactionsplist e on  b.uid=e.refuid and e.doctypeid=40 inner join pur_price_list f on c.uid=f.itemuid and c.partyuid=f.suppuid left join generalm i on f.igstid=i.uid where a.docno='" + txtdcno.Text + "' group by c.itemname,d.generalname,b.itemuid,b.uid,b.pqty,f.price,disper,i.f1,c.hsnid having b.pqty-isnull(sum(e.pqty),0) >0) tab";

                    Genclass.cmd = new SqlCommand(Genclass.strsql, conn);
                    SqlDataAdapter aptr1 = new SqlDataAdapter(Genclass.cmd);
                    DataTable tap1 = new DataTable();
                    aptr1.Fill(tap1);
                    //txtname.Text = tap1.Rows[0]["Name"].ToString();
                    //txtpuid.Text = tap1.Rows[0]["Partyuid"].ToString();
                    for (int i = 0; i < tap1.Rows.Count; i++)
                    {
                        var index = HFIT.Rows.Add();



                        HFIT.Rows[index].Cells[0].Value = tap1.Rows[i]["itemname"].ToString();
                        HFIT.Rows[index].Cells[1].Value = tap1.Rows[i]["UOM"].ToString();
                        HFIT.Rows[index].Cells[2].Value = tap1.Rows[i]["Price"].ToString();
                        HFIT.Rows[index].Cells[3].Value = tap1.Rows[i]["qty"].ToString();
                        HFIT.Rows[index].Cells[4].Value = tap1.Rows[i]["BasicValue"].ToString();
                        HFIT.Rows[index].Cells[5].Value = tap1.Rows[i]["itemuid"].ToString();
                        HFIT.Rows[index].Cells[6].Value = tap1.Rows[i]["refid"].ToString();
                        HFIT.Rows[index].Cells[7].Value = tap1.Rows[i]["disper"].ToString();
                        HFIT.Rows[index].Cells[8].Value = tap1.Rows[i]["Disvalue"].ToString();
                        HFIT.Rows[index].Cells[9].Value = tap1.Rows[i]["Taxablevalue"].ToString();
                        HFIT.Rows[index].Cells[10].Value = tap1.Rows[i]["ig"].ToString();
                        HFIT.Rows[index].Cells[11].Value = tap1.Rows[i]["igstval"].ToString();
                        HFIT.Rows[index].Cells[12].Value = tap1.Rows[i]["gid"].ToString();
                        HFIT.Rows[index].Cells[13].Value = tap1.Rows[i]["gt"].ToString();
                        HFIT.Rows[index].Cells[14].Value = tap1.Rows[i]["Total"].ToString();
                        HFIT.Rows[index].Cells[15].Value = tap1.Rows[i]["hsnid"].ToString();

                        Genclass.sum1 = Genclass.sum1 + Convert.ToDouble(HFIT.Rows[i].Cells[14].Value);
                        Txttot.Text = Genclass.sum1.ToString();




                        //Titlep();



                    }

                }


            }
            else
            {
                txtitemname.Focus();

            }
        }

        private void TxtNetAmt_TextChanged(object sender, EventArgs e)
        {

        }

        private void buttcusok_Click_1(object sender, EventArgs e)
        {


        }

        private void txtitemname_KeyDown_1(object sender, KeyEventArgs e)
        {

        }

        private void txtqty_TextChanged_1(object sender, EventArgs e)
        {

        }

        private void txtplace_KeyDown(object sender, KeyEventArgs e)
        {

        }

        private void txtcharges_TextChanged(object sender, EventArgs e)
        {

        }

        private void label16_Click(object sender, EventArgs e)
        {

        }

        private void TxtRoff_TextChanged(object sender, EventArgs e)
        {

        }

        private void label17_Click(object sender, EventArgs e)
        {

        }

        private void txttbval_TextChanged(object sender, EventArgs e)
        {

        }

        private void txttdis_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtprice_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtname_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                Genclass.fieldone = "";
                Genclass.fieldtwo = "";
                Genclass.fieldthree = "";
                Genclass.fieldFour = "";
                Genclass.fieldFive = "";
                if (Genclass.Dtype == 40)
                {
                    Genclass.Gbtxtid = 130;
                    Genclass.type = 1;
                    loadput();
                }
                else
                {
                    Genclass.type = 6;
                    loadput();
                }

            }
            else if (e.KeyCode == Keys.Escape)
            {
                txtname.Text = "";
                txtname.Focus();
            }

        }

        private void txtplace_KeyDown_1(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                Genclass.fieldone = "";
                Genclass.fieldtwo = "";
                Genclass.fieldthree = "";
                Genclass.fieldFour = "";
                Genclass.fieldFive = "";

                Genclass.type = 7;
                loadput();


            }
            else if (e.KeyCode == Keys.Escape)
            {
                txtplace.Text = "";
                txtplace.Focus();
            }
        }

        private void txtitemname_KeyDown(object sender, KeyEventArgs e)
        {

            //    if(e.KeyCode==Keys.Enter)
            //    {
            //         Genclass.fieldone = "";
            //        Genclass.fieldtwo = "";
            //        Genclass.fieldthree = "";
            //        Genclass.fieldFour = "";
            //        Genclass.fieldFive = "";

            //        Genclass.type = 5;
            //        loadput1();
            //}

        }

        private void buttcusok_Click(object sender, EventArgs e)
        {




            //Genclass.strsql = "select distinct Itemname,UoM,itemuid,0 as refid,qty,PRICE,convert(decimal(18,2),BasicValue,105) AS BasicValue,disper,convert(decimal(18,2),Disvalue,105) AS Disvalue,convert(decimal(18,2),Taxablevalue,105) AS Taxablevalue,convert(decimal(18,2),Cgstval,105) AS EDVAL,convert(decimal(18,2),sgstval,105) AS VATVAL,0 AS igstval,CG as ED,sg as VAT,0 as ig,convert(decimal(18,2),BasicValue-(isnull(Disvalue,0))+ (isnull(Cgstval,0)+isnull(sgstval,0)),105) as total from (select distinct   c.itemname,d.generalname as uom,b.itemuid,b.uid as refid," + txtqty.Text + " as qty," + txtprice.Text + " as Price," + txtbval.Text + " as BasicValue,disper,(" + txtbval.Text + " * disper)/100 as Disvalue," + txtbval.Text + "-((" + txtbval.Text + " * disper)/100) as Taxablevalue,g.f1 as CG,((" + txtbval.Text + "-((" + txtbval.Text + " * disper)/100)) * g.f1)/100 as Cgstval,h.f1 as SG,((" + txtbval.Text + "-((" + txtbval.Text + " * disper)/100)+ ((" + txtbval.Text + "-((" + txtbval.Text + " * disper)/100)) * g.f1)/100) * h.f1)/100 as sgstval from transactionsp a inner join transactionsplist b on  a.uid=b.transactionspuid and a.doctypeid=30 and a.companyid=1 inner join itemm c on b.itemuid=c.uid left join generalm d on c.uom_uid=d.uid   inner join pur_price_list f on c.uid=f.itemuid and c.partyuid=f.suppuid inner join generalm g on f.cgstid=g.uid inner join generalm h on f.sgstid=h.uid inner join generalm i on f.igstid=i.uid where c.uid=" + txttitemid.Text + " group by c.itemname,d.generalname,b.itemuid,b.uid,b.pqty,f.price,disper,g.f1,h.f1,i.f1) tab";
            //Genclass.strsql = "select distinct uid as itemuid,hsnid,Itemname,UoM,0 as refid," + txtqty.Text + " as qty,PRICE,convert(decimal(18,2),BasicValue,105) AS BasicValue,disper,convert(decimal(18,2),Disvalue,105) AS Disvalue,convert(decimal(18,2),Taxablevalue,105) AS Taxablevalue,gstper,gstval,convert(decimal(18,2),BasicValue-(isnull(Disvalue,0))+ (isnull(gstval,0)),105) as total from (select distinct   c.hsnid,c.itemname,d.generalname as uom,c.uid,10 as qty,42.00 as Price," + txtbval.Text + " as BasicValue,disper,(" + txtbval.Text + " * disper)/100 as Disvalue," + txtbval.Text + "-((" + txtbval.Text + " * disper)/100) as Taxablevalue,i.f1 as gstper,((" + txtqty.Text + " * f.Price)-(((" + txtqty.Text + " * f.Price)/100)* disper))/100 * i.f1 as gstval from  itemm c left join generalm d on c.uom_uid=d.uid   inner join pur_price_list f on c.uid=f.itemuid and c.partyuid=f.suppuid  left join generalm i on f.igstid=i.uid where c.uid=" + txttitemid.Text + " group by c.hsnid,c.itemname,d.generalname,c.uid,f.price,disper,i.f1) tab";
            if (Genclass.ty1 == 2)
            {
                if (Genclass.data1 == 1)
                {
                    Genclass.strsql = "select * from Pgsotmp";
                }
                else
                {
                    Genclass.strsql = "select * from sotmp";
                }

            }
            else
            {
                if (txtqty.Text == "" || txtqty.Text == "0" || txtprice.Text == "" || txtprice.Text == "0" || txtitemname.Text == "" || txtitemname.Text == "0" || txtname.Text == "" || txtname.Text == "0" || txttitemid.Text == "0")
                {
                    MessageBox.Show("Enter the Party or Item or Rate or Qty to Proceed");
                    return;
                }

                Genclass.strsql = "select distinct uid as itemuid,hsnid,Itemname,UoM,0 as refid,qty,PRICE,convert(decimal(18,2),BasicValue,105) AS BasicValue,disper,convert(decimal(18,2),Disvalue,105) AS Disvalue,convert(decimal(18,2),Taxablevalue,105) AS Taxablevalue,gstper,gstval,convert(decimal(18,2),BasicValue-(isnull(Disvalue,0))+ (isnull(gstval,0)),105) as total from (select distinct c.hsnid,c.itemname,d.generalname as uom,c.uid," + txtqty.Text + " as qty," + txtprice.Text + " as Price," + txtbval.Text + " as BasicValue,0 as disper,0 as Disvalue," + txtbval.Text + " as Taxablevalue,i.f1 as gstper,convert(decimal(18,2),(" + txtbval.Text + " /100 * i.f1),105) as gstval from  itemm c left join generalm d on c.uom_uid=d.uid left join ItemGroup j on c.itemgroup_Uid=j.UId left join Hsndet f on j.hsnid=f.uid left join generalm i on f.sgid=i.uid  where c.Uid=" + txttitemid.Text + " group by c.hsnid,c.itemname,d.generalname,c.uid,i.f1) tab";
            }
            Genclass.cmd = new SqlCommand(Genclass.strsql, conn);
            SqlDataAdapter aptr1 = new SqlDataAdapter(Genclass.cmd);
            DataTable tap1 = new DataTable();
            aptr1.Fill(tap1);

            for (int i = 0; i < tap1.Rows.Count; i++)
            {



                var index = HFIT.Rows.Add();
                HFIT.Rows[index].Cells[0].Value = tap1.Rows[i]["itemname"].ToString();



                HFIT.Rows[index].Cells[1].Value = tap1.Rows[i]["UOM"].ToString();

                double sump = Convert.ToDouble(tap1.Rows[i]["Price"].ToString());
                HFIT.Rows[index].Cells[2].Value = sump.ToString("0.000");
                double sumq = Convert.ToDouble(tap1.Rows[i]["qty"].ToString());
                HFIT.Rows[index].Cells[3].Value = sumq.ToString("0.00");
                double sumb = Convert.ToDouble(tap1.Rows[i]["BasicValue"].ToString());
                HFIT.Rows[index].Cells[4].Value = sumb.ToString("0.00");

                HFIT.Rows[index].Cells[5].Value = txtnotes.Text;

                HFIT.Rows[index].Cells[6].Value = tap1.Rows[i]["itemuid"].ToString();

                if (Genclass.ty1 == 2)
                {
                    HFIT.Rows[index].Cells[7].Value = tap1.Rows[i]["gstper"].ToString();
                    HFIT.Rows[index].Cells[8].Value = tap1.Rows[i]["gstval"].ToString();


                    HFIT.Rows[index].Cells[10].Value = tap1.Rows[i]["refid"].ToString();

                    if (Genclass.STR == "")
                    {
                        Genclass.STR = tap1.Rows[i]["refid"].ToString();
                    }
                    else
                    {
                        Genclass.STR = Genclass.STR + "," + tap1.Rows[i]["refid"].ToString();
                    }

                    //Genclass.STR = Genclass.STR + ")";
                    HFIT.Rows[index].Cells[13].Value = tap1.Rows[i]["pono"].ToString();
                    HFIT.Rows[index].Cells[14].Value = tap1.Rows[i]["Podate"].ToString();

                }
                else
                {
                    if (txt100.Text == "0")
                    {
                        HFIT.Rows[index].Cells[7].Value = tap1.Rows[i]["gstper"].ToString();
                        HFIT.Rows[index].Cells[8].Value = tap1.Rows[i]["gstval"].ToString();
                    }
                    else
                    {
                        HFIT.Rows[index].Cells[7].Value = 0;

                        HFIT.Rows[index].Cells[8].Value = 0;
                    }

                    if (txtlisid.Text == "")
                    {
                        HFIT.Rows[index].Cells[10].Value = 0;
                    }
                    else
                    {
                        HFIT.Rows[index].Cells[10].Value = txtlisid.Text;

                        if (Genclass.STR == "")
                        {
                            Genclass.STR = txtlisid.Text;
                        }
                        else
                        {
                            Genclass.STR = Genclass.STR + "," + txtlisid.Text;
                        }

                        //Genclass.STR = Genclass.STR + ")";
                    }
                }

                HFIT.Rows[index].Cells[9].Value = 0;


                HFIT.Rows[index].Cells[11].Value = tap1.Rows[i]["hsnid"].ToString();
                HFIT.Rows[index].Cells[12].Value = tap1.Rows[i]["total"].ToString();


                Genclass.sum1 = Genclass.sum1 + Convert.ToDouble(HFIT.Rows[index].Cells[4].Value);
                Txttot.Text = Genclass.sum1.ToString("0.00");


            }



            Titlep();
            txtitemname.Text = "";
            txttitemid.Text = "";
            txtprice.Text = "";
            txtqty.Text = "";
            txtnotes.Text = "";
            txtbval.Text = "";
            txtitemname.Focus();
            Genclass.sum1 = 0;
            for (int j = 0; j < HFIT.RowCount - 1; j++)
            {

                Genclass.sum1 = Genclass.sum1 + Convert.ToDouble(HFIT.Rows[j].Cells[4].Value);
                Txttot.Text = Genclass.sum1.ToString("#,0.00");
            }
            conn.Open();
            qur.CommandText = "delete from sotmp";
            qur.ExecuteNonQuery();
            conn.Close();
            button11_Click_2(sender, e);

        }

        private void txtprice_TextChanged_1(object sender, EventArgs e)
        {
            if (txtprice.Text != "" && txtqty.Text != "")
            {
                double add = Convert.ToDouble(txtprice.Text) * Convert.ToDouble(txtqty.Text);
                txtbval.Text = add.ToString("#,0.000");

                txtbval.Text = txtbval.Text.Replace(",", "");
            }

        }

        private void txtqty_TextChanged_2(object sender, EventArgs e)
        {
            if (txtqty.Text != "")
            {
                if (txtqty.Text != Convert.ToString("0.00"))
                {
                    if (Genclass.ty == 1)
                    {



                        //Genclass.strsql = "select a.UId,a.DocNo,isnull(b.PQty,0)-isnull(c.PQty,0)-isnull(d.PQty,0) as balqty,b.prate,f.ItemName,f.ItemCode from sTransactionsP a inner join sTransactionsPList b on a.UId=b.TransactionsPUId   and b.DocTypeID = 80   left join Stransactionsplist  d on b.uid=  d.Refuid  and d.doctypeid=40 left join  TransactionsPList c on b.uid=c.Refuid     and c.DocTypeID=110  left join Stransactionsplist e on c.Uid=e.refuid and e.doctypeid=40  left join itemm f on b.ItemUId=f.Uid and b.itemuid=" + txttitemid.Text + "      group  by   a.uid,a.docno,b.pqty,d.pqty,c.pqty,f.ItemName,f.ItemCode,b.prate having       isnull(b.PQty,0)-isnull(c.PQty,0)-isnull(d.PQty,0) >0";
                        Genclass.strsql = "select a.UId,c.uid as listid,a.DocNo,isnull(b.PQty,0)-isnull(c.PQty,0)-isnull(e.PQty,0)  as  balqty,c.prate,f.ItemName,f.ItemCode,f.uid as itemid from sTransactionsP a inner join sTransactionsPList b on a.UId=b.TransactionsPUId     and b.DocTypeID = 80   left join Stransactionsplist  d on b.uid=  d.Refuid  and d.doctypeid=40 left join    TransactionsPList c    on b.uid=c.Refuid  and c.DocTypeID=110  left join Stransactionsplist e on c.Uid=e.refuid    and e.doctypeid=40  left join     itemm f on c.ItemUId=f.Uid  where f.itemname is not null   and f.companyid=1   group  by   a.uid,a.docno,b.pqty,e.pqty,c.pqty,f.ItemName,c.uid,c.prate ,    f.ItemCode,f.uid having isnull(b.PQty,0)-isnull(c.PQty,0)-isnull(e.PQty,0) >0";


                        Genclass.cmd = new SqlCommand(Genclass.strsql, conn);
                        SqlDataAdapter aptr1 = new SqlDataAdapter(Genclass.cmd);
                        DataTable tap1 = new DataTable();
                        aptr1.Fill(tap1);
                        if (tap1.Rows.Count > 0)
                        {
                            txtite.Text = tap1.Rows[0]["balqty"].ToString();
                            if (txtqty.Text != txtite.Text)
                            {
                                if (Convert.ToDouble(txtqty.Text) > Convert.ToDouble(txtite.Text))
                                {
                                    MessageBox.Show("Qty Exceeds");
                                    txtqty.Text = "";
                                    txtqty.Focus();
                                    return;
                                }
                            }
                        }
                    }



                    else if (Genclass.Dtype == 1 && Genclass.ty1 == 2)
                    {


                        Genclass.strsql = "select a.UId,a.DocNo,isnull(b.PQty,0)-isnull(d.PQty,0) as balqty,b.prate,f.ItemName,f.ItemCode,b.uid as listid  from sTransactionsP a inner join sTransactionsPList b on a.UId=b.TransactionsPUId   and b.DocTypeID = 80   left join  Stransactionsplist  d on b.uid=  d.Refuid  and d.doctypeid=40   left join itemm f on     b.ItemUId=f.Uid  and b.DocTypeID=80 where  a.Partyuid=" + txtpuid.Text + " and a.companyid= " + Genclass.data1 + " and b.itemuid=" + txttitemid.Text + "   group  by   a.uid,a.docno,b.pqty,d.pqty,  f.ItemName,f.ItemCode,b.prate,    b.uid having       isnull(b.PQty,0)-isnull(d.PQty,0) >0 ";


                        Genclass.cmd = new SqlCommand(Genclass.strsql, conn);
                        SqlDataAdapter aptr1 = new SqlDataAdapter(Genclass.cmd);
                        DataTable tap1 = new DataTable();
                        aptr1.Fill(tap1);
                        if (tap1.Rows.Count > 0)
                        {
                            txtite.Text = tap1.Rows[0]["balqty"].ToString();
                            if (txtqty.Text != txtite.Text)
                            {
                                if (Convert.ToDouble(txtqty.Text) > Convert.ToDouble(txtite.Text))
                                {
                                    MessageBox.Show("Qty Exceeds");
                                    txtqty.Text = "";
                                    txtqty.Focus();
                                    return;
                                }
                            }
                        }
                    }

                    else if (txtprice.Text != "")
                    {
                        double add = Convert.ToDouble(txtprice.Text) * Convert.ToDouble(txtqty.Text);
                        txtbval.Text = add.ToString("#,0.00");
                        txtbval.Text = txtbval.Text.Replace(",", "");
                    }
                }
            }
        }

        private void txtname_TextChanged(object sender, EventArgs e)
        {

        }

        private void butcan_Click(object sender, EventArgs e)
        {
            string message = "Are you sure to cancel this Invoice ?";
            string caption = "Dilama";
            MessageBoxButtons buttons = MessageBoxButtons.YesNo;
            DialogResult result;

            result = MessageBox.Show(message, caption, buttons);

            if (result == System.Windows.Forms.DialogResult.Yes)
            {

                int i = HFGP.SelectedCells[0].RowIndex;
                uid = HFGP.Rows[i].Cells[0].Value.ToString();
                qur.CommandText = "Update stransactionsp set active=0 where uid=" + uid + "";
                qur.ExecuteNonQuery();
                MessageBox.Show("Invoice Cancelled");
            }
            Loadgrid();
        }

        private void HFIT_KeyDown(object sender, KeyEventArgs e)
        {
            int j = HFIT.SelectedCells[0].RowIndex;

            double sun = Convert.ToDouble(HFIT.Rows[j].Cells[4].Value.ToString());

            Genclass.sum1 = Convert.ToDouble(Txttot.Text);


            Genclass.sum1 = Genclass.sum1 - sun;

            Txttot.Text = Genclass.sum1.ToString("0.00");
        }

        private void txtpuid_TextChanged(object sender, EventArgs e)
        {
            if (txtpuid.Text != "")
            {

                Genclass.strsql = "Select address1, address2 ,city,tngst,generalname,a.tag  from partym a inner join generalm b on a.stateuid=b.uid where a.uid=" + txtpuid.Text + " and a.companyid=" + Genclass.data1 + "";



                Genclass.cmd = new SqlCommand(Genclass.strsql, conn);
                SqlDataAdapter aptr1 = new SqlDataAdapter(Genclass.cmd);
                DataTable tap1 = new DataTable();
                aptr1.Fill(tap1);

                if (tap1.Rows.Count > 0)
                {
                    txtpadd1.Text = tap1.Rows[0]["address1"].ToString() + ' ' + tap1.Rows[0]["address2"].ToString() + ' ' + tap1.Rows[0]["city"].ToString() + ' ' + tap1.Rows[0]["generalname"].ToString() + ' ' + tap1.Rows[0]["tngst"].ToString();
                    txtplace.Text = txtname.Text;
                    Genclass.cat = Convert.ToInt16(txtpuid.Text);
                    txtpluid.Text = txtpuid.Text;
                    txtpadd2.Text = txtpadd1.Text;
                    txt100.Text = tap1.Rows[0]["tag"].ToString();
                }

            }
        }

        private void txtpluid_TextChanged(object sender, EventArgs e)
        {
            if (txtpluid.Text != "")
            {

                Genclass.strsql = "Select address1, address2 ,city  from partym where uid=" + txtpluid.Text + " and companyid=" + Genclass.data1 + "";



                Genclass.cmd = new SqlCommand(Genclass.strsql, conn);
                SqlDataAdapter aptr1 = new SqlDataAdapter(Genclass.cmd);
                DataTable tap1 = new DataTable();
                aptr1.Fill(tap1);

                if (tap1.Rows.Count > 0)
                {
                    txtpadd2.Text = tap1.Rows[0]["address1"].ToString() + ' ' + tap1.Rows[0]["address2"].ToString() + ' ' + tap1.Rows[0]["city"].ToString();
                    //txtplace.Text = txtname.Text;
                    //txtpluid.Text = txtpuid.Text;
                    //txtpadd2.Text = txtpadd1.Text;
                }
            }
        }

        private void txtitemname_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtdcid_TextChanged(object sender, EventArgs e)
        {

            if (txtdcid.Text != "")
            {


                //Genclass.strsql = "select Docno,convert(varchar,Docdate,105) as Docdate,a.transp,c.hsnid,c.itemname,uom,c.uid,a.uid as Tuid,b.uid as Listuid,b.pqty-isnull(sum(z.pqty),0) as qty,b.PRate as Price,b.BasicValue,i.f1 as gstper,convert(decimal(18,2),(b.BasicValue /100 * i.f1),105) as gstval,b.width,b.length,b.nos from orderp a inner join orderplist b on a.uid=b.TransactionsPUId left join stransactionsplist z on b.Uid=z.Refuid inner join itemm c on b.ItemUId=c.uid  left join ItemGroup j on c.itemgroup_Uid=j.UId left join Hsndet f on j.hsnid=f.uid left join generalm i on f.sgid=i.uid where a.uid=" + txtdcid.Text + " group by Docno,Docdate,c.hsnid,c.itemname,c.uid,i.f1,b.pqty,b.PRate,b.BasicValue,i.f1,b.width,b.length,b.nos,uom,a.uid,a.transp,b.uid having b.pqty-isnull(sum(z.pqty),0)>0";
                Genclass.strsql = "select a.UId,a.DocNo,isnull(b.PQty,0)-isnull(c.PQty,0)-isnull(d.PQty,0) as balqty,b.prate,f.ItemName,f.ItemCode from sTransactionsP a inner join sTransactionsPList b on a.UId=b.TransactionsPUId   and b.DocTypeID = 80   left join Stransactionsplist  d on b.uid=  d.Refuid  and d.doctypeid=40 left join  TransactionsPList c on b.uid=c.Refuid     and c.DocTypeID=110  left join Stransactionsplist e on c.Uid=e.refuid and e.doctypeid=40  left join itemm f on b.ItemUId=f.Uid      group  by   a.uid,a.docno,b.pqty,d.pqty,c.pqty,f.ItemName,f.ItemCode,b.prate having       isnull(b.PQty,0)-isnull(c.PQty,0)-isnull(d.PQty,0) >0";


                Genclass.cmd = new SqlCommand(Genclass.strsql, conn);
                SqlDataAdapter aptr1 = new SqlDataAdapter(Genclass.cmd);
                DataTable tap1 = new DataTable();
                aptr1.Fill(tap1);

                txtqty.Text = tap1.Rows[0]["balqty"].ToString();
                txtprice.Text = tap1.Rows[0]["prate"].ToString();


                //for (int i = 0; i < tap1.Rows.Count; i++)
                //{


                //    var index = HFIT.Rows.Add();
                //    HFIT.Rows[index].Cells[0].Value = tap1.Rows[i]["itemname"].ToString();
                //    // double sump1 =  Convert.ToDouble(tap1.Rows[i]["width"].ToString());
                //    //HFIT.Rows[index].Cells[1].Value = sump1.ToString("0.000");
                //    //  double sumq1 =  Convert.ToDouble(tap1.Rows[i]["length"].ToString());
                //    //  HFIT.Rows[index].Cells[2].Value = sumq1.ToString("0.00");


                //    HFIT.Rows[index].Cells[1].Value = tap1.Rows[i]["UOM"].ToString();

                //    double sump = Convert.ToDouble(tap1.Rows[i]["Price"].ToString());
                //    HFIT.Rows[index].Cells[2].Value = sump.ToString("0.000");
                //    double sumq = Convert.ToDouble(tap1.Rows[i]["qty"].ToString());
                //    HFIT.Rows[index].Cells[3].Value = sumq.ToString("0.00");
                //    double sumb = Convert.ToDouble(tap1.Rows[i]["BasicValue"].ToString());
                //    HFIT.Rows[index].Cells[4].Value = sumb.ToString("0.00");

                //    HFIT.Rows[index].Cells[5].Value = txtnotes.Text;

                //    HFIT.Rows[index].Cells[6].Value = tap1.Rows[i]["uid"].ToString();
                //    HFIT.Rows[index].Cells[7].Value = tap1.Rows[i]["gstper"].ToString();

                //    HFIT.Rows[index].Cells[8].Value = tap1.Rows[i]["gstval"].ToString();
                //    HFIT.Rows[index].Cells[9].Value = tap1.Rows[i]["docno"].ToString();

                //    HFIT.Rows[index].Cells[10].Value = tap1.Rows[i]["listuid"].ToString();
                //    HFIT.Rows[index].Cells[11].Value = tap1.Rows[i]["hsnid"].ToString();
                //    HFIT.Rows[index].Cells[12].Value = tap1.Rows[i]["total"].ToString();



                //}



                //Titlep();
                //txtitemname.Text = "";
                //txtprice.Text = "";
                //txtqty.Text = "";
                //txtnotes.Text = "";
                //txtbval.Text = "";
                //txtitemname.Focus();

            }
            fun();
            button11_Click_2(sender, e);


        }

        private void fun()
        {
            for (int i = 0; i < HFIT.Rows.Count - 1; i++)
            {

                Genclass.sum1 = Genclass.sum1 + Convert.ToDouble(HFIT.Rows[i].Cells[4].Value);
                Txttot.Text = Genclass.sum1.ToString("0.00");

                //Genclass.sum2 = Genclass.sum2 + Convert.ToDouble(HFIT.Rows[i].Cells[2].Value);
                //txtprice.Text = Genclass.sum2.ToString("0.00");

                // Genclass.sum3 = Genclass.sum3 + Convert.ToDouble(HFIT.Rows[i].Cells[3].Value);
                //txtqty.Text = Genclass.sum3.ToString();

                //Genclass.sum4 = Genclass.sum4 + Convert.ToDouble(HFIT.Rows[i].Cells[4].Value);
                //txtbval.Text = Genclass.sum4.ToString("0.00");
            }
        }
        private void txttrans_TextChanged(object sender, EventArgs e)
        {

        }

        private void dtpfnt_ValueChanged(object sender, EventArgs e)
        {
            Loadgrid();
            TotalAmount();

        }

        private void Genpan_Paint(object sender, PaintEventArgs e)
        {

        }

        private void txttdis_TextChanged_1(object sender, EventArgs e)
        {
            if (txttdis.Text != "")
            {
                if (Convert.ToDouble(txttdis.Text) > 0)
                {
                    double dis = (Convert.ToDouble(txttbval.Text) / 100) * (Convert.ToDouble(txttdis.Text));
                    txttdisc.Text = dis.ToString();
                }
                else
                {
                    txttdisc.Text = "0";
                }
                splittax();
            }
        }

        private void txtcharges_TextChanged_1(object sender, EventArgs e)
        {
            splittax();
        }

        private void btnsave_Click_1(object sender, EventArgs e)
        {
            if (txtname.Text == "")
            {
                MessageBox.Show("Enter the Party");
                txtname.Focus();
            }

            if (conn.State != ConnectionState.Open)
            {
                conn.Open();
            }

            Txttot.Text = Txttot.Text.Replace(",", "");
            txtexcise.Text = txtexcise.Text.Replace(",", "");
            Genclass.Dtype = 40;

            if (mode == 2)
            {
                qur.CommandText = "delete from Stransactionsplist where transactionspuid=" + uid + "";
                qur.ExecuteNonQuery();
            }

            string orderno = "";

            if (Genclass.data1 == 3)
            {
                if (HFIT.Rows[0].Cells[10].Value.ToString() != "")
                {
                    string quy5 = "select transp + ' / ' + convert(varchar,dcdate,105) as docno from STransactionsPlist a left join STransactionsP b on a.transactionspuid=b.uid where a.doctypeid=80  and  a.uid=" + HFIT.Rows[0].Cells[10].Value + "";
                    Genclass.cmd = new SqlCommand(quy5, conn);
                    SqlDataAdapter aptr5 = new SqlDataAdapter(Genclass.cmd);
                    DataTable tap5 = new DataTable();
                    aptr5.Fill(tap5);
                    if (tap5.Rows.Count > 0)
                    {
                        //if (orderno == "")
                        //{
                        orderno = tap5.Rows[0]["docno"].ToString();
                        //}
                        //else
                        //{
                        //    orderno = orderno + ", " + tap5.Rows[0]["docno"].ToString();
                        //}

                        //txtsaleorderno.Text = txtsaleorderno.Text + "," + txtsaleorderno;

                    }
                }
            }

            for (int i = 0; i < HFIT.RowCount - 1; i++)
            {
                if (mode == 1)
                {
                    txttrans.Text = "";

                    if (Genclass.data1 == 1)
                    {
                        for (int j = 0; j < HFIT.RowCount - 1; j++)
                        {
                            if (HFIT.Rows[j].Cells[10].Value.ToString() != "")
                            {
                                string quy5 = "select transp +  ' / ' +  convert(varchar,dcdate,102) as docno from STransactionsPlist a left join STransactionsP b on a.transactionspuid=b.uid where a.doctypeid=80  and  a.uid=" + HFIT.Rows[j].Cells[10].Value + "";
                                Genclass.cmd = new SqlCommand(quy5, conn);
                                SqlDataAdapter aptr5 = new SqlDataAdapter(Genclass.cmd);
                                DataTable tap5 = new DataTable();
                                aptr5.Fill(tap5);
                                if (tap5.Rows.Count > 0)
                                {
                                    if (orderno == "")
                                    {
                                        orderno = tap5.Rows[0]["docno"].ToString();
                                    }
                                    else
                                    {
                                        orderno = orderno + ", " + tap5.Rows[0]["docno"].ToString();
                                    }

                                    txtsaleorderno.Text = txtsaleorderno.Text + "," + txtsaleorderno;

                                }
                            }

                        }
                    }


                    txttrans.Text = orderno;

                    //qur.CommandText = "Exec Sp_SalesINvoice " + Genclass.Dtype + ",'" + txtgrn.Text + "','" + DTPDOCDT.Value + "','" + txtdcno.Text + "','" + Dtpdt.Value + "'," + txtpuid.Text + ",'" + txtrem.Text + "',1," + Genclass.data1 + "," + HFIT.Rows[i].Cells[5].Value + " ," + HFIT.Rows[i].Cells[3].Value + " ," + HFIT.Rows[i].Cells[2].Value + "   ,   " + HFIT.Rows[i].Cells[4].Value + " , 0," + HFIT.Rows[i].Cells[7].Value + "   ,    " + HFIT.Rows[i].Cells[8].Value + "    ,0, 0 ,0, 0,  " + HFIT.Rows[i].Cells[10].Value + ", " + HFIT.Rows[i].Cells[11].Value + ",0," + i + "," + mode + "," + Txttot.Text + "," + TxtNetAmt.Text + "," + Genclass.Yearid + "," + HFIT.Rows[i].Cells[9].Value + "," + TxtRoff.Text + ",'" + Dtppre.Value + "','" + Dtprem.Value + "','" + txttrans.Text + "','" + txtveh.Text + "'," + txtpluid.Text + "";
                    if (i == 0)
                    {
                        qur.CommandText = "insert into Stransactionsp values(" + Genclass.Dtype + ",'" + txtgrn.Text + "','" + DTPDOCDT.Value + "','" + txtdcno.Text + "','" + Dtpdt.Value + "'," + txtpuid.Text + ",'" + txtrem.Text + "',1," + Genclass.data1 + "," + TxtNetAmt.Text + "," + Genclass.Yearid + "," + TxtRoff.Text + ",'" + Dtppre.Value + "','" + Dtprem.Value + "','0','" + txtveh.Text + "'," + txtpluid.Text + ",'" + txtorderno.Text + "')";
                        qur.ExecuteNonQuery();
                        qur.CommandText = "update Stransactionsp set transp='" + txttrans.Text + "' where docno='" + txtgrn.Text + "' and  doctypeid=" + Genclass.Dtype + " and companyid=" + Genclass.data1 + "";
                        qur.ExecuteNonQuery();
                    }
                    string quy = "select uid from STransactionsP where doctypeid=" + Genclass.Dtype + "  and  docno='" + txtgrn.Text + "' and companyid=" + Genclass.data1 + "";
                    Genclass.cmd = new SqlCommand(quy, conn);
                    SqlDataAdapter aptr = new SqlDataAdapter(Genclass.cmd);
                    DataTable tap = new DataTable();
                    aptr.Fill(tap);

                    txtgrnid.Text = tap.Rows[0]["uid"].ToString();

                    if (txttdis.Text == "0" || txttdis.Text == null || txttdis.Text == "")
                    {
                        qur.CommandText = "insert into Stransactionsplist values(" + Genclass.Dtype + "," + txtgrnid.Text + "," + HFIT.Rows[i].Cells[6].Value + " ," + HFIT.Rows[i].Cells[3].Value + " ," + HFIT.Rows[i].Cells[2].Value + " ," + HFIT.Rows[i].Cells[4].Value + ",  " + txtexcise.Text + "," + HFIT.Rows[i].Cells[10].Value + ",0,0,-1, 0 ,0, 0," + txttgstp.Text + ", " + txttgstval.Text + "," + mode + "," + TxtNetAmt.Text + ",'" + HFIT.Rows[i].Cells[5].Value + "')";
                        qur.ExecuteNonQuery();
                    }
                    else
                    {
                        Txttot.Text = Txttot.Text.Replace(",", "");
                        qur.CommandText = "insert into Stransactionsplist values(" + Genclass.Dtype + "," + txtgrnid.Text + "," + HFIT.Rows[i].Cells[6].Value + " ," + HFIT.Rows[i].Cells[3].Value + " ," + HFIT.Rows[i].Cells[2].Value + "   ,   " + HFIT.Rows[i].Cells[4].Value + ",  " + txtexcise.Text + " , " + HFIT.Rows[i].Cells[10].Value + "," + txttdis.Text + "   ,   " + txttdisc.Text + "    ,-1, 0 ,0, 0,  " + txttgstp.Text + ", " + txttgstval.Text + "," + mode + "," + TxtNetAmt.Text + ",'" + HFIT.Rows[i].Cells[5].Value + "')";
                        qur.ExecuteNonQuery();
                    }

                }


                if (mode == 2)
                {



                    qur.CommandText = "update Stransactionsp set docdate='" + DTPDOCDT.Text + "',DcNo='" + txtdcno.Text + "',dcdate='" + Dtpdt.Value + "',PartyUid=" + txtpuid.Text + ",remarks='" + txtrem.Text + "',active=1,companyid=" + Genclass.data1 + ",netvalue=" + TxtNetAmt.Text + ",placeuid=" + txtpluid.Text + ",transp='" + txttrans.Text + "',vehno='" + txtveh.Text + "',dtpre='" + Dtppre.Value + "',dtrem='" + Dtprem.Text + "',roff=" + TxtRoff.Text + ",orderdt='" + txtorderno.Text + "' where UId=" + uid + "";
                    qur.ExecuteNonQuery();
                    qur.CommandText = "insert into Stransactionsplist values(" + Genclass.Dtype + "," + txtgrnid.Text + "," + HFIT.Rows[i].Cells[6].Value + " ," + HFIT.Rows[i].Cells[3].Value + " ," + HFIT.Rows[i].Cells[2].Value + "   ,   " + HFIT.Rows[i].Cells[4].Value + ",  " + txtexcise.Text + " ,  " + HFIT.Rows[i].Cells[10].Value + "," + txttdis.Text + "   ,   " + txttdisc.Text + "    ,-1, 0 ,0, 0,  " + txttgstp.Text + ", " + txttgstval.Text + "," + mode + "," + TxtNetAmt.Text + ",'" + HFIT.Rows[i].Cells[5].Value + "')";
                    qur.ExecuteNonQuery();


                }
            }


            qur.CommandText = "delete from  STransactionsPterms where stransactionspuid=" + txtgrnid.Text + "";
            qur.ExecuteNonQuery();
            qur.CommandText = "delete from  Stransactionspcharges where stransactionspuid=" + txtgrnid.Text + "";
            qur.ExecuteNonQuery();

            if (txttdis.Text == "" || txttdis.Text == null)
            {
                txttdis.Text = "0";
                txttdisc.Text = "0";
            }

            for (int j = 0; j < HFGT.Rows.Count - 1; j++)
            {

                qur.CommandText = "insert into STransactionsPterms values (" + txtgrnid.Text + "," + Genclass.Dtype + ",'" + HFGT.Rows[j].Cells[0].Value + "',0,'0'," + Genclass.Yearid + ")";
                qur.ExecuteNonQuery();
            }


            if (txtcharges.Text != "")
            {


                if (txtcoucharge.Text == "" || txtcoucharge.Text == null)
                {
                    txtcoucharge.Text = "0.00";
                }
                double str3 = Convert.ToDouble(txtcharges.Text) + Convert.ToDouble(txtcoucharge.Text);

                qur.CommandText = "insert into Stransactionspcharges  values ( " + txtgrnid.Text + "," + Genclass.Dtype + ",13," + txtcharges.Text + "," + str3 + ",'0'," + Genclass.Yearid + ")";
                qur.ExecuteNonQuery();
            }

            if (mode == 1)
            {
                qur.CommandText = "update doctypem set lastno= lastno + 1 where doctypeid=" + Genclass.Dtype + " and companyid=" + Genclass.data1 + "";
                qur.ExecuteNonQuery();
            }

            Updtord();
            MessageBox.Show("Record has been saved", "Save", MessageBoxButtons.OK);

            Genclass.STR = "";
            conn.Close();
            Loadgrid();
            TotalAmount();
            panadd.Visible = true;
            Taxpan.Visible = false;
            addipan.Visible = false;
            Editpan.Visible = false;
            Genpan.Visible = true;
        }

        private void Updtord()
        {
            qur.CommandText = "delete from salesorout where invno='" + txtgrn.Text + "'";
            qur.ExecuteNonQuery();
            qur.CommandText = "insert into salesorout select  aa.Docno,aa.Docdate,aa.Dcno,aa.dcdate,aa.ItemCode,aa.ItemName,aa.reqqty,aa.pqty,aa.reqdate,aa.qty,aa.partyuid,0,aa.refid,aa.reqqty-bb.pendingqty,aa.invno,aa.invdate,aa.itemuid  from (select  a.dcdate,a.docno,a.uid,a.Docdate,a.transp as Dcno,e.ItemCode,e.ItemName,c.reqqty,d.pqty,c.reqdate,c.qty,a.partyuid,d.uid as refid,isnull(i.docno,'') as invno,i.docdate as invdate,b.itemuid from stransactionsp   a inner join  stransactionsplist b on a.uid=b.TransactionsPUId and a.doctypeid=b.doctypeid and b.DocTypeID=80 left join Ordreqqty c  on a.uid=c.Tpuid and b.itemuid=c.itid  and b.PQty=c.reqqty   inner join ItemM e on b.ItemUId=e.uid left join stransactionsplist  d on b.Uid=d.Refuid and b.ItemUId=d.itemuid and d.DocTypeID=40   left join  stransactionsp   i on d.transactionspuid=i.uid where a.companyid=" + Genclass.data1 + " and i.uid=" + txtgrnid.Text + " ) aa left join (select   isnull(sum(d.pqty),0) as pendingqty,b.itemuid,a.uid,d.uid as refid from stransactionsp   a inner join  stransactionsplist b on a.uid=b.TransactionsPUId and a.doctypeid=b.doctypeid and b.DocTypeID=80  left join Ordreqqty c  on a.uid=c.Tpuid and b.itemuid=c.itid  and b.PQty=c.reqqty   inner join ItemM e on b.ItemUId=e.uid left join stransactionsplist  d on b.Uid=d.Refuid  and b.ItemUId=d.itemuid and d.DocTypeID=40   left join  stransactionsp   i on d.transactionspuid=i.uid where a.companyid=" + Genclass.data1 + " group by b.itemuid,a.uid,d.uid)  bb on aa.uid=bb.uid  and aa.ItemUId=bb.ItemUId   and aa.refid=bb.refid";
            qur.ExecuteNonQuery();

        }
        private void HFIT_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void txtrem_TextChanged(object sender, EventArgs e)
        {

        }

        private void panadd_Paint(object sender, PaintEventArgs e)
        {

        }

        private void txtcoucharge_TextChanged(object sender, EventArgs e)
        {
            splittax();
        }

        private void txttrans_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {

                Genclass.fieldone = "";
                Genclass.fieldtwo = "";
                Genclass.fieldthree = "";
                Genclass.fieldFour = "";
                Genclass.fieldFive = "";

                Genclass.type = 8;
                loadput();


            }
        }

        private void txtdcno_TextChanged_1(object sender, EventArgs e)
        {

        }

        private void txtname_MouseClick(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                Genclass.fieldone = "";
                Genclass.fieldtwo = "";
                Genclass.fieldthree = "";
                Genclass.fieldFour = "";
                Genclass.fieldFive = "";
                if (Genclass.Dtype == 40)
                {
                    Genclass.Gbtxtid = 130;
                    Genclass.type = 1;
                    loadput();
                }
                else
                {
                    Genclass.type = 6;
                    loadput();
                }

            }
            else if (e.Button == MouseButtons.Left)
            {
                Genclass.fieldone = "";
                Genclass.fieldtwo = "";
                Genclass.fieldthree = "";
                Genclass.fieldFour = "";
                Genclass.fieldFive = "";
                if (Genclass.Dtype == 40)
                {
                    Genclass.Gbtxtid = 130;
                    Genclass.type = 1;
                    loadput();
                }
                else
                {
                    Genclass.type = 6;
                    loadput();
                }



            }
        }

        private void txtname_MouseDown(object sender, MouseEventArgs e)
        {
            //    if (e.Button == MouseButtons.Right)
            //    {
            //        Genclass.fieldone = "";
            //        Genclass.fieldtwo = "";
            //        Genclass.fieldthree = "";
            //        Genclass.fieldFour = "";
            //        Genclass.fieldFive = "";
            //        if (Genclass.Dtype == 40)
            //        {
            //            Genclass.Gbtxtid = 130;
            //            Genclass.type = 1;
            //            loadput();
            //        }
            //        else
            //        {
            //            Genclass.type = 6;
            //            loadput();
            //        }

            //    }
            //    else if  (e.Button == MouseButtons.Left)
            //    {
            //        Genclass.fieldone = "";
            //        Genclass.fieldtwo = "";
            //        Genclass.fieldthree = "";
            //        Genclass.fieldFour = "";
            //        Genclass.fieldFive = "";
            //        if (Genclass.Dtype == 40)
            //        {
            //            Genclass.Gbtxtid = 130;
            //            Genclass.type = 1;
            //            loadput();
            //        }
            //        else
            //        {
            //            Genclass.type = 6;
            //            loadput();
            //        }



            //}
        }

        private void txtname_MouseEnter(object sender, EventArgs e)
        {

        }



        private void txtname_KeyPress(object sender, KeyPressEventArgs e)
        {
            //if (e.KeyChar == 13)
            //{
            //    Genclass.fieldone = "";
            //    Genclass.fieldtwo = "";
            //    Genclass.fieldthree = "";
            //    Genclass.fieldFour = "";
            //    Genclass.fieldFive = "";
            //    if (Genclass.Dtype == 40)
            //    {
            //        Genclass.Gbtxtid = 130;
            //        Genclass.type = 1;
            //        loadput();
            //    }
            //    else
            //    {
            //        Genclass.type = 6;
            //        loadput();
            //    }

            //}
        }

        private void txtname_KeyUp(object sender, KeyEventArgs e)
        {
            //if (e.KeyCode == Keys.Enter)
            //{
            //    Genclass.fieldone = "";
            //    Genclass.fieldtwo = "";
            //    Genclass.fieldthree = "";
            //    Genclass.fieldFour = "";
            //    Genclass.fieldFive = "";
            //    if (Genclass.Dtype == 40)
            //    {
            //        Genclass.Gbtxtid = 130;
            //        Genclass.type = 1;
            //        loadput();
            //    }
            //    else
            //    {
            //        Genclass.type = 6;
            //        loadput();
            //    }

            //}
        }

        private void txtitemname_Click(object sender, EventArgs e)
        {
            Genclass.fieldone = "";
            Genclass.fieldtwo = "";
            Genclass.fieldthree = "";
            Genclass.fieldFour = "";
            Genclass.fieldFive = "";

            Genclass.type = 5;
            if (Genclass.data1 == 1)
            {
                loadput1();
            }
            else
            {
                loadput1();
            }


        }

        private void txtitemname_KeyPress(object sender, KeyPressEventArgs e)
        {
            //if (e.KeyChar == 13)
            //{
            //    Genclass.fieldone = "";
            //    Genclass.fieldtwo = "";
            //    Genclass.fieldthree = "";
            //    Genclass.fieldFour = "";
            //    Genclass.fieldFive = "";

            //    Genclass.type = 5;
            //    loadput1();
            //}
        }

        private void txtitemname_KeyUp(object sender, KeyEventArgs e)
        {
            //if (e.KeyCode == Keys.Enter)
            //{
            //    Genclass.fieldone = "";
            //    Genclass.fieldtwo = "";
            //    Genclass.fieldthree = "";
            //    Genclass.fieldFour = "";
            //    Genclass.fieldFive = "";

            //    Genclass.type = 5;
            //    loadput1();
            //}
        }

        private void txtitemname_MouseClick(object sender, MouseEventArgs e)
        {
            //if (e.Button == MouseButtons.Right)
            //{
            //    Genclass.fieldone = "";
            //    Genclass.fieldtwo = "";
            //    Genclass.fieldthree = "";
            //    Genclass.fieldFour = "";
            //    Genclass.fieldFive = "";

            //    Genclass.type = 5;
            //    loadput1();
            //}
            //else if (e.Button == MouseButtons.Left)
            //    {
            //        Genclass.fieldone = "";
            //        Genclass.fieldtwo = "";
            //        Genclass.fieldthree = "";
            //        Genclass.fieldFour = "";
            //        Genclass.fieldFive = "";

            //        Genclass.type = 5;
            //        loadput1();
            //    }
        }

        private void txtitemname_MouseDown(object sender, MouseEventArgs e)
        {
            //if (e.Button == MouseButtons.Right)
            //{
            //    Genclass.fieldone = "";
            //    Genclass.fieldtwo = "";
            //    Genclass.fieldthree = "";
            //    Genclass.fieldFour = "";
            //    Genclass.fieldFive = "";

            //    Genclass.type = 5;
            //    loadput1();
            //}
            //else if (e.Button == MouseButtons.Left)
            //{
            //    Genclass.fieldone = "";
            //    Genclass.fieldtwo = "";
            //    Genclass.fieldthree = "";
            //    Genclass.fieldFour = "";
            //    Genclass.fieldFive = "";

            //    Genclass.type = 5;
            //    loadput1();
            //}
        }

        private void txtitemname_MouseEnter(object sender, EventArgs e)
        {

        }

        private void txtitemname_MouseMove(object sender, MouseEventArgs e)
        {
            //if (e.Button == MouseButtons.Right)
            //{
            //    Genclass.fieldone = "";
            //    Genclass.fieldtwo = "";
            //    Genclass.fieldthree = "";
            //    Genclass.fieldFour = "";
            //    Genclass.fieldFive = "";

            //    Genclass.type = 5;
            //    loadput1();
            //}
            //else if (e.Button == MouseButtons.Left)
            //{
            //    Genclass.fieldone = "";
            //    Genclass.fieldtwo = "";
            //    Genclass.fieldthree = "";
            //    Genclass.fieldFour = "";
            //    Genclass.fieldFive = "";

            //    Genclass.type = 5;
            //    loadput1();
            //} 

        }

        private void txtitemname_MouseUp(object sender, MouseEventArgs e)
        {
            // if (e.Button == MouseButtons.Right)
            //{

            //    Genclass.fieldone = "";
            //    Genclass.fieldtwo = "";
            //    Genclass.fieldthree = "";
            //    Genclass.fieldFour = "";
            //    Genclass.fieldFive = "";

            //    Genclass.type = 5;
            //    loadput1();
            //}
            // else if (e.Button == MouseButtons.Left)
            // {

            //     Genclass.fieldone = "";
            //     Genclass.fieldtwo = "";
            //     Genclass.fieldthree = "";
            //     Genclass.fieldFour = "";
            //     Genclass.fieldFive = "";

            //     Genclass.type = 5;
            //     loadput1();
            // }
        }

        private void txttitemid_TextChanged(object sender, EventArgs e)
        {
            if (txttitemid.Text != "")
            {
                //if (txtsaleorderno.Text != "")
                //{

                //}
                //Genclass.strsql = "select Docno,convert(varchar,Docdate,105) as Docdate,a.transp,c.hsnid,c.itemname,uom,c.uid,a.uid as Tuid,b.uid as Listuid,b.pqty-isnull(sum(z.pqty),0) as qty,b.PRate as Price,b.BasicValue,i.f1 as gstper,convert(decimal(18,2),(b.BasicValue /100 * i.f1),105) as gstval,b.width,b.length,b.nos from orderp a inner join orderplist b on a.uid=b.TransactionsPUId left join stransactionsplist z on b.Uid=z.Refuid inner join itemm c on b.ItemUId=c.uid  left join ItemGroup j on c.itemgroup_Uid=j.UId left join Hsndet f on j.hsnid=f.uid left join generalm i on f.sgid=i.uid where a.uid=" + txtdcid.Text + " group by Docno,Docdate,c.hsnid,c.itemname,c.uid,i.f1,b.pqty,b.PRate,b.BasicValue,i.f1,b.width,b.length,b.nos,uom,a.uid,a.transp,b.uid having b.pqty-isnull(sum(z.pqty),0)>0";

                FrmlookupItem contc1 = new FrmlookupItem();
                TabControl tab = (TabControl)contc1.Controls["TabC"];



                if (Genclass.ty == 1)
                {
                    TabPage tab1 = (TabPage)tab.Controls["TabPage1"];
                    DataGridView grid = (DataGridView)tab1.Controls["hfgp"];


                    //Genclass.strsql = "select a.UId,a.DocNo,isnull(b.PQty,0)-isnull(c.PQty,0)-isnull(d.PQty,0) as balqty,b.prate,f.ItemName,f.ItemCode from sTransactionsP a inner join sTransactionsPList b on a.UId=b.TransactionsPUId   and b.DocTypeID = 80   left join Stransactionsplist  d on b.uid=  d.Refuid  and d.doctypeid=40 left join  TransactionsPList c on b.uid=c.Refuid     and c.DocTypeID=110  left join Stransactionsplist e on c.Uid=e.refuid and e.doctypeid=40  left join itemm f on b.ItemUId=f.Uid and b.itemuid=" + txttitemid.Text + "      group  by   a.uid,a.docno,b.pqty,d.pqty,c.pqty,f.ItemName,f.ItemCode,b.prate having       isnull(b.PQty,0)-isnull(c.PQty,0)-isnull(d.PQty,0) >0";
                    Genclass.strsql = "select a.UId,c.uid as listid,a.DocNo,isnull(b.PQty,0)-isnull(c.PQty,0)-isnull(e.PQty,0)  as  balqty,c.prate,f.ItemName,f.ItemCode,f.uid as itemid from sTransactionsP a inner join sTransactionsPList b on a.UId=b.TransactionsPUId     and b.DocTypeID = 80   left join Stransactionsplist  d on b.uid=  d.Refuid  and d.doctypeid=40 left join    TransactionsPList c    on b.uid=c.Refuid  and c.DocTypeID=110  left join Stransactionsplist e on c.Uid=e.refuid    and e.doctypeid=40  left join     itemm f on c.ItemUId=f.Uid  where f.itemname is not null and b.itemuid=" + txttitemid.Text + "   and f.companyid=" + Genclass.data1 + "   group  by   a.uid,a.docno,b.pqty,e.pqty,c.pqty,f.ItemName,c.uid,c.prate ,    f.ItemCode,f.uid having isnull(b.PQty,0)-isnull(c.PQty,0)-isnull(e.PQty,0) >0";


                    Genclass.cmd = new SqlCommand(Genclass.strsql, conn);
                    SqlDataAdapter aptr1 = new SqlDataAdapter(Genclass.cmd);
                    DataTable tap1 = new DataTable();
                    aptr1.Fill(tap1);
                    if (tap1.Rows.Count > 0)
                    {
                        txtitemname.Text = tap1.Rows[0]["ItemName"].ToString();
                        txtqty.Text = tap1.Rows[0]["balqty"].ToString();
                        txtprice.Text = tap1.Rows[0]["prate"].ToString();
                        txtlisid.Text = tap1.Rows[0]["listid"].ToString();

                    }
                }



                else if (Genclass.ty1 == 2)
                {
                    if (txtlisid.Text != "")
                    {
                        TabPage tab1 = (TabPage)tab.Controls["TabPage2"];
                        DataGridView grid = (DataGridView)tab1.Controls["HFGP4"];

                        ////Genclass.strsql = "select a.UId,a.DocNo,isnull(b.PQty,0)-isnull(c.PQty,0)-isnull(d.PQty,0) as balqty,b.prate,f.ItemName,f.ItemCode,b.uid as listid from sTransactionsP a inner join sTransactionsPList b on a.UId=b.TransactionsPUId   and b.DocTypeID = 80   left join Stransactionsplist  d on b.uid=  d.Refuid  and d.doctypeid=40 left join  TransactionsPList c on b.uid=c.Refuid     and c.DocTypeID=110  left join Stransactionsplist e on c.Uid=e.refuid and e.doctypeid=40  left join itemm f on b.ItemUId=f.Uid and b.itemuid=" + txttitemid.Text + "      group  by   a.uid,a.docno,b.pqty,d.pqty,c.pqty,f.ItemName,f.ItemCode,b.prate,b.uid having       isnull(b.PQty,0)-isnull(c.PQty,0)-isnull(d.PQty,0) >0";
                        if (Genclass.STR == "")
                        {
                            Genclass.strsql = "select a.UId,a.DocNo,isnull(b.PQty,0)-isnull(d.PQty,0) as balqty,b.prate,f.ItemName,f.ItemCode,b.uid as listid,a.docdate  from sTransactionsP a inner join sTransactionsPList b on a.UId=b.TransactionsPUId   and b.DocTypeID = 80   left join  Stransactionsplist  d on b.uid=  d.Refuid  and d.doctypeid=40   left join itemm f on     b.ItemUId=f.Uid  and b.DocTypeID=80 where  a.Partyuid=" + txtpuid.Text + " and a.companyid= " + Genclass.data1 + " and b.itemuid=" + txttitemid.Text + " and b.uid =" + txtlisid.Text + "   group  by   a.uid,a.docno,b.pqty,d.pqty,  f.ItemName,f.ItemCode,b.prate,    b.uid,a.docdate having       isnull(b.PQty,0)-isnull(d.PQty,0) >0 ";

                        }
                        else
                        {
                            Genclass.strsql = "select a.UId,a.DocNo,isnull(b.PQty,0)-isnull(d.PQty,0) as balqty,b.prate,f.ItemName,f.ItemCode,b.uid as listid,a.docdate  from sTransactionsP a inner join sTransactionsPList b on a.UId=b.TransactionsPUId   and b.DocTypeID = 80 and b.uid not in (" + Genclass.STR + ")  left join  Stransactionsplist  d on b.uid=  d.Refuid  and d.doctypeid=40   left join itemm f on     b.ItemUId=f.Uid  and b.DocTypeID=80 where  a.Partyuid=" + txtpuid.Text + " and a.companyid= " + Genclass.data1 + "  and b.itemuid=" + txttitemid.Text + "  and b.uid =" + txtlisid.Text + "  group  by   a.uid,a.docno,b.pqty,d.pqty,  f.ItemName,f.ItemCode,b.prate,    b.uid,a.docdate having       isnull(b.PQty,0)-isnull(d.PQty,0) >0 ";
                        }
                        Genclass.cmd = new SqlCommand(Genclass.strsql, conn);
                        SqlDataAdapter aptr1 = new SqlDataAdapter(Genclass.cmd);
                        DataTable tap1 = new DataTable();
                        aptr1.Fill(tap1);
                        if (tap1.Rows.Count > 0)
                        {
                            txtitemname.Text = tap1.Rows[0]["ItemName"].ToString();
                            txtqty.Text = tap1.Rows[0]["balqty"].ToString();
                            txtprice.Text = tap1.Rows[0]["prate"].ToString();
                            //txtlisid.Text = tap1.Rows[0]["listid"].ToString();
                            txttrans.Text = txtsaleorderno.Text;
                            txtorderno.Text = tap1.Rows[0]["docdate"].ToString();
                        }
                    }
                }


                else if (Genclass.ty2 == 3)
                {
                    TabPage tab1 = (TabPage)tab.Controls["TabPage3"];
                    DataGridView grid = (DataGridView)tab1.Controls["HFGP2"];

                    Genclass.strsql = "select c.uid,c.itemname,c.itemcode,isnull(a.price,0) as price from pur_price_list  a inner join  PartyM b on b.Uid=a.suppuid inner join ItemM c on a.Itemuid=c.Uid   where a.companyid=" + Genclass.data1 + "  and b.uid=" + txtpuid.Text + " and c.uid=" + txttitemid.Text + " and a.eff_to is null";


                    Genclass.cmd = new SqlCommand(Genclass.strsql, conn);
                    SqlDataAdapter aptr1 = new SqlDataAdapter(Genclass.cmd);
                    DataTable tap1 = new DataTable();
                    aptr1.Fill(tap1);
                    if (tap1.Rows.Count > 0)
                    {
                        txtitemname.Text = tap1.Rows[0]["ItemName"].ToString();
                        //txtqty.Text = tap1.Rows[0]["balqty"].ToString();
                        txtprice.Text = tap1.Rows[0]["price"].ToString();
                        //txtbuid.Text = tap1.Rows[0]["listid"].ToString();
                    }
                }

                else if (Genclass.ty3 == 4)
                {
                    TabPage tab1 = (TabPage)tab.Controls["TabPage4"];
                    DataGridView grid = (DataGridView)tab1.Controls["HFGP3"];
                    Genclass.strsql = "select distinct uid,itemname,itemcode FROM itemm  where  active=1 and companyid=" + Genclass.data1 + "";


                    Genclass.cmd = new SqlCommand(Genclass.strsql, conn);
                    SqlDataAdapter aptr1 = new SqlDataAdapter(Genclass.cmd);
                    DataTable tap1 = new DataTable();
                    aptr1.Fill(tap1);
                    if (tap1.Rows.Count > 0)
                    {
                        //txtqty.Text = tap1.Rows[0]["balqty"].ToString();
                        //txtprice.Text = tap1.Rows[0]["price"].ToString();
                        //txtbuid.Text = tap1.Rows[0]["listid"].ToString();
                    }
                }


            }


        }

        private void button13_Click(object sender, EventArgs e)
        {
            //Genclass.cat = 2;
            conn.Close();
            conn.Open();
            qur.CommandText = "delete from  NotoWords";
            qur.ExecuteNonQuery();

            Int64 NumVal = Convert.ToInt64(HFGP.CurrentRow.Cells[9].Value);
            string Nw = Rupees(NumVal);

            qur.CommandText = "Insert into NotoWords values('" + Nw + "',1)";
            qur.ExecuteNonQuery();

            Genclass.Prtid = Convert.ToInt16(HFGP.CurrentRow.Cells[0].Value.ToString());


            Genclass.slno = 1;
            Crviewer crv = new Crviewer();
            crv.Show();



            conn.Close();
        }

        private void button14_Click(object sender, EventArgs e)
        {

            //termspan.Visible = true;
            buttnnxt_Click(sender, e);

        }

        private void button7_Click_1(object sender, EventArgs e)
        {

        }

        private void button15_Click(object sender, EventArgs e)
        {


        }

        private void button15_Click_1(object sender, EventArgs e)
        {
            termspan.Visible = false;
            button15.Visible = false;
        }

        private void buttnfinbk_Click_1(object sender, EventArgs e)
        {
            addipan.Visible = false;
            Taxpan.Visible = false;

            buttnfinbk.Visible = false;
            buttnnxt.Visible = false;

            panadd.Visible = true;
            Editpan.Visible = false;
            Genpan.Visible = true;
            Genclass.STR = "";
            TxtNetAmt.Text = Txttot.Text;
        }

        private void button14_Click_1(object sender, EventArgs e)
        {
            buttnnxt_Click(sender, e);
        }

        private void btnsave_Click_2(object sender, EventArgs e)
        {
            btnsave_Click_1(sender, e);
        }

        private void Editpan_Paint(object sender, PaintEventArgs e)
        {
            //Frmlookup f = new Frmlookup();
            //f.Close();
        }

        private void txtlisid_TextChanged(object sender, EventArgs e)
        {
            txttitemid_TextChanged(sender, e);
        }

        private void button7_Click_2(object sender, EventArgs e)
        {
            //HFGT.AutoGenerateColumns = false;
            //var index = HFGT.Rows.Add();
            //HFGT.Rows[index].Cells[0].Value = txtterms.Text;
            //// double sump1 =  Convert.ToDouble(tap1.Rows[i]["width"].ToString());
            ////HFIT.Rows[index].Cells[1].Value = sump1.ToString("0.000");
            ////  double sumq1 =  Convert.ToDouble(tap1.Rows[i]["length"].ToString());
            ////  HFIT.Rows[index].Cells[2].Value = sumq1.ToString("0.00");


            //HFGT.Rows[index].Cells[1].Value = txttdisc.Text;

            //double sump = Convert.ToDouble(tap1.Rows[i]["Price"].ToString());
            //HFGT.Rows[index].Cells[2].Value = sump.ToString("0.000");
            //double sumq = Convert.ToDouble(tap1.Rows[i]["qty"].ToString());
            //HFGT.Rows[index].Cells[3].Value = sumq.ToString("0.00");
            //double sumb = Convert.ToDouble(tap1.Rows[i]["BasicValue"].ToString());
            //HFGT.Rows[index].Cells[4].Value = sumb.ToString("0.00");

            //HFGT.Rows[index].Cells[5].Value = txtnotes.Text;

            //HFGT.AllowUserToAddRows = true;
            //HFGT.AutoGenerateColumns = true;
            var index = HFGT.Rows.Add();
            //HFGT.Rows[index].Cells[0].Value = txtterms.Text;
            //HFGT.Rows[index].Cells[1].Value = txttdisc.Text;


            //for (int j = 1; j < HFGA.Rows.Count - 1; j++)
            //{
            HFGT.Rows[index].Cells[0].Value = txtterms.Text;
            //HFGT.Rows[index].Cells[1].Value = txttdisc.Text;
            if (HFGT.Rows.Count > 6)
            {
                MessageBox.Show("can not enter the terms more then 6 ");
                return;
            }

        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void checkBox1_Click(object sender, EventArgs e)
        {

            if (checkBox1.Checked == true)
            {
                for (int i = 0; i < HFG9.RowCount; i++)
                {
                    HFG9[0, i].Value = true;
                }
            }
            else
            {
                for (int i = 0; i < HFG9.RowCount; i++)
                {
                    HFG9[0, i].Value = false;
                }
            }
        }
        private void Loadhfit()
        {

            //HFGT.Refresh();
            //HFGT.DataSource = null;
            //HFGT.Rows.Clear();


            string quy = "select b.generalname as Terms from tmpitid a inner join generalm b on a.itemid=b.uid  ";
            Genclass.cmd = new SqlCommand(quy, conn);

            SqlDataAdapter aptr = new SqlDataAdapter(Genclass.cmd);
            DataTable tap = new DataTable();
            aptr.Fill(tap);
            for (int k = 0; k < tap.Rows.Count; k++)
            {
                var index = HFGT.Rows.Add();
                HFGT.Rows[index].Cells[0].Value = tap.Rows[k]["Terms"].ToString();
                //HFGT.Rows[index].Cells[1].Value = tap.Rows[k]["description"].ToString();
            }
            if (HFGT.Rows.Count > 6)
            {
                MessageBox.Show("can not enter the terms more then 6 ");
                return;
            }

        }

        private void button16_Click(object sender, EventArgs e)
        {

            conn.Close();
            conn.Open();
            int i = 0;
            qur.CommandText = "delete from tmpitid";
            qur.ExecuteNonQuery();

            foreach (DataGridViewRow row in HFG9.Rows)
            {
                DataGridViewCheckBoxCell chk = (DataGridViewCheckBoxCell)row.Cells[0];

                if (Convert.ToBoolean(row.Cells[0].Value) == true)
                {
                    qur.CommandText = "delete from tmpitid where itemid=" + HFG9.Rows[i].Cells[2].Value.ToString() + "";
                    qur.ExecuteNonQuery();
                    qur.CommandText = "insert into tmpitid values (" + HFG9.Rows[i].Cells[2].Value.ToString() + ")";
                    qur.ExecuteNonQuery();
                }

                i = i + 1;

            }

            Loadhfit();

        }

        private void HFGT_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void txtremde_TextChanged(object sender, EventArgs e)
        {

        }

        private void button17_Click(object sender, EventArgs e)
        {
            termspan.Visible = false;
            button17.Visible = false;
            button14.Visible = true;
            buttnfinbk.Visible = true;
            btnsave.Visible = true;
        }

        private void Editpan_Click(object sender, EventArgs e)
        {
            Frmlookup f = new Frmlookup();
            if (f != null)
            {
                f.ShowDialog(this);
            }

        }

        private void Editpan_ContextMenuStripChanged(object sender, EventArgs e)
        {
            Frmlookup f = new Frmlookup();
            f.Close();
        }

        public FormWindowState Normal { get; set; }

        private void txtscr7_TextChanged(object sender, EventArgs e)
        {
            Loadgrid();
        }

        private void txtscr8_TextChanged(object sender, EventArgs e)
        {
            Loadgrid();
        }

        private void label57_Click(object sender, EventArgs e)
        {

        }

        private void label58_Click(object sender, EventArgs e)
        {

        }

        private void textBox1_TextChanged_1(object sender, EventArgs e)
        {
            Loadgrid();
        }
    }
}

