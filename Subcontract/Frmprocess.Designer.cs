﻿namespace Subcontract
{
    partial class Frmprocess
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label14 = new System.Windows.Forms.Label();
            this.txtprocessqty = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.txtprocestime = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.txtrate = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.txtsqgno = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.txtproceesname = new System.Windows.Forms.TextBox();
            this.hfg3 = new System.Windows.Forms.DataGridView();
            this.btnsave = new System.Windows.Forms.Button();
            this.btnaddrcan = new System.Windows.Forms.Button();
            this.buttrqok = new System.Windows.Forms.Button();
            this.txtprocessid = new System.Windows.Forms.TextBox();
            this.txtsettime = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txthour = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.hfg3)).BeginInit();
            this.SuspendLayout();
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Bookman Old Style", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(685, 26);
            this.label14.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(87, 19);
            this.label14.TabIndex = 158;
            this.label14.Text = "Proc. Qty";
            // 
            // txtprocessqty
            // 
            this.txtprocessqty.Location = new System.Drawing.Point(681, 50);
            this.txtprocessqty.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtprocessqty.Name = "txtprocessqty";
            this.txtprocessqty.Size = new System.Drawing.Size(84, 20);
            this.txtprocessqty.TabIndex = 157;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Bookman Old Style", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(599, 26);
            this.label13.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(94, 19);
            this.label13.TabIndex = 156;
            this.label13.Text = "Proc.Time";
            // 
            // txtprocestime
            // 
            this.txtprocestime.Location = new System.Drawing.Point(595, 50);
            this.txtprocestime.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtprocestime.Name = "txtprocestime";
            this.txtprocestime.Size = new System.Drawing.Size(84, 20);
            this.txtprocestime.TabIndex = 155;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Bookman Old Style", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(449, 26);
            this.label12.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(47, 19);
            this.label12.TabIndex = 154;
            this.label12.Text = "Rate";
            // 
            // txtrate
            // 
            this.txtrate.Location = new System.Drawing.Point(448, 50);
            this.txtrate.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtrate.Name = "txtrate";
            this.txtrate.Size = new System.Drawing.Size(59, 20);
            this.txtrate.TabIndex = 153;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Bookman Old Style", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(277, 26);
            this.label11.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(61, 19);
            this.label11.TabIndex = 152;
            this.label11.Text = "SeqNo";
            // 
            // txtsqgno
            // 
            this.txtsqgno.Location = new System.Drawing.Point(281, 52);
            this.txtsqgno.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtsqgno.Name = "txtsqgno";
            this.txtsqgno.Size = new System.Drawing.Size(160, 20);
            this.txtsqgno.TabIndex = 151;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Bookman Old Style", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(19, 26);
            this.label10.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(117, 19);
            this.label10.TabIndex = 150;
            this.label10.Text = "Processname";
            // 
            // txtproceesname
            // 
            this.txtproceesname.Location = new System.Drawing.Point(23, 52);
            this.txtproceesname.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtproceesname.Name = "txtproceesname";
            this.txtproceesname.Size = new System.Drawing.Size(269, 20);
            this.txtproceesname.TabIndex = 149;
            this.txtproceesname.TextChanged += new System.EventHandler(this.txtproceesname_TextChanged);
            this.txtproceesname.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtproceesname_KeyDown);
            // 
            // hfg3
            // 
            this.hfg3.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.hfg3.Location = new System.Drawing.Point(23, 82);
            this.hfg3.Name = "hfg3";
            this.hfg3.Size = new System.Drawing.Size(895, 304);
            this.hfg3.TabIndex = 148;
            // 
            // btnsave
            // 
            this.btnsave.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnsave.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnsave.Image = global::Subcontract.Properties.Resources.save;
            this.btnsave.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnsave.Location = new System.Drawing.Point(347, 392);
            this.btnsave.Name = "btnsave";
            this.btnsave.Size = new System.Drawing.Size(90, 38);
            this.btnsave.TabIndex = 161;
            this.btnsave.Text = "Save";
            this.btnsave.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnsave.UseVisualStyleBackColor = false;
            this.btnsave.Click += new System.EventHandler(this.btnsave_Click);
            // 
            // btnaddrcan
            // 
            this.btnaddrcan.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnaddrcan.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnaddrcan.Image = global::Subcontract.Properties.Resources.can2;
            this.btnaddrcan.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnaddrcan.Location = new System.Drawing.Point(453, 392);
            this.btnaddrcan.Name = "btnaddrcan";
            this.btnaddrcan.Size = new System.Drawing.Size(93, 38);
            this.btnaddrcan.TabIndex = 162;
            this.btnaddrcan.Text = "Back";
            this.btnaddrcan.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnaddrcan.UseVisualStyleBackColor = false;
            this.btnaddrcan.Click += new System.EventHandler(this.btnaddrcan_Click);
            // 
            // buttrqok
            // 
            this.buttrqok.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.buttrqok.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttrqok.Image = global::Subcontract.Properties.Resources.ok1_25x25;
            this.buttrqok.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.buttrqok.Location = new System.Drawing.Point(858, 45);
            this.buttrqok.Name = "buttrqok";
            this.buttrqok.Size = new System.Drawing.Size(34, 32);
            this.buttrqok.TabIndex = 159;
            this.buttrqok.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.buttrqok.UseVisualStyleBackColor = false;
            this.buttrqok.Click += new System.EventHandler(this.buttrqok_Click);
            // 
            // txtprocessid
            // 
            this.txtprocessid.Location = new System.Drawing.Point(110, 52);
            this.txtprocessid.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtprocessid.Name = "txtprocessid";
            this.txtprocessid.Size = new System.Drawing.Size(26, 20);
            this.txtprocessid.TabIndex = 163;
            // 
            // txtsettime
            // 
            this.txtsettime.Location = new System.Drawing.Point(509, 50);
            this.txtsettime.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtsettime.Name = "txtsettime";
            this.txtsettime.Size = new System.Drawing.Size(84, 20);
            this.txtsettime.TabIndex = 155;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Bookman Old Style", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(513, 26);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(83, 19);
            this.label1.TabIndex = 156;
            this.label1.Text = "Set.Time";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Bookman Old Style", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(771, 26);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(77, 19);
            this.label2.TabIndex = 165;
            this.label2.Text = "Rate/Hr";
            // 
            // txthour
            // 
            this.txthour.Location = new System.Drawing.Point(767, 50);
            this.txthour.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txthour.Name = "txthour";
            this.txthour.Size = new System.Drawing.Size(84, 20);
            this.txthour.TabIndex = 164;
            // 
            // Frmprocess
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(937, 447);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txthour);
            this.Controls.Add(this.btnsave);
            this.Controls.Add(this.btnaddrcan);
            this.Controls.Add(this.buttrqok);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.txtprocessqty);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtsettime);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.txtprocestime);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.txtrate);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.txtsqgno);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.txtproceesname);
            this.Controls.Add(this.hfg3);
            this.Controls.Add(this.txtprocessid);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "Frmprocess";
            this.Text = "Frmprocess";
            this.Load += new System.EventHandler(this.Frmprocess_Load);
            ((System.ComponentModel.ISupportInitialize)(this.hfg3)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnsave;
        private System.Windows.Forms.Button btnaddrcan;
        private System.Windows.Forms.Button buttrqok;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox txtprocessqty;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox txtprocestime;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox txtrate;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox txtsqgno;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox txtproceesname;
        private System.Windows.Forms.DataGridView hfg3;
        private System.Windows.Forms.TextBox txtprocessid;
        private System.Windows.Forms.TextBox txtsettime;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txthour;
    }
}