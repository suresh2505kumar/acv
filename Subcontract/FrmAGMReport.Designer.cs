﻿namespace ACV
{
    partial class FrmAGMReport
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.grFront = new System.Windows.Forms.GroupBox();
            this.SfdDataGridRpt = new Syncfusion.WinForms.DataGrid.SfDataGrid();
            this.grFront.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.SfdDataGridRpt)).BeginInit();
            this.SuspendLayout();
            // 
            // grFront
            // 
            this.grFront.Controls.Add(this.SfdDataGridRpt);
            this.grFront.Location = new System.Drawing.Point(9, 3);
            this.grFront.Name = "grFront";
            this.grFront.Size = new System.Drawing.Size(675, 480);
            this.grFront.TabIndex = 0;
            this.grFront.TabStop = false;
            // 
            // SfdDataGridRpt
            // 
            this.SfdDataGridRpt.AccessibleName = "Table";
            this.SfdDataGridRpt.AllowFiltering = true;
            this.SfdDataGridRpt.AllowSorting = false;
            this.SfdDataGridRpt.Location = new System.Drawing.Point(6, 19);
            this.SfdDataGridRpt.Name = "SfdDataGridRpt";
            this.SfdDataGridRpt.Size = new System.Drawing.Size(663, 455);
            this.SfdDataGridRpt.TabIndex = 0;
            this.SfdDataGridRpt.Text = "sfDataGrid1";
            // 
            // FrmAGMReport
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.ClientSize = new System.Drawing.Size(694, 494);
            this.Controls.Add(this.grFront);
            this.Name = "FrmAGMReport";
            this.Text = "Report";
            this.Load += new System.EventHandler(this.FrmAGMReport_Load);
            this.grFront.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.SfdDataGridRpt)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox grFront;
        private Syncfusion.WinForms.DataGrid.SfDataGrid SfdDataGridRpt;
    }
}