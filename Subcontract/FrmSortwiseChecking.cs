﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ACV
{
    public partial class FrmSortwiseChecking : Form
    {
        public FrmSortwiseChecking()
        {
            InitializeComponent();
            sfDataGrid1.Style.HeaderStyle.BackColor = Color.DeepSkyBlue;
        }
        SqlConnection conn = new SqlConnection("Data Source=" + Genclass.data2 + "; Initial Catalog=" + Genclass.data5 + ";User id=" + Genclass.data3 + ";Password=" + Genclass.data4 + "");
        BindingSource BsSortNo = new BindingSource();
        SQLDBHelper db = new SQLDBHelper();
        int SelectedId = 0;
        int Fillid = 0;
        private void GrFront_Enter(object sender, EventArgs e)
        {

        }

        private void TxtSortNo_Click(object sender, EventArgs e)
        {
            try
            {
                string Query = "Select Uid, SortNo from SortDet Order by SortNo";
                DataTable dt = db.GetDataWithoutParam(CommandType.Text, Query, conn);
                BsSortNo.DataSource = dt;
                Point loc = FindLocation(txtSortNo);
                grSearch.Location = new Point(loc.X, loc.Y + 20);
                LoadDataGrid(dt, 1);
                grSearch.Visible = true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private Point FindLocation(Control ctrl)
        {
            if (ctrl.Parent is Form)
                return ctrl.Location;
            else
            {
                Point p = FindLocation(ctrl.Parent);
                p.X += ctrl.Location.X;
                p.Y += ctrl.Location.Y;
                return p;
            }
        }

        private void LoadDataGrid(DataTable dt, int FillId)
        {
            try
            {
                SelectedId = 1;
                DataGridCommon.DataSource = null;
                if (FillId == 1)
                {
                    Fillid = 1;
                    DataGridCommon.Columns.Clear();
                    DataGridCommon.AutoGenerateColumns = false;
                    DataGridCommon.ColumnCount = 2;
                    DataGridCommon.Columns[0].Name = "Uid";
                    DataGridCommon.Columns[0].HeaderText = "Uid";
                    DataGridCommon.Columns[0].DataPropertyName = "Uid";
                    DataGridCommon.Columns[0].Visible =false;

                    DataGridCommon.Columns[1].Name = "SortNo";
                    DataGridCommon.Columns[1].HeaderText = "SortNo";
                    DataGridCommon.Columns[1].DataPropertyName = "SortNo";
                    DataGridCommon.Columns[1].Width = 340;
                    DataGridCommon.DataSource = BsSortNo;
                }
                SelectedId = 0;
                grSearch.Visible = true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void BtnSelect_Click(object sender, EventArgs e)
        {
            try
            {
                SelectedId = 1;
                if (Fillid == 1)
                {
                    int Index = DataGridCommon.SelectedCells[0].RowIndex;
                    txtSortNo.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                    txtSortNo.Tag = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                }
                SelectedId = 0;
                grSearch.Visible = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void DataGridCommon_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            BtnSelect_Click(sender, e);
        }

        private void BtnHide_Click(object sender, EventArgs e)
        {
            grSearch.Visible = false;
        }

        private void BtnExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void BtnSts_Click(object sender, EventArgs e)
        {
            try
            {
                if(txtSortNo.Text != string.Empty)
                {
                    SqlParameter[] sqlParameters = {
                        new SqlParameter("@SORTNO",txtSortNo.Text),
                        new SqlParameter("@FDT",Convert.ToDateTime(DtpFromDate.Text).ToString("yyyy-MM-dd")),
                        new SqlParameter("@TDT",Convert.ToDateTime(DtpToDate.Text).ToString("yyyy-MM-dd"))
                    };
                    DataTable dataTable = db.GetDataWithParam(CommandType.StoredProcedure, "PROC_RPT_SORTWISEDETAIL", sqlParameters, conn);
                    sfDataGrid1.DataSource = null;
                    sfDataGrid1.DataSource = dataTable;
                    sfDataGrid1.AutoSizeColumnsMode = Syncfusion.WinForms.DataGrid.Enums.AutoSizeColumnsMode.AllCells;
                }
                else
                {
                    MessageBox.Show("Select Sort No", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void txtSortNo_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (SelectedId == 0)
                {
                    BsSortNo.Filter = string.Format("SortNo Like '%{0}%'", txtSortNo.Text);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }
    }
}
