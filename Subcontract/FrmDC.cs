﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Drawing.Imaging;
using System.Configuration;
using System.Data.SqlClient;


namespace Subcontract
{
    public partial class FrmDC : Form
    {
        public FrmDC()
        {
            CenterToParent();
            this.BackColor = Color.LightSkyBlue;
            InitializeComponent();
        }
        string uid = "";
        int mode = 0;
        int type = 0;
        //SqlCommand cmd;
        SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["connstr"].ConnectionString);
        SqlCommand qur = new SqlCommand();
        private void FrmDC_Load(object sender, EventArgs e)
        {
            qur.Connection = conn;
            Genclass.Dtype = 30;
            Left = (MdiParent.ClientRectangle.Width - Width) / 3;
            Top = (MdiParent.ClientRectangle.Height - Height) / 3;
            Titlep();
            Loadgrid();
        }
        private void Titlep()
        {
            HFIT.ColumnCount = 6;
            HFIT.Columns[0].Name = "Itemname";
            HFIT.Columns[1].Name = "Qty";
            HFIT.Columns[2].Name = "Addnotes";
            HFIT.Columns[3].Name = "itemid";
            //HFIT.Columns[4].Name = "Itid";
            HFIT.Columns[0].Width = 450;
            HFIT.Columns[1].Width = 50;
            HFIT.Columns[2].Width = 50;
            HFIT.Columns[3].Width = 250;
            HFIT.Columns[4].Visible = false;
            //HFIT.Columns[5].Visible = false;
        }
        private void Loadgrid()
        {
            try
            {
                conn.Open();

                Genclass.StrSrch = "";


                Genclass.FSSQLSortStr = "Docno";
                Genclass.FSSQLSortStr1 = "Docdate";
                Genclass.FSSQLSortStr2 = "Dcno";
                Genclass.FSSQLSortStr3 = "Dcdate";
                Genclass.FSSQLSortStr4 = "Name";



                if (txtscr1.Text != "")
                {
                    if (Genclass.StrSrch == "")
                    {
                        Genclass.StrSrch = Genclass.FSSQLSortStr + " like '%" + txtscr1.Text + "%'";
                    }
                    else
                    {
                        Genclass.StrSrch = Genclass.StrSrch + " and " + Genclass.FSSQLSortStr + " like '%" + txtscr1.Text + "%'";
                    }

                }

                if (Txtscr2.Text != "")
                {
                    if (Genclass.StrSrch == "")
                    {
                        Genclass.StrSrch = Genclass.FSSQLSortStr1 + " like '%" + Txtscr2.Text + "%'";
                    }
                    else
                    {
                        Genclass.StrSrch = Genclass.StrSrch + " and " + Genclass.FSSQLSortStr1 + " like '%" + Txtscr2.Text + "%'";
                    }

                }

                if (Txtscr3.Text != "")
                {
                    if (Genclass.StrSrch == "")
                    {
                        Genclass.StrSrch = Genclass.FSSQLSortStr2 + " like '%" + Txtscr3.Text + "%'";
                    }
                    else
                    {
                        Genclass.StrSrch = Genclass.StrSrch + " and " + Genclass.FSSQLSortStr2 + " like '%" + Txtscr3.Text + "%'";
                    }

                }

                if (txtscr4.Text != "")
                {
                    if (Genclass.StrSrch == "")
                    {
                        Genclass.StrSrch = Genclass.FSSQLSortStr3 + " like '%" + txtscr4.Text + "%'";
                    }
                    else
                    {
                        Genclass.StrSrch = Genclass.StrSrch + " and " + Genclass.FSSQLSortStr3 + " like '%" + txtscr4.Text + "%'";
                    }

                }

                if (txtscr5.Text != "")
                {
                    if (Genclass.StrSrch == "")
                    {
                        Genclass.StrSrch = Genclass.FSSQLSortStr4 + " like '%" + txtscr5.Text + "%'";
                    }
                    else
                    {
                        Genclass.StrSrch = Genclass.StrSrch + " and " + Genclass.FSSQLSortStr4 + " like '%" + txtscr5.Text + "%'";
                    }

                }





                if (txtscr1.Text != "")
                {
                    Genclass.StrSrch = " " + Genclass.StrSrch;
                }
                else
                {
                    Genclass.StrSrch = "a.uid <> 0";
                }

                string quy = "select a.UId,DocNo,DocDate,DcNo,DcDate,Name as Party,a.refdocno from TransactionsP a inner join PartyM b on a.PartyUid=b.uid where a.doctypeid=30 and  " + Genclass.StrSrch + "";
                Genclass.cmd = new SqlCommand(quy, conn);



                SqlDataAdapter aptr = new SqlDataAdapter(Genclass.cmd);
                DataTable tap = new DataTable();
                aptr.Fill(tap);

                this.HFGP.DefaultCellStyle.Font = new Font("Arial", 10);
                //dgv.ColumnHeadersDefaultCellStyle.Font = new Font(dgv.ColumnHeadersDefaultCellStyle.Font, FontStyle.Bold);
                this.HFGP.ColumnHeadersDefaultCellStyle.Font = new Font("Arial", 10, FontStyle.Bold);
                HFGP.AutoGenerateColumns = false;
                HFGP.Refresh();
                HFGP.DataSource = null;
                HFGP.Rows.Clear();


                HFGP.ColumnCount = tap.Columns.Count;
                Genclass.i = 0;
                foreach (DataColumn column in tap.Columns)
                {
                    HFGP.Columns[Genclass.i].Name = column.ColumnName;
                    HFGP.Columns[Genclass.i].HeaderText = column.ColumnName;
                    HFGP.Columns[Genclass.i].DataPropertyName = column.ColumnName;
                    Genclass.i = Genclass.i + 1;
                }



                HFGP.Columns[0].Visible = false;

                HFGP.Columns[1].Width = 120;
                HFGP.Columns[2].Width = 120;
                HFGP.Columns[3].Width = 120;
                HFGP.Columns[4].Width = 120;
                HFGP.Columns[5].Width = 400;




                HFGP.DataSource = tap;

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return;
            }
            finally
            {
                conn.Close();
                Genclass.cmd.Dispose();
            }
        }

           
             private void txtgrnno_KeyDown(object sender, KeyEventArgs e)
             {
                 loadput();
             }

             private void btnexit_Click(object sender, EventArgs e)
             {
                 this.Dispose();
             }

             private void button1_Click(object sender, EventArgs e)
             {
                 mode = 1;
                 Genpan.Visible = false;

                 Module.Gendocno();
                 txtgrn.Text = Genclass.ST;
                 DateTime d = new DateTime();
                 d = DateTime.Now;
                 txtgrndt.Text = d.ToString("dd.MM.yyyy");
                 txtdcno.Focus();
             }

             private void txtname_TextChanged(object sender, EventArgs e)
             {

             }

             private void txtname_KeyDown(object sender, KeyEventArgs e)
             {
                 loadput();
             }
             private void loadput()
             {
                 conn.Open();

                
                 {
                     Module.Partylistviewcont("uid", "Name", Genclass.strsql, this, txtpuid, txtname,editpnl);
                     Genclass.strsql = "select uid,Name as Party from Partym where active=1";
                 }
              
                 Genclass.cmd = new SqlCommand(Genclass.strsql, conn);
                 SqlDataAdapter aptr = new SqlDataAdapter(Genclass.cmd);
                 DataTable tap = new DataTable();
                 aptr.Fill(tap);
                 Frmlookup contc = new Frmlookup();
                 DataGridView dt = (DataGridView)contc.Controls["HFGP"];
                 dt.Refresh();
                 dt.ColumnCount = tap.Columns.Count;
                 dt.Columns[0].Visible = false;
                 dt.Columns[1].Width = 400;


                 dt.DefaultCellStyle.Font = new Font("Arial", 10);

                 dt.ColumnHeadersDefaultCellStyle.Font = new Font("Arial", 10, FontStyle.Bold);
                 dt.AutoGenerateColumns = false;

                 Genclass.i = 0;
                 foreach (DataColumn column in tap.Columns)
                 {
                     dt.Columns[Genclass.i].Name = column.ColumnName;
                     dt.Columns[Genclass.i].HeaderText = column.ColumnName;
                     dt.Columns[Genclass.i].DataPropertyName = column.ColumnName;
                     Genclass.i = Genclass.i + 1;
                 }

                 dt.DataSource = tap;
                 contc.Show();
                 conn.Close();
             }

             private void txtitem_TextChanged(object sender, EventArgs e)
             {
                 
             }

             private void txtitem_KeyDown(object sender, KeyEventArgs e)
             {
                 loaditem();
             }

             private void loaditem()
             {
                 conn.Open();


                 {
                     Module.Partylistviewcont1("uid", "itemname","qty","dcwid","itemid" ,Genclass.strsql, this, txtgrnid, txtitem,txtqty,txtgrnno, txtititd,editpnl);
                     Genclass.strsql = "select b.uid,e.itemname,b.pqty-isnull(sum(c.pqty),0)-isnull(sum(d.pqty),0) * isnull(f.usedqty,0) as qty  ,c.uid  as dcwid,  e.uid  as itemid  from transactionsp a  inner join transactionsplist b on a.uid=b.transactionspuid and b.doctypeid=10  left join   transactionsplist c on b.uid=c.refuid  and b.doctypeid=20 left join transactionsplist d on c.uid=d.refuid  and d.doctypeid=30   left join itemsmbom f on b.itemuid=f.itemm_uid left join itemm e on f.useditem_uid=e.uid group by   b.uid,c.uid,b.pqty,e.itemname,f.usedqty,e.uid having b.pqty-isnull(sum(c.pqty),0)-isnull(sum(d.pqty),0)>0 ";
                 }

                 Genclass.cmd = new SqlCommand(Genclass.strsql, conn);
                 SqlDataAdapter aptr = new SqlDataAdapter(Genclass.cmd);
                 DataTable tap = new DataTable();
                 aptr.Fill(tap);
                 Frmlookup contc = new Frmlookup();
                 DataGridView dt = (DataGridView)contc.Controls["HFGP"];
                 dt.Refresh();
                 dt.ColumnCount = tap.Columns.Count;
                 dt.Columns[0].Visible = false;
                 dt.Columns[1].Visible = false;
                 dt.Columns[2].Width = 400;
                  dt.Columns[3].Width = 400;
                  dt.Columns[4].Visible = false;;


                 dt.DefaultCellStyle.Font = new Font("Arial", 10);

                 dt.ColumnHeadersDefaultCellStyle.Font = new Font("Arial", 10, FontStyle.Bold);
                 dt.AutoGenerateColumns = false;

                 foreach (DataColumn column in tap.Columns)
                 {
                     dt.Columns[Genclass.i].Name = column.ColumnName;
                     dt.Columns[Genclass.i].HeaderText = column.ColumnName;
                     dt.Columns[Genclass.i].DataPropertyName = column.ColumnName;
                     Genclass.i = Genclass.i + 1;
                 }

                 dt.DataSource = tap;
                 contc.Show();
                 conn.Close();
             }

             private void btnadd_Click(object sender, EventArgs e)
             {
                 if (txtitem.Text == "")
                 {
                     MessageBox.Show("Select the Item");
                     txtitem.Focus();
                     return;
                 }
                 if (txtqty.Text == "")
                 {
                     MessageBox.Show("Enter the Quantity");
                     txtqty.Focus();
                     return;
                 }
               


                 var index = HFIT.Rows.Add();
                 HFIT.Rows[index].Cells[0].Value = txtgrnid.Text;
                 HFIT.Rows[index].Cells[1].Value = txtitem.Text;
                 HFIT.Rows[index].Cells[2].Value = txtqty.Text;
                 HFIT.Rows[index].Cells[3].Value = txtaddnotes.Text;
                 HFIT.Rows[index].Cells[4].Value = txtititd.Text;
                 HFIT.Rows[index].Cells[6].Value = txtgrnno.Text;
                 HFIT.Rows[index].Cells[7].Value = index;

                 txtitem.Text = "";
                 txtqty.Text = "";
                 txtaddnotes.Text = "";
                 txtititd.Text = "";
                 loaditem();
             }

             private void btnsave_Click(object sender, EventArgs e)
             {
                  if (txtname.Text == "")
            {
                MessageBox.Show("Enter the Party");
                txtname.Focus();
            }

            conn.Open();
            if (mode == 1)
            {

                qur.CommandText = "insert into TransactionsP (doctypeid,DocNo,DocDate, DcNo, DcDate, PartyUid,Narration,active) values (" + Genclass.Dtype + ",'" + txtgrn.Text + "','" + txtgrndt.Text + "','" + txtdcno.Text + "','" + Dtpdt.Value + "'," + txtpuid.Text + ",'" + txtnar.Text + "',1)";
                qur.ExecuteNonQuery();

                string quy = "select uid from TransactionsP where doctypeid=30 and docno='" + txtgrn.Text + "'";
                Genclass.cmd = new SqlCommand(quy, conn);
                SqlDataAdapter aptr = new SqlDataAdapter(Genclass.cmd);
                DataTable tap = new DataTable();
                aptr.Fill(tap);

                txtgrnid.Text = tap.Rows[0]["uid"].ToString();

                for (int i = 0; i < HFIT.Rows.Count - 1; i++)
                {
                    qur.CommandText = "insert into TransactionsPlist (doctypeid,TransactionsPUId,ItemUId,PQty,AddNotes,active,refuid)  values ( " + Genclass.Dtype + "," + txtgrnid.Text + "," + HFIT.Rows[i].Cells[4].Value + "," + HFIT.Rows[i].Cells[1].Value + "," + HFIT.Rows[i].Cells[2].Value + ",'" + HFIT.Rows[i].Cells[3].Value + "',1,'" + HFIT.Rows[i].Cells[0].Value + "')";
                    qur.ExecuteNonQuery();
                }

              
                MessageBox.Show("Record has been saved", "Save", MessageBoxButtons.OK);
            }


            if (mode == 2)
            {

                qur.CommandText = "Update TransactionsP set DcNo='" + txtdcno.Text + "',DcDate='" + Dtpdt.Text + "',PartyUid='" + txtpuid.Text + "',Narration='" + txtnar.Text + "' where uid=" + txtgrnid.Text + "";
                qur.ExecuteNonQuery();

                qur.CommandText = "delete from  TransactionsPlist where  doctypeid=30 and TransactionsPuid=" + txtgrnid.Text + "";
                qur.ExecuteNonQuery();

                         for (int i = 0; i < HFIT.Rows.Count - 1; i++)
                {
                    qur.CommandText = "insert into TransactionsPlist (doctypeid,TransactionsPUId,ItemUId,PQty,dcqty,AddNotes,active)  values ( " + Genclass.Dtype + "," + txtgrnid.Text + "," + HFIT.Rows[i].Cells[4].Value + "," + HFIT.Rows[i].Cells[1].Value + "," + HFIT.Rows[i].Cells[2].Value + ",'" + HFIT.Rows[i].Cells[3].Value + "',1)";
                    qur.ExecuteNonQuery();
                }

         
                MessageBox.Show("Record has been Updated", "Save", MessageBoxButtons.OK);
            }
         

                 qur.CommandText = "delete from mfg_stock_ledger where doctypeid="+ Genclass.Dtype + " and  docno='" + txtgrn.Text + "'";
                 qur.ExecuteNonQuery();

                //string quy2 = "select uid from TransactionsP a inner join transactionsp b on a.ui=b.transactionspuid  where docno='" + txtgrn.Text + "'  and doctypeid="+ Genclass.Dtype+"";
                //Genclass.cmd = new SqlCommand(quy2, conn);
                //SqlDataAdapter aptr2 = new SqlDataAdapter(Genclass.cmd);
                //DataTable tap2 = new DataTable();
                //aptr2.Fill(tap2);

                //txtgrnid.Text = tap2.Rows[0]["uid"].ToString();


                for (int i = 0; i < HFIT.Rows.Count - 1; i++)
                {
                    qur.CommandText = "insert into mfg_stock_ledger (Docno,docdate,doctypeid,trantype,itemid,qty,yearid,tranuid,tranname,active,partyuid)  values ( '" + txtgrn.Text + "','" + txtgrndt.Text + "'," + Genclass.Dtype + ",'Receipt'," + HFIT.Rows[i].Cells[4].Value + "," + HFIT.Rows[i].Cells[2].Value + ",4,'" + HFIT.Rows[i].Cells[1].Value + "','GRN',1,"+ txtpuid.Text +")";
                    qur.ExecuteNonQuery();
                }


            
            conn.Close();
                              
            Loadgrid();
            Genpan.Visible = true;
        }

             private void btnaddrcan_Click(object sender, EventArgs e)
             {
                 editpnl.Visible = false;
                 Genpan.Visible = true;
                 Loadgrid();
             }

             private void Genpan_Paint(object sender, PaintEventArgs e)
             {

             }
             }

        }

 

