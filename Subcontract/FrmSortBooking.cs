﻿using System;
using System.Configuration;
using System.Data;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.IO;
using System.Windows.Forms;

namespace ACV
{
    public partial class FrmSortBooking : Form
    {
        public FrmSortBooking()
        {
            InitializeComponent();
        }
        SqlConnection conn = new SqlConnection("Data Source=" + Genclass.data2 + "; Initial Catalog=" + Genclass.data5 + ";User id=" + Genclass.data3 + ";Password=" + Genclass.data4 + "");
        SQLDBHelper db = new SQLDBHelper();
        private void FrmSortBooking_Load(object sender, EventArgs e)
        {
            dtpdtae.Format = DateTimePickerFormat.Custom;
            dtpdtae.CustomFormat = "MMM-yyyy";
            GetData();
            grFront.Visible = true;
            grBack.Visible = false;
            button5.Visible = false;
        }

        protected void GetData()
        {
            try
            {
                int CompletedOn;
                if (chckCompleted.Checked == true)
                {
                    CompletedOn = 1;
                }
                else
                {
                    CompletedOn = 0;
                }
                SqlParameter[] para = { new SqlParameter("@Date", Convert.ToDateTime(dtpdtae.Text).Month), new SqlParameter("@year", Convert.ToDateTime(dtpdtae.Text).Year), new SqlParameter("@Completed", CompletedOn) };
                DataTable dt = db.GetDataWithParam(CommandType.StoredProcedure, "SP_GetSortBooking", para, conn);
                DataGridSortBooking.DataSource = null;
                DataGridSortBooking.AutoGenerateColumns = false;
                DataGridSortBooking.ColumnCount = 6;
                DataGridSortBooking.Columns[0].Name = "id";
                DataGridSortBooking.Columns[0].HeaderText = "id";
                DataGridSortBooking.Columns[0].DataPropertyName = "id";
                DataGridSortBooking.Columns[0].Visible = false;
                DataGridSortBooking.Columns[1].Name = "sortno";
                DataGridSortBooking.Columns[1].HeaderText = "SortNo";
                DataGridSortBooking.Columns[1].DataPropertyName = "sortno";
                DataGridSortBooking.Columns[1].Width = 150;
                DataGridSortBooking.Columns[2].Name = "loomno";
                DataGridSortBooking.Columns[2].HeaderText = "LoomNo";
                DataGridSortBooking.Columns[2].DataPropertyName = "LoomNo";
                DataGridSortBooking.Columns[2].Width = 150;
                DataGridSortBooking.Columns[3].Name = "beamno";
                DataGridSortBooking.Columns[3].HeaderText = "Beam No";
                DataGridSortBooking.Columns[3].DataPropertyName = "beamno";
                DataGridSortBooking.Columns[3].Width = 140;
                DataGridSortBooking.Columns[4].Name = "jobcardno";
                DataGridSortBooking.Columns[4].HeaderText = "Job CardNo";
                DataGridSortBooking.Columns[4].DataPropertyName = "jobcardno";
                DataGridSortBooking.Columns[4].Width = 150;
                DataGridSortBooking.Columns[5].Name = "completedon";
                DataGridSortBooking.Columns[5].HeaderText = "Completed On";
                DataGridSortBooking.Columns[5].DataPropertyName = "completedon";
                DataGridSortBooking.Columns[5].Width = 150;
                DataGridSortBooking.DataSource = dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void chckCompleted_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                GetData();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void butedit_Click(object sender, EventArgs e)
        {
            try
            {
                txtSortNo.Text = string.Empty;
                txtLoomNo.Text = string.Empty;
                txtBeamNo.Text = string.Empty;
                txtJobCard.Text = string.Empty;
                txtCompletedOn.Text = string.Empty;
                int Index = DataGridSortBooking.SelectedCells[0].RowIndex;
                txtSortNo.Text = DataGridSortBooking.Rows[Index].Cells[1].Value.ToString();
                txtLoomNo.Text = DataGridSortBooking.Rows[Index].Cells[2].Value.ToString();
                txtBeamNo.Text = DataGridSortBooking.Rows[Index].Cells[3].Value.ToString();
                txtJobCard.Text = DataGridSortBooking.Rows[Index].Cells[4].Value.ToString();
                txtCompletedOn.Text = DataGridSortBooking.Rows[Index].Cells[5].Value.ToString();
                grFront.Visible = false;
                grBack.Visible = true;
                button5.Visible = true;
                butedit.Visible = false;
                buttnext1.Visible = false;

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void buttnext1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            grBack.Visible = false;
            grFront.Visible = true;
            button5.Visible = false;
            butedit.Visible = true;
            buttnext1.Visible = true;
        }

        private void btnBrowse_Click(object sender, EventArgs e)
        {
            try
            {
                OpenFileDialog file = new OpenFileDialog();
                file.InitialDirectory = "C:";
                file.Filter = "Excel Files| *.xls; *.xlsx;";
                file.Title = "Select File";
                string excelConnectionString = string.Empty;
                if (file.ShowDialog() == DialogResult.OK)
                {
                    lblPath.Text = file.FileName;
                    string filename = file.FileName;
                    string extension = Path.GetExtension(filename);
                    switch (extension)
                    {
                        case ".xls": //Excel 97-03
                            excelConnectionString = (@"Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + filename + ";Extended Properties=Excel 12.0;");
                            break;
                        case ".xlsx": //Excel 07 or higher
                            excelConnectionString = ConfigurationManager.ConnectionStrings["Excel07+ConString"].ConnectionString;
                            break;
                    }
                    OleDbConnection connection = new OleDbConnection(excelConnectionString);
                    OleDbCommand command = new OleDbCommand("Select * FROM [Sheet1$]", connection);
                    OleDbDataAdapter da = new OleDbDataAdapter(command);
                    DataTable dt = new DataTable();
                    da.Fill(dt);
                    DataTable dtSerevr = new DataTable();
                    dtSerevr.Columns.Add("PickUid", typeof(int));
                    dtSerevr.Columns.Add("Dte", typeof(DateTime));
                    dtSerevr.Columns.Add("Shiftname", typeof(string));
                    dtSerevr.Columns.Add("LoomNumber", typeof(string));
                    dtSerevr.Columns.Add("Kpic", typeof(decimal));
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        DataRow row = dtSerevr.NewRow();
                        row["PickUid"] = i + 1;
                        row["Dte"] = Convert.ToDateTime(dtpDate.Text);
                        row["Shiftname"] = cmbShift.SelectedValue;
                        row["LoomNumber"] = dt.Rows[i]["Machine"].ToString();
                        row["Kpic"] = dt.Rows[i]["KPicks"];
                        dtSerevr.Rows.Add(row);
                    }
                    using (SqlBulkCopy bulkCopy = new SqlBulkCopy(conn))
                    {
                        conn.Open();
                        bulkCopy.DestinationTableName = "dbo.ImportPickData";
                        bulkCopy.WriteToServer(dtSerevr);
                        MessageBox.Show("Data Exoprted To Sql Server Succefully", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        conn.Close();
                    }
                    SqlParameter[] para = { new SqlParameter("@Date", Convert.ToDateTime(dtpDate.Text).ToString("yyyy-MM-dd")) };
                    DataTable dtGrid = db.GetDataWithParam(CommandType.StoredProcedure, "sp_GetImportPickData", para, conn);
                    FillData(dtGrid);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                conn.Close();
                return;
            }
        }

        private void FillData(DataTable dt)
        {
            DataGridImport.DataSource = null;
            DataGridImport.AutoGenerateColumns = false;
            DataGridImport.ColumnCount = 7;
            DataGridImport.Columns[0].Name = "SLNo";
            DataGridImport.Columns[0].HeaderText = "SLNo";

            DataGridImport.Columns[1].Name = "PickUid";
            DataGridImport.Columns[1].HeaderText = "PickUid";
            DataGridImport.Columns[1].DataPropertyName = "PickUid";
            DataGridImport.Columns[1].Visible = false;
            DataGridImport.Columns[2].Name = "Date";
            DataGridImport.Columns[2].HeaderText = "Date";
            DataGridImport.Columns[2].DataPropertyName = "Dte";

            DataGridImport.Columns[3].Name = "Shift";
            DataGridImport.Columns[3].HeaderText = "Shift";
            DataGridImport.Columns[3].DataPropertyName = "GeneralName";
            DataGridImport.Columns[3].Width = 200;
            DataGridImport.Columns[4].Name = "LoomNumber";
            DataGridImport.Columns[4].HeaderText = "LoomNumber";
            DataGridImport.Columns[4].DataPropertyName = "LoomNumer";
            DataGridImport.Columns[4].Width = 150;
            DataGridImport.Columns[5].Name = "KPicks";
            DataGridImport.Columns[5].HeaderText = "KPicks";
            DataGridImport.Columns[5].DataPropertyName = "Kpicks";
            DataGridImport.Columns[5].Width = 150;
            DataGridImport.Columns[6].Name = "ShiftName";
            DataGridImport.Columns[6].HeaderText = "ShiftName";
            DataGridImport.Columns[6].DataPropertyName = "ShiftName";
            DataGridImport.Columns[6].Visible = false;
            DataGridImport.DataSource = dt;
            for (int i = 0; i < DataGridImport.Rows.Count; i++)
            {
                DataGridImport.Rows[i].Cells[0].Value = i + 1;
            }
        }

        private void btnImport_Click(object sender, EventArgs e)
        {
            grImport.Visible = true;
            int TypeM_uid = 27;
            int Active = 1;
            SqlParameter[] para = { new SqlParameter("@TypeM_Uid", TypeM_uid), new SqlParameter("@Active", Active) };
            DataTable dt = db.GetDataWithParam(CommandType.StoredProcedure, "SP_GetGeneralM", para, conn);
            cmbShift.DataSource = null;
            cmbShift.DisplayMember = "GeneralName";
            cmbShift.ValueMember = "Uid";
            cmbShift.DataSource = dt;
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            grImport.Visible = false;
        }
    }
}
