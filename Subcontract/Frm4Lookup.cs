﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Configuration;
using System.Data.SqlClient;
using System.Data.OleDb;

namespace Subcontract
{
    public partial class Frm4Lookup : Form
    {
        public Frm4Lookup()
        {
            InitializeComponent();
        }
        SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["connstr"].ConnectionString);

        private void Frm4Lookup_Load(object sender, EventArgs e)
        {
            Genclass.canclclik = false;
        }

        private void btnadd_Click(object sender, EventArgs e)
        {
            txtinp();
          
           
        }

        private void txtinp()
        {
            foreach (Form Frm in Application.OpenForms)
            {
                if (Frm.Name == Genclass.frmwhat)
                {
                    foreach (Control ccontrol in Frm.Controls)
                    {

                        if (ccontrol is Panel)
                        {

                            foreach (Control c in Genclass.Parent.Controls)
                            {
                                if (c is TextBox)
                                {
                                    {
                                        if (c.Name == Genclass.fieldone)
                                        {
                                            int i = Hfgp.SelectedCells[0].RowIndex;
                                            c.Text = Hfgp.Rows[i].Cells[0].Value.ToString();
                                        }
                                        if (c.Name == Genclass.fieldtwo)
                                        {
                                            int i = Hfgp.SelectedCells[0].RowIndex;
                                            c.Text = Hfgp.Rows[i].Cells[1].Value.ToString();
                                        }

                                        if (c.Name == Genclass.fieldthree)
                                        {
                                            int i = Hfgp.SelectedCells[0].RowIndex;
                                            c.Text = Hfgp.Rows[i].Cells[2].Value.ToString();
                                        }

                                        if (c.Name == Genclass.fieldFour)
                                        {
                                            int i = Hfgp.SelectedCells[0].RowIndex;
                                            c.Text = Hfgp.Rows[i].Cells[3].Value.ToString();
                                        }
                                        if (c.Name == Genclass.fieldFive)
                                        {
                                            int i = Hfgp.SelectedCells[0].RowIndex;
                                            c.Text = Hfgp.Rows[i].Cells[4].Value.ToString();
                                        }


                                    }
                                }
                            }
                        }
                    }
                }
            }
            this.Dispose();
        }

        private void btnaddrcan_Click(object sender, EventArgs e)
        {
            Genclass.canclclik = true;
            this.Dispose();
        }
    }
}