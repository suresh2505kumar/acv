﻿
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Drawing.Imaging;
using System.Configuration;
using System.Data.SqlClient;
using System.Diagnostics;
using excel = Microsoft.Office.Interop.Excel;
using CrystalDecisions.Shared;
using CrystalDecisions.CrystalReports.Engine;

//using Bytescout.Spreadsheet;
namespace ACV
{
    public partial class FrmPur : Form
    {

        public string fo
        {
            get { return txtitemname.Text; }
            set { txtitemname.Text = value; }

        }
        public FrmPur()
        {
            CenterToParent();
            this.BackColor = Color.White;
            InitializeComponent();
        }


        //private System.ComponentModel.Container components = null;

        ReportDocument doc = new ReportDocument();
        string uid = "";
        int mode = 0;

        double dis3 = 0;
        double dis4 = 0;
        double dd8 = 0;
        double dd1 = 0;
        double dd2 = 0;
        int j = -1;
        BindingSource bsc = new BindingSource();
        string str1key;

        //SqlCommand cmd;
        SqlConnection conn = new SqlConnection("Data Source=" + Genclass.data2 + "; Initial Catalog=" + Genclass.data5 + ";User id=" + Genclass.data3 + ";Password=" + Genclass.data4 + "");

        //SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["connstr"].ConnectionString);
        SqlCommand qur = new SqlCommand();
        private void FrmPur_Load(object sender, EventArgs e)
        {
            Genclass.Dtype = 90;
            qur.Connection = conn;


            Left = (MdiParent.ClientRectangle.Width - Width) / 3;
            Top = (MdiParent.ClientRectangle.Height - Height) / 3;
            HFGP.RowHeadersVisible = false;
            HFIT.RowHeadersVisible = false;
            HFGA.RowHeadersVisible = false;
            HFGT.RowHeadersVisible = false;
            HFGTAX.RowHeadersVisible = false;
            HFGST.RowHeadersVisible = false;
            HFGP2.RowHeadersVisible = false;
            Genpan.Visible = true;
            Taxpan.Visible = false;
            Editpan.Visible = false;
            TitleAdd();
            Titleterm();
            Titlep();
            Titlegst();

            this.HFGP.DefaultCellStyle.Font = new Font("calibri", 10);
            this.HFGP.ColumnHeadersDefaultCellStyle.Font = new Font("calibri", 10, FontStyle.Bold);
            this.HFIT.DefaultCellStyle.Font = new Font("calibri", 10);
            this.HFIT.ColumnHeadersDefaultCellStyle.Font = new Font("calibri", 10, FontStyle.Bold);
            this.HFGA.DefaultCellStyle.Font = new Font("calibri", 10);
            this.HFGA.ColumnHeadersDefaultCellStyle.Font = new Font("calibri", 10, FontStyle.Bold);
            this.HFGT.DefaultCellStyle.Font = new Font("calibri", 10);
            this.HFGT.ColumnHeadersDefaultCellStyle.Font = new Font("calibri", 10, FontStyle.Bold);
            this.HFGTAX.DefaultCellStyle.Font = new Font("calibri", 10);
            this.HFGTAX.ColumnHeadersDefaultCellStyle.Font = new Font("calibri", 10, FontStyle.Bold);
            this.HFGST.DefaultCellStyle.Font = new Font("calibri", 10);
            this.HFGST.ColumnHeadersDefaultCellStyle.Font = new Font("calibri", 10, FontStyle.Bold);
            this.HFGP2.DefaultCellStyle.Font = new Font("calibri", 10);
            this.HFGP2.ColumnHeadersDefaultCellStyle.Font = new Font("calibri", 10, FontStyle.Bold);
            txtname.ReadOnly = true;
            txtplace.ReadOnly = true;
            txtitemname.ReadOnly = true;


            txttbval.ReadOnly = true;
            txttprdval.ReadOnly = true;
            txtexcise.ReadOnly = true;
            txtigval.ReadOnly = true;
            txtttot.ReadOnly = true;
            TxtNetAmt.ReadOnly = true;
            //loadtax();

            txtexcise.Text = "";


            chkact.Checked = true;
            Genclass.Module.buttonstyleform(this);
            Genclass.Module.buttonstylepanel(panadd);
            panadd.Visible = true;
            HFGP.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            HFGP.EnableHeadersVisualStyles = false;
            HFGP.ColumnHeadersDefaultCellStyle.BackColor = SystemColors.GradientActiveCaption;
            HFIT.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            HFIT.EnableHeadersVisualStyles = false;
            HFIT.ColumnHeadersDefaultCellStyle.BackColor = SystemColors.GradientActiveCaption;
            HFGA.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            HFGA.EnableHeadersVisualStyles = false;
            HFGA.ColumnHeadersDefaultCellStyle.BackColor = SystemColors.GradientActiveCaption;
            //HFGT.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            HFGT.EnableHeadersVisualStyles = false;
            HFGT.ColumnHeadersDefaultCellStyle.BackColor = SystemColors.GradientActiveCaption;
            HFGTAX.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            HFGTAX.EnableHeadersVisualStyles = false;
            HFGTAX.ColumnHeadersDefaultCellStyle.BackColor = SystemColors.GradientActiveCaption;
            HFGST.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            HFGST.EnableHeadersVisualStyles = false;
            HFGST.ColumnHeadersDefaultCellStyle.BackColor = SystemColors.GradientActiveCaption;
            HFGP2.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            HFGP2.EnableHeadersVisualStyles = false;
            HFGP2.ColumnHeadersDefaultCellStyle.BackColor = SystemColors.GradientActiveCaption;
            HFGP.Focus();
            Genclass.sum1 = 0;
            lkppnl.Visible = false;
        }



        private void loadtax()
        {

            conn.Open();
            string qur = "select a.UId,a.GeneralName from  GENERALM a inner join typem b  on a.TypeM_Uid=b.UId where a.TypeM_Uid in (4,5,6,7,8,9) and Active=1 ";
            SqlCommand cmd = new SqlCommand(qur, conn);
            SqlDataAdapter apt = new SqlDataAdapter(cmd);
            DataTable tab = new DataTable();
            apt.Fill(tab);
            //cbotax.DataSource = null;
            //cbotax.DataSource = tab;
            //cbotax.DisplayMember = "GeneralName";
            //cbotax.ValueMember = "uid";
            //cbotax.SelectedIndex = -1;
            conn.Close();



        }
        private void Loadgrid()
        {
            try
            {
                conn.Close();
                conn.Open();

                Genclass.StrSrch = "";


                Genclass.FSSQLSortStr = "Docno";
                Genclass.FSSQLSortStr1 = "Docdate";
                //Genclass.FSSQLSortStr2 = "Dcno";
                //Genclass.FSSQLSortStr3 = "Dcdate";
                Genclass.FSSQLSortStr4 = "b.Name";
                Genclass.FSSQLSortStr5 = "Netvalue";



                if (txtscr1.Text != "")
                {
                    if (Genclass.StrSrch == "")
                    {
                        Genclass.StrSrch = Genclass.FSSQLSortStr + " like '%" + txtscr1.Text + "%'";
                    }
                    else
                    {
                        Genclass.StrSrch = Genclass.StrSrch + " and " + Genclass.FSSQLSortStr + " like '%" + txtscr1.Text + "%'";
                    }

                }

                if (Txtscr2.Text != "")
                {
                    if (Genclass.StrSrch == "")
                    {
                        Genclass.StrSrch = Genclass.FSSQLSortStr1 + " like '%" + Txtscr2.Text + "%'";
                    }
                    else
                    {
                        Genclass.StrSrch = Genclass.StrSrch + " and " + Genclass.FSSQLSortStr1 + " like '%" + Txtscr2.Text + "%'";
                    }

                }

                if (Txtscr3.Text != "")
                {
                    if (Genclass.StrSrch == "")
                    {
                        Genclass.StrSrch = Genclass.FSSQLSortStr2 + " like '%" + Txtscr3.Text + "%'";
                    }
                    else
                    {
                        Genclass.StrSrch = Genclass.StrSrch + " and " + Genclass.FSSQLSortStr2 + " like '%" + Txtscr3.Text + "%'";
                    }

                }

                if (txtscr4.Text != "")
                {
                    if (Genclass.StrSrch == "")
                    {
                        Genclass.StrSrch = Genclass.FSSQLSortStr3 + " like '%" + txtscr4.Text + "%'";
                    }
                    else
                    {
                        Genclass.StrSrch = Genclass.StrSrch + " and " + Genclass.FSSQLSortStr3 + " like '%" + txtscr4.Text + "%'";
                    }

                }

                if (txtscr5.Text != "")
                {
                    if (Genclass.StrSrch == "")
                    {
                        Genclass.StrSrch = Genclass.FSSQLSortStr4 + " like '%" + txtscr5.Text + "%'";
                    }
                    else
                    {
                        Genclass.StrSrch = Genclass.StrSrch + " and " + Genclass.FSSQLSortStr4 + " like '%" + txtscr5.Text + "%'";
                    }

                }

                if (txtscr6.Text != "")
                {
                    if (Genclass.StrSrch == "")
                    {
                        Genclass.StrSrch = Genclass.FSSQLSortStr5 + " like '%" + txtscr6.Text + "%'";
                    }
                    else
                    {
                        Genclass.StrSrch = Genclass.StrSrch + " and " + Genclass.FSSQLSortStr5 + " like '%" + txtscr6.Text + "%'";
                    }

                }





                if (txtscr1.Text != "")
                {
                    Genclass.StrSrch = " " + Genclass.StrSrch;
                }
                else if (Txtscr2.Text != "")
                {
                    Genclass.StrSrch = " " + Genclass.StrSrch;
                }

                else if (Txtscr3.Text != "")
                {
                    Genclass.StrSrch = " " + Genclass.StrSrch;
                }
                else if (txtscr4.Text != "")
                {
                    Genclass.StrSrch = " " + Genclass.StrSrch;
                }
                else if (txtscr5.Text != "")
                {
                    Genclass.StrSrch = " " + Genclass.StrSrch;
                }
                else if (txtscr6.Text != "")
                {
                    Genclass.StrSrch = " " + Genclass.StrSrch;
                }
                else
                {
                    Genclass.StrSrch = "a.uid <> 0";
                }
                if (Genclass.Dtype == 90)
                {
                    if (chkact.Checked == true)
                    {
                        string quy = "Select distinct a.Uid,DocNo,convert(nvarchar,docdate,106) as  DocDate,Dcno, convert(nvarchar,docdate,106) as  Dcdate,b.Name,a.Netvalue,a.partyuid,a.placeuid,isnull(c.name,'') as Placename,dtpre,dtrem,transp,vehno,a.remarks from stransactionsp a inner join  partym b on a.partyuid=b.uid left join  partym c on a.placeuid=c.uid where a.active=1 and a.doctypeid=90 and a.companyid=" + Genclass.data1 + " and  " + Genclass.StrSrch + "";
                        Genclass.cmd = new SqlCommand(quy, conn);
                    }
                    else
                    {

                        string quy = "Select distinct a.Uid,DocNo, convert(nvarchar,docdate,106) as  DocDate,Dcno, convert(nvarchar,docdate,106) as  Dcdate,b.Name,a.Netvalue,a.partyuid,a.placeuid,isnull(c.name,'') as Placename,dtpre,dtrem,transp,vehno,a.remarks from stransactionsp a inner join  partym b on a.partyuid=b.uid left join  partym c on a.placeuid=c.uid where a.active=0 and a.doctypeid=90 and a.companyid=" + Genclass.data1 + "  and  " + Genclass.StrSrch + "";
                        Genclass.cmd = new SqlCommand(quy, conn);
                    }

                }
                else if (Genclass.Dtype == 80)
                {
                    if (chkact.Checked == true)
                    {
                        string quy = "Select distinct a.Uid,DocNo, convert(nvarchar,docdate,106) as  DocDate,Dcno, convert(nvarchar,docdate,106) as Dcdate,Name,a.Netvalue,a.partyuid from stransactionsp a inner join  partym b on a.partyuid=b.uid  where a.active=1 and a.doctypeid=80  and  " + Genclass.StrSrch + "";
                        Genclass.cmd = new SqlCommand(quy, conn);
                    }
                    else
                    {
                        string quy = "Select distinct a.Uid,DocNo, convert(nvarchar,docdate,106) as  DocDate,Dcno,convert(nvarchar,docdate,106) as Dcdate,Name,a.Netvalue,a.partyuid from stransactionsp a inner join  partym b on a.partyuid=b.uid  where a.active=0 and a.doctypeid=80  and  " + Genclass.StrSrch + "";
                        Genclass.cmd = new SqlCommand(quy, conn);
                    }
                }

                SqlDataAdapter aptr = new SqlDataAdapter(Genclass.cmd);
                DataTable tap = new DataTable();
                aptr.Fill(tap);




                HFGP.AutoGenerateColumns = false;
                HFGP.Refresh();
                HFGP.DataSource = null;
                HFGP.Rows.Clear();


                HFGP.ColumnCount = tap.Columns.Count;
                Genclass.i = 0;

                foreach (DataColumn column in tap.Columns)
                {
                    HFGP.Columns[Genclass.i].Name = column.ColumnName;
                    HFGP.Columns[Genclass.i].HeaderText = column.ColumnName;
                    HFGP.Columns[Genclass.i].DataPropertyName = column.ColumnName;
                    Genclass.i = Genclass.i + 1;
                }



                HFGP.Columns[0].Visible = false;

                HFGP.Columns[1].Width = 87;
                HFGP.Columns[2].Width = 89;
                HFGP.Columns[3].Visible = false;
                HFGP.Columns[4].Visible = false;
                HFGP.Columns[5].Width = 870;
                HFGP.Columns[6].Width = 100;
                HFGP.Columns[7].Visible = false;
                HFGP.Columns[8].Visible = false;
                HFGP.Columns[9].Visible = false;
                HFGP.Columns[10].Visible = false;
                HFGP.Columns[11].Visible = false;
                HFGP.Columns[12].Visible = false;
                HFGP.Columns[13].Visible = false;
                HFGP.Columns[14].Visible = false;



                HFGP.DataSource = tap;
                HFGP.Columns[2].DefaultCellStyle.Format = "yyyy'/'MM'/'dd";
                HFGP.Columns[4].DefaultCellStyle.Format = "yyyy'/'MM'/'dd";


                int index = HFGP.Rows.Count - 1;

                if (HFGP.Rows[0].Cells[1].Value.ToString() == "" || HFGP.Rows[0].Cells[1].Value == null)
                {
                    lblno1.Text = "0";
                }
                else
                {
                    lblno1.Text = "1";
                }


                lblno2.Text = "of " + index.ToString();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return;
            }
            finally
            {
                conn.Close();
                Genclass.cmd.Dispose();
            }
        }


        private void Titlep()
        {
            HFIT.ColumnCount = 17;
            HFIT.Columns[0].Name = "Itemname";
            HFIT.Columns[1].Name = "UoM";
            //HFIT.Columns[2].Name = "Addnotes";
            HFIT.Columns[2].Name = "Price";
            HFIT.Columns[3].Name = "Qty";
            HFIT.Columns[4].Name = "Value";
            HFIT.Columns[5].Name = "Itemuid";
            HFIT.Columns[6].Name = "Refuid";
            HFIT.Columns[7].Name = "Dis%";
            HFIT.Columns[8].Name = "Disval";
            HFIT.Columns[9].Name = "Taxableval";
            HFIT.Columns[10].Name = "GST%";
            HFIT.Columns[11].Name = "GSTVAL";
            HFIT.Columns[12].Name = "VAT%";
            HFIT.Columns[13].Name = "VATval";
            HFIT.Columns[14].Name = "Total";
            HFIT.Columns[15].Name = "Hsnuid";
            HFIT.Columns[16].Name = "Notes";



            HFIT.Columns[0].Width = 310;
            HFIT.Columns[1].Width = 62;
            HFIT.Columns[2].Width = 68;
            HFIT.Columns[3].Width = 60;
            HFIT.Columns[4].Width = 99;

            HFIT.Columns[5].Visible = false;
            HFIT.Columns[6].Visible = false;

            HFIT.Columns[7].Visible = false;

            HFIT.Columns[8].Visible = false;
            HFIT.Columns[9].Visible = false;
            HFIT.Columns[10].Width = 60;
            HFIT.Columns[11].Visible = false;
            HFIT.Columns[12].Visible = false;
            HFIT.Columns[13].Visible = false;
            HFIT.Columns[14].Visible = false;
            HFIT.Columns[15].Visible = false;
            HFIT.Columns[16].Width = 200;

        }

        private void Titletax()
        {
            HFGTAX.ColumnCount = 6;
            HFGTAX.Columns[0].Name = "Taxable Value";
            HFGTAX.Columns[1].Name = "SGST%";
            HFGTAX.Columns[2].Name = "SGST Value";
            HFGTAX.Columns[3].Name = "CGST%";
            HFGTAX.Columns[4].Name = "CGST Value";
            HFGTAX.Columns[0].Width = 100;
            HFGTAX.Columns[1].Width = 60;
            HFGTAX.Columns[2].Width = 100;
            HFGTAX.Columns[3].Width = 60;
            HFGTAX.Columns[4].Width = 100;
            HFGTAX.Columns[5].Name = "hsnid";
            HFGTAX.Columns[5].Visible = false;
        }

        private void Loadgrid1()
        {
            try
            {
                conn.Open();

                string quy = "select c.itemname,b.pqty,b.itemuid,refuid from transactionsp a inner join transactionsplist b on a.uid=b.transactionspuid and a.companyid=" + Genclass.data1 + " inner join itemm c on b.itemuid=c.uid where a.uid=" + uid + "";


                Genclass.cmd = new SqlCommand(quy, conn);



                SqlDataAdapter aptr = new SqlDataAdapter(Genclass.cmd);
                DataTable tap = new DataTable();
                aptr.Fill(tap);

                this.HFIT.DefaultCellStyle.Font = new Font("Arial", 10);
                //dgv.ColumnHeadersDefaultCellStyle.Font = new Font(dgv.ColumnHeadersDefaultCellStyle.Font, FontStyle.Bold);
                this.HFIT.ColumnHeadersDefaultCellStyle.Font = new Font("Arial", 10, FontStyle.Bold);
                HFIT.AutoGenerateColumns = false;
                HFIT.Refresh();
                HFIT.DataSource = null;
                HFIT.Rows.Clear();


                HFIT.ColumnCount = tap.Columns.Count;
                Genclass.i = 0;
                foreach (DataColumn column in tap.Columns)
                {
                    HFIT.Columns[Genclass.i].Name = column.ColumnName;
                    HFIT.Columns[Genclass.i].HeaderText = column.ColumnName;
                    HFIT.Columns[Genclass.i].DataPropertyName = column.ColumnName;
                    Genclass.i = Genclass.i + 1;
                }



                HFIT.Columns[0].Width = 450;

                HFIT.Columns[1].Width = 75;
                HFIT.Columns[2].Visible = false;
                HFIT.Columns[3].Visible = false;

                //HFIT.Columns[5].Width = 400;




                HFIT.DataSource = tap;

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return;
            }
            finally
            {
                conn.Close();
                Genclass.cmd.Dispose();
            }
        }
        private void button1_Click(object sender, System.EventArgs e)
        {
            mode = 1;
            Genpan.Visible = false;

            Genclass.Module.ClearTextBox(this, Editpan);
            if (Genclass.Dtype == 40)
            {
                Genclass.Module.Gendocno();
                txtgrn.Text = Genclass.ST;
                label15.Visible = false;
                txtitemname.Visible = false;
                label14.Visible = false;
                txtprice.Visible = false;
                label31.Visible = false;
                txtqty.Visible = false;
                label32.Visible = false;
                txtbval.Visible = false;

            }
            else if (Genclass.Dtype == 80)
            {
                Genclass.Module.Gendocno();
                txtgrn.Text = Genclass.ST;
            }
            Editpan.Visible = true;
            DTPDOCDT.Focus();
            HFIT.Refresh();
            HFIT.DataSource = null;
            HFIT.Rows.Clear();

        }

        private void loadput()
        {
            conn.Close();
            conn.Open();

            if (Genclass.type == 1)
            {
                Genclass.Module.Partylistviewcont3("uid", "Name", "Address1", Genclass.strsql, this, txtpuid, txtname, txttempadd1, Editpan);
                Genclass.strsql = "select top 25 uid,Name as Party,Address1  from Partym where active=1 and ptype<>1  and  companyid=" + Genclass.data1 + " and type='Supplier' order by name";
                //Genclass.strsql = "select distinct  c.uid,c.name from transactionsp a inner join transactionsplist b on  a.uid=b.transactionspuid and a.doctypeid=20 and a.companyid=" + Genclass.data1 + " inner join partym c on a.partyuid=c.uid left join stransactionsplist e on  b.uid=e.refuid and e.doctypeid=40 group by c.uid,c.name,b.pqty  having b.pqty-isnull(sum(e.pqty),0) >0";

                Genclass.FSSQLSortStr = "Name";
            }

            else if (Genclass.type == 2)
            {
                Genclass.Module.Partylistviewcont("uid", "Docno", Genclass.strsql, this, txtdcid, txtdcno, Editpan);
                Genclass.strsql = "select distinct  a.uid,a.docno as Dcno from transactionsp a inner join transactionsplist b on  a.uid=b.transactionspuid and a.doctypeid=20 and a.companyid=" + Genclass.data1 + " inner join partym c on a.partyuid=c.uid   left join stransactionsplist e on  b.uid=e.refuid and e.doctypeid=90 group by a.uid,a.docno,b.pqty  having b.pqty-isnull(sum(e.pqty),0) >0";
                Genclass.FSSQLSortStr = "docno";
            }

            else if (Genclass.type == 3)
            {
                Genclass.Module.Partylistviewcont("uid", "AddtionalCharges", Genclass.strsql, this, txtaddid, txtaddcharge, addipan);
                Genclass.strsql = "select Uid ,GeneralName   from Generalm where TypeM_UId =13 and active=1 and companyid==" + Genclass.data1 + " ";
                Genclass.FSSQLSortStr = "GeneralName";
            }
            else if (Genclass.type == 4)
            {
                Genclass.Module.Partylistviewcont("uid", "Terms", Genclass.strsql, this, txttermid, txtterms, termspan);
                Genclass.strsql = "select Uid ,GeneralName   from Generalm where TypeM_UId =14 and active=1 and companyid==" + Genclass.data1 + " ";
                Genclass.FSSQLSortStr = "GeneralName";
            }

            else if (Genclass.type == 5)
            {

                string quy4 = "select * from pur_price_list where suppuid=" + txtpuid.Text + "";
                Genclass.cmd = new SqlCommand(quy4, conn);
                SqlDataAdapter aptr4 = new SqlDataAdapter(Genclass.cmd);
                DataTable tap4 = new DataTable();
                aptr4.Fill(tap4);

                if (tap4.Rows.Count > 0)
                {
                    Genclass.Module.Partylistviewcont2("uid", "Itemcode", "Itemname", "Price", Genclass.strsql, this, txttitemid, txtdcid, txtitemname, txtprice, Editpan);
                    Genclass.strsql = "select top 25 b.uid,itemcode,ItemName,price  from pur_price_list a inner join itemm b on a.itemuid=b.uid where a.suppuid=" + txtpuid.Text + "  and a.companyid=" + Genclass.data1 + "";
                    Genclass.FSSQLSortStr = "itemcode";
                }
                else
                {
                    Genclass.Module.Partylistviewcont3("uid", "Item", "Itemcode", Genclass.strsql, this, txttitemid, txtitemname, txtdcid, Editpan);
                    Genclass.strsql = "select top 25 uid,ItemName,Itemcode  from Itemm where active=1  and companyid=" + Genclass.data1 + "";
                    Genclass.FSSQLSortStr = "ItemName";
                }



            }
            else if (Genclass.type == 6)

            {
                Genclass.Module.Partylistviewcont("uid", "Name", Genclass.strsql, this, txtpuid, txtname, Editpan);
                Genclass.strsql = "select top 50 uid,Name as Party from Partym where active=1 and ptype<>1 and  companyid=" + Genclass.data1 + " ";
                //Genclass.strsql = "    select distinct b.uid,name from pur_price_list a inner join partym b on a.Suppuid=b.uid where  active=1 and a.companyid=1 and a.Eff_to is null";

                Genclass.FSSQLSortStr = "Name";
            }

            else if (Genclass.type == 7)
            {
                Genclass.Module.Partylistviewcont3("uid", "Name", "Address1", Genclass.strsql, this, txtpluid, txtplace, txttempadd2, Editpan);
                Genclass.strsql = "select top 25 uid,Name as Party,Address1  from Partym where active=1 and ptype<>1  and  companyid=" + Genclass.data1 + "";

                Genclass.FSSQLSortStr = "Name";
            }

            Genclass.cmd = new SqlCommand(Genclass.strsql, conn);
            SqlDataAdapter aptr = new SqlDataAdapter(Genclass.cmd);
            DataTable tap = new DataTable();
            aptr.Fill(tap);

            Frmlookup contc = new Frmlookup();
            contc.width = 750;
            DataGridView dt = (DataGridView)contc.Controls["HFGP"];
            dt.Refresh();
            dt.ColumnCount = tap.Columns.Count;
            dt.Columns[0].Visible = false;
            dt.Columns[1].Width = 400;

            if (Genclass.type == 1 || Genclass.type == 7 || Genclass.type == 5)
            {

                dt.Columns[2].Width = 280;

            }


            dt.DefaultCellStyle.Font = new Font("Arial", 10);

            dt.ColumnHeadersDefaultCellStyle.Font = new Font("Arial", 10, FontStyle.Bold);
            dt.AutoGenerateColumns = false;

            Genclass.i = 0;
            foreach (DataColumn column in tap.Columns)
            {
                dt.Columns[Genclass.i].Name = column.ColumnName;
                dt.Columns[Genclass.i].HeaderText = column.ColumnName;
                dt.Columns[Genclass.i].DataPropertyName = column.ColumnName;
                Genclass.i = Genclass.i + 1;
            }

            dt.DataSource = tap;
            contc.width = 750;
            contc.Show();

            conn.Close();


        }

        private Size New(int p1, int p2)
        {
            throw new NotImplementedException();
        }


        //private void HFIT_CellEnter(object sender, DataGridViewCellEventArgs e)
        //{
        //    int i = HFIT.SelectedCells[0].RowIndex;
        //    HFIT.Rows[i].Cells[4].Value = Convert.ToDouble(HFIT.Rows[i].Cells[2].Value) * Convert.ToDouble(HFIT.Rows[i].Cells[3].Value);
        //}

        //private void HFIT_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        //{
        //    if (HFIT.RowCount - 2 > 0)
        //    {
        //        Genclass.sum = 0;
        //        txtamt.Text = null;
        //        txtamttot.Text = null;
        //        if (HFIT.RowCount > 2)
        //        {
        //            for (int i = 0; i < HFIT.RowCount - 1; i++)
        //            {
        //                if (HFIT.Rows[i].Cells[4].Value.ToString() == "" || HFIT.Rows[i].Cells[4].Value.ToString() == null)
        //                {
        //                    return;
        //                }

        //                else
        //                {
        //                    //int sum1 = (int.TryParse(HFIT.Rows[i].Cells[2].Value.ToString(), out quantity) && int.TryParse(HFIT.Rows[i].Cells[4].Value.ToString(), out rate));
        //                    Genclass.sum = Genclass.sum + Convert.ToInt16(HFIT.Rows[i].Cells[4].Value.ToString());

        //                    txtamttot.Text = Genclass.sum.ToString();
        //                    txtamt.Text = txtamttot.Text;
        //                }
        //            }
        //        }
        //    }
        //}


        private void btnexit_Click(object sender, System.EventArgs e)
        {
            this.Dispose();
        }

        private void txtper_TextChanged(object sender, System.EventArgs e)
        {
            int val1;
            int val2;
            int val3;
            val1 = Convert.ToInt16(txttaxable.Text);
            val2 = Convert.ToInt16(txtper.Text);
            val3 = (val1 * val2) / 100;
            //txtdis.Text = Convert.ToString(val3);
            CalcNetAmt();

        }

        //private void textBox2_TextChanged(object sender, System.EventArgs e)
        //{

        //}
        private void CalcNetAmt()
        {

            //int val4;
            //int val5;
            //int roff;
            int totamt;

            totamt = Convert.ToInt16(txttaxable.Text);

            //int dis = Convert.ToInt16(txtdis.Text);
            //if (txtper.Text == "")
            //{
            //    TxtNetAmt.Text = txttaxable.Text + txtexcise.Text + txttax.Text + txtpf.Text;
            //    val4 = Convert.ToInt16(TxtNetAmt.Text);
            //    val5 = val4 - Convert.ToInt16(txtdis.Text);
            //    // Genclass.strfin = totamt + Convert.ToInt16(txtpf.Text) - dis;

            //    TxtNetAmt.Text = Convert.ToString(val5);
            //}
            //else
            //{
            //    TxtNetAmt.Text = "0.00";


            //    TxtNetAmt.Text = txttaxable.Text + txtexcise.Text + txttax.Text + txtpf.Text;
            //    int cal5 = Convert.ToInt16(TxtNetAmt.Text);
            //    val4 = Convert.ToInt16(TxtNetAmt.Text) - Convert.ToInt16(txtdis.Text);
            //    //val5 = val4 - Convert.ToInt16(txtdis.Text);
            //    //txtpf.Text = "0.00";
            //    string cal1 = txtpf.Text;


            //TxtNetAmt.Text = Convert.ToString(val4);
            Genclass.strfin = TxtNetAmt.Text;
        }

        //TxtRoff.Text =val4 -Convert.ToString(TxtNetAmt.Text);

        //}

        //private void cboexcise_SelectedIndexChanged(object sender, System.EventArgs e)
        //{

        //    }
        //private void exciseduty()
        //{

        // if(cboexcise.SelectedValue==null)
        //    {
        //    return;

        //    }
        //    else
        //    {
        //    conn.Open();
        //        {
        //         Genclass.strsql = "Select * from GeneralM where TypeM_Uid=5 and Uid= "+ cboexcise.SelectedValue +" ";
        //        Genclass.cmd = new SqlCommand(Genclass.strsql, conn);
        //        SqlDataAdapter aptr1 = new SqlDataAdapter(Genclass.cmd);
        //        DataTable tap1 = new DataTable();
        //        aptr1.Fill(tap1);
        //        LblED1.Text = tap1.Rows[0]["F1"].ToString();
        //        LblCess.Text = tap1.Rows[0]["F2"].ToString();
        //        LblHECess.Text = tap1.Rows[0]["F3"].ToString();

        //        }
        //}



        //        }


        private void Taxduty()
        {

            //if (cbotax.SelectedValue == null)
            //{
            //    return;

            //}
            //else
            //{
            //    conn.Open();
            //    {
            //        Genclass.strsql = "Select * from GeneralM where  Uid= " + cbotax.SelectedValue + " ";
            //        Genclass.cmd = new SqlCommand(Genclass.strsql, conn);
            //        SqlDataAdapter aptr1 = new SqlDataAdapter(Genclass.cmd);
            //        DataTable tap1 = new DataTable();
            //        aptr1.Fill(tap1);
            //        lbltax.Text = tap1.Rows[0]["F1"].ToString();


            //    }
            //    conn.Close();
            //}



        }

        //private void cboexcise_Click(object sender, System.EventArgs e)
        //{
        //    if (cboexcise.SelectedValue != null)
        //    {
        //        exciseduty();
        //        txtted.Text = "";
        //        CalcNetAmt();
        //        int ed;
        //        int Cess;
        //        int HECess;
        //        int excise;
        //        int lbled;

        //        //val3 = (val1 * val2) / 100;
        //        //txtdis.Text = Convert.ToString(val3);
        //        lbled = Convert.ToInt16(LblED1.Text);
        //        ed = (Convert.ToInt16(Genclass.strfin) * lbled) / 100;
        //        txtted.Text = Convert.ToString(ed);
        //        Cess = ed * Convert.ToInt16(LblCess.Text) / 100;
        //        txtcess.Text = Convert.ToString(Cess);
        //        HECess = ed * Convert.ToInt16(LblHECess.Text) / 100;
        //        txthecess.Text = Convert.ToString(HECess);
        //        excise = ed + Cess + HECess;
        //        txtexcise.Text = Convert.ToString(excise);

        //    }   
        //    else
        //    {
        //        txtted.Text = "0.00";
        //        txtcess.Text = "0.00";
        //        txthecess.Text = "0.00";
        //        txtexcise.Text = "";
        //        LblED1.Text = "";
        //        LblCess.Text = "";
        //        LblHECess.Text = "";



        //    }

        //}

        private void btnsave_Click(object sender, System.EventArgs e)
        {
            if (txtname.Text == "")
            {
                MessageBox.Show("Enter the Party");
                txtname.Focus();
            }

            if (conn.State != ConnectionState.Open)
            {
                conn.Open();
            }


            Genclass.Dtype = 90;


            if (txttdis.Text == "")
            {
                txttdis.Text = "0";
            }
            if (txttdisc.Text == "")
            {
                txttdisc.Text = "0";
            }
            if (txtcharges.Text == "")
            {
                txtcharges.Text = "0";
            }
            if (mode == 2)
            {
                qur.CommandText = "delete from Stransactionsplist where transactionspuid=" + uid + "";
                qur.ExecuteNonQuery();
            }
            for (int i = 0; i < HFIT.RowCount - 1; i++)
            {
                Txttot.Text = Txttot.Text.Replace(",", "");
                txtexcise.Text = txtexcise.Text.Replace(",", "");
                if (mode == 1)
                {

                    //qur.CommandText = "Exec Sp_SalesINvoice " + Genclass.Dtype + ",'" + txtgrn.Text + "','" + DTPDOCDT.Value + "','" + txtdcno.Text + "','" + Dtpdt.Value + "'," + txtpuid.Text + ",'" + txtrem.Text + "',1," + Genclass.data1 + "," + HFIT.Rows[i].Cells[5].Value + " ," + HFIT.Rows[i].Cells[3].Value + " ," + HFIT.Rows[i].Cells[2].Value + "   ,   " + HFIT.Rows[i].Cells[4].Value + " , 0," + HFIT.Rows[i].Cells[7].Value + "   ,    " + HFIT.Rows[i].Cells[8].Value + "    ,0, 0 ,0, 0,  " + HFIT.Rows[i].Cells[10].Value + ", " + HFIT.Rows[i].Cells[11].Value + ",0," + i + "," + mode + "," + Txttot.Text + "," + TxtNetAmt.Text + "," + Genclass.Yearid + "," + HFIT.Rows[i].Cells[9].Value + "," + TxtRoff.Text + ",'" + Dtppre.Value + "','" + Dtprem.Value + "','" + txttrans.Text + "','" + txtveh.Text + "'," + txtpluid.Text + "";
                    if (i == 0)
                    {
                        qur.CommandText = "insert into Stransactionsp values(" + Genclass.Dtype + ",'" + txtgrn.Text + "','" + DTPDOCDT.Value + "','" + txtdcno.Text + "','" + Dtpdt.Value + "'," + txtpuid.Text + ",'" + txtrem.Text + "',1," + Genclass.data1 + "," + TxtNetAmt.Text + "," + Genclass.Yearid + "," + TxtRoff.Text + ",'" + Dtppre.Value + "','" + Dtprem.Value + "','" + txttrans.Text + "','" + txtveh.Text + "'," + txtpluid.Text + ",0)";
                        qur.ExecuteNonQuery();
                    }
                    string quy = "select uid from STransactionsP where doctypeid=" + Genclass.Dtype + "  and  docno='" + txtgrn.Text + "' and companyid=" + Genclass.data1 + "";
                    Genclass.cmd = new SqlCommand(quy, conn);
                    SqlDataAdapter aptr = new SqlDataAdapter(Genclass.cmd);
                    DataTable tap = new DataTable();
                    aptr.Fill(tap);

                    txtgrnid.Text = tap.Rows[0]["uid"].ToString();


                    string quyt = "select isnull(h.f1,0) as f1,z.generalname as uom from itemm b  inner join generalm z on b.uom_uid=z.uid left join itemgroup c on b.itemgroup_uid=c.uid  left join generalm h on b.tax=h.uid where b.uid=" + HFIT.Rows[i].Cells[5].Value + "";
                    Genclass.cmd = new SqlCommand(quyt, conn);
                    SqlDataAdapter aptrt = new SqlDataAdapter(Genclass.cmd);
                    DataTable tapt = new DataTable();
                    aptrt.Fill(tapt);

                    if (tapt.Rows.Count > 0)
                    {
                        if (txtstid.Text == "24")
                        {
                            dd1 = Convert.ToDouble(tapt.Rows[0]["f1"].ToString()) / 2;

                            txttcgstp.Text = dd1.ToString("0.00");
                            if (txttdis.Text == "0")
                            {
                                dd2 = ((Convert.ToDouble(HFIT.Rows[i].Cells[4].Value) / 100) * Convert.ToDouble(tapt.Rows[0]["f1"].ToString())) / 2;
                                dd8 = 0;
                            }
                            else
                            {
                                dd8 = ((Convert.ToDouble(HFIT.Rows[i].Cells[4].Value) / 100) * Convert.ToDouble(txttdis.Text));

                                dd2 = (((Convert.ToDouble(HFIT.Rows[i].Cells[4].Value) - dd8) / 100) * Convert.ToDouble(tapt.Rows[0]["f1"].ToString()) / 2);
                            }
                            txttcgval.Text = dd2.ToString("0.00");
                            double dd9 = Convert.ToDouble(HFIT.Rows[i].Cells[4].Value) - dd8;
                            txtbval.Text = dd9.ToString("0.00");

                            qur.CommandText = "insert into Stransactionsplist values(" + Genclass.Dtype + "," + txtgrnid.Text + "," + HFIT.Rows[i].Cells[5].Value + "," + HFIT.Rows[i].Cells[3].Value + " ," + HFIT.Rows[i].Cells[2].Value + "   ,   " + HFIT.Rows[i].Cells[4].Value + ",  " + txtbval.Text + ", 0," + txttdis.Text + "   ,   " + dd8 + "    ,  " + txttcgstp.Text + ", " + txttcgval.Text + ", " + txttcgstp.Text + ", " + txttcgval.Text + ",0,0," + mode + "," + Txttot.Text + ",'" + HFIT.Rows[i].Cells[16].Value + "',0,0,0)";
                            qur.ExecuteNonQuery();
                            txtbval.Text = "";
                        }
                        else

                        {

                            dd8 = ((Convert.ToDouble(HFIT.Rows[i].Cells[4].Value) / 100) * Convert.ToDouble(txttdis.Text));

                            dd2 = (((Convert.ToDouble(HFIT.Rows[i].Cells[4].Value) - dd8) / 100) * Convert.ToDouble(tapt.Rows[0]["f1"].ToString()));
                            txttcgstp.Text = tapt.Rows[0]["f1"].ToString();
                            txttcgval.Text = dd2.ToString("0.00");
                            double dd9 = Convert.ToDouble(HFIT.Rows[i].Cells[4].Value) - dd8;
                            txtbval.Text = dd9.ToString("0.00");
                            qur.CommandText = "insert into Stransactionsplist values(" + Genclass.Dtype + "," + txtgrnid.Text + "," + HFIT.Rows[i].Cells[5].Value + "," + HFIT.Rows[i].Cells[3].Value + " ," + HFIT.Rows[i].Cells[2].Value + "," + HFIT.Rows[i].Cells[4].Value + "," + txtbval.Text + ",0," + txttdis.Text + "," + dd8 + ",0, 0 ,0, 0, " + txttcgstp.Text + ", " + txttcgval.Text + "," + mode + "," + Txttot.Text + ",'" + HFIT.Rows[i].Cells[16].Value + "',0,0,0)";
                            qur.ExecuteNonQuery();
                        }



                    }

                }

                //if (mode == 2)
                //{

                //        dd1 = Convert.ToDouble(tapt.Rows[0]["f1"].ToString());

                //        txttcgstp.Text = dd1.ToString("0.00");
                //        if (txttdis.Text == "0")
                //        {
                //            dd2 = ((Convert.ToDouble(HFIT.Rows[i].Cells[4].Value) / 100) * Convert.ToDouble(tapt.Rows[0]["f1"].ToString())) / 2;
                //            dd8 = 0;
                //        }
                //        else
                //        {
                //            dd8 = ((Convert.ToDouble(HFIT.Rows[i].Cells[4].Value) / 100) * Convert.ToDouble(txttdis.Text));

                //            dd2 = (((Convert.ToDouble(HFIT.Rows[i].Cells[4].Value) - dd8) / 100) * Convert.ToDouble(tapt.Rows[0]["f1"].ToString()));
                //        }
                //        txttcgval.Text = dd2.ToString("0.00");
                //        if (txtstid.Text == "24")
                //        {
                //            qur.CommandText = "insert into Stransactionsplist values(" + Genclass.Dtype + "," + txtgrnid.Text + "," + HFIT.Rows[i].Cells[5].Value + "," + HFIT.Rows[i].Cells[3].Value + " ," + HFIT.Rows[i].Cells[2].Value + "," + HFIT.Rows[i].Cells[4].Value + "," + txtexcise.Text + ",0," + txttdis.Text + "," + dd8 + ",0, 0 ,0, 0, " + txttcgstp.Text + ", " + txttcgval.Text + "," + mode + "," + Txttot.Text + ",'" + HFIT.Rows[i].Cells[16].Value + "')";
                //            qur.ExecuteNonQuery();
                //        }
                //        else
                //        {

                //             dd8 = ((Convert.ToDouble(HFIT.Rows[i].Cells[4].Value) / 100) * Convert.ToDouble(txttdis.Text));

                //            dd2 = (((Convert.ToDouble(HFIT.Rows[i].Cells[4].Value) - dd8) / 100) * Convert.ToDouble(tapt.Rows[0]["f1"].ToString()));
                //            txttcgstp.Text = tapt.Rows[0]["f1"].ToString();
                //            txttcgval.Text = dd2.ToString("0.00");
                //            qur.CommandText = "insert into Stransactionsplist values(" + Genclass.Dtype + "," + txtgrnid.Text + "," + HFIT.Rows[i].Cells[5].Value + "," + HFIT.Rows[i].Cells[3].Value + " ," + HFIT.Rows[i].Cells[2].Value + "," + HFIT.Rows[i].Cells[4].Value + "," + txtexcise.Text + ",0," + txttdis.Text + "," + dd8 + ",0, 0 ,0, 0, " + txttcgstp.Text + ", " + txttcgval.Text + "," + mode + "," + Txttot.Text + ",'" + HFIT.Rows[i].Cells[16].Value + "')";
                //            qur.ExecuteNonQuery();
                //        }

                //    }

                //}


                if (mode == 2)
                {



                    qur.CommandText = "update Stransactionsp set DcNo='" + txtdcno.Text + "',dcdate='" + Dtpdt.Value + "',PartyUid=" + txtpuid.Text + ",remarks='" + txtrem.Text + "',active=1,companyid=" + Genclass.data1 + ",netvalue=" + TxtNetAmt.Text + ",placeuid=" + txtpluid.Text + ",transp='" + txttrans.Text + "',vehno='" + txtveh.Text + "',dtpre='" + Dtppre.Value + "',dtrem='" + Dtprem.Text + "',roff=" + TxtRoff.Text + " where UId=" + uid + "";
                    qur.ExecuteNonQuery();


                    string quyq = "select isnull(c.sname,0) as f1,z.generalname as uom from itemm b  inner join generalm z on b.uom_uid=z.uid inner join itemgroup c on b.itemgroup_uid=c.uid where b.uid=" + HFIT.Rows[i].Cells[5].Value + "";

                    Genclass.cmd = new SqlCommand(quyq, conn);
                    SqlDataAdapter aptrq = new SqlDataAdapter(Genclass.cmd);
                    DataTable tapq = new DataTable();
                    aptrq.Fill(tapq);

                    if (tapq.Rows.Count > 0)
                    {
                        if (txtstid.Text == "24")
                        {
                            dd1 = Convert.ToDouble(tapq.Rows[0]["f1"].ToString()) / 2;

                            txttcgstp.Text = dd1.ToString("0.00");
                            if (txttdis.Text == "0")
                            {
                                dd2 = ((Convert.ToDouble(HFIT.Rows[i].Cells[4].Value) / 100) * Convert.ToDouble(tapq.Rows[0]["f1"].ToString())) / 2;
                                dd8 = 0;
                            }
                            else
                            {
                                dd8 = ((Convert.ToDouble(HFIT.Rows[i].Cells[4].Value) / 100) * Convert.ToDouble(txttdis.Text));

                                dd2 = (((Convert.ToDouble(HFIT.Rows[i].Cells[4].Value) - dd8) / 100) * Convert.ToDouble(tapq.Rows[0]["f1"].ToString()) / 2);
                            }
                            txttcgval.Text = dd2.ToString("0.00");
                            double dd9 = Convert.ToDouble(HFIT.Rows[i].Cells[4].Value) - dd8;
                            txtbval.Text = dd9.ToString("0.00");
                            txtexcise.Text = txtexcise.Text.Replace(",", "");
                            qur.CommandText = "insert into Stransactionsplist values(" + Genclass.Dtype + "," + txtgrnid.Text + "," + HFIT.Rows[i].Cells[5].Value + " ," + HFIT.Rows[i].Cells[3].Value + " ," + HFIT.Rows[i].Cells[2].Value + "   ,   " + HFIT.Rows[i].Cells[4].Value + ",  " + txtbval.Text + " , 0," + txttdis.Text + "   ,   " + txttdisc.Text + "," + txttcgstp.Text + ", " + txttcgval.Text + ", " + txttcgstp.Text + ", " + txttcgval.Text + ",  0,0," + mode + "," + Txttot.Text + ",'" + HFIT.Rows[i].Cells[16].Value + "',0,0,0)";
                            qur.ExecuteNonQuery();
                            txtbval.Text = "";
                        }
                        else
                        {
                            dd1 = Convert.ToDouble(tapq.Rows[0]["f1"].ToString());

                            txttcgstp.Text = dd1.ToString("0.00");

                            dd8 = ((Convert.ToDouble(HFIT.Rows[i].Cells[4].Value) / 100) * Convert.ToDouble(txttdis.Text));

                            dd2 = (((Convert.ToDouble(HFIT.Rows[i].Cells[4].Value) - dd8) / 100) * Convert.ToDouble(tapq.Rows[0]["f1"].ToString()));
                            txttcgval.Text = dd2.ToString("0.00");
                            double dd9 = Convert.ToDouble(HFIT.Rows[i].Cells[4].Value) - dd8;
                            txtbval.Text = dd9.ToString("0.00");
                            txtexcise.Text = txtexcise.Text.Replace(",", "");
                            qur.CommandText = "insert into Stransactionsplist values(" + Genclass.Dtype + "," + txtgrnid.Text + "," + HFIT.Rows[i].Cells[5].Value + " ," + HFIT.Rows[i].Cells[3].Value + " ," + HFIT.Rows[i].Cells[2].Value + "   ,   " + HFIT.Rows[i].Cells[4].Value + ",  " + txtbval.Text + " , 0," + txttdis.Text + "   ,   " + txttdisc.Text + "    ,0, 0 ,0, 0, " + txttcgstp.Text + ", " + txttcgval.Text + "," + mode + "," + Txttot.Text + ",'" + HFIT.Rows[i].Cells[16].Value + "',0,0,0)";
                            qur.ExecuteNonQuery();
                            txtbval.Text = "";


                        }
                    }
                }
            }

            //qur.CommandText = "delete from  STransactionsPterms where stransactionspuid=" + txtgrnid.Text + "";
            //qur.ExecuteNonQuery();
            qur.CommandText = "delete from  Stransactionspcharges where stransactionspuid=" + txtgrnid.Text + "";
            qur.ExecuteNonQuery();

            if (txttdis.Text == "" || txttdis.Text == null)
            {
                txttdis.Text = "0";
                txttdisc.Text = "0";
            }

            //for (int j = 0; j < HFGT.Rows.Count - 1; j++)
            //{

            //    qur.CommandText = "insert into STransactionsPterms values (" + txtgrnid.Text + "," + Genclass.Dtype + ",'" + HFGT.Rows[j].Cells[0].Value + "','" + HFGT.Rows[j].Cells[1].Value + "','0'," + Genclass.Yearid + ")";
            //    qur.ExecuteNonQuery();
            //}



            if (txtcharges.Text != "")
            {
                qur.CommandText = "insert into Stransactionspcharges  values ( " + txtgrnid.Text + "," + Genclass.Dtype + ",13," + txtcharges.Text + "," + txtcharges.Text + ",'0'," + Genclass.Yearid + ")";
                qur.ExecuteNonQuery();
            }

            if (mode == 1)
            {
                qur.CommandText = "update doctypem set lastno= lastno + 1 where doctypeid=" + Genclass.Dtype + " and companyid=" + Genclass.data1 + "";
                qur.ExecuteNonQuery();
            }



            MessageBox.Show("Record has been saved", "Save", MessageBoxButtons.OK);




            conn.Close();
            Loadgrid();
            addipan.Visible = false;
            Editpan.Visible = false;
            Taxpan.Visible = false;

            panadd.Visible = true;
            Genpan.Visible = true;
            Genclass.sum5 = 0;
            j = -1;


        }


        private void cbotax_Click(object sender, System.EventArgs e)
        {
            //if (cbotax.SelectedValue != null)
            //{
            //    int ed;
            //    int Cess;
            //    int HECess;
            //    int excise;
            //    int lbled;
            //    int tax;
            //    int vat;

            //    lbled = Convert.ToInt16(LblED1.Text);
            //    ed = (Convert.ToInt16(Genclass.strfin) * lbled) / 100;
            //    txtted.Text = Convert.ToString(ed);
            //    Cess = ed * Convert.ToInt16(LblCess.Text) / 100;
            //    txtcess.Text = Convert.ToString(Cess);
            //    HECess = ed * Convert.ToInt16(LblHECess.Text) / 100;
            //    txthecess.Text = Convert.ToString(HECess);
            //    excise = ed + Cess + HECess;
            //    txtexcise.Text = Convert.ToString(excise);
            //    CalcNetAmt();
            //    Taxduty();
            //    tax = Convert.ToInt16(lbltax.Text);
            //    vat = Convert.ToInt16(TxtNetAmt.Text) * tax / 100;
            //    txttax.Text = Convert.ToString(vat);
            CalcNetAmt();
            //}
        }


        private void txtdcno_TextChanged(object sender, EventArgs e)
        {


            //if (Genclass.Dtype == 40)
            //{

            //    if (txtdcno.Text == "")
            //    {
            //        return;

            //    }
            //    else
            //    {
            //        Genclass.strsql = "select Itemname,UoM,itemuid,refid,qty,PRICE,convert(decimal(18,2),BasicValue,105) AS BasicValue,disper,convert(decimal(18,2),Disvalue,105) AS Disvalue,convert(decimal(18,2),Taxablevalue,105) AS Taxablevalue,convert(decimal(18,2),Cgstval,105) AS EDVAL,convert(decimal(18,2),sgstval,105) AS VATVAL,convert(decimal(18,2),igstval,105) AS igstval,CG as ED,sg as VAT,ig, convert(decimal(18,2),(BasicValue-(isnull(Disvalue,0))+ (isnull(Cgstval,0)+isnull(sgstval,0))),105) as total from (select distinct   c.itemname,d.generalname as uom,b.itemuid,b.uid as refid,b.pqty-isnull(sum(e.pqty),0) as qty,f.Price ,(b.pqty-isnull(sum(e.pqty),0)) * f.Price as BasicValue,disper,((b.pqty-isnull(sum(e.pqty),0)) * f.Price/100)* disper as Disvalue,((b.pqty-isnull(sum(e.pqty),0)) * f.Price)-(((b.pqty-isnull(sum(e.pqty),0)) * f.Price/100)* disper) as Taxablevalue,g.f1 as CG,(((b.pqty-isnull(sum(e.pqty),0)) * f.Price)-(((b.pqty-isnull(sum(e.pqty),0)) * f.Price/100)* disper))/100 * g.f1 as Cgstval,h.f1 as SG,(((b.pqty-isnull(sum(e.pqty),0)) * f.Price)-(((b.pqty-isnull(sum(e.pqty),0)) * f.Price/100)* disper))/100 * h.f1 as sgstval,i.f1 as ig,(((b.pqty-isnull(sum(e.pqty),0)) * f.Price)-(((b.pqty-isnull(sum(e.pqty),0)) * f.Price/100)* disper))/100 * i.f1 as igstval from transactionsp a inner join transactionsplist b on  a.uid=b.transactionspuid and a.doctypeid=30 and a.companyid=1 inner join itemm c on b.itemuid=c.uid left join generalm d on c.uom_uid=d.uid  left join transactionsplist e on  b.uid=e.refuid and e.doctypeid=40 inner join pur_price_list f on c.uid=f.itemuid and c.partyuid=f.suppuid inner join generalm g on f.cgstid=g.uid inner join generalm h on f.sgstid=h.uid inner join generalm i on f.igstid=i.uid where a.docno='0001/17-18' group by c.itemname,d.generalname,b.itemuid,b.uid,b.pqty,f.price,disper,g.f1,h.f1,i.f1 having b.pqty-isnull(sum(e.pqty),0) >0) tab";

            //        Genclass.cmd = new SqlCommand(Genclass.strsql, conn);
            //        SqlDataAdapter aptr1 = new SqlDataAdapter(Genclass.cmd);
            //        DataTable tap1 = new DataTable();
            //        aptr1.Fill(tap1);
            //        //txtname.Text = tap1.Rows[0]["Name"].ToString();
            //        //txtpuid.Text = tap1.Rows[0]["Partyuid"].ToString();
            //        for (int i = 0; i < tap1.Rows.Count; i++)
            //        {
            //            var index = HFIT.Rows.Add();
            //            HFIT.Rows[index].Cells[0].Value = tap1.Rows[i]["itemname"].ToString();
            //            HFIT.Rows[index].Cells[1].Value = tap1.Rows[i]["UOM"].ToString();
            //            HFIT.Rows[index].Cells[2].Value = tap1.Rows[i]["Price"].ToString();
            //            HFIT.Rows[index].Cells[3].Value = tap1.Rows[i]["qty"].ToString();
            //            HFIT.Rows[index].Cells[4].Value = tap1.Rows[i]["BasicValue"].ToString();
            //            HFIT.Rows[index].Cells[5].Value = tap1.Rows[i]["itemuid"].ToString();
            //            HFIT.Rows[index].Cells[6].Value = tap1.Rows[i]["refid"].ToString();
            //            HFIT.Rows[index].Cells[7].Value = tap1.Rows[i]["disper"].ToString();
            //            HFIT.Rows[index].Cells[8].Value = tap1.Rows[i]["Disvalue"].ToString();
            //            HFIT.Rows[index].Cells[9].Value = tap1.Rows[i]["Taxablevalue"].ToString();
            //            HFIT.Rows[index].Cells[10].Value = tap1.Rows[i]["ED"].ToString();
            //            HFIT.Rows[index].Cells[11].Value = tap1.Rows[i]["EDVAL"].ToString();
            //            HFIT.Rows[index].Cells[12].Value = tap1.Rows[i]["VAT"].ToString();
            //            HFIT.Rows[index].Cells[13].Value = tap1.Rows[i]["VATVAL"].ToString();
            //            HFIT.Rows[index].Cells[14].Value = tap1.Rows[i]["Total"].ToString();
            //            HFIT.Rows[index].Cells[15].Value = index;

            //            Genclass.sum1 = Genclass.sum1 + Convert.ToDouble(HFIT.Rows[i].Cells[14].Value);
            //            Txttot.Text = Genclass.sum1.ToString();




            //            //Titlep();



            //        }

            //    }


            //}
            //else
            //{
            //    txtitemname.Focus();

            //}
        }



        private void button4_Click(object sender, EventArgs e)
        {
            Editpan.Visible = false;
            Genpan.Visible = true;
        }

        private void HFIT_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            //int i = HFIT.SelectedCells[0].RowIndex;
            //HFIT.Rows[i].Cells[4].Value = Convert.ToDouble(HFIT.Rows[i].Cells[2].Value) * Convert.ToDouble(HFIT.Rows[i].Cells[3].Value);
        }

        private void button6_Click(object sender, EventArgs e)
        {

        }

        private void button8_Click(object sender, EventArgs e)
        {
            Editpan.Visible = false;
            Taxpan.Visible = true;
            txtexcise.Text = Txttot.Text;
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void button9_Click(object sender, EventArgs e)
        {
            //termspan.Visible = false;
            addipan.Visible = true;
            button9.Visible = false;
            button10.Visible = true;

        }

        private void button10_Click(object sender, EventArgs e)
        {

        }

        private void txtexcise_TextChanged(object sender, EventArgs e)
        {

        }

        private void Taxpan_Paint(object sender, PaintEventArgs e)
        {

        }

        private void button10_Click_1(object sender, EventArgs e)
        {

        }

        //private void panel1_Paint(object sender, PaintEventArgs e)
        //{

        //}
        private void TitleAdd()
        {
            HFGA.ColumnCount = 3;
            HFGA.Columns[0].Name = "Additional Charge";
            HFGA.Columns[1].Name = "Amount";
            HFGA.Columns[2].Name = "uid";

            HFGA.Columns[0].Width = 334;
            HFGA.Columns[1].Width = 90;
            HFGA.Columns[2].Visible = false;
        }

        private void Titleterm()
        {
            HFGT.ColumnCount = 3;
            HFGT.Columns[0].Name = "Terms";
            HFGT.Columns[1].Name = "Term Description";
            HFGT.Columns[2].Name = "uid";

            HFGT.Columns[0].Width = 300;
            HFGT.Columns[1].Width = 310;
            HFGT.Columns[2].Visible = false;



        }

        private void txtterms_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                Genclass.fieldone = "";
                Genclass.fieldtwo = "";
                Genclass.fieldthree = "";
                Genclass.fieldFour = "";
                Genclass.fieldFive = "";
                Genclass.type = 4;
                loadput();
            }
        }

        private void txtterms_TextChanged(object sender, EventArgs e)
        {

        }

        private void button11_Click(object sender, EventArgs e)
        {

            HFGT.AllowUserToAddRows = true;
            HFGT.AutoGenerateColumns = true;
            var index = HFGT.Rows.Add();
            HFGT.Rows[index].Cells[0].Value = txtterms.Text;
            HFGT.Rows[index].Cells[1].Value = txtremde.Text;
            HFGT.Rows[index].Cells[2].Value = txttermid.Text;

        }

        private void button12_Click(object sender, EventArgs e)
        {

        }

        private void button10_Click_2(object sender, EventArgs e)
        {
            addipan.Visible = false;
            termspan.Visible = true;
            button9.Visible = true;
            button10.Visible = false;
        }

        private void txtaddcharge_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtaddcharge_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                Genclass.fieldone = "";
                Genclass.fieldtwo = "";
                Genclass.fieldthree = "";
                Genclass.fieldFour = "";
                Genclass.fieldFive = "";
                Genclass.type = 3;
                loadput();
            }
        }

        private void txttotaddd_TextChanged(object sender, EventArgs e)
        {
            if (txttotaddd.Text == "0" || txttotaddd.Text == "")
            {
                TxtNetAmt.Text = txtexcise.Text;
            }
            else
            {
                TxtNetAmt.Text = "0";
                double val2 = Convert.ToDouble(txtexcise.Text) + Convert.ToDouble(txttotaddd.Text);
                TxtNetAmt.Text = val2.ToString();

            }
        }

        private void btnedit_Click(object sender, EventArgs e)
        {
            buttnfinbk.Visible = true;
            HFIT.AutoGenerateColumns = false;
            HFIT.Refresh();
            HFIT.DataSource = null;
            HFIT.Rows.Clear();
            HFGA.AutoGenerateColumns = false;
            HFGA.Refresh();
            HFGA.DataSource = null;
            HFGA.Rows.Clear();
            HFGT.AutoGenerateColumns = false;
            HFGT.Refresh();
            HFGT.DataSource = null;
            HFGT.Rows.Clear();
            HFGST.Refresh();
            HFGST.DataSource = null;
            HFGST.Rows.Clear();
            mode = 2;
            dis3 = 0;
            dis4 = 0;
            str1key = "F2";
            txttdis.Text = "0";
            txtexcise.Text = "0";
            txtigval.Text = "0";
            HFIT.Columns[12].ReadOnly = true;
            HFIT.Columns[13].ReadOnly = true;
            Genpan.Visible = false;
            Editpan.Visible = true;
            lkppnl.Visible = false;
            int i = HFGP.SelectedCells[0].RowIndex;
            uid = HFGP.Rows[i].Cells[0].Value.ToString();
            txtgrnid.Text = HFGP.Rows[i].Cells[0].Value.ToString();
            txtgrn.Text = HFGP.Rows[i].Cells[1].Value.ToString();
            DTPDOCDT.Text = HFGP.Rows[i].Cells[2].Value.ToString();
            txtdcno.Text = HFGP.Rows[i].Cells[3].Value.ToString();
            Dtpdt.Text = HFGP.Rows[i].Cells[4].Value.ToString();
            txtname.Text = HFGP.Rows[i].Cells[5].Value.ToString();
            txtpuid.Text = HFGP.Rows[i].Cells[7].Value.ToString();
            txtveh.Text = HFGP.Rows[i].Cells[13].Value.ToString();
            txtrem.Text = HFGP.Rows[i].Cells[14].Value.ToString();
            conn.Close();
            conn.Open();
            {
                Genclass.strsql = " select b.uid,c.ItemName,d.GeneralName as uom,PRate,pqty,BasicValue,ItemUId,b.refuid,disp,disval,Taxableval,Cgstid,Cgstval,Sgstid,Sgstval,igstid,igstval,totvalue from stransactionsp a inner join Stransactionsplist b on a.uid=b.transactionspuid  left join itemm c  on b.itemuid=c.uid left join generalm d on c.UOM_UId=d.Uid  where b.transactionspuid=" + txtgrnid.Text + " ";

                Genclass.cmd = new SqlCommand(Genclass.strsql, conn);
                SqlDataAdapter aptr1 = new SqlDataAdapter(Genclass.cmd);
                DataTable tap1 = new DataTable();
                aptr1.Fill(tap1);
                //txtname.Text = tap1.Rows[0]["Name"].ToString();
                //txtpuid.Text = tap1.Rows[0]["Partyuid"].ToString();
                Genclass.sum1 = 0;
                Genclass.sum5 = 0;


                Txttot.Text = "";
                for (int k = 0; k < tap1.Rows.Count; k++)
                {
                    var index = HFIT.Rows.Add();
                    HFIT.Rows[index].Cells[0].Value = tap1.Rows[k]["itemname"].ToString();
                    HFIT.Rows[index].Cells[1].Value = tap1.Rows[k]["UOM"].ToString();
                    HFIT.Rows[index].Cells[2].Value = tap1.Rows[k]["PRate"].ToString();
                    HFIT.Rows[index].Cells[3].Value = tap1.Rows[k]["pqty"].ToString();
                    double sumb = Convert.ToDouble(tap1.Rows[k]["BasicValue"].ToString());
                    HFIT.Rows[index].Cells[4].Value = sumb.ToString("0.00");
                    txtbval.Text = sumb.ToString("0.00");
                    HFIT.Rows[index].Cells[5].Value = tap1.Rows[k]["itemuid"].ToString();
                    HFIT.Rows[index].Cells[6].Value = tap1.Rows[k]["refuid"].ToString();
                    HFIT.Rows[index].Cells[7].Value = tap1.Rows[k]["disp"].ToString();
                    HFIT.Rows[index].Cells[8].Value = tap1.Rows[k]["disval"].ToString();
                    HFIT.Rows[index].Cells[9].Value = tap1.Rows[k]["Taxableval"].ToString();
                    if (txtstid.Text == "24")
                    {
                        double uy = Convert.ToDouble(tap1.Rows[k]["Sgstid"].ToString()) * 2;
                        HFIT.Rows[index].Cells[10].Value = uy.ToString();
                    }
                    else
                    {
                        HFIT.Rows[index].Cells[10].Value = tap1.Rows[k]["igstid"].ToString();
                    }

                    HFIT.Rows[index].Cells[11].Value = tap1.Rows[k]["igstval"].ToString();
                    HFIT.Rows[index].Cells[12].Value = tap1.Rows[k]["Sgstid"].ToString();
                    HFIT.Rows[index].Cells[13].Value = tap1.Rows[k]["Sgstval"].ToString();
                    HFIT.Rows[index].Cells[14].Value = tap1.Rows[k]["totvalue"].ToString();
                    HFIT.Rows[index].Cells[15].Value = tap1.Rows[k]["uid"].ToString();
                    if (txtstid.Text == "24")
                    {
                        dis4 = Convert.ToDouble(tap1.Rows[k]["Sgstid"].ToString()) * 2;
                        dis3 = Convert.ToDouble(tap1.Rows[k]["Sgstval"].ToString()) * 2;
                    }
                    else
                    {
                        dis4 = Convert.ToDouble(tap1.Rows[k]["igstid"].ToString());
                        dis3 = Convert.ToDouble(tap1.Rows[k]["igstval"].ToString());
                    }

                    Genclass.sum1 = Genclass.sum1 + Convert.ToDouble(HFIT.Rows[index].Cells[4].Value.ToString());

                }

                Txttot.Text = Genclass.sum1.ToString();

                Titlep();

                btnaddrcan.Visible = false;


                Genclass.strsql = "select * from Stransactionspcharges a left join generalm b on a.chargesuid=b.uid  where a.stransactionspuid=" + txtgrnid.Text + " ";

                Genclass.cmd = new SqlCommand(Genclass.strsql, conn);
                SqlDataAdapter aptr2 = new SqlDataAdapter(Genclass.cmd);
                DataTable tap2 = new DataTable();
                aptr2.Fill(tap2);
                if (tap2.Rows.Count > 0)
                {
                    txtcharges.Text = tap2.Rows[0]["chargeamount"].ToString();
                }

                if (tap1.Rows.Count > 0)
                {
                    txttbval.Text = Txttot.Text;
                    txttdis.Text = tap1.Rows[0]["disp"].ToString();
                    //txttdisc.Text = tap1.Rows[0]["disval"].ToString();

                }
                else
                {
                    txttdis_TextChanged_1(sender, e);
                }
            }
        }




        private void txtscr1_TextChanged(object sender, EventArgs e)
        {
            Loadgrid();
        }

        private void Txtscr2_TextChanged(object sender, EventArgs e)
        {
            Loadgrid();
        }

        private void Txtscr3_TextChanged(object sender, EventArgs e)
        {
            Loadgrid();
        }

        private void txtscr4_TextChanged(object sender, EventArgs e)
        {
            Loadgrid();
        }

        private void txtscr5_TextChanged(object sender, EventArgs e)
        {
            Loadgrid();
        }

        private void HFGP_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void chkact_CheckedChanged(object sender, EventArgs e)
        {
            Loadgrid();
        }

        private void button6_Click_1(object sender, EventArgs e)
        {
            mode = 1;
            Genpan.Visible = false;

            Genclass.Module.ClearTextBox(this, Editpan);

            Genclass.Module.Gendocno();
            txtgrn.Text = Genclass.ST;
            label15.Visible = true;
            txtitemname.Visible = true;
            label14.Visible = true;
            txtprice.Visible = true;
            label31.Visible = true;
            txtqty.Visible = true;
            label32.Visible = true;
            txtbval.Visible = true;
            buttcusok.Visible = true;
            //}
            Editpan.Visible = true;
            btnsave.Visible = true;
            btnaddrcan.Visible = false;
            buttnnxt.Visible = false;
            buttnfinbk.Visible = true;
            button11.Visible = true;
            button12.Visible = true;
            DTPDOCDT.Focus();
            HFIT.Refresh();
            HFIT.DataSource = null;
            HFIT.Rows.Clear();
            HFGST.Refresh();
            HFGST.DataSource = null;
            HFGST.Rows.Clear();
            Genclass.sum = 1;
            lkppnl.Visible = false;
            //HFIT.Columns[0].Width = 300;
            //HFIT.Columns[1].Visible = false;
            //HFIT.Columns[2].Width = 60;
            //HFIT.Columns[3].Width = 60;
            //HFIT.Columns[4].Width = 90;

            //HFIT.Columns[5].Visible = false;
            //HFIT.Columns[6].Visible = false;
            //HFIT.Columns[9].Visible = false;

            //HFIT.Columns[7].Width = 40;
            //HFIT.Columns[8].Width = 60;

            //HFIT.Columns[10].Width = 40;
            //HFIT.Columns[11].Width = 60;
            //HFIT.Columns[12].Width = 43;
            //HFIT.Columns[13].Width = 60;
            //HFIT.Columns[14].Width = 92;
            panadd.Visible = false;
            Titlep();
            str1key = "F2";

        }

        private void butedit_Click(object sender, EventArgs e)
        {

            mode = 2;
            panadd.Visible = false;
            Genpan.Visible = false;
            Taxpan.Visible = false;
            Editpan.Visible = true;
            //button11.Visible = true;
            //button12.Visible = true;

            label15.Visible = true;
            txtitemname.Visible = true;
            label14.Visible = true;
            txtprice.Visible = true;
            label31.Visible = true;
            txtqty.Visible = true;
            label32.Visible = true;
            txtbval.Visible = true;
            //buttcusok.Visible = true;
            buttnfinbk.Visible = true;
            btnsave.Visible = true;

            int i = HFGP.SelectedCells[0].RowIndex;
            uid = HFGP.Rows[i].Cells[0].Value.ToString();
            txtgrnid.Text = HFGP.Rows[i].Cells[0].Value.ToString();
            txtgrn.Text = HFGP.Rows[i].Cells[1].Value.ToString();
            DTPDOCDT.Text = HFGP.Rows[i].Cells[2].Value.ToString();
            txtdcno.Text = HFGP.Rows[i].Cells[3].Value.ToString();
            Dtpdt.Text = HFGP.Rows[i].Cells[4].Value.ToString();
            txtname.Text = HFGP.Rows[i].Cells[5].Value.ToString();
            txtpuid.Text = HFGP.Rows[i].Cells[7].Value.ToString();
            txtpluid.Text = HFGP.Rows[i].Cells[8].Value.ToString();
            txtplace.Text = HFGP.Rows[i].Cells[9].Value.ToString();
            Dtppre.Text = HFGP.Rows[i].Cells[10].Value.ToString();
            Dtprem.Text = HFGP.Rows[i].Cells[11].Value.ToString();
            txttrans.Text = HFGP.Rows[i].Cells[12].Value.ToString();
            txtveh.Text = HFGP.Rows[i].Cells[13].Value.ToString();
            txtrem.Text = HFGP.Rows[i].Cells[14].Value.ToString();

            conn.Open();
            //HFIT.AutoGenerateColumns = false;

            HFIT.Refresh();
            HFIT.DataSource = null;
            HFIT.Rows.Clear();

            //HFGA.AutoGenerateColumns = false;
            HFGA.Refresh();
            HFGA.DataSource = null;
            HFGA.Rows.Clear();
            //HFGT.AutoGenerateColumns = false;
            HFGT.Refresh();
            HFGT.DataSource = null;
            HFGT.Rows.Clear();


            {
                Genclass.strsql = " select distinct b.uid,c.ItemName,d.GeneralName as uom,PRate,pqty,BasicValue,ItemUId,b.refuid,disp,disval,Taxableval,igstid,igstval,totvalue,addnotes,isnull(chargeamount,0) as chargeamount from stransactionsp a inner join Stransactionsplist b on a.uid=b.transactionspuid  left join itemm c  on b.itemuid=c.uid left join generalm d on c.UOM_UId=d.Uid left join sTransactionsPcharges e on a.uid=e.stransactionspuid where b.transactionspuid=" + uid + " and b.doctypeid=90 ";

                Genclass.cmd = new SqlCommand(Genclass.strsql, conn);
                SqlDataAdapter aptr1 = new SqlDataAdapter(Genclass.cmd);
                DataTable tap1 = new DataTable();
                aptr1.Fill(tap1);
                //txtname.Text = tap1.Rows[0]["Name"].ToString();
                //txtpuid.Text = tap1.Rows[0]["Partyuid"].ToString();
                Genclass.sum1 = 0;
                Txttot.Text = "";
                for (int k = 0; k < tap1.Rows.Count; k++)
                {
                    var index = HFIT.Rows.Add();
                    HFIT.Rows[index].Cells[0].Value = tap1.Rows[k]["itemname"].ToString();
                    HFIT.Rows[index].Cells[1].Value = tap1.Rows[k]["UOM"].ToString();
                    HFIT.Rows[index].Cells[2].Value = tap1.Rows[k]["PRate"].ToString();
                    HFIT.Rows[index].Cells[3].Value = tap1.Rows[k]["pqty"].ToString();
                    HFIT.Rows[index].Cells[4].Value = tap1.Rows[k]["BasicValue"].ToString();
                    HFIT.Rows[index].Cells[5].Value = tap1.Rows[k]["itemuid"].ToString();
                    HFIT.Rows[index].Cells[6].Value = tap1.Rows[k]["refuid"].ToString();
                    //HFIT.Rows[index].Cells[7].Value = tap1.Rows[k]["disp"].ToString();
                    //HFIT.Rows[index].Cells[8].Value = tap1.Rows[k]["disval"].ToString();
                    //HFIT.Rows[index].Cells[9].Value = tap1.Rows[k]["Taxableval"].ToString();
                    HFIT.Rows[index].Cells[10].Value = tap1.Rows[k]["igstid"].ToString();
                    HFIT.Rows[index].Cells[11].Value = tap1.Rows[k]["igstval"].ToString();
                    //HFIT.Rows[index].Cells[12].Value = tap1.Rows[k]["Sgstid"].ToString();
                    //HFIT.Rows[index].Cells[13].Value = tap1.Rows[k]["Sgstval"].ToString();
                    HFIT.Rows[index].Cells[14].Value = tap1.Rows[k]["totvalue"].ToString();
                    HFIT.Rows[index].Cells[15].Value = tap1.Rows[k]["uid"].ToString();
                    HFIT.Rows[index].Cells[16].Value = tap1.Rows[k]["addnotes"].ToString();

                    Genclass.sum1 = Genclass.sum1 + Convert.ToDouble(HFIT.Rows[k].Cells[4].Value);
                    Txttot.Text = Genclass.sum1.ToString();
                    //txtexcise.Text = Genclass.sum1.ToString();
                }

                txttbval.Text = Txttot.Text;
                if (tap1.Rows.Count > 0)
                {
                    txttdis.Text = tap1.Rows[0]["disp"].ToString();
                    txttdisc.Text = tap1.Rows[0]["disval"].ToString();
                    txtcharges.Text = tap1.Rows[0]["chargeamount"].ToString();
                    //splittax();
                }
                Titlep();
                //btnsave.Visible = false;
                btnaddrcan.Visible = false;

                conn.Close();
            }
            conn.Open();
            {
                Genclass.strsql = "select * from Stransactionspcharges a left join generalm b on a.chargesuid=b.uid  where a.stransactionspuid=" + txtgrnid.Text + " ";

                Genclass.cmd = new SqlCommand(Genclass.strsql, conn);
                SqlDataAdapter aptr2 = new SqlDataAdapter(Genclass.cmd);
                DataTable tap2 = new DataTable();
                aptr2.Fill(tap2);
                if (tap2.Rows.Count > 0)
                {
                    txtcharges.Text = tap2.Rows[0]["chargeamount"].ToString();
                }
                //Genclass.sum1 = 0;
                //for (int j = 0; j < tap2.Rows.Count; j++)
                //{
                //    var index = HFGA.Rows.Add();
                //    HFGA.Rows[index].Cells[0].Value = tap2.Rows[j]["generalname"].ToString();
                //    HFGA.Rows[index].Cells[1].Value = tap2.Rows[j]["chargeamount"].ToString();
                //    HFGA.Rows[index].Cells[2].Value = tap2.Rows[j]["chargesuid"].ToString();


                //    Genclass.sum1 = Genclass.sum1 + Convert.ToDouble(HFGA.Rows[j].Cells[1].Value);
                //    txttotaddd.Text = Genclass.sum1.ToString();
                //}
                conn.Close();
            }
            conn.Open();
            {
                Genclass.strsql = "select * from STransactionsPterms   where stransactionspuid=" + txtgrnid.Text + " ";

                Genclass.cmd = new SqlCommand(Genclass.strsql, conn);
                SqlDataAdapter aptr3 = new SqlDataAdapter(Genclass.cmd);
                DataTable tap3 = new DataTable();
                aptr3.Fill(tap3);

                for (int g = 0; g < tap3.Rows.Count; g++)
                {
                    var index = HFGT.Rows.Add();
                    HFGT.Rows[index].Cells[0].Value = tap3.Rows[g]["termsuid"].ToString();
                    HFGT.Rows[index].Cells[1].Value = tap3.Rows[g]["termsdesc"].ToString();
                    //HFGT.Rows[index].Cells[2].Value = tap3.Rows[g]["termsuid"].ToString();


                }
                conn.Close();
                double val2 = Convert.ToDouble(txtexcise.Text) + Convert.ToDouble(txtcharges.Text);
                TxtNetAmt.Text = val2.ToString();
            }
        }

        private void buttnext1_Click(object sender, EventArgs e)
        {
            this.Dispose();
        }

        private void HFGP_CellMouseUp(object sender, DataGridViewCellMouseEventArgs e)
        {
            if (HFGP.Rows[0].Cells[0].Value.ToString() != "")
            {
                int index = HFGP.SelectedRows[0].Index;
                if (index < HFGP.Rows.Count - 1)
                {
                    index = index + 1;
                    int index1 = HFGP.Rows.Count - 1;
                    lblno1.Text = index.ToString();
                    lblno2.Text = "of " + index1.ToString();
                }
            }
        }

        private void HFGP_KeyUp(object sender, KeyEventArgs e)
        {
            if (HFGP.Rows[0].Cells[0].Value.ToString() != "")
            {
                int index = HFGP.SelectedRows[0].Index;
                if (index < HFGP.Rows.Count - 1)
                {
                    index = index + 1;
                    int index1 = HFGP.Rows.Count - 1;
                    lblno1.Text = index.ToString();
                    lblno2.Text = "of " + index1.ToString();
                }
            }
        }

        private void buttnnvfst_Click(object sender, EventArgs e)
        {
            int index = HFGP.SelectedRows[0].Index;
            int index1 = 0;
            int index2 = HFGP.Rows.Count - 1;

            HFGP.Rows[index].Selected = false;
            HFGP.Rows[index1].Selected = true;
            index1 = index1 + 1;
            lblno1.Text = index1.ToString();
            lblno2.Text = "of " + index2.ToString();
        }

        private void buttnnxtlft_Click(object sender, EventArgs e)
        {
            int index = HFGP.SelectedRows[0].Index;
            int index1 = HFGP.Rows.Count - 1; ;
            if (index > 0)
            {
                HFGP.Rows[index].Selected = false;
                HFGP.Rows[index - 1].Selected = true;
                lblno1.Text = index.ToString();
                lblno2.Text = "of " + index1.ToString();
            }
        }

        private void buttrnxt_Click(object sender, EventArgs e)
        {
            int index = HFGP.SelectedRows[0].Index;
            int index2 = HFGP.SelectedRows[0].Index + 2;
            int index1 = HFGP.Rows.Count - 1; ;
            if (index < HFGP.Rows.Count - 2)
            {
                HFGP.Rows[index].Selected = false;
                HFGP.Rows[index + 1].Selected = true;
                lblno1.Text = index2.ToString();
                lblno2.Text = "of " + index1.ToString();

            }
        }

        private void btnfinnxt_Click(object sender, EventArgs e)
        {
            int index = HFGP.SelectedRows[0].Index;
            int index1 = HFGP.Rows.Count - 1;


            HFGP.Rows[index].Selected = false;

            HFGP.Rows[index1 - 1].Selected = true;
            lblno1.Text = index1.ToString();
            lblno2.Text = "of " + index1.ToString();

        }

        private void txtscr6_TextChanged(object sender, EventArgs e)
        {
            Loadgrid();
        }

        private void btnadd_Click(object sender, EventArgs e)
        {

            for (int i = 0; i < HFGA.RowCount - 1; i++)
            {
                if (HFGA.Rows[i].Cells[2].Value.ToString() == txtaddid.Text)
                {
                    MessageBox.Show("Charge Already Added");
                    txtaddcharge.Text = "";
                    txtcharges.Text = "";
                    txtaddcharge.Focus();
                    return;
                }
            }

            HFGA.AllowUserToAddRows = true;
            HFGA.AutoGenerateColumns = true;
            var index = HFGA.Rows.Add();
            HFGA.Rows[index].Cells[0].Value = txtaddcharge.Text;
            HFGA.Rows[index].Cells[1].Value = txtcharges.Text;
            HFGA.Rows[index].Cells[2].Value = txtaddid.Text;




            Genclass.sum1 = Genclass.sum1 + Convert.ToDouble(txtcharges.Text);
            txttotaddd.Text = Genclass.sum1.ToString();
            txttotc.Text = Genclass.sum1.ToString();
            splittax();
            txtaddcharge.Text = "";
            txtcharges.Text = "";
            txtaddcharge.Focus();
        }



        private void button11_Click_2(object sender, EventArgs e)
        {

            if (Txttot.Text == "" || Txttot.Text == null)
            {
                MessageBox.Show("Enter the item to proceed");
                return;
            }
            Taxpan.Visible = true;
            btnsave.Visible = false;
            btnaddrcan.Visible = false;

            txtexcise.Text = Txttot.Text;
            //button9.Visible = false;
            addipan.Visible = false;
            //termspan.Visible = false;
            button11.Visible = false;
            button12.Visible = false;
            btnsave.Visible = true;
            buttnnxt.Visible = false;
            buttnfinbk.Visible = true;
            HFGT.Visible = false;
            label34.Visible = false;
            Editpan.Visible = true;
            txtexcise.Text = Txttot.Text;
            //Titletax();
            Genclass.sum1 = 0;
            conn.Open();
            //qur.CommandText = "delete from salestaxsplit ";
            //qur.ExecuteNonQuery();

            //for (int i = 0; i < HFIT.RowCount - 1; i++)
            //{
            //    qur.CommandText = "insert into salestaxsplit values(" + HFIT.Rows[i].Cells[15].Value + "," + HFIT.Rows[i].Cells[9].Value + "," + HFIT.Rows[i].Cells[10].Value + "," + HFIT.Rows[i].Cells[11].Value + ")";
            //    qur.ExecuteNonQuery();
            //}

            //Genclass.strsql = "select hsnid,SUM(taxableval) as taxableval,convert(decimal(18,2),gstper/2,105) as cgstper,convert(decimal(18,2),gstval/2,105) as cgstval,convert(decimal(18,2),gstper/2,105) as sgstper,convert(decimal(18,2),gstval/2,105) as sgstval from salestaxsplit group by hsnid,gstper,gstval";

            //Genclass.cmd = new SqlCommand(Genclass.strsql, conn);
            //SqlDataAdapter aptr1 = new SqlDataAdapter(Genclass.cmd);
            //DataTable tap1 = new DataTable();
            //aptr1.Fill(tap1);


            //for (int j = 0; j < tap1.Rows.Count; j++)
            //{
            //    var index = HFGTAX.Rows.Add();
            //    HFGTAX.Rows[index].Cells[0].Value = tap1.Rows[j]["taxableval"].ToString();
            //    HFGTAX.Rows[index].Cells[1].Value = tap1.Rows[j]["cgstper"].ToString();
            //    HFGTAX.Rows[index].Cells[2].Value = tap1.Rows[j]["cgstval"].ToString();
            //    HFGTAX.Rows[index].Cells[3].Value = tap1.Rows[j]["sgstper"].ToString();
            //    HFGTAX.Rows[index].Cells[4].Value = tap1.Rows[j]["Sgstval"].ToString();
            //    HFGTAX.Rows[index].Cells[5].Value = tap1.Rows[j]["hsnid"].ToString();
            //}



            //Titleterm();
            //if (HFGT.Rows[0].Cells[0].Value == "" || HFGT.Rows[0].Cells[0].Value == null)
            //{
            //    var index = HFGT.Rows.Add();
            //    HFGT.Rows[index].Cells[0].Value = "No.of Cases";

            //    var index1 = HFGT.Rows.Add();
            //    HFGT.Rows[index1].Cells[0].Value = "Box No/Size";
            //    var index2 = HFGT.Rows.Add();
            //    HFGT.Rows[index2].Cells[0].Value = "Gross Weight";
            //    var index3 = HFGT.Rows.Add();
            //    HFGT.Rows[index3].Cells[0].Value = "Carrier";
            //    var index4 = HFGT.Rows.Add();
            //    HFGT.Rows[index4].Cells[0].Value = "Destination";
            //    var index5 = HFGT.Rows.Add();
            //    HFGT.Rows[index5].Cells[0].Value = "LR/RR/RPPNO./DT";
            //}
            //HFGT.Columns[0].ReadOnly = true;

            ////Txttot.Text = "4352.86";
            txttbval.Text = Txttot.Text;

            splittax();


            conn.Close();
        }

        private void splittax()
        {
            if (txttdis.Text == "" || txttdis.Text == null || txttdis.Text == "0")
            {
                txttprdval.Text = txttbval.Text;

            }
            else
            {
                double dis6 = Convert.ToDouble(txttbval.Text) / 100 * Convert.ToDouble(txttdis.Text);
                txttdisc.Text = dis6.ToString("0.00");

                double dis4 = Convert.ToDouble(txttbval.Text) - Convert.ToDouble(txttdisc.Text);
                txttprdval.Text = dis4.ToString("#,0.00");
            }

            if (txtcharges.Text == "" || txtcharges.Text == null || txtcharges.Text == "0")
            {
                txtexcise.Text = txttprdval.Text;
            }
            else
            {
                double dis4 = Convert.ToDouble(txttprdval.Text) + Convert.ToDouble(txtcharges.Text);
                txtexcise.Text = dis4.ToString("#,0.00");
            }




            Genclass.strsql = "select generalname from partym a inner join generalm b on a.stateuid=b.uid where a.companyid=" + Genclass.data1 + " and a.uid=" + txtpuid.Text + "";

            Genclass.cmd = new SqlCommand(Genclass.strsql, conn);
            SqlDataAdapter aptr1 = new SqlDataAdapter(Genclass.cmd);
            DataTable tap1 = new DataTable();
            aptr1.Fill(tap1);

            //MessageBox.Show(tap1.Rows[0]["generalname"].ToString());
            if (tap1.Rows[0]["generalname"].ToString() == "Tamil Nadu 33")
            {
                txttgstp.Text = HFIT.Rows[0].Cells[10].Value.ToString();
                txttgstval.Text = HFIT.Rows[0].Cells[11].Value.ToString();
                double dis4 = Convert.ToDouble(HFIT.Rows[0].Cells[10].Value.ToString()) / 2;
                txttcgstp.Text = dis4.ToString("#,0.00");
                txtsgstp.Text = dis4.ToString("#,0.00");
                double dis3 = (Convert.ToDouble(txtexcise.Text) / 100) * (Convert.ToDouble(HFIT.Rows[0].Cells[10].Value.ToString()));
                dis3 = dis3 / 2;
                txttcgval.Text = dis3.ToString("#,0.00");
                txttsgval.Text = dis3.ToString("#,0.00");
                double dis5 = Convert.ToDouble(txtexcise.Text) + dis3 + dis3;
                txtttot.Text = dis5.ToString("#,0.00");


            }
            else
            {
                txttgstp.Text = HFIT.Rows[0].Cells[10].Value.ToString();
                txttgstval.Text = HFIT.Rows[0].Cells[11].Value.ToString();
                txtigstp.Text = HFIT.Rows[0].Cells[10].Value.ToString();
                double dis3 = (Convert.ToDouble(txtexcise.Text) / 100) * (Convert.ToDouble(HFIT.Rows[0].Cells[10].Value.ToString()));
                txtigval.Text = dis3.ToString("#,0.00");
                double dis5 = Convert.ToDouble(txtexcise.Text) + dis3;
                txtttot.Text = dis5.ToString("#,0.00");
            }

            Genclass.sum2 = 0;
            if (txtstid.Text == "24")
            {
                for (int m = 0; m < HFGST.RowCount - 1; m++)
                {
                    Genclass.sum2 = Genclass.sum2 + Convert.ToDouble(HFGST.Rows[m].Cells[4].Value);
                    txttaxtot.Text = Genclass.sum2.ToString();
                    txtigval.Text = Genclass.sum2.ToString();
                }
            }
            else
            {

                for (int m = 0; m < HFGST.RowCount - 1; m++)
                {
                    Genclass.sum2 = Genclass.sum2 + Convert.ToDouble(HFGST.Rows[m].Cells[1].Value);
                    txttaxtot.Text = Genclass.sum2.ToString();
                    txtigval.Text = Genclass.sum2.ToString();
                }
            }


            Double net1 = Convert.ToDouble(txtttot.Text);
            TxtNetAmt.Text = net1.ToString("#,0.00");
            double someInt = (int)net1;

            double rof = Math.Round(net1 - someInt, 2);
            TxtRoff.Text = rof.ToString("#,0.00");

            if (Convert.ToDouble(TxtRoff.Text) < 0.49)
            {
                Double rof1 = -1 * Convert.ToDouble(TxtRoff.Text);
                TxtRoff.Text = rof1.ToString("#,0.00");
            }
            else
            {
                Double rof2 = 1 - Convert.ToDouble(TxtRoff.Text);
                TxtRoff.Text = rof2.ToString("#,0.00");
            }

            Double net = Convert.ToDouble(TxtNetAmt.Text) + Convert.ToDouble(TxtRoff.Text);
            //int ne=Convert.ToInt16(net);
            TxtNetAmt.Text = net.ToString("#,0.00");

            TxtNetAmt.Text = TxtNetAmt.Text.Replace(",", "");
            txttcgval.Text = txttcgval.Text.Replace(",", "");
            txttsgval.Text = txttsgval.Text.Replace(",", "");
            txtigval.Text = txtigval.Text.Replace(",", "");
            txtttot.Text = txtttot.Text.Replace(",", "");
            txttprdval.Text = txttprdval.Text.Replace(",", "");
            txtexcise.Text = txtexcise.Text.Replace(",", "");
            txttdisc.Text = txttdisc.Text.Replace(",", "");

        }

        private void button12_Click_2(object sender, EventArgs e)
        {
            Editpan.Visible = false;
            Genpan.Visible = true;
            panadd.Visible = true;
            Loadgrid();
        }

        private void button7_Click(object sender, EventArgs e)
        {

            for (int i = 0; i < HFGT.RowCount - 1; i++)
            {
                if (HFGT.Rows[i].Cells[2].Value.ToString() == txttermid.Text)
                {
                    MessageBox.Show("Terms Already Added");
                    txtterms.Text = "";
                    txtremde.Text = "";
                    txtterms.Focus();
                    return;
                }
            }

            HFGT.AllowUserToAddRows = true;
            HFGT.AutoGenerateColumns = true;
            var index = HFGT.Rows.Add();
            HFGT.Rows[index].Cells[0].Value = txtterms.Text;
            HFGT.Rows[index].Cells[1].Value = txtremde.Text;
            HFGT.Rows[index].Cells[2].Value = txttermid.Text;

            txtterms.Text = "";
            txtremde.Text = "";
            txtterms.Focus();
        }

        private void btnaddrcan_Click_1(object sender, EventArgs e)
        {
            addipan.Visible = true;
            //termspan.Visible = false;
            Editpan.Visible = false;
            //button1.Visible = false;
            btnaddrcan.Visible = false;
            btnsave.Visible = false;
            buttnfinbk.Visible = true;
            buttnnxt.Visible = true;
        }


        private void txtqty_TextChanged(object sender, EventArgs e)
        {
            if (txtqty.Text != "")
            {
                double bval = Convert.ToDouble(txtqty.Text) * Convert.ToDouble(txtprice.Text);
                txtbval.Text = bval.ToString();
            }

        }



        private void buttnnxt_Click(object sender, EventArgs e)
        {
            addipan.Visible = false;
            termspan.Visible = true;
            buttnfinbk.Visible = false;
            buttnnxt.Visible = false;
            btnaddrcan.Visible = true;
            btnsave.Visible = true;

            //HFGT.
            if (HFGT.Rows[0].Cells[0].Value.ToString() == "" || HFGT.Rows[0].Cells[0].Value == null)
            {
                var index = HFGT.Rows.Add();
                HFGT.Rows[index].Cells[0].Value = "No.of Cases";

                var index1 = HFGT.Rows.Add();
                HFGT.Rows[index1].Cells[0].Value = "Box No/Size";
                var index2 = HFGT.Rows.Add();
                HFGT.Rows[index2].Cells[0].Value = "Gross Weight";
                var index3 = HFGT.Rows.Add();
                HFGT.Rows[index3].Cells[0].Value = "Carrier";
                var index4 = HFGT.Rows.Add();
                HFGT.Rows[index4].Cells[0].Value = "Destination";
                var index5 = HFGT.Rows.Add();
                HFGT.Rows[index5].Cells[0].Value = "LR/RR/RPPNO./DT";
            }
            HFGT.Columns[0].ReadOnly = true;
        }

        private void buttnfinbk_Click(object sender, EventArgs e)
        {
            addipan.Visible = false;
            Taxpan.Visible = false;
            //termspan.Visible = false;
            buttnfinbk.Visible = false;
            buttnnxt.Visible = false;
            //button11.Visible = true;
            //button12.Visible = true;
            panadd.Visible = true;
            Editpan.Visible = false;
            Genpan.Visible = true;

            TxtNetAmt.Text = Txttot.Text;

        }

        private void button1_Click_1(object sender, EventArgs e)
        {



            conn.Close();
            conn.Open();

            qur.CommandText = "delete from  NotoWords";
            qur.ExecuteNonQuery();

            Int64 NumVal = Convert.ToInt64(HFGP.CurrentRow.Cells[6].Value);
            string Nw = Rupees(NumVal);

            qur.CommandText = "Insert into NotoWords values('" + Nw + "',1)";
            qur.ExecuteNonQuery();

            Genclass.Prtid = Convert.ToInt16(HFGP.CurrentRow.Cells[0].Value.ToString());


            for (int i = 1; i < 4; i++)
            {
                Genclass.slno = i;
                Crviewer crv = new Crviewer();

                Genclass.strsql = "select * from Vw_salprtexlfin   where muid=" + Genclass.Prtid + "  and doctypeid=90";

                Genclass.cmd = new SqlCommand(Genclass.strsql, conn);
                SqlDataAdapter aptr = new SqlDataAdapter(Genclass.cmd);
                DataTable tap = new DataTable();
                aptr.Fill(tap);
                if (tap.Rows.Count > 0)
                {
                    if (tap.Rows[0]["bstate"].ToString() == "Tamil Nadu 33")
                    {


                        SqlDataAdapter da = new SqlDataAdapter("[sp_pdfNew1]", conn);
                        da.SelectCommand.CommandType = CommandType.StoredProcedure;
                        da.SelectCommand.Parameters.Add("@Muid", SqlDbType.Int).Value = Genclass.Prtid;
                        da.SelectCommand.Parameters.Add("@slno", SqlDbType.Int).Value = Genclass.slno;
                        da.SelectCommand.Parameters.Add("@companyid", SqlDbType.Int).Value = Genclass.data1;
                        da.SelectCommand.Parameters.Add("@doctypeid", SqlDbType.Int).Value = 90;
                        DataSet ds = new DataSet();
                        da.Fill(ds, "salesinvoice");

                        if (Genclass.data1 == 1)
                        {
                            doc.Load(Application.StartupPath + "\\PurchaseOrder.rpt");
                        }
                        else
                        {
                            doc.Load(Application.StartupPath + "\\salesinvoiceven.rpt");
                        }



                        SqlDataAdapter da1 = new SqlDataAdapter("terms", conn);
                        da1.SelectCommand.CommandType = CommandType.StoredProcedure;
                        da1.SelectCommand.Parameters.Add("@uid", SqlDbType.Int).Value = Genclass.Prtid;
                        DataTable ds1 = new DataTable("Terms");
                        da1.Fill(ds1);
                        ds.Tables.Add(ds1);

                        doc.Subreports["Termrpt1.rpt"].SetDataSource(ds1);

                        SqlDataAdapter da2 = new SqlDataAdapter("SP_taxcal", conn);
                        da2.SelectCommand.CommandType = CommandType.StoredProcedure;
                        da2.SelectCommand.Parameters.Add("@muid", SqlDbType.Int).Value = Genclass.Prtid;
                        da2.SelectCommand.Parameters.Add("@statuid", SqlDbType.Int).Value = tap.Rows[0]["stateuid"].ToString();
                        da2.SelectCommand.Parameters.Add("@companyid", SqlDbType.Int).Value = Genclass.data1;
                        DataTable ds2 = new DataTable("taxcal");
                        da2.Fill(ds2);
                        ds.Tables.Add(ds2);

                        doc.Subreports["Taxrpt.rpt"].SetDataSource(ds2);
                        doc.SetDataSource(ds);


                    }

                    else
                    {
                        SqlDataAdapter da = new SqlDataAdapter("[sp_pdfNew1]", conn);
                        da.SelectCommand.CommandType = CommandType.StoredProcedure;
                        da.SelectCommand.Parameters.Add("@Muid", SqlDbType.Int).Value = Genclass.Prtid;
                        da.SelectCommand.Parameters.Add("@slno", SqlDbType.Int).Value = Genclass.slno;
                        da.SelectCommand.Parameters.Add("@companyid", SqlDbType.Int).Value = Genclass.data1;
                        da.SelectCommand.Parameters.Add("@doctypeid", SqlDbType.Int).Value = 90;
                        DataSet ds = new DataSet();
                        da.Fill(ds, "salesinvoice");

                        if (Genclass.data1 == 1)
                        {
                            doc.Load(Application.StartupPath + "\\PurchaseOrder.rpt");
                        }
                        else
                        {
                            doc.Load(Application.StartupPath + "\\salesinvoiceven.rpt");
                        }


                        SqlDataAdapter da1 = new SqlDataAdapter("terms", conn);
                        da1.SelectCommand.CommandType = CommandType.StoredProcedure;
                        da1.SelectCommand.Parameters.Add("@uid", SqlDbType.Int).Value = Genclass.Prtid;
                        DataTable ds1 = new DataTable("Terms");
                        da1.Fill(ds1);
                        ds.Tables.Add(ds1);

                        doc.Subreports["Termrpt1.rpt"].SetDataSource(ds1);

                        SqlDataAdapter da2 = new SqlDataAdapter("SP_taxcal", conn);
                        da2.SelectCommand.CommandType = CommandType.StoredProcedure;
                        da2.SelectCommand.Parameters.Add("@muid", SqlDbType.Int).Value = Genclass.Prtid;
                        da2.SelectCommand.Parameters.Add("@statuid", SqlDbType.Int).Value = tap.Rows[0]["stateuid"].ToString();
                        da2.SelectCommand.Parameters.Add("@companyid", SqlDbType.Int).Value = Genclass.data1;
                        DataTable ds2 = new DataTable("taxcal");
                        da2.Fill(ds2);
                        ds.Tables.Add(ds2);

                        doc.Subreports["Taxrpt.rpt"].SetDataSource(ds2);
                        doc.SetDataSource(ds);

                    }
                }



                doc.PrintToPrinter(1, false, 0, 0);
            }
        }



        public string Rupees(Int64 rup)
        {
            string result = "";
            Int64 res;
            if ((rup / 10000000) > 0)
            {
                res = rup / 10000000;
                rup = rup % 10000000;
                result = result + ' ' + RupeesToWords(res) + " Crore";
            }
            if ((rup / 100000) > 0)
            {
                res = rup / 100000;
                rup = rup % 100000;
                result = result + ' ' + RupeesToWords(res) + " Lakh";
            }
            if ((rup / 1000) > 0)
            {
                res = rup / 1000;
                rup = rup % 1000;
                result = result + ' ' + RupeesToWords(res) + " Thousand";
            }


            if ((rup / 100) > 0)
            {
                res = rup / 100;
                rup = rup % 100;
                if (rup > 0)
                {
                    result = result + ' ' + RupeesToWords(res) + " Hundred and";
                }
                else
                {
                    result = result + ' ' + RupeesToWords(res) + " Hundred";
                }
            }
            if ((rup % 10) >= 0)
            {
                res = rup % 100;
                result = result + " " + RupeesToWords(res);
            }
            result = "Rupees " + result + ' ' + "only";
            return result;
        }

        public string RupeesToWords(Int64 rup)
        {
            string result = "";
            if ((rup >= 1) && (rup <= 10))
            {
                if ((rup % 10) == 1) result = "One";
                if ((rup % 10) == 2) result = "Two";
                if ((rup % 10) == 3) result = "Three";
                if ((rup % 10) == 4) result = "Four";
                if ((rup % 10) == 5) result = "Five";
                if ((rup % 10) == 6) result = "Six";
                if ((rup % 10) == 7) result = "Seven";
                if ((rup % 10) == 8) result = "Eight";
                if ((rup % 10) == 9) result = "Nine";
                if ((rup % 10) == 0) result = "Ten";
            }
            if (rup > 9 && rup < 20)
            {
                if (rup == 11) result = "Eleven";
                if (rup == 12) result = "Twelve";
                if (rup == 13) result = "Thirteen";
                if (rup == 14) result = "Forteen";
                if (rup == 15) result = "Fifteen";
                if (rup == 16) result = "Sixteen";
                if (rup == 17) result = "Seventeen";
                if (rup == 18) result = "Eighteen";
                if (rup == 19) result = "Nineteen";
            }

            if (rup >= 20 && (rup / 10) == 2 && (rup % 10) == 0) result = "Twenty";
            if (rup > 20 && (rup / 10) == 3 && (rup % 10) == 0) result = "Thirty";
            if (rup > 20 && (rup / 10) == 4 && (rup % 10) == 0) result = "Forty";
            if (rup > 20 && (rup / 10) == 5 && (rup % 10) == 0) result = "Fifty";
            if (rup > 20 && (rup / 10) == 6 && (rup % 10) == 0) result = "Sixty";
            if (rup > 20 && (rup / 10) == 7 && (rup % 10) == 0) result = "Seventy";
            if (rup > 20 && (rup / 10) == 8 && (rup % 10) == 0) result = "Eighty";
            if (rup > 20 && (rup / 10) == 9 && (rup % 10) == 0) result = "Ninty";

            if (rup > 20 && (rup / 10) == 2 && (rup % 10) != 0)
            {
                if ((rup % 10) == 1) result = "Twenty One";
                if ((rup % 10) == 2) result = "Twenty Two";
                if ((rup % 10) == 3) result = "Twenty Three";
                if ((rup % 10) == 4) result = "Twenty Four";
                if ((rup % 10) == 5) result = "Twenty Five";
                if ((rup % 10) == 6) result = "Twenty Six";
                if ((rup % 10) == 7) result = "Twenty Seven";
                if ((rup % 10) == 8) result = "Twenty Eight";
                if ((rup % 10) == 9) result = "Twenty Nine";
            }
            if (rup > 20 && (rup / 10) == 3 && (rup % 10) != 0)
            {
                if ((rup % 10) == 1) result = "Thirty One";
                if ((rup % 10) == 2) result = "Thirty Two";
                if ((rup % 10) == 3) result = "Thirty Three";
                if ((rup % 10) == 4) result = "Thirty Four";
                if ((rup % 10) == 5) result = "Thirty Five";
                if ((rup % 10) == 6) result = "Thirty Six";
                if ((rup % 10) == 7) result = "Thirty Seven";
                if ((rup % 10) == 8) result = "Thirty Eight";
                if ((rup % 10) == 9) result = "Thirty Nine";
            }
            if (rup > 20 && (rup / 10) == 4 && (rup % 10) != 0)
            {
                if ((rup % 10) == 1) result = "Forty One";
                if ((rup % 10) == 2) result = "Forty Two";
                if ((rup % 10) == 3) result = "Forty Three";
                if ((rup % 10) == 4) result = "Forty Four";
                if ((rup % 10) == 5) result = "Forty Five";
                if ((rup % 10) == 6) result = "Forty Six";
                if ((rup % 10) == 7) result = "Forty Seven";
                if ((rup % 10) == 8) result = "Forty Eight";
                if ((rup % 10) == 9) result = "Forty Nine";
            }
            if (rup > 20 && (rup / 10) == 5 && (rup % 10) != 0)
            {
                if ((rup % 10) == 1) result = "Fifty One";
                if ((rup % 10) == 2) result = "Fifty Two";
                if ((rup % 10) == 3) result = "Fifty Three";
                if ((rup % 10) == 4) result = "Fifty Four";
                if ((rup % 10) == 5) result = "Fifty Five";
                if ((rup % 10) == 6) result = "Fifty Six";
                if ((rup % 10) == 7) result = "Fifty Seven";
                if ((rup % 10) == 8) result = "Fifty Eight";
                if ((rup % 10) == 9) result = "Fifty Nine";
            }
            if (rup > 20 && (rup / 10) == 6 && (rup % 10) != 0)
            {
                if ((rup % 10) == 1) result = "Sixty One";
                if ((rup % 10) == 2) result = "Sixty Two";
                if ((rup % 10) == 3) result = "Sixty Three";
                if ((rup % 10) == 4) result = "Sixty Four";
                if ((rup % 10) == 5) result = "Sixty Five";
                if ((rup % 10) == 6) result = "Sixty Six";
                if ((rup % 10) == 7) result = "Sixty Seven";
                if ((rup % 10) == 8) result = "Sixty Eight";
                if ((rup % 10) == 9) result = "Sixty Nine";
            }
            if (rup > 20 && (rup / 10) == 7 && (rup % 10) != 0)
            {
                if ((rup % 10) == 1) result = "Seventy One";
                if ((rup % 10) == 2) result = "Seventy Two";
                if ((rup % 10) == 3) result = "Seventy Three";
                if ((rup % 10) == 4) result = "Seventy Four";
                if ((rup % 10) == 5) result = "Seventy Five";
                if ((rup % 10) == 6) result = "Seventy Six";
                if ((rup % 10) == 7) result = "Seventy Seven";
                if ((rup % 10) == 8) result = "Seventy Eight";
                if ((rup % 10) == 9) result = "Seventy Nine";
            }
            if (rup > 20 && (rup / 10) == 8 && (rup % 10) != 0)
            {
                if ((rup % 10) == 1) result = "Eighty One";
                if ((rup % 10) == 2) result = "Eighty Two";
                if ((rup % 10) == 3) result = "Eighty Three";
                if ((rup % 10) == 4) result = "Eighty Four";
                if ((rup % 10) == 5) result = "Eighty Five";
                if ((rup % 10) == 6) result = "Eighty Six";
                if ((rup % 10) == 7) result = "Eighty Seven";
                if ((rup % 10) == 8) result = "Eighty Eight";
                if ((rup % 10) == 9) result = "Eighty Nine";
            }
            if (rup > 20 && (rup / 10) == 9 && (rup % 10) != 0)
            {
                if ((rup % 10) == 1) result = "Ninty One";
                if ((rup % 10) == 2) result = "Ninty Two";
                if ((rup % 10) == 3) result = "Ninty Three";
                if ((rup % 10) == 4) result = "Ninty Four";
                if ((rup % 10) == 5) result = "Ninty Five";
                if ((rup % 10) == 6) result = "Ninty Six";
                if ((rup % 10) == 7) result = "Ninty Seven";
                if ((rup % 10) == 8) result = "Ninty Eight";
                if ((rup % 10) == 9) result = "Ninty Nine";
            }
            return result;
        }

        //public string convertToString(int number)
        //{
        //    string[] numbers = { "Zero", "One", "Two", "Three", "Four", "Five", "Six", "Seven", "Eight", "Nine", "Ten", "Eleven", "Twelve", "Thirteen", "Fourteen", "Fifteen", "Sixteen", "Seventeen", "Eighteen", "Nineteen" };
        //    string[] tens = { "", "Ten", "Twenty", "Thirty", "Forty", "Fifty", "Sixty", "Seventy", "Eighty", "Ninety" };

        //    char[] digits = number.ToString().ToCharArray();
        //    string words = null;

        //    if (number >= 0 && number <= 19)
        //    {
        //        words = words + numbers[number];
        //    }
        //    else if (number >= 20 && number <= 99)
        //    {
        //        int firstDigit = (int)Char.GetNumericValue(digits[0]);
        //        int secondPart = number % 10;

        //        words = words + tens[firstDigit];

        //        if (secondPart > 0)
        //        {
        //            words = words + " " + convertToString(secondPart);
        //        }
        //    }
        //    else if (number >= 100 && number <= 999)
        //    {
        //        int firstDigit = (int)Char.GetNumericValue(digits[0]);
        //        int secondPart = number % 100;

        //        words = words + numbers[firstDigit] + " Hundred";

        //        if (secondPart > 0)
        //        {
        //            words = words + " and " + convertToString(secondPart);
        //        }
        //    }
        //    else if (number >= 1000 && number <= 19999)
        //    {
        //        int firstPart = (int)Char.GetNumericValue(digits[0]);
        //        if (number >= 10000)
        //        {
        //            string twoDigits = digits[0].ToString() + digits[1].ToString();
        //            firstPart = Convert.ToInt16(twoDigits);
        //        }
        //        int secondPart = number % 1000;

        //        words = words + numbers[firstPart] + " Thousand";

        //        if (secondPart > 0 && secondPart <= 99)
        //        {
        //            words = words + " and " + convertToString(secondPart);
        //        }
        //        else if (secondPart >= 100)
        //        {
        //            words = words + " " + convertToString(secondPart);
        //        }
        //    }
        //    else if (number >= 20000 && number <= 999999)
        //    {
        //        string firstStringPart = Char.GetNumericValue(digits[0]).ToString() + Char.GetNumericValue(digits[1]).ToString();

        //        if (number >= 100000)
        //        {
        //            firstStringPart = firstStringPart + Char.GetNumericValue(digits[2]).ToString();
        //        }

        //        int firstPart = Convert.ToInt16(firstStringPart);
        //        int secondPart = number - (firstPart * 1000);

        //        words = words + convertToString(firstPart) + " Thousand";

        //        if (secondPart > 0 && secondPart <= 99)
        //        {
        //            words = words + " and " + convertToString(secondPart);
        //        }
        //        else if (secondPart >= 100)
        //        {
        //            words = words + " " + convertToString(secondPart);
        //        }

        //    }
        //    else if (number == 100000)
        //    {
        //        words = words + "One Lakh";
        //    }
        //    return words;
        //}


        private void txtname_KeyDown_1(object sender, KeyEventArgs e)
        {

        }

        private void txtdcno_KeyDown_1(object sender, KeyEventArgs e)
        {
            //if (Genclass.Dtype == 40)
            //{
            //    if (e.KeyCode == Keys.Enter)
            //    {
            //        Genclass.type = 2;
            //        loadput();
            //    }
            //    else if (e.KeyCode == Keys.Escape)
            //    {
            //        txtdcno.Text = "";
            //        txtdcno.Focus();
            //    }
            //}

        }

        private void txtdcid_TextChanged_1(object sender, EventArgs e)
        {

            if (Genclass.Dtype == 90)
            {

                if (txtdcno.Text == "")
                {
                    return;

                }
                else
                {
                    Genclass.strsql = "select Itemname,UoM,itemuid,refid,qty,PRICE,convert(decimal(18,2),BasicValue,105) AS BasicValue,disper,convert(decimal(18,2),Disvalue,105) AS Disvalue,convert(decimal(18,2),Taxablevalue,105) AS Taxablevalue,convert(decimal(18,2),igstval,105) AS igstval,ig,0 as gid,0 as gt,convert(decimal(18,2),(BasicValue-(isnull(Disvalue,0))+ (isnull(igstval,0))),105) as total,hsnid from (select distinct   c.itemname,d.generalname as uom,b.itemuid,b.uid as refid,b.pqty-isnull(sum(e.pqty),0) as qty,f.Price ,(b.pqty-isnull(sum(e.pqty),0)) * f.Price as BasicValue,disper,((b.pqty-isnull(sum(e.pqty),0)) * f.Price/100)* disper as Disvalue,((b.pqty-isnull(sum(e.pqty),0)) * f.Price)-(((b.pqty-isnull(sum(e.pqty),0)) * f.Price/100)* disper) as Taxablevalue,i.f1 as ig,(((b.pqty-isnull(sum(e.pqty),0)) * f.Price)-(((b.pqty-isnull(sum(e.pqty),0)) * f.Price/100)* disper))/100 * i.f1 as igstval,c.hsnid from transactionsp a inner join transactionsplist b on  a.uid=b.transactionspuid and a.doctypeid=20 and a.companyid=1 inner join itemm c on b.itemuid=c.uid left join generalm d on c.uom_uid=d.uid  left join transactionsplist e on  b.uid=e.refuid and e.doctypeid=40 inner join pur_price_list f on c.uid=f.itemuid and c.partyuid=f.suppuid left join generalm i on f.igstid=i.uid where a.docno='" + txtdcno.Text + "' group by c.itemname,d.generalname,b.itemuid,b.uid,b.pqty,f.price,disper,i.f1,c.hsnid having b.pqty-isnull(sum(e.pqty),0) >0) tab";

                    Genclass.cmd = new SqlCommand(Genclass.strsql, conn);
                    SqlDataAdapter aptr1 = new SqlDataAdapter(Genclass.cmd);
                    DataTable tap1 = new DataTable();
                    aptr1.Fill(tap1);
                    //txtname.Text = tap1.Rows[0]["Name"].ToString();
                    //txtpuid.Text = tap1.Rows[0]["Partyuid"].ToString();
                    for (int i = 0; i < tap1.Rows.Count; i++)
                    {
                        var index = HFIT.Rows.Add();



                        HFIT.Rows[index].Cells[0].Value = tap1.Rows[i]["itemname"].ToString();
                        HFIT.Rows[index].Cells[1].Value = tap1.Rows[i]["UOM"].ToString();
                        HFIT.Rows[index].Cells[2].Value = tap1.Rows[i]["Price"].ToString();
                        HFIT.Rows[index].Cells[3].Value = tap1.Rows[i]["qty"].ToString();
                        HFIT.Rows[index].Cells[4].Value = tap1.Rows[i]["BasicValue"].ToString();
                        HFIT.Rows[index].Cells[5].Value = tap1.Rows[i]["itemuid"].ToString();
                        HFIT.Rows[index].Cells[6].Value = tap1.Rows[i]["refid"].ToString();
                        HFIT.Rows[index].Cells[7].Value = tap1.Rows[i]["disper"].ToString();
                        HFIT.Rows[index].Cells[8].Value = tap1.Rows[i]["Disvalue"].ToString();
                        HFIT.Rows[index].Cells[9].Value = tap1.Rows[i]["Taxablevalue"].ToString();
                        HFIT.Rows[index].Cells[10].Value = tap1.Rows[i]["ig"].ToString();
                        HFIT.Rows[index].Cells[11].Value = tap1.Rows[i]["igstval"].ToString();
                        HFIT.Rows[index].Cells[12].Value = tap1.Rows[i]["gid"].ToString();
                        HFIT.Rows[index].Cells[13].Value = tap1.Rows[i]["gt"].ToString();
                        HFIT.Rows[index].Cells[14].Value = tap1.Rows[i]["Total"].ToString();
                        HFIT.Rows[index].Cells[15].Value = tap1.Rows[i]["hsnid"].ToString();

                        Genclass.sum1 = Genclass.sum1 + Convert.ToDouble(HFIT.Rows[i].Cells[14].Value);
                        Txttot.Text = Genclass.sum1.ToString();




                        //Titlep();



                    }

                }


            }
            else
            {
                txtitemname.Focus();

            }
        }

        private void TxtNetAmt_TextChanged(object sender, EventArgs e)
        {

        }

        private void buttcusok_Click_1(object sender, EventArgs e)
        {


        }

        private void txtitemname_KeyDown_1(object sender, KeyEventArgs e)
        {

        }

        private void txtqty_TextChanged_1(object sender, EventArgs e)
        {

        }

        private void txtplace_KeyDown(object sender, KeyEventArgs e)
        {

        }

        private void txtcharges_TextChanged(object sender, EventArgs e)
        {
            splittax();
        }

        private void label16_Click(object sender, EventArgs e)
        {

        }

        private void TxtRoff_TextChanged(object sender, EventArgs e)
        {

        }

        private void label17_Click(object sender, EventArgs e)
        {

        }

        private void txttbval_TextChanged(object sender, EventArgs e)
        {

        }

        private void txttdis_TextChanged(object sender, EventArgs e)
        {
            if (txttdis.Text != "")
            {
                if (Convert.ToDouble(txttdis.Text) > 0)
                {
                    double dis = (Convert.ToDouble(txttbval.Text) / 100) * (Convert.ToDouble(txttdis.Text));
                    txttdisc.Text = dis.ToString();
                }
                else
                {
                    txttdisc.Text = "0";
                }
                splittax();
            }
        }

        private void txtprice_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtname_KeyDown(object sender, KeyEventArgs e)
        {

        }

        private void txtplace_KeyDown_1(object sender, KeyEventArgs e)
        {

        }

        private void txtitemname_KeyDown(object sender, KeyEventArgs e)
        {



        }
        private void loadput1()
        {
            conn.Close();
            conn.Open();
            //txttitemid.Text = "";

            if (Genclass.type == 5)
            {

                //Genclass.Module.Partylistviewcont3("uid", "Item","Itemcode", Genclass.strsql, this, txttitemid, txtitemname,txtdcid, Editpan);

                //Genclass.strsql = "select distinct b.uid,itemname,price from pur_price_list a inner join itemm b on a.itemuid=b.uid where a.suppuid=" + txtpuid.Text + " and b.active=1 and a.companyid=1 and a.Eff_to is null";
                if (Genclass.data1 == 1)
                {
                    string quy4 = "select * from pur_price_list where suppuid=" + txtpuid.Text + "";
                    Genclass.cmd = new SqlCommand(quy4, conn);
                    SqlDataAdapter aptr4 = new SqlDataAdapter(Genclass.cmd);
                    DataTable tap4 = new DataTable();
                    aptr4.Fill(tap4);
                    if (tap4.Rows.Count > 0)
                    {
                        Genclass.Module.Partylistviewcont2("uid", "Itemcode", "Itemname", "Price", Genclass.strsql, this, txttitemid, txtitemname, txtdcid, txtprice, Editpan);
                        Genclass.strsql = "select top 25 b.uid,ItemName,itemcode,price  from pur_price_list a inner join itemm b on a.itemuid=b.uid where a.suppuid=" + txtpuid.Text + "  and a.companyid=" + Genclass.data1 + "";
                        Genclass.FSSQLSortStr = "ItemName";
                    }

                    else
                    {
                        Genclass.Module.Partylistviewcont3("uid", "Item", "Itemcode", Genclass.strsql, this, txttitemid, txtitemname, txtdcid, Editpan);
                        Genclass.strsql = "select top 25 uid,ItemName,Itemcode  from Itemm where active=1  and companyid=" + Genclass.data1 + "";
                        Genclass.FSSQLSortStr = "ItemName";
                    }
                }
                else
                {
                    string quy4 = "select * from pur_price_list where suppuid=" + txtpuid.Text + "";
                    Genclass.cmd = new SqlCommand(quy4, conn);
                    SqlDataAdapter aptr4 = new SqlDataAdapter(Genclass.cmd);
                    DataTable tap4 = new DataTable();
                    aptr4.Fill(tap4);
                    if (tap4.Rows.Count > 0)
                    {
                        Genclass.Module.Partylistviewcont2("uid", "Itemcode", "Itemname", "Price", Genclass.strsql, this, txttitemid, txtdcid, txtitemname, txtprice, Editpan);
                        Genclass.strsql = "select top 25 b.uid,itemcode,ItemName,price  from pur_price_list a inner join itemm b on a.itemuid=b.uid where a.suppuid=" + txtpuid.Text + "  and a.companyid=" + Genclass.data1 + "";
                        Genclass.FSSQLSortStr = "itemcode";
                    }

                    else
                    {
                        Genclass.Module.Partylistviewcont3("uid", "Item", "Itemcode", Genclass.strsql, this, txttitemid, txtdcid, txtitemname, Editpan);
                        Genclass.strsql = "select top 25 uid,Itemcode,ItemName  from Itemm where active=1  and companyid=" + Genclass.data1 + "";
                        Genclass.FSSQLSortStr = "itemcode";
                    }
                }

                Genclass.cmd = new SqlCommand(Genclass.strsql, conn);
                SqlDataAdapter aptr = new SqlDataAdapter(Genclass.cmd);
                DataTable tap = new DataTable();
                aptr.Fill(tap);


                FrmLookup2 contc = new FrmLookup2();
                TabControl tab = (TabControl)contc.Controls["tabC"];
                //    TabPage tab1 = (TabPage)tab.Controls["tabPage1"];
                //tab1.Controls.


                TabPage tab3 = (TabPage)tab.Controls["tabPage2"];
                DataGridView grid2 = (DataGridView)tab3.Controls["HFGP2"];
                grid2.Refresh();
                grid2.ColumnCount = tap.Columns.Count;
                grid2.Columns[0].Visible = false;

                if (Genclass.data1 == 1)
                {
                    grid2.Columns[1].Width = 350;
                    grid2.Columns[2].Width = 150;
                }
                else
                {
                    grid2.Columns[1].Width = 150;
                    grid2.Columns[2].Width = 350;
                }



                grid2.DefaultCellStyle.Font = new Font("Arial", 10);

                grid2.ColumnHeadersDefaultCellStyle.Font = new Font("Arial", 10, FontStyle.Bold);
                grid2.AutoGenerateColumns = false;

                Genclass.i = 0;
                foreach (DataColumn column in tap.Columns)
                {
                    grid2.Columns[Genclass.i].Name = column.ColumnName;
                    grid2.Columns[Genclass.i].HeaderText = column.ColumnName;
                    grid2.Columns[Genclass.i].DataPropertyName = column.ColumnName;
                    Genclass.i = Genclass.i + 1;
                }


                grid2.DataSource = tap;

                if (Genclass.data1 == 1)
                {
                    Genclass.Module.Partylistviewcont3("uid", "itemname", "itemcode", Genclass.strsql, this, txttitemid, txtitemname, txtdcid, Editpan);

                    Genclass.strsql = "select uid,itemname,itemcode from  ItemM    where companyid=" + Genclass.data1 + "";
                    Genclass.FSSQLSortStr = "itemname";
                    Genclass.FSSQLSortStr1 = "itemcode";
                }
                else
                {
                    Genclass.Module.Partylistviewcont3("uid", "itemname", "itemcode", Genclass.strsql, this, txttitemid, txtdcid, txtitemname, Editpan);

                    Genclass.strsql = "select uid,itemcode,itemname from  ItemM    where companyid=" + Genclass.data1 + "";
                    Genclass.FSSQLSortStr = "itemcode";
                    Genclass.FSSQLSortStr1 = "itemname";
                }

                Genclass.cmd = new SqlCommand(Genclass.strsql, conn);
                SqlDataAdapter aptr3 = new SqlDataAdapter(Genclass.cmd);
                DataTable tap3 = new DataTable();
                aptr3.Fill(tap3);

                TabPage tab4 = (TabPage)tab.Controls["tabPage3"];
                DataGridView grid3 = (DataGridView)tab4.Controls["HFGP3"];
                grid3.Refresh();
                grid3.ColumnCount = tap3.Columns.Count;
                grid3.Columns[0].Visible = false;

                if (Genclass.data1 == 1)
                {
                    grid3.Columns[1].Width = 350;
                    grid3.Columns[2].Width = 150;
                }
                else
                {
                    grid3.Columns[1].Width = 150;
                    grid3.Columns[2].Width = 350;
                }



                grid3.DefaultCellStyle.Font = new Font("Arial", 10);

                grid3.ColumnHeadersDefaultCellStyle.Font = new Font("Arial", 10, FontStyle.Bold);
                grid3.AutoGenerateColumns = false;

                Genclass.i = 0;
                foreach (DataColumn column in tap3.Columns)
                {
                    grid3.Columns[Genclass.i].Name = column.ColumnName;
                    grid3.Columns[Genclass.i].HeaderText = column.ColumnName;
                    grid3.Columns[Genclass.i].DataPropertyName = column.ColumnName;
                    Genclass.i = Genclass.i + 1;
                }


                grid3.DataSource = tap3;



                contc.Show();
                conn.Close();
            }

        }

        private void buttcusok_Click(object sender, EventArgs e)
        {
            if (txtqty.Text == "" || txtqty.Text == "0" || txtprice.Text == "" || txtprice.Text == "0" || txtitemname.Text == "" || txtitemname.Text == "0" || txtname.Text == "" || txtname.Text == "0")
            {
                MessageBox.Show("Enter the Party or Item or Rate or Qty to Proceed");
                return;
            }

            if (txtbval.Text != "" || txtbval.Text != "0")
            {
                //Genclass.strsql = "select distinct Itemname,UoM,itemuid,0 as refid,qty,PRICE,convert(decimal(18,2),BasicValue,105) AS BasicValue,disper,convert(decimal(18,2),Disvalue,105) AS Disvalue,convert(decimal(18,2),Taxablevalue,105) AS Taxablevalue,convert(decimal(18,2),Cgstval,105) AS EDVAL,convert(decimal(18,2),sgstval,105) AS VATVAL,0 AS igstval,CG as ED,sg as VAT,0 as ig,convert(decimal(18,2),BasicValue-(isnull(Disvalue,0))+ (isnull(Cgstval,0)+isnull(sgstval,0)),105) as total from (select distinct   c.itemname,d.generalname as uom,b.itemuid,b.uid as refid," + txtqty.Text + " as qty," + txtprice.Text + " as Price," + txtbval.Text + " as BasicValue,disper,(" + txtbval.Text + " * disper)/100 as Disvalue," + txtbval.Text + "-((" + txtbval.Text + " * disper)/100) as Taxablevalue,g.f1 as CG,((" + txtbval.Text + "-((" + txtbval.Text + " * disper)/100)) * g.f1)/100 as Cgstval,h.f1 as SG,((" + txtbval.Text + "-((" + txtbval.Text + " * disper)/100)+ ((" + txtbval.Text + "-((" + txtbval.Text + " * disper)/100)) * g.f1)/100) * h.f1)/100 as sgstval from transactionsp a inner join transactionsplist b on  a.uid=b.transactionspuid and a.doctypeid=30 and a.companyid=1 inner join itemm c on b.itemuid=c.uid left join generalm d on c.uom_uid=d.uid   inner join pur_price_list f on c.uid=f.itemuid and c.partyuid=f.suppuid inner join generalm g on f.cgstid=g.uid inner join generalm h on f.sgstid=h.uid inner join generalm i on f.igstid=i.uid where c.uid=" + txttitemid.Text + " group by c.itemname,d.generalname,b.itemuid,b.uid,b.pqty,f.price,disper,g.f1,h.f1,i.f1) tab";
                //Genclass.strsql = "select distinct uid as itemuid,hsnid,Itemname,UoM,0 as refid," + txtqty.Text + " as qty,PRICE,convert(decimal(18,2),BasicValue,105) AS BasicValue,disper,convert(decimal(18,2),Disvalue,105) AS Disvalue,convert(decimal(18,2),Taxablevalue,105) AS Taxablevalue,gstper,gstval,convert(decimal(18,2),BasicValue-(isnull(Disvalue,0))+ (isnull(gstval,0)),105) as total from (select distinct   c.hsnid,c.itemname,d.generalname as uom,c.uid,10 as qty,42.00 as Price," + txtbval.Text + " as BasicValue,disper,(" + txtbval.Text + " * disper)/100 as Disvalue," + txtbval.Text + "-((" + txtbval.Text + " * disper)/100) as Taxablevalue,i.f1 as gstper,((" + txtqty.Text + " * f.Price)-(((" + txtqty.Text + " * f.Price)/100)* disper))/100 * i.f1 as gstval from  itemm c left join generalm d on c.uom_uid=d.uid   inner join pur_price_list f on c.uid=f.itemuid and c.partyuid=f.suppuid  left join generalm i on f.igstid=i.uid where c.uid=" + txttitemid.Text + " group by c.hsnid,c.itemname,d.generalname,c.uid,f.price,disper,i.f1) tab";
                //Genclass.strsql = "select distinct uid as itemuid,hsnid,Itemname,UoM,0 as refid,qty,PRICE,convert(decimal(18,2),BasicValue,105) AS BasicValue,disper,convert(decimal(18,2),Disvalue,105) AS Disvalue,convert(decimal(18,2),Taxablevalue,105) AS Taxablevalue,gstper,gstval,convert(decimal(18,2),BasicValue-(isnull(Disvalue,0))+ (isnull(gstval,0)),105) as total from (select distinct c.hsnid,c.itemname,d.generalname as uom,c.uid," + txtqty.Text + " as qty," + txtprice.Text + " as Price," + txtbval.Text + " as BasicValue,0 as disper,0 as Disvalue," + txtbval.Text + " as Taxablevalue,i.f1 as gstper,convert(decimal(18,2),(" + txtbval.Text + " /100 * i.f1),105) as gstval from  itemm c left join generalm d on c.uom_uid=d.uid left join ItemGroup j on c.itemgroup_Uid=j.UId left join Hsndet f on j.hsnid=f.uid left join generalm i on f.sgid=i.uid  where c.Uid=" + txttitemid.Text + " group by c.hsnid,c.itemname,d.generalname,c.uid,i.f1) tab";
                Genclass.strsql = "select distinct uid as itemuid,Itemname,UoM,0 as hsnid,0 as refid,qty,PRICE,convert(decimal(18,2),BasicValue,105) AS  BasicValue,disper,convert(decimal(18,2),Disvalue,105) AS Disvalue,convert(decimal(18,2),Taxablevalue,105) AS   Taxablevalue,gstper,gstval,convert(decimal(18,2),BasicValue-(isnull(Disvalue,0))+ (isnull(gstval,0)),105) as total   from (select distinct c.itemname,d.generalname as uom,c.uid," + txtqty.Text + " as qty," + txtprice.Text + "  as Price," + txtbval.Text + " as BasicValue,  0 as disper,0 as Disvalue," + txtbval.Text + " as Taxablevalue,h.f1 as gstper,convert(decimal(18,2),(" + txtbval.Text + "/100 * h.f1),105) as gstval   from  itemm c left join generalm d on c.uom_uid=d.uid left join ItemGroup j on c.itemgroup_Uid=j.UId  left join generalm h on c.tax=h.uid   where c.Uid=" + txttitemid.Text + " group by c.itemname,d.generalname,c.uid,sname,h.f1)  tab";




                Genclass.cmd = new SqlCommand(Genclass.strsql, conn);
                SqlDataAdapter aptr1 = new SqlDataAdapter(Genclass.cmd);
                DataTable tap1 = new DataTable();
                aptr1.Fill(tap1);
                //txtname.Text = tap1.Rows[0]["Name"].ToString();
                //txtpuid.Text = tap1.Rows[0]["Partyuid"].ToString();
                //for (int i = 0; i < tap1.Rows.Count; i++)
                //{
                var index = HFIT.Rows.Add();
                HFIT.Rows[index].Cells[0].Value = tap1.Rows[0]["itemname"].ToString();
                HFIT.Rows[index].Cells[1].Value = tap1.Rows[0]["UOM"].ToString();

                double sump = Convert.ToDouble(tap1.Rows[0]["Price"].ToString());
                HFIT.Rows[index].Cells[2].Value = sump.ToString("0.000");
                double sumq = Convert.ToDouble(tap1.Rows[0]["qty"].ToString());
                HFIT.Rows[index].Cells[3].Value = sumq.ToString("0.00");
                double sumb = Convert.ToDouble(tap1.Rows[0]["BasicValue"].ToString());
                HFIT.Rows[index].Cells[4].Value = sumb.ToString("0.00");

                //HFIT.Rows[index].Cells[2].Value = Txttot.Text.Replace(",", "");
                //Txttot.Text = Txttot.Text.Replace(",", "");
                //Txttot.Text = Txttot.Text.Replace(",", "");
                //HFIT.Rows[index].Cells[2].Value = tap1.Rows[i]["Price"].ToString();
                //HFIT.Rows[index].Cells[3].Value = tap1.Rows[i]["qty"].ToString();
                //HFIT.Rows[index].Cells[4].Value = tap1.Rows[i]["BasicValue"].ToString();
                HFIT.Rows[index].Cells[5].Value = tap1.Rows[0]["itemuid"].ToString();
                HFIT.Rows[index].Cells[6].Value = tap1.Rows[0]["refid"].ToString();
                HFIT.Rows[index].Cells[7].Value = tap1.Rows[0]["disper"].ToString();
                HFIT.Rows[index].Cells[8].Value = tap1.Rows[0]["Disvalue"].ToString();
                HFIT.Rows[index].Cells[9].Value = tap1.Rows[0]["Taxablevalue"].ToString();
                HFIT.Rows[index].Cells[10].Value = tap1.Rows[0]["GSTper"].ToString();
                HFIT.Rows[index].Cells[11].Value = tap1.Rows[0]["GSTVAL"].ToString();
                //HFIT.Rows[index].Cells[12].Value = tap1.Rows[i]["VAT"].ToString();
                //HFIT.Rows[index].Cells[13].Value = tap1.Rows[i]["VATVAL"].ToString();
                HFIT.Rows[index].Cells[14].Value = tap1.Rows[0]["Total"].ToString();
                HFIT.Rows[index].Cells[15].Value = tap1.Rows[0]["hsnid"].ToString();
                HFIT.Rows[index].Cells[16].Value = txtnotes.Text;
                dis4 = Convert.ToDouble(HFIT.Rows[index].Cells[10].Value);
                dis3 = Convert.ToDouble(HFIT.Rows[index].Cells[11].Value);
                Genclass.sum1 = Genclass.sum1 + Convert.ToDouble(HFIT.Rows[index].Cells[4].Value);
                Txttot.Text = Genclass.sum1.ToString("#,0.00");
                txttbval.Text = Genclass.sum1.ToString("#,0.00");

                Txttot.Text = Txttot.Text.Replace(",", "");
                Titlegst();


                if (txtstid.Text == "24")
                {

                    dis4 = Convert.ToDouble(HFIT.Rows[index].Cells[10].Value) / 2;
                    dis3 = Convert.ToDouble(HFIT.Rows[index].Cells[11].Value) / 2;
                }
                else
                {
                    dis4 = Convert.ToDouble(HFIT.Rows[index].Cells[10].Value);
                    dis3 = Convert.ToDouble(HFIT.Rows[index].Cells[11].Value);
                }


                HFIT.Rows[index].Cells[14].Value = tap1.Rows[0]["Total"].ToString();
                HFIT.Rows[index].Cells[15].Value = tap1.Rows[0]["hsnid"].ToString();
                HFIT.Rows[index].Cells[16].Value = txtnotes.Text;
                txttdis_TextChanged_1(sender, e);

            }




            Titlep();
            txtitemname.Text = "";
            txtprice.Text = "";
            txtqty.Text = "";
            txtnotes.Text = "";
            txtbval.Text = "";
            txtscr11.Focus();




            //Genclass.sum1 = 0;
            //for (int j = 0; j < HFIT.RowCount - 1; j++)
            //{

            //    Genclass.sum1 = Genclass.sum1 + Convert.ToDouble(HFIT.Rows[j].Cells[4].Value);
            //    Txttot.Text = Genclass.sum1.ToString("#,0.00");
            //}
            //button11_Click_2(sender, e);
        }


        private void txtprice_TextChanged_1(object sender, EventArgs e)
        {
            if (txtprice.Text != "" && txtqty.Text != "")
            {
                double add = Convert.ToDouble(txtprice.Text) * Convert.ToDouble(txtqty.Text);
                txtbval.Text = add.ToString("#,0.000");

                txtbval.Text = txtbval.Text.Replace(",", "");
            }

        }
        private void Titlegst()
        {
            if (txtstid.Text != "")
            {
                if (txtstid.Text == "24")
                {
                    HFGST.ColumnCount = 5;

                    HFGST.Columns[0].Name = "CGST%";
                    HFGST.Columns[1].Name = "CGST";

                    HFGST.Columns[2].Name = "SGST%";
                    HFGST.Columns[3].Name = "SGST";
                    HFGST.Columns[4].Name = "Total";


                    HFGST.Columns[0].Width = 60;
                    HFGST.Columns[1].Width = 70;
                    HFGST.Columns[2].Width = 60;
                    HFGST.Columns[3].Width = 70;
                    HFGST.Columns[4].Width = 60;

                }


                else
                {
                    HFGST.ColumnCount = 2;

                    HFGST.Columns[0].Name = "IGST%";
                    HFGST.Columns[1].Name = "IGST";
                    HFGST.Columns[0].Width = 100;
                    HFGST.Columns[1].Width = 150;

                }
            }
        }

        private void txtqty_TextChanged_2(object sender, EventArgs e)
        {
            if (txtqty.Text != "")
            {
                double add = Convert.ToDouble(txtprice.Text) * Convert.ToDouble(txtqty.Text);
                txtbval.Text = add.ToString("#,0.00");
                txtbval.Text = txtbval.Text.Replace(",", "");
            }
        }

        private void txtname_TextChanged(object sender, EventArgs e)
        {

        }

        private void butcan_Click(object sender, EventArgs e)
        {
            string message = "Are you sure to cancel this Invoice ?";
            string caption = "Dilama";
            MessageBoxButtons buttons = MessageBoxButtons.YesNo;
            DialogResult result;
            conn.Close();
            conn.Open();
            result = MessageBox.Show(message, caption, buttons);

            if (result == System.Windows.Forms.DialogResult.Yes)
            {

                int i = HFGP.SelectedCells[0].RowIndex;
                uid = HFGP.Rows[i].Cells[0].Value.ToString();
                qur.CommandText = "Update stransactionsp set active=0 where uid=" + uid + "";
                qur.ExecuteNonQuery();
                MessageBox.Show("Invoice Cancelled");
            }
            Loadgrid();
        }

        private void HFIT_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Delete)
            {
                j = HFIT.SelectedCells[0].RowIndex;

                double sun = Convert.ToDouble(HFIT.Rows[j].Cells[4].Value.ToString());

                Genclass.sum1 = Convert.ToDouble(Txttot.Text);


                Genclass.sum1 = Genclass.sum1 - sun;

                Txttot.Text = Genclass.sum1.ToString("0.00");
                txttbval.Text = Genclass.sum1.ToString("0.00");
                txttdis_TextChanged_1(sender, e);
            }
        }

        private void txtpuid_TextChanged(object sender, EventArgs e)
        {
            if (txtpuid.Text != "")
            {
                Genclass.strsql = "Select address1, address2 ,city,stateuid  from partym where uid=" + txtpuid.Text + " and companyid=" + Genclass.data1 + "";



                Genclass.cmd = new SqlCommand(Genclass.strsql, conn);
                SqlDataAdapter aptr1 = new SqlDataAdapter(Genclass.cmd);
                DataTable tap1 = new DataTable();
                aptr1.Fill(tap1);

                if (tap1.Rows.Count > 0)
                {
                    txtpadd1.Text = tap1.Rows[0]["address1"].ToString() + ' ' + tap1.Rows[0]["address2"].ToString() + ' ' + tap1.Rows[0]["city"].ToString();
                    txtplace.Text = txtname.Text;
                    txtpluid.Text = txtpuid.Text;
                    txtpadd2.Text = txtpadd1.Text;
                    txtstid.Text = tap1.Rows[0]["stateuid"].ToString();
                }
            }
        }

        private void txtpluid_TextChanged(object sender, EventArgs e)
        {
            if (txtpluid.Text != "")
            {

                Genclass.strsql = "Select address1, address2 ,city,stateuid  from partym where uid=" + txtpluid.Text + " and companyid=" + Genclass.data1 + "";



                Genclass.cmd = new SqlCommand(Genclass.strsql, conn);
                SqlDataAdapter aptr1 = new SqlDataAdapter(Genclass.cmd);
                DataTable tap1 = new DataTable();
                aptr1.Fill(tap1);

                if (tap1.Rows.Count > 0)
                {
                    txtpadd2.Text = tap1.Rows[0]["address1"].ToString() + ' ' + tap1.Rows[0]["address2"].ToString() + ' ' + tap1.Rows[0]["city"].ToString();
                    //txtplace.Text = txtname.Text;
                    //txtpluid.Text = txtpuid.Text;
                    //txtpadd2.Text = txtpadd1.Text;
                    txtstid.Text = tap1.Rows[0]["stateuid"].ToString();
                }
            }
        }

        private void txtitemname_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtdcid_TextChanged(object sender, EventArgs e)
        {




        }

        private void txttrans_TextChanged(object sender, EventArgs e)
        {

        }

        private void Editpan_Paint(object sender, PaintEventArgs e)
        {

        }

        private void txtname_Click(object sender, EventArgs e)
        {

            Genclass.fieldone = "";
            Genclass.fieldtwo = "";
            Genclass.fieldthree = "";
            Genclass.fieldFour = "";
            Genclass.fieldFive = "";
            if (Genclass.Dtype == 90)
            {
                Genclass.type = 1;
                loadput();
            }





        }

        private void txtitemname_Click(object sender, EventArgs e)
        {


            if (txtpuid.Text == "")
            {
                MessageBox.Show("Select the Party");
                return;
            }
            if (str1key == "F2")
            {
                lkppnl.Visible = true;
                Genclass.Dtype = 2;
                txtscr11.Focus();
                if (Genclass.data1 == 1)
                {

                    Genclass.strsql = "select top 25  b.uid,ItemName,Itemcode,Price  from pur_price_list a inner join itemm b on a.itemuid=b.uid where a.suppuid=" + txtpuid.Text + "  and a.companyid=" + Genclass.data1 + " and b.tax <> '' or b.tax<>0  and eff_to is null";
                    Genclass.FSSQLSortStr = "itemcode";
                }
                else
                {

                    Genclass.strsql = "select top 25  b.uid,Itemcode,ItemName,Price  from pur_price_list a inner join itemm b on a.itemuid=b.uid where a.suppuid=" + txtpuid.Text + "  and a.companyid=" + Genclass.data1 + " and b.tax <> '' or b.tax<>0  and eff_to is null";
                    Genclass.FSSQLSortStr = "itemcode";
                }

            }

            else
            {
                lkppnl.Visible = true;
                txtscr11.Focus();
                Genclass.Dtype = 3;
                if (Genclass.data1 == 1)
                {
                    Genclass.strsql = "select  uid,ItemName,Itemcode  from Itemm where active=1  and companyid=" + Genclass.data1 + " and tax <> '' or tax<>0 ";
                    Genclass.FSSQLSortStr = "ItemName";
                }
                else
                {
                    Genclass.strsql = "select  uid,Itemcode,ItemName  from Itemm where active=1  and companyid=" + Genclass.data1 + "  and tax <> '' or tax<>0";
                    Genclass.FSSQLSortStr = "ItemName";
                }

            }






            Genclass.cmd = new SqlCommand(Genclass.strsql, conn);
            SqlDataAdapter aptr = new SqlDataAdapter(Genclass.cmd);
            DataTable tap = new DataTable();
            aptr.Fill(tap);
            bsc.DataSource = tap;



            HFGP2.AutoGenerateColumns = false;
            HFGP2.Refresh();
            HFGP2.DataSource = null;
            HFGP2.Rows.Clear();


            HFGP2.ColumnCount = tap.Columns.Count;
            Genclass.i = 0;

            foreach (DataColumn column in tap.Columns)
            {
                HFGP2.Columns[Genclass.i].Name = column.ColumnName;
                HFGP2.Columns[Genclass.i].HeaderText = column.ColumnName;
                HFGP2.Columns[Genclass.i].DataPropertyName = column.ColumnName;
                Genclass.i = Genclass.i + 1;
            }
            HFGP2.DataSource = tap;

            HFGP2.Columns[0].Visible = false;
            if (Genclass.data1 == 3)
            {
                HFGP2.Columns[1].Width = 201;
                HFGP2.Columns[2].Width = 300;
            }
            else
            {
                HFGP2.Columns[1].Width = 300;
                HFGP2.Columns[2].Width = 201;
                txtscr11.Width = 303;
                txtscr12.Width = 201;
                txtscr12.Location = new Point(308, 37);
            }
            if (str1key == "F2")
            {
                HFGP2.Columns[3].Width = 80;
            }

            //str1key = "F2";

            conn.Close();


        }
        private void txttdis_TextChanged_1(object sender, EventArgs e)
        {
            if (txttdis.Text == "")
            {
                txttdis.Text = "0";
            }
            if (txtcharges.Text == "")
            {
                txttdis.Text = "0";
            }


            if (txttbval.Text != "")
            {


                if (txttdis.Text == "" || txttdis.Text == "0")
                {
                    txttdisc.Text = "0";
                }

                if (txtcharges.Text == "")
                {
                    txtcharges.Text = "0";
                }


                //if (Convert.ToDouble(txttdis.Text) > 0)
                //{
                double dis = (Convert.ToDouble(txttbval.Text) / 100) * (Convert.ToDouble(txttdis.Text));
                txttdisc.Text = dis.ToString();
                double yt7 = Convert.ToDouble(txttbval.Text) - Convert.ToDouble(txttdisc.Text);
                txttprdval.Text = yt7.ToString();
                if (txtcharges.Text == "" || txtcharges.Text == null)
                {
                    txtexcise.Text = txttprdval.Text;
                }
                else
                {
                    dis4 = Convert.ToDouble(txttprdval.Text) + Convert.ToDouble(txtcharges.Text);
                    txtexcise.Text = dis4.ToString("0.00");
                }
                Genclass.sum5 = 0;
                double df = 0;
                HFGST.Refresh();
                HFGST.DataSource = null;
                HFGST.Rows.Clear();
                txtigval.Text = "0";
                txttaxtot.Text = "0";

                Titlegst();
                for (int l = 0; l < HFIT.RowCount - 1; l++)
                {

                    if (j != l)
                    {

                        dis4 = Convert.ToDouble(HFIT.Rows[l].Cells[10].Value);


                        dis3 = Convert.ToDouble(HFIT.Rows[l].Cells[4].Value) - (Convert.ToDouble(HFIT.Rows[l].Cells[4].Value) / 100) * (Convert.ToDouble(txttdis.Text));
                        dis3 = (dis3 / 100) * dis4;




                        if (txtstid.Text == "24")
                        {

                            if (mode == 2)
                            {
                                dis4 = Convert.ToDouble(HFIT.Rows[l].Cells[12].Value) * 2;
                                dis3 = Convert.ToDouble(HFIT.Rows[l].Cells[13].Value) * 2;
                            }
                            dis4 = dis4 / 2;
                            dis3 = dis3 / 2;

                            int boo = 1;
                            if (HFGST.Rows.Count - 1 == 0)
                            {

                                var index1 = HFGST.Rows.Add();
                                HFGST.Rows[index1].Cells[0].Value = dis4.ToString("0.00");
                                HFGST.Rows[index1].Cells[2].Value = dis4.ToString("0.00");
                                HFGST.Rows[index1].Cells[1].Value = dis3.ToString("0.00");
                                HFGST.Rows[index1].Cells[3].Value = dis3.ToString("0.00");
                                HFGST.Rows[index1].Cells[4].Value = Convert.ToDouble(dis3.ToString("0.00")) * 2;
                            }
                            else
                            {
                                for (int k = 0; k < HFGST.Rows.Count - 1; k++)
                                {
                                    if (Convert.ToDouble(HFGST.Rows[k].Cells[0].Value) == dis4)
                                    {

                                        double dg = Convert.ToDouble(HFGST.Rows[k].Cells[1].Value) + dis3;
                                        HFGST.Rows[k].Cells[1].Value = dg.ToString("0.00");

                                        HFGST.Rows[k].Cells[3].Value = dg.ToString("0.00");
                                        double dg1 = Convert.ToDouble(HFGST.Rows[k].Cells[4].Value) + (Convert.ToDouble(dis3.ToString("0.00")) * 2);
                                        HFGST.Rows[k].Cells[4].Value = dg1.ToString("0.00");
                                        boo = 1;
                                        break;
                                    }
                                    else
                                    {
                                        boo = 2;
                                    }
                                }
                            }

                            if (boo == 2)
                            {

                                var index1 = HFGST.Rows.Add();
                                HFGST.Rows[index1].Cells[0].Value = dis4.ToString("0.00");
                                HFGST.Rows[index1].Cells[2].Value = dis4.ToString("0.00");
                                HFGST.Rows[index1].Cells[1].Value = dis3.ToString("0.00");
                                HFGST.Rows[index1].Cells[3].Value = dis3.ToString("0.00");
                                HFGST.Rows[index1].Cells[4].Value = Convert.ToDouble(dis3.ToString("0.00")) * 2;
                            }

                            if (txtigval.Text == "")
                            {
                                txtigval.Text = "0";
                            }
                            df = Convert.ToDouble(txtigval.Text) + (Convert.ToDouble(dis3.ToString("0.00")) * 2);
                            txtigval.Text = df.ToString("0.00");
                            txttaxtot.Text = df.ToString("0.00");
                        }
                        else
                        {
                            int boo = 1;
                            if (HFGST.Rows.Count - 1 == 0)
                            {
                                var index1 = HFGST.Rows.Add();
                                HFGST.Rows[index1].Cells[0].Value = dis4.ToString("0.00");
                                HFGST.Rows[index1].Cells[1].Value = dis3.ToString("0.00");
                            }
                            else
                            {
                                for (int k = 0; k < HFGST.Rows.Count - 1; k++)
                                {
                                    if (Convert.ToDouble(HFGST.Rows[k].Cells[0].Value) == dis4)
                                    {

                                        double dg = Convert.ToDouble(HFGST.Rows[k].Cells[1].Value) + dis3;
                                        HFGST.Rows[k].Cells[1].Value = dg.ToString("0.00");


                                        boo = 1;
                                        break;
                                    }
                                    else
                                    {
                                        boo = 2;
                                    }
                                }
                            }

                            if (boo == 2)
                            {

                                var index1 = HFGST.Rows.Add();
                                HFGST.Rows[index1].Cells[0].Value = dis4.ToString("0.00");

                                HFGST.Rows[index1].Cells[1].Value = dis3.ToString("0.00");


                            }
                            if (txtigval.Text == "")
                            {
                                txtigval.Text = "0";
                            }
                            df = df + Convert.ToDouble(dis3.ToString("0.00"));
                            txtigval.Text = df.ToString("0.00");
                            txttaxtot.Text = df.ToString("0.00");
                        }
                    }


                    //txtigval.Text = hg.ToString("0.00");
                    //txttaxtot.Text = hg.ToString("0.00");
                    //double tot = Convert.ToDouble(txtexcise.Text) + Convert.ToDouble(txtigval.Text);
                    //txtttot.Text = tot.ToString("0.00");
                    double net1 = Convert.ToDouble(txtexcise.Text) + Convert.ToDouble(txtigval.Text) + Convert.ToDouble(txtcharges.Text);
                    txtttot.Text = net1.ToString("0.00");
                    TxtNetAmt.Text = net1.ToString("0.00");
                    double someInt = (int)net1;

                    double rof = Math.Round(net1 - someInt, 2);
                    TxtRoff.Text = rof.ToString("0.00");

                    if (Convert.ToDouble(TxtRoff.Text) < 0.49)
                    {
                        Double rof1 = -1 * Convert.ToDouble(TxtRoff.Text);
                        TxtRoff.Text = rof1.ToString("0.00");
                    }
                    else
                    {
                        Double rof2 = 1 - Convert.ToDouble(TxtRoff.Text);
                        TxtRoff.Text = rof2.ToString("0.00");
                    }

                    Double net = Convert.ToDouble(TxtNetAmt.Text) + Convert.ToDouble(TxtRoff.Text);
                    //int ne=Convert.ToInt16(net);
                    TxtNetAmt.Text = net.ToString("0.00");
                }
            }
        }

        private void txtcharges_TextChanged_1(object sender, EventArgs e)
        {
            if (txtcharges.Text != "")
            {
                if (txtcharges.Text == "" || txtcharges.Text == "0")
                {
                    txtcharges.Text = "0";
                }
                if (txttdisc.Text == "" || txttdisc.Text == "0")
                {
                    txttdisc.Text = "0";
                }
                //    double yt7 = Convert.ToDouble(txttbval.Text) - Convert.ToDouble(txttdisc.Text);
                //    txttprdval.Text = yt7.ToString();

                //    if (txtcharges.Text == "" || txtcharges.Text == null)
                //    {
                //        txtexcise.Text = txttprdval.Text;
                //    }
                //    else
                //    {
                //        dis4 = Convert.ToDouble(txttprdval.Text) + Convert.ToDouble(txtcharges.Text);
                //        txtexcise.Text = dis4.ToString("0.00");
                //    }

                //    Genclass.sum5 = 0;
                //    hg = 0;

                //    for (int l = 0; l < HFIT.RowCount - 1; l++)
                //    {
                //        for (int k = 0; k < HFGST.RowCount - 1; k++)
                //        {
                //            double tyu = Convert.ToDouble(HFGST.Rows[k].Cells[0].Value);
                //            double tyu1 = Convert.ToDouble(HFIT.Rows[l].Cells[10].Value);
                //            if (tyu1 == (tyu * 2))
                //            {
                //                Genclass.sum5 = (Convert.ToDouble(HFIT.Rows[l].Cells[4].Value) / 100) * (Convert.ToDouble(txttdis.Text));
                //                Genclass.sum4 = ((Convert.ToDouble(HFIT.Rows[l].Cells[4].Value) - (Genclass.sum5)) / 100) * Convert.ToDouble(HFGST.Rows[k].Cells[0].Value);
                //                HFGST.Rows[k].Cells[1].Value = Genclass.sum4.ToString("0.00");
                //                if (txtstid.Text == "24")
                //                {
                //                    HFGST.Rows[k].Cells[3].Value = Genclass.sum4.ToString("0.00");
                //                }
                //                if (txtstid.Text == "24")
                //                {
                //                    hg = hg + (Genclass.sum4 * 2);
                //                }

                //                else
                //                {
                //                    hg = hg + (Genclass.sum4);
                //                }
                //            }
                //        }
                //    }

                //    txtigval.Text = hg.ToString("0.00");
                //    double tot = Convert.ToDouble(txtexcise.Text) + Convert.ToDouble(txtigval.Text);
                //    txtttot.Text = tot.ToString("0.00");

                //    Double net1 = Convert.ToDouble(txtttot.Text);
                //    TxtNetAmt.Text = net1.ToString("0.00");
                //    double someInt = (int)net1;

                //    double rof = Math.Round(net1 - someInt, 2);
                //    TxtRoff.Text = rof.ToString("0.00");

                //    if (Convert.ToDouble(TxtRoff.Text) < 0.49)
                //    {
                //        Double rof1 = -1 * Convert.ToDouble(TxtRoff.Text);
                //        TxtRoff.Text = rof1.ToString("0.00");
                //    }
                //    else
                //    {
                //        Double rof2 = 1 - Convert.ToDouble(TxtRoff.Text);
                //        TxtRoff.Text = rof2.ToString("0.00");
                //    }

                //    Double net = Convert.ToDouble(TxtNetAmt.Text) + Convert.ToDouble(TxtRoff.Text);
                //    //int ne=Convert.ToInt16(net);
                //    TxtNetAmt.Text = net.ToString("0.00");

            }
        }

        private void txtname_MouseClick(object sender, MouseEventArgs e)
        {

            //if (e.Button == MouseButtons.Right)
            //{
            //    Genclass.fieldone = "";
            //    Genclass.fieldtwo = "";
            //    Genclass.fieldthree = "";
            //    Genclass.fieldFour = "";
            //    Genclass.fieldFive = "";
            //    if (Genclass.Dtype == 90)
            //    {

            //        Genclass.type = 1;
            //        loadput();
            //    }


            //}
            //else if (e.Button == MouseButtons.Left)
            //{
            //    Genclass.fieldone = "";
            //    Genclass.fieldtwo = "";
            //    Genclass.fieldthree = "";
            //    Genclass.fieldFour = "";
            //    Genclass.fieldFive = "";
            //    if (Genclass.Dtype == 90)
            //    {

            //        Genclass.type = 1;
            //        loadput();
            //    }




            //}
        }

        private void button13_Click(object sender, EventArgs e)
        {
            Genclass.cat = 3;
            conn.Close();
            conn.Open();
            qur.CommandText = "delete from  NotoWords";
            qur.ExecuteNonQuery();

            Int64 NumVal = Convert.ToInt64(HFGP.CurrentRow.Cells[6].Value);
            string Nw = Rupees(NumVal);

            qur.CommandText = "Insert into NotoWords values('" + Nw + "',1)";
            qur.ExecuteNonQuery();

            Genclass.Prtid = Convert.ToInt16(HFGP.CurrentRow.Cells[0].Value.ToString());


            Genclass.slno = 1;
            Crviewer crv = new Crviewer();
            crv.Show();



            conn.Close();
        }

        private void butedit_Click_1(object sender, EventArgs e)
        {
            btnedit_Click(sender, e);
            panadd.Visible = false;
            txtbval.Text = "";

        }

        private void txtscr11_TextChanged(object sender, EventArgs e)
        {
            if (Genclass.data1 == 3)
            {
                bsc.Filter = string.Format("itemcode LIKE '%{0}%'  and itemname LIKE '%{1}%'", txtscr11.Text, txtscr12.Text);
            }
            else
            {
                bsc.Filter = string.Format("itemname LIKE '%{0}%' and itemcode LIKE '%{1}%'", txtscr11.Text, txtscr12.Text);
            }
        }

        private void txtscr12_TextChanged(object sender, EventArgs e)
        {
            if (Genclass.data1 == 1)
            {
                bsc.Filter = string.Format("itemname LIKE '%{0}%' and itemcode LIKE '%{1}%'", txtscr12.Text, txtscr11.Text);
            }
            else
            {
                bsc.Filter = string.Format("itemcode LIKE '%{0}%'  and itemname LIKE '%{1}%'", txtscr12.Text, txtscr11.Text);
            }
        }

        private void HFGP2_Click(object sender, EventArgs e)
        {
            txtprice.Text = "";
            if (Genclass.data1 == 1)
            {
                if (str1key == "F2")
                {
                    txttitemid.Text = HFGP2.CurrentRow.Cells[0].Value.ToString();
                    txtitemname.Text = HFGP2.CurrentRow.Cells[1].Value.ToString();
                    txtprice.Text = HFGP2.CurrentRow.Cells[3].Value.ToString();

                }
                else
                {
                    txttitemid.Text = HFGP2.CurrentRow.Cells[0].Value.ToString();
                    txtitemname.Text = HFGP2.CurrentRow.Cells[1].Value.ToString();
                    txtprice.Focus();
                    txtqty.Focus();
                }

            }
            else
            {
                if (str1key == "F2")
                {
                    txttitemid.Text = HFGP2.CurrentRow.Cells[0].Value.ToString();
                    txtitemname.Text = HFGP2.CurrentRow.Cells[2].Value.ToString();
                    txtprice.Text = HFGP2.CurrentRow.Cells[3].Value.ToString();
                    txtqty.Focus();

                }
                else
                {
                    txttitemid.Text = HFGP2.CurrentRow.Cells[0].Value.ToString();
                    txtitemname.Text = HFGP2.CurrentRow.Cells[2].Value.ToString();
                    txtprice.Focus();
                }

            }

            //lkppnl.Visible = false;

        }

        private void txtscr11_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.F3)
            {
                if (str1key == "F3")
                {
                    Genclass.type = 2;
                    str1key = "F2";
                    label59.Text = "Mapped Items";
                    label60.Text = "Press F3 to Search from Items List";
                    txtitemname_Click(sender, e);
                }
                else
                {
                    Genclass.type = 3;
                    str1key = "F3";
                    label59.Text = "Items List";
                    label60.Text = "Press F3 to Search from Mapped Items";
                    txtitemname_Click(sender, e);
                }


            }
            else if (e.KeyCode == Keys.Escape)
            {
                lkppnl.Visible = false;

                txtitemname.Focus();

                return;

            }
            if (e.KeyValue == 40)
            {

                HFGP2.Select();
            }

        }

        private void HFGP2_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void HFIT_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void txtplace_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtplace_Click(object sender, EventArgs e)
        {
            Genclass.fieldone = "";
            Genclass.fieldtwo = "";
            Genclass.fieldthree = "";
            Genclass.fieldFour = "";
            Genclass.fieldFive = "";
            if (Genclass.Dtype == 90)
            {
                Genclass.type = 1;
                loadput();
            }


        }
    }
}

