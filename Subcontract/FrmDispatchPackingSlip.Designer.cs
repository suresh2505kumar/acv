﻿namespace ACV
{
    partial class FrmDispatchPackingSlip
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle27 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmDispatchPackingSlip));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle25 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle26 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle28 = new System.Windows.Forms.DataGridViewCellStyle();
            this.grBack = new System.Windows.Forms.GroupBox();
            this.CmbToSortNo = new System.Windows.Forms.ComboBox();
            this.label10 = new System.Windows.Forms.Label();
            this.txtTotalFrWght = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.txtTotalWeight = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txtTotalMeters = new System.Windows.Forms.TextBox();
            this.txtVechileNo = new System.Windows.Forms.TextBox();
            this.grSearch = new System.Windows.Forms.Panel();
            this.DataGridCommon = new System.Windows.Forms.DataGridView();
            this.btnSelect = new System.Windows.Forms.Button();
            this.btnHide = new System.Windows.Forms.Button();
            this.BtnAddBags = new System.Windows.Forms.Button();
            this.DataGridSortSummary = new System.Windows.Forms.DataGridView();
            this.DataGridSortRoll = new System.Windows.Forms.DataGridView();
            this.CmbSortNo = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.CmbPurpose = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.CmbTo = new System.Windows.Forms.ComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this.Phone = new System.Windows.Forms.Label();
            this.txtDocNo = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.dtpDocDate = new System.Windows.Forms.DateTimePicker();
            this.txtDispatchSlipNo = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.BtnReverse = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.panadd = new System.Windows.Forms.Panel();
            this.btnAdd = new System.Windows.Forms.Button();
            this.btnEdit = new System.Windows.Forms.Button();
            this.btnPackingSlipPrint = new System.Windows.Forms.Button();
            this.btnPrint = new System.Windows.Forms.Button();
            this.btnExit = new System.Windows.Forms.Button();
            this.btnDelete = new System.Windows.Forms.Button();
            this.btnsave = new System.Windows.Forms.Button();
            this.btnaddrcan = new System.Windows.Forms.Button();
            this.grFront = new System.Windows.Forms.GroupBox();
            this.txtSearch = new System.Windows.Forms.TextBox();
            this.DataGridDispatchPakSlip = new System.Windows.Forms.DataGridView();
            this.txtFrTotalMtr = new System.Windows.Forms.TextBox();
            this.ChckAll = new System.Windows.Forms.CheckBox();
            this.grBack.SuspendLayout();
            this.grSearch.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridCommon)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridSortSummary)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridSortRoll)).BeginInit();
            this.panadd.SuspendLayout();
            this.grFront.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridDispatchPakSlip)).BeginInit();
            this.SuspendLayout();
            // 
            // grBack
            // 
            this.grBack.Controls.Add(this.ChckAll);
            this.grBack.Controls.Add(this.txtFrTotalMtr);
            this.grBack.Controls.Add(this.label10);
            this.grBack.Controls.Add(this.txtTotalFrWght);
            this.grBack.Controls.Add(this.label9);
            this.grBack.Controls.Add(this.label6);
            this.grBack.Controls.Add(this.txtTotalWeight);
            this.grBack.Controls.Add(this.label5);
            this.grBack.Controls.Add(this.txtTotalMeters);
            this.grBack.Controls.Add(this.txtVechileNo);
            this.grBack.Controls.Add(this.grSearch);
            this.grBack.Controls.Add(this.BtnAddBags);
            this.grBack.Controls.Add(this.DataGridSortSummary);
            this.grBack.Controls.Add(this.DataGridSortRoll);
            this.grBack.Controls.Add(this.CmbSortNo);
            this.grBack.Controls.Add(this.label3);
            this.grBack.Controls.Add(this.CmbPurpose);
            this.grBack.Controls.Add(this.label1);
            this.grBack.Controls.Add(this.CmbTo);
            this.grBack.Controls.Add(this.label7);
            this.grBack.Controls.Add(this.Phone);
            this.grBack.Controls.Add(this.txtDocNo);
            this.grBack.Controls.Add(this.label2);
            this.grBack.Controls.Add(this.dtpDocDate);
            this.grBack.Controls.Add(this.txtDispatchSlipNo);
            this.grBack.Controls.Add(this.label8);
            this.grBack.Controls.Add(this.BtnReverse);
            this.grBack.Controls.Add(this.label4);
            this.grBack.Controls.Add(this.CmbToSortNo);
            this.grBack.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grBack.Location = new System.Drawing.Point(6, 0);
            this.grBack.Name = "grBack";
            this.grBack.Size = new System.Drawing.Size(1075, 561);
            this.grBack.TabIndex = 0;
            this.grBack.TabStop = false;
            // 
            // CmbToSortNo
            // 
            this.CmbToSortNo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.CmbToSortNo.FormattingEnabled = true;
            this.CmbToSortNo.Location = new System.Drawing.Point(420, 88);
            this.CmbToSortNo.Name = "CmbToSortNo";
            this.CmbToSortNo.Size = new System.Drawing.Size(264, 26);
            this.CmbToSortNo.TabIndex = 430;
            this.CmbToSortNo.Visible = false;
            this.CmbToSortNo.SelectedIndexChanged += new System.EventHandler(this.CmbToSortNo_SelectedIndexChanged);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.ForeColor = System.Drawing.Color.Red;
            this.label10.Location = new System.Drawing.Point(422, 530);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(306, 18);
            this.label10.TabIndex = 429;
            this.label10.Text = "Select Row and Press Delete Key to delete a row";
            // 
            // txtTotalFrWght
            // 
            this.txtTotalFrWght.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtTotalFrWght.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTotalFrWght.Location = new System.Drawing.Point(316, 527);
            this.txtTotalFrWght.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtTotalFrWght.Name = "txtTotalFrWght";
            this.txtTotalFrWght.Size = new System.Drawing.Size(98, 26);
            this.txtTotalFrWght.TabIndex = 428;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(73, 531);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(0, 18);
            this.label9.TabIndex = 427;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(981, 192);
            this.label6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(82, 18);
            this.label6.TabIndex = 425;
            this.label6.Text = "TotalWeight";
            // 
            // txtTotalWeight
            // 
            this.txtTotalWeight.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtTotalWeight.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTotalWeight.Location = new System.Drawing.Point(981, 215);
            this.txtTotalWeight.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtTotalWeight.Name = "txtTotalWeight";
            this.txtTotalWeight.Size = new System.Drawing.Size(90, 26);
            this.txtTotalWeight.TabIndex = 424;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(981, 126);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(85, 18);
            this.label5.TabIndex = 423;
            this.label5.Text = "Total Meters";
            // 
            // txtTotalMeters
            // 
            this.txtTotalMeters.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtTotalMeters.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTotalMeters.Location = new System.Drawing.Point(981, 149);
            this.txtTotalMeters.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtTotalMeters.Name = "txtTotalMeters";
            this.txtTotalMeters.Size = new System.Drawing.Size(90, 26);
            this.txtTotalMeters.TabIndex = 422;
            // 
            // txtVechileNo
            // 
            this.txtVechileNo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtVechileNo.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtVechileNo.Location = new System.Drawing.Point(923, 71);
            this.txtVechileNo.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtVechileNo.Name = "txtVechileNo";
            this.txtVechileNo.Size = new System.Drawing.Size(143, 26);
            this.txtVechileNo.TabIndex = 420;
            // 
            // grSearch
            // 
            this.grSearch.BackColor = System.Drawing.Color.White;
            this.grSearch.Controls.Add(this.DataGridCommon);
            this.grSearch.Controls.Add(this.btnSelect);
            this.grSearch.Controls.Add(this.btnHide);
            this.grSearch.Location = new System.Drawing.Point(442, 126);
            this.grSearch.Name = "grSearch";
            this.grSearch.Size = new System.Drawing.Size(382, 300);
            this.grSearch.TabIndex = 419;
            this.grSearch.Visible = false;
            // 
            // DataGridCommon
            // 
            this.DataGridCommon.AllowUserToAddRows = false;
            this.DataGridCommon.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.DataGridCommon.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DataGridCommon.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.DataGridCommon.Location = new System.Drawing.Point(4, 4);
            this.DataGridCommon.Name = "DataGridCommon";
            this.DataGridCommon.ReadOnly = true;
            this.DataGridCommon.RowHeadersVisible = false;
            dataGridViewCellStyle27.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.DataGridCommon.RowsDefaultCellStyle = dataGridViewCellStyle27;
            this.DataGridCommon.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.DataGridCommon.Size = new System.Drawing.Size(375, 264);
            this.DataGridCommon.TabIndex = 0;
            this.DataGridCommon.CellMouseDoubleClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.DataGridCommon_CellMouseDoubleClick);
            // 
            // btnSelect
            // 
            this.btnSelect.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnSelect.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSelect.Image = global::ACV.Properties.Resources.ok;
            this.btnSelect.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.btnSelect.Location = new System.Drawing.Point(175, 268);
            this.btnSelect.Name = "btnSelect";
            this.btnSelect.Size = new System.Drawing.Size(100, 28);
            this.btnSelect.TabIndex = 394;
            this.btnSelect.Text = "Select (F6)";
            this.btnSelect.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnSelect.UseVisualStyleBackColor = false;
            this.btnSelect.Click += new System.EventHandler(this.BtnSelect_Click);
            // 
            // btnHide
            // 
            this.btnHide.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnHide.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnHide.Image = ((System.Drawing.Image)(resources.GetObject("btnHide.Image")));
            this.btnHide.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnHide.Location = new System.Drawing.Point(277, 269);
            this.btnHide.Name = "btnHide";
            this.btnHide.Size = new System.Drawing.Size(100, 27);
            this.btnHide.TabIndex = 393;
            this.btnHide.Text = "Close (F7)";
            this.btnHide.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnHide.UseVisualStyleBackColor = false;
            this.btnHide.Click += new System.EventHandler(this.BtnHide_Click);
            // 
            // BtnAddBags
            // 
            this.BtnAddBags.Location = new System.Drawing.Point(322, 87);
            this.BtnAddBags.Name = "BtnAddBags";
            this.BtnAddBags.Size = new System.Drawing.Size(92, 26);
            this.BtnAddBags.TabIndex = 418;
            this.BtnAddBags.Text = "Add Bags";
            this.BtnAddBags.UseVisualStyleBackColor = true;
            this.BtnAddBags.Click += new System.EventHandler(this.BtnAddBags_Click);
            // 
            // DataGridSortSummary
            // 
            this.DataGridSortSummary.AllowUserToAddRows = false;
            this.DataGridSortSummary.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle25.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle25.BackColor = System.Drawing.Color.DeepSkyBlue;
            dataGridViewCellStyle25.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle25.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle25.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle25.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle25.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.DataGridSortSummary.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle25;
            this.DataGridSortSummary.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DataGridSortSummary.EnableHeadersVisualStyles = false;
            this.DataGridSortSummary.Location = new System.Drawing.Point(420, 116);
            this.DataGridSortSummary.Name = "DataGridSortSummary";
            this.DataGridSortSummary.RowHeadersVisible = false;
            this.DataGridSortSummary.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.DataGridSortSummary.Size = new System.Drawing.Size(558, 405);
            this.DataGridSortSummary.TabIndex = 417;
            this.DataGridSortSummary.KeyDown += new System.Windows.Forms.KeyEventHandler(this.DataGridSortSummary_KeyDown);
            // 
            // DataGridSortRoll
            // 
            this.DataGridSortRoll.AllowUserToAddRows = false;
            this.DataGridSortRoll.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle26.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle26.BackColor = System.Drawing.Color.DeepSkyBlue;
            dataGridViewCellStyle26.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle26.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle26.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle26.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle26.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.DataGridSortRoll.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle26;
            this.DataGridSortRoll.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DataGridSortRoll.EnableHeadersVisualStyles = false;
            this.DataGridSortRoll.Location = new System.Drawing.Point(10, 116);
            this.DataGridSortRoll.Name = "DataGridSortRoll";
            this.DataGridSortRoll.RowHeadersVisible = false;
            this.DataGridSortRoll.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.DataGridSortRoll.Size = new System.Drawing.Size(404, 405);
            this.DataGridSortRoll.TabIndex = 416;
            this.DataGridSortRoll.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.DataGridSortRoll_CellContentClick);
            this.DataGridSortRoll.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.DataGridSortRoll_CellValueChanged);
            // 
            // CmbSortNo
            // 
            this.CmbSortNo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.CmbSortNo.FormattingEnabled = true;
            this.CmbSortNo.Location = new System.Drawing.Point(10, 87);
            this.CmbSortNo.Name = "CmbSortNo";
            this.CmbSortNo.Size = new System.Drawing.Size(264, 26);
            this.CmbSortNo.TabIndex = 3;
            this.CmbSortNo.SelectedIndexChanged += new System.EventHandler(this.CmbSortNo_SelectedIndexChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(10, 68);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(54, 18);
            this.label3.TabIndex = 415;
            this.label3.Text = "Sort No";
            // 
            // CmbPurpose
            // 
            this.CmbPurpose.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.CmbPurpose.FormattingEnabled = true;
            this.CmbPurpose.Location = new System.Drawing.Point(826, 38);
            this.CmbPurpose.Name = "CmbPurpose";
            this.CmbPurpose.Size = new System.Drawing.Size(244, 26);
            this.CmbPurpose.TabIndex = 2;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(826, 19);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(59, 18);
            this.label1.TabIndex = 413;
            this.label1.Text = "Purpose";
            // 
            // CmbTo
            // 
            this.CmbTo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.CmbTo.FormattingEnabled = true;
            this.CmbTo.Location = new System.Drawing.Point(466, 38);
            this.CmbTo.Name = "CmbTo";
            this.CmbTo.Size = new System.Drawing.Size(358, 26);
            this.CmbTo.TabIndex = 1;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(466, 19);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(22, 18);
            this.label7.TabIndex = 411;
            this.label7.Text = "To";
            // 
            // Phone
            // 
            this.Phone.AutoSize = true;
            this.Phone.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Phone.Location = new System.Drawing.Point(131, 19);
            this.Phone.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.Phone.Name = "Phone";
            this.Phone.Size = new System.Drawing.Size(49, 18);
            this.Phone.TabIndex = 409;
            this.Phone.Text = "DocNo";
            // 
            // txtDocNo
            // 
            this.txtDocNo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtDocNo.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDocNo.Location = new System.Drawing.Point(131, 38);
            this.txtDocNo.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtDocNo.Name = "txtDocNo";
            this.txtDocNo.Size = new System.Drawing.Size(143, 26);
            this.txtDocNo.TabIndex = 408;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(7, 18);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(63, 18);
            this.label2.TabIndex = 407;
            this.label2.Text = "Doc Date";
            // 
            // dtpDocDate
            // 
            this.dtpDocDate.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpDocDate.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpDocDate.Location = new System.Drawing.Point(7, 38);
            this.dtpDocDate.Name = "dtpDocDate";
            this.dtpDocDate.Size = new System.Drawing.Size(123, 26);
            this.dtpDocDate.TabIndex = 404;
            // 
            // txtDispatchSlipNo
            // 
            this.txtDispatchSlipNo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtDispatchSlipNo.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDispatchSlipNo.Location = new System.Drawing.Point(275, 38);
            this.txtDispatchSlipNo.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtDispatchSlipNo.Name = "txtDispatchSlipNo";
            this.txtDispatchSlipNo.Size = new System.Drawing.Size(190, 26);
            this.txtDispatchSlipNo.TabIndex = 0;
            this.txtDispatchSlipNo.Click += new System.EventHandler(this.TxtDispatchSlipNo_Click);
            this.txtDispatchSlipNo.TextChanged += new System.EventHandler(this.TxtDispatchSlipNo_TextChanged);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(275, 16);
            this.label8.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(102, 18);
            this.label8.TabIndex = 406;
            this.label8.Text = "DispatchSlipNo";
            // 
            // BtnReverse
            // 
            this.BtnReverse.Location = new System.Drawing.Point(690, 87);
            this.BtnReverse.Name = "BtnReverse";
            this.BtnReverse.Size = new System.Drawing.Size(104, 26);
            this.BtnReverse.TabIndex = 426;
            this.BtnReverse.Text = "Remove Bags";
            this.BtnReverse.UseVisualStyleBackColor = true;
            this.BtnReverse.Visible = false;
            this.BtnReverse.Click += new System.EventHandler(this.BtnReverse_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(840, 75);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(75, 18);
            this.label4.TabIndex = 421;
            this.label4.Text = "Vechile No";
            // 
            // panadd
            // 
            this.panadd.BackColor = System.Drawing.Color.White;
            this.panadd.Controls.Add(this.btnAdd);
            this.panadd.Controls.Add(this.btnEdit);
            this.panadd.Controls.Add(this.btnPackingSlipPrint);
            this.panadd.Controls.Add(this.btnPrint);
            this.panadd.Controls.Add(this.btnExit);
            this.panadd.Controls.Add(this.btnDelete);
            this.panadd.Controls.Add(this.btnsave);
            this.panadd.Controls.Add(this.btnaddrcan);
            this.panadd.Location = new System.Drawing.Point(5, 567);
            this.panadd.Name = "panadd";
            this.panadd.Size = new System.Drawing.Size(1076, 34);
            this.panadd.TabIndex = 393;
            // 
            // btnAdd
            // 
            this.btnAdd.BackColor = System.Drawing.Color.White;
            this.btnAdd.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.btnAdd.FlatAppearance.BorderSize = 0;
            this.btnAdd.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAdd.Image = ((System.Drawing.Image)(resources.GetObject("btnAdd.Image")));
            this.btnAdd.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnAdd.Location = new System.Drawing.Point(803, 2);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(87, 30);
            this.btnAdd.TabIndex = 184;
            this.btnAdd.Text = "Add new";
            this.btnAdd.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnAdd.UseVisualStyleBackColor = false;
            this.btnAdd.Click += new System.EventHandler(this.BtnAdd_Click);
            // 
            // btnEdit
            // 
            this.btnEdit.BackColor = System.Drawing.Color.White;
            this.btnEdit.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnEdit.Image = ((System.Drawing.Image)(resources.GetObject("btnEdit.Image")));
            this.btnEdit.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnEdit.Location = new System.Drawing.Point(889, 2);
            this.btnEdit.Name = "btnEdit";
            this.btnEdit.Size = new System.Drawing.Size(58, 30);
            this.btnEdit.TabIndex = 185;
            this.btnEdit.Text = "Edit";
            this.btnEdit.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnEdit.UseVisualStyleBackColor = false;
            this.btnEdit.Visible = false;
            // 
            // btnPackingSlipPrint
            // 
            this.btnPackingSlipPrint.BackColor = System.Drawing.Color.White;
            this.btnPackingSlipPrint.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPackingSlipPrint.Image = ((System.Drawing.Image)(resources.GetObject("btnPackingSlipPrint.Image")));
            this.btnPackingSlipPrint.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnPackingSlipPrint.Location = new System.Drawing.Point(3, 2);
            this.btnPackingSlipPrint.Name = "btnPackingSlipPrint";
            this.btnPackingSlipPrint.Size = new System.Drawing.Size(108, 30);
            this.btnPackingSlipPrint.TabIndex = 216;
            this.btnPackingSlipPrint.Text = "Print Pk Slip";
            this.btnPackingSlipPrint.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnPackingSlipPrint.UseVisualStyleBackColor = false;
            this.btnPackingSlipPrint.Click += new System.EventHandler(this.BtnPackingSlipPrint_Click);
            // 
            // btnPrint
            // 
            this.btnPrint.BackColor = System.Drawing.Color.White;
            this.btnPrint.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPrint.Image = ((System.Drawing.Image)(resources.GetObject("btnPrint.Image")));
            this.btnPrint.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnPrint.Location = new System.Drawing.Point(3, 2);
            this.btnPrint.Name = "btnPrint";
            this.btnPrint.Size = new System.Drawing.Size(64, 30);
            this.btnPrint.TabIndex = 215;
            this.btnPrint.Text = "Print";
            this.btnPrint.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnPrint.UseVisualStyleBackColor = false;
            // 
            // btnExit
            // 
            this.btnExit.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnExit.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnExit.Image = ((System.Drawing.Image)(resources.GetObject("btnExit.Image")));
            this.btnExit.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnExit.Location = new System.Drawing.Point(1014, 2);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(57, 30);
            this.btnExit.TabIndex = 208;
            this.btnExit.Text = "Exit";
            this.btnExit.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnExit.UseVisualStyleBackColor = false;
            this.btnExit.Click += new System.EventHandler(this.BtnExit_Click);
            // 
            // btnDelete
            // 
            this.btnDelete.BackColor = System.Drawing.Color.White;
            this.btnDelete.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDelete.Image = ((System.Drawing.Image)(resources.GetObject("btnDelete.Image")));
            this.btnDelete.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnDelete.Location = new System.Drawing.Point(946, 2);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(69, 30);
            this.btnDelete.TabIndex = 186;
            this.btnDelete.Text = "Delete";
            this.btnDelete.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnDelete.UseVisualStyleBackColor = false;
            // 
            // btnsave
            // 
            this.btnsave.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnsave.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnsave.Image = ((System.Drawing.Image)(resources.GetObject("btnsave.Image")));
            this.btnsave.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnsave.Location = new System.Drawing.Point(933, 2);
            this.btnsave.Name = "btnsave";
            this.btnsave.Size = new System.Drawing.Size(71, 30);
            this.btnsave.TabIndex = 212;
            this.btnsave.Text = "Save";
            this.btnsave.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnsave.UseVisualStyleBackColor = false;
            this.btnsave.Click += new System.EventHandler(this.Btnsave_Click);
            // 
            // btnaddrcan
            // 
            this.btnaddrcan.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnaddrcan.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnaddrcan.Image = ((System.Drawing.Image)(resources.GetObject("btnaddrcan.Image")));
            this.btnaddrcan.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnaddrcan.Location = new System.Drawing.Point(1003, 3);
            this.btnaddrcan.Name = "btnaddrcan";
            this.btnaddrcan.Size = new System.Drawing.Size(64, 29);
            this.btnaddrcan.TabIndex = 213;
            this.btnaddrcan.Text = "Back";
            this.btnaddrcan.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnaddrcan.UseVisualStyleBackColor = false;
            this.btnaddrcan.Click += new System.EventHandler(this.Btnaddrcan_Click);
            // 
            // grFront
            // 
            this.grFront.Controls.Add(this.txtSearch);
            this.grFront.Controls.Add(this.DataGridDispatchPakSlip);
            this.grFront.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grFront.Location = new System.Drawing.Point(5, 2);
            this.grFront.Name = "grFront";
            this.grFront.Size = new System.Drawing.Size(1076, 558);
            this.grFront.TabIndex = 419;
            this.grFront.TabStop = false;
            // 
            // txtSearch
            // 
            this.txtSearch.Location = new System.Drawing.Point(6, 14);
            this.txtSearch.Name = "txtSearch";
            this.txtSearch.Size = new System.Drawing.Size(1065, 26);
            this.txtSearch.TabIndex = 1;
            // 
            // DataGridDispatchPakSlip
            // 
            this.DataGridDispatchPakSlip.AllowUserToAddRows = false;
            this.DataGridDispatchPakSlip.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle28.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle28.BackColor = System.Drawing.Color.DeepSkyBlue;
            dataGridViewCellStyle28.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle28.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle28.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle28.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle28.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.DataGridDispatchPakSlip.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle28;
            this.DataGridDispatchPakSlip.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DataGridDispatchPakSlip.EnableHeadersVisualStyles = false;
            this.DataGridDispatchPakSlip.Location = new System.Drawing.Point(6, 44);
            this.DataGridDispatchPakSlip.Name = "DataGridDispatchPakSlip";
            this.DataGridDispatchPakSlip.ReadOnly = true;
            this.DataGridDispatchPakSlip.RowHeadersVisible = false;
            this.DataGridDispatchPakSlip.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.DataGridDispatchPakSlip.Size = new System.Drawing.Size(1064, 505);
            this.DataGridDispatchPakSlip.TabIndex = 0;
            // 
            // txtFrTotalMtr
            // 
            this.txtFrTotalMtr.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtFrTotalMtr.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtFrTotalMtr.Location = new System.Drawing.Point(210, 527);
            this.txtFrTotalMtr.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtFrTotalMtr.Name = "txtFrTotalMtr";
            this.txtFrTotalMtr.Size = new System.Drawing.Size(105, 26);
            this.txtFrTotalMtr.TabIndex = 431;
            // 
            // ChckAll
            // 
            this.ChckAll.AutoSize = true;
            this.ChckAll.Location = new System.Drawing.Point(10, 529);
            this.ChckAll.Name = "ChckAll";
            this.ChckAll.Size = new System.Drawing.Size(85, 22);
            this.ChckAll.TabIndex = 432;
            this.ChckAll.Text = "Select All";
            this.ChckAll.UseVisualStyleBackColor = true;
            this.ChckAll.CheckedChanged += new System.EventHandler(this.ChckAll_CheckedChanged);
            // 
            // FrmDispatchPackingSlip
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.ClientSize = new System.Drawing.Size(1087, 604);
            this.Controls.Add(this.panadd);
            this.Controls.Add(this.grBack);
            this.Controls.Add(this.grFront);
            this.Name = "FrmDispatchPackingSlip";
            this.Text = "Dispatch Packing Slip";
            this.Load += new System.EventHandler(this.FrmDispatchPackingSlip_Load);
            this.grBack.ResumeLayout(false);
            this.grBack.PerformLayout();
            this.grSearch.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.DataGridCommon)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridSortSummary)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridSortRoll)).EndInit();
            this.panadd.ResumeLayout(false);
            this.grFront.ResumeLayout(false);
            this.grFront.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridDispatchPakSlip)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox grBack;
        private System.Windows.Forms.Panel panadd;
        private System.Windows.Forms.Button btnAdd;
        private System.Windows.Forms.Button btnEdit;
        private System.Windows.Forms.Button btnDelete;
        private System.Windows.Forms.Button btnPackingSlipPrint;
        private System.Windows.Forms.Button btnExit;
        private System.Windows.Forms.Button btnaddrcan;
        private System.Windows.Forms.Button btnsave;
        private System.Windows.Forms.DateTimePicker dtpDocDate;
        private System.Windows.Forms.TextBox txtDispatchSlipNo;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label Phone;
        private System.Windows.Forms.TextBox txtDocNo;
        private System.Windows.Forms.ComboBox CmbPurpose;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox CmbTo;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.ComboBox CmbSortNo;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.DataGridView DataGridSortSummary;
        private System.Windows.Forms.DataGridView DataGridSortRoll;
        private System.Windows.Forms.Button BtnAddBags;
        private System.Windows.Forms.GroupBox grFront;
        private System.Windows.Forms.TextBox txtSearch;
        private System.Windows.Forms.DataGridView DataGridDispatchPakSlip;
        private System.Windows.Forms.Panel grSearch;
        private System.Windows.Forms.DataGridView DataGridCommon;
        private System.Windows.Forms.Button btnSelect;
        private System.Windows.Forms.Button btnHide;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtVechileNo;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtTotalWeight;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtTotalMeters;
        private System.Windows.Forms.Button BtnReverse;
        private System.Windows.Forms.Button btnPrint;
        private System.Windows.Forms.TextBox txtTotalFrWght;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.ComboBox CmbToSortNo;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox txtFrTotalMtr;
        private System.Windows.Forms.CheckBox ChckAll;
    }
}