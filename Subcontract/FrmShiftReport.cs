﻿using CrystalDecisions.CrystalReports.Engine;
using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Net.NetworkInformation;
using System.Security.Cryptography;
using System.Text;
using System.Windows.Forms;

namespace ACV
{
    public partial class FrmShiftReport : Form
    {
        public FrmShiftReport()
        {
            InitializeComponent();
        }
        SQLDBHelper db = new SQLDBHelper();
        DataTable dtPublic = new DataTable();
        SqlConnection connImport = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnStrIMport"].ConnectionString);
        private void radioRollWise_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                if (radioRollWise.Checked == true)
                {
                    Genclass.ReportType = "RollWise";
                    radioSortWise.Checked = false;
                    radioLoomWise.Checked = false;
                }
                else if (radioSortWise.Checked == true)
                {
                    Genclass.ReportType = "SortWise";
                    radioRollWise.Checked = false;
                    radioLoomWise.Checked = false;
                }
                else
                {
                    Genclass.ReportType = "LoomWise";
                    radioRollWise.Checked = false;
                    radioSortWise.Checked = false;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }
        protected void LoadShift()
        {
            try
            {
                SqlConnection conn = new SqlConnection("Data Source=" + Genclass.data2 + "; Initial Catalog=" + Genclass.data5 + ";User id=" + Genclass.data3 + ";Password=" + Genclass.data4 + "");
                SqlParameter[] para = {
                    new SqlParameter("@TypeM_Uid",27),
                    new SqlParameter("@Active",1)
                };
                DataTable dt = db.GetDataWithParam(CommandType.StoredProcedure, "SP_GetGeneralM", para, conn);
                cmbShift.DataSource = null;
                cmbShift.DisplayMember = "GeneralName";
                cmbShift.ValueMember = "Uid";
                cmbShift.DataSource = dt;
                DataRow row = dt.NewRow();
                row["GeneralName"] = "All";
                row["Uid"] = 0;
                dt.Rows.InsertAt(row, 0);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void FrmShiftReport_Load(object sender, EventArgs e)
        {

            Genclass.data1 = Convert.ToInt16(Decrypt(ConfigurationManager.AppSettings["Data1"]));
            Genclass.data2 = Decrypt(ConfigurationManager.AppSettings["Data2"]);
            Genclass.data3 = Decrypt(ConfigurationManager.AppSettings["Data3"]);
            Genclass.data4 = Decrypt(ConfigurationManager.AppSettings["Data4"]);
            Genclass.data5 = Decrypt(ConfigurationManager.AppSettings["Data5"]);
            Genclass.data6 = Decrypt(ConfigurationManager.AppSettings["Data6"]);
            LoadShift();
            dtpDate.Format = DateTimePickerFormat.Custom;
            dtpDate.CustomFormat = "dd/MMM/yyyy";
        }

        private void button3_Click(object sender, EventArgs e)
        {
            try
            {
                if(radioLoomWise.Checked == true)
                {
                    DataTable dt = new DataTable();
                    dt.Columns.Add("LOOMNO", typeof(string));
                    dt.Columns.Add("SORTNO", typeof(string));
                    dt.Columns.Add("BEAMNO", typeof(string));
                    dt.Columns.Add("ROLLS", typeof(string));
                    dt.Columns.Add("NETWT", typeof(string));
                    dt.Columns.Add("MTRS", typeof(string));
                    for (int i = 0; i < DataGridShiftReport.Rows.Count; i++)
                    {
                        DataRow row = dt.NewRow();
                        row["LOOMNO"] = DataGridShiftReport.Rows[i].Cells[0].Value;
                        row["SORTNO"] = DataGridShiftReport.Rows[i].Cells[1].Value;
                        row["BEAMNO"] = DataGridShiftReport.Rows[i].Cells[2].Value;
                        row["ROLLS"] = DataGridShiftReport.Rows[i].Cells[3].Value;
                        row["NETWT"] = DataGridShiftReport.Rows[i].Cells[4].Value;
                        row["MTRS"] = DataGridShiftReport.Rows[i].Cells[5].Value;
                        dt.Rows.Add(row);
                    }
                    Genclass.dtLoom = dt;
                    Genclass.Dtype =5;
                    Genclass.ReportType = "LoomWise";
                    Genclass.ReortDate = Convert.ToDateTime(dtpDate.Text);
                    Genclass.RpeortId = Convert.ToInt32(cmbShift.SelectedValue);
                    FrmReprtViwer rpt = new FrmReprtViwer();
                    rpt.MdiParent = this.MdiParent;
                    rpt.StartPosition = FormStartPosition.CenterScreen;
                    rpt.Show();
                }
                else if(radioRollWise.Checked == true)
                {
                    Genclass.Dtype = 1;
                    Genclass.ReportType = "RollWise";
                    Genclass.ReortDate = Convert.ToDateTime(dtpDate.Text);
                    Genclass.RpeortId = Convert.ToInt32(cmbShift.SelectedValue);
                    FrmReprtViwer rpt = new FrmReprtViwer();
                    rpt.MdiParent = this.MdiParent;
                    rpt.StartPosition = FormStartPosition.CenterScreen;
                    rpt.Show();
                }
                else
                {
                    Genclass.Dtype = 6;
                    Genclass.ReportType = "SortWise";
                    Genclass.ReortDate = Convert.ToDateTime(dtpDate.Text);
                    Genclass.RpeortId = Convert.ToInt32(cmbShift.SelectedValue);
                    FrmReprtViwer rpt = new FrmReprtViwer();
                    rpt.MdiParent = this.MdiParent;
                    rpt.StartPosition = FormStartPosition.CenterScreen;
                    rpt.Show();
                }
                
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                SqlConnection conn1 = new SqlConnection("Data Source=" + Genclass.data2 + "; Initial Catalog=" + Genclass.data5 + ";User id=" + Genclass.data3 + ";Password=" + Genclass.data4 + "");
                SqlParameter[] para = {
                    new SqlParameter("@DT",Convert.ToDateTime(dtpDate.Text).ToString("yyyy-MM-dd")),
                    new SqlParameter("@SHIFTID",cmbShift.SelectedValue)
                };
                string ProcedureName = string.Empty;
                if (radioRollWise.Checked == true)
                {
                    ProcedureName = "RPT_SHIFTREPORTROLLWISE";
                }
                else if (radioSortWise.Checked == true)
                {
                    ProcedureName = "RPT_SHIFTREPORTSORTWISE";
                }
                else if (radioLoomWise.Checked == true)
                {
                    ProcedureName = "RPT_SHIFTREPORTLOOMWISE";
                }
                else
                {
                    MessageBox.Show("Select Report Type", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }
                string Query = "Select* from ImportPickData where Cast (Dt as date) ='" + Convert.ToDateTime(dtpDate.Text).ToString("yyyy-MM-dd") + "' and ShiftName ='" + cmbShift.SelectedValue + "'";
                SqlCommand cmd = new SqlCommand(Query, conn1);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dtv = new DataTable();
                da.Fill(dtv);
                if(dtv.Rows.Count == 0)
                {
                    GetImportData();
                }
                DataTable dt = db.GetDataWithParam(CommandType.StoredProcedure, ProcedureName, para, conn1);
                dtPublic = dt;
                if (radioRollWise.Checked == true)
                {
                    DataGridShiftReport.DataSource = null;
                    DataGridShiftReport.AutoGenerateColumns = false;
                    DataGridShiftReport.ColumnCount = 7;
                    DataGridShiftReport.Columns[0].Name = "RollNo";
                    DataGridShiftReport.Columns[0].HeaderText = "Roll No";
                    DataGridShiftReport.Columns[0].DataPropertyName = "RNO";
                    DataGridShiftReport.Columns[0].Width = 80;
                    DataGridShiftReport.Columns[1].Name = "LOOMNO";
                    DataGridShiftReport.Columns[1].HeaderText = "LOOM NO";
                    DataGridShiftReport.Columns[1].DataPropertyName = "LOOMNO";
                    DataGridShiftReport.Columns[1].Width = 80;
                    DataGridShiftReport.Columns[2].Name = "SortNo";
                    DataGridShiftReport.Columns[2].HeaderText = "SortNo";
                    DataGridShiftReport.Columns[2].DataPropertyName = "SortNo";
                    DataGridShiftReport.Columns[2].Width = 150;
                    DataGridShiftReport.Columns[3].Name = "NetWt";
                    DataGridShiftReport.Columns[3].HeaderText = "NetWt";
                    DataGridShiftReport.Columns[3].DataPropertyName = "NETWT";
                    DataGridShiftReport.Columns[3].Width = 90;
                    DataGridShiftReport.Columns[4].Name = "CMTRS";
                    DataGridShiftReport.Columns[4].HeaderText = "CMTRS";
                    DataGridShiftReport.Columns[4].DataPropertyName = "CMTRS";
                    DataGridShiftReport.Columns[4].Width = 80;
                    DataGridShiftReport.Columns[5].Name = "WTMTR";
                    DataGridShiftReport.Columns[5].HeaderText = "WTMTR";
                    DataGridShiftReport.Columns[5].DataPropertyName = "WTMTR";
                    DataGridShiftReport.Columns[5].Width = 60;
                    DataGridShiftReport.Columns[6].Name = "Time";
                    DataGridShiftReport.Columns[6].HeaderText = "Time";
                    DataGridShiftReport.Columns[6].DataPropertyName = "CTIME";
                    DataGridShiftReport.Columns[6].Width = 99;
                    DataGridShiftReport.DataSource = dt;
                }
                else if (radioSortWise.Checked == true)
                {
                    DataGridShiftReport.DataSource = null;
                    DataGridShiftReport.AutoGenerateColumns = false;
                    DataGridShiftReport.ColumnCount = 4;
                    DataGridShiftReport.Columns[0].Name = "SORTNO";
                    DataGridShiftReport.Columns[0].HeaderText = "SORTNO";
                    DataGridShiftReport.Columns[0].DataPropertyName = "SORTNO";
                    DataGridShiftReport.Columns[0].Width = 200;
                    DataGridShiftReport.Columns[1].Name = "ROLLS";
                    DataGridShiftReport.Columns[1].HeaderText = "ROLLS";
                    DataGridShiftReport.Columns[1].DataPropertyName = "ROLLS";
                    DataGridShiftReport.Columns[1].Width = 150;
                    DataGridShiftReport.Columns[2].Name = "NETWT";
                    DataGridShiftReport.Columns[2].HeaderText = "NETWT";
                    DataGridShiftReport.Columns[2].DataPropertyName = "NETWT";
                    DataGridShiftReport.Columns[2].Width = 145;
                    DataGridShiftReport.Columns[3].Name = "MTRS";
                    DataGridShiftReport.Columns[3].HeaderText = "MTRS";
                    DataGridShiftReport.Columns[3].DataPropertyName = "MTRS";
                    DataGridShiftReport.Columns[3].Width = 144;
                    DataGridShiftReport.DataSource = dt;
                }
                else if (radioLoomWise.Checked == true)
                {
                    DataGridShiftReport.DataSource = null;
                    DataGridShiftReport.AutoGenerateColumns = false;
                    DataGridShiftReport.ColumnCount = 6;
                    DataGridShiftReport.Columns[0].Name = "LOOMNO";
                    DataGridShiftReport.Columns[0].HeaderText = "LOOMNO";
                    DataGridShiftReport.Columns[0].DataPropertyName = "LOOMNO";

                    DataGridShiftReport.Columns[1].Name = "SORTNO";
                    DataGridShiftReport.Columns[1].HeaderText = "SORTNO";
                    DataGridShiftReport.Columns[1].DataPropertyName = "SORTNO";
                    DataGridShiftReport.Columns[1].Width = 138;

                    DataGridShiftReport.Columns[2].Name = "BeamNo";
                    DataGridShiftReport.Columns[2].HeaderText = "BEAMNO";
                    DataGridShiftReport.Columns[2].DataPropertyName = "BEAMNO";
                    DataGridShiftReport.Columns[2].Width = 140;

                    DataGridShiftReport.Columns[3].Name = "ROLLS";
                    DataGridShiftReport.Columns[3].HeaderText = "ROLLS";
                    DataGridShiftReport.Columns[3].DataPropertyName = "ROLLS";
                    DataGridShiftReport.Columns[3].Width = 60;

                    DataGridShiftReport.Columns[4].Name = "NETWT";
                    DataGridShiftReport.Columns[4].HeaderText = "NETWT";
                    DataGridShiftReport.Columns[4].DataPropertyName = "NETWT";

                    DataGridShiftReport.Columns[5].Name = "MTRS";
                    DataGridShiftReport.Columns[5].HeaderText = "MTRS";
                    DataGridShiftReport.Columns[5].DataPropertyName = "MTRS";
                    DataGridShiftReport.DataSource = dt;
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void GetImportData()
        {
            SqlConnection conn = new SqlConnection("Data Source=" + Genclass.data2 + "; Initial Catalog=" + Genclass.data5 + ";User id=" + Genclass.data3 + ";Password=" + Genclass.data4 + "");
            int ShiftIdW = 0;
            DateTime dte = Convert.ToDateTime(dtpDate.Text);
            string Month = dte.Month.ToString("00");
            int Year = dte.Year;
            int ShiftId = Convert.ToInt32(cmbShift.SelectedValue);
            if (cmbShift.SelectedValue.ToString() == "984")
            {
                ShiftId = 1;
            }
            else if (cmbShift.SelectedValue.ToString() == "985")
            {
                ShiftId = 2;
            }
            else if (cmbShift.SelectedValue.ToString() == "986")
            {
                ShiftId = 3;
            }
            string day = dte.Day.ToString("00");
            string Query = "select sdate,shift,mcno,SUM(picks) as picks from PROD_" + Month + Year + " where SDATE = '" + Year + Month + day + "' and shift = " + ShiftId + " group by sdate,shift,mcno";
            DataTable dt = db.GetDataWithoutParam(CommandType.Text, Query, connImport);
            DataTable dtServer = new DataTable();
            dtServer.Columns.Add("PickUid", typeof(int));
            dtServer.Columns.Add("Dt", typeof(DateTime));
            dtServer.Columns.Add("Shiftname", typeof(string));
            dtServer.Columns.Add("LNO", typeof(int));
            dtServer.Columns.Add("Kpicks", typeof(decimal));
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                DataRow row = dtServer.NewRow();
                row["PickUid"] = i + 1;
                row["Dt"] = Convert.ToDateTime(dt.Rows[i]["sdate"].ToString());
                ShiftIdW = Convert.ToInt32(cmbShift.SelectedValue);
                //if (cmbShift.Text == "1")
                //{
                //    ShiftIdW = 984;
                //}
                //else if (cmbShift.Text == "2")
                //{
                //    ShiftIdW = 985;
                //}
                //else if (cmbShift.Text == "3")
                //{
                //    ShiftIdW = 986;
                //}
                row["Shiftname"] = dt.Rows[i]["shift"].ToString(); ;
                row["Kpicks"] = Convert.ToDecimal(dt.Rows[i]["picks"].ToString());
                row["LNO"] = Convert.ToInt32(dt.Rows[i]["mcno"].ToString());
                dtServer.Rows.Add(row);
            }
            SqlParameter[] paradelet = {
                    new SqlParameter("@Date",Convert.ToDateTime(dtpDate.Text).ToString("yyyy-MM-dd")),
                    new SqlParameter("@ShifId",ShiftIdW)
                };
            db.ExecuteNonQuery(CommandType.StoredProcedure, "SP_DeleteImportPickData", paradelet, conn);
            using (SqlBulkCopy bulkCopy = new SqlBulkCopy(conn))
            {
                conn.Open();
                bulkCopy.DestinationTableName = "dbo.ImportPickData";
                bulkCopy.WriteToServer(dtServer);
                //MessageBox.Show("Data Exoprted To Sql Server Succefully", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                conn.Close();
            }
            db.ExecuteNonQuery(CommandType.StoredProcedure, "SP_UpdateImportPickdata", conn);
        }

        public static string Decrypt(string cipherText)
        {
            string EncryptionKey = "myla123";
            cipherText = cipherText.Replace(" ", "+");
            byte[] cipherBytes = Convert.FromBase64String(cipherText);
            using (Aes encryptor = Aes.Create())
            {
                Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes(EncryptionKey, new byte[] { 0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 });
                encryptor.Key = pdb.GetBytes(32);
                encryptor.IV = pdb.GetBytes(16);
                using (MemoryStream ms = new MemoryStream())
                {
                    using (CryptoStream cs = new CryptoStream(ms, encryptor.CreateDecryptor(), CryptoStreamMode.Write))
                    {
                        cs.Write(cipherBytes, 0, cipherBytes.Length);
                        cs.Close();
                    }
                    cipherText = Encoding.Unicode.GetString(ms.ToArray());
                }
            }
            return cipherText;
        }

        private void cmbShift_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cmbShift.SelectedIndex != -1)
            {
                //button1_Click(sender, e);
            }
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            bool connection = NetworkInterface.GetIsNetworkAvailable();
            if (connection == true)
            {
            }
            else
            {
                MessageBox.Show("not available");
            }
        }

        private void btnExport_Click(object sender, EventArgs e)
        {
            GetImportData();
            btnView.Enabled = true;
        }

        private void btnDownloadExcel_Click(object sender, EventArgs e)
        {
            try
            {
                Microsoft.Office.Interop.Excel._Application app = new Microsoft.Office.Interop.Excel.Application();
                Microsoft.Office.Interop.Excel._Workbook workbook = app.Workbooks.Open(Application.StartupPath + "/ShitwiseReport.xls", ReadOnly: false, Editable: true);
                Microsoft.Office.Interop.Excel._Worksheet worksheet = workbook.Worksheets.Item[1] as Microsoft.Office.Interop.Excel.Worksheet;
                app.Visible = true;
                worksheet = workbook.Sheets["Sheet1"];

                Microsoft.Office.Interop.Excel.Range row1 = worksheet.Rows.Cells[1, 1];
                Microsoft.Office.Interop.Excel.Range row2 = worksheet.Rows.Cells[2, 2];
                Microsoft.Office.Interop.Excel.Range row3 = worksheet.Rows.Cells[2, 9];
                row2.Value = Convert.ToDateTime(dtpDate.Text).ToString("dd-MMM-yyyy");
                row3.Value = cmbShift.Text;
                int j = 4;
                for (int i = 0; i < dtPublic.Rows.Count; i++)
                {
                    Microsoft.Office.Interop.Excel.Range k = worksheet.Rows.Cells[j, 1];
                    Microsoft.Office.Interop.Excel.Range L = worksheet.Rows.Cells[j, 2];
                    Microsoft.Office.Interop.Excel.Range M = worksheet.Rows.Cells[j, 3];
                    Microsoft.Office.Interop.Excel.Range N = worksheet.Rows.Cells[j, 4];
                    Microsoft.Office.Interop.Excel.Range o = worksheet.Rows.Cells[j, 5];
                    Microsoft.Office.Interop.Excel.Range p = worksheet.Rows.Cells[j, 6];
                    Microsoft.Office.Interop.Excel.Range q = worksheet.Rows.Cells[j, 7];
                    Microsoft.Office.Interop.Excel.Range r = worksheet.Rows.Cells[j, 8];
                    Microsoft.Office.Interop.Excel.Range s = worksheet.Rows.Cells[j, 9];
                    Microsoft.Office.Interop.Excel.Range t = worksheet.Rows.Cells[j, 10];

                    k.Value = i + 1;
                    L.Value = dtPublic.Rows[i]["RNO"].ToString();
                    M.Value = dtPublic.Rows[i]["LOOMNO"].ToString();
                    N.Value = dtPublic.Rows[i]["SORTNO"].ToString();
                    o.Value = dtPublic.Rows[i]["WTMTR"].ToString();
                    p.Value = dtPublic.Rows[i]["NETWT"].ToString();
                    q.Value = dtPublic.Rows[i]["CMTRS"].ToString();
                    r.Value = dtPublic.Rows[i]["MTR"].ToString();
                    s.Value = dtPublic.Rows[i]["CWT"].ToString();
                    t.Value = dtPublic.Rows[i]["CTIME"].ToString();
                    j += 1;
                }
                MessageBox.Show("Exported Successfully", "Infromation", MessageBoxButtons.OK, MessageBoxIcon.Information);
                workbook.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void btnReconciliation_Click(object sender, EventArgs e)
        {
            try
            {
                SqlConnection conn = new SqlConnection("Data Source=" + Genclass.data2 + "; Initial Catalog=" + Genclass.data5 + ";User id=" + Genclass.data3 + ";Password=" + Genclass.data4 + "");
                if (cmbShift.SelectedValue.ToString() == "0")
                {
                    MessageBox.Show("select Shift", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }
                else
                {
                    string Query = "select id from sortdetail where completedon is not null and Cast(cdate as Date) = '" + Convert.ToDateTime(dtpDate.Text) + "' and completedshifttype =" + cmbShift.SelectedValue + "";
                    DataTable dt = db.GetDataWithoutParam(CommandType.Text, Query, conn);
                    if(dt.Rows.Count != 0)
                    {
                        for (int i = 0; i < dt.Rows.Count; i++)
                        {
                            SqlParameter[] para = {
                                new SqlParameter("@Uid",Convert.ToInt32(dt.Rows[i]["id"].ToString()))
                            };
                            db.ExecuteNonQuery(CommandType.StoredProcedure, "UpdateTotalPick_SP", para,conn);
                        }
                        MessageBox.Show("Completed Successfully", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    else
                    {

                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }
    }
}
