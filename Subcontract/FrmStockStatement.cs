﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace ACV
{
    public partial class FrmStockStatement : Form
    {
        public FrmStockStatement()
        {
            InitializeComponent();
        }

        private void BtnPrint_Click(object sender, EventArgs e)
        {
            try
            {
                Genclass.Dtype = 14;
                Genclass.FromDate = Convert.ToDateTime(DtpFromDate.Text);
                Genclass.ToDate = Convert.ToDateTime(DtpToDate.Text);
                FrmReprtViwer frmReprtViwer = new FrmReprtViwer();
                frmReprtViwer.MdiParent = this.MdiParent;
                frmReprtViwer.StartPosition = FormStartPosition.CenterScreen;
                frmReprtViwer.Show();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }
    }
}
