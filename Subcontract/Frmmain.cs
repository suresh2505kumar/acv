﻿using System;
using System.Data;
using System.Windows.Forms;
using System.IO;
using System.Data.SqlClient;
using excel = Microsoft.Office.Interop.Excel;
using System.Diagnostics;

namespace ACV
{
    public partial class FrmMain : Form
    {

        public void Partylistviewcont(String name, String uid, String str, Form whatform, TextBox whatfldone, TextBox whatfldtwo)
        {
            Genclass.fieldone = whatfldone.Name;
            Genclass.fieldtwo = whatfldtwo.Name;
            Genclass.sql = whatform.Name;

            Frmlookup contc = new Frmlookup();
            DataGridView dt = (DataGridView)contc.Controls["dataGridView1"];
            dt.Refresh();
            contc.Show();
        }

        public FrmMain()
        {
            InitializeComponent();
            this.FormClosing += Frmmain_FormClosing;
        }

        void Frmmain_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (e.CloseReason == CloseReason.UserClosing)
            {
                DialogResult result = MessageBox.Show("Do you really want to exit?", "Closing", MessageBoxButtons.YesNo, MessageBoxIcon.Stop);
                if (result == DialogResult.Yes)
                {
                    Process.GetCurrentProcess().Kill();
                }
                else
                {
                    e.Cancel = true;
                }
            }
            else
            {
                e.Cancel = true;
            }
        }

        private static excel.Workbook mWorkBook;
        private static excel.Sheets mWorkSheets;
        private static excel.Worksheet mWSheet1;
        private static excel.Application oXL;
        SqlConnection conn = new SqlConnection("Data Source=" + Genclass.data2 + "; Initial Catalog=" + Genclass.data5 + ";User id=" + Genclass.data3 + ";Password=" + Genclass.data4 + "");

        SqlCommand qur = new SqlCommand();

        private void ShowNewForm(object sender, EventArgs e)
        {
            Genclass.Gbtxtid = 1;
            Genclass.Genlbl = "Item Group Name";
            Frmitemgrp contc = new Frmitemgrp();
            contc.MdiParent = this;
            contc.Show();

        }
        protected override void OnResize(EventArgs e)
        {
            CenterForms();
            base.OnResize(e);
        }
        private void CenterForms()
        {
            foreach (var form in MdiChildren)
            {
                form.Left = (ClientRectangle.Width - form.Width) / 3;
                form.Top = (ClientRectangle.Height - form.Height) / 3;
            }

        }

        private void ExitToolsStripMenuItem_Click(object sender, EventArgs e)
        {
            Genclass.Gbtxtid = 5;
            Genclass.Genlbl = "Process Name";
            Frmitemgrp contc = new Frmitemgrp();
            contc.MdiParent = this;
            contc.Show();
        }

        private void CutToolStripMenuItem_Click(object sender, EventArgs e)
        {
        }

        private void CopyToolStripMenuItem_Click(object sender, EventArgs e)
        {
        }

        private void PasteToolStripMenuItem_Click(object sender, EventArgs e)
        {
        }

        private void ToolBarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //toolStrip.Visible = toolBarToolStripMenuItem.Checked;
        }

        private void StatusBarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //statusStrip.Visible = statusBarToolStripMenuItem.Checked;
        }

        private void CascadeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            LayoutMdi(MdiLayout.Cascade);
        }

        private void TileVerticalToolStripMenuItem_Click(object sender, EventArgs e)
        {
            LayoutMdi(MdiLayout.TileVertical);
        }

        private void TileHorizontalToolStripMenuItem_Click(object sender, EventArgs e)
        {
            LayoutMdi(MdiLayout.TileHorizontal);
        }

        private void ArrangeIconsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            LayoutMdi(MdiLayout.ArrangeIcons);
        }

        private void CloseAllToolStripMenuItem_Click(object sender, EventArgs e)
        {
            foreach (Form childForm in MdiChildren)
            {
                childForm.Close();
            }
        }



        private void Mnu_item_Click(object sender, EventArgs e)
        {
            Frmitem contc = new Frmitem();
            contc.MdiParent = this;
            contc.Show();
        }

        private void Frmmain_Load(object sender, EventArgs e)
        {

            MdiClient ctlmdi;
            foreach (Control ctl in this.Controls)
            {
                try
                {
                    ctlmdi = (MdiClient)ctl;
                    ctlmdi.BackColor = this.BackColor;
                }
                catch (InvalidCastException)
                {
                }
            }

        }

        private void mnu_subgrp_Click(object sender, EventArgs e)
        {
            Genclass.Gbtxtid = 2;
            Genclass.Genlbl = "Brand";
            Frmitemgrp contc = new Frmitemgrp();
            contc.MdiParent = this;
            contc.Show();
        }

        private void mnu_cat_Click(object sender, EventArgs e)
        {
            Genclass.Gbtxtid = 3;
            Genclass.Genlbl = "Category Type";
            Frmitemgrp contc = new Frmitemgrp();
            contc.MdiParent = this;
            contc.Show();
        }

        private void Mnu_uom_Click(object sender, EventArgs e)
        {
            Genclass.Gbtxtid = 4;
            Genclass.Genlbl = "UoM";
            Frmitemgrp contc = new Frmitemgrp();
            contc.MdiParent = this;
            contc.Show();
        }

        private void Mnu_grn_Click(object sender, EventArgs e)
        {

        }

        private void exitToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            Genclass.Gbtxtid = 7;
            Frmitemgrp contc = new Frmitemgrp();
            contc.MdiParent = this;
            contc.Show();
        }

        private void invoiceToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Genclass.Dtype = 100;
            FrmPurchaseOrder billacc = new FrmPurchaseOrder();
            billacc.MdiParent = this;

            billacc.Show();
        }

        private void exitToolStripMenuItem2_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void taxToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Genclass.Gbtxtid = 6;
            Frmitemgrp contc = new Frmitemgrp();
            contc.MdiParent = this;
            contc.Show();
        }

        private void dCProcessToolStripMenuItem_Click_1(object sender, EventArgs e)
        {

        }
        private void Mnu_mac_Click(object sender, EventArgs e)
        {
            Genclass.Gbtxtid = 8;
            Genclass.Genlbl = "Machine Name";
            Frmitemgrp contc = new Frmitemgrp();
            contc.MdiParent = this;
            contc.Show();
        }

        private void Mnu_emp_Click(object sender, EventArgs e)
        {
            Genclass.Gbtxtid = 9;
            Genclass.Genlbl = "Employee Name";
            Frmitemgrp contc = new Frmitemgrp();
            contc.MdiParent = this;
            contc.Show();
        }

        private void Mnu_opstk_Click(object sender, EventArgs e)
        {

        }

        private void Mnu_stkin_Click(object sender, EventArgs e)
        {
        }

        private void Mnu_stkout_Click(object sender, EventArgs e)
        {
        }

        private void stockReportToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Genclass.Gbtxtid = 210;
            Frmreport wprocess1 = new Frmreport();
            wprocess1.MdiParent = this;
            wprocess1.Show();
        }

        private void toolStripMenuItem1_Click(object sender, EventArgs e)
        {

        }

        private void Mnu_Hsn_Click(object sender, EventArgs e)
        {
            Genclass.Gbtxtid = 10;
            Genclass.Genlbl = "HSN Name";
            Frmitemgrp contc = new Frmitemgrp();
            contc.MdiParent = this;
            contc.Show();
        }

        private void Mnu_CGST_Click(object sender, EventArgs e)
        {
            Genclass.Gbtxtid = 11;
            Genclass.Genlbl = "Tax Type";
            Genclass.Genlbl1 = "Tax Name";
            Genclass.Genlbl2 = "Tax Percentage";
            Genclass.Genlbl3 = "Tax Structure";
            Frmitemgrp contc = new Frmitemgrp();
            contc.MdiParent = this;
            contc.Show();
        }


        private void Mnu_Pricelist_Click_1(object sender, EventArgs e)
        {

        }

        private void Mnu_par_Click(object sender, EventArgs e)
        {

        }

        private void Mnu_terms_Click(object sender, EventArgs e)
        {
            Genclass.Gbtxtid = 12;
            Genclass.Genlbl = "Terms & Conditions";
            Frmitemgrp contc = new Frmitemgrp();
            contc.MdiParent = this;
            contc.Show();
        }

        private void Mnu_addc_Click(object sender, EventArgs e)
        {
            Genclass.Gbtxtid = 13;
            Genclass.Genlbl = "Charges";
            Frmitemgrp contc = new Frmitemgrp();
            contc.MdiParent = this;
            contc.Show();
        }

        private void mnu_order_Click(object sender, EventArgs e)
        {
        }

        private void Mnu_Billacc_Click(object sender, EventArgs e)
        {
        }

        private void mun_cusparty_Click(object sender, EventArgs e)
        {
            Genclass.Gbtxtid = 110;
            Frmpar contc1 = new Frmpar();
            contc1.MdiParent = this;
            contc1.Show();
        }

        private void Mun_suppparty_Click(object sender, EventArgs e)
        {
            Genclass.Gbtxtid = 120;
            Frmpar contc = new Frmpar();
            contc.MdiParent = this;
            contc.Show();
        }

        private void salesPriceListToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Genclass.Gbtxtid = 130;
            Frmpricelist contc2 = new Frmpricelist();
            contc2.MdiParent = this;
            contc2.Show();
        }

        private void purchasePriceListToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Genclass.Gbtxtid = 140;
            Frmpricelist contc3 = new Frmpricelist();
            contc3.MdiParent = this;
            contc3.Show();

        }

        private void customerOrderToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void menuStrip_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {

        }

        private void returnableDCToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void nonReturableDCToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void nonReturnableReceiptToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void purchaseOrderToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Genclass.Dtype = 90;
            FrmPurchaseOrder billacc = new FrmPurchaseOrder
            {
                MdiParent = this
            };
            billacc.Show();

            //Genclass.Dtype = 90;
            //FrmPur sor = new FrmPur();
            //sor.MdiParent = this;
            //sor.Show();

        }

        private void gRNToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Genclass.Dtype = 10;
            FrmGRN contc = new FrmGRN();
            contc.MdiParent = this;
            contc.Show();
        }

        private void billAccountingToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Genclass.Dtype = 100;
            FrmBillAccounting billacc = new FrmBillAccounting
            {
                MdiParent = this
            };
            billacc.Show();
        }

        private void salesOrderToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Genclass.Dtype = 80;
            FrmSale sinv5 = new FrmSale();

            sinv5.MdiParent = this;
            sinv5.Show();
        }

        private void salesDCToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Genclass.Dtype = 110;
            FrmDCSales saldc = new FrmDCSales();
            saldc.MdiParent = this;
            saldc.Show();
        }

        private void salesInvoiceToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Genclass.Dtype = 40;
            FrmInvoice sinv = new FrmInvoice();
            sinv.Text = "Sales Invoice";
            sinv.MdiParent = this;
            sinv.Show();
        }

        private void openingStockToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Genclass.Dtype = 0;

            Frmopstock contc = new Frmopstock();
            contc.MdiParent = this;
            contc.Show();
        }

        private void stockInToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Genclass.Dtype = 60;
            FrmDCWithoutPro wprocess1 = new FrmDCWithoutPro();
            wprocess1.MdiParent = this;
            wprocess1.Show();
        }

        private void stockOutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Genclass.Dtype = 70;
            FrmDCWithoutPro wprocess1 = new FrmDCWithoutPro();
            wprocess1.MdiParent = this;
            wprocess1.Show();
        }

        private void returnableDCToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            Genclass.Dtype = 150;
            FrmDCSales sinv6 = new FrmDCSales();

            sinv6.MdiParent = this;
            sinv6.Show();
        }

        private void nonReturnableReceiptToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            Genclass.Dtype = 170;
            FrmDCSales sinv7 = new FrmDCSales();

            sinv7.MdiParent = this;
            sinv7.Show();
        }

        private void nonReturnableDCToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Genclass.Dtype = 160;
            FrmDCSales sinv6 = new FrmDCSales();

            sinv6.MdiParent = this;
            sinv6.Show();
        }

        private void companyToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Company sinv7 = new Company();
            sinv7.MdiParent = this;
            sinv7.Show();
        }

        private void termsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Genclass.Gbtxtid = 15;
            Genclass.Genlbl = "Terms";
            Frmitemgrp contc = new Frmitemgrp();
            contc.MdiParent = this;
            contc.Show();
        }

        private void moneyReceiptToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void accountsToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void toolStripMenuItem2_Click(object sender, EventArgs e)
        {

        }

        private void moneyReceiptToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            Genclass.Dtype = 180;
            FrmMoneyReceipt contc5 = new FrmMoneyReceipt();
            contc5.MdiParent = this;
            contc5.Show();
        }

        private void toolStripMenuItem1_Click_1(object sender, EventArgs e)
        {
            Genclass.Dtype = 190;
            FrmMoneyReceipt contc5 = new FrmMoneyReceipt();
            contc5.MdiParent = this;
            contc5.Show();
        }

        private void Mnu_salret_Click(object sender, EventArgs e)
        {
            Genclass.Dtype = 200;
            Frmsalret contc5 = new Frmsalret();
            contc5.MdiParent = this;
            contc5.Show();
        }

        private void salesOrderPendingToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Genclass.Gbtxtid = 230;
            Frmstkrpt wprocess1 = new Frmstkrpt();
            wprocess1.MdiParent = this;
            wprocess1.Show();
        }

        private void toolStripMenuItem2_Click_1(object sender, EventArgs e)
        {
            string path = Application.StartupPath + "\\CustomerOutstandingReport.xls";
            if (!File.Exists(path))
            {
                File.Create(path);
            }
            oXL = new excel.Application();
            oXL.Visible = true;
            oXL.DisplayAlerts = false;
            mWorkBook = oXL.Workbooks.Open(path, 0, false, 5, "", "", false, excel.XlPlatform.xlWindows, "", true, false, 0, true, false, false);
            mWorkSheets = mWorkBook.Worksheets;
            mWSheet1 = (excel.Worksheet)mWorkSheets.get_Item("CustomerOutstandingReport");
            excel.Range range = mWSheet1.UsedRange;
            Genclass.strsql = "Exec SP_CusOutStRpt " + Genclass.data1 + " ";
            Genclass.cmd = new SqlCommand(Genclass.strsql, conn);
            SqlDataAdapter aptr3 = new SqlDataAdapter(Genclass.cmd);
            DataTable tap3 = new DataTable();
            aptr3.Fill(tap3);
            if (tap3.Rows.Count > 0)
            {
                Genclass.i = 1;
                foreach (DataColumn column in tap3.Columns)
                {
                    mWSheet1.Cells[3, Genclass.i] = column.ColumnName;
                    Genclass.i = Genclass.i + 1;
                }
            }
            int i = 5;
            for (int j = 0; j < tap3.Rows.Count; j++)
            {
                mWSheet1.Cells[i, 1] = tap3.Rows[j]["PartyName"].ToString();
                mWSheet1.Cells[i, 2] = tap3.Rows[j]["LastYear"].ToString();
                mWSheet1.Cells[i, 3] = tap3.Rows[j]["April"].ToString();
                mWSheet1.Cells[i, 4] = tap3.Rows[j]["May"].ToString();
                mWSheet1.Cells[i, 5] = tap3.Rows[j]["June"].ToString();
                mWSheet1.Cells[i, 6] = tap3.Rows[j]["July"].ToString();
                mWSheet1.Cells[i, 7] = tap3.Rows[j]["Aug"].ToString();
                mWSheet1.Cells[i, 8] = tap3.Rows[j]["Sep"].ToString();
                mWSheet1.Cells[i, 9] = tap3.Rows[j]["Oct"].ToString();
                mWSheet1.Cells[i, 10] = tap3.Rows[j]["Nov"].ToString();
                mWSheet1.Cells[i, 11] = tap3.Rows[j]["December"].ToString();
                mWSheet1.Cells[i, 12] = tap3.Rows[j]["Jan"].ToString();
                mWSheet1.Cells[i, 13] = tap3.Rows[j]["Feb"].ToString();
                mWSheet1.Cells[i, 14] = tap3.Rows[j]["march"].ToString();
                i = i + 1;
            }

        }

        private void Mnu_salesm_Click(object sender, EventArgs e)
        {
            Genclass.Dtype = 40;
            FrmInvoiceM sinv = new FrmInvoiceM();
            sinv.Text = "Sales Invoice";
            sinv.MdiParent = this;
            sinv.Show();

        }

        private void Mnu_som_Click(object sender, EventArgs e)
        {
            Genclass.Dtype = 80;
            FrmSaleM sinv = new FrmSaleM();
            sinv.Text = "Sales Order";
            sinv.MdiParent = this;
            sinv.Show();
        }

        private void stockLedgerToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            //Frmstkrpt sinv = new Frmstkrpt();
            //sinv.Text = "Stock Ledger";
            //sinv.MdiParent = this;
            //sinv.Show();
        }

        private void stockStatementToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //Frmstkrpt sinv = new Frmstkrpt();
            //sinv.Text = "Stock Statement";
            //sinv.MdiParent = this;
            //sinv.Show();
        }

        private void yarnIssueToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmyarnIssue sinv = new FrmyarnIssue();
            sinv.Text = "Yarn Issue";
            sinv.MdiParent = this;
            sinv.Show();
        }

        private void jobOrderVendorToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Genclass.Gbtxtid = 290;
            Frmpar contc = new Frmpar();
            contc.MdiParent = this;
            contc.Show();
        }

        private void millToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Genclass.Gbtxtid = 300;
            Frmpar contc = new Frmpar();
            contc.MdiParent = this;
            contc.Show();
        }

        private void beamNoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Genclass.Gbtxtid = 20;
            Genclass.Genlbl = "Beam No";
            Genclass.Genlbl1 = "Beam No";
            Genclass.Genlbl2 = "Beam Weight";
            Genclass.Genlbl3 = "Beam No";
            Frmitemgrp contc = new Frmitemgrp();
            contc.MdiParent = this;
            contc.Show();
        }

        private void spindleWeightToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Genclass.Gbtxtid = 21;
            Genclass.Genlbl = "Spindle";
            Genclass.Genlbl1 = "Spindle Name";
            Genclass.Genlbl2 = "Spindle Weight";
            Genclass.Genlbl3 = "Spindle Weight";
            Frmitemgrp contc = new Frmitemgrp();
            contc.MdiParent = this;
            contc.Show();
        }

        private void yarnReceiptToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Genclass.Dtype = 220;
            FrmyarnReceipt sinv = new FrmyarnReceipt();
            sinv.MdiParent = this;
            sinv.Show();
        }

        private void packingSlipToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmWepIssue packingSlip = new FrmWepIssue();
            packingSlip.MdiParent = this;
            packingSlip.StartPosition = FormStartPosition.CenterScreen;
            packingSlip.Show();
        }

        private void packingSlipToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            FrmWepIssue packingSlip = new FrmWepIssue();
            packingSlip.MdiParent = this;
            packingSlip.StartPosition = FormStartPosition.CenterScreen;
            packingSlip.Show();
        }

        private void sortingDetailsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmSortDet packingSlip = new FrmSortDet();
            packingSlip.MdiParent = this;
            packingSlip.StartPosition = FormStartPosition.CenterScreen;
            packingSlip.Show();
        }

        private void internalOrderToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmInternalOrder order = new FrmInternalOrder();
            order.MdiParent = this;
            order.StartPosition = FormStartPosition.CenterScreen;
            order.Show();
        }

        private void jobCardToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmJobCard jc = new FrmJobCard();
            jc.MdiParent = this;
            jc.StartPosition = FormStartPosition.CenterScreen;
            jc.Show();
        }

        private void machineMasterToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmMachine machine = new FrmMachine();
            machine.MdiParent = this;
            machine.StartPosition = FormStartPosition.CenterScreen;
            machine.Show();
        }

        private void openingStockToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            FrmOpeningStock stock = new FrmOpeningStock();
            stock.MdiParent = this;
            stock.StartPosition = FormStartPosition.CenterScreen;
            stock.Show();
        }

        private void warbBookingToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmWarbBooking warb = new FrmWarbBooking();
            warb.MdiParent = this;
            warb.StartPosition = FormStartPosition.CenterScreen;
            warb.Show();
        }

        private void yarnMasterToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmYarnMaster ms = new FrmYarnMaster();
            ms.MdiParent = this;
            ms.StartPosition = FormStartPosition.CenterScreen;
            ms.Show();
        }

        private void pLYToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Genclass.Dtype = 20;
            FrmYarnSubMast subMast = new FrmYarnSubMast();
            subMast.MdiParent = this;
            subMast.StartPosition = FormStartPosition.CenterScreen;
            subMast.Text = "PLY Master";
            subMast.Show();
        }

        private void countToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Genclass.Dtype = 21;
            FrmYarnSubMast Count = new FrmYarnSubMast();
            Count.MdiParent = this;
            Count.StartPosition = FormStartPosition.CenterScreen;
            Count.Text = "COUNT Master";
            Count.Show();
        }

        private void yarnTypeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Genclass.Dtype = 22;
            FrmYarnSubMast Count = new FrmYarnSubMast();
            Count.MdiParent = this;
            Count.StartPosition = FormStartPosition.CenterScreen;
            Count.Text = "YARN TYPE Master";
            Count.Show();
        }

        private void cTNPCToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Genclass.Dtype = 23;
            FrmYarnSubMast Count = new FrmYarnSubMast();
            Count.MdiParent = this;
            Count.StartPosition = FormStartPosition.CenterScreen;
            Count.Text = "CTN/PC Master";
            Count.Show();
        }

        private void blendToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Genclass.Dtype = 24;
            FrmYarnSubMast Count = new FrmYarnSubMast();
            Count.MdiParent = this;
            Count.StartPosition = FormStartPosition.CenterScreen;
            Count.Text = "BLEND Master";
            Count.Show();
        }

        private void itemTypeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Genclass.Dtype = 25;
            FrmYarnSubMast Count = new FrmYarnSubMast();
            Count.MdiParent = this;
            Count.StartPosition = FormStartPosition.CenterScreen;
            Count.Text = "ITEM TYPE Master";
            Count.Show();
        }

        private void sortBookingToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmSortBooking Count = new FrmSortBooking();
            Count.MdiParent = this;
            Count.StartPosition = FormStartPosition.CenterScreen;
            Count.Show();
        }

        private void shiftWiseReportToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmShiftReport rpt = new FrmShiftReport();
            rpt.MdiParent = this;
            rpt.StartPosition = FormStartPosition.CenterScreen;
            rpt.Show();
        }

        private void qCReportToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmQCReport qc = new FrmQCReport();
            qc.MdiParent = this;
            qc.StartPosition = FormStartPosition.CenterScreen;
            qc.Show();
        }

        private void fabricIssueToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmFabricIssue fab = new FrmFabricIssue();
            fab.MdiParent = this;
            fab.StartPosition = FormStartPosition.CenterScreen;
            fab.Show();
        }

        private void rollReportToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmRollReport rep = new FrmRollReport();
            rep.MdiParent = this;
            rep.StartPosition = FormStartPosition.CenterScreen;
            rep.Show();
        }

        private void mendorToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmMendor mendor = new FrmMendor();
            mendor.MdiParent = this;
            mendor.StartPosition = FormStartPosition.CenterScreen;
            mendor.Show();
        }

        private void closeJobCardToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmCloseJobCard closeJobCard = new FrmCloseJobCard();
            closeJobCard.MdiParent = this;
            closeJobCard.StartPosition = FormStartPosition.CenterScreen;
            closeJobCard.Show();
        }

        private void weptIssueToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmWeptIssue frmWeptIssue = new FrmWeptIssue();
            frmWeptIssue.MdiParent = this;
            frmWeptIssue.StartPosition = FormStartPosition.CenterScreen;
            frmWeptIssue.Show();
        }

        private void toolStripMenuItem3_Click(object sender, EventArgs e)
        {
            FrmSortChange frmSortChange = new FrmSortChange();
            frmSortChange.MdiParent = this;
            frmSortChange.StartPosition = FormStartPosition.CenterScreen;
            frmSortChange.Show();
        }

        private void jobcardReportToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmJobCardReport frmJobCardReport = new FrmJobCardReport();
            frmJobCardReport.MdiParent = this;
            frmJobCardReport.StartPosition = FormStartPosition.CenterScreen;
            frmJobCardReport.Show();
        }

        private void stockLedgerToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmStockStatement frmStockStatement = new FrmStockStatement();
            frmStockStatement.MdiParent = this;
            frmStockStatement.StartPosition = FormStartPosition.CenterScreen;
            frmStockStatement.Show();
        }

        private void loomBookingToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmSortChange frmSortChange = new FrmSortChange();
            frmSortChange.MdiParent = this;
            frmSortChange.StartPosition = FormStartPosition.CenterScreen;
            frmSortChange.Show();
        }

        private void dCRegisterToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmDCRegister frmDCRegister = new FrmDCRegister();
            frmDCRegister.MdiParent = this;
            frmDCRegister.StartPosition = FormStartPosition.CenterScreen;
            frmDCRegister.Show();
        }

        private void dCLedgerToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmDCLedger frmDCLedger = new FrmDCLedger();
            frmDCLedger.MdiParent = this;
            frmDCLedger.StartPosition = FormStartPosition.CenterScreen;
            frmDCLedger.Show();
        }

        private void mISToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void dispatchPackingSlipToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmDispatchPackingSlip frmDispatchPackingSlip = new FrmDispatchPackingSlip();
            frmDispatchPackingSlip.MdiParent = this;
            frmDispatchPackingSlip.StartPosition = FormStartPosition.CenterScreen;
            frmDispatchPackingSlip.Show();
        }

        private void aGMLedgerToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void bEAMSTATUSToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmBeamStatus frmBeamStatus = new FrmBeamStatus();
            frmBeamStatus.MdiParent = this;
            frmBeamStatus.StartPosition = FormStartPosition.CenterScreen;
            frmBeamStatus.Show();
        }

        private void sortwiseCheckingReportToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmSortwiseChecking frmSortwiseChecking = new FrmSortwiseChecking();
            frmSortwiseChecking.MdiParent = this;
            frmSortwiseChecking.StartPosition = FormStartPosition.CenterScreen;
            frmSortwiseChecking.Show();
        }

        private void dailyProductionDataToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmDailyProductionReport frmDailyProductionReport = new FrmDailyProductionReport();
            frmDailyProductionReport.MdiParent = this;
            frmDailyProductionReport.StartPosition = FormStartPosition.CenterScreen;
            frmDailyProductionReport.Show();
        }

        private void knottingStatusToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmKnottingStatus frmNottingStatus = new FrmKnottingStatus();
            frmNottingStatus.MdiParent = this;
            frmNottingStatus.StartPosition = FormStartPosition.CenterScreen;
            frmNottingStatus.Show();
        }
    }
}