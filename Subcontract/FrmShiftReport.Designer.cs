﻿namespace ACV
{
    partial class FrmShiftReport
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmShiftReport));
            this.grReport = new System.Windows.Forms.GroupBox();
            this.btnExport = new System.Windows.Forms.Button();
            this.btnView = new System.Windows.Forms.Button();
            this.DataGridShiftReport = new System.Windows.Forms.DataGridView();
            this.radioLoomWise = new System.Windows.Forms.RadioButton();
            this.radioSortWise = new System.Windows.Forms.RadioButton();
            this.radioRollWise = new System.Windows.Forms.RadioButton();
            this.label2 = new System.Windows.Forms.Label();
            this.dtpDate = new System.Windows.Forms.DateTimePicker();
            this.cmbShift = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.panadd = new System.Windows.Forms.Panel();
            this.btnDownloadExcel = new System.Windows.Forms.Button();
            this.btnPrint = new System.Windows.Forms.Button();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.btnReconciliation = new System.Windows.Forms.Button();
            this.grReport.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridShiftReport)).BeginInit();
            this.panadd.SuspendLayout();
            this.SuspendLayout();
            // 
            // grReport
            // 
            this.grReport.Controls.Add(this.btnReconciliation);
            this.grReport.Controls.Add(this.btnExport);
            this.grReport.Controls.Add(this.btnView);
            this.grReport.Controls.Add(this.DataGridShiftReport);
            this.grReport.Controls.Add(this.radioLoomWise);
            this.grReport.Controls.Add(this.radioSortWise);
            this.grReport.Controls.Add(this.radioRollWise);
            this.grReport.Controls.Add(this.label2);
            this.grReport.Controls.Add(this.dtpDate);
            this.grReport.Controls.Add(this.cmbShift);
            this.grReport.Controls.Add(this.label1);
            this.grReport.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grReport.Location = new System.Drawing.Point(6, -3);
            this.grReport.Name = "grReport";
            this.grReport.Size = new System.Drawing.Size(671, 455);
            this.grReport.TabIndex = 0;
            this.grReport.TabStop = false;
            // 
            // btnExport
            // 
            this.btnExport.BackColor = System.Drawing.Color.White;
            this.btnExport.Image = global::ACV.Properties.Resources.if_import_export_63126;
            this.btnExport.ImageAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.btnExport.Location = new System.Drawing.Point(562, 43);
            this.btnExport.Name = "btnExport";
            this.btnExport.Size = new System.Drawing.Size(89, 29);
            this.btnExport.TabIndex = 227;
            this.btnExport.Text = "Export";
            this.btnExport.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnExport.UseVisualStyleBackColor = false;
            this.btnExport.Visible = false;
            this.btnExport.Click += new System.EventHandler(this.btnExport_Click);
            // 
            // btnView
            // 
            this.btnView.BackColor = System.Drawing.Color.White;
            this.btnView.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnView.Image = global::ACV.Properties.Resources.ok;
            this.btnView.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnView.Location = new System.Drawing.Point(287, 45);
            this.btnView.Name = "btnView";
            this.btnView.Size = new System.Drawing.Size(67, 28);
            this.btnView.TabIndex = 226;
            this.btnView.Text = "View";
            this.btnView.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnView.UseVisualStyleBackColor = false;
            this.btnView.Click += new System.EventHandler(this.button1_Click);
            // 
            // DataGridShiftReport
            // 
            this.DataGridShiftReport.AllowUserToAddRows = false;
            this.DataGridShiftReport.BackgroundColor = System.Drawing.Color.White;
            this.DataGridShiftReport.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DataGridShiftReport.Location = new System.Drawing.Point(6, 77);
            this.DataGridShiftReport.Name = "DataGridShiftReport";
            this.DataGridShiftReport.ReadOnly = true;
            this.DataGridShiftReport.RowHeadersVisible = false;
            this.DataGridShiftReport.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.DataGridShiftReport.Size = new System.Drawing.Size(659, 373);
            this.DataGridShiftReport.TabIndex = 5;
            // 
            // radioLoomWise
            // 
            this.radioLoomWise.AutoSize = true;
            this.radioLoomWise.Location = new System.Drawing.Point(472, 15);
            this.radioLoomWise.Name = "radioLoomWise";
            this.radioLoomWise.Size = new System.Drawing.Size(98, 22);
            this.radioLoomWise.TabIndex = 4;
            this.radioLoomWise.TabStop = true;
            this.radioLoomWise.Text = "LOOM WISE";
            this.radioLoomWise.UseVisualStyleBackColor = true;
            // 
            // radioSortWise
            // 
            this.radioSortWise.AutoSize = true;
            this.radioSortWise.Location = new System.Drawing.Point(349, 15);
            this.radioSortWise.Name = "radioSortWise";
            this.radioSortWise.Size = new System.Drawing.Size(92, 22);
            this.radioSortWise.TabIndex = 3;
            this.radioSortWise.TabStop = true;
            this.radioSortWise.Text = "SORT WISE";
            this.radioSortWise.UseVisualStyleBackColor = true;
            // 
            // radioRollWise
            // 
            this.radioRollWise.AutoSize = true;
            this.radioRollWise.Checked = true;
            this.radioRollWise.Location = new System.Drawing.Point(237, 15);
            this.radioRollWise.Name = "radioRollWise";
            this.radioRollWise.Size = new System.Drawing.Size(90, 22);
            this.radioRollWise.TabIndex = 2;
            this.radioRollWise.TabStop = true;
            this.radioRollWise.Text = "ROLL WISE";
            this.radioRollWise.UseVisualStyleBackColor = true;
            this.radioRollWise.CheckedChanged += new System.EventHandler(this.radioRollWise_CheckedChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(37, 17);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(37, 18);
            this.label2.TabIndex = 3;
            this.label2.Text = "Date";
            // 
            // dtpDate
            // 
            this.dtpDate.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpDate.Location = new System.Drawing.Point(81, 13);
            this.dtpDate.Name = "dtpDate";
            this.dtpDate.Size = new System.Drawing.Size(129, 26);
            this.dtpDate.TabIndex = 0;
            // 
            // cmbShift
            // 
            this.cmbShift.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbShift.FormattingEnabled = true;
            this.cmbShift.Location = new System.Drawing.Point(81, 45);
            this.cmbShift.Name = "cmbShift";
            this.cmbShift.Size = new System.Drawing.Size(200, 26);
            this.cmbShift.TabIndex = 1;
            this.cmbShift.SelectedIndexChanged += new System.EventHandler(this.cmbShift_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(38, 49);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(36, 18);
            this.label1.TabIndex = 0;
            this.label1.Text = "Shift";
            // 
            // panadd
            // 
            this.panadd.BackColor = System.Drawing.Color.White;
            this.panadd.Controls.Add(this.btnDownloadExcel);
            this.panadd.Controls.Add(this.btnPrint);
            this.panadd.Location = new System.Drawing.Point(0, 457);
            this.panadd.Name = "panadd";
            this.panadd.Size = new System.Drawing.Size(680, 37);
            this.panadd.TabIndex = 224;
            // 
            // btnDownloadExcel
            // 
            this.btnDownloadExcel.BackColor = System.Drawing.Color.White;
            this.btnDownloadExcel.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDownloadExcel.Image = global::ACV.Properties.Resources.ok;
            this.btnDownloadExcel.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnDownloadExcel.Location = new System.Drawing.Point(470, 4);
            this.btnDownloadExcel.Name = "btnDownloadExcel";
            this.btnDownloadExcel.Size = new System.Drawing.Size(137, 30);
            this.btnDownloadExcel.TabIndex = 228;
            this.btnDownloadExcel.Text = "Download Excel";
            this.btnDownloadExcel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnDownloadExcel.UseVisualStyleBackColor = false;
            this.btnDownloadExcel.Visible = false;
            this.btnDownloadExcel.Click += new System.EventHandler(this.btnDownloadExcel_Click);
            // 
            // btnPrint
            // 
            this.btnPrint.BackColor = System.Drawing.Color.White;
            this.btnPrint.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPrint.Image = ((System.Drawing.Image)(resources.GetObject("btnPrint.Image")));
            this.btnPrint.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnPrint.Location = new System.Drawing.Point(613, 4);
            this.btnPrint.Name = "btnPrint";
            this.btnPrint.Size = new System.Drawing.Size(64, 30);
            this.btnPrint.TabIndex = 225;
            this.btnPrint.Text = "Print";
            this.btnPrint.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnPrint.UseVisualStyleBackColor = false;
            this.btnPrint.Click += new System.EventHandler(this.button3_Click);
            // 
            // timer1
            // 
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // btnReconciliation
            // 
            this.btnReconciliation.BackColor = System.Drawing.Color.White;
            this.btnReconciliation.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnReconciliation.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnReconciliation.Location = new System.Drawing.Point(385, 45);
            this.btnReconciliation.Name = "btnReconciliation";
            this.btnReconciliation.Size = new System.Drawing.Size(106, 28);
            this.btnReconciliation.TabIndex = 228;
            this.btnReconciliation.Text = "Reconcile Total Pick";
            this.btnReconciliation.UseVisualStyleBackColor = false;
            this.btnReconciliation.Click += new System.EventHandler(this.btnReconciliation_Click);
            // 
            // FrmShiftReport
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.ClientSize = new System.Drawing.Size(683, 496);
            this.Controls.Add(this.panadd);
            this.Controls.Add(this.grReport);
            this.MaximizeBox = false;
            this.Name = "FrmShiftReport";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Shift Report";
            this.Load += new System.EventHandler(this.FrmShiftReport_Load);
            this.grReport.ResumeLayout(false);
            this.grReport.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridShiftReport)).EndInit();
            this.panadd.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox grReport;
        private System.Windows.Forms.RadioButton radioLoomWise;
        private System.Windows.Forms.RadioButton radioSortWise;
        private System.Windows.Forms.RadioButton radioRollWise;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.DateTimePicker dtpDate;
        private System.Windows.Forms.ComboBox cmbShift;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panadd;
        private System.Windows.Forms.Button btnPrint;
        private System.Windows.Forms.DataGridView DataGridShiftReport;
        private System.Windows.Forms.Button btnView;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.Button btnExport;
        private System.Windows.Forms.Button btnDownloadExcel;
        private System.Windows.Forms.Button btnReconciliation;
    }
}