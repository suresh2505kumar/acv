﻿namespace ACV
{
    partial class FrmFabricIssue
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmFabricIssue));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            this.grFront = new System.Windows.Forms.GroupBox();
            this.DtpMonth = new System.Windows.Forms.DateTimePicker();
            this.txtSearch = new System.Windows.Forms.TextBox();
            this.DataGridFabIssue = new System.Windows.Forms.DataGridView();
            this.panadd = new System.Windows.Forms.Panel();
            this.btnDelete = new System.Windows.Forms.Button();
            this.BtnRECONCILE = new System.Windows.Forms.Button();
            this.btnUpdateRoll = new System.Windows.Forms.Button();
            this.panNumbers = new System.Windows.Forms.Panel();
            this.lblFrom = new System.Windows.Forms.Label();
            this.lblCount = new System.Windows.Forms.Label();
            this.flowLayoutPanel3 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel2 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.btnFstBack = new System.Windows.Forms.Button();
            this.btnBack = new System.Windows.Forms.Button();
            this.btnFstNext = new System.Windows.Forms.Button();
            this.btnNext = new System.Windows.Forms.Button();
            this.chckActive = new System.Windows.Forms.CheckBox();
            this.btnAdd = new System.Windows.Forms.Button();
            this.btnEdit = new System.Windows.Forms.Button();
            this.btnPackingSlipPrint = new System.Windows.Forms.Button();
            this.btnExit = new System.Windows.Forms.Button();
            this.btnPrint = new System.Windows.Forms.Button();
            this.btnaddrcan = new System.Windows.Forms.Button();
            this.btnsave = new System.Windows.Forms.Button();
            this.grBack = new System.Windows.Forms.GroupBox();
            this.GrRollUpdate = new System.Windows.Forms.GroupBox();
            this.btnRollNoClose = new System.Windows.Forms.Button();
            this.btnRollNoSave = new System.Windows.Forms.Button();
            this.label23 = new System.Windows.Forms.Label();
            this.txtMeter = new System.Windows.Forms.TextBox();
            this.label21 = new System.Windows.Forms.Label();
            this.txtWeight = new System.Windows.Forms.TextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.txtSortNo = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.txtRollNo = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.txtVechNo = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.txtBaseTotal = new System.Windows.Forms.TextBox();
            this.grSearch = new System.Windows.Forms.Panel();
            this.DataGridCommon = new System.Windows.Forms.DataGridView();
            this.btnSelect = new System.Windows.Forms.Button();
            this.btnHide = new System.Windows.Forms.Button();
            this.label14 = new System.Windows.Forms.Label();
            this.txtRoundOff = new System.Windows.Forms.TextBox();
            this.txtNetValue = new System.Windows.Forms.TextBox();
            this.label22 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.txttotalValue = new System.Windows.Forms.TextBox();
            this.txtBasic = new System.Windows.Forms.TextBox();
            this.label20 = new System.Windows.Forms.Label();
            this.DataGridFabricItem = new System.Windows.Forms.DataGridView();
            this.txtVehicle = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.Phone = new System.Windows.Forms.Label();
            this.txtDocNo = new System.Windows.Forms.TextBox();
            this.dtpGrnDate = new System.Windows.Forms.DateTimePicker();
            this.txtSupplerName = new System.Windows.Forms.TextBox();
            this.DataGridTax = new System.Windows.Forms.DataGridView();
            this.txtDispatchSlip = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.txtRemarks = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.txtTranspoter = new System.Windows.Forms.TextBox();
            this.txtLRNo = new System.Windows.Forms.TextBox();
            this.txtDispatchTo = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.txtEwayBill = new System.Windows.Forms.TextBox();
            this.txtFrom = new System.Windows.Forms.ComboBox();
            this.cmbFor = new System.Windows.Forms.ComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this.DataGridDet = new System.Windows.Forms.DataGridView();
            this.grdetView = new System.Windows.Forms.GroupBox();
            this.btnClose = new System.Windows.Forms.Button();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.grFront.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridFabIssue)).BeginInit();
            this.panadd.SuspendLayout();
            this.panNumbers.SuspendLayout();
            this.grBack.SuspendLayout();
            this.GrRollUpdate.SuspendLayout();
            this.grSearch.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridCommon)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridFabricItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridTax)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridDet)).BeginInit();
            this.grdetView.SuspendLayout();
            this.SuspendLayout();
            // 
            // grFront
            // 
            this.grFront.Controls.Add(this.DtpMonth);
            this.grFront.Controls.Add(this.txtSearch);
            this.grFront.Controls.Add(this.DataGridFabIssue);
            this.grFront.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grFront.Location = new System.Drawing.Point(8, 3);
            this.grFront.Name = "grFront";
            this.grFront.Size = new System.Drawing.Size(1143, 572);
            this.grFront.TabIndex = 391;
            this.grFront.TabStop = false;
            // 
            // DtpMonth
            // 
            this.DtpMonth.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.DtpMonth.Location = new System.Drawing.Point(996, 14);
            this.DtpMonth.Name = "DtpMonth";
            this.DtpMonth.Size = new System.Drawing.Size(139, 26);
            this.DtpMonth.TabIndex = 6;
            this.DtpMonth.ValueChanged += new System.EventHandler(this.DtpMonth_ValueChanged);
            // 
            // txtSearch
            // 
            this.txtSearch.Location = new System.Drawing.Point(7, 15);
            this.txtSearch.Name = "txtSearch";
            this.txtSearch.Size = new System.Drawing.Size(988, 26);
            this.txtSearch.TabIndex = 5;
            this.txtSearch.TextChanged += new System.EventHandler(this.TxtSearch_TextChanged);
            // 
            // DataGridFabIssue
            // 
            this.DataGridFabIssue.AllowUserToAddRows = false;
            this.DataGridFabIssue.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.ActiveCaption;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.DataGridFabIssue.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.DataGridFabIssue.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DataGridFabIssue.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnF2;
            this.DataGridFabIssue.EnableHeadersVisualStyles = false;
            this.DataGridFabIssue.Location = new System.Drawing.Point(7, 42);
            this.DataGridFabIssue.Margin = new System.Windows.Forms.Padding(4);
            this.DataGridFabIssue.Name = "DataGridFabIssue";
            this.DataGridFabIssue.ReadOnly = true;
            this.DataGridFabIssue.RowHeadersVisible = false;
            this.DataGridFabIssue.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.DataGridFabIssue.Size = new System.Drawing.Size(1129, 523);
            this.DataGridFabIssue.TabIndex = 0;
            // 
            // panadd
            // 
            this.panadd.BackColor = System.Drawing.Color.White;
            this.panadd.Controls.Add(this.btnDelete);
            this.panadd.Controls.Add(this.BtnRECONCILE);
            this.panadd.Controls.Add(this.btnUpdateRoll);
            this.panadd.Controls.Add(this.panNumbers);
            this.panadd.Controls.Add(this.btnFstBack);
            this.panadd.Controls.Add(this.btnBack);
            this.panadd.Controls.Add(this.btnFstNext);
            this.panadd.Controls.Add(this.btnNext);
            this.panadd.Controls.Add(this.chckActive);
            this.panadd.Controls.Add(this.btnAdd);
            this.panadd.Controls.Add(this.btnEdit);
            this.panadd.Controls.Add(this.btnPackingSlipPrint);
            this.panadd.Controls.Add(this.btnExit);
            this.panadd.Controls.Add(this.btnPrint);
            this.panadd.Controls.Add(this.btnaddrcan);
            this.panadd.Controls.Add(this.btnsave);
            this.panadd.Location = new System.Drawing.Point(6, 578);
            this.panadd.Name = "panadd";
            this.panadd.Size = new System.Drawing.Size(1147, 34);
            this.panadd.TabIndex = 392;
            this.panadd.Paint += new System.Windows.Forms.PaintEventHandler(this.panadd_Paint);
            // 
            // btnDelete
            // 
            this.btnDelete.BackColor = System.Drawing.Color.White;
            this.btnDelete.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDelete.Image = ((System.Drawing.Image)(resources.GetObject("btnDelete.Image")));
            this.btnDelete.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnDelete.Location = new System.Drawing.Point(849, 2);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(69, 30);
            this.btnDelete.TabIndex = 186;
            this.btnDelete.Text = "Delete";
            this.btnDelete.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnDelete.UseVisualStyleBackColor = false;
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // BtnRECONCILE
            // 
            this.BtnRECONCILE.BackColor = System.Drawing.Color.White;
            this.BtnRECONCILE.Location = new System.Drawing.Point(373, 3);
            this.BtnRECONCILE.Name = "BtnRECONCILE";
            this.BtnRECONCILE.Size = new System.Drawing.Size(146, 30);
            this.BtnRECONCILE.TabIndex = 218;
            this.BtnRECONCILE.Text = "RECONCILE DispatchSlip";
            this.BtnRECONCILE.UseVisualStyleBackColor = false;
            this.BtnRECONCILE.Visible = false;
            this.BtnRECONCILE.Click += new System.EventHandler(this.BtnRECONCILE_Click);
            // 
            // btnUpdateRoll
            // 
            this.btnUpdateRoll.BackColor = System.Drawing.Color.White;
            this.btnUpdateRoll.Location = new System.Drawing.Point(609, 2);
            this.btnUpdateRoll.Name = "btnUpdateRoll";
            this.btnUpdateRoll.Size = new System.Drawing.Size(95, 30);
            this.btnUpdateRoll.TabIndex = 217;
            this.btnUpdateRoll.Text = "RollNo Update";
            this.btnUpdateRoll.UseVisualStyleBackColor = false;
            this.btnUpdateRoll.Visible = false;
            this.btnUpdateRoll.Click += new System.EventHandler(this.btnUpdateRoll_Click);
            // 
            // panNumbers
            // 
            this.panNumbers.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panNumbers.Controls.Add(this.lblFrom);
            this.panNumbers.Controls.Add(this.lblCount);
            this.panNumbers.Controls.Add(this.flowLayoutPanel3);
            this.panNumbers.Controls.Add(this.flowLayoutPanel2);
            this.panNumbers.Controls.Add(this.flowLayoutPanel1);
            this.panNumbers.Location = new System.Drawing.Point(66, 5);
            this.panNumbers.Name = "panNumbers";
            this.panNumbers.Size = new System.Drawing.Size(74, 24);
            this.panNumbers.TabIndex = 214;
            // 
            // lblFrom
            // 
            this.lblFrom.AutoSize = true;
            this.lblFrom.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFrom.ForeColor = System.Drawing.Color.Black;
            this.lblFrom.Location = new System.Drawing.Point(4, 3);
            this.lblFrom.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblFrom.Name = "lblFrom";
            this.lblFrom.Size = new System.Drawing.Size(15, 16);
            this.lblFrom.TabIndex = 163;
            this.lblFrom.Text = "1";
            // 
            // lblCount
            // 
            this.lblCount.AutoSize = true;
            this.lblCount.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCount.ForeColor = System.Drawing.Color.Black;
            this.lblCount.Location = new System.Drawing.Point(27, 3);
            this.lblCount.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblCount.Name = "lblCount";
            this.lblCount.Size = new System.Drawing.Size(29, 16);
            this.lblCount.TabIndex = 162;
            this.lblCount.Text = "of 1";
            // 
            // flowLayoutPanel3
            // 
            this.flowLayoutPanel3.Location = new System.Drawing.Point(61, 30);
            this.flowLayoutPanel3.Name = "flowLayoutPanel3";
            this.flowLayoutPanel3.Size = new System.Drawing.Size(200, 100);
            this.flowLayoutPanel3.TabIndex = 2;
            // 
            // flowLayoutPanel2
            // 
            this.flowLayoutPanel2.Location = new System.Drawing.Point(38, 35);
            this.flowLayoutPanel2.Name = "flowLayoutPanel2";
            this.flowLayoutPanel2.Size = new System.Drawing.Size(200, 100);
            this.flowLayoutPanel2.TabIndex = 1;
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.Location = new System.Drawing.Point(62, -129);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(200, 100);
            this.flowLayoutPanel1.TabIndex = 0;
            // 
            // btnFstBack
            // 
            this.btnFstBack.BackColor = System.Drawing.Color.White;
            this.btnFstBack.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.btnFstBack.FlatAppearance.BorderSize = 0;
            this.btnFstBack.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnFstBack.Image = ((System.Drawing.Image)(resources.GetObject("btnFstBack.Image")));
            this.btnFstBack.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnFstBack.Location = new System.Drawing.Point(9, 4);
            this.btnFstBack.Name = "btnFstBack";
            this.btnFstBack.Size = new System.Drawing.Size(19, 26);
            this.btnFstBack.TabIndex = 213;
            this.btnFstBack.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnFstBack.UseVisualStyleBackColor = false;
            // 
            // btnBack
            // 
            this.btnBack.BackColor = System.Drawing.Color.White;
            this.btnBack.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.btnBack.FlatAppearance.BorderSize = 0;
            this.btnBack.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnBack.Image = ((System.Drawing.Image)(resources.GetObject("btnBack.Image")));
            this.btnBack.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnBack.Location = new System.Drawing.Point(38, 4);
            this.btnBack.Name = "btnBack";
            this.btnBack.Size = new System.Drawing.Size(18, 26);
            this.btnBack.TabIndex = 212;
            this.btnBack.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnBack.UseVisualStyleBackColor = false;
            // 
            // btnFstNext
            // 
            this.btnFstNext.BackColor = System.Drawing.Color.White;
            this.btnFstNext.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.btnFstNext.FlatAppearance.BorderSize = 0;
            this.btnFstNext.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnFstNext.Image = ((System.Drawing.Image)(resources.GetObject("btnFstNext.Image")));
            this.btnFstNext.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnFstNext.Location = new System.Drawing.Point(178, 4);
            this.btnFstNext.Name = "btnFstNext";
            this.btnFstNext.Size = new System.Drawing.Size(19, 26);
            this.btnFstNext.TabIndex = 211;
            this.btnFstNext.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnFstNext.UseVisualStyleBackColor = false;
            // 
            // btnNext
            // 
            this.btnNext.BackColor = System.Drawing.Color.White;
            this.btnNext.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.btnNext.FlatAppearance.BorderSize = 0;
            this.btnNext.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnNext.Image = ((System.Drawing.Image)(resources.GetObject("btnNext.Image")));
            this.btnNext.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnNext.Location = new System.Drawing.Point(150, 4);
            this.btnNext.Name = "btnNext";
            this.btnNext.Size = new System.Drawing.Size(18, 26);
            this.btnNext.TabIndex = 210;
            this.btnNext.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnNext.UseVisualStyleBackColor = false;
            // 
            // chckActive
            // 
            this.chckActive.AutoSize = true;
            this.chckActive.BackColor = System.Drawing.Color.White;
            this.chckActive.Checked = true;
            this.chckActive.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chckActive.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chckActive.Location = new System.Drawing.Point(302, 6);
            this.chckActive.Name = "chckActive";
            this.chckActive.Size = new System.Drawing.Size(65, 22);
            this.chckActive.TabIndex = 187;
            this.chckActive.Text = "Active";
            this.chckActive.UseVisualStyleBackColor = false;
            this.chckActive.CheckedChanged += new System.EventHandler(this.chckActive_CheckedChanged);
            // 
            // btnAdd
            // 
            this.btnAdd.BackColor = System.Drawing.Color.White;
            this.btnAdd.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.btnAdd.FlatAppearance.BorderSize = 0;
            this.btnAdd.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAdd.Image = ((System.Drawing.Image)(resources.GetObject("btnAdd.Image")));
            this.btnAdd.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnAdd.Location = new System.Drawing.Point(706, 2);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(87, 30);
            this.btnAdd.TabIndex = 184;
            this.btnAdd.Text = "Add new";
            this.btnAdd.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnAdd.UseVisualStyleBackColor = false;
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // btnEdit
            // 
            this.btnEdit.BackColor = System.Drawing.Color.White;
            this.btnEdit.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnEdit.Image = ((System.Drawing.Image)(resources.GetObject("btnEdit.Image")));
            this.btnEdit.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnEdit.Location = new System.Drawing.Point(792, 2);
            this.btnEdit.Name = "btnEdit";
            this.btnEdit.Size = new System.Drawing.Size(58, 30);
            this.btnEdit.TabIndex = 185;
            this.btnEdit.Text = "Edit";
            this.btnEdit.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnEdit.UseVisualStyleBackColor = false;
            this.btnEdit.Visible = false;
            this.btnEdit.Click += new System.EventHandler(this.btnEdit_Click);
            // 
            // btnPackingSlipPrint
            // 
            this.btnPackingSlipPrint.BackColor = System.Drawing.Color.White;
            this.btnPackingSlipPrint.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPackingSlipPrint.Image = ((System.Drawing.Image)(resources.GetObject("btnPackingSlipPrint.Image")));
            this.btnPackingSlipPrint.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnPackingSlipPrint.Location = new System.Drawing.Point(1036, 2);
            this.btnPackingSlipPrint.Name = "btnPackingSlipPrint";
            this.btnPackingSlipPrint.Size = new System.Drawing.Size(108, 30);
            this.btnPackingSlipPrint.TabIndex = 216;
            this.btnPackingSlipPrint.Text = "Print Pk Slip";
            this.btnPackingSlipPrint.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnPackingSlipPrint.UseVisualStyleBackColor = false;
            this.btnPackingSlipPrint.Click += new System.EventHandler(this.button1_Click);
            // 
            // btnExit
            // 
            this.btnExit.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnExit.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnExit.Image = ((System.Drawing.Image)(resources.GetObject("btnExit.Image")));
            this.btnExit.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnExit.Location = new System.Drawing.Point(917, 2);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(57, 30);
            this.btnExit.TabIndex = 208;
            this.btnExit.Text = "Exit";
            this.btnExit.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnExit.UseVisualStyleBackColor = false;
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // btnPrint
            // 
            this.btnPrint.BackColor = System.Drawing.Color.White;
            this.btnPrint.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPrint.Image = ((System.Drawing.Image)(resources.GetObject("btnPrint.Image")));
            this.btnPrint.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnPrint.Location = new System.Drawing.Point(972, 2);
            this.btnPrint.Name = "btnPrint";
            this.btnPrint.Size = new System.Drawing.Size(64, 30);
            this.btnPrint.TabIndex = 215;
            this.btnPrint.Text = "Print";
            this.btnPrint.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnPrint.UseVisualStyleBackColor = false;
            this.btnPrint.Click += new System.EventHandler(this.btnPrint_Click);
            // 
            // btnaddrcan
            // 
            this.btnaddrcan.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnaddrcan.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnaddrcan.Image = ((System.Drawing.Image)(resources.GetObject("btnaddrcan.Image")));
            this.btnaddrcan.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnaddrcan.Location = new System.Drawing.Point(965, 3);
            this.btnaddrcan.Name = "btnaddrcan";
            this.btnaddrcan.Size = new System.Drawing.Size(64, 29);
            this.btnaddrcan.TabIndex = 213;
            this.btnaddrcan.Text = "Back";
            this.btnaddrcan.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnaddrcan.UseVisualStyleBackColor = false;
            this.btnaddrcan.Click += new System.EventHandler(this.btnaddrcan_Click);
            // 
            // btnsave
            // 
            this.btnsave.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnsave.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnsave.Image = ((System.Drawing.Image)(resources.GetObject("btnsave.Image")));
            this.btnsave.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnsave.Location = new System.Drawing.Point(895, 2);
            this.btnsave.Name = "btnsave";
            this.btnsave.Size = new System.Drawing.Size(71, 30);
            this.btnsave.TabIndex = 212;
            this.btnsave.Text = "Save";
            this.btnsave.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnsave.UseVisualStyleBackColor = false;
            this.btnsave.Click += new System.EventHandler(this.btnsave_Click);
            // 
            // grBack
            // 
            this.grBack.Controls.Add(this.GrRollUpdate);
            this.grBack.Controls.Add(this.label17);
            this.grBack.Controls.Add(this.txtVechNo);
            this.grBack.Controls.Add(this.label16);
            this.grBack.Controls.Add(this.label15);
            this.grBack.Controls.Add(this.label12);
            this.grBack.Controls.Add(this.label5);
            this.grBack.Controls.Add(this.label11);
            this.grBack.Controls.Add(this.txtBaseTotal);
            this.grBack.Controls.Add(this.grSearch);
            this.grBack.Controls.Add(this.label14);
            this.grBack.Controls.Add(this.txtRoundOff);
            this.grBack.Controls.Add(this.txtNetValue);
            this.grBack.Controls.Add(this.label22);
            this.grBack.Controls.Add(this.label24);
            this.grBack.Controls.Add(this.txttotalValue);
            this.grBack.Controls.Add(this.txtBasic);
            this.grBack.Controls.Add(this.label20);
            this.grBack.Controls.Add(this.DataGridFabricItem);
            this.grBack.Controls.Add(this.txtVehicle);
            this.grBack.Controls.Add(this.label1);
            this.grBack.Controls.Add(this.label2);
            this.grBack.Controls.Add(this.label3);
            this.grBack.Controls.Add(this.Phone);
            this.grBack.Controls.Add(this.txtDocNo);
            this.grBack.Controls.Add(this.dtpGrnDate);
            this.grBack.Controls.Add(this.txtSupplerName);
            this.grBack.Controls.Add(this.DataGridTax);
            this.grBack.Controls.Add(this.txtDispatchSlip);
            this.grBack.Controls.Add(this.label8);
            this.grBack.Controls.Add(this.txtRemarks);
            this.grBack.Controls.Add(this.label6);
            this.grBack.Controls.Add(this.label4);
            this.grBack.Controls.Add(this.txtTranspoter);
            this.grBack.Controls.Add(this.txtLRNo);
            this.grBack.Controls.Add(this.txtDispatchTo);
            this.grBack.Controls.Add(this.label13);
            this.grBack.Controls.Add(this.txtEwayBill);
            this.grBack.Controls.Add(this.txtFrom);
            this.grBack.Controls.Add(this.cmbFor);
            this.grBack.Controls.Add(this.label7);
            this.grBack.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grBack.Location = new System.Drawing.Point(8, 3);
            this.grBack.Name = "grBack";
            this.grBack.Size = new System.Drawing.Size(1143, 572);
            this.grBack.TabIndex = 6;
            this.grBack.TabStop = false;
            this.grBack.Enter += new System.EventHandler(this.grBack_Enter);
            // 
            // GrRollUpdate
            // 
            this.GrRollUpdate.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.GrRollUpdate.Controls.Add(this.btnRollNoClose);
            this.GrRollUpdate.Controls.Add(this.btnRollNoSave);
            this.GrRollUpdate.Controls.Add(this.label23);
            this.GrRollUpdate.Controls.Add(this.txtMeter);
            this.GrRollUpdate.Controls.Add(this.label21);
            this.GrRollUpdate.Controls.Add(this.txtWeight);
            this.GrRollUpdate.Controls.Add(this.label19);
            this.GrRollUpdate.Controls.Add(this.txtSortNo);
            this.GrRollUpdate.Controls.Add(this.label18);
            this.GrRollUpdate.Controls.Add(this.txtRollNo);
            this.GrRollUpdate.Location = new System.Drawing.Point(193, 136);
            this.GrRollUpdate.Name = "GrRollUpdate";
            this.GrRollUpdate.Size = new System.Drawing.Size(555, 313);
            this.GrRollUpdate.TabIndex = 417;
            this.GrRollUpdate.TabStop = false;
            this.GrRollUpdate.Visible = false;
            // 
            // btnRollNoClose
            // 
            this.btnRollNoClose.Location = new System.Drawing.Point(293, 226);
            this.btnRollNoClose.Name = "btnRollNoClose";
            this.btnRollNoClose.Size = new System.Drawing.Size(75, 31);
            this.btnRollNoClose.TabIndex = 9;
            this.btnRollNoClose.Text = "Close";
            this.btnRollNoClose.UseVisualStyleBackColor = true;
            this.btnRollNoClose.Click += new System.EventHandler(this.BtnRollNoClose_Click);
            // 
            // btnRollNoSave
            // 
            this.btnRollNoSave.Location = new System.Drawing.Point(212, 226);
            this.btnRollNoSave.Name = "btnRollNoSave";
            this.btnRollNoSave.Size = new System.Drawing.Size(75, 31);
            this.btnRollNoSave.TabIndex = 8;
            this.btnRollNoSave.Text = "Save";
            this.btnRollNoSave.UseVisualStyleBackColor = true;
            this.btnRollNoSave.Click += new System.EventHandler(this.BtnRollNoSave_Click);
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(160, 186);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(46, 18);
            this.label23.TabIndex = 7;
            this.label23.Text = "Meter";
            // 
            // txtMeter
            // 
            this.txtMeter.Location = new System.Drawing.Point(212, 182);
            this.txtMeter.Name = "txtMeter";
            this.txtMeter.Size = new System.Drawing.Size(202, 26);
            this.txtMeter.TabIndex = 6;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(154, 145);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(52, 18);
            this.label21.TabIndex = 5;
            this.label21.Text = "Weight";
            // 
            // txtWeight
            // 
            this.txtWeight.Location = new System.Drawing.Point(212, 141);
            this.txtWeight.Name = "txtWeight";
            this.txtWeight.Size = new System.Drawing.Size(202, 26);
            this.txtWeight.TabIndex = 4;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(152, 105);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(54, 18);
            this.label19.TabIndex = 3;
            this.label19.Text = "Sort No";
            // 
            // txtSortNo
            // 
            this.txtSortNo.Location = new System.Drawing.Point(212, 101);
            this.txtSortNo.Name = "txtSortNo";
            this.txtSortNo.Size = new System.Drawing.Size(202, 26);
            this.txtSortNo.TabIndex = 2;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(154, 64);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(53, 18);
            this.label18.TabIndex = 1;
            this.label18.Text = "Roll No";
            // 
            // txtRollNo
            // 
            this.txtRollNo.Location = new System.Drawing.Point(213, 60);
            this.txtRollNo.Name = "txtRollNo";
            this.txtRollNo.Size = new System.Drawing.Size(202, 26);
            this.txtRollNo.TabIndex = 0;
            this.txtRollNo.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TxtRollNo_KeyDown);
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.Location = new System.Drawing.Point(628, 431);
            this.label17.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(54, 18);
            this.label17.TabIndex = 416;
            this.label17.Text = "Vehicle";
            // 
            // txtVechNo
            // 
            this.txtVechNo.Location = new System.Drawing.Point(628, 451);
            this.txtVechNo.Name = "txtVechNo";
            this.txtVechNo.Size = new System.Drawing.Size(191, 26);
            this.txtVechNo.TabIndex = 415;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(625, 430);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(40, 18);
            this.label16.TabIndex = 413;
            this.label16.Text = "From";
            this.label16.Visible = false;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(428, 392);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(123, 18);
            this.label15.TabIndex = 412;
            this.label15.Text = "E-Way Bill Number";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(45, 426);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(40, 18);
            this.label12.TabIndex = 408;
            this.label12.Text = "LRNo";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(15, 393);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(74, 18);
            this.label5.TabIndex = 406;
            this.label5.Text = "Transpoter";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(946, 417);
            this.label11.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(76, 18);
            this.label11.TabIndex = 405;
            this.label11.Text = "Total Value";
            // 
            // txtBaseTotal
            // 
            this.txtBaseTotal.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtBaseTotal.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtBaseTotal.Location = new System.Drawing.Point(1028, 415);
            this.txtBaseTotal.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtBaseTotal.Name = "txtBaseTotal";
            this.txtBaseTotal.ReadOnly = true;
            this.txtBaseTotal.Size = new System.Drawing.Size(109, 26);
            this.txtBaseTotal.TabIndex = 404;
            this.txtBaseTotal.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // grSearch
            // 
            this.grSearch.BackColor = System.Drawing.Color.White;
            this.grSearch.Controls.Add(this.DataGridCommon);
            this.grSearch.Controls.Add(this.btnSelect);
            this.grSearch.Controls.Add(this.btnHide);
            this.grSearch.Location = new System.Drawing.Point(273, 52);
            this.grSearch.Name = "grSearch";
            this.grSearch.Size = new System.Drawing.Size(382, 300);
            this.grSearch.TabIndex = 393;
            this.grSearch.Visible = false;
            // 
            // DataGridCommon
            // 
            this.DataGridCommon.AllowUserToAddRows = false;
            this.DataGridCommon.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.DataGridCommon.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DataGridCommon.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.DataGridCommon.Location = new System.Drawing.Point(4, 4);
            this.DataGridCommon.Name = "DataGridCommon";
            this.DataGridCommon.ReadOnly = true;
            this.DataGridCommon.RowHeadersVisible = false;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.DataGridCommon.RowsDefaultCellStyle = dataGridViewCellStyle2;
            this.DataGridCommon.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.DataGridCommon.Size = new System.Drawing.Size(375, 264);
            this.DataGridCommon.TabIndex = 0;
            this.DataGridCommon.CellMouseDoubleClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.DataGridCommon_CellMouseDoubleClick);
            this.DataGridCommon.KeyDown += new System.Windows.Forms.KeyEventHandler(this.DataGridCommon_KeyDown);
            // 
            // btnSelect
            // 
            this.btnSelect.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnSelect.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSelect.Image = global::ACV.Properties.Resources.ok;
            this.btnSelect.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.btnSelect.Location = new System.Drawing.Point(175, 268);
            this.btnSelect.Name = "btnSelect";
            this.btnSelect.Size = new System.Drawing.Size(100, 28);
            this.btnSelect.TabIndex = 394;
            this.btnSelect.Text = "Select (F6)";
            this.btnSelect.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnSelect.UseVisualStyleBackColor = false;
            this.btnSelect.Click += new System.EventHandler(this.BtnSelect_Click);
            // 
            // btnHide
            // 
            this.btnHide.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnHide.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnHide.Image = ((System.Drawing.Image)(resources.GetObject("btnHide.Image")));
            this.btnHide.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnHide.Location = new System.Drawing.Point(277, 269);
            this.btnHide.Name = "btnHide";
            this.btnHide.Size = new System.Drawing.Size(100, 27);
            this.btnHide.TabIndex = 393;
            this.btnHide.Text = "Close (F7)";
            this.btnHide.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnHide.UseVisualStyleBackColor = false;
            this.btnHide.Click += new System.EventHandler(this.BtnHide_Click);
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(952, 446);
            this.label14.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(70, 18);
            this.label14.TabIndex = 388;
            this.label14.Text = "Round Off";
            // 
            // txtRoundOff
            // 
            this.txtRoundOff.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtRoundOff.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtRoundOff.Location = new System.Drawing.Point(1028, 442);
            this.txtRoundOff.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtRoundOff.Name = "txtRoundOff";
            this.txtRoundOff.ReadOnly = true;
            this.txtRoundOff.Size = new System.Drawing.Size(109, 26);
            this.txtRoundOff.TabIndex = 15;
            this.txtRoundOff.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txtNetValue
            // 
            this.txtNetValue.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtNetValue.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNetValue.Location = new System.Drawing.Point(1028, 469);
            this.txtNetValue.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtNetValue.Name = "txtNetValue";
            this.txtNetValue.ReadOnly = true;
            this.txtNetValue.Size = new System.Drawing.Size(109, 26);
            this.txtNetValue.TabIndex = 382;
            this.txtNetValue.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.Location = new System.Drawing.Point(954, 474);
            this.label22.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(68, 18);
            this.label22.TabIndex = 381;
            this.label22.Text = "Net value";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.Location = new System.Drawing.Point(956, 393);
            this.label24.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(66, 18);
            this.label24.TabIndex = 386;
            this.label24.Text = "Tax Value";
            // 
            // txttotalValue
            // 
            this.txttotalValue.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txttotalValue.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txttotalValue.Location = new System.Drawing.Point(1028, 388);
            this.txttotalValue.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txttotalValue.Name = "txttotalValue";
            this.txttotalValue.ReadOnly = true;
            this.txttotalValue.Size = new System.Drawing.Size(109, 26);
            this.txttotalValue.TabIndex = 385;
            this.txttotalValue.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txtBasic
            // 
            this.txtBasic.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtBasic.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtBasic.Location = new System.Drawing.Point(1028, 361);
            this.txtBasic.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtBasic.Name = "txtBasic";
            this.txtBasic.ReadOnly = true;
            this.txtBasic.Size = new System.Drawing.Size(109, 26);
            this.txtBasic.TabIndex = 379;
            this.txtBasic.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.Location = new System.Drawing.Point(950, 365);
            this.label20.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(72, 18);
            this.label20.TabIndex = 378;
            this.label20.Text = "Basic Total";
            // 
            // DataGridFabricItem
            // 
            this.DataGridFabricItem.AllowUserToAddRows = false;
            this.DataGridFabricItem.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.ActiveCaption;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.DataGridFabricItem.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.DataGridFabricItem.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DataGridFabricItem.EnableHeadersVisualStyles = false;
            this.DataGridFabricItem.Location = new System.Drawing.Point(5, 67);
            this.DataGridFabricItem.Name = "DataGridFabricItem";
            this.DataGridFabricItem.RowHeadersVisible = false;
            this.DataGridFabricItem.Size = new System.Drawing.Size(1130, 289);
            this.DataGridFabricItem.TabIndex = 367;
            this.DataGridFabricItem.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.DataGridFabricItem_CellEndEdit);
            this.DataGridFabricItem.CellMouseDoubleClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.DataGridFabricItem_CellMouseDoubleClick);
            this.DataGridFabricItem.KeyDown += new System.Windows.Forms.KeyEventHandler(this.DataGridFabricItem_KeyDown);
            // 
            // txtVehicle
            // 
            this.txtVehicle.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtVehicle.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtVehicle.Location = new System.Drawing.Point(273, 36);
            this.txtVehicle.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtVehicle.Name = "txtVehicle";
            this.txtVehicle.Size = new System.Drawing.Size(191, 26);
            this.txtVehicle.TabIndex = 1;
            this.txtVehicle.Click += new System.EventHandler(this.TxtVehicle_Click);
            this.txtVehicle.TextChanged += new System.EventHandler(this.TxtVehicle_TextChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(273, 16);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(54, 18);
            this.label1.TabIndex = 198;
            this.label1.Text = "Vehicle";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(149, 16);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(63, 18);
            this.label2.TabIndex = 196;
            this.label2.Text = "Doc Date";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(657, 17);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(110, 18);
            this.label3.TabIndex = 193;
            this.label3.Text = "Consignor Name";
            // 
            // Phone
            // 
            this.Phone.AutoSize = true;
            this.Phone.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Phone.Location = new System.Drawing.Point(5, 17);
            this.Phone.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.Phone.Name = "Phone";
            this.Phone.Size = new System.Drawing.Size(49, 18);
            this.Phone.TabIndex = 195;
            this.Phone.Text = "DocNo";
            // 
            // txtDocNo
            // 
            this.txtDocNo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtDocNo.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDocNo.Location = new System.Drawing.Point(5, 37);
            this.txtDocNo.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtDocNo.Name = "txtDocNo";
            this.txtDocNo.Size = new System.Drawing.Size(143, 26);
            this.txtDocNo.TabIndex = 194;
            // 
            // dtpGrnDate
            // 
            this.dtpGrnDate.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpGrnDate.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpGrnDate.Location = new System.Drawing.Point(149, 37);
            this.dtpGrnDate.Name = "dtpGrnDate";
            this.dtpGrnDate.Size = new System.Drawing.Size(122, 26);
            this.dtpGrnDate.TabIndex = 197;
            // 
            // txtSupplerName
            // 
            this.txtSupplerName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtSupplerName.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSupplerName.Location = new System.Drawing.Point(657, 37);
            this.txtSupplerName.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtSupplerName.Name = "txtSupplerName";
            this.txtSupplerName.Size = new System.Drawing.Size(221, 26);
            this.txtSupplerName.TabIndex = 2;
            this.txtSupplerName.Click += new System.EventHandler(this.TxtSupplerName_Click);
            this.txtSupplerName.TextChanged += new System.EventHandler(this.txtSupplerName_TextChanged);
            this.txtSupplerName.Enter += new System.EventHandler(this.txtSupplerName_Enter);
            // 
            // DataGridTax
            // 
            this.DataGridTax.AllowUserToAddRows = false;
            this.DataGridTax.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.ActiveCaption;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.DataGridTax.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle4;
            this.DataGridTax.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DataGridTax.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.DataGridTax.EnableHeadersVisualStyles = false;
            this.DataGridTax.Location = new System.Drawing.Point(96, 451);
            this.DataGridTax.Name = "DataGridTax";
            this.DataGridTax.RowHeadersVisible = false;
            this.DataGridTax.Size = new System.Drawing.Size(523, 114);
            this.DataGridTax.TabIndex = 9;
            // 
            // txtDispatchSlip
            // 
            this.txtDispatchSlip.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtDispatchSlip.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDispatchSlip.Location = new System.Drawing.Point(465, 36);
            this.txtDispatchSlip.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtDispatchSlip.Name = "txtDispatchSlip";
            this.txtDispatchSlip.Size = new System.Drawing.Size(190, 26);
            this.txtDispatchSlip.TabIndex = 402;
            this.txtDispatchSlip.TextChanged += new System.EventHandler(this.txtDispatchSlip_TextChanged);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(465, 15);
            this.label8.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(102, 18);
            this.label8.TabIndex = 403;
            this.label8.Text = "DispatchSlipNo";
            // 
            // txtRemarks
            // 
            this.txtRemarks.Location = new System.Drawing.Point(96, 358);
            this.txtRemarks.Name = "txtRemarks";
            this.txtRemarks.Size = new System.Drawing.Size(438, 26);
            this.txtRemarks.TabIndex = 4;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(28, 361);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(61, 18);
            this.label6.TabIndex = 396;
            this.label6.Text = "Remarks";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(7, 458);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(88, 18);
            this.label4.TabIndex = 401;
            this.label4.Text = "Tax Structure";
            // 
            // txtTranspoter
            // 
            this.txtTranspoter.Location = new System.Drawing.Point(96, 390);
            this.txtTranspoter.Name = "txtTranspoter";
            this.txtTranspoter.Size = new System.Drawing.Size(326, 26);
            this.txtTranspoter.TabIndex = 5;
            // 
            // txtLRNo
            // 
            this.txtLRNo.Location = new System.Drawing.Point(96, 422);
            this.txtLRNo.Name = "txtLRNo";
            this.txtLRNo.Size = new System.Drawing.Size(326, 26);
            this.txtLRNo.TabIndex = 7;
            // 
            // txtDispatchTo
            // 
            this.txtDispatchTo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtDispatchTo.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDispatchTo.Location = new System.Drawing.Point(886, 37);
            this.txtDispatchTo.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtDispatchTo.Name = "txtDispatchTo";
            this.txtDispatchTo.Size = new System.Drawing.Size(248, 26);
            this.txtDispatchTo.TabIndex = 3;
            this.txtDispatchTo.Click += new System.EventHandler(this.txtDispatchTo_Click);
            this.txtDispatchTo.TextChanged += new System.EventHandler(this.txtDispatchTo_TextChanged);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(886, 17);
            this.label13.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(78, 18);
            this.label13.TabIndex = 410;
            this.label13.Text = "Dispatch To";
            // 
            // txtEwayBill
            // 
            this.txtEwayBill.Location = new System.Drawing.Point(557, 388);
            this.txtEwayBill.Name = "txtEwayBill";
            this.txtEwayBill.Size = new System.Drawing.Size(181, 26);
            this.txtEwayBill.TabIndex = 6;
            // 
            // txtFrom
            // 
            this.txtFrom.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.txtFrom.FormattingEnabled = true;
            this.txtFrom.Location = new System.Drawing.Point(628, 451);
            this.txtFrom.Name = "txtFrom";
            this.txtFrom.Size = new System.Drawing.Size(191, 26);
            this.txtFrom.TabIndex = 0;
            this.txtFrom.Visible = false;
            // 
            // cmbFor
            // 
            this.cmbFor.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbFor.FormattingEnabled = true;
            this.cmbFor.Location = new System.Drawing.Point(459, 422);
            this.cmbFor.Name = "cmbFor";
            this.cmbFor.Size = new System.Drawing.Size(156, 26);
            this.cmbFor.TabIndex = 8;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(431, 426);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(28, 18);
            this.label7.TabIndex = 398;
            this.label7.Text = "For";
            // 
            // DataGridDet
            // 
            this.DataGridDet.AllowUserToAddRows = false;
            this.DataGridDet.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle5.BackColor = System.Drawing.SystemColors.ActiveCaption;
            dataGridViewCellStyle5.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle5.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.DataGridDet.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle5;
            this.DataGridDet.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DataGridDet.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.DataGridDet.EnableHeadersVisualStyles = false;
            this.DataGridDet.Location = new System.Drawing.Point(6, 25);
            this.DataGridDet.Name = "DataGridDet";
            this.DataGridDet.RowHeadersVisible = false;
            this.DataGridDet.Size = new System.Drawing.Size(657, 416);
            this.DataGridDet.TabIndex = 377;
            // 
            // grdetView
            // 
            this.grdetView.Controls.Add(this.btnClose);
            this.grdetView.Controls.Add(this.label10);
            this.grdetView.Controls.Add(this.label9);
            this.grdetView.Controls.Add(this.DataGridDet);
            this.grdetView.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grdetView.Location = new System.Drawing.Point(6, 3);
            this.grdetView.Name = "grdetView";
            this.grdetView.Size = new System.Drawing.Size(672, 571);
            this.grdetView.TabIndex = 395;
            this.grdetView.TabStop = false;
            this.grdetView.Text = "Detail View";
            // 
            // btnClose
            // 
            this.btnClose.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnClose.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClose.Image = ((System.Drawing.Image)(resources.GetObject("btnClose.Image")));
            this.btnClose.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnClose.Location = new System.Drawing.Point(594, 535);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(69, 30);
            this.btnClose.TabIndex = 380;
            this.btnClose.Text = "Close";
            this.btnClose.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnClose.UseVisualStyleBackColor = false;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(14, 476);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(97, 18);
            this.label10.TabIndex = 379;
            this.label10.Text = "Totalm Meters";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(14, 453);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(75, 18);
            this.label9.TabIndex = 378;
            this.label9.Text = "No of Rolls";
            // 
            // FrmFabricIssue
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.ClientSize = new System.Drawing.Size(1155, 615);
            this.Controls.Add(this.panadd);
            this.Controls.Add(this.grdetView);
            this.Controls.Add(this.grFront);
            this.Controls.Add(this.grBack);
            this.Name = "FrmFabricIssue";
            this.Text = "Fabric Issue";
            this.Load += new System.EventHandler(this.FrmFabricIssue_Load);
            this.grFront.ResumeLayout(false);
            this.grFront.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridFabIssue)).EndInit();
            this.panadd.ResumeLayout(false);
            this.panadd.PerformLayout();
            this.panNumbers.ResumeLayout(false);
            this.panNumbers.PerformLayout();
            this.grBack.ResumeLayout(false);
            this.grBack.PerformLayout();
            this.GrRollUpdate.ResumeLayout(false);
            this.GrRollUpdate.PerformLayout();
            this.grSearch.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.DataGridCommon)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridFabricItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridTax)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridDet)).EndInit();
            this.grdetView.ResumeLayout(false);
            this.grdetView.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox grFront;
        private System.Windows.Forms.TextBox txtSearch;
        private System.Windows.Forms.DataGridView DataGridFabIssue;
        private System.Windows.Forms.Panel panadd;
        private System.Windows.Forms.Button btnDelete;
        private System.Windows.Forms.Panel panNumbers;
        private System.Windows.Forms.Label lblFrom;
        private System.Windows.Forms.Label lblCount;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel3;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel2;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private System.Windows.Forms.Button btnFstBack;
        private System.Windows.Forms.Button btnBack;
        private System.Windows.Forms.Button btnFstNext;
        private System.Windows.Forms.Button btnNext;
        private System.Windows.Forms.CheckBox chckActive;
        private System.Windows.Forms.Button btnAdd;
        private System.Windows.Forms.Button btnPrint;
        private System.Windows.Forms.Button btnExit;
        private System.Windows.Forms.Button btnaddrcan;
        private System.Windows.Forms.Button btnEdit;
        private System.Windows.Forms.Button btnsave;
        private System.Windows.Forms.GroupBox grBack;
        private System.Windows.Forms.Panel grSearch;
        private System.Windows.Forms.Button btnSelect;
        private System.Windows.Forms.Button btnHide;
        private System.Windows.Forms.DataGridView DataGridCommon;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox txtRoundOff;
        private System.Windows.Forms.TextBox txtNetValue;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.TextBox txttotalValue;
        private System.Windows.Forms.TextBox txtBasic;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.DataGridView DataGridDet;
        private System.Windows.Forms.DataGridView DataGridFabricItem;
        private System.Windows.Forms.TextBox txtVehicle;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label Phone;
        private System.Windows.Forms.TextBox txtDocNo;
        private System.Windows.Forms.DateTimePicker dtpGrnDate;
        private System.Windows.Forms.TextBox txtSupplerName;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ComboBox cmbFor;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtRemarks;
        private System.Windows.Forms.DataGridView DataGridTax;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txtDispatchSlip;
        private System.Windows.Forms.GroupBox grdetView;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox txtBaseTotal;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox txtTranspoter;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtLRNo;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Button btnPackingSlipPrint;
        private System.Windows.Forms.TextBox txtDispatchTo;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox txtEwayBill;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox txtVechNo;
        private System.Windows.Forms.ComboBox txtFrom;
        private System.Windows.Forms.DateTimePicker DtpMonth;
        private System.Windows.Forms.Button btnUpdateRoll;
        private System.Windows.Forms.GroupBox GrRollUpdate;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.TextBox txtRollNo;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.TextBox txtSortNo;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.TextBox txtMeter;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.TextBox txtWeight;
        private System.Windows.Forms.Button btnRollNoClose;
        private System.Windows.Forms.Button btnRollNoSave;
        private System.Windows.Forms.Button BtnRECONCILE;
    }
}