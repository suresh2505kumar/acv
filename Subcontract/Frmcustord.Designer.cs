﻿namespace ACV
{
    partial class Frmcustord
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Frmcustord));
            this.Genpan = new System.Windows.Forms.Panel();
            this.HFGP = new System.Windows.Forms.DataGridView();
            this.chkact = new System.Windows.Forms.CheckBox();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.txtscr5 = new System.Windows.Forms.TextBox();
            this.txtscr4 = new System.Windows.Forms.TextBox();
            this.Txtscr3 = new System.Windows.Forms.TextBox();
            this.Txtscr2 = new System.Windows.Forms.TextBox();
            this.btnexit = new System.Windows.Forms.Button();
            this.btndelete = new System.Windows.Forms.Button();
            this.btnedit = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.txtscr1 = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.Editpan = new System.Windows.Forms.Panel();
            this.button4 = new System.Windows.Forms.Button();
            this.Remarks = new System.Windows.Forms.Label();
            this.txtrem = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.txtvalue = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtrate = new System.Windows.Forms.TextBox();
            this.button3 = new System.Windows.Forms.Button();
            this.txtamt = new System.Windows.Forms.TextBox();
            this.button2 = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.DTPDOCDT = new System.Windows.Forms.DateTimePicker();
            this.HFIT = new System.Windows.Forms.DataGridView();
            this.Dtpdt = new System.Windows.Forms.DateTimePicker();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.txtdcno = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtitem = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtname = new System.Windows.Forms.TextBox();
            this.Phone = new System.Windows.Forms.Label();
            this.txtdocno = new System.Windows.Forms.TextBox();
            this.txtpuid = new System.Windows.Forms.TextBox();
            this.txtqty = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.txtgrnid = new System.Windows.Forms.TextBox();
            this.txtitid = new System.Windows.Forms.TextBox();
            this.Taxpan = new System.Windows.Forms.Panel();
            this.txtexcise = new System.Windows.Forms.TextBox();
            this.btnsave = new System.Windows.Forms.Button();
            this.btnaddrcan = new System.Windows.Forms.Button();
            this.label17 = new System.Windows.Forms.Label();
            this.TxtNetAmt = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.TxtRoff = new System.Windows.Forms.TextBox();
            this.txttax = new System.Windows.Forms.TextBox();
            this.cbotax = new System.Windows.Forms.ComboBox();
            this.label15 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.txtcess = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.txthecess = new System.Windows.Forms.TextBox();
            this.txtted = new System.Windows.Forms.TextBox();
            this.cboexcise = new System.Windows.Forms.ComboBox();
            this.label12 = new System.Windows.Forms.Label();
            this.txtpf = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.txtdis = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.txtper = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.txtamttot = new System.Windows.Forms.TextBox();
            this.LblED1 = new System.Windows.Forms.TextBox();
            this.LblCess = new System.Windows.Forms.TextBox();
            this.LblHECess = new System.Windows.Forms.TextBox();
            this.lbltax = new System.Windows.Forms.TextBox();
            this.Genpan.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.HFGP)).BeginInit();
            this.Editpan.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.HFIT)).BeginInit();
            this.Taxpan.SuspendLayout();
            this.SuspendLayout();
            // 
            // Genpan
            // 
            this.Genpan.Controls.Add(this.HFGP);
            this.Genpan.Controls.Add(this.chkact);
            this.Genpan.Controls.Add(this.textBox1);
            this.Genpan.Controls.Add(this.txtscr5);
            this.Genpan.Controls.Add(this.txtscr4);
            this.Genpan.Controls.Add(this.Txtscr3);
            this.Genpan.Controls.Add(this.Txtscr2);
            this.Genpan.Controls.Add(this.btnexit);
            this.Genpan.Controls.Add(this.btndelete);
            this.Genpan.Controls.Add(this.btnedit);
            this.Genpan.Controls.Add(this.button1);
            this.Genpan.Controls.Add(this.txtscr1);
            this.Genpan.Location = new System.Drawing.Point(13, 38);
            this.Genpan.Margin = new System.Windows.Forms.Padding(4);
            this.Genpan.Name = "Genpan";
            this.Genpan.Size = new System.Drawing.Size(1064, 496);
            this.Genpan.TabIndex = 188;
            // 
            // HFGP
            // 
            this.HFGP.AllowUserToDeleteRows = false;
            this.HFGP.AllowUserToOrderColumns = true;
            this.HFGP.BackgroundColor = System.Drawing.Color.White;
            this.HFGP.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.HFGP.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.HFGP.Location = new System.Drawing.Point(16, 40);
            this.HFGP.Name = "HFGP";
            this.HFGP.Size = new System.Drawing.Size(903, 413);
            this.HFGP.TabIndex = 104;
            // 
            // chkact
            // 
            this.chkact.AutoSize = true;
            this.chkact.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkact.Location = new System.Drawing.Point(967, 117);
            this.chkact.Name = "chkact";
            this.chkact.Size = new System.Drawing.Size(70, 20);
            this.chkact.TabIndex = 103;
            this.chkact.Text = "Active";
            // 
            // textBox1
            // 
            this.textBox1.BackColor = System.Drawing.Color.Silver;
            this.textBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox1.Location = new System.Drawing.Point(800, 461);
            this.textBox1.Margin = new System.Windows.Forms.Padding(5);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(119, 22);
            this.textBox1.TabIndex = 101;
            // 
            // txtscr5
            // 
            this.txtscr5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtscr5.Location = new System.Drawing.Point(489, 11);
            this.txtscr5.Margin = new System.Windows.Forms.Padding(5);
            this.txtscr5.Name = "txtscr5";
            this.txtscr5.Size = new System.Drawing.Size(430, 22);
            this.txtscr5.TabIndex = 90;
            // 
            // txtscr4
            // 
            this.txtscr4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtscr4.Location = new System.Drawing.Point(370, 11);
            this.txtscr4.Margin = new System.Windows.Forms.Padding(5);
            this.txtscr4.Name = "txtscr4";
            this.txtscr4.Size = new System.Drawing.Size(119, 22);
            this.txtscr4.TabIndex = 100;
            // 
            // Txtscr3
            // 
            this.Txtscr3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txtscr3.Location = new System.Drawing.Point(255, 11);
            this.Txtscr3.Margin = new System.Windows.Forms.Padding(5);
            this.Txtscr3.Name = "Txtscr3";
            this.Txtscr3.Size = new System.Drawing.Size(119, 22);
            this.Txtscr3.TabIndex = 88;
            // 
            // Txtscr2
            // 
            this.Txtscr2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txtscr2.Location = new System.Drawing.Point(137, 11);
            this.Txtscr2.Margin = new System.Windows.Forms.Padding(5);
            this.Txtscr2.Name = "Txtscr2";
            this.Txtscr2.Size = new System.Drawing.Size(119, 22);
            this.Txtscr2.TabIndex = 87;
            // 
            // btnexit
            // 
            this.btnexit.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnexit.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnexit.Image = ((System.Drawing.Image)(resources.GetObject("btnexit.Image")));
            this.btnexit.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnexit.Location = new System.Drawing.Point(951, 330);
            this.btnexit.Margin = new System.Windows.Forms.Padding(4);
            this.btnexit.Name = "btnexit";
            this.btnexit.Size = new System.Drawing.Size(87, 37);
            this.btnexit.TabIndex = 86;
            this.btnexit.Text = "Exit";
            this.btnexit.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnexit.UseVisualStyleBackColor = false;
            this.btnexit.Click += new System.EventHandler(this.btnexit_Click_1);
            // 
            // btndelete
            // 
            this.btndelete.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btndelete.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btndelete.Image = ((System.Drawing.Image)(resources.GetObject("btndelete.Image")));
            this.btndelete.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btndelete.Location = new System.Drawing.Point(950, 277);
            this.btndelete.Margin = new System.Windows.Forms.Padding(4);
            this.btndelete.Name = "btndelete";
            this.btndelete.Size = new System.Drawing.Size(88, 37);
            this.btndelete.TabIndex = 85;
            this.btndelete.Text = "Delete";
            this.btndelete.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btndelete.UseVisualStyleBackColor = false;
            // 
            // btnedit
            // 
            this.btnedit.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnedit.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnedit.Image = ((System.Drawing.Image)(resources.GetObject("btnedit.Image")));
            this.btnedit.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.btnedit.Location = new System.Drawing.Point(950, 222);
            this.btnedit.Margin = new System.Windows.Forms.Padding(4);
            this.btnedit.Name = "btnedit";
            this.btnedit.Size = new System.Drawing.Size(87, 39);
            this.btnedit.TabIndex = 84;
            this.btnedit.Text = "Edit";
            this.btnedit.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnedit.UseVisualStyleBackColor = false;
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.button1.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.Image = ((System.Drawing.Image)(resources.GetObject("button1.Image")));
            this.button1.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.button1.Location = new System.Drawing.Point(949, 165);
            this.button1.Margin = new System.Windows.Forms.Padding(4);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(88, 41);
            this.button1.TabIndex = 83;
            this.button1.Text = "Add";
            this.button1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click_1);
            // 
            // txtscr1
            // 
            this.txtscr1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtscr1.Location = new System.Drawing.Point(19, 11);
            this.txtscr1.Margin = new System.Windows.Forms.Padding(5);
            this.txtscr1.Name = "txtscr1";
            this.txtscr1.Size = new System.Drawing.Size(119, 22);
            this.txtscr1.TabIndex = 1;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(28, 9);
            this.label7.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(136, 20);
            this.label7.TabIndex = 201;
            this.label7.Text = "Customer Order";
            // 
            // Editpan
            // 
            this.Editpan.Controls.Add(this.button4);
            this.Editpan.Controls.Add(this.Remarks);
            this.Editpan.Controls.Add(this.txtrem);
            this.Editpan.Controls.Add(this.label8);
            this.Editpan.Controls.Add(this.txtvalue);
            this.Editpan.Controls.Add(this.label4);
            this.Editpan.Controls.Add(this.txtrate);
            this.Editpan.Controls.Add(this.button3);
            this.Editpan.Controls.Add(this.txtamt);
            this.Editpan.Controls.Add(this.button2);
            this.Editpan.Controls.Add(this.label1);
            this.Editpan.Controls.Add(this.DTPDOCDT);
            this.Editpan.Controls.Add(this.HFIT);
            this.Editpan.Controls.Add(this.Dtpdt);
            this.Editpan.Controls.Add(this.label6);
            this.Editpan.Controls.Add(this.label5);
            this.Editpan.Controls.Add(this.txtdcno);
            this.Editpan.Controls.Add(this.label2);
            this.Editpan.Controls.Add(this.txtitem);
            this.Editpan.Controls.Add(this.label3);
            this.Editpan.Controls.Add(this.txtname);
            this.Editpan.Controls.Add(this.Phone);
            this.Editpan.Controls.Add(this.txtdocno);
            this.Editpan.Controls.Add(this.txtpuid);
            this.Editpan.Controls.Add(this.txtqty);
            this.Editpan.Controls.Add(this.label11);
            this.Editpan.Controls.Add(this.txtgrnid);
            this.Editpan.Controls.Add(this.txtitid);
            this.Editpan.Location = new System.Drawing.Point(13, 38);
            this.Editpan.Margin = new System.Windows.Forms.Padding(4);
            this.Editpan.Name = "Editpan";
            this.Editpan.Size = new System.Drawing.Size(1062, 496);
            this.Editpan.TabIndex = 202;
            // 
            // button4
            // 
            this.button4.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.button4.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button4.Image = global::ACV.Properties.Resources.ok1;
            this.button4.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button4.Location = new System.Drawing.Point(713, 136);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(38, 36);
            this.button4.TabIndex = 243;
            this.button4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button4.UseVisualStyleBackColor = false;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // Remarks
            // 
            this.Remarks.AutoSize = true;
            this.Remarks.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Remarks.Location = new System.Drawing.Point(444, 71);
            this.Remarks.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.Remarks.Name = "Remarks";
            this.Remarks.Size = new System.Drawing.Size(76, 18);
            this.Remarks.TabIndex = 234;
            this.Remarks.Text = "Remarks";
            // 
            // txtrem
            // 
            this.txtrem.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtrem.Location = new System.Drawing.Point(450, 95);
            this.txtrem.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.txtrem.MaxLength = 100;
            this.txtrem.Name = "txtrem";
            this.txtrem.Size = new System.Drawing.Size(492, 22);
            this.txtrem.TabIndex = 233;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(611, 119);
            this.label8.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(49, 18);
            this.label8.TabIndex = 232;
            this.label8.Text = "Value";
            // 
            // txtvalue
            // 
            this.txtvalue.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtvalue.Location = new System.Drawing.Point(611, 143);
            this.txtvalue.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.txtvalue.Name = "txtvalue";
            this.txtvalue.Size = new System.Drawing.Size(94, 22);
            this.txtvalue.TabIndex = 231;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(529, 119);
            this.label4.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(47, 18);
            this.label4.TabIndex = 230;
            this.label4.Text = "Price";
            // 
            // txtrate
            // 
            this.txtrate.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtrate.Location = new System.Drawing.Point(529, 143);
            this.txtrate.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.txtrate.Name = "txtrate";
            this.txtrate.Size = new System.Drawing.Size(72, 22);
            this.txtrate.TabIndex = 229;
            this.txtrate.TextChanged += new System.EventHandler(this.txtrate_TextChanged);
            // 
            // button3
            // 
            this.button3.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.button3.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button3.Image = ((System.Drawing.Image)(resources.GetObject("button3.Image")));
            this.button3.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button3.Location = new System.Drawing.Point(1012, 549);
            this.button3.Margin = new System.Windows.Forms.Padding(4);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(124, 47);
            this.button3.TabIndex = 227;
            this.button3.Text = "Back";
            this.button3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button3.UseVisualStyleBackColor = false;
            // 
            // txtamt
            // 
            this.txtamt.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtamt.Location = new System.Drawing.Point(775, 559);
            this.txtamt.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.txtamt.Name = "txtamt";
            this.txtamt.Size = new System.Drawing.Size(161, 22);
            this.txtamt.TabIndex = 226;
            // 
            // button2
            // 
            this.button2.BackColor = System.Drawing.Color.White;
            this.button2.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button2.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button2.Location = new System.Drawing.Point(1144, 550);
            this.button2.Margin = new System.Windows.Forms.Padding(4);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(113, 46);
            this.button2.TabIndex = 224;
            this.button2.Text = "Tax";
            this.button2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button2.UseVisualStyleBackColor = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(447, 119);
            this.label1.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(34, 18);
            this.label1.TabIndex = 223;
            this.label1.Text = "Oty";
            // 
            // DTPDOCDT
            // 
            this.DTPDOCDT.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DTPDOCDT.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.DTPDOCDT.Location = new System.Drawing.Point(190, 43);
            this.DTPDOCDT.Margin = new System.Windows.Forms.Padding(4);
            this.DTPDOCDT.Name = "DTPDOCDT";
            this.DTPDOCDT.Size = new System.Drawing.Size(113, 22);
            this.DTPDOCDT.TabIndex = 221;
            // 
            // HFIT
            // 
            this.HFIT.BackgroundColor = System.Drawing.Color.White;
            this.HFIT.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.HFIT.Location = new System.Drawing.Point(30, 175);
            this.HFIT.Margin = new System.Windows.Forms.Padding(4);
            this.HFIT.Name = "HFIT";
            this.HFIT.Size = new System.Drawing.Size(913, 295);
            this.HFIT.TabIndex = 214;
            // 
            // Dtpdt
            // 
            this.Dtpdt.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Dtpdt.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.Dtpdt.Location = new System.Drawing.Point(482, 43);
            this.Dtpdt.Margin = new System.Windows.Forms.Padding(4);
            this.Dtpdt.Name = "Dtpdt";
            this.Dtpdt.Size = new System.Drawing.Size(113, 22);
            this.Dtpdt.TabIndex = 205;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(479, 19);
            this.label6.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(110, 18);
            this.label6.TabIndex = 204;
            this.label6.Text = "Ref.Doc.Date";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(309, 23);
            this.label5.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(97, 18);
            this.label5.TabIndex = 203;
            this.label5.Text = "Ref.Doc.No";
            // 
            // txtdcno
            // 
            this.txtdcno.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtdcno.Location = new System.Drawing.Point(312, 43);
            this.txtdcno.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.txtdcno.Name = "txtdcno";
            this.txtdcno.Size = new System.Drawing.Size(161, 22);
            this.txtdcno.TabIndex = 202;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(187, 23);
            this.label2.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(79, 18);
            this.label2.TabIndex = 201;
            this.label2.Text = "Doc.Date";
            // 
            // txtitem
            // 
            this.txtitem.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtitem.Location = new System.Drawing.Point(29, 143);
            this.txtitem.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.txtitem.Name = "txtitem";
            this.txtitem.Size = new System.Drawing.Size(411, 22);
            this.txtitem.TabIndex = 196;
            this.txtitem.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtitem_KeyDown);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(26, 71);
            this.label3.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(96, 18);
            this.label3.TabIndex = 195;
            this.label3.Text = "Party Name";
            // 
            // txtname
            // 
            this.txtname.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtname.Location = new System.Drawing.Point(30, 95);
            this.txtname.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.txtname.MaxLength = 100;
            this.txtname.Name = "txtname";
            this.txtname.Size = new System.Drawing.Size(411, 22);
            this.txtname.TabIndex = 194;
            this.txtname.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtname_KeyDown_1);
            // 
            // Phone
            // 
            this.Phone.AutoSize = true;
            this.Phone.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Phone.Location = new System.Drawing.Point(26, 20);
            this.Phone.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.Phone.Name = "Phone";
            this.Phone.Size = new System.Drawing.Size(66, 18);
            this.Phone.TabIndex = 199;
            this.Phone.Text = "Doc.No";
            // 
            // txtdocno
            // 
            this.txtdocno.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtdocno.Location = new System.Drawing.Point(29, 43);
            this.txtdocno.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.txtdocno.Name = "txtdocno";
            this.txtdocno.Size = new System.Drawing.Size(161, 22);
            this.txtdocno.TabIndex = 198;
            // 
            // txtpuid
            // 
            this.txtpuid.Location = new System.Drawing.Point(216, 223);
            this.txtpuid.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.txtpuid.Name = "txtpuid";
            this.txtpuid.Size = new System.Drawing.Size(87, 20);
            this.txtpuid.TabIndex = 217;
            // 
            // txtqty
            // 
            this.txtqty.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtqty.Location = new System.Drawing.Point(447, 143);
            this.txtqty.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.txtqty.Name = "txtqty";
            this.txtqty.Size = new System.Drawing.Size(72, 22);
            this.txtqty.TabIndex = 219;
            this.txtqty.TextChanged += new System.EventHandler(this.txtqty_TextChanged);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(26, 119);
            this.label11.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(40, 18);
            this.label11.TabIndex = 218;
            this.label11.Text = "Item";
            // 
            // txtgrnid
            // 
            this.txtgrnid.Location = new System.Drawing.Point(485, 256);
            this.txtgrnid.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.txtgrnid.Name = "txtgrnid";
            this.txtgrnid.Size = new System.Drawing.Size(28, 20);
            this.txtgrnid.TabIndex = 220;
            // 
            // txtitid
            // 
            this.txtitid.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtitid.Location = new System.Drawing.Point(167, 143);
            this.txtitid.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.txtitid.Name = "txtitid";
            this.txtitid.Size = new System.Drawing.Size(23, 22);
            this.txtitid.TabIndex = 228;
            // 
            // Taxpan
            // 
            this.Taxpan.Controls.Add(this.txtexcise);
            this.Taxpan.Controls.Add(this.btnsave);
            this.Taxpan.Controls.Add(this.btnaddrcan);
            this.Taxpan.Controls.Add(this.label17);
            this.Taxpan.Controls.Add(this.TxtNetAmt);
            this.Taxpan.Controls.Add(this.label16);
            this.Taxpan.Controls.Add(this.TxtRoff);
            this.Taxpan.Controls.Add(this.txttax);
            this.Taxpan.Controls.Add(this.cbotax);
            this.Taxpan.Controls.Add(this.label15);
            this.Taxpan.Controls.Add(this.label14);
            this.Taxpan.Controls.Add(this.txtcess);
            this.Taxpan.Controls.Add(this.label13);
            this.Taxpan.Controls.Add(this.txthecess);
            this.Taxpan.Controls.Add(this.txtted);
            this.Taxpan.Controls.Add(this.cboexcise);
            this.Taxpan.Controls.Add(this.label12);
            this.Taxpan.Controls.Add(this.txtpf);
            this.Taxpan.Controls.Add(this.label10);
            this.Taxpan.Controls.Add(this.txtdis);
            this.Taxpan.Controls.Add(this.label9);
            this.Taxpan.Controls.Add(this.txtper);
            this.Taxpan.Controls.Add(this.label18);
            this.Taxpan.Controls.Add(this.txtamttot);
            this.Taxpan.Controls.Add(this.LblED1);
            this.Taxpan.Controls.Add(this.LblCess);
            this.Taxpan.Controls.Add(this.LblHECess);
            this.Taxpan.Controls.Add(this.lbltax);
            this.Taxpan.Location = new System.Drawing.Point(13, 38);
            this.Taxpan.Margin = new System.Windows.Forms.Padding(4);
            this.Taxpan.Name = "Taxpan";
            this.Taxpan.Size = new System.Drawing.Size(1064, 496);
            this.Taxpan.TabIndex = 203;
            // 
            // txtexcise
            // 
            this.txtexcise.Location = new System.Drawing.Point(516, 322);
            this.txtexcise.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.txtexcise.Name = "txtexcise";
            this.txtexcise.Size = new System.Drawing.Size(161, 20);
            this.txtexcise.TabIndex = 226;
            // 
            // btnsave
            // 
            this.btnsave.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnsave.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnsave.Image = ((System.Drawing.Image)(resources.GetObject("btnsave.Image")));
            this.btnsave.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnsave.Location = new System.Drawing.Point(831, 150);
            this.btnsave.Margin = new System.Windows.Forms.Padding(4);
            this.btnsave.Name = "btnsave";
            this.btnsave.Size = new System.Drawing.Size(120, 47);
            this.btnsave.TabIndex = 221;
            this.btnsave.Text = "Save";
            this.btnsave.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnsave.UseVisualStyleBackColor = false;
            // 
            // btnaddrcan
            // 
            this.btnaddrcan.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnaddrcan.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnaddrcan.Image = ((System.Drawing.Image)(resources.GetObject("btnaddrcan.Image")));
            this.btnaddrcan.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnaddrcan.Location = new System.Drawing.Point(831, 213);
            this.btnaddrcan.Margin = new System.Windows.Forms.Padding(4);
            this.btnaddrcan.Name = "btnaddrcan";
            this.btnaddrcan.Size = new System.Drawing.Size(124, 47);
            this.btnaddrcan.TabIndex = 222;
            this.btnaddrcan.Text = "Back";
            this.btnaddrcan.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnaddrcan.UseVisualStyleBackColor = false;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.Location = new System.Drawing.Point(692, 55);
            this.label17.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(96, 18);
            this.label17.TabIndex = 220;
            this.label17.Text = "Net Amount";
            // 
            // TxtNetAmt
            // 
            this.TxtNetAmt.Location = new System.Drawing.Point(831, 57);
            this.TxtNetAmt.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.TxtNetAmt.Name = "TxtNetAmt";
            this.TxtNetAmt.Size = new System.Drawing.Size(161, 20);
            this.TxtNetAmt.TabIndex = 219;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(749, 23);
            this.label16.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(48, 18);
            this.label16.TabIndex = 218;
            this.label16.Text = "R.Off";
            // 
            // TxtRoff
            // 
            this.TxtRoff.Location = new System.Drawing.Point(831, 23);
            this.TxtRoff.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.TxtRoff.Name = "TxtRoff";
            this.TxtRoff.Size = new System.Drawing.Size(161, 20);
            this.TxtRoff.TabIndex = 217;
            // 
            // txttax
            // 
            this.txttax.Location = new System.Drawing.Point(397, 261);
            this.txttax.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.txttax.Name = "txttax";
            this.txttax.Size = new System.Drawing.Size(161, 20);
            this.txttax.TabIndex = 216;
            // 
            // cbotax
            // 
            this.cbotax.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cbotax.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cbotax.FormattingEnabled = true;
            this.cbotax.Location = new System.Drawing.Point(213, 260);
            this.cbotax.Margin = new System.Windows.Forms.Padding(4);
            this.cbotax.Name = "cbotax";
            this.cbotax.Size = new System.Drawing.Size(167, 21);
            this.cbotax.TabIndex = 215;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(99, 260);
            this.label15.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(76, 18);
            this.label15.TabIndex = 214;
            this.label15.Text = "VAT/CST";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(316, 181);
            this.label14.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(47, 18);
            this.label14.TabIndex = 213;
            this.label14.Text = "Cess";
            // 
            // txtcess
            // 
            this.txtcess.Location = new System.Drawing.Point(399, 178);
            this.txtcess.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.txtcess.Name = "txtcess";
            this.txtcess.Size = new System.Drawing.Size(161, 20);
            this.txtcess.TabIndex = 212;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(276, 220);
            this.label13.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(75, 18);
            this.label13.TabIndex = 211;
            this.label13.Text = "HE.Cess";
            // 
            // txthecess
            // 
            this.txthecess.Location = new System.Drawing.Point(397, 218);
            this.txthecess.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.txthecess.Name = "txthecess";
            this.txthecess.Size = new System.Drawing.Size(161, 20);
            this.txthecess.TabIndex = 210;
            // 
            // txtted
            // 
            this.txtted.Location = new System.Drawing.Point(397, 142);
            this.txtted.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.txtted.Name = "txtted";
            this.txtted.Size = new System.Drawing.Size(161, 20);
            this.txtted.TabIndex = 209;
            // 
            // cboexcise
            // 
            this.cboexcise.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboexcise.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboexcise.FormattingEnabled = true;
            this.cboexcise.Location = new System.Drawing.Point(213, 142);
            this.cboexcise.Margin = new System.Windows.Forms.Padding(4);
            this.cboexcise.Name = "cboexcise";
            this.cboexcise.Size = new System.Drawing.Size(167, 21);
            this.cboexcise.TabIndex = 208;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(71, 142);
            this.label12.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(97, 18);
            this.label12.TabIndex = 207;
            this.label12.Text = "Excise Duty";
            // 
            // txtpf
            // 
            this.txtpf.Location = new System.Drawing.Point(399, 102);
            this.txtpf.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.txtpf.Name = "txtpf";
            this.txtpf.Size = new System.Drawing.Size(161, 20);
            this.txtpf.TabIndex = 206;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(323, 105);
            this.label10.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(34, 18);
            this.label10.TabIndex = 205;
            this.label10.Text = "P F";
            // 
            // txtdis
            // 
            this.txtdis.Location = new System.Drawing.Point(399, 60);
            this.txtdis.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.txtdis.Name = "txtdis";
            this.txtdis.Size = new System.Drawing.Size(161, 20);
            this.txtdis.TabIndex = 204;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(204, 59);
            this.label9.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(75, 18);
            this.label9.TabIndex = 203;
            this.label9.Text = "Discount";
            // 
            // txtper
            // 
            this.txtper.Location = new System.Drawing.Point(305, 60);
            this.txtper.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.txtper.Name = "txtper";
            this.txtper.Size = new System.Drawing.Size(81, 20);
            this.txtper.TabIndex = 202;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.Location = new System.Drawing.Point(317, 20);
            this.label18.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(46, 18);
            this.label18.TabIndex = 201;
            this.label18.Text = "Total";
            // 
            // txtamttot
            // 
            this.txtamttot.Location = new System.Drawing.Point(399, 20);
            this.txtamttot.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.txtamttot.Name = "txtamttot";
            this.txtamttot.Size = new System.Drawing.Size(161, 20);
            this.txtamttot.TabIndex = 200;
            // 
            // LblED1
            // 
            this.LblED1.Location = new System.Drawing.Point(397, 145);
            this.LblED1.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.LblED1.Name = "LblED1";
            this.LblED1.Size = new System.Drawing.Size(61, 20);
            this.LblED1.TabIndex = 227;
            // 
            // LblCess
            // 
            this.LblCess.Location = new System.Drawing.Point(397, 182);
            this.LblCess.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.LblCess.Name = "LblCess";
            this.LblCess.Size = new System.Drawing.Size(61, 20);
            this.LblCess.TabIndex = 229;
            // 
            // LblHECess
            // 
            this.LblHECess.Location = new System.Drawing.Point(397, 218);
            this.LblHECess.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.LblHECess.Name = "LblHECess";
            this.LblHECess.Size = new System.Drawing.Size(61, 20);
            this.LblHECess.TabIndex = 228;
            // 
            // lbltax
            // 
            this.lbltax.Location = new System.Drawing.Point(448, 262);
            this.lbltax.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.lbltax.Name = "lbltax";
            this.lbltax.Size = new System.Drawing.Size(43, 20);
            this.lbltax.TabIndex = 230;
            // 
            // Frmcustord
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1123, 562);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.Taxpan);
            this.Controls.Add(this.Editpan);
            this.Controls.Add(this.Genpan);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "Frmcustord";
            this.Text = "                                    ";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.Frmcustord_Load);
            this.Genpan.ResumeLayout(false);
            this.Genpan.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.HFGP)).EndInit();
            this.Editpan.ResumeLayout(false);
            this.Editpan.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.HFIT)).EndInit();
            this.Taxpan.ResumeLayout(false);
            this.Taxpan.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel Genpan;
        private System.Windows.Forms.CheckBox chkact;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.TextBox txtscr5;
        private System.Windows.Forms.TextBox txtscr4;
        private System.Windows.Forms.TextBox Txtscr3;
        private System.Windows.Forms.TextBox Txtscr2;
        private System.Windows.Forms.Button btnexit;
        private System.Windows.Forms.Button btndelete;
        private System.Windows.Forms.Button btnedit;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox txtscr1;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Panel Editpan;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.TextBox txtamt;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DateTimePicker DTPDOCDT;
        private System.Windows.Forms.DataGridView HFIT;
        private System.Windows.Forms.DateTimePicker Dtpdt;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtdcno;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtitem;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtname;
        private System.Windows.Forms.Label Phone;
        private System.Windows.Forms.TextBox txtdocno;
        private System.Windows.Forms.TextBox txtpuid;
        private System.Windows.Forms.TextBox txtqty;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox txtgrnid;
        private System.Windows.Forms.TextBox txtitid;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtrate;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txtvalue;
        private System.Windows.Forms.Label Remarks;
        private System.Windows.Forms.TextBox txtrem;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Panel Taxpan;
        private System.Windows.Forms.TextBox txtexcise;
        private System.Windows.Forms.Button btnsave;
        private System.Windows.Forms.Button btnaddrcan;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox TxtNetAmt;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.TextBox TxtRoff;
        private System.Windows.Forms.TextBox txttax;
        private System.Windows.Forms.ComboBox cbotax;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox txtcess;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox txthecess;
        private System.Windows.Forms.TextBox txtted;
        private System.Windows.Forms.ComboBox cboexcise;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox txtpf;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox txtdis;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox txtper;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.TextBox txtamttot;
        private System.Windows.Forms.TextBox LblED1;
        private System.Windows.Forms.TextBox LblCess;
        private System.Windows.Forms.TextBox LblHECess;
        private System.Windows.Forms.TextBox lbltax;
        private System.Windows.Forms.DataGridView HFGP;
    }
}