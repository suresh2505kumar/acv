﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Drawing.Imaging;
using System.Configuration;
using System.Data.SqlClient;
using System.Diagnostics;
//using System.IO.FileSystemInfo;
//using System.IO.DirectoryInfo;
//using excel = Microsoft.Office.Interop.Excel;

using Excel = Microsoft.Office.Interop.Excel;
using ExcelAutoFormat = Microsoft.Office.Interop.Excel.XlRangeAutoFormat;

namespace ACV
{
    public partial class Frmreport : Form
    {
        public Frmreport()
        {
            InitializeComponent();
        }

        SqlConnection conn = new SqlConnection("Data Source=" + Genclass.data2 + "; Initial Catalog=" + Genclass.data5 + ";User id=" + Genclass.data3 + ";Password=" + Genclass.data4 + "");
        SqlCommand qur = new SqlCommand();


        //private static Microsoft.Office.Interop.Excel.Workbook mWorkBook;
        //private static Microsoft.Office.Interop.Excel.Sheets mWorkSheets;
        //private static Microsoft.Office.Interop.Excel.Worksheet mWSheet1;
        //private static Microsoft.Office.Interop.Excel.Application oXL;

        private void buttnext_Click(object sender, EventArgs e)
        {
            this.Dispose();
        }

        private void Frmreport_Load(object sender, EventArgs e)
        {
            qur.Connection = conn;

            Left = (MdiParent.ClientRectangle.Width - Width) / 3;
            Top = (MdiParent.ClientRectangle.Height - Height) / 3;
            //cbotype.SelectedIndex = -1;
            cbotype.Text = "Sales Register";
            dtpfrom.Value = DateTime.Now;
            dtpto.Value = DateTime.Now;
        }

        private void buttnshow_Click(object sender, EventArgs e)
        {
            if (cbotype.Text == "Sales Register")
            {
                string quy = "Exec sp_Invwise '" + dtpfrom.Text + "', '" + dtpto.Text + "'," + Genclass.data1 + "";


                Genclass.cmd = new SqlCommand(quy, conn);



                SqlDataAdapter sda = new SqlDataAdapter(Genclass.cmd);
                DataTable dt = new DataTable();
                sda.Fill(dt);


                if (dt.Rows.Count > 0)
                {
                    string path = Application.StartupPath + "\\";

                    if (!Directory.Exists(path))
                    {
                        Directory.CreateDirectory(path);
                    }

                    File.Delete(path + "Sales Register.xls");


                    Excel.Application xlAppToExport = new Excel.Application();
                    xlAppToExport.Workbooks.Add("");

                    Excel.Worksheet xlWorkSheetToExport = default(Excel.Worksheet);
                    xlWorkSheetToExport = (Excel.Worksheet)xlAppToExport.Sheets["Sheet1"];


                    int iRowCnt = 3;


                    xlWorkSheetToExport.Cells[1, 3] = "Venketeshwara Rubber Products";
                    xlWorkSheetToExport.Cells[2, 3] = "Sales Register for the period '" + dtpfrom.Value + "' '" + dtpto.Value + "'";

                    xlWorkSheetToExport.Cells[3, 1] = "InvNo";
                    xlWorkSheetToExport.Cells[3, 2] = "Date";
                    xlWorkSheetToExport.Cells[3, 3] = "Customer Name";
                    xlWorkSheetToExport.Cells[3, 4] = "State";
                    xlWorkSheetToExport.Cells[3, 5] = "GSTIN No";
                    xlWorkSheetToExport.Cells[3, 6] = "Taxable Value";
                    xlWorkSheetToExport.Cells[3, 7] = "IGST";
                    xlWorkSheetToExport.Cells[3, 8] = "CGST";
                    xlWorkSheetToExport.Cells[3, 9] = "SGST";
                    xlWorkSheetToExport.Cells[3, 10] = "Total Value";

                    Excel.Range range = xlWorkSheetToExport.Cells[1, 1] as Excel.Range;
                    range.EntireRow.Font.Name = "Calibri";
                    range.EntireRow.Font.Bold = true;
                    range.EntireRow.Font.Size = 16;

                    xlWorkSheetToExport.Range["C1:C1"].MergeCells = true;



                    int i;

                    for (i = 0; i <= dt.Rows.Count - 1; i++)
                    {
                        xlWorkSheetToExport.Cells[iRowCnt + 1, 1] = dt.Rows[i]["Invno"].ToString();
                        xlWorkSheetToExport.Cells[iRowCnt + 1, 2] = dt.Rows[i]["date"].ToString();
                        xlWorkSheetToExport.Cells[iRowCnt + 1, 3] = dt.Rows[i]["customer"].ToString();
                        xlWorkSheetToExport.Cells[iRowCnt + 1, 4] = dt.Rows[i]["state"].ToString();
                        xlWorkSheetToExport.Cells[iRowCnt + 1, 5] = dt.Rows[i]["gstin"].ToString();
                        xlWorkSheetToExport.Cells[iRowCnt + 1, 6] = dt.Rows[i]["taxablevalue"].ToString();
                        xlWorkSheetToExport.Cells[iRowCnt + 1, 7] = dt.Rows[i]["igst"].ToString();
                        xlWorkSheetToExport.Cells[iRowCnt + 1, 8] = dt.Rows[i]["cgst"].ToString();
                        xlWorkSheetToExport.Cells[iRowCnt + 1, 9] = dt.Rows[i]["sgst"].ToString();
                        xlWorkSheetToExport.Cells[iRowCnt + 1, 10] = dt.Rows[i]["totvalue"].ToString();


                        iRowCnt = iRowCnt + 1;
                    }

                    Excel.Range range1 = xlAppToExport.ActiveCell.Worksheet.Cells[3, 1] as Excel.Range;
                    range1.AutoFormat(ExcelAutoFormat.xlRangeAutoFormatList3);


                    xlWorkSheetToExport.SaveAs(path + "Sales Register.xls");


                    xlAppToExport.Workbooks.Close();
                    xlAppToExport.Quit();
                    xlAppToExport = null;
                    xlWorkSheetToExport = null;

                }
            }
            else if (cbotype.Text == "Hsncode Register")
            {

                string quy = "Exec Sp_Hsnwise '" + dtpfrom.Text + "', '" + dtpto.Text + "'," + Genclass.data1 + "";

                Genclass.cmd = new SqlCommand(quy, conn);



                SqlDataAdapter sda = new SqlDataAdapter(Genclass.cmd);
                DataTable dt = new DataTable();
                sda.Fill(dt);


                if (dt.Rows.Count > 0)
                {
                    string path = Application.StartupPath + "\\";

                    if (!Directory.Exists(path))
                    {
                        Directory.CreateDirectory(path);
                    }

                    File.Delete(path + "Hsncode Register.xls");


                    Excel.Application xlAppToExport = new Excel.Application();
                    xlAppToExport.Workbooks.Add("");

                    Excel.Worksheet xlWorkSheetToExport = default(Excel.Worksheet);
                    xlWorkSheetToExport = (Excel.Worksheet)xlAppToExport.Sheets["Sheet1"];


                    int iRowCnt = 3;


                    xlWorkSheetToExport.Cells[1, 2] = "Venketeshwara Rubber Products";
                    xlWorkSheetToExport.Cells[2, 2] = "HSNCODE Register for the period " + dtpfrom.Text + " To " + dtpto.Text + "";

                    Excel.Range range = xlWorkSheetToExport.Cells[1, 1] as Excel.Range;
                    range.EntireRow.Font.Name = "Calibri";
                    range.EntireRow.Font.Bold = true;
                    range.EntireRow.Font.Size = 16;

                    xlWorkSheetToExport.Range["B1:H1"].MergeCells = true;
                    xlWorkSheetToExport.Range["A2:H2"].MergeCells = true;

                    if (dt.Rows.Count > 0)
                    {
                        int j = 1;
                        foreach (DataColumn column in dt.Columns)
                        {

                            xlWorkSheetToExport.Cells[3, j] = column.ColumnName;
                            j = j + 1;
                        }
                    }







                    int i;

                    for (i = 0; i <= dt.Rows.Count - 1; i++)
                    {
                        xlWorkSheetToExport.Cells[iRowCnt + 1, 1] = dt.Rows[i]["Hsncode"].ToString();
                        xlWorkSheetToExport.Cells[iRowCnt + 1, 2] = dt.Rows[i]["Qty"].ToString();
                        xlWorkSheetToExport.Cells[iRowCnt + 1, 3] = dt.Rows[i]["Uom"].ToString();
                        xlWorkSheetToExport.Cells[iRowCnt + 1, 4] = dt.Rows[i]["Taxablevalue"].ToString();
                        xlWorkSheetToExport.Cells[iRowCnt + 1, 5] = dt.Rows[i]["Rate"].ToString();
                        xlWorkSheetToExport.Cells[iRowCnt + 1, 6] = dt.Rows[i]["CGST"].ToString();
                        xlWorkSheetToExport.Cells[iRowCnt + 1, 7] = dt.Rows[i]["SGST"].ToString();
                        xlWorkSheetToExport.Cells[iRowCnt + 1, 8] = dt.Rows[i]["IGST"].ToString();



                        iRowCnt = iRowCnt + 1;
                    }

                    Excel.Range range1 = xlAppToExport.ActiveCell.Worksheet.Cells[3, 1] as Excel.Range;
                    range1.AutoFormat(ExcelAutoFormat.xlRangeAutoFormatList3);


                    xlWorkSheetToExport.SaveAs(path + "Hsncode Register.xls");


                    xlAppToExport.Workbooks.Close();
                    xlAppToExport.Quit();
                    xlAppToExport = null;
                    xlWorkSheetToExport = null;

                }

            }

        }
    }
}
