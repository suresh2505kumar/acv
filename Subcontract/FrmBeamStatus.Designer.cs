﻿namespace ACV
{
    partial class FrmBeamStatus
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmBeamStatus));
            this.GrFront = new System.Windows.Forms.GroupBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtBeam = new System.Windows.Forms.TextBox();
            this.grSearch = new System.Windows.Forms.Panel();
            this.DataGridCommon = new System.Windows.Forms.DataGridView();
            this.btnSelect = new System.Windows.Forms.Button();
            this.btnHide = new System.Windows.Forms.Button();
            this.BtnExit = new System.Windows.Forms.Button();
            this.sfDataGrid1 = new Syncfusion.WinForms.DataGrid.SfDataGrid();
            this.BtnSts = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.txtYarn = new System.Windows.Forms.TextBox();
            this.sfDataGrid2 = new Syncfusion.WinForms.DataGrid.SfDataGrid();
            this.GrFront.SuspendLayout();
            this.grSearch.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridCommon)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sfDataGrid1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sfDataGrid2)).BeginInit();
            this.SuspendLayout();
            // 
            // GrFront
            // 
            this.GrFront.Controls.Add(this.grSearch);
            this.GrFront.Controls.Add(this.sfDataGrid2);
            this.GrFront.Controls.Add(this.label2);
            this.GrFront.Controls.Add(this.txtBeam);
            this.GrFront.Controls.Add(this.BtnExit);
            this.GrFront.Controls.Add(this.sfDataGrid1);
            this.GrFront.Controls.Add(this.BtnSts);
            this.GrFront.Controls.Add(this.label1);
            this.GrFront.Controls.Add(this.txtYarn);
            this.GrFront.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.GrFront.Location = new System.Drawing.Point(8, 5);
            this.GrFront.Name = "GrFront";
            this.GrFront.Size = new System.Drawing.Size(1015, 511);
            this.GrFront.TabIndex = 0;
            this.GrFront.TabStop = false;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(362, 25);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(43, 18);
            this.label2.TabIndex = 422;
            this.label2.Text = "Beam";
            // 
            // txtBeam
            // 
            this.txtBeam.Location = new System.Drawing.Point(405, 21);
            this.txtBeam.Name = "txtBeam";
            this.txtBeam.Size = new System.Drawing.Size(178, 26);
            this.txtBeam.TabIndex = 421;
            this.txtBeam.Click += new System.EventHandler(this.TxtBeam_Click);
            this.txtBeam.TextChanged += new System.EventHandler(this.txtBeam_TextChanged);
            // 
            // grSearch
            // 
            this.grSearch.BackColor = System.Drawing.Color.White;
            this.grSearch.Controls.Add(this.DataGridCommon);
            this.grSearch.Controls.Add(this.btnSelect);
            this.grSearch.Controls.Add(this.btnHide);
            this.grSearch.Location = new System.Drawing.Point(84, 35);
            this.grSearch.Name = "grSearch";
            this.grSearch.Size = new System.Drawing.Size(376, 300);
            this.grSearch.TabIndex = 420;
            this.grSearch.Visible = false;
            // 
            // DataGridCommon
            // 
            this.DataGridCommon.AllowUserToAddRows = false;
            this.DataGridCommon.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.DataGridCommon.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DataGridCommon.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.DataGridCommon.Location = new System.Drawing.Point(4, 4);
            this.DataGridCommon.Name = "DataGridCommon";
            this.DataGridCommon.ReadOnly = true;
            this.DataGridCommon.RowHeadersVisible = false;
            dataGridViewCellStyle8.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.DataGridCommon.RowsDefaultCellStyle = dataGridViewCellStyle8;
            this.DataGridCommon.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.DataGridCommon.Size = new System.Drawing.Size(369, 264);
            this.DataGridCommon.TabIndex = 0;
            this.DataGridCommon.CellMouseDoubleClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.DataGridCommon_CellMouseDoubleClick);
            // 
            // btnSelect
            // 
            this.btnSelect.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnSelect.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSelect.Image = global::ACV.Properties.Resources.ok;
            this.btnSelect.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.btnSelect.Location = new System.Drawing.Point(297, 268);
            this.btnSelect.Name = "btnSelect";
            this.btnSelect.Size = new System.Drawing.Size(76, 28);
            this.btnSelect.TabIndex = 394;
            this.btnSelect.Text = "Select";
            this.btnSelect.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnSelect.UseVisualStyleBackColor = false;
            this.btnSelect.Click += new System.EventHandler(this.btnSelect_Click);
            // 
            // btnHide
            // 
            this.btnHide.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnHide.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnHide.Image = ((System.Drawing.Image)(resources.GetObject("btnHide.Image")));
            this.btnHide.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnHide.Location = new System.Drawing.Point(3, 269);
            this.btnHide.Name = "btnHide";
            this.btnHide.Size = new System.Drawing.Size(72, 27);
            this.btnHide.TabIndex = 393;
            this.btnHide.Text = "Close";
            this.btnHide.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnHide.UseVisualStyleBackColor = false;
            this.btnHide.Click += new System.EventHandler(this.btnHide_Click);
            // 
            // BtnExit
            // 
            this.BtnExit.Location = new System.Drawing.Point(946, 22);
            this.BtnExit.Name = "BtnExit";
            this.BtnExit.Size = new System.Drawing.Size(63, 25);
            this.BtnExit.TabIndex = 227;
            this.BtnExit.Text = "Exit";
            this.BtnExit.UseVisualStyleBackColor = true;
            this.BtnExit.Click += new System.EventHandler(this.BtnExit_Click);
            // 
            // sfDataGrid1
            // 
            this.sfDataGrid1.AccessibleName = "Table";
            this.sfDataGrid1.AllowFiltering = true;
            this.sfDataGrid1.AllowResizingColumns = true;
            this.sfDataGrid1.Location = new System.Drawing.Point(6, 51);
            this.sfDataGrid1.Name = "sfDataGrid1";
            this.sfDataGrid1.Size = new System.Drawing.Size(1003, 75);
            this.sfDataGrid1.TabIndex = 226;
            this.sfDataGrid1.Text = "sfDataGrid1";
            // 
            // BtnSts
            // 
            this.BtnSts.Location = new System.Drawing.Point(589, 22);
            this.BtnSts.Name = "BtnSts";
            this.BtnSts.Size = new System.Drawing.Size(51, 25);
            this.BtnSts.TabIndex = 2;
            this.BtnSts.Text = "Ok";
            this.BtnSts.UseVisualStyleBackColor = true;
            this.BtnSts.Click += new System.EventHandler(this.BtnSts_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(9, 25);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(74, 18);
            this.label1.TabIndex = 1;
            this.label1.Text = "Yarn Count";
            // 
            // txtYarn
            // 
            this.txtYarn.Location = new System.Drawing.Point(84, 21);
            this.txtYarn.Name = "txtYarn";
            this.txtYarn.Size = new System.Drawing.Size(272, 26);
            this.txtYarn.TabIndex = 0;
            this.txtYarn.Click += new System.EventHandler(this.TxtYarn_Click);
            this.txtYarn.TextChanged += new System.EventHandler(this.TxtYarn_TextChanged);
            // 
            // sfDataGrid2
            // 
            this.sfDataGrid2.AccessibleName = "Table";
            this.sfDataGrid2.AllowFiltering = true;
            this.sfDataGrid2.AllowResizingColumns = true;
            this.sfDataGrid2.Location = new System.Drawing.Point(6, 128);
            this.sfDataGrid2.Name = "sfDataGrid2";
            this.sfDataGrid2.Size = new System.Drawing.Size(1003, 376);
            this.sfDataGrid2.TabIndex = 423;
            this.sfDataGrid2.Text = "sfDataGrid2";
            // 
            // FrmBeamStatus
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.ClientSize = new System.Drawing.Size(1030, 520);
            this.Controls.Add(this.GrFront);
            this.Name = "FrmBeamStatus";
            this.Text = "FrmBeamStatus";
            this.Load += new System.EventHandler(this.FrmBeamStatus_Load);
            this.GrFront.ResumeLayout(false);
            this.GrFront.PerformLayout();
            this.grSearch.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.DataGridCommon)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sfDataGrid1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sfDataGrid2)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox GrFront;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtYarn;
        private System.Windows.Forms.Button BtnSts;
        private Syncfusion.WinForms.DataGrid.SfDataGrid sfDataGrid1;
        private System.Windows.Forms.Button BtnExit;
        private System.Windows.Forms.Panel grSearch;
        private System.Windows.Forms.DataGridView DataGridCommon;
        private System.Windows.Forms.Button btnSelect;
        private System.Windows.Forms.Button btnHide;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtBeam;
        private Syncfusion.WinForms.DataGrid.SfDataGrid sfDataGrid2;
    }
}