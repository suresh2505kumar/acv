﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Drawing.Imaging;
using System.Configuration;
using System.Data.SqlClient;

namespace ACV
{
    public partial class Frmitem : Form
    {
        public Frmitem()
        {
            CenterToParent();
            this.BackColor = Color.White;
            InitializeComponent();
        }

        string uid = "";
        int mode = 0;

        //SqlCommand cmd;
        SqlConnection conn = new SqlConnection("Data Source=" + Genclass.data2 + "; Initial Catalog=" + Genclass.data5 + ";User id=" + Genclass.data3 + ";Password=" + Genclass.data4 + "");

        //SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["connstr"].ConnectionString);
        SqlCommand qur = new SqlCommand();

        private void Txtitgrp_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                Genclass.type = 3;
                loaduom();
                conn.Close();
            }
        }

        private void loaduom()
        {
            Genclass.okclick = false;
            conn.Close();
            conn.Open();
            if (Genclass.type == 9)
            {
                Genclass.Module.Partylistviewcont("uid", "HSN", Genclass.strsql, this, txthsnid, txthsn, Editpnl);
                Genclass.strsql = "select uid,Hsncode as HSN from HSN where active=1 and  (cgid <>0 or sgid <>0 or igid <>0) and companyid=" + Genclass.data1 + "";
                Genclass.FSSQLSortStr = "Hsncode";
            }
            else if (Genclass.type == 8)
            {
                Genclass.Module.Partylistviewcont3("uid", "ItemGroup", "hsncode", Genclass.strsql, this, txtgrpid, txtgrp, txthsncode, Editpnl);
                Genclass.strsql = "select uid,ItemGroup,isnull(hsncode,0) as hsncode from ItemGroup where active=1 and companyid=" + Genclass.data1 + "";
                Genclass.FSSQLSortStr = "ItemGroup";
            }
            else if (Genclass.type == 3)
            {
                Genclass.Module.Partylistviewcont("uid", "Brand", Genclass.strsql, this, txtitgrpid, Txtitgrp, Editpnl);
                Genclass.strsql = "select uid,ItemsubGroup as Brand from ItemsubGroup where active=1 and companyid=" + Genclass.data1 + "";
                Genclass.FSSQLSortStr = "ItemsubGroup";
            }
            else if (Genclass.type == 2)
            {
                Genclass.Module.Partylistviewcont("uid", "itemname", Genclass.strsql, this, txtalterid, txtbomitem, bompro);
                Genclass.strsql = "select uid,itemname   from itemm where uid<> " + uid + " and active=1 and companyid=" + Genclass.data1 + "";

                Genclass.FSSQLSortStr = "Itemname";
            }
            else if (Genclass.type == 1)
            {
                Genclass.Module.Partylistviewcont("uid", "UoM", Genclass.strsql, this, txtuomid, txtuom1, Editpnl);
                Genclass.strsql = "select uid,Generalname as UoM from Generalm where active=1 and typem_uid=1 and companyid=" + Genclass.data1 + "";
                Genclass.FSSQLSortStr = "Uom";
            }
            else if (Genclass.type == 4)
            {
                Genclass.Module.Partylistviewcont("uid", "name", Genclass.strsql, this, txtpartyid, txtparty, Editpnl);
                Genclass.strsql = "select uid,name as Party from partym where active=1 and companyid=" + Genclass.data1 + "";
                Genclass.FSSQLSortStr = "name";
            }
            if (Genclass.type == 5)
            {

                Genclass.Module.Partylistviewcont("uid", "Categorytype", Genclass.strsql, this, txtcatid, txttax1, Editpnl);
                Genclass.strsql = "select uid,Categorytype from Categorytypem where active=1 and companyid=" + Genclass.data1 + "";
                Genclass.FSSQLSortStr = "Categorytype";
            }
            //else if (type == 6)
            //{
            //    Genclass.Module.Partylistviewcont("itemcode", "uom", Genclass.strsql, this, txtbomcode, txbomuom, bompro);
            //    Genclass.strsql = "select itemcode,generalname as uom from itemm a inner join generalm b on a.uom_uid=b.uid where a.active=1 and a.uid=" + txtalterid.Text + "";

            //}
            else if (Genclass.type == 7)
            {
                Genclass.Module.Partylistviewcont("uid", "processname", Genclass.strsql, this, txtprocessid, txtproceesname, bompro);
                Genclass.strsql = "select uid,processname from processm where active=1 and companyid=" + Genclass.data1 + " and uid not in (select ProcessM_UId from ItemsMProcess where ItemM_UId=" + txtalterid.Text + " and companyid=" + Genclass.data1 + ")";
                Genclass.FSSQLSortStr = "processname";
            }
            else if (Genclass.type == 11)
            {
                Genclass.Module.Partylistviewcont("uid", "Hsncode", Genclass.strsql, this, txthsnid, txthsncode, Editpnl);
                Genclass.strsql = "select distinct uid,Hsncode from hsn where active=1 and companyid=" + Genclass.data1 + " ";
                Genclass.FSSQLSortStr = "Hsncode";
            }
            Genclass.cmd = new SqlCommand(Genclass.strsql, conn);
            SqlDataAdapter aptr = new SqlDataAdapter(Genclass.cmd);
            DataTable tap = new DataTable();
            aptr.Fill(tap);
            Frmlookup contc = new Frmlookup();
            DataGridView dt = (DataGridView)contc.Controls["HFGP"];
            dt.Refresh();
            dt.ColumnCount = tap.Columns.Count;
            dt.Columns[0].Visible = false;
            dt.Columns[1].Width = 530;


            dt.DefaultCellStyle.Font = new Font("Arial", 10);

            dt.ColumnHeadersDefaultCellStyle.Font = new Font("Arial", 10, FontStyle.Bold);
            dt.AutoGenerateColumns = false;

            Genclass.i = 0;
            foreach (DataColumn column in tap.Columns)
            {
                dt.Columns[Genclass.i].Name = column.ColumnName;
                dt.Columns[Genclass.i].HeaderText = column.ColumnName;
                dt.Columns[Genclass.i].DataPropertyName = column.ColumnName;
                Genclass.i = Genclass.i + 1;
            }

            dt.DataSource = tap;
            conn.Close();
            contc.Show();


        }
        public void Category()
        {
            conn.Open();
            string qur = "select * from Categorym";
            SqlCommand cmd = new SqlCommand(qur, conn);
            SqlDataAdapter apt = new SqlDataAdapter(cmd);
            DataTable tab = new DataTable();
            apt.Fill(tab);
            txtcat.DataSource = null;
            txtcat.DataSource = tab;
            txtcat.DisplayMember = "Category";
            txtcat.ValueMember = "uid";
            txtcat.SelectedIndex = -1;
            conn.Close();
        }
        public void tax()
        {
            conn.Open();
            string qur = "select Generalname as tax,uid from generalm where   Typem_uid in (6) and companyid=" + Genclass.data1 + " and active=1 ";
            SqlCommand cmd = new SqlCommand(qur, conn);
            SqlDataAdapter apt = new SqlDataAdapter(cmd);
            DataTable tab = new DataTable();
            apt.Fill(tab);
            txttax.DataSource = null;
            txttax.DataSource = tab;
            txttax.DisplayMember = "tax";
            txttax.ValueMember = "uid";
            txttax.SelectedIndex = -1;
            conn.Close();
        }
        public void uom()
        {
            conn.Open();
            string qur = "select Generalname as Uom,uid from generalm where typem_uid =1";
            SqlCommand cmd = new SqlCommand(qur, conn);
            SqlDataAdapter apt = new SqlDataAdapter(cmd);
            DataTable tab = new DataTable();
            apt.Fill(tab);
            txtuom.DataSource = null;
            txtuom.DataSource = tab;
            txtuom.DisplayMember = "Uom";
            txtuom.ValueMember = "uid";
            txtuom.SelectedIndex = -1;
            conn.Close();
        }
        private void Frmitem_Load(object sender, EventArgs e)
        {

            qur.Connection = conn;


            comboBox1.Text = "Mapped Items";
            Left = (MdiParent.ClientRectangle.Width - Width) / 3;
            Top = (MdiParent.ClientRectangle.Height - Height) / 3;
            Genclass.Module.buttonstyleform(this);
            Genclass.Module.buttonstylepanel(panadd);
            Genclass.Module.buttonstylepanel(bompro);
            panadd.Visible = true;
            chkact.Checked = true;
            buttnbomext.Visible = false;
            HFGP.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            hfg3.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            HFG4.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            HFGP.EnableHeadersVisualStyles = false;
            hfg3.EnableHeadersVisualStyles = false;
            HFG4.EnableHeadersVisualStyles = false;
            HFG4.ColumnHeadersDefaultCellStyle.BackColor = SystemColors.GradientActiveCaption;
            HFGP.ColumnHeadersDefaultCellStyle.BackColor = SystemColors.GradientActiveCaption;
            hfg3.ColumnHeadersDefaultCellStyle.BackColor = SystemColors.GradientActiveCaption;

            Left = (MdiParent.ClientRectangle.Width - Width) / 3;
            Top = (MdiParent.ClientRectangle.Height - Height) / 3;
            //this.CenterToParent();

            this.HFGP.DefaultCellStyle.Font = new Font("Calibri", 10);
            this.HFGP.ColumnHeadersDefaultCellStyle.Font = new Font("Calibri", 10, FontStyle.Regular);
            this.hfg3.DefaultCellStyle.Font = new Font("Calibri", 10);
            this.hfg3.ColumnHeadersDefaultCellStyle.Font = new Font("Calibri", 10, FontStyle.Regular);
            this.HFG4.DefaultCellStyle.Font = new Font("Calibri", 10);
            this.HFG4.ColumnHeadersDefaultCellStyle.Font = new Font("Calibri", 10, FontStyle.Regular);

            HFGP.RowHeadersVisible = false;
            hfg3.RowHeadersVisible = false;
            HFG4.RowHeadersVisible = false;

            bompro.Visible = false;
            Editpnl.Visible = false;
            Genpan.Visible = true;
            //Loadgrid();
            panadd.Visible = true;
            chkact.Checked = true;

            Category();
            uom();
            tax();
        }

        private void Loadgrid2()
        {

            conn.Open();

            {
                string quy = "select c.uid,c.processname as Process,a.sequenceno as SeqNo,processrateqty as qty,processsetuptime as SetTime ,processtime as Proctime,processqty as ProcQty,processratehour as RateHr,b.uid,a.uid from itemsmprocess a inner join  itemm b on a.itemm_uid=b.uid and a.companyid=" + Genclass.data1 + " inner join processm c on a.processm_uid=c.uid where a.itemm_uid=" + Genclass.bomstr + "";
                Genclass.cmd = new SqlCommand(quy, conn);
            }


            SqlDataAdapter aptr = new SqlDataAdapter(Genclass.cmd);
            DataTable tap = new DataTable();
            aptr.Fill(tap);


            HFG4.AutoGenerateColumns = false;
            HFG4.Refresh();
            HFG4.DataSource = null;
            HFG4.Rows.Clear();


            HFG4.ColumnCount = tap.Columns.Count;
            Genclass.i = 0;
            foreach (DataColumn column in tap.Columns)
            {
                HFG4.Columns[Genclass.i].Name = column.ColumnName;
                HFG4.Columns[Genclass.i].HeaderText = column.ColumnName;
                HFG4.Columns[Genclass.i].DataPropertyName = column.ColumnName;
                Genclass.i = Genclass.i + 1;
            }


            HFG4.Columns[0].Visible = false;
            HFG4.Columns[0].Width = 0;
            HFG4.Columns[1].Width = 210;
            HFG4.Columns[2].Width = 167;
            HFG4.Columns[3].Width = 61;
            HFG4.Columns[4].Width = 86;
            HFG4.Columns[5].Width = 86;
            HFG4.Columns[6].Width = 86;
            HFG4.Columns[7].Width = 84;

            HFG4.Columns[8].Visible = false;
            HFG4.Columns[9].Visible = false;




            HFG4.DataSource = tap;
            //TitlepPROCESS();

        }


        private void btnaddrcan_Click(object sender, EventArgs e)
        {

            this.Dispose();
        }
        private void Titlep()
        {
            hfg3.ColumnCount = 7;
            hfg3.Columns[0].Name = "uid";
            hfg3.Columns[1].Name = "Itemname";
            hfg3.Columns[2].Name = "itemcode";
            hfg3.Columns[3].Name = "uom";
            hfg3.Columns[4].Name = "usedqty";
            hfg3.Columns[5].Name = "Usedper";
            hfg3.Columns[6].Name = "oilit";
            hfg3.Columns[0].Width = 0;
            hfg3.Columns[1].Width = 350;
            hfg3.Columns[2].Width = 168;
            hfg3.Columns[3].Width = 72;
            hfg3.Columns[4].Width = 92;
            hfg3.Columns[5].Width = 84;
            hfg3.Columns[6].Visible = false;
        }

        private void TitlepPROCESS()
        {
            HFG4.ColumnCount = 9;
            HFG4.Columns[0].Name = "Processid";
            HFG4.Columns[1].Name = "Process";
            HFG4.Columns[2].Name = "SeqNo";
            HFG4.Columns[3].Name = "Rate/Qty";
            HFG4.Columns[4].Name = "Set.Time";
            HFG4.Columns[5].Name = "Proc.Time";
            HFG4.Columns[6].Name = "Proc. Qty";
            HFG4.Columns[7].Name = "Rate/Hr";
            HFG4.Columns[8].Name = "itemid";
            HFG4.Columns[0].Width = 0;
            HFG4.Columns[1].Width = 210;
            HFG4.Columns[2].Width = 167;
            HFG4.Columns[3].Width = 61;
            HFG4.Columns[4].Width = 86;
            HFG4.Columns[5].Width = 86;
            HFG4.Columns[6].Width = 86;
            HFG4.Columns[7].Width = 84;

            HFG4.Columns[8].Visible = false;
        }
        private void Loadgrid()
        {
            try
            {
                conn.Close();
                conn.Open();

                Genclass.StrSrch = "";


                Genclass.FSSQLSortStr = "ItemCode";
                Genclass.FSSQLSortStr1 = "ItemName";
                Genclass.FSSQLSortStr2 = "ItemGroup";
                Genclass.FSSQLSortStr3 = "e.Hsncode";
                Genclass.FSSQLSortStr4 = "m.generalname";
                Genclass.FSSQLSortStr5 = "itemsubgroup";


                if (txtscr1.Text != "")
                {
                    if (Genclass.StrSrch == "")
                    {
                        Genclass.StrSrch = Genclass.FSSQLSortStr + " like '%" + txtscr1.Text + "%'";
                    }
                    else
                    {
                        Genclass.StrSrch = Genclass.StrSrch + " and " + Genclass.FSSQLSortStr + " like '%" + txtscr1.Text + "%'";
                    }

                }

                if (Txtscr2.Text != "")
                {
                    if (Genclass.StrSrch == "")
                    {
                        Genclass.StrSrch = Genclass.FSSQLSortStr1 + " like '%" + Txtscr2.Text + "%'";
                    }
                    else
                    {
                        Genclass.StrSrch = Genclass.StrSrch + " and " + Genclass.FSSQLSortStr1 + " like '%" + Txtscr2.Text + "%'";
                    }

                }

                if (Txtscr3.Text != "")
                {
                    if (Genclass.StrSrch == "")
                    {
                        Genclass.StrSrch = Genclass.FSSQLSortStr2 + " like '%" + Txtscr3.Text + "%'";
                    }
                    else
                    {
                        Genclass.StrSrch = Genclass.StrSrch + " and " + Genclass.FSSQLSortStr2 + " like '%" + Txtscr3.Text + "%'";
                    }

                }

                if (Txtscr4.Text != "")
                {
                    if (Genclass.StrSrch == "")
                    {
                        Genclass.StrSrch = Genclass.FSSQLSortStr3 + " like '%" + Txtscr4.Text + "%'";
                    }
                    else
                    {
                        Genclass.StrSrch = Genclass.StrSrch + " and " + Genclass.FSSQLSortStr3 + " like '%" + Txtscr4.Text + "%'";
                    }

                }

                if (Txtscr5.Text != "")
                {
                    if (Genclass.StrSrch == "")
                    {
                        Genclass.StrSrch = Genclass.FSSQLSortStr4 + " like '%" + Txtscr5.Text + "%'";
                    }
                    else
                    {
                        Genclass.StrSrch = Genclass.StrSrch + " and " + Genclass.FSSQLSortStr4 + " like '%" + Txtscr5.Text + "%'";
                    }
                }

                if (txtscr1.Text != "")
                {
                    Genclass.StrSrch = " " + Genclass.StrSrch;
                }
                else if (Txtscr2.Text != "")
                {
                    Genclass.StrSrch = " " + Genclass.StrSrch;
                }
                else if (Txtscr3.Text != "")
                {
                    Genclass.StrSrch = " " + Genclass.StrSrch;
                }
                else if (Txtscr4.Text != "")
                {
                    Genclass.StrSrch = " " + Genclass.StrSrch;
                }
                else if (Txtscr5.Text != "")
                {
                    Genclass.StrSrch = " " + Genclass.StrSrch;
                }
                else
                {
                    Genclass.StrSrch = "a.uid <> 0";
                }
                if (chkact.Checked == true && comboBox1.Text == "Mapped Items")
                {
                    string quy = "select top 25 Uid,ItemCode,ItemName,ItemGroup,Hsncode,sname as Tax,uom,ItemSubGroup,catid,subgrpid,uomid,ItemSpec1,partyuid,category, ItemGroup,grpid,HSN,hsnid,active,sname,hsncode,tax  from (SELECT   a.Uid, a.partyuid,a.ItemCode, a.ItemName,a.Cusitemcode,a.Cusitemname,b.Category,d.generalname  as uom,c.ItemSubGroup,e.ItemGroup,ISNULL(z.hsncode,'') as HSN, f.uid as hsnid,e.Hsncode as Hsncode1 ,a.active,b.uid as catid,c.uid as subgrpid,d.Uid as uomid,ItemSpec1,isnull(m.generalname,0)  as sname,  e.UId as grpid,a.Companyid,isnull(a.hsnid,'') as hsncode,isnull(tax,0) as tax from  dbo.ItemM  a left JOIN  dbo.Categorym  b ON a.CategoryTypeM_UId = b.UId left join ItemSubGroup c on   a.ItemSubGroup_UId=c.UId left join   generalm d on a.UOM_UId=d.uid left join ItemGroup e on a.ItemGroup_UId=e.UId left join Hsndet f on a.hsnid=f.uid     left join Hsn z on f.Hsnid=z.uid   left join generalm m on a.tax=m.uid  where " + Genclass.StrSrch + " and a.tax<>0 ) tab    order by ItemCode desc ";
                    Genclass.cmd = new SqlCommand(quy, conn);
                }
                else if (chkact.Checked == false && comboBox1.Text == "Mapped Items")
                {
                    string quy = "select top 25 Uid,ItemCode,ItemName,ItemGroup,Hsncode,sname as Tax,uom,ItemSubGroup,catid,subgrpid,uomid,ItemSpec1,partyuid,categorytype,ItemGroup,grpid,HSN,hsnid,active  from (SELECT   a.Uid, a.partyuid,a.ItemCode,a.ItemName,a.Cusitemcode,a.Cusitemname,b.CategoryType,d.generalname as uom,c.ItemSubGroup,e.ItemGroup,ISNULL(z.hsncode,'') as HSN,f.uid as hsnid,e.Hsncode,a.active,b.uid as catid,c.uid as subgrpid,d.Uid as uomid,ItemSpec1,e.UId as grpid,a.Companyid from  dbo.ItemM  a left JOIN dbo.CategoryTypeM AS b ON a.CategoryTypeM_UId = b.UId left join ItemSubGroup c on a.ItemSubGroup_UId=c.UId left join generalm d on a.UOM_UId=d.uid left join ItemGroup e on a.ItemGroup_UId=e.UId left join Hsndet f on a.hsnid=f.uid left join Hsn z on f.Hsnid=z.uid where and " + Genclass.StrSrch + " and  a.tax<>0 )   order by ItemCode desc";
                    Genclass.cmd = new SqlCommand(quy, conn);

                }
                else if (chkact.Checked == false && comboBox1.Text == "unmapped Items")
                {
                    string quy = "select top 25 Uid,ItemCode,ItemName,ItemGroup,Hsncode,sname as Tax,uom,ItemSubGroup,catid,subgrpid,uomid,ItemSpec1,partyuid,category, ItemGroup,grpid,HSN,hsnid,active,sname,hsncode,tax  from (SELECT   a.Uid, a.partyuid,a.ItemCode,a.ItemName,a.Cusitemcode,a.Cusitemname,b.Category,d.generalname  as uom,c.ItemSubGroup,e.ItemGroup,ISNULL(z.hsncode,'') as HSN,f.uid as hsnid,e.Hsncode as Hsncode1 ,a.active,b.uid as catid,c.uid as subgrpid,d.Uid as uomid,ItemSpec1,isnull(m.generalname,0) as sname,  e.UId as grpid,a.Companyid,isnull(a.hsnid,'') as hsncode,isnull(tax,0) as tax from  dbo.ItemM  a left JOIN dbo.Categorym  b ON a.CategoryTypeM_UId = b.UId left join ItemSubGroup c on   a.ItemSubGroup_UId=c.UId left join generalm d on a.UOM_UId=d.uid left join ItemGroup e on a.ItemGroup_UId=e.UId left join Hsndet f on a.hsnid=f.uid   left join Hsn z on f.Hsnid=z.uid   left join generalm m on a.tax=m.uid  where " + Genclass.StrSrch + " and a.tax=0 ) tab order by ItemCode desc ";
                    Genclass.cmd = new SqlCommand(quy, conn);

                }
                else if (chkact.Checked == false && comboBox1.Text == "unmapped Items")
                {
                    string quy = "select top 25 Uid,ItemCode,ItemName,ItemGroup,Hsncode,sname as Tax,uom,ItemSubGroup,catid,subgrpid,uomid,ItemSpec1,partyuid,categorytype,ItemGroup,grpid,HSN,hsnid,active  from (SELECT   a.Uid, a.partyuid,a.ItemCode,a.ItemName,a.Cusitemcode,a.Cusitemname,b.CategoryType,d.generalname as uom,c.ItemSubGroup,e.ItemGroup,ISNULL(z.hsncode,'') as HSN,f.uid as hsnid,e.Hsncode,a.active,b.uid as catid,c.uid as subgrpid,d.Uid as uomid,ItemSpec1,e.UId as grpid,a.Companyid from  dbo.ItemM  a left JOIN dbo.CategoryTypeM AS b ON a.CategoryTypeM_UId = b.UId left join ItemSubGroup c on a.ItemSubGroup_UId=c.UId left join generalm d on a.UOM_UId=d.uid left join ItemGroup e on a.ItemGroup_UId=e.UId left join Hsndet f on a.hsnid=f.uid left join Hsn z on f.Hsnid=z.uid where  a.tax=0  and " + Genclass.StrSrch + " ) tab order by ItemCode desc";
                    Genclass.cmd = new SqlCommand(quy, conn);

                }

               
                SqlDataAdapter aptr = new SqlDataAdapter(Genclass.cmd);
                DataTable tap = new DataTable();
                aptr.Fill(tap);


                HFGP.AutoGenerateColumns = false;
                HFGP.Refresh();
                HFGP.DataSource = null;
                HFGP.Rows.Clear();


                HFGP.ColumnCount = tap.Columns.Count;
                Genclass.i = 0;
                foreach (DataColumn column in tap.Columns)
                {
                    HFGP.Columns[Genclass.i].Name = column.ColumnName;
                    HFGP.Columns[Genclass.i].HeaderText = column.ColumnName;
                    HFGP.Columns[Genclass.i].DataPropertyName = column.ColumnName;
                    Genclass.i = Genclass.i + 1;
                }



                HFGP.Columns[0].Visible = false;

                HFGP.Columns[1].Width = 198;
                HFGP.Columns[2].Width = 300;
                HFGP.Columns[3].Width = 200;
                HFGP.Columns[4].Width = 170;
                HFGP.Columns[5].Width = 110;
                HFGP.Columns[6].Visible = false;
                HFGP.Columns[7].Visible = false;
                HFGP.Columns[8].Visible = false;
                HFGP.Columns[9].Visible = false;
                HFGP.Columns[10].Visible = false;
                HFGP.Columns[11].Visible = false;
                HFGP.Columns[12].Visible = false;
                HFGP.Columns[13].Visible = false;
                HFGP.Columns[14].Visible = false;
                HFGP.Columns[15].Visible = false;
                HFGP.Columns[16].Visible = false;
                HFGP.Columns[17].Visible = false;
                HFGP.Columns[18].Visible = false;
                HFGP.Columns[19].Visible = false;
                HFGP.Columns[20].Visible = false;
                HFGP.Columns[21].Visible = false;


                HFGP.DataSource = tap;

                int index = HFGP.Rows.Count - 1;

                if (HFGP.Rows[0].Cells[1].Value.ToString() == "" || HFGP.Rows[0].Cells[1].Value == null)
                {
                    lblno1.Text = "0";
                }
                else
                {
                    lblno1.Text = "1";
                }


                lblno2.Text = "of " + index.ToString();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return;
            }
            finally
            {
                conn.Close();
                Genclass.cmd.Dispose();
            }
        }



        private void btnexit_Click_1(object sender, EventArgs e)
        {
            this.Dispose();
        }

        //private void btnaddrcan_Click(object sender, EventArgs e)
        //{
        //    Genclass.Module.ClearTextBox(this);
        //    Genpan.Visible = true;
        //    Editpnl.Visible = false;
        //    Loadgrid();
        //}

        private void btnadd_Click(object sender, EventArgs e)
        {
            mode = 1;
            uid = "0";
            Genpan.Visible = false;
            //bompro.Visible = false;


            Editpnl.Visible = true;
            Chkedtact.Checked = true;
            Genclass.Module.ClearTextBox(this, Editpnl);
            Txtitcode.Focus();


        }

        private void btnedit_Click(object sender, EventArgs e)
        {


            mode = 2;
            Editpnl.Visible = true;
            Genpan.Visible = false;
            int i = HFGP.SelectedCells[0].RowIndex;
            Txtitcode.Text = HFGP.Rows[i].Cells[1].Value.ToString();
            txtitname.Text = HFGP.Rows[i].Cells[2].Value.ToString();
            txtcusitcode.Text = HFGP.Rows[i].Cells[3].Value.ToString();
            txtcusitname.Text = HFGP.Rows[i].Cells[4].Value.ToString();
            txtparty.Text = HFGP.Rows[i].Cells[5].Value.ToString();
            txtuom1.Text = HFGP.Rows[i].Cells[6].Value.ToString();
            Txtitgrp.Text = HFGP.Rows[i].Cells[7].Value.ToString();
            txtcatid.Text = HFGP.Rows[i].Cells[8].Value.ToString();
            txtitgrpid.Text = HFGP.Rows[i].Cells[9].Value.ToString();
            txtuomid.Text = HFGP.Rows[i].Cells[10].Value.ToString();
            txtspec.Text = HFGP.Rows[i].Cells[11].Value.ToString();
            txtpartyid.Text = HFGP.Rows[i].Cells[12].Value.ToString();
            txttax1.Text = HFGP.Rows[i].Cells[13].Value.ToString();
            txtgrp.Text = HFGP.Rows[i].Cells[14].Value.ToString();
            txtgrpid.Text = HFGP.Rows[i].Cells[15].Value.ToString();
            txthsn.Text = HFGP.Rows[i].Cells[16].Value.ToString();
            txthsnid.Text = HFGP.Rows[i].Cells[17].Value.ToString();
            if (HFGP.Rows[i].Cells[18].Value.ToString() == "True")
            {
                Chkedtact.Checked = true;
            }
            else
            {
                Chkedtact.Checked = false;
            }
            uid = HFGP.Rows[i].Cells[0].Value.ToString();
            panadd.Visible = false;
            buttnbomext.Visible = false;
        }

        private void txtuom_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                Genclass.type = 1;
                loaduom();
            }

        }

        private void txtcat_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                Genclass.type = 5;
                loaduom();
            }
        }


        private void txtscr1_TextChanged(object sender, EventArgs e)
        {
            Loadgrid();
        }

        private void Txtscr2_TextChanged(object sender, EventArgs e)
        {
            Loadgrid();
        }

        private void Txtscr3_TextChanged(object sender, EventArgs e)
        {
            Loadgrid();
        }

        private void Txtscr4_TextChanged(object sender, EventArgs e)
        {
            Loadgrid();
        }

        private void Txtscr5_TextChanged(object sender, EventArgs e)
        {
            Loadgrid();
        }

        private void txtuom_TextChanged(object sender, EventArgs e)
        {

        }

        //private void btndelete_Click(object sender, EventArgs e)
        //{
        //    mode = 3;
        //    conn.Open();

        //    {
        //        qur.CommandText = "Exec Sp_bom " + Genclass.bomstr + "," + txtalterid.Text + "," + txtusedqty.Text + "," + txtperqty.Text + "," + mode + "";

        //    }
        //    qur.ExecuteNonQuery();
        //    MessageBox.Show("Record has been Deactivated", "Delete", MessageBoxButtons.OK);

        //    conn.Close();
        //    Loadgrid3();
        //}

        private void txtparty_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtparty_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                Genclass.type = 4;
                loaduom();
            }
        }

        private void txtcat_TextChanged(object sender, EventArgs e)
        {

        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void cmdcan_Click(object sender, EventArgs e)
        {

        }

        private void txtbomitem_KeyDown(object sender, KeyEventArgs e)
        {

        }

        private void txtbomcode_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtbomcode_KeyDown(object sender, KeyEventArgs e)
        {

        }

        //private void Cmditembom_Click(object sender, EventArgs e)
        //{
        //    Genclass.bomstr = HFGP.CurrentRow.Cells[0].Value.ToString();
        //    FrmBom bom = new FrmBom();
        //    bom MdiParent = this;
        //    bom.Show();

        //}

        //private void CmdProcess_Click(object sender, EventArgs e)
        //{
        //    Genclass.bomstr = HFGP.CurrentRow.Cells[0].Value.ToString();

        //    Frmprocess process = new Frmprocess();
        //    process.Show();
        //}

        //private void bompro_Paint(object sender, PaintEventArgs e)
        //{

        //}

        private void btnaddrcan_Click_1(object sender, EventArgs e)
        {
            Editpnl.Visible = false;
            Genpan.Visible = true;
            Genclass.Module.ClearTextBox(this, Editpnl);
            Loadgrid();
        }

        private void btnsave_Click_1(object sender, EventArgs e)
        {
            if (Txtitcode.Text == "")
            {
                MessageBox.Show("Enter the ItemCode");
                Txtitcode.Focus();
                return;
            }
            if (txtitname.Text == "")
            {
                MessageBox.Show("Enter the ItemName");
                txtitname.Focus();
                return;
            }
            //if (txtuom1.Text == "")
            //{
            //    MessageBox.Show("Enter the UoM");
            //    txtuom1.Focus();
            //    return;
            //}
            //if (txtcat.Text == "")
            //{
            //    MessageBox.Show("Enter the Category");
            //    txtcat.Focus();
            //    return;
            //}
            //if (Txtitgrp.Text == "")
            //{
            //    MessageBox.Show("Enter the ItemSubGroup");
            //    Txtitgrp.Focus();
            //    return;
            //}
            //if (txtparty.Text == "")
            //{
            //    MessageBox.Show("Enter the Partyname");
            //    Txtitgrp.Focus();
            //    return;
            //}
            if (txtparty.Text == "")
            {
                MessageBox.Show("Enter the ItemGroup");
                Txtitgrp.Focus();
                return;
            }
            if (txthsn.Text == "")
            {
                MessageBox.Show("Enter the Hsn");
                Txtitgrp.Focus();
                return;
            }


            conn.Open();
            //if (mode == 1)
            //{

            qur.CommandText = "";
            qur.CommandText = "Exec sp_item '" + Txtitcode.Text + "','" + txtitname.Text + "','" + txtcusitcode.Text + "','" + txtcusitname.Text + "','" + txtspec.Text + "'," + txtuomid.Text + "," + txtcatid.Text + "," + txtitgrpid.Text + ",'" + Chkedtact.Checked + "'," + txtpartyid.Text + "," + txtgrpid.Text + "," + uid + ",0," + mode + "," + Genclass.data1 + "";
            qur.ExecuteNonQuery();


            //string quy = "select * from Msgdes";
            //Genclass.cmd = new SqlCommand(quy, conn);

            //SqlDataAdapter aptr = new SqlDataAdapter(Genclass.cmd);
            //DataTable tap = new DataTable();
            //aptr.Fill(tap);

            //MessageBox.Show(tap.Rows[0]["Msg"].ToString(), "Save", MessageBoxButtons.OK);


            //}


            //if (mode == 2)
            //{

            //    qur.CommandText = "Update ItemM set ItemCode='" + Txtitcode.Text + "',ItemName='" + txtitname.Text + "',CusItemCode='" + txtcusitcode.Text + "',CusItemName='" + txtcusitname.Text + "',ItemSpec1='" + txtspec.Text + "',UOM_UId=" + txtuomid.Text + ",CategoryTypeM_UId=" + txtcatid.Text + ",ItemSubGroup_UId=" + txtitgrpid.Text + ",partyuid=" + txtpartyid.Text + " ,ItemGroup_UId=" + txtgrpid.Text + " where uid=" + uid + "";

            //    qur.ExecuteNonQuery();

            //    MessageBox.Show("Record has been Updated", "Save", MessageBoxButtons.OK);
            //}



            conn.Close();
            Loadgrid();
            Genpan.Visible = true;
            Editpnl.Visible = false;
        }



        private void txtparty_KeyDown_1(object sender, KeyEventArgs e)
        {
            //if (e.KeyCode == Keys.Enter)
            //{
            Genclass.type = 4;
            loaduom();
            //}
        }



        private void txtuom_KeyDown_1(object sender, KeyEventArgs e)
        {
            //if (e.KeyCode == Keys.Enter)
            //{
            Genclass.type = 1;
            loaduom();
            //}
        }

        private void txtcat_TextChanged_1(object sender, EventArgs e)
        {
            txtgrp.Focus();
        }

        private void txtcat_KeyDown_1(object sender, KeyEventArgs e)
        {
            Genclass.fieldone = "";
            Genclass.fieldtwo = "";
            Genclass.fieldthree = "";
            Genclass.fieldFour = "";
            Genclass.fieldFive = "";
            Genclass.type = 5;
            loaduom();
            //}
        }

        private void Txtitgrp_KeyDown_1(object sender, KeyEventArgs e)
        {
            //if (e.KeyCode == Keys.Enter)
            //{
            Genclass.type = 3;
            loaduom();
            //}
        }

        private void Cmditembom_Click_1(object sender, EventArgs e)
        {
            if (mode == 2)
            {
                Genclass.bomstr = HFGP.CurrentRow.Cells[0].Value.ToString();
                Genclass.Module.ClearTextBox(this, bompro);
                bompro.Visible = true;
                Txtptitem.Text = txtitname.Text;
                txtptparty.Text = txtparty.Text;
                hfg3.Refresh();
                hfg3.DataSource = 1;
                HFG4.Refresh();
                HFG4.DataSource = 1;
                //Titlep();
                //TitlepPROCESS();
                buttnbomext.Visible = true;
                Loadgrid3();
                Loadgrid4();

            }
            else
            {
                return;
            }

        }

        private void buttrqok_Click(object sender, EventArgs e)
        {
            mode = 2;
            if (txtbomitem.Text == "")
            {
                MessageBox.Show("Enter the ItemName");
                txtbomitem.Focus();
                return;
            }
            if (txtbomcode.Text == "")
            {
                MessageBox.Show("Enter the Itemcode");
                txtbomcode.Focus();
                return;
            }
            if (txtusedqty.Text == "")
            {
                MessageBox.Show("Enter the qty");
                txtusedqty.Focus();
                return;
            }
            if (txtperqty.Text == "")
            {
                MessageBox.Show("Enter the Perqty");
                txtperqty.Focus();
                return;
            }





            conn.Open();
            //if (mode == 2)
            //{
            qur.CommandText = "Exec Sp_bom " + Genclass.bomstr + "," + txtalterid.Text + "," + txtusedqty.Text + "," + txtperqty.Text + "," + mode + "," + Genclass.data1 + "";
            //qur.CommandText = "insert into itemsmbom values (" + Genclass.bomstr + ",'" + txtalterid.Text + "','" + txtusedqty.Text + "','" + txtperqty.Text + "')";
            qur.ExecuteNonQuery();
            //MessageBox.Show("Record has been saved", "Save", MessageBoxButtons.OK);
            //}


            conn.Close();

            txtbomitem.Text = "";
            txtbomcode.Text = "";
            txbomuom.Text = "";
            txtusedqty.Text = "";
            txtperqty.Text = "";

            Loadgrid1();




            //MessageBox.Show("Record has been saved", "Save", MessageBoxButtons.OK);





        }

        private void button1_Click(object sender, EventArgs e)
        {
            mode = 2;
            if (txtproceesname.Text == "")
            {
                MessageBox.Show("Enter the Processname");
                txtproceesname.Focus();
                return;
            }
            if (txtsqgno.Text == "")
            {
                MessageBox.Show("Enter the sequence");
                txtsqgno.Focus();
                return;
            }
            if (txtrate.Text == "")
            {
                MessageBox.Show("Enter the qty");
                txtrate.Focus();
                return;
            }
            if (txtprocessqty.Text == "")
            {
                MessageBox.Show("Enter the processqty");
                txtprocessqty.Focus();
                return;
            }


            conn.Open();

            {

                if (mode == 2)
                {
                    qur.CommandText = "exec sp_process " + Genclass.bomstr + ",'" + txtsqgno.Text + "','" + txtprocessid.Text + "','" + txtrate.Text + "','" + txtsettime.Text + "','" + txtprocestime.Text + "'," + txtprocessqty.Text + ",'" + txthour.Text + "'," + mode + "," + Genclass.data1 + "";
                    //qur.CommandText = "insert into itemsmprocess values (" + Genclass.bomstr + ",'" + txtsqgno.Text + "','" + txtprocessid.Text + "','" + txtrate.Text + "','" + txtsettime.Text + "','" + txtprocestime.Text + "'," + txtprocessqty.Text + ",'" + txthour.Text + "',0,1)";
                    qur.ExecuteNonQuery();
                    //MessageBox.Show("Record has been saved", "Save", MessageBoxButtons.OK);
                }



            }



            conn.Close();
            txtprocessid.Text = "";
            txtproceesname.Text = "";
            txtsqgno.Text = "";
            txtrate.Text = "";
            txtsettime.Text = "";
            txtprocessqty.Text = "";
            txtprocestime.Text = "";
            txthour.Text = "";
            Loadgrid2();
        }
        private void Loadgrid1()
        {
            try
            {
                conn.Open();

                {
                    string quy = "select itemm_uid,itemname,itemcode,c.generalname as uom,usedqty,qtyper,useditem_uid,a.uid from itemsmbom a inner join itemm b on a.useditem_uid=b.uid and a.companyid=" + Genclass.data1 + " inner join generalm c on b.uom_uid=c.uid and itemm_uid= " + HFGP.CurrentRow.Cells[0].Value.ToString() + "";
                    Genclass.cmd = new SqlCommand(quy, conn);
                }




                SqlDataAdapter aptr = new SqlDataAdapter(Genclass.cmd);
                DataTable tap = new DataTable();
                aptr.Fill(tap);

                hfg3.AutoGenerateColumns = false;
                hfg3.Refresh();
                hfg3.DataSource = null;
                hfg3.Rows.Clear();


                hfg3.ColumnCount = tap.Columns.Count;
                Genclass.i = 0;
                foreach (DataColumn column in tap.Columns)
                {
                    hfg3.Columns[Genclass.i].Name = column.ColumnName;
                    hfg3.Columns[Genclass.i].HeaderText = column.ColumnName;
                    hfg3.Columns[Genclass.i].DataPropertyName = column.ColumnName;
                    Genclass.i = Genclass.i + 1;
                }


                hfg3.Columns[0].Visible = false;
                hfg3.Columns[0].Width = 0;
                hfg3.Columns[1].Width = 350;
                hfg3.Columns[2].Width = 168;
                hfg3.Columns[3].Width = 72;
                hfg3.Columns[4].Width = 92;
                hfg3.Columns[5].Width = 84;
                hfg3.Columns[6].Visible = false;
                hfg3.Columns[7].Visible = false;




                hfg3.DataSource = tap;

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return;
            }
            finally
            {
                conn.Close();
                Genclass.cmd.Dispose();
                //Titlep();
            }
        }



        private void txtbomitem_KeyDown_1(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                Genclass.type = 2;
                loaduom();
            }
        }

        //private void txtbomcode_KeyDown_1(object sender, KeyEventArgs e)
        //{
        //    if (e.KeyCode == Keys.Enter)
        //    {
        //        type = 6;
        //        loaduom();
        //    }
        //}

        private void txtproceesname_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                Genclass.type = 7;
                loaduom();
            }
        }

        private void Loadgrid3()
        {
            try
            {
                conn.Open();

                {
                    string quy = "select itemm_uid,itemname,itemcode,c.generalname as uom,usedqty,qtyper,useditem_uid,a.uid from itemsmbom a inner join itemm b on a.useditem_uid=b.uid and a.companyid=" + Genclass.data1 + " inner join generalm c on b.uom_uid=c.uid  and itemm_uid= " + HFGP.CurrentRow.Cells[0].Value.ToString() + "";
                    Genclass.cmd = new SqlCommand(quy, conn);
                }




                SqlDataAdapter aptr = new SqlDataAdapter(Genclass.cmd);
                DataTable tap = new DataTable();
                aptr.Fill(tap);

                hfg3.AutoGenerateColumns = false;
                hfg3.Refresh();
                hfg3.DataSource = null;
                hfg3.Rows.Clear();


                hfg3.ColumnCount = tap.Columns.Count;
                Genclass.i = 0;
                foreach (DataColumn column in tap.Columns)
                {
                    hfg3.Columns[Genclass.i].Name = column.ColumnName;
                    hfg3.Columns[Genclass.i].HeaderText = column.ColumnName;
                    hfg3.Columns[Genclass.i].DataPropertyName = column.ColumnName;
                    Genclass.i = Genclass.i + 1;
                }


                hfg3.Columns[0].Visible = false;
                hfg3.Columns[0].Width = 0;
                hfg3.Columns[1].Width = 350;
                hfg3.Columns[2].Width = 168;
                hfg3.Columns[3].Width = 72;
                hfg3.Columns[4].Width = 92;
                hfg3.Columns[5].Width = 84;
                hfg3.Columns[6].Visible = false;
                hfg3.Columns[7].Visible = false;




                hfg3.DataSource = tap;

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return;
            }
            finally
            {
                conn.Close();
                Genclass.cmd.Dispose();
                //Titlep();
            }
        }
        private void Loadgrid4()
        {
            try
            {
                conn.Open();

                {
                    string quy = "select c.uid,c.processname as Process,a.sequenceno as SeqNo,processrateqty as qty,processsetuptime as SetTime ,processtime as ProcTime,processqty as ProcQty,processratehour as RateHr,b.uid,a.uid from itemsmprocess a inner join  itemm b on a.itemm_uid=b.uid and a.companyid=" + Genclass.data1 + " inner join processm c on a.processm_uid=c.uid  and a.itemm_uid= " + HFGP.CurrentRow.Cells[0].Value.ToString() + "";
                    Genclass.cmd = new SqlCommand(quy, conn);
                }




                SqlDataAdapter aptr = new SqlDataAdapter(Genclass.cmd);
                DataTable tap = new DataTable();
                aptr.Fill(tap);


                HFG4.AutoGenerateColumns = false;
                HFG4.Refresh();
                HFG4.DataSource = null;
                HFG4.Rows.Clear();


                HFG4.ColumnCount = tap.Columns.Count;
                Genclass.i = 0;
                foreach (DataColumn column in tap.Columns)
                {
                    HFG4.Columns[Genclass.i].Name = column.ColumnName;
                    HFG4.Columns[Genclass.i].HeaderText = column.ColumnName;
                    HFG4.Columns[Genclass.i].DataPropertyName = column.ColumnName;
                    Genclass.i = Genclass.i + 1;
                }


                HFG4.Columns[0].Visible = false;
                HFG4.Columns[0].Width = 0;
                HFG4.Columns[1].Width = 210;
                HFG4.Columns[2].Width = 167;
                HFG4.Columns[3].Width = 61;
                HFG4.Columns[4].Width = 86;
                HFG4.Columns[5].Width = 86;
                HFG4.Columns[6].Width = 86;
                HFG4.Columns[7].Width = 84;

                HFG4.Columns[8].Visible = false;
                HFG4.Columns[9].Visible = false;




                HFG4.DataSource = tap;

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return;
            }
            finally
            {
                conn.Close();
                Genclass.cmd.Dispose();
                //TitlepPROCESS();
            }
        }



        private void button3_Click(object sender, EventArgs e)
        {
            bompro.Visible = false;
            Genclass.Module.ClearTextBox(this, bompro);
        }



        private void hfg3_KeyDown(object sender, KeyEventArgs e)
        {
            mode = 3;
            conn.Open();
            if (e.KeyCode == Keys.Delete)

            {
                int i = hfg3.SelectedCells[0].RowIndex;
                qur.CommandText = "Exec Sp_bom " + Genclass.bomstr + "," + hfg3.Rows[i].Cells[6].Value + ",0,0," + mode + "," + Genclass.data1 + "";

            }
            qur.ExecuteNonQuery();
            MessageBox.Show("Record has been Deactivated", "Delete", MessageBoxButtons.OK);

            conn.Close();
            Loadgrid3();
        }

        private void HFG4_KeyDown(object sender, KeyEventArgs e)
        {
            mode = 3;
            conn.Close();
            conn.Open();
            if (e.KeyCode == Keys.Delete)
            {
                int i = HFG4.SelectedCells[0].RowIndex;
                qur.CommandText = "exec sp_process " + Genclass.bomstr + "," + HFG4.Rows[i].Cells[0].Value + ",0,0,0,0,0,0," + mode + "," + Genclass.data1 + "";

            }
            qur.ExecuteNonQuery();
            MessageBox.Show("Record has been Deactivated", "Delete", MessageBoxButtons.OK);

            conn.Close();
            Loadgrid4();
        }



        private void txtalterid_TextChanged(object sender, EventArgs e)
        {
            if (txtalterid.Text == "")
            {
                return;
            }
            else
            {
                Genclass.strsql = "select itemcode,generalname as uom,b.uid from itemm a inner join generalm b on a.uom_uid=b.uid where a.active=1 and a.uid=" + txtalterid.Text + "";
                Genclass.cmd = new SqlCommand(Genclass.strsql, conn);
                SqlDataAdapter aptr1 = new SqlDataAdapter(Genclass.cmd);
                DataTable tap1 = new DataTable();
                aptr1.Fill(tap1);
                txtbomcode.Text = tap1.Rows[0]["itemcode"].ToString();
                txbomuom.Text = tap1.Rows[0]["uom"].ToString();
                txtbomuomid.Text = tap1.Rows[0]["uid"].ToString();
            }
        }



        private void txtgrp_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                Genclass.type = 8;
                loaduom();
            }
        }


        private void Txtitcode_TextChanged(object sender, EventArgs e)
        {

            txtcusitcode.Text = Txtitcode.Text;
        }

        private void txtitname_TextChanged(object sender, EventArgs e)
        {
            txtcusitname.Text = txtitname.Text;
        }



        //private void chkact_Click(object sender, EventArgs e)
        //{
        //    if (chkact.Checked == false)
        //    {
        //        chkact.Text = "Inactive";

        //        Loadgrid();
        //    }
        //    else
        //    {

        //        chkact.Text = "Active";
        //        Loadgrid();

        //    }
        //}

        //private void Chkedtact_Click(object sender, EventArgs e)
        //{
        //    if (Chkedtact.Checked == false)
        //    {
        //        Chkedtact.Text = "Inactive";

        //        Loadgrid();
        //    }
        //    else
        //    {

        //        Chkedtact.Text = "Active";

        //        Loadgrid();
        //    }

        //}

        private void chkact_CheckedChanged(object sender, EventArgs e)
        {


            Loadgrid();


        }

        private void Chkedtact_CheckedChanged(object sender, EventArgs e)
        {
            //if (Chkedtact.Checked == false)
            //{
            //    Chkedtact.Text = "Inactive";


            //}
            //else
            //{

            //    Chkedtact.Text = "Active";


            //}


        }

        private void hfg3_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void HFG4_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void txtproceesname_TextChanged(object sender, EventArgs e)
        {
            txtsqgno.Focus();
        }

        private void txtbomitem_TextChanged(object sender, EventArgs e)
        {
            txtusedqty.Focus();
        }

        private void btndelete_Click(object sender, EventArgs e)
        {
            conn.Open();
            mode = 3;
            qur.CommandText = "";
            int i = HFGP.SelectedCells[0].RowIndex;
            uid = HFGP.Rows[i].Cells[0].Value.ToString();
            qur.CommandText = "Exec sp_item 0,0,0,0,0,0,0,0,0,0,0," + uid + "," + mode + "," + Genclass.data1 + "";
            qur.ExecuteNonQuery();


            string quy = "select * from Msgdes";
            Genclass.cmd = new SqlCommand(quy, conn);

            SqlDataAdapter aptr = new SqlDataAdapter(Genclass.cmd);
            DataTable tap = new DataTable();
            aptr.Fill(tap);

            MessageBox.Show(tap.Rows[0]["Msg"].ToString(), "Save", MessageBoxButtons.OK);
            conn.Close();
            Loadgrid();

        }

        private void txtuom_TextChanged_1(object sender, EventArgs e)
        {
            txttax1.Focus();
        }

        private void txtgrp_TextChanged(object sender, EventArgs e)
        {
            Txtitgrp.Focus();
        }

        private void Txtitgrp_TextChanged(object sender, EventArgs e)
        {
            txtspec.Focus();
        }

        private void txtparty_TextChanged_1(object sender, EventArgs e)
        {
            txtcusitcode.Focus();
        }

        private void txtusedqty_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) &&
        (e.KeyChar != '.'))
            {
                e.Handled = true;
            }

            // only allow one decimal point
            if ((e.KeyChar == '.') && ((sender as TextBox).Text.IndexOf('.') > -1))
            {
                e.Handled = true;
            }
        }

        private void txthsn_KeyDown(object sender, KeyEventArgs e)
        {
            Genclass.type = 9;
            loaduom();
        }

        private void chkact_CheckedChanged_1(object sender, EventArgs e)
        {
            Loadgrid();
        }

        private void btnadd_Click_1(object sender, EventArgs e)
        {
            mode = 1;
            uid = "0";

            Genpan.Visible = false;
            bompro.Visible = false;

            panadd.Visible = false;
            Editpnl.Visible = true;
            Chkedtact.Checked = true;
            Genclass.Module.ClearTextBox(this, Editpnl);
            txtcatid.Text = "0";
            txtitgrpid.Text = "0";
            Txtitcode.Focus();

        }

        private void butedit_Click(object sender, EventArgs e)
        {

            mode = 2;
            Editpnl.Visible = true;
            Genpan.Visible = false;
            int i = HFGP.SelectedCells[0].RowIndex;
            Txtitcode.Text = HFGP.Rows[i].Cells[1].Value.ToString();
            txtitname.Text = HFGP.Rows[i].Cells[2].Value.ToString();
            txtcusitcode.Text = HFGP.Rows[i].Cells[3].Value.ToString();
            txtcusitname.Text = HFGP.Rows[i].Cells[4].Value.ToString();
            txtparty.Text = HFGP.Rows[i].Cells[5].Value.ToString();
            //txtpartyid.Text = HFGP.Rows[i].Cells[5].Value.ToString();
            txtuom.Text = HFGP.Rows[i].Cells[6].Value.ToString();
            Txtitgrp.Text = HFGP.Rows[i].Cells[7].Value.ToString();
            txtcat.SelectedValue = HFGP.Rows[i].Cells[8].Value.ToString();
            txtitgrpid.Text = HFGP.Rows[i].Cells[9].Value.ToString();
            txtcat.SelectedValue = HFGP.Rows[i].Cells[10].Value.ToString();
            txtspec.Text = HFGP.Rows[i].Cells[11].Value.ToString();
            txtpartyid.Text = HFGP.Rows[i].Cells[12].Value.ToString();
            txtcat.Text = HFGP.Rows[i].Cells[13].Value.ToString();
            txtgrp.Text = HFGP.Rows[i].Cells[14].Value.ToString();
            txtgrpid.Text = HFGP.Rows[i].Cells[15].Value.ToString();
            txthsn.Text = HFGP.Rows[i].Cells[16].Value.ToString();
            txthsnid.Text = HFGP.Rows[i].Cells[17].Value.ToString();

            txthsncode.Text = HFGP.Rows[i].Cells[20].Value.ToString();
            txttax.Text = HFGP.Rows[i].Cells[19].Value.ToString();
            txttax.SelectedValue = HFGP.Rows[i].Cells[21].Value.ToString();
            if (HFGP.Rows[i].Cells[18].Value.ToString() == "True")
            {
                Chkedtact.Checked = true;
            }
            else
            {
                Chkedtact.Checked = false;
            }
            uid = HFGP.Rows[i].Cells[0].Value.ToString();


            panadd.Visible = false;
        }

        private void butexit_Click(object sender, EventArgs e)
        {
            string message = "Are You Sure to Inctive this item ?";
            string caption = "Dilama";
            MessageBoxButtons buttons = MessageBoxButtons.YesNo;
            DialogResult result;

            result = MessageBox.Show(message, caption, buttons);

            if (result == System.Windows.Forms.DialogResult.Yes)
            {
                int i = HFGP.SelectedCells[0].RowIndex;
                int uid = HFGP.SelectedCells[0].RowIndex;

                conn.Close();
                conn.Open();


                Genclass.strsql = "select * from stransactionsp a inner join stransactionsplist b on a.uid=b.transactionspuid where b.itemuid=" + HFGP.CurrentRow.Cells[0].Value.ToString() + "  and a.companyid=" + Genclass.data1 + "";

                Genclass.cmd = new SqlCommand(Genclass.strsql, conn);
                SqlDataAdapter aptr4 = new SqlDataAdapter(Genclass.cmd);
                DataTable tap4 = new DataTable();
                aptr4.Fill(tap4);
                if (tap4.Rows.Count == 0)
                {
                    qur.CommandText = "delete from itemm where uid=" + HFGP.CurrentRow.Cells[0].Value.ToString() + " ";
                    qur.ExecuteNonQuery();

                }
                else
                {
                    MessageBox.Show("This Item already refered in transactions");
                }
                Loadgrid();
            }
        }

        private void buttnext1_Click(object sender, EventArgs e)
        {
            this.Dispose();
        }

        private void butnsave_Click(object sender, EventArgs e)
        {
            //if (Txtitcode.Text == "")
            //{
            //    MessageBox.Show("Enter the ItemCode");
            //    Txtitcode.Focus();
            //    return;
            //}
            if (txtitname.Text == "")
            {
                MessageBox.Show("Enter the ItemName");
                txtitname.Focus();
                return;
            }
            //if (txtuom1.Text == "")
            //{
            //    MessageBox.Show("Enter the UoM");
            //    txtuom1.Focus();
            //    return;
            //}
            //if (txtcatid.Text == "")
            //{
            //    MessageBox.Show("Enter the Category");
            //    txtcat.Focus();
            //    return;
            //}
            //if (txtitgrpid.Text == "")
            //{
            //    MessageBox.Show("Enter the ItemSubGroup");
            //    Txtitgrp.Focus();
            //    return;
            //}
            //if (txtparty.Text == "")
            //{
            //    MessageBox.Show("Enter the Partyname");
            //    Txtitgrp.Focus();
            //    return;
            //}
            if (txtgrp.Text == "")
            {
                MessageBox.Show("Enter the ItemGroup");
                txtgrp.Focus();
                return;
            }
            //if (txthsn.Text == "")
            //{
            //    MessageBox.Show("Enter the Hsn");
            //    txthsn.Focus();
            //    return;
            //}


            conn.Open();

            if (txtcatid.Text == "")
            {
                txtcatid.Text = "0";
            }
            if (txtitgrpid.Text == "")
            {
                txtitgrpid.Text = "0";
            }
            if (txtpartyid.Text == "")
            {
                txtpartyid.Text = "0";
            }





            qur.CommandText = "";
            qur.CommandText = "Exec sp_item '" + Txtitcode.Text + "','" + txtitname.Text + "','" + txtcusitcode.Text + "','" + txtcusitname.Text + "','" + txtspec.Text + "'," + txtuom.SelectedValue + "," + txtcat.SelectedValue + "," + txtitgrpid.Text + ",'" + Chkedtact.Checked + "'," + txtpartyid.Text + "," + txtgrpid.Text + "," + uid + ",0," + mode + "," + Genclass.data1 + ",'" + txthsncode.Text + "'," + txttax.SelectedValue + "";
            qur.ExecuteNonQuery();


            string quy = "select * from Msgdes";
            Genclass.cmd = new SqlCommand(quy, conn);

            SqlDataAdapter aptr = new SqlDataAdapter(Genclass.cmd);
            DataTable tap = new DataTable();
            aptr.Fill(tap);

            MessageBox.Show(tap.Rows[0]["Msg"].ToString(), "Save", MessageBoxButtons.OK);


            //}


            //if (mode == 2)
            //{

            //    qur.CommandText = "Update ItemM set ItemCode='" + Txtitcode.Text + "',ItemName='" + txtitname.Text + "',CusItemCode='" + txtcusitcode.Text + "',CusItemName='" + txtcusitname.Text + "',ItemSpec1='" + txtspec.Text + "',UOM_UId=" + txtuomid.Text + ",CategoryTypeM_UId=" + txtcatid.Text + ",ItemSubGroup_UId=" + txtitgrpid.Text + ",partyuid=" + txtpartyid.Text + " ,ItemGroup_UId=" + txtgrpid.Text + " where uid=" + uid + "";

            //    qur.ExecuteNonQuery();

            //    MessageBox.Show("Record has been Updated", "Save", MessageBoxButtons.OK);
            //}



            conn.Close();
            Loadgrid();
            Genpan.Visible = true;
            Editpnl.Visible = false;
            panadd.Visible = true;
        }

        private void btnaddrcan_Click_2(object sender, EventArgs e)
        {
            Editpnl.Visible = false;
            Genpan.Visible = true;
            panadd.Visible = true;
            Genclass.Module.ClearTextBox(this, Editpnl);
            Loadgrid();
        }

        private void buttnext2_Click(object sender, EventArgs e)
        {
            this.Dispose();
        }

        private void Editpnl_Paint(object sender, PaintEventArgs e)
        {

        }

        private void buttnbomext_Click(object sender, EventArgs e)
        {
            bompro.Visible = false;
            buttnbomext.Visible = false;
        }

        private void button4_Click(object sender, EventArgs e)
        {
            if (mode == 2)
            {
                Genclass.bomstr = HFGP.CurrentRow.Cells[0].Value.ToString();
                Genclass.Module.ClearTextBox(this, bompro);
                bompro.Visible = true;
                Txtptitem.Text = txtitname.Text;
                txtptparty.Text = txtparty.Text;
                hfg3.Refresh();
                hfg3.DataSource = 1;
                HFG4.Refresh();
                HFG4.DataSource = 1;
                //Titlep();
                //TitlepPROCESS();
                buttnbomext.Visible = true;
                Loadgrid3();
                Loadgrid4();

            }
            else
            {
                return;
            }
        }

        private void HFGP_KeyUp(object sender, KeyEventArgs e)
        {
            if (HFGP.Rows[0].Cells[0].Value.ToString() != "")
            {
                int index = HFGP.SelectedRows[0].Index;
                if (index < HFGP.Rows.Count - 1)
                {
                    index = index + 1;
                    int index1 = HFGP.Rows.Count - 1;
                    lblno1.Text = index.ToString();
                    lblno2.Text = "of " + index1.ToString();
                }
            }
        }

        private void HFGP_CellMouseUp(object sender, DataGridViewCellMouseEventArgs e)
        {
            if (HFGP.Rows[0].Cells[0].Value.ToString() != "")
            {
                int index = HFGP.SelectedRows[0].Index;
                if (index < HFGP.Rows.Count - 1)
                {
                    index = index + 1;
                    int index1 = HFGP.Rows.Count - 1;
                    lblno1.Text = index.ToString();
                    lblno2.Text = "of " + index1.ToString();
                }
            }
        }

        private void buttnnvfst_Click(object sender, EventArgs e)
        {
            int index = HFGP.SelectedRows[0].Index;
            int index1 = 0;
            int index2 = HFGP.Rows.Count - 1;

            HFGP.Rows[index].Selected = false;
            HFGP.Rows[index1].Selected = true;
            index1 = index1 + 1;
            lblno1.Text = index1.ToString();
            lblno2.Text = "of " + index2.ToString();
        }

        private void buttnnxtlft_Click(object sender, EventArgs e)
        {

            int index = HFGP.SelectedRows[0].Index;
            int index1 = HFGP.Rows.Count - 1; ;
            if (index > 0)
            {
                HFGP.Rows[index].Selected = false;
                HFGP.Rows[index - 1].Selected = true;
                lblno1.Text = index.ToString();
                lblno2.Text = "of " + index1.ToString();
            }
        }

        private void buttrnxt_Click(object sender, EventArgs e)
        {

            int index = HFGP.SelectedRows[0].Index;
            int index2 = HFGP.SelectedRows[0].Index + 2;
            int index1 = HFGP.Rows.Count - 1; ;
            if (index < HFGP.Rows.Count - 2)
            {
                HFGP.Rows[index].Selected = false;
                HFGP.Rows[index + 1].Selected = true;
                lblno1.Text = index2.ToString();
                lblno2.Text = "of " + index1.ToString();

            }

        }

        private void btnfinnxt_Click(object sender, EventArgs e)
        {
            int index = HFGP.SelectedRows[0].Index;
            int index1 = HFGP.Rows.Count - 1;


            HFGP.Rows[index].Selected = false;

            HFGP.Rows[index1 - 1].Selected = true;
            lblno1.Text = index1.ToString();
            lblno2.Text = "of " + index1.ToString();
        }

        private void Genpan_Paint(object sender, PaintEventArgs e)
        {

        }

        private void txtparty_Click(object sender, EventArgs e)
        {
            Genclass.type = 4;
            loaduom();
        }

        private void txtuom_Click(object sender, EventArgs e)
        {
            Genclass.type = 1;
            loaduom();
        }

        private void txtcat_Click(object sender, EventArgs e)
        {
            Genclass.type = 5;
            loaduom();
        }

        private void txtgrp_Click(object sender, EventArgs e)
        {
            Genclass.type = 8;
            loaduom();
        }

        private void Txtitgrp_Click(object sender, EventArgs e)
        {
            Genclass.type = 3;
            loaduom();
            conn.Close();
        }

        private void txthsncode_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                Genclass.type = 11;
                loaduom();
            }
        }

        private void txtspec_TextChanged(object sender, EventArgs e)
        {

        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void comboBox1_Click(object sender, EventArgs e)
        {
            Loadgrid();
        }

        private void buttrqok_Click_1(object sender, EventArgs e)
        {

        }

    }

}
