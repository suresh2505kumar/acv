﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace ACV
{
    public partial class FrmDCRegister : Form
    {
        public FrmDCRegister()
        {
            InitializeComponent();
        }
        SqlConnection conn = new SqlConnection("Data Source=" + Genclass.data2 + "; Initial Catalog=" + Genclass.data5 + ";User id=" + Genclass.data3 + ";Password=" + Genclass.data4 + "");
        SQLDBHelper db = new SQLDBHelper();

        private void FrmDCRegister_Load(object sender, EventArgs e)
        {
            GetParty();
        }

        protected void GetParty()
        {
            try
            {
                DataTable dt = db.GetDataWithoutParam(CommandType.StoredProcedure, "SP_getParty", conn);
                CmbCustomer.DataSource = null;
                CmbCustomer.DisplayMember = "Name";
                CmbCustomer.ValueMember = "Uid";
                CmbCustomer.DataSource = dt;
                DataRow row = dt.NewRow();
                row[0] = "0";
                row[1] = "ALL";
                row[2] = "ALL";
                dt.Rows.InsertAt(row, 0);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message,ex.StackTrace);
                return;
            }

        }

        private void BtnExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void BtnPrint_Click(object sender, EventArgs e)
        {
            try
            {
                int Uid = Convert.ToInt32(CmbCustomer.SelectedValue);
                Genclass.FromDate = Convert.ToDateTime(DtpFromDate.Text);
                Genclass.ToDate = Convert.ToDateTime(DtpToDate.Text);
                Genclass.parameter = "S";
                if(ChckSummary.Checked == true)
                {
                    Genclass.parameter = "S";
                }
                else
                {
                    Genclass.parameter = "D";
                }
                Genclass.Dtype = 16;
                Genclass.RpeortId = Uid;
                FrmReprtViwer rpt = new FrmReprtViwer
                {
                    StartPosition = FormStartPosition.CenterScreen,
                    MdiParent = this.MdiParent
                };
                rpt.Show();
            }
            catch (Exception)
            {

                throw;
            }
        }

        private void ChckSummary_CheckedChanged(object sender, EventArgs e)
        {

        }
    }
}
