﻿namespace ACV
{
    partial class FrmKnottingStatus
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.GrFront = new System.Windows.Forms.GroupBox();
            this.sfDataGridRunout = new Syncfusion.WinForms.DataGrid.SfDataGrid();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.sfDataGridKnotting = new Syncfusion.WinForms.DataGrid.SfDataGrid();
            this.BtnExit = new System.Windows.Forms.Button();
            this.BtnOk = new System.Windows.Forms.Button();
            this.DtpToDate = new System.Windows.Forms.DateTimePicker();
            this.label1 = new System.Windows.Forms.Label();
            this.DtpFromDate = new System.Windows.Forms.DateTimePicker();
            this.label4 = new System.Windows.Forms.Label();
            this.GrFront.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.sfDataGridRunout)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sfDataGridKnotting)).BeginInit();
            this.SuspendLayout();
            // 
            // GrFront
            // 
            this.GrFront.Controls.Add(this.DtpFromDate);
            this.GrFront.Controls.Add(this.label4);
            this.GrFront.Controls.Add(this.sfDataGridRunout);
            this.GrFront.Controls.Add(this.label3);
            this.GrFront.Controls.Add(this.label2);
            this.GrFront.Controls.Add(this.sfDataGridKnotting);
            this.GrFront.Controls.Add(this.BtnExit);
            this.GrFront.Controls.Add(this.BtnOk);
            this.GrFront.Controls.Add(this.DtpToDate);
            this.GrFront.Controls.Add(this.label1);
            this.GrFront.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.GrFront.Location = new System.Drawing.Point(8, 0);
            this.GrFront.Name = "GrFront";
            this.GrFront.Size = new System.Drawing.Size(1192, 555);
            this.GrFront.TabIndex = 0;
            this.GrFront.TabStop = false;
            // 
            // sfDataGridRunout
            // 
            this.sfDataGridRunout.AccessibleName = "Table";
            this.sfDataGridRunout.AllowFiltering = true;
            this.sfDataGridRunout.AllowResizingColumns = true;
            this.sfDataGridRunout.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.sfDataGridRunout.Location = new System.Drawing.Point(7, 53);
            this.sfDataGridRunout.Name = "sfDataGridRunout";
            this.sfDataGridRunout.Size = new System.Drawing.Size(1179, 495);
            this.sfDataGridRunout.TabIndex = 425;
            this.sfDataGridRunout.Text = "sfDataGrid2";
            this.sfDataGridRunout.QueryCellStyle += new Syncfusion.WinForms.DataGrid.Events.QueryCellStyleEventHandler(this.sfDataGridRunout_QueryCellStyle);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(7, 32);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(53, 18);
            this.label3.TabIndex = 428;
            this.label3.Text = "Runout";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 263);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(60, 18);
            this.label2.TabIndex = 427;
            this.label2.Text = "Knotting";
            // 
            // sfDataGridKnotting
            // 
            this.sfDataGridKnotting.AccessibleName = "Table";
            this.sfDataGridKnotting.AllowFiltering = true;
            this.sfDataGridKnotting.AllowResizingColumns = true;
            this.sfDataGridKnotting.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.sfDataGridKnotting.Location = new System.Drawing.Point(6, 284);
            this.sfDataGridKnotting.Name = "sfDataGridKnotting";
            this.sfDataGridKnotting.Size = new System.Drawing.Size(771, 200);
            this.sfDataGridKnotting.TabIndex = 426;
            this.sfDataGridKnotting.Text = "sfDataGrid2";
            // 
            // BtnExit
            // 
            this.BtnExit.Location = new System.Drawing.Point(1111, 20);
            this.BtnExit.Name = "BtnExit";
            this.BtnExit.Size = new System.Drawing.Size(75, 27);
            this.BtnExit.TabIndex = 3;
            this.BtnExit.Text = "Exit";
            this.BtnExit.UseVisualStyleBackColor = true;
            this.BtnExit.Click += new System.EventHandler(this.BtnExit_Click);
            // 
            // BtnOk
            // 
            this.BtnOk.Location = new System.Drawing.Point(702, 21);
            this.BtnOk.Name = "BtnOk";
            this.BtnOk.Size = new System.Drawing.Size(53, 27);
            this.BtnOk.TabIndex = 2;
            this.BtnOk.Text = "OK";
            this.BtnOk.UseVisualStyleBackColor = true;
            this.BtnOk.Click += new System.EventHandler(this.BtnOk_Click);
            // 
            // DtpToDate
            // 
            this.DtpToDate.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.DtpToDate.Location = new System.Drawing.Point(573, 21);
            this.DtpToDate.Name = "DtpToDate";
            this.DtpToDate.Size = new System.Drawing.Size(123, 26);
            this.DtpToDate.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(515, 25);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(54, 18);
            this.label1.TabIndex = 0;
            this.label1.Text = "To Date";
            // 
            // DtpFromDate
            // 
            this.DtpFromDate.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.DtpFromDate.Location = new System.Drawing.Point(386, 21);
            this.DtpFromDate.Name = "DtpFromDate";
            this.DtpFromDate.Size = new System.Drawing.Size(123, 26);
            this.DtpFromDate.TabIndex = 430;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(308, 25);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(72, 18);
            this.label4.TabIndex = 429;
            this.label4.Text = "From Date";
            // 
            // FrmKnottingStatus
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.ClientSize = new System.Drawing.Size(1207, 560);
            this.Controls.Add(this.GrFront);
            this.Name = "FrmKnottingStatus";
            this.Text = "Knotting Status";
            this.GrFront.ResumeLayout(false);
            this.GrFront.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.sfDataGridRunout)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sfDataGridKnotting)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox GrFront;
        private System.Windows.Forms.Button BtnExit;
        private System.Windows.Forms.Button BtnOk;
        private System.Windows.Forms.DateTimePicker DtpToDate;
        private System.Windows.Forms.Label label1;
        private Syncfusion.WinForms.DataGrid.SfDataGrid sfDataGridRunout;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private Syncfusion.WinForms.DataGrid.SfDataGrid sfDataGridKnotting;
        private System.Windows.Forms.DateTimePicker DtpFromDate;
        private System.Windows.Forms.Label label4;
    }
}