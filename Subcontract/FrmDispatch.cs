﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace ACV
{
    public partial class FrmDispatch : Form
    {
        public FrmDispatch()
        {
            InitializeComponent();
        }
        SQLDBHelper db = new SQLDBHelper();
        SqlConnection conn = new SqlConnection("Data Source=" + Genclass.data2 + "; Initial Catalog=" + Genclass.data5 + ";User id=" + Genclass.data3 + ";Password=" + Genclass.data4 + "");
        private void FrmDispatch_Load(object sender, EventArgs e)
        {

        }

        private void LoadDispatch()
        {
            try
            {
                SqlParameter[] parameters = {
                    new SqlParameter("@Date",Convert.ToDateTime(dtpDate.Text))
                };
                DataTable dt = db.GetDataWithParam(CommandType.StoredProcedure, "SP_getDispatch", parameters, conn);
                DataGridDispatch.DataSource = null;
                DataGridDispatch.AutoGenerateColumns = false;
                DataGridDispatch.ColumnCount = 4;
                DataGridDispatch.Columns[0].Name = "ID";
                DataGridDispatch.Columns[0].HeaderText = "ID";
                DataGridDispatch.Columns[0].DataPropertyName = "Uid";
                DataGridDispatch.Columns[0].Visible = false;

                DataGridDispatch.Columns[1].Name = "DispatchNo";
                DataGridDispatch.Columns[1].HeaderText = "DispatchNo";
                DataGridDispatch.Columns[1].DataPropertyName = "DispatchNo";

                DataGridDispatch.Columns[2].Name = "DispatchDate";
                DataGridDispatch.Columns[2].HeaderText = "DispatchDate";
                DataGridDispatch.Columns[2].DataPropertyName = "DispatchDate";

                DataGridDispatch.Columns[3].Name = "ID";
                DataGridDispatch.Columns[3].HeaderText = "ID";
                DataGridDispatch.Columns[3].DataPropertyName = "ID";
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
