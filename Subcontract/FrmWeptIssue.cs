﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Windows.Forms;

namespace ACV
{
    public partial class FrmWeptIssue : Form
    {
        public FrmWeptIssue()
        {
            InitializeComponent();
        }
        SqlConnection conn = new SqlConnection("Data Source=" + Genclass.data2 + "; Initial Catalog=" + Genclass.data5 + ";User id=" + Genclass.data3 + ";Password=" + Genclass.data4 + "");
        SQLDBHelper db = new SQLDBHelper();
        int Fillid;
        BindingSource bsParty = new BindingSource();
        BindingSource bsItem = new BindingSource();
        BindingSource bsMill = new BindingSource();
        public int SelectId = 0;
        private void FrmWeptIssue_Load(object sender, EventArgs e)
        {
            Genclass.Module.buttonstyleform(this);
            Genclass.Module.buttonstylepanel(panadd);
            LoadWeightrGrid();
            LoadButtons(0);
            GetWeptIssue();
        }

        protected void LoadWeightrGrid()
        {
            try
            {
                DataGridWeight.DataSource = null;
                DataGridWeight.AutoGenerateColumns = false;
                DataGridWeight.ColumnCount = 10;
                DataGridWeight.Columns[0].Name = "ItemName";
                DataGridWeight.Columns[0].HeaderText = "ItemName";
                DataGridWeight.Columns[0].Width = 150;

                DataGridWeight.Columns[1].Name = "MillName";
                DataGridWeight.Columns[1].HeaderText = "MillName";
                DataGridWeight.Columns[1].Width = 130;

                DataGridWeight.Columns[2].Name = "Bag No";
                DataGridWeight.Columns[2].HeaderText = "Bag No";
                DataGridWeight.Columns[2].Width = 80;

                DataGridWeight.Columns[3].Name = "Tare Wght";
                DataGridWeight.Columns[3].HeaderText = "Tare Wght";
                DataGridWeight.Columns[3].Width = 100;

                DataGridWeight.Columns[4].Name = "Gross Wght";
                DataGridWeight.Columns[4].HeaderText = "Gross Wghto";
                DataGridWeight.Columns[4].Width = 90;

                DataGridWeight.Columns[5].Name = "NetWeight";
                DataGridWeight.Columns[5].HeaderText = "NetWeight";
                DataGridWeight.Columns[5].DefaultCellStyle.Format = "N3";
                DataGridWeight.Columns[5].Width = 90;

                DataGridWeight.Columns[6].Name = "ItemUid";
                DataGridWeight.Columns[6].HeaderText = "ItemUid";
                DataGridWeight.Columns[6].Visible = false;

                DataGridWeight.Columns[7].Name = "PkDUid";
                DataGridWeight.Columns[7].HeaderText = "PkDUid";
                DataGridWeight.Columns[7].Visible = false;

                DataGridWeight.Columns[8].Name = "MillUid";
                DataGridWeight.Columns[8].HeaderText = "MillUid";
                DataGridWeight.Columns[8].Visible = false;

                DataGridWeight.Columns[9].Name = "PackType";
                DataGridWeight.Columns[9].HeaderText = "PackType";
                DataGridWeight.Columns[9].Width = 80;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Inforamtion", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        protected void LoadButtons(int id)
        {
            try
            {
                if (id == 0)
                {
                    grPkBack.Visible = false;
                    grPkFront.Visible = true;
                    btnAdd.Visible = true;
                    btnEdit.Visible = true;
                    btnDelete.Visible = true;
                    btnExit.Visible = true;
                    chckActive.Visible = true;
                    btnFstBack.Visible = true;
                    btnBack.Visible = true;
                    btnNext.Visible = true;
                    btnFstNext.Visible = true;
                    panNumbers.Visible = true;
                    btnsave.Visible = false;
                    btnaddrcan.Visible = false;
                    btnsave.Text = "Save";
                }
                else if (id == 1)
                {
                    grPkBack.Visible = true;
                    grPkFront.Visible = false;
                    btnAdd.Visible = false;
                    btnEdit.Visible = false;
                    btnDelete.Visible = false;
                    btnExit.Visible = false;
                    chckActive.Visible = false;
                    btnFstBack.Visible = false;
                    btnBack.Visible = false;
                    btnNext.Visible = false;
                    btnFstNext.Visible = false;
                    panNumbers.Visible = false;
                    btnsave.Visible = true;
                    btnaddrcan.Visible = true;
                    btnsave.Text = "Save";
                }

                else if (id == 2)
                {
                    grPkBack.Visible = true;
                    grPkFront.Visible = false;
                    btnAdd.Visible = false;
                    btnEdit.Visible = false;
                    btnDelete.Visible = false;
                    btnExit.Visible = false;
                    chckActive.Visible = false;
                    btnFstBack.Visible = false;
                    btnBack.Visible = false;
                    btnNext.Visible = false;
                    btnFstNext.Visible = false;
                    panNumbers.Visible = false;
                    btnsave.Visible = true;
                    btnaddrcan.Visible = true;
                    btnsave.Text = "Update";
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        protected void ClearControl()
        {
            try
            {
                txtSlipNumber.Text = string.Empty;
                txtItem.Text = string.Empty;
                txtItem.Tag = string.Empty;
                txtBagNo.Text = string.Empty;
                txtWeight.Text = string.Empty;
                txtNoogBags.Text = string.Empty;
                txtTotalWeight.Text = string.Empty;
                DataGridWeight.Rows.Clear();
                LoadWeightrGrid();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            try
            {
                ClearControl();
                LoadButtons(1);
                cmbPackType.SelectedIndex = 0;
                cmbManulaPackType.SelectedIndex = 0;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Inforamtion", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        protected void FillGrid(DataTable dt, int FillId)
        {
            try
            {
                DataGridCommon.DataSource = null;
                DataGridCommon.AutoGenerateColumns = false;
                if (FillId == 2)
                {
                    Fillid = 2;
                    DataGridCommon.ColumnCount = 3;
                    DataGridCommon.Columns[0].Name = "Uid";
                    DataGridCommon.Columns[0].HeaderText = "Uid";
                    DataGridCommon.Columns[0].DataPropertyName = "Uid";
                    DataGridCommon.Columns[1].Name = "Name";
                    DataGridCommon.Columns[1].HeaderText = "Name";
                    DataGridCommon.Columns[1].DataPropertyName = "ItemName";
                    DataGridCommon.Columns[2].Name = "Code";
                    DataGridCommon.Columns[2].HeaderText = "Code";
                    DataGridCommon.Columns[2].DataPropertyName = "ItemCode";
                    DataGridCommon.Columns[1].Width = 260;
                    DataGridCommon.DataSource = bsItem;
                    DataGridCommon.Columns[0].Visible = false;
                }
                else
                {
                    Fillid = 3;
                    DataGridCommon.ColumnCount = 3;
                    DataGridCommon.Columns[0].Name = "Uid";
                    DataGridCommon.Columns[0].HeaderText = "Uid";
                    DataGridCommon.Columns[0].DataPropertyName = "Uid";
                    DataGridCommon.Columns[1].Name = "Name";
                    DataGridCommon.Columns[1].HeaderText = "Name";
                    DataGridCommon.Columns[1].DataPropertyName = "Name";

                    DataGridCommon.Columns[2].Name = "Code";
                    DataGridCommon.Columns[2].HeaderText = "Code";
                    DataGridCommon.Columns[2].DataPropertyName = "Code";
                    DataGridCommon.Columns[1].Width = 260;
                    DataGridCommon.DataSource = bsMill;
                    DataGridCommon.Columns[0].Visible = false;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void chckNofBags_CheckedChanged(object sender, EventArgs e)
        {
            if (chckNofBags.Checked == true)
            {
                panBulkEntry.Visible = true;
            }
            else
            {
                panBulkEntry.Visible = false;
            }
        }

        private void btnLoad_Click(object sender, EventArgs e)
        {
            try
            {
                if (txtFromBagNo.Text != string.Empty && txtBNofBags.Text != string.Empty && txtBGrossWght.Text != string.Empty && txtBTareWght.Text != string.Empty)
                {
                    decimal net = Convert.ToDecimal(txtBGrossWght.Text) - Convert.ToDecimal(txtBTareWght.Text);
                    int bags = Convert.ToInt16(txtBNofBags.Text);
                    int hg = 0;
                    int kk = 0;
                    int i = 0;
                    for (int p = 0; p < bags; p++)
                    {
                        kk = Convert.ToInt16(txtFromBagNo.Text) + hg;
                        for (int y = 0; y < DataGridWeight.Rows.Count - 1; y++)
                        {
                            if (DataGridWeight.Rows[y].Cells[2].Value.ToString() == Convert.ToString(kk))
                            {
                                MessageBox.Show("Bagno alreay Exist");
                                return;
                            }
                        }
                        i += 1;
                        DataGridViewRow row = (DataGridViewRow)DataGridWeight.Rows[0].Clone();
                        row.Cells[0].Value = txtItem.Text;
                        row.Cells[1].Value = txtMillName.Text;
                        row.Cells[2].Value = kk;
                        row.Cells[3].Value = txtBTareWght.Text;
                        row.Cells[4].Value = txtBGrossWght.Text;
                        row.Cells[5].Value = net;
                        row.Cells[6].Value = txtItem.Tag;
                        row.Cells[7].Value = 0;
                        row.Cells[8].Value = txtMillName.Tag;
                        row.Cells[9].Value = cmbPackType.Text;
                        DataGridWeight.Rows.Add(row);
                        LoadNoBag();
                        hg += 1;
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            try
            {
                if (txtItem.Text != string.Empty && txtBagNo.Text != string.Empty && txtWeight.Text != string.Empty)
                {
                    bool checkBagNo = CheckGridValue();
                    if (checkBagNo == false)
                    {
                        DataGridViewRow row = (DataGridViewRow)DataGridWeight.Rows[0].Clone();
                        row.Cells[0].Value = txtItem.Text;
                        row.Cells[1].Value = txtMillName.Text;
                        row.Cells[2].Value = txtBagNo.Text;
                        row.Cells[3].Value = txtTarWght.Text;
                        row.Cells[4].Value = txtGrossWght.Text;
                        row.Cells[5].Value = txtWeight.Text;
                        row.Cells[6].Value = txtItem.Tag;
                        row.Cells[7].Value = 0;
                        row.Cells[8].Value = txtMillName.Tag;
                        row.Cells[9].Value = cmbManulaPackType.Text;
                        DataGridWeight.Rows.Add(row);
                        LoadNoBag();
                    }
                    else
                    {
                        MessageBox.Show("Entry already exist", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                }
                else
                {
                    MessageBox.Show("Select Count Name", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }
                txtBagNo.Text = string.Empty;
                txtWeight.Text = string.Empty;
                txtBagNo.Focus();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        protected void LoadNoBag()
        {
            decimal TotalWeight = 0;
            for (int i = 0; i < DataGridWeight.Rows.Count - 1; i++)
            {
                TotalWeight += Convert.ToDecimal(DataGridWeight.Rows[i].Cells[5].Value);
            }
            txtTotalWeight.Text = TotalWeight.ToString("0.0000");
            txtNoogBags.Text = (DataGridWeight.Rows.Count - 1).ToString();
        }

        protected bool CheckGridValue()
        {
            bool entryFound = false;
            try
            {
                foreach (DataGridViewRow row in DataGridWeight.Rows)
                {
                    object val2 = row.Cells[0].Value;
                    if (val2 != null && val2.ToString() == txtBagNo.Text)
                    {
                        entryFound = true;
                        break;
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            return entryFound;
        }

        private void txtItem_Click(object sender, EventArgs e)
        {
            DataTable dt = getItem();
            bsItem.DataSource = dt;
            FillGrid(dt, 2);
            Point loc = FindLocation(txtItem);
            grSearch.Location = new Point(loc.X, loc.Y + 20);
            grSearch.Visible = true;
            grSearch.Text = "Item Search";
        }

        private Point FindLocation(Control ctrl)
        {
            if (ctrl.Parent is Form)
                return ctrl.Location;
            else
            {
                Point p = FindLocation(ctrl.Parent);
                p.X += ctrl.Location.X;
                p.Y += ctrl.Location.Y;
                return p;
            }
        }

        protected DataTable getItem()
        {
            DataTable dt = new DataTable();
            try
            {
                dt = db.GetDataWithoutParam(CommandType.StoredProcedure, "SP_GetItemNew", conn);
                bsItem.DataSource = dt;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            return dt;
        }

        private void txtMillName_Click(object sender, EventArgs e)
        {
            try
            {
                DataTable dt = db.GetDataWithoutParam(CommandType.StoredProcedure, "SP_GetMills", conn);
                bsMill.DataSource = dt;
                FillGrid(dt, 3);
                Point loc = FindLocation(txtMillName);
                grSearch.Location = new Point(loc.X, loc.Y + 20);
                grSearch.Visible = true;
                grSearch.Text = "Search Mill";
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void DataGridCommon_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            try
            {
                SelectId = 1;
                int Index = DataGridCommon.SelectedCells[0].RowIndex;
                if (Fillid == 2)
                {
                    txtItem.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                    txtItem.Tag = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                }
                else
                {
                    txtMillName.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                    txtMillName.Tag = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                }
                grSearch.Visible = false;
                SelectId = 0;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            try
            {
                SelectId = 1;
                int Index = DataGridCommon.SelectedCells[0].RowIndex;
                if (Fillid == 2)
                {
                    txtItem.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                    txtItem.Tag = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                }
                else
                {
                    txtMillName.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                    txtMillName.Tag = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                }
                grSearch.Visible = false;
                SelectId = 0;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void btnHide_Click(object sender, EventArgs e)
        {
            grSearch.Visible = false;
        }

        private void DataGridCommon_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.F10)
                {
                    grSearch.Visible = false;
                }
                else if (e.KeyCode == Keys.F2)
                {
                    SelectId = 1;
                    int Index = DataGridCommon.SelectedCells[0].RowIndex;
                    if (Fillid == 2)
                    {
                        txtItem.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                        txtItem.Tag = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                    }
                    else
                    {
                        txtMillName.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                        txtMillName.Tag = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                    }
                    grSearch.Visible = false;
                    SelectId = 0;
                }
                else if (e.KeyCode == Keys.Escape)
                {
                    grSearch.Visible = false;
                }
                else
                {

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void btnsave_Click(object sender, EventArgs e)
        {
            try
            {
                if (txtItem.Text != string.Empty)
                {
                    if (btnsave.Text == "Save")
                    {
                        SqlParameter[] para = {
                            new SqlParameter("@Tag","Insert"),
                            new SqlParameter("@DocNo",txtSlipNumber.Text),
                            new SqlParameter("@DocDate",Convert.ToDateTime(dtpSlipDae.Text).ToString("yyyy-MM-dd")),
                            new SqlParameter("@NoOfBags",Convert.ToInt32(txtNoogBags.Text)),
                            new SqlParameter("@TotalWeight",Convert.ToDecimal(txtTotalWeight.Text).ToString("0.0000")),
                            new SqlParameter("@ReturnUid",SqlDbType.Int),
                        };
                        para[5].Direction = ParameterDirection.Output;
                        int Id = db.ExecuteNonQuery(CommandType.StoredProcedure, "SP_WeptIssueM", para, conn, 5);
                        for (int i = 0; i < DataGridWeight.Rows.Count - 1; i++)
                        {
                            SqlParameter[] detPara = { new SqlParameter("@Tag","Insert"),
                                new  SqlParameter("@WeptUId",Id),
                                new SqlParameter("@ItemUid",Convert.ToInt32(DataGridWeight.Rows[i].Cells[6].Value.ToString())),
                                new SqlParameter("@BagNo",DataGridWeight.Rows[i].Cells[2].Value.ToString()),
                                new SqlParameter("@TareWght",DataGridWeight.Rows[i].Cells[3].Value.ToString()),
                                new SqlParameter("@GrossWght",DataGridWeight.Rows[i].Cells[4].Value.ToString()),
                                new SqlParameter("@Weight",Convert.ToDecimal(DataGridWeight.Rows[i].Cells[5].Value.ToString()).ToString("0.0000")),
                                new SqlParameter("@MillUid",Convert.ToInt32(DataGridWeight.Rows[i].Cells[8].Value.ToString())),
                                new SqlParameter("@PackType",DataGridWeight.Rows[i].Cells[9].Value.ToString())
                            };
                            db.ExecuteNonQuery(CommandType.StoredProcedure, "SP_WeptIssueDet", detPara, conn);
                        }
                    }
                    else
                    {
                        SqlParameter[] para = {
                            new SqlParameter("@Tag","Update"),
                            new SqlParameter("@DocNo",txtSlipNumber.Text),
                            new SqlParameter("@DocDate",Convert.ToDateTime(dtpSlipDae.Text).ToString("yyyy-MM-dd")),
                            new SqlParameter("@NoOfBags",Convert.ToInt32(txtNoogBags.Text)),
                            new SqlParameter("@TotalWeight",Convert.ToDecimal(txtTotalWeight.Text).ToString("0.0000")),
                            new SqlParameter("@ReturnUid",SqlDbType.Int),
                            new SqlParameter("@ID",Convert.ToInt32(txtSlipNumber.Tag)),
                        };
                        para[5].Direction = ParameterDirection.Output;
                        int Id = db.ExecuteNonQuery(CommandType.StoredProcedure, "SP_WeptIssueM", para, conn, 5);
                        for (int i = 0; i < DataGridWeight.Rows.Count - 1; i++)
                        {
                            SqlParameter[] detPara = {
                                new SqlParameter("@Tag","Update"),
                                new  SqlParameter("@WeptUId",Id),
                                new SqlParameter("@ItemUid",Convert.ToInt32(DataGridWeight.Rows[i].Cells[6].Value.ToString())),
                                new SqlParameter("@BagNo",DataGridWeight.Rows[i].Cells[2].Value.ToString()),
                                new SqlParameter("@TareWght",DataGridWeight.Rows[i].Cells[3].Value.ToString()),
                                new SqlParameter("@GrossWght",DataGridWeight.Rows[i].Cells[4].Value.ToString()),
                                new SqlParameter("@Weight",Convert.ToDecimal(DataGridWeight.Rows[i].Cells[5].Value.ToString()).ToString("0.0000")),
                                new SqlParameter("@MillUid",Convert.ToInt32(DataGridWeight.Rows[i].Cells[8].Value.ToString())),
                                new  SqlParameter("@WeptDId",Convert.ToInt32(DataGridWeight.Rows[i].Cells[7].Value.ToString())),
                                new SqlParameter("@PackType",DataGridWeight.Rows[i].Cells[9].Value.ToString())
                            };
                            db.ExecuteNonQuery(CommandType.StoredProcedure, "SP_WeptIssueDet", detPara, conn);
                        }
                    }
                    MessageBox.Show("Packing Slip Generated suucessfully", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    ClearControl();
                    GetWeptIssue();
                    LoadButtons(0);
                }
                else
                {
                    MessageBox.Show("Select Party Name and Item Name", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void GetWeptIssue()
        {
            SqlParameter[] para = { new SqlParameter("@Tag", "Select") };
            DataTable dt = db.GetDataWithParam(CommandType.StoredProcedure, "SP_GetWeptIssue", para, conn);
            LoadFrontGrid(dt);
        }

        protected void LoadFrontGrid(DataTable dt)
        {
            try
            {
                DataGridWeptIssue.DataSource = null;
                DataGridWeptIssue.AutoGenerateColumns = false;
                DataGridWeptIssue.ColumnCount = 5;
                DataGridWeptIssue.Columns[0].Name = "PkSId";
                DataGridWeptIssue.Columns[0].HeaderText = "PkSId";
                DataGridWeptIssue.Columns[0].DataPropertyName = "ID";
                DataGridWeptIssue.Columns[0].Visible = false;
                
                DataGridWeptIssue.Columns[1].Name = "DocNo";
                DataGridWeptIssue.Columns[1].HeaderText = "DocNo";
                DataGridWeptIssue.Columns[1].DataPropertyName = "DocNo";
                DataGridWeptIssue.Columns[1].Width = 200;

                DataGridWeptIssue.Columns[2].Name = "DocDate";
                DataGridWeptIssue.Columns[2].HeaderText = "Date";
                DataGridWeptIssue.Columns[2].DataPropertyName = "DocDate";
                DataGridWeptIssue.Columns[2].Width = 200;

                DataGridWeptIssue.Columns[3].Name = "NoOfBags";
                DataGridWeptIssue.Columns[3].HeaderText = "No Of Bags";
                DataGridWeptIssue.Columns[3].DataPropertyName = "NoOfBags";
                DataGridWeptIssue.Columns[3].DefaultCellStyle.Format = "N0";
                DataGridWeptIssue.Columns[3].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                DataGridWeptIssue.Columns[3].Width = 120;

                DataGridWeptIssue.Columns[4].Name = "TotalWeight";
                DataGridWeptIssue.Columns[4].HeaderText = "Total Weight";
                DataGridWeptIssue.Columns[4].DataPropertyName = "TotalWeight";
                DataGridWeptIssue.Columns[4].DefaultCellStyle.Format = "N4";
                DataGridWeptIssue.Columns[4].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                DataGridWeptIssue.Columns[4].Width = 200;
                DataGridWeptIssue.DataSource = dt;
                lblCount.Text = "of " + dt.Rows.Count.ToString();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void txtTarWght_TextChanged(object sender, EventArgs e)
        {
            try
            {
                decimal gross = Convert.ToDecimal(txtGrossWght.Text);
                decimal tare = Convert.ToDecimal(txtTarWght.Text);
                decimal Net = gross - tare;
                txtWeight.Text = Net.ToString("0.000");

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }
    }
}
