﻿namespace Subcontract
{
    partial class FrmStirring
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Frapan = new System.Windows.Forms.Panel();
            this.btnexit = new System.Windows.Forms.Button();
            this.btndelete = new System.Windows.Forms.Button();
            this.btnedit = new System.Windows.Forms.Button();
            this.btnadd = new System.Windows.Forms.Button();
            this.HFGP = new System.Windows.Forms.DataGridView();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.txttype = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtdocdate = new System.Windows.Forms.DateTimePicker();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.txtdocno = new System.Windows.Forms.TextBox();
            this.cboshift = new System.Windows.Forms.ComboBox();
            this.totime = new System.Windows.Forms.DateTimePicker();
            this.Fromtime = new System.Windows.Forms.DateTimePicker();
            this.label8 = new System.Windows.Forms.Label();
            this.txtbondno = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.txtrubcom = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.txtwt = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.txtstisoln = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.txtmirrmas = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.txtwhiterub = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.txtyellow = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.txteva = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.txtactualtot = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.txtfilterloss = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.txttot = new System.Windows.Forms.TextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.txtfiltime = new System.Windows.Forms.TextBox();
            this.label20 = new System.Windows.Forms.Label();
            this.txtafterfil = new System.Windows.Forms.TextBox();
            this.label21 = new System.Windows.Forms.Label();
            this.txtremovedby = new System.Windows.Forms.TextBox();
            this.label22 = new System.Windows.Forms.Label();
            this.txtstigivenby = new System.Windows.Forms.TextBox();
            this.btnsave = new System.Windows.Forms.Button();
            this.btnaddrcan = new System.Windows.Forms.Button();
            this.label23 = new System.Windows.Forms.Label();
            this.txtfilby = new System.Windows.Forms.TextBox();
            this.Frapan.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.HFGP)).BeginInit();
            this.SuspendLayout();
            // 
            // Frapan
            // 
            this.Frapan.Controls.Add(this.btnexit);
            this.Frapan.Controls.Add(this.btndelete);
            this.Frapan.Controls.Add(this.btnedit);
            this.Frapan.Controls.Add(this.btnadd);
            this.Frapan.Controls.Add(this.HFGP);
            this.Frapan.Controls.Add(this.textBox1);
            this.Frapan.Location = new System.Drawing.Point(58, 41);
            this.Frapan.Name = "Frapan";
            this.Frapan.Size = new System.Drawing.Size(865, 489);
            this.Frapan.TabIndex = 84;
            // 
            // btnexit
            // 
            this.btnexit.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnexit.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnexit.Image = global::Subcontract.Properties.Resources.eee;
            this.btnexit.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnexit.Location = new System.Drawing.Point(741, 293);
            this.btnexit.Name = "btnexit";
            this.btnexit.Size = new System.Drawing.Size(90, 39);
            this.btnexit.TabIndex = 86;
            this.btnexit.Text = "EXIT";
            this.btnexit.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnexit.UseVisualStyleBackColor = false;
            // 
            // btndelete
            // 
            this.btndelete.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btndelete.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btndelete.Image = global::Subcontract.Properties.Resources.exit8;
            this.btndelete.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btndelete.Location = new System.Drawing.Point(741, 234);
            this.btndelete.Name = "btndelete";
            this.btndelete.Size = new System.Drawing.Size(90, 38);
            this.btndelete.TabIndex = 85;
            this.btndelete.Text = "DELETE";
            this.btndelete.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btndelete.UseVisualStyleBackColor = false;
            // 
            // btnedit
            // 
            this.btnedit.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnedit.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnedit.Image = global::Subcontract.Properties.Resources.edit;
            this.btnedit.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.btnedit.Location = new System.Drawing.Point(741, 170);
            this.btnedit.Name = "btnedit";
            this.btnedit.Size = new System.Drawing.Size(90, 43);
            this.btnedit.TabIndex = 84;
            this.btnedit.Text = "EDIT";
            this.btnedit.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnedit.UseVisualStyleBackColor = false;
            this.btnedit.Click += new System.EventHandler(this.btnedit_Click);
            // 
            // btnadd
            // 
            this.btnadd.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnadd.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnadd.Image = global::Subcontract.Properties.Resources.Add;
            this.btnadd.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.btnadd.Location = new System.Drawing.Point(741, 109);
            this.btnadd.Name = "btnadd";
            this.btnadd.Size = new System.Drawing.Size(90, 42);
            this.btnadd.TabIndex = 83;
            this.btnadd.Text = "ADD";
            this.btnadd.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnadd.UseVisualStyleBackColor = false;
            this.btnadd.Click += new System.EventHandler(this.btnadd_Click);
            // 
            // HFGP
            // 
            this.HFGP.BackgroundColor = System.Drawing.Color.White;
            this.HFGP.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.HFGP.Location = new System.Drawing.Point(12, 42);
            this.HFGP.Name = "HFGP";
            this.HFGP.Size = new System.Drawing.Size(712, 412);
            this.HFGP.TabIndex = 3;
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(12, 13);
            this.textBox1.Margin = new System.Windows.Forms.Padding(4);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(430, 20);
            this.textBox1.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(21, 22);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(136, 16);
            this.label1.TabIndex = 85;
            this.label1.Text = "STIRRING CHART";
            // 
            // txttype
            // 
            this.txttype.Location = new System.Drawing.Point(211, 136);
            this.txttype.Margin = new System.Windows.Forms.Padding(4);
            this.txttype.Name = "txttype";
            this.txttype.Size = new System.Drawing.Size(114, 20);
            this.txttype.TabIndex = 86;
            this.txttype.TextChanged += new System.EventHandler(this.textBox2_TextChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(74, 59);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(41, 16);
            this.label2.TabIndex = 87;
            this.label2.Text = "Date";
            // 
            // txtdocdate
            // 
            this.txtdocdate.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.txtdocdate.Location = new System.Drawing.Point(211, 58);
            this.txtdocdate.Name = "txtdocdate";
            this.txtdocdate.Size = new System.Drawing.Size(120, 20);
            this.txtdocdate.TabIndex = 88;
            this.txtdocdate.ValueChanged += new System.EventHandler(this.dateTimePicker1_ValueChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(74, 101);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(82, 16);
            this.label3.TabIndex = 89;
            this.label3.Text = "From Time";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(398, 100);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(66, 16);
            this.label4.TabIndex = 90;
            this.label4.Text = "To Time";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(398, 58);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(81, 16);
            this.label5.TabIndex = 91;
            this.label5.Text = "Stirring No";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(74, 137);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(44, 16);
            this.label6.TabIndex = 92;
            this.label6.Text = "Type";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(398, 140);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(38, 16);
            this.label7.TabIndex = 93;
            this.label7.Text = "Shift";
            this.label7.Click += new System.EventHandler(this.label7_Click);
            // 
            // txtdocno
            // 
            this.txtdocno.Location = new System.Drawing.Point(515, 57);
            this.txtdocno.Margin = new System.Windows.Forms.Padding(4);
            this.txtdocno.Name = "txtdocno";
            this.txtdocno.Size = new System.Drawing.Size(129, 20);
            this.txtdocno.TabIndex = 94;
            this.txtdocno.TextChanged += new System.EventHandler(this.txtdocno_TextChanged);
            // 
            // cboshift
            // 
            this.cboshift.FormattingEnabled = true;
            this.cboshift.Items.AddRange(new object[] {
            "I",
            "II",
            "III"});
            this.cboshift.Location = new System.Drawing.Point(520, 135);
            this.cboshift.Name = "cboshift";
            this.cboshift.Size = new System.Drawing.Size(123, 21);
            this.cboshift.TabIndex = 95;
            // 
            // totime
            // 
            this.totime.Format = System.Windows.Forms.DateTimePickerFormat.Time;
            this.totime.Location = new System.Drawing.Point(520, 100);
            this.totime.Name = "totime";
            this.totime.Size = new System.Drawing.Size(126, 20);
            this.totime.TabIndex = 96;
            // 
            // Fromtime
            // 
            this.Fromtime.Format = System.Windows.Forms.DateTimePickerFormat.Time;
            this.Fromtime.Location = new System.Drawing.Point(211, 100);
            this.Fromtime.Name = "Fromtime";
            this.Fromtime.Size = new System.Drawing.Size(117, 20);
            this.Fromtime.TabIndex = 97;
            this.Fromtime.ValueChanged += new System.EventHandler(this.dateTimePicker3_ValueChanged);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(74, 186);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(85, 16);
            this.label8.TabIndex = 99;
            this.label8.Text = "BondingNo";
            // 
            // txtbondno
            // 
            this.txtbondno.Location = new System.Drawing.Point(211, 186);
            this.txtbondno.Margin = new System.Windows.Forms.Padding(4);
            this.txtbondno.Name = "txtbondno";
            this.txtbondno.Size = new System.Drawing.Size(114, 20);
            this.txtbondno.TabIndex = 98;
            this.txtbondno.TextChanged += new System.EventHandler(this.textBox3_TextChanged);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(74, 237);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(137, 16);
            this.label9.TabIndex = 101;
            this.label9.Text = "Rubber Compound";
            // 
            // txtrubcom
            // 
            this.txtrubcom.Location = new System.Drawing.Point(211, 235);
            this.txtrubcom.Margin = new System.Windows.Forms.Padding(4);
            this.txtrubcom.Name = "txtrubcom";
            this.txtrubcom.Size = new System.Drawing.Size(114, 20);
            this.txtrubcom.TabIndex = 100;
            this.txtrubcom.TextChanged += new System.EventHandler(this.textBox4_TextChanged);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(393, 185);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(56, 16);
            this.label10.TabIndex = 94;
            this.label10.Text = "Weight";
            // 
            // txtwt
            // 
            this.txtwt.Location = new System.Drawing.Point(520, 182);
            this.txtwt.Margin = new System.Windows.Forms.Padding(4);
            this.txtwt.Name = "txtwt";
            this.txtwt.Size = new System.Drawing.Size(123, 20);
            this.txtwt.TabIndex = 93;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(393, 236);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(125, 16);
            this.label11.TabIndex = 103;
            this.label11.Text = "Stirring Solutions";
            // 
            // txtstisoln
            // 
            this.txtstisoln.Location = new System.Drawing.Point(520, 236);
            this.txtstisoln.Margin = new System.Windows.Forms.Padding(4);
            this.txtstisoln.Name = "txtstisoln";
            this.txtstisoln.Size = new System.Drawing.Size(123, 20);
            this.txtstisoln.TabIndex = 102;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(74, 289);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(90, 16);
            this.label12.TabIndex = 105;
            this.label12.Text = "Stirring Mek";
            // 
            // txtmirrmas
            // 
            this.txtmirrmas.Location = new System.Drawing.Point(211, 288);
            this.txtmirrmas.Margin = new System.Windows.Forms.Padding(4);
            this.txtmirrmas.Name = "txtmirrmas";
            this.txtmirrmas.Size = new System.Drawing.Size(114, 20);
            this.txtmirrmas.TabIndex = 104;
            this.txtmirrmas.TextChanged += new System.EventHandler(this.textBox7_TextChanged);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(398, 292);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(102, 16);
            this.label13.TabIndex = 107;
            this.label13.Text = "White Rubber";
            // 
            // txtwhiterub
            // 
            this.txtwhiterub.Location = new System.Drawing.Point(520, 285);
            this.txtwhiterub.Margin = new System.Windows.Forms.Padding(4);
            this.txtwhiterub.Name = "txtwhiterub";
            this.txtwhiterub.Size = new System.Drawing.Size(124, 20);
            this.txtwhiterub.TabIndex = 106;
            this.txtwhiterub.TextChanged += new System.EventHandler(this.textBox8_TextChanged);
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(74, 339);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(109, 16);
            this.label14.TabIndex = 109;
            this.label14.Text = "Yellow Rubber";
            // 
            // txtyellow
            // 
            this.txtyellow.Location = new System.Drawing.Point(211, 339);
            this.txtyellow.Margin = new System.Windows.Forms.Padding(4);
            this.txtyellow.Name = "txtyellow";
            this.txtyellow.Size = new System.Drawing.Size(114, 20);
            this.txtyellow.TabIndex = 108;
            this.txtyellow.TextChanged += new System.EventHandler(this.textBox9_TextChanged);
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(83, 425);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(111, 16);
            this.label15.TabIndex = 111;
            this.label15.Text = "Eva.Losses Wt";
            // 
            // txteva
            // 
            this.txteva.Location = new System.Drawing.Point(210, 426);
            this.txteva.Margin = new System.Windows.Forms.Padding(4);
            this.txteva.Name = "txteva";
            this.txteva.Size = new System.Drawing.Size(115, 20);
            this.txteva.TabIndex = 110;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(383, 341);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(108, 16);
            this.label16.TabIndex = 113;
            this.label16.Text = "Actual Soln Wt";
            // 
            // txtactualtot
            // 
            this.txtactualtot.Location = new System.Drawing.Point(520, 339);
            this.txtactualtot.Margin = new System.Windows.Forms.Padding(4);
            this.txtactualtot.Name = "txtactualtot";
            this.txtactualtot.Size = new System.Drawing.Size(123, 20);
            this.txtactualtot.TabIndex = 112;
            this.txtactualtot.TextChanged += new System.EventHandler(this.textBox11_TextChanged);
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.Location = new System.Drawing.Point(83, 469);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(76, 16);
            this.label17.TabIndex = 115;
            this.label17.Text = "Filter loss";
            // 
            // txtfilterloss
            // 
            this.txtfilterloss.Location = new System.Drawing.Point(210, 468);
            this.txtfilterloss.Margin = new System.Windows.Forms.Padding(4);
            this.txtfilterloss.Name = "txtfilterloss";
            this.txtfilterloss.Size = new System.Drawing.Size(115, 20);
            this.txtfilterloss.TabIndex = 114;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.Location = new System.Drawing.Point(74, 384);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(62, 16);
            this.label18.TabIndex = 113;
            this.label18.Text = "Total wt";
            // 
            // txttot
            // 
            this.txttot.Location = new System.Drawing.Point(211, 380);
            this.txttot.Margin = new System.Windows.Forms.Padding(4);
            this.txttot.Name = "txttot";
            this.txttot.Size = new System.Drawing.Size(114, 20);
            this.txttot.TabIndex = 112;
            this.txttot.TextChanged += new System.EventHandler(this.textBox13_TextChanged);
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.Location = new System.Drawing.Point(385, 470);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(121, 16);
            this.label19.TabIndex = 119;
            this.label19.Text = "Filter date & Time";
            // 
            // txtfiltime
            // 
            this.txtfiltime.Location = new System.Drawing.Point(520, 469);
            this.txtfiltime.Margin = new System.Windows.Forms.Padding(4);
            this.txtfiltime.Name = "txtfiltime";
            this.txtfiltime.Size = new System.Drawing.Size(123, 20);
            this.txtfiltime.TabIndex = 118;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.Location = new System.Drawing.Point(385, 426);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(92, 16);
            this.label20.TabIndex = 117;
            this.label20.Text = "After filter wt";
            // 
            // txtafterfil
            // 
            this.txtafterfil.Location = new System.Drawing.Point(520, 427);
            this.txtafterfil.Margin = new System.Windows.Forms.Padding(4);
            this.txtafterfil.Name = "txtafterfil";
            this.txtafterfil.Size = new System.Drawing.Size(123, 20);
            this.txtafterfil.TabIndex = 116;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.Location = new System.Drawing.Point(385, 511);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(97, 16);
            this.label21.TabIndex = 123;
            this.label21.Text = "Removed By";
            // 
            // txtremovedby
            // 
            this.txtremovedby.Location = new System.Drawing.Point(520, 510);
            this.txtremovedby.Margin = new System.Windows.Forms.Padding(4);
            this.txtremovedby.Name = "txtremovedby";
            this.txtremovedby.Size = new System.Drawing.Size(123, 20);
            this.txtremovedby.TabIndex = 122;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.Location = new System.Drawing.Point(83, 510);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(125, 16);
            this.label22.TabIndex = 121;
            this.label22.Text = "Stirring Given BY";
            // 
            // txtstigivenby
            // 
            this.txtstigivenby.Location = new System.Drawing.Point(210, 509);
            this.txtstigivenby.Margin = new System.Windows.Forms.Padding(4);
            this.txtstigivenby.Name = "txtstigivenby";
            this.txtstigivenby.Size = new System.Drawing.Size(115, 20);
            this.txtstigivenby.TabIndex = 120;
            // 
            // btnsave
            // 
            this.btnsave.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnsave.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnsave.Image = global::Subcontract.Properties.Resources.save;
            this.btnsave.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnsave.Location = new System.Drawing.Point(714, 185);
            this.btnsave.Name = "btnsave";
            this.btnsave.Size = new System.Drawing.Size(90, 38);
            this.btnsave.TabIndex = 125;
            this.btnsave.Text = "Save";
            this.btnsave.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnsave.UseVisualStyleBackColor = false;
            this.btnsave.Click += new System.EventHandler(this.btnsave_Click);
            // 
            // btnaddrcan
            // 
            this.btnaddrcan.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnaddrcan.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnaddrcan.Image = global::Subcontract.Properties.Resources.cancel;
            this.btnaddrcan.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnaddrcan.Location = new System.Drawing.Point(711, 247);
            this.btnaddrcan.Name = "btnaddrcan";
            this.btnaddrcan.Size = new System.Drawing.Size(93, 38);
            this.btnaddrcan.TabIndex = 124;
            this.btnaddrcan.Text = "Back";
            this.btnaddrcan.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnaddrcan.UseVisualStyleBackColor = false;
            this.btnaddrcan.Click += new System.EventHandler(this.btnaddrcan_Click);
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.Location = new System.Drawing.Point(654, 512);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(85, 16);
            this.label23.TabIndex = 127;
            this.label23.Text = "Filtered BY";
            // 
            // txtfilby
            // 
            this.txtfilby.Location = new System.Drawing.Point(789, 511);
            this.txtfilby.Margin = new System.Windows.Forms.Padding(4);
            this.txtfilby.Name = "txtfilby";
            this.txtfilby.Size = new System.Drawing.Size(123, 20);
            this.txtfilby.TabIndex = 126;
            // 
            // FrmStirring
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(912, 557);
            this.Controls.Add(this.label23);
            this.Controls.Add(this.txtfilby);
            this.Controls.Add(this.Frapan);
            this.Controls.Add(this.btnsave);
            this.Controls.Add(this.btnaddrcan);
            this.Controls.Add(this.label21);
            this.Controls.Add(this.txtremovedby);
            this.Controls.Add(this.label22);
            this.Controls.Add(this.txtstigivenby);
            this.Controls.Add(this.label19);
            this.Controls.Add(this.txtfiltime);
            this.Controls.Add(this.label20);
            this.Controls.Add(this.txtafterfil);
            this.Controls.Add(this.label18);
            this.Controls.Add(this.label17);
            this.Controls.Add(this.txttot);
            this.Controls.Add(this.txtfilterloss);
            this.Controls.Add(this.label16);
            this.Controls.Add(this.txtactualtot);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.txteva);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.txtyellow);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.txtwhiterub);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.txtmirrmas);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.txtstisoln);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.txtwt);
            this.Controls.Add(this.txtrubcom);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.txtbondno);
            this.Controls.Add(this.Fromtime);
            this.Controls.Add(this.totime);
            this.Controls.Add(this.cboshift);
            this.Controls.Add(this.txtdocno);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.txtdocdate);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txttype);
            this.Controls.Add(this.label1);
            this.Name = "FrmStirring";
            this.Text = "FrmStirring";
            this.Load += new System.EventHandler(this.FrmStirring_Load);
            this.Frapan.ResumeLayout(false);
            this.Frapan.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.HFGP)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel Frapan;
        private System.Windows.Forms.Button btnexit;
        private System.Windows.Forms.Button btndelete;
        private System.Windows.Forms.Button btnedit;
        private System.Windows.Forms.Button btnadd;
        private System.Windows.Forms.DataGridView HFGP;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txttype;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.DateTimePicker txtdocdate;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtdocno;
        private System.Windows.Forms.ComboBox cboshift;
        private System.Windows.Forms.DateTimePicker totime;
        private System.Windows.Forms.DateTimePicker Fromtime;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txtbondno;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox txtrubcom;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox txtwt;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox txtstisoln;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox txtmirrmas;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox txtwhiterub;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox txtyellow;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox txteva;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.TextBox txtactualtot;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox txtfilterloss;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.TextBox txttot;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.TextBox txtfiltime;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.TextBox txtafterfil;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.TextBox txtremovedby;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.TextBox txtstigivenby;
        private System.Windows.Forms.Button btnsave;
        private System.Windows.Forms.Button btnaddrcan;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.TextBox txtfilby;
    }
}