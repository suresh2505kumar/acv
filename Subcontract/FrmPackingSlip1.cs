﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace ACV
{
    public partial class FrmPackingSli_ : Form
    {
        public FrmPackingSli_()
        {
            InitializeComponent();
        }
        SqlConnection conn = new SqlConnection("Data Source=" + Genclass.data2 + "; Initial Catalog=" + Genclass.data5 + ";User id=" + Genclass.data3 + ";Password=" + Genclass.data4 + "");
        SQLDBHelper db = new SQLDBHelper();
        int Fillid;
        BindingSource bsParty = new BindingSource();
        BindingSource bsItem = new BindingSource();
        BindingSource bsMill = new BindingSource();
        public int SelectId = 0;
        private void FrmPackingSli__Load(object sender, EventArgs e)
        {
            Genclass.Module.buttonstyleform(this);
            Genclass.Module.buttonstylepanel(panadd);
            LoadWeightrGrid();
            GetPackingSlip();
            LoadButtons(0);
        }
        protected void LoadWeightrGrid()
        {
            try
            {
                DataGridWeight.DataSource = null;
                DataGridWeight.AutoGenerateColumns = false;
                DataGridWeight.ColumnCount = 6;
                DataGridWeight.Columns[0].Name = "Bag No";
                DataGridWeight.Columns[0].HeaderText = "Bag No";
                DataGridWeight.Columns[0].Width = 150;
                DataGridWeight.Columns[1].Name = "Tare Wght";
                DataGridWeight.Columns[1].HeaderText = "Tare Wght";
                DataGridWeight.Columns[1].Width = 80;
                DataGridWeight.Columns[2].Name = "Gross Wght";
                DataGridWeight.Columns[2].HeaderText = "Gross Wghto";
                DataGridWeight.Columns[2].Width = 80;
                DataGridWeight.Columns[3].Name = "NetWeight";
                DataGridWeight.Columns[3].HeaderText = "NetWeight";
                DataGridWeight.Columns[3].DefaultCellStyle.Format = "N4";
                DataGridWeight.Columns[3].Width = 80;
                DataGridWeight.Columns[4].Name = "ItemUid";
                DataGridWeight.Columns[4].HeaderText = "ItemUid";
                DataGridWeight.Columns[4].Visible = false;
                DataGridWeight.Columns[5].Name = "PkDUid";
                DataGridWeight.Columns[5].HeaderText = "PkDUid";
                DataGridWeight.Columns[5].Visible = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Inforamtion", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }
        protected void LoadButtons(int id)
        {
            try
            {
                if (id == 0)
                {
                    grPkBack.Visible = false;
                    grPkFront.Visible = true;
                    btnAdd.Visible = true;
                    btnEdit.Visible = true;
                    btnDelete.Visible = true;
                    btnExit.Visible = true;
                    btnPrint.Visible = true;
                    chckActive.Visible = true;
                    btnFstBack.Visible = true;
                    btnBack.Visible = true;
                    btnNext.Visible = true;
                    btnFstNext.Visible = true;
                    panNumbers.Visible = true;
                    btnsave.Visible = false;
                    btnaddrcan.Visible = false;
                    btnsave.Text = "Save";
                }
                else if (id == 1)
                {
                    grPkBack.Visible = true;
                    grPkFront.Visible = false;
                    btnAdd.Visible = false;
                    btnEdit.Visible = false;
                    btnDelete.Visible = false;
                    btnExit.Visible = false;
                    btnPrint.Visible = false;
                    chckActive.Visible = false;
                    btnFstBack.Visible = false;
                    btnBack.Visible = false;
                    btnNext.Visible = false;
                    btnFstNext.Visible = false;
                    panNumbers.Visible = false;
                    btnsave.Visible = true;
                    btnaddrcan.Visible = true;
                    btnsave.Text = "Save";
                }

                else if (id == 2)
                {
                    grPkBack.Visible = true;
                    grPkFront.Visible = false;
                    btnAdd.Visible = false;
                    btnEdit.Visible = false;
                    btnDelete.Visible = false;
                    btnExit.Visible = false;
                    btnPrint.Visible = false;
                    chckActive.Visible = false;
                    btnFstBack.Visible = false;
                    btnBack.Visible = false;
                    btnNext.Visible = false;
                    btnFstNext.Visible = false;
                    panNumbers.Visible = false;
                    btnsave.Visible = true;
                    btnaddrcan.Visible = true;
                    btnsave.Text = "Update";
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            try
            {
                ClearControl();
                LoadButtons(1);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Inforamtion", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void btnaddrcan_Click(object sender, EventArgs e)
        {
            LoadButtons(0);
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void txtPartyName_MouseClick(object sender, MouseEventArgs e)
        {
            DataTable dt = getParty();
            bsParty.DataSource = dt;
            FillGrid(dt, 1);
            Point loc = FindLocation(txtPartyName);
            grSearch.Location = new Point(loc.X, loc.Y + 20);
            grSearch.Visible = true;
            grSearch.Text = "Party Search";
        }
        private Point FindLocation(Control ctrl)
        {
            if (ctrl.Parent is Form)
                return ctrl.Location;
            else
            {
                Point p = FindLocation(ctrl.Parent);
                p.X += ctrl.Location.X;
                p.Y += ctrl.Location.Y;
                return p;
            }
        }

        private void txtItem_MouseClick(object sender, MouseEventArgs e)
        {
            DataTable dt = getItem();
            bsItem.DataSource = dt;
            FillGrid(dt, 2);
            Point loc = FindLocation(txtItem);
            grSearch.Location = new Point(loc.X, loc.Y + 20);
            grSearch.Visible = true;
            grSearch.Text = "Item Search";
        }
        protected DataTable getParty()
        {
            DataTable dt = new DataTable();
            try
            {
                dt = db.GetDataWithoutParam(CommandType.StoredProcedure, "SP_getParty", conn);
                bsParty.DataSource = dt;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            return dt;
        }
        protected DataTable getItem()
        {
            DataTable dt = new DataTable();
            try
            {
                dt = db.GetDataWithoutParam(CommandType.StoredProcedure, "SP_GetItem", conn);
                bsItem.DataSource = dt;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            return dt;
        }
        protected void FillGrid(DataTable dt, int FillId)
        {
            try
            {
                DataGridCommon.DataSource = null;
                DataGridCommon.AutoGenerateColumns = false;
                if (FillId == 1)
                {
                    Fillid = 1;
                    DataGridCommon.ColumnCount = 3;
                    DataGridCommon.Columns[0].Name = "Uid";
                    DataGridCommon.Columns[0].HeaderText = "Uid";
                    DataGridCommon.Columns[0].DataPropertyName = "Uid";
                    DataGridCommon.Columns[1].Name = "Name";
                    DataGridCommon.Columns[1].HeaderText = "Name";
                    DataGridCommon.Columns[1].DataPropertyName = "Name";

                    DataGridCommon.Columns[2].Name = "Code";
                    DataGridCommon.Columns[2].HeaderText = "Code";
                    DataGridCommon.Columns[2].DataPropertyName = "Code";
                    DataGridCommon.Columns[1].Width = 280;
                    DataGridCommon.DataSource = bsParty;
                    DataGridCommon.Columns[0].Visible = false;
                }
                else if (FillId == 2)
                {
                    Fillid = 2;
                    DataGridCommon.ColumnCount = 3;
                    DataGridCommon.Columns[0].Name = "Uid";
                    DataGridCommon.Columns[0].HeaderText = "Uid";
                    DataGridCommon.Columns[0].DataPropertyName = "Uid";
                    DataGridCommon.Columns[1].Name = "Name";
                    DataGridCommon.Columns[1].HeaderText = "Name";
                    DataGridCommon.Columns[1].DataPropertyName = "ItemName";
                    DataGridCommon.Columns[2].Name = "Code";
                    DataGridCommon.Columns[2].HeaderText = "Code";
                    DataGridCommon.Columns[2].DataPropertyName = "ItemCode";
                    DataGridCommon.Columns[1].Width = 260;
                    DataGridCommon.DataSource = bsItem;
                    DataGridCommon.Columns[0].Visible = false;
                }
                else
                {
                    Fillid = 3;
                    DataGridCommon.ColumnCount = 3;
                    DataGridCommon.Columns[0].Name = "Uid";
                    DataGridCommon.Columns[0].HeaderText = "Uid";
                    DataGridCommon.Columns[0].DataPropertyName = "Uid";
                    DataGridCommon.Columns[1].Name = "Name";
                    DataGridCommon.Columns[1].HeaderText = "Name";
                    DataGridCommon.Columns[1].DataPropertyName = "Name";

                    DataGridCommon.Columns[2].Name = "Code";
                    DataGridCommon.Columns[2].HeaderText = "Code";
                    DataGridCommon.Columns[2].DataPropertyName = "Code";
                    DataGridCommon.Columns[1].Width = 260;
                    DataGridCommon.DataSource = bsMill;
                    DataGridCommon.Columns[0].Visible = false;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void txtSearch_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
            {
                grSearch.Visible = false;
            }
        }

        private void btnHide_Click(object sender, EventArgs e)
        {
            grSearch.Visible = false;
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            try
            {
                if (txtPartyName.Text != string.Empty && txtItem.Text != string.Empty && txtBagNo.Text != string.Empty && txtWeight.Text != string.Empty)
                {
                    bool checkBagNo = CheckGridValue();
                    if (checkBagNo == false)
                    {
                        DataGridViewRow row = (DataGridViewRow)DataGridWeight.Rows[0].Clone();
                        row.Cells[0].Value = txtBagNo.Text;
                        row.Cells[1].Value = txtTarWght.Text;
                        row.Cells[2].Value = txtGrossWght.Text;
                        row.Cells[3].Value = txtWeight.Text;
                        row.Cells[4].Value = txtItem.Tag;
                        row.Cells[5].Value = 0;
                        DataGridWeight.Rows.Add(row);
                        LoadNoBag();
                    }
                    else
                    {
                        MessageBox.Show("Entry already exist", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                }
                else
                {
                    MessageBox.Show("Select Party Name and Item Name", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    txtPartyName.Focus();
                    return;
                }
                txtBagNo.Text = string.Empty;
                txtWeight.Text = string.Empty;
                txtBagNo.Focus();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }
        protected void LoadNoBag()
        {
            decimal TotalWeight = 0;
            for (int i = 0; i < DataGridWeight.Rows.Count - 1; i++)
            {
                TotalWeight += Convert.ToDecimal(DataGridWeight.Rows[i].Cells[3].Value);
            }
            txtTotalWeight.Text = TotalWeight.ToString("0.0000");
            txtNoogBags.Text = (DataGridWeight.Rows.Count - 1).ToString();
        }

        private void DataGridCommon_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            try
            {
                SelectId = 1;
                int Index = DataGridCommon.SelectedCells[0].RowIndex;
                if (Fillid == 1)
                {
                    txtPartyName.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                    txtPartyName.Tag = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                }
                else if (Fillid == 2)
                {
                    txtItem.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                    txtItem.Tag = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                }
                else
                {
                    txtMillName.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                    txtMillName.Tag = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                }
                grSearch.Visible = false;
                SelectId = 0;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }
        protected void GetPackingSlip()
        {
            SqlParameter[] para = { new SqlParameter("@Tag", "Select") };
            DataTable dt = db.GetDataWithParam(CommandType.StoredProcedure, "SP_GetPackingSlip", para, conn);
            LoadFrontGrid(dt);
        }

        protected bool CheckGridValue()
        {
            bool entryFound = false;
            try
            {
                foreach (DataGridViewRow row in DataGridWeight.Rows)
                {
                    object val2 = row.Cells[0].Value;
                    if (val2 != null && val2.ToString() == txtBagNo.Text)
                    {
                        entryFound = true;
                        break;
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            return entryFound;
        }

        private void btnsave_Click(object sender, EventArgs e)
        {
            try
            {
                if (txtSlipNumber.Text != string.Empty && txtPartyName.Text != string.Empty && txtItem.Text != string.Empty)
                {
                    if (btnsave.Text == "Save")
                    {
                        SqlParameter[] para = { new SqlParameter("@Tag","Insert"),
                        new  SqlParameter("@PartyUid",txtPartyName.Tag),
                        new SqlParameter("@PkSlipNo",txtSlipNumber.Text),
                        new SqlParameter("@NoOfBags",Convert.ToInt32(txtNoogBags.Text)),
                        new SqlParameter("@PkSlipDtae",Convert.ToDateTime(dtpSlipDae.Text).ToString("yyyy-MM-dd")),
                        new SqlParameter("@TotalWeight",Convert.ToDecimal(txtTotalWeight.Text).ToString("0.0000")),
                        new SqlParameter("@ItemUid",Convert.ToInt32(txtItem.Tag)),
                        new SqlParameter("@ReturnUid",SqlDbType.Int),
                        new SqlParameter("@MillUid",Convert.ToInt32(txtMillName.Tag))
                        };
                        para[7].Direction = ParameterDirection.Output;
                        int Id = db.ExecuteNonQuery(CommandType.StoredProcedure, "SP_PackingSlipM", para, conn, 7);
                        for (int i = 0; i < DataGridWeight.Rows.Count - 1; i++)
                        {
                            SqlParameter[] detPara = { new SqlParameter("@Tag","Insert"),
                            new  SqlParameter("@PkslipUid",Id),
                            new SqlParameter("@ItemUid",Convert.ToInt32(txtItem.Tag)),
                            new SqlParameter("@BagNo",DataGridWeight.Rows[i].Cells[0].Value.ToString()),
                            new SqlParameter("@TareWght",DataGridWeight.Rows[i].Cells[1].Value.ToString()),
                            new SqlParameter("@GrossWght",DataGridWeight.Rows[i].Cells[2].Value.ToString()),
                            new SqlParameter("@Weight",Convert.ToDecimal(DataGridWeight.Rows[i].Cells[3].Value.ToString()).ToString("0.0000"))
                            };
                            db.ExecuteNonQuery(CommandType.StoredProcedure, "SP_PackingSlipdet", detPara, conn);
                        }
                    }
                    else
                    {
                        SqlParameter[] para = { new SqlParameter("@Tag","Update"),
                            new  SqlParameter("@PartyUid",txtPartyName.Tag),
                            new SqlParameter("@PkSlipNo",txtSlipNumber.Text),
                            new SqlParameter("@NoOfBags",Convert.ToInt32(txtNoogBags.Text)),
                            new SqlParameter("@PkSlipDtae",Convert.ToDateTime(dtpSlipDae.Text).ToString("yyyy-MM-dd")),
                            new SqlParameter("@TotalWeight",Convert.ToDecimal(txtTotalWeight.Text).ToString("0.0000")),
                            new SqlParameter("@ItemUid",Convert.ToInt32(txtItem.Tag)),
                            new SqlParameter("@ReturnUid",SqlDbType.Int),
                            new SqlParameter("@PkSId",Convert.ToInt32(txtSlipNumber.Tag)),
                            new SqlParameter("@MillUid",Convert.ToInt32(txtMillName.Tag))
                        };
                        para[7].Direction = ParameterDirection.Output;
                        int Id = db.ExecuteNonQuery(CommandType.StoredProcedure, "SP_PackingSlipM", para, conn, 7);
                        for (int i = 0; i < DataGridWeight.Rows.Count - 1; i++)
                        {
                            SqlParameter[] detPara = {
                                new SqlParameter("@Tag","Update"),
                                new SqlParameter("@PkslipUid",Id),
                                new SqlParameter("@ItemUid",Convert.ToInt32(txtItem.Tag)),
                                new SqlParameter("@BagNo",DataGridWeight.Rows[i].Cells[0].Value.ToString()),
                                new SqlParameter("@TareWght",DataGridWeight.Rows[i].Cells[1].Value.ToString()),
                                new SqlParameter("@GrossWght",DataGridWeight.Rows[i].Cells[2].Value.ToString()),
                                new SqlParameter("@Weight",Convert.ToDecimal(DataGridWeight.Rows[i].Cells[3].Value.ToString()).ToString("0.0000")),
                                new  SqlParameter("@PkSlipDUid",Convert.ToInt32(DataGridWeight.Rows[i].Cells[3].Value.ToString()))
                            };
                            db.ExecuteNonQuery(CommandType.StoredProcedure, "SP_PackingSlipdet", detPara, conn);
                        }
                    }

                    MessageBox.Show("Packing Slip Generated suucessfully", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    ClearControl();
                    GetPackingSlip();
                    LoadButtons(0);
                }
                else
                {
                    MessageBox.Show("Select Party Name and Item Name", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void DataGridWeight_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                LoadNoBag();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }
        protected void ClearControl()
        {
            try
            {
                txtSlipNumber.Text = string.Empty;
                txtPartyName.Text = string.Empty;
                txtPartyName.Tag = string.Empty;
                txtItem.Text = string.Empty;
                txtItem.Tag = string.Empty;
                txtBagNo.Text = string.Empty;
                txtWeight.Text = string.Empty;
                txtNoogBags.Text = string.Empty;
                txtTotalWeight.Text = string.Empty;
                DataGridWeight.Rows.Clear();
                LoadWeightrGrid();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }
        protected void LoadFrontGrid(DataTable dt)
        {
            try
            {
                DataGridPackingSlip.DataSource = null;
                DataGridPackingSlip.AutoGenerateColumns = false;
                DataGridPackingSlip.ColumnCount = 7;
                DataGridPackingSlip.Columns[0].Name = "PkSId";
                DataGridPackingSlip.Columns[0].HeaderText = "PkSId";
                DataGridPackingSlip.Columns[0].DataPropertyName = "PkSId";
                DataGridPackingSlip.Columns[0].Visible = false;

                DataGridPackingSlip.Columns[1].Name = "PkSlipNo";
                DataGridPackingSlip.Columns[1].HeaderText = "Slip Number";
                DataGridPackingSlip.Columns[1].DataPropertyName = "PkSlipNo";
                DataGridPackingSlip.Columns[1].Width = 150;
                DataGridPackingSlip.Columns[2].Name = "PkSlipNo";
                DataGridPackingSlip.Columns[2].HeaderText = "Slip Date";
                DataGridPackingSlip.Columns[2].DataPropertyName = "PkSlipDate";
                DataGridPackingSlip.Columns[2].Width = 150;
                DataGridPackingSlip.Columns[3].Name = "Party Name";
                DataGridPackingSlip.Columns[3].HeaderText = "Party Name";
                DataGridPackingSlip.Columns[3].DataPropertyName = "Name";
                DataGridPackingSlip.Columns[3].Width = 200;
                DataGridPackingSlip.Columns[4].Name = "NoOfBags";
                DataGridPackingSlip.Columns[4].HeaderText = "No Of Bags";
                DataGridPackingSlip.Columns[4].DataPropertyName = "NoOfBags";
                DataGridPackingSlip.Columns[4].DefaultCellStyle.Format = "N0";
                DataGridPackingSlip.Columns[4].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                DataGridPackingSlip.Columns[5].Name = "TotalWeight";
                DataGridPackingSlip.Columns[5].HeaderText = "Total Weight";
                DataGridPackingSlip.Columns[5].DataPropertyName = "TotalWeight";
                DataGridPackingSlip.Columns[5].DefaultCellStyle.Format = "N4";
                DataGridPackingSlip.Columns[5].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                DataGridPackingSlip.Columns[6].Name = "PartyUid";
                DataGridPackingSlip.Columns[6].HeaderText = "PartyUid";
                DataGridPackingSlip.Columns[6].DataPropertyName = "PartyUid";
                DataGridPackingSlip.Columns[6].Visible = false;
                DataGridPackingSlip.DataSource = dt;
                lblCount.Text = "of " + dt.Rows.Count.ToString();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            try
            {
                ClearControl();
                int Index = DataGridPackingSlip.SelectedCells[0].RowIndex;
                int EditId = (int)DataGridPackingSlip.Rows[Index].Cells[0].Value;
                SqlParameter[] para = { new SqlParameter("@Tag", "SelectById"), new SqlParameter("@PkSid", EditId) };
                DataSet ds = new DataSet();
                ds = db.GetDataWithParamMultiple(CommandType.StoredProcedure, "SP_GetPackingSlip", para, conn);
                DataTable dtM = ds.Tables[0];
                DataTable dtD = ds.Tables[1];
                txtSlipNumber.Text = dtM.Rows[0]["PkSlipNo"].ToString();
                txtPartyName.Text = dtM.Rows[0]["Name"].ToString();
                txtPartyName.Tag = dtM.Rows[0]["PartyUid"].ToString();
                txtItem.Text = dtM.Rows[0]["ItemName"].ToString();
                txtItem.Tag = dtM.Rows[0]["ItemUid"].ToString();
                txtNoogBags.Text = dtM.Rows[0]["NoOfBags"].ToString();
                txtTotalWeight.Text = dtM.Rows[0]["TotalWeight"].ToString();
                dtpSlipDae.Text = dtM.Rows[0]["PkSlipdate"].ToString();
                txtSlipNumber.Tag = dtM.Rows[0]["PkSId"].ToString();
                txtMillName.Text = dtM.Rows[0]["MillName"].ToString();
                txtMillName.Tag = dtM.Rows[0]["MillUid"].ToString();
                DataGridWeight.DataSource = null;
                LoadWeightrGrid();
                for (int i = 0; i < dtD.Rows.Count; i++)
                {
                    DataGridViewRow row = (DataGridViewRow)DataGridWeight.Rows[0].Clone();
                    row.Cells[0].Value = dtD.Rows[i]["BagNo"].ToString();
                    row.Cells[1].Value = dtD.Rows[i]["TareWght"].ToString();
                    row.Cells[2].Value = dtD.Rows[i]["GrossWght"].ToString();
                    row.Cells[3].Value = dtD.Rows[i]["Weight"].ToString();
                    row.Cells[4].Value = dtD.Rows[i]["ItemUid"].ToString();
                    row.Cells[5].Value = dtD.Rows[i]["PkSlipDUid"].ToString();
                    DataGridWeight.Rows.Add(row);
                }
                LoadNoBag();
                LoadButtons(2);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void txtWeight_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                btnOk_Click(sender, e);
            }
        }

        private void DataGridPackingSlip_CellMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            try
            {
                int i = DataGridPackingSlip.CurrentCell.RowIndex;
                lblFrom.Text = (i + 1).ToString();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void btnBack_Click(object sender, EventArgs e)
        {
            try
            {
                int Cnt = DataGridPackingSlip.CurrentCell.RowIndex;
                if (Cnt > 0)
                {
                    int i = DataGridPackingSlip.CurrentCell.RowIndex - 1;
                    DataGridPackingSlip.CurrentCell = DataGridPackingSlip.Rows[i].Cells[1];
                    DataGridPackingSlip.Rows[i].Selected = true;
                    lblFrom.Text = (i + 1).ToString();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void btnNext_Click(object sender, EventArgs e)
        {
            try
            {
                int Cnt = DataGridPackingSlip.CurrentCell.RowIndex + 1;
                if (Cnt >= 0 && Cnt != DataGridPackingSlip.Rows.Count)
                {
                    int i = DataGridPackingSlip.CurrentCell.RowIndex + 1;
                    DataGridPackingSlip.CurrentCell = DataGridPackingSlip.Rows[i].Cells[1];
                    DataGridPackingSlip.Rows[i].Selected = true;
                    lblFrom.Text = (i + 1).ToString();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void btnFstNext_Click(object sender, EventArgs e)
        {
            try
            {
                int i = DataGridPackingSlip.Rows.Count - 1;
                DataGridPackingSlip.CurrentCell = DataGridPackingSlip.Rows[i].Cells[1];
                DataGridPackingSlip.Rows[i].Selected = true;
                lblFrom.Text = (i + 1).ToString();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void btnFstBack_Click(object sender, EventArgs e)
        {
            try
            {
                DataGridPackingSlip.CurrentCell = DataGridPackingSlip.Rows[0].Cells[1];
                DataGridPackingSlip.Rows[0].Selected = true;
                int i = DataGridPackingSlip.CurrentCell.RowIndex;
                lblFrom.Text = (i + 1).ToString();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void btnPrint_Click(object sender, EventArgs e)
        {
            try
            {

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void txtMillName_MouseClick(object sender, MouseEventArgs e)
        {
            try
            {
                DataTable dt = db.GetDataWithoutParam(CommandType.StoredProcedure, "SP_GetMills", conn);
                bsMill.DataSource = dt;
                FillGrid(dt, 3);
                Point loc = FindLocation(txtMillName);
                grSearch.Location = new Point(loc.X, loc.Y + 20);
                grSearch.Visible = true;
                grSearch.Text = "Search Mill";
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void txtPartyName_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (SelectId == 0)
                {
                    bsParty.Filter = string.Format("Name LIKE '%{0}%' ", txtPartyName.Text);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }

        }

        private void txtItem_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (SelectId == 0)
                {
                    bsItem.Filter = string.Format("ItemName LIKE '%{0}%' ", txtItem.Text);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void txtMillName_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (SelectId == 0)
                {
                    bsItem.Filter = string.Format("Name LIKE '%{0}%' ", txtMillName.Text);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }

        }

        private void btnSelect_Click(object sender, EventArgs e)
        {
            try
            {
                SelectId = 1;
                int Index = DataGridCommon.SelectedCells[0].RowIndex;
                if (Fillid == 1)
                {
                    txtPartyName.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                    txtPartyName.Tag = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                }
                else if (Fillid == 2)
                {
                    txtItem.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                    txtItem.Tag = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                }
                else
                {
                    txtMillName.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                    txtMillName.Tag = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                }
                grSearch.Visible = false;
                SelectId = 0;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }

        }

        private void txtPartyName_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.F10)
                {
                    grSearch.Visible = false;
                }
                else if (e.KeyCode == Keys.F2)
                {
                    SelectId = 1;
                    int Index = DataGridCommon.SelectedCells[0].RowIndex;
                    if (Fillid == 1)
                    {
                        txtPartyName.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                        txtPartyName.Tag = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                    }
                    else if (Fillid == 2)
                    {
                        txtItem.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                        txtItem.Tag = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                    }
                    else
                    {
                        txtMillName.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                        txtMillName.Tag = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                    }
                    grSearch.Visible = false;
                    SelectId = 0;
                }
                else if (e.KeyCode == Keys.Escape)
                {
                    grSearch.Visible = false;
                }
                else
                {

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void DataGridCommon_KeyDown(object sender, KeyEventArgs e)
        {
            txtPartyName_KeyDown(sender, e);
        }

        private void grPkBack_Enter(object sender, EventArgs e)
        {

        }

        private void chckNofBags_CheckedChanged(object sender, EventArgs e)
        {
            if (chckActive.Checked == true)
            {
                panBulkEntry.Visible = true;
            }
            else
            {
                panBulkEntry.Visible = false;
            }
        }

        private void btnLoad_Click(object sender, EventArgs e)
        {
            try
            {
                int Id = Convert.ToInt32(txtFromBagNo.Text);
                int Count = Convert.ToInt32(txtBNofBags.Text);
                for (int i = 1; i < Count; i++)
                {
                    if (i == 1)
                    {
                        DataGridViewRow row = (DataGridViewRow)DataGridWeight.Rows[0].Clone();
                        row.Cells[0].Value = txtBagNo.Text;
                        row.Cells[1].Value = txtWeight.Text;
                        row.Cells[2].Value = txtTarWght.Text;
                        row.Cells[3].Value = txtGrossWght.Text;
                        row.Cells[4].Value = txtItem.Tag;
                        row.Cells[5].Value = 0;
                        DataGridWeight.Rows.Add(row);
                        LoadNoBag();
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void DataGridWeight_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Delete)
                {
                    DialogResult res = MessageBox.Show("Do you want delete the bagno", "Information", MessageBoxButtons.YesNo, MessageBoxIcon.Information);
                    if (res == DialogResult.Yes)
                    {
                        if (DataGridWeight.SelectedRows.Count > 0)
                        {
                            DataGridWeight.Rows.RemoveAt(DataGridWeight.CurrentRow.Index);
                            DataGridWeight.ClearSelection();
                        }
                    }
                }
                LoadNoBag();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }
    }
}