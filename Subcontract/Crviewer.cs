﻿using System;
using System.Data;
using System.Windows.Forms;
using System.Data.SqlClient;
using CrystalDecisions.CrystalReports.Engine;

namespace ACV
{
    public partial class Crviewer : Form
    {
        private CrystalDecisions.Windows.Forms.CrystalReportViewer Cryview;
        private System.ComponentModel.Container components = null;
        ReportDocument doc = new ReportDocument();
        public Crviewer()
        {
            InitializeComponent();
        }
        SqlConnection conn = new SqlConnection("Data Source=" + Genclass.data2 + "; Initial Catalog=" + Genclass.data5 + ";User id=" + Genclass.data3 + ";Password=" + Genclass.data4 + "");
        SqlCommand qur = new SqlCommand();
        SQLDBHelper db = new SQLDBHelper();
        private void createReport()
        {

        }

        private void Cryview_Load(object sender, EventArgs e)
        {
            conn.Open();
            if (Genclass.Dtype == 40)
            {
                Genclass.strsql = "select * from Vw_salprtexlfin   where muid=" + Genclass.Prtid + " and Companyid=" + Genclass.data1 + "";
                Genclass.cmd = new SqlCommand(Genclass.strsql, conn);
                SqlDataAdapter aptr3 = new SqlDataAdapter(Genclass.cmd);
                DataTable tap3 = new DataTable();
                aptr3.Fill(tap3);
                if (tap3.Rows.Count > 0)
                {
                    if (tap3.Rows[0]["bstate"].ToString() == "Tamil Nadu 33")
                    {
                        if (tap3.Rows[0]["tag"].ToString() == "0")
                        {
                            if (Genclass.data1 == 3 && Convert.ToInt32(tap3.Rows[0]["partyuid"].ToString()) == 1367)
                            {
                                Genclass.strsql = "Exec sp_pdffincsven  " + Genclass.Prtid + " ," + Genclass.slno + "," + Genclass.data1 + ",40";
                                Genclass.cmd = new SqlCommand(Genclass.strsql, conn);
                                SqlDataAdapter aptr5 = new SqlDataAdapter(Genclass.cmd);
                                DataTable tap5 = new DataTable();
                                aptr5.Fill(tap5);
                                if (tap5.Rows.Count > 0)
                                {
                                    SqlDataAdapter da = new SqlDataAdapter("sp_pdffincs", conn);
                                    da.SelectCommand.CommandType = CommandType.StoredProcedure;
                                    da.SelectCommand.Parameters.Add("@Muid", SqlDbType.Int).Value = Genclass.Prtid;
                                    da.SelectCommand.Parameters.Add("@slno", SqlDbType.Int).Value = Genclass.slno;
                                    da.SelectCommand.Parameters.Add("@companyid", SqlDbType.Int).Value = Genclass.data1;
                                    da.SelectCommand.Parameters.Add("@doctypeid", SqlDbType.Int).Value = 40;
                                    DataSet ds = new DataSet();
                                    da.Fill(ds, "salesinvoice");
                                    doc.Load(Application.StartupPath + "\\salesinvoiceven.rpt");
                                    SqlDataAdapter da1 = new SqlDataAdapter("terms", conn);
                                    da1.SelectCommand.CommandType = CommandType.StoredProcedure;
                                    da1.SelectCommand.Parameters.Add("@uid", SqlDbType.Int).Value = Genclass.Prtid;
                                    DataTable ds1 = new DataTable("Terms");
                                    da1.Fill(ds1);
                                    ds.Tables.Add(ds1);
                                    doc.Subreports["Termrpt1.rpt"].SetDataSource(ds1);
                                    doc.SetDataSource(ds);
                                    Cryview.ReportSource = doc;
                                }
                                else
                                {
                                    SqlDataAdapter da = new SqlDataAdapter("[sp_pdfnew1]", conn);
                                    da.SelectCommand.CommandType = CommandType.StoredProcedure;
                                    da.SelectCommand.Parameters.Add("@Muid", SqlDbType.Int).Value = Genclass.Prtid;
                                    da.SelectCommand.Parameters.Add("@slno", SqlDbType.Int).Value = Genclass.slno;
                                    da.SelectCommand.Parameters.Add("@companyid", SqlDbType.Int).Value = Genclass.data1;
                                    da.SelectCommand.Parameters.Add("@doctypeid", SqlDbType.Int).Value = 40;
                                    DataSet ds = new DataSet();
                                    da.Fill(ds, "salesinvoice");
                                    if (Genclass.data1 == 1)
                                    {
                                        doc.Load(Application.StartupPath + "\\salesinvoiceNew.rpt");
                                    }
                                    else
                                    {
                                        doc.Load(Application.StartupPath + "\\salesinvoiceNewVen.rpt");
                                    }
                                    SqlDataAdapter da1 = new SqlDataAdapter("terms", conn);
                                    da1.SelectCommand.CommandType = CommandType.StoredProcedure;
                                    da1.SelectCommand.Parameters.Add("@uid", SqlDbType.Int).Value = Genclass.Prtid;
                                    DataTable ds1 = new DataTable("Terms");
                                    da1.Fill(ds1);
                                    ds.Tables.Add(ds1);
                                    doc.Subreports["Termrpt1.rpt"].SetDataSource(ds1);
                                    SqlDataAdapter da2 = new SqlDataAdapter("SP_taxcal", conn);
                                    da1.SelectCommand.CommandType = CommandType.StoredProcedure;
                                    da1.SelectCommand.Parameters.Add("@muid", SqlDbType.Int).Value = Genclass.Prtid;
                                    da1.SelectCommand.Parameters.Add("@statuid", SqlDbType.Int).Value = tap3.Rows[0]["stateuid"].ToString();
                                    DataTable ds2 = new DataTable("taxcal");
                                    da2.Fill(ds2);
                                    ds.Tables.Add(ds2);
                                    doc.Subreports["Taxrpt.rpt"].SetDataSource(ds2);
                                    doc.SetDataSource(ds);
                                    Cryview.ReportSource = doc;
                                }
                            }
                            else
                            {
                                SqlDataAdapter da = new SqlDataAdapter("[sp_pdfnew1]", conn);
                                da.SelectCommand.CommandType = CommandType.StoredProcedure;
                                da.SelectCommand.Parameters.Add("@Muid", SqlDbType.Int).Value = Genclass.Prtid;
                                da.SelectCommand.Parameters.Add("@slno", SqlDbType.Int).Value = Genclass.slno;
                                da.SelectCommand.Parameters.Add("@companyid", SqlDbType.Int).Value = Genclass.data1;
                                da.SelectCommand.Parameters.Add("@doctypeid", SqlDbType.Int).Value = 40;
                                DataSet ds = new DataSet();
                                da.Fill(ds, "salesinvoice");
                                if (Genclass.data1 == 1)
                                {
                                    doc.Load(Application.StartupPath + "\\salesinvoiceNew.rpt");
                                }
                                else
                                {
                                    doc.Load(Application.StartupPath + "\\salesinvoiceNewVen.rpt");
                                }
                                SqlDataAdapter da1 = new SqlDataAdapter("terms", conn);
                                da1.SelectCommand.CommandType = CommandType.StoredProcedure;
                                da1.SelectCommand.Parameters.Add("@uid", SqlDbType.Int).Value = Genclass.Prtid;
                                DataTable ds1 = new DataTable("Terms");
                                da1.Fill(ds1);
                                ds.Tables.Add(ds1);
                                doc.Subreports["Termrpt1.rpt"].SetDataSource(ds1);
                                SqlDataAdapter da2 = new SqlDataAdapter("SP_taxcal", conn);
                                da2.SelectCommand.CommandType = CommandType.StoredProcedure;
                                da2.SelectCommand.Parameters.Add("@muid", SqlDbType.Int).Value = Genclass.Prtid;
                                da2.SelectCommand.Parameters.Add("@statuid", SqlDbType.Int).Value = tap3.Rows[0]["stateuid"].ToString();
                                DataTable ds2 = new DataTable("taxcal");
                                da2.Fill(ds2);
                                ds.Tables.Add(ds2);
                                doc.Subreports["Taxrpt.rpt"].SetDataSource(ds2);
                                doc.SetDataSource(ds);
                                Cryview.ReportSource = doc;
                            }
                        }
                        else if (tap3.Rows[0]["tag"].ToString() == "1")
                        {
                            SqlDataAdapter da = new SqlDataAdapter("sp_pdfcgsg", conn);
                            da.SelectCommand.CommandType = CommandType.StoredProcedure;
                            da.SelectCommand.Parameters.Add("@Muid", SqlDbType.Int).Value = Genclass.Prtid;
                            da.SelectCommand.Parameters.Add("@slno", SqlDbType.Int).Value = Genclass.slno;
                            da.SelectCommand.Parameters.Add("@companyid", SqlDbType.Int).Value = Genclass.data1;
                            DataSet ds = new DataSet();
                            da.Fill(ds, "salesinvoice");
                            doc.Load(Application.StartupPath + "\\salesinvoicecgsg.rpt");
                            SqlDataAdapter da1 = new SqlDataAdapter("terms", conn);
                            da1.SelectCommand.CommandType = CommandType.StoredProcedure;
                            da1.SelectCommand.Parameters.Add("@uid", SqlDbType.Int).Value = Genclass.Prtid;
                            DataTable ds1 = new DataTable("Terms");
                            da1.Fill(ds1);
                            ds.Tables.Add(ds1);
                            doc.Subreports["Termrpt1.rpt"].SetDataSource(ds1);
                            doc.SetDataSource(ds);
                            Cryview.ReportSource = doc;
                        }
                    }
                    else
                    {
                        if (tap3.Rows[0]["tag"].ToString() == "0")
                        {
                            SqlDataAdapter da = new SqlDataAdapter("[sp_pdfnew1]", conn);
                            da.SelectCommand.CommandType = CommandType.StoredProcedure;
                            da.SelectCommand.Parameters.Add("@Muid", SqlDbType.Int).Value = Genclass.Prtid;
                            da.SelectCommand.Parameters.Add("@slno", SqlDbType.Int).Value = Genclass.slno;
                            da.SelectCommand.Parameters.Add("@companyid", SqlDbType.Int).Value = Genclass.data1;
                            da.SelectCommand.Parameters.Add("@doctypeid", SqlDbType.Int).Value = 40;
                            DataSet ds = new DataSet();
                            da.Fill(ds, "salesinvoice");
                            if (Genclass.data1 == 1)
                            {
                                doc.Load(Application.StartupPath + "\\salesinvoiceNew.rpt");
                            }
                            else
                            {
                                doc.Load(Application.StartupPath + "\\salesinvoiceNewVen.rpt");
                            }
                            SqlDataAdapter da1 = new SqlDataAdapter("terms", conn);
                            da1.SelectCommand.CommandType = CommandType.StoredProcedure;
                            da1.SelectCommand.Parameters.Add("@uid", SqlDbType.Int).Value = Genclass.Prtid;
                            DataTable ds1 = new DataTable("Terms");
                            da1.Fill(ds1);
                            ds.Tables.Add(ds1);
                            doc.Subreports["Termrpt1.rpt"].SetDataSource(ds1);
                            Genclass.strsql = "SP_taxcal  " + Genclass.Prtid + " ," + tap3.Rows[0]["stateuid"].ToString() + "";
                            SqlDataAdapter da2 = new SqlDataAdapter("SP_taxcal", conn);
                            da2.SelectCommand.CommandType = CommandType.StoredProcedure;
                            da2.SelectCommand.Parameters.Add("@muid", SqlDbType.Int).Value = Genclass.Prtid;
                            da2.SelectCommand.Parameters.Add("@statuid", SqlDbType.Int).Value = tap3.Rows[0]["stateuid"].ToString();
                            DataTable ds2 = new DataTable("taxcal");
                            da2.Fill(ds2);
                            ds.Tables.Add(ds2);
                            doc.Subreports["Taxrpt.rpt"].SetDataSource(ds2);
                            doc.SetDataSource(ds);
                            Cryview.ReportSource = doc;
                        }
                        if (tap3.Rows[0]["tag"].ToString() == "1")
                        {
                            SqlDataAdapter da = new SqlDataAdapter("Sp_salprtexlfinigst", conn);
                            da.SelectCommand.CommandType = CommandType.StoredProcedure;
                            da.SelectCommand.Parameters.Add("@Muid", SqlDbType.Int).Value = Genclass.Prtid;
                            da.SelectCommand.Parameters.Add("@slno", SqlDbType.Int).Value = Genclass.slno;
                            da.SelectCommand.Parameters.Add("@companyid", SqlDbType.Int).Value = Genclass.data1;
                            DataSet ds = new DataSet();
                            da.Fill(ds, "salesinvoice");
                            if (Genclass.data1 == 1)
                            {
                                doc.Load(Application.StartupPath + "\\salesinvoicecgsg.rpt");
                            }
                            else
                            {
                                doc.Load(Application.StartupPath + "\\salesinvoiceNewVen.rpt");
                            }
                            SqlDataAdapter da1 = new SqlDataAdapter("terms", conn);
                            da1.SelectCommand.CommandType = CommandType.StoredProcedure;
                            da1.SelectCommand.Parameters.Add("@uid", SqlDbType.Int).Value = Genclass.Prtid;
                            DataTable ds1 = new DataTable("Terms");
                            da1.Fill(ds1);
                            ds.Tables.Add(ds1);
                            doc.Subreports["Termrpt1.rpt"].SetDataSource(ds1);
                            doc.SetDataSource(ds);
                            Cryview.ReportSource = doc;
                        }
                    }
                }
            }
            else if (Genclass.Dtype == 90)
            {
                Genclass.strsql = "select * from Vw_salprtexlfin   where muid=" + Genclass.Prtid + "  and doctypeid=90";

                Genclass.cmd = new SqlCommand(Genclass.strsql, conn);
                SqlDataAdapter aptr = new SqlDataAdapter(Genclass.cmd);
                DataTable tap = new DataTable();
                aptr.Fill(tap);
                if (tap.Rows.Count > 0)
                {
                    if (tap.Rows[0]["bstate"].ToString() == "Tamil Nadu 33")
                    {


                        SqlDataAdapter da = new SqlDataAdapter("sp_pdfNew1", conn);
                        da.SelectCommand.CommandType = CommandType.StoredProcedure;
                        da.SelectCommand.Parameters.Add("@Muid", SqlDbType.Int).Value = Genclass.Prtid;
                        da.SelectCommand.Parameters.Add("@slno", SqlDbType.Int).Value = Genclass.slno;
                        da.SelectCommand.Parameters.Add("@companyid", SqlDbType.Int).Value = Genclass.data1;
                        da.SelectCommand.Parameters.Add("@doctypeid", SqlDbType.Int).Value = 90;
                        DataSet ds = new DataSet();
                        da.Fill(ds, "salesinvoice");

                        if (Genclass.data1 == 1)
                        {
                            doc.Load(Application.StartupPath + "\\PurchaseOrder.rpt");
                        }
                        else
                        {
                            doc.Load(Application.StartupPath + "\\salesinvoiceven.rpt");
                        }



                        SqlDataAdapter da1 = new SqlDataAdapter("terms", conn);
                        da1.SelectCommand.CommandType = CommandType.StoredProcedure;
                        da1.SelectCommand.Parameters.Add("@uid", SqlDbType.Int).Value = Genclass.Prtid;
                        DataTable ds1 = new DataTable("Terms");
                        da1.Fill(ds1);
                        ds.Tables.Add(ds1);

                        doc.Subreports["Termrpt1.rpt"].SetDataSource(ds1);

                        SqlDataAdapter da2 = new SqlDataAdapter("SP_taxcal", conn);
                        da2.SelectCommand.CommandType = CommandType.StoredProcedure;
                        da2.SelectCommand.Parameters.Add("@muid", SqlDbType.Int).Value = Genclass.Prtid;
                        da2.SelectCommand.Parameters.Add("@statuid", SqlDbType.Int).Value = tap.Rows[0]["stateuid"].ToString();
                        da2.SelectCommand.Parameters.Add("@companyid", SqlDbType.Int).Value = Genclass.data1;
                        DataTable ds2 = new DataTable("taxcal");
                        da2.Fill(ds2);
                        ds.Tables.Add(ds2);
                        doc.Subreports["Taxrpt.rpt"].SetDataSource(ds2);
                        doc.SetDataSource(ds);
                        Cryview.ReportSource = doc;
                    }

                    else
                    {
                        SqlDataAdapter da = new SqlDataAdapter("[sp_pdfNew1]", conn);
                        da.SelectCommand.CommandType = CommandType.StoredProcedure;
                        da.SelectCommand.Parameters.Add("@Muid", SqlDbType.Int).Value = Genclass.Prtid;
                        da.SelectCommand.Parameters.Add("@slno", SqlDbType.Int).Value = Genclass.slno;
                        da.SelectCommand.Parameters.Add("@companyid", SqlDbType.Int).Value = Genclass.data1;
                        da.SelectCommand.Parameters.Add("@doctypeid", SqlDbType.Int).Value = 90;
                        DataSet ds = new DataSet();
                        da.Fill(ds, "salesinvoice");
                        if (Genclass.data1 == 1)
                        {
                            doc.Load(Application.StartupPath + "\\PurchaseOrder.rpt");
                        }
                        else
                        {
                            doc.Load(Application.StartupPath + "\\salesinvoiceven.rpt");
                        }
                        SqlDataAdapter da1 = new SqlDataAdapter("terms", conn);
                        da1.SelectCommand.CommandType = CommandType.StoredProcedure;
                        da1.SelectCommand.Parameters.Add("@uid", SqlDbType.Int).Value = Genclass.Prtid;
                        DataTable ds1 = new DataTable("Terms");
                        da1.Fill(ds1);
                        ds.Tables.Add(ds1);
                        doc.Subreports["Termrpt1.rpt"].SetDataSource(ds1);
                        SqlDataAdapter da2 = new SqlDataAdapter("SP_taxcal", conn);
                        da2.SelectCommand.CommandType = CommandType.StoredProcedure;
                        da2.SelectCommand.Parameters.Add("@muid", SqlDbType.Int).Value = Genclass.Prtid;
                        da2.SelectCommand.Parameters.Add("@statuid", SqlDbType.Int).Value = tap.Rows[0]["stateuid"].ToString();
                        da2.SelectCommand.Parameters.Add("@companyid", SqlDbType.Int).Value = Genclass.data1;
                        DataTable ds2 = new DataTable("taxcal");
                        da2.Fill(ds2);
                        ds.Tables.Add(ds2);
                        doc.Subreports["Taxrpt.rpt"].SetDataSource(ds2);
                        doc.SetDataSource(ds);
                        Cryview.ReportSource = doc;
                    }
                }
            }
            else if (Genclass.Dtype == 100)
            {
                Genclass.strsql = "select * from Vw_salprtexlfin   where muid=" + Genclass.Prtid + " and Companyid=" + Genclass.data1 + " and doctypeid=100";

                Genclass.cmd = new SqlCommand(Genclass.strsql, conn);
                SqlDataAdapter aptr = new SqlDataAdapter(Genclass.cmd);
                DataTable tap = new DataTable();
                aptr.Fill(tap);
                if (tap.Rows.Count > 0)
                {
                    if (tap.Rows[0]["bstate"].ToString() == "Tamil Nadu 33")
                    {
                        SqlDataAdapter da = new SqlDataAdapter("[sp_pdfNew1]", conn);
                        da.SelectCommand.CommandType = CommandType.StoredProcedure;
                        da.SelectCommand.Parameters.Add("@Muid", SqlDbType.Int).Value = Genclass.Prtid;
                        da.SelectCommand.Parameters.Add("@slno", SqlDbType.Int).Value = Genclass.slno;
                        da.SelectCommand.Parameters.Add("@companyid", SqlDbType.Int).Value = Genclass.data1;
                        da.SelectCommand.Parameters.Add("@doctypeid", SqlDbType.Int).Value = 100;
                        DataSet ds = new DataSet();
                        da.Fill(ds, "salesinvoice");
                        if (Genclass.data1 == 1)
                        {
                            doc.Load(Application.StartupPath + "\\BillAccounting.rpt");
                        }
                        else
                        {
                            doc.Load(Application.StartupPath + "\\salesinvoiceven.rpt");
                        }
                        SqlDataAdapter da1 = new SqlDataAdapter("terms", conn);
                        da1.SelectCommand.CommandType = CommandType.StoredProcedure;
                        da1.SelectCommand.Parameters.Add("@uid", SqlDbType.Int).Value = Genclass.Prtid;
                        DataTable ds1 = new DataTable("Terms");
                        da1.Fill(ds1);
                        ds.Tables.Add(ds1);
                        doc.Subreports["Termrpt1.rpt"].SetDataSource(ds1);
                        SqlDataAdapter da2 = new SqlDataAdapter("SP_taxcal", conn);
                        da2.SelectCommand.CommandType = CommandType.StoredProcedure;
                        da2.SelectCommand.Parameters.Add("@muid", SqlDbType.Int).Value = Genclass.Prtid;
                        da2.SelectCommand.Parameters.Add("@statuid", SqlDbType.Int).Value = tap.Rows[0]["stateuid"].ToString();
                        da2.SelectCommand.Parameters.Add("@companyid", SqlDbType.Int).Value = Genclass.data1;
                        DataTable ds2 = new DataTable("taxcal");
                        da2.Fill(ds2);
                        ds.Tables.Add(ds2);
                        doc.Subreports["Taxrpt.rpt"].SetDataSource(ds2);
                        doc.SetDataSource(ds);
                        Cryview.ReportSource = doc;
                    }

                    else
                    {
                        SqlDataAdapter da = new SqlDataAdapter("[sp_pdfNew1]", conn);
                        da.SelectCommand.CommandType = CommandType.StoredProcedure;
                        da.SelectCommand.Parameters.Add("@Muid", SqlDbType.Int).Value = Genclass.Prtid;
                        da.SelectCommand.Parameters.Add("@slno", SqlDbType.Int).Value = Genclass.slno;
                        da.SelectCommand.Parameters.Add("@companyid", SqlDbType.Int).Value = Genclass.data1;
                        da.SelectCommand.Parameters.Add("@doctypeid", SqlDbType.Int).Value = 100;
                        DataSet ds = new DataSet();
                        da.Fill(ds, "salesinvoice");
                        if (Genclass.data1 == 1)
                        {
                            doc.Load(Application.StartupPath + "\\BillAccounting.rpt");
                        }
                        else
                        {
                            doc.Load(Application.StartupPath + "\\salesinvoiceven.rpt");
                        }
                        SqlDataAdapter da1 = new SqlDataAdapter("terms", conn);
                        da1.SelectCommand.CommandType = CommandType.StoredProcedure;
                        da1.SelectCommand.Parameters.Add("@uid", SqlDbType.Int).Value = Genclass.Prtid;
                        DataTable ds1 = new DataTable("Terms");
                        da1.Fill(ds1);
                        ds.Tables.Add(ds1);
                        doc.Subreports["Termrpt1.rpt"].SetDataSource(ds1);
                        SqlDataAdapter da2 = new SqlDataAdapter("SP_taxcal", conn);
                        da2.SelectCommand.CommandType = CommandType.StoredProcedure;
                        da2.SelectCommand.Parameters.Add("@muid", SqlDbType.Int).Value = Genclass.Prtid;
                        da2.SelectCommand.Parameters.Add("@statuid", SqlDbType.Int).Value = tap.Rows[0]["stateuid"].ToString();
                        da2.SelectCommand.Parameters.Add("@companyid", SqlDbType.Int).Value = Genclass.data1;
                        DataTable ds2 = new DataTable("taxcal");
                        da2.Fill(ds2);
                        ds.Tables.Add(ds2);
                        doc.Subreports["Taxrpt.rpt"].SetDataSource(ds2);
                        doc.SetDataSource(ds);
                        Cryview.ReportSource = doc;
                    }
                }
            }
            else if (Genclass.Dtype == 1)
            {
                SqlDataAdapter da = new SqlDataAdapter("SP_stkstmt", conn);
                da.SelectCommand.CommandType = CommandType.StoredProcedure;
                da.SelectCommand.Parameters.Add("@fromdt", SqlDbType.SmallDateTime).Value = Genclass.prttodate;
                da.SelectCommand.Parameters.Add("@todt", SqlDbType.SmallDateTime).Value = Genclass.prttodate;
                da.SelectCommand.Parameters.Add("@companyid", SqlDbType.Int).Value = Genclass.data1;
                DataSet ds = new DataSet();
                da.Fill(ds, "Stockstmt");
                doc.Load(Application.StartupPath + "\\Stmtitem.rpt");
                doc.SetDataSource(ds);
                Cryview.ReportSource = doc;
            }
            else if (Genclass.Dtype == 2)
            {
                SqlDataAdapter da = new SqlDataAdapter("SP_stkledger", conn);
                da.SelectCommand.CommandType = CommandType.StoredProcedure;
                da.SelectCommand.Parameters.Add("@fromdt", SqlDbType.SmallDateTime).Value = Genclass.prttodate;
                da.SelectCommand.Parameters.Add("@todt", SqlDbType.SmallDateTime).Value = Genclass.prttodate;
                da.SelectCommand.Parameters.Add("@companyid", SqlDbType.Int).Value = Genclass.data1;
                DataSet ds = new DataSet();
                da.Fill(ds, "Stockledger");
                doc.Load(Application.StartupPath + "\\Stocklednew.rpt");
                doc.SetDataSource(ds);
                Cryview.ReportSource = doc;
            }
            else if (Genclass.Dtype == 210)
            {
                string query = @"select  docno, Docdate,   Doctypeid,itemuid, itemname,uom, millname, Qty,  Name, Address1,Address2,hsncode, remarks,  city ,Phone,bgstno,Iname, Iadd1,Iadd2,Iphone,
                                 Iemail ,Igstin, IPan,bb.totalqty,noofbags,jj.copyname, price,taxablevalue,basicvalue,totalvalue,total,roff,taxper,taxval,sgstid,sgstval,cgstval ,cgstid ,taxtot,z.taxtotal
                                from   
                                (select docno,Docdate,Doctypeid,d.itemname,e.Name as millname,b.itemuid,b.Qty,c.Name,c.Address1,c.Address2,isnull(c.City,'')+ '' + isnull(c.pin,'') as city,  
                                c.Phone,'GSTIN : ' + c.TNGST as bgstno,pp.name as Iname,pp.Address1 as Iadd1,pp.Address2 as Iadd2,a.remarks as remarks,hh.hsncode,b.noofbags,b.price,b.basicvalue,
                                a.taxablealue  as taxablevalue,a.totalvalue,a.roff,a.totalvalue as Total,a.sgstper as sgstid,A.sgstvalue as sgstval,cgstper as cgstid,cgstvalue as cgstval, case cgstper when 0 then Igstvalue else cgstvalue*2  end as taxtot,
                                igstper taxper,Igstvalue taxval ,  g.generalname as uom,pp.Phone as Iphone,pp.Emailid as Iemail,pp.TNGST as Igstin,pp.CST as IPan,a.uid
                                from JoI a inner join JoIlist b on a.Uid=b.JoIuid inner join PartyM c on a.Supplieruid=c.Uid  
                                left join ItemM d on b.Itemuid=d.Uid  left join generalm g on d.uom_uid=g.uid left join PartyM e on b.millid=e.uid left join PartyM pp on pp.uid=pp.uid and pp.Ptype=1  
                                left join hsn hh on D.hsnid=hh.uid where a.Uid=@uid group by docno,Docdate,Doctypeid,d.itemname,e.Name,b.itemuid,b.Qty,c.Name,c.Address1,c.Address2,c.City,c.pin,c.Phone,c.TNGST ,pp.name,pp.Address1,pp.Address2,
                                a.remarks ,hh.hsncode,b.noofbags,b.price,b.basicvalue,a.taxablealue,a.totalvalue,a.roff,sgstper,sgstvalue,igstper,cgstvalue,Igstvalue,cgstper,
                                pp.Phone,pp.Emailid,pp.TNGST,pp.CST,a.uid , g.generalname)aa  
                                left join   
                                (select a.Uid,SUM(Qty) as totalqty  
                                from JoI a inner join JoIlist b on a.Uid=b.JoIuid inner join PartyM c on a.Supplieruid=c.Uid  
                                left join ItemM d on b.Itemuid=d.Uid  left join PartyM e on b.millid=e.uid left join PartyM pp on pp.uid=pp.uid and pp.Ptype=1  where a.Uid=@uid  
                                group by a.uid)bb on aa.uid=bb.uid  
                                left join(select sum(taxablevalue) as taxtotal,JoIuid from JoIlist group by  JoIuid)z on aa.uid=z.JoIuid and z.JoIuid=@uid
                                left join   printcopy1 jj on JJ.slno=JJ.slno where slno=@slno";
                SqlDataAdapter da = new SqlDataAdapter(query, conn);
                da.SelectCommand.CommandType = CommandType.Text;
                da.SelectCommand.Parameters.Add("@uid", SqlDbType.Int).Value = Genclass.Prtid;
                da.SelectCommand.Parameters.Add("@slno", SqlDbType.Int).Value = 1;
                DataSet ds = new DataSet();
                da.Fill(ds, "PRINT");
                doc.Load(Application.StartupPath + "\\JobOrderIssuePrt.rpt");
                doc.SetDataSource(ds);
                Cryview.ReportSource = doc;
            }
            else if (Genclass.Dtype == 45)
            {
                SqlDataAdapter da = new SqlDataAdapter("SP_PLSLIPPRT", conn);
                da.SelectCommand.CommandType = CommandType.StoredProcedure;
                da.SelectCommand.Parameters.Add("@pksid", SqlDbType.Int).Value = Genclass.Prtid;
                DataSet ds = new DataSet();
                da.Fill(ds, "PackingSlip");
                doc.Load(Application.StartupPath + "\\PackingSliprpt.rpt");
                doc.SetDataSource(ds);
                Cryview.ReportSource = doc;
            }
            else if (Genclass.Dtype == 1000)
            {
                ReportDocument docBar = new ReportDocument();
                string Barcode = Genclass.Barcode;
                int Uid = Genclass.Prtid;
                string Sort = Genclass.SortNo;
                KeepDynamic.Barcode.CrystalReport.BarCode br = new KeepDynamic.Barcode.CrystalReport.BarCode();
                br.SymbologyType = KeepDynamic.Barcode.CrystalReport.SymbologyType.Code128;
                br.CodeText = Barcode;
                byte[] bt = br.drawBarcodeAsBytes();
                SqlParameter[] para = { new SqlParameter("@uid", Uid) };
                DataTable dtS = db.GetDataWithParam(CommandType.StoredProcedure, "SP_LoomCardPrint", para, conn);
                DataTable dt = new DataTable();
                dt.Columns.Add("DocNo", typeof(string));
                dt.Columns.Add("Barcode", typeof(byte[]));
                dt.Columns.Add("sortNo", typeof(string));
                dt.Columns.Add("design", typeof(string));
                dt.Columns.Add("rollno", typeof(string));
                dt.Columns.Add("warpcount", typeof(string));
                dt.Columns.Add("WeptCount", typeof(string));
                dt.Columns.Add("epi", typeof(string));
                dt.Columns.Add("ppi", typeof(string));
                dt.Columns.Add("GreyWidth", typeof(string));
                dt.Columns.Add("TotalEnds", typeof(decimal));
                dt.Columns.Add("Reed", typeof(string));
                dt.Columns.Add("ReedSpace", typeof(string));
                dt.Columns.Add("PickWheel", typeof(string));
                dt.Columns.Add("wtmtr", typeof(string));
                dt.Columns.Add("gtlen", typeof(decimal));
                dt.Columns.Add("gtwei", typeof(decimal));
                DataRow dr = dt.NewRow();
                dr["DocNo"] = Sort;
                dr["Barcode"] = bt;
                dr["sortNo"] = Sort;
                dr["design"] = dtS.Rows[0]["design"].ToString();
                dr["rollno"] = dtS.Rows[0]["rollno"].ToString();
                dr["warpcount"] = dtS.Rows[0]["warpcount"].ToString();
                dr["WeptCount"] = dtS.Rows[0]["WeptCount"].ToString();
                dr["epi"] = dtS.Rows[0]["epi"].ToString();
                dr["ppi"] = dtS.Rows[0]["ppi"].ToString();
                dr["GreyWidth"] = dtS.Rows[0]["GreyWidth"].ToString();
                dr["TotalEnds"] = dtS.Rows[0]["TotalEnds"];
                dr["Reed"] = dtS.Rows[0]["Reed"].ToString();
                dr["ReedSpace"] = dtS.Rows[0]["ReedSpace"].ToString();
                dr["PickWheel"] = dtS.Rows[0]["PickWheel"].ToString();
                dr["wtmtr"] = dtS.Rows[0]["wtmtr"];
                decimal gtLen = (decimal)dtS.Rows[0]["gtlen"];
                decimal gtwei = (decimal)dtS.Rows[0]["gtwei"];
                if (gtLen == 0)
                {
                    dr["gtlen"] = DBNull.Value;
                }
                else
                {
                    dr["gtlen"] = dtS.Rows[0]["gtlen"];
                }
                if (gtwei == 0)
                {
                    dr["gtwei"] = DBNull.Value;
                }
                else
                {
                    dr["gtwei"] = dtS.Rows[0]["gtwei"];
                }
                dt.Rows.Add(dr);
                docBar.Load(Application.StartupPath + "\\CryLoomCard.rpt");

                docBar.SetDataSource(dt);
                Cryview.ReportSource = docBar;

            }
            conn.Close();
        }


        public string Rupees(Int64 rup)
        {
            string result = "";
            Int64 res;
            if ((rup / 10000000) > 0)
            {
                res = rup / 10000000;
                rup = rup % 10000000;
                result = result + ' ' + RupeesToWords(res) + " Crore";
            }
            if ((rup / 100000) > 0)
            {
                res = rup / 100000;
                rup = rup % 100000;
                result = result + ' ' + RupeesToWords(res) + " Lakh";
            }
            if ((rup / 1000) > 0)
            {
                res = rup / 1000;
                rup = rup % 1000;
                result = result + ' ' + RupeesToWords(res) + " Thousand";
            }


            if ((rup / 100) > 0)
            {
                res = rup / 100;
                rup = rup % 100;
                if (rup > 0)
                {
                    result = result + ' ' + RupeesToWords(res) + " Hundred and";
                }
                else
                {
                    result = result + ' ' + RupeesToWords(res) + " Hundred";
                }
            }
            if ((rup % 10) >= 0)
            {
                res = rup % 100;
                result = result + " " + RupeesToWords(res);
            }
            result = "Rupees " + result + ' ' + "only";
            return result;
        }

        public string RupeesToWords(Int64 rup)
        {
            string result = "";
            if ((rup >= 1) && (rup <= 10))
            {
                if ((rup % 10) == 1) result = "One";
                if ((rup % 10) == 2) result = "Two";
                if ((rup % 10) == 3) result = "Three";
                if ((rup % 10) == 4) result = "Four";
                if ((rup % 10) == 5) result = "Five";
                if ((rup % 10) == 6) result = "Six";
                if ((rup % 10) == 7) result = "Seven";
                if ((rup % 10) == 8) result = "Eight";
                if ((rup % 10) == 9) result = "Nine";
                if ((rup % 10) == 0) result = "Ten";
            }
            if (rup > 9 && rup < 20)
            {
                if (rup == 11) result = "Eleven";
                if (rup == 12) result = "Twelve";
                if (rup == 13) result = "Thirteen";
                if (rup == 14) result = "Forteen";
                if (rup == 15) result = "Fifteen";
                if (rup == 16) result = "Sixteen";
                if (rup == 17) result = "Seventeen";
                if (rup == 18) result = "Eighteen";
                if (rup == 19) result = "Nineteen";
            }

            if (rup >= 20 && (rup / 10) == 2 && (rup % 10) == 0) result = "Twenty";
            if (rup > 20 && (rup / 10) == 3 && (rup % 10) == 0) result = "Thirty";
            if (rup > 20 && (rup / 10) == 4 && (rup % 10) == 0) result = "Forty";
            if (rup > 20 && (rup / 10) == 5 && (rup % 10) == 0) result = "Fifty";
            if (rup > 20 && (rup / 10) == 6 && (rup % 10) == 0) result = "Sixty";
            if (rup > 20 && (rup / 10) == 7 && (rup % 10) == 0) result = "Seventy";
            if (rup > 20 && (rup / 10) == 8 && (rup % 10) == 0) result = "Eighty";
            if (rup > 20 && (rup / 10) == 9 && (rup % 10) == 0) result = "Ninty";

            if (rup > 20 && (rup / 10) == 2 && (rup % 10) != 0)
            {
                if ((rup % 10) == 1) result = "Twenty One";
                if ((rup % 10) == 2) result = "Twenty Two";
                if ((rup % 10) == 3) result = "Twenty Three";
                if ((rup % 10) == 4) result = "Twenty Four";
                if ((rup % 10) == 5) result = "Twenty Five";
                if ((rup % 10) == 6) result = "Twenty Six";
                if ((rup % 10) == 7) result = "Twenty Seven";
                if ((rup % 10) == 8) result = "Twenty Eight";
                if ((rup % 10) == 9) result = "Twenty Nine";
            }
            if (rup > 20 && (rup / 10) == 3 && (rup % 10) != 0)
            {
                if ((rup % 10) == 1) result = "Thirty One";
                if ((rup % 10) == 2) result = "Thirty Two";
                if ((rup % 10) == 3) result = "Thirty Three";
                if ((rup % 10) == 4) result = "Thirty Four";
                if ((rup % 10) == 5) result = "Thirty Five";
                if ((rup % 10) == 6) result = "Thirty Six";
                if ((rup % 10) == 7) result = "Thirty Seven";
                if ((rup % 10) == 8) result = "Thirty Eight";
                if ((rup % 10) == 9) result = "Thirty Nine";
            }
            if (rup > 20 && (rup / 10) == 4 && (rup % 10) != 0)
            {
                if ((rup % 10) == 1) result = "Forty One";
                if ((rup % 10) == 2) result = "Forty Two";
                if ((rup % 10) == 3) result = "Forty Three";
                if ((rup % 10) == 4) result = "Forty Four";
                if ((rup % 10) == 5) result = "Forty Five";
                if ((rup % 10) == 6) result = "Forty Six";
                if ((rup % 10) == 7) result = "Forty Seven";
                if ((rup % 10) == 8) result = "Forty Eight";
                if ((rup % 10) == 9) result = "Forty Nine";
            }
            if (rup > 20 && (rup / 10) == 5 && (rup % 10) != 0)
            {
                if ((rup % 10) == 1) result = "Fifty One";
                if ((rup % 10) == 2) result = "Fifty Two";
                if ((rup % 10) == 3) result = "Fifty Three";
                if ((rup % 10) == 4) result = "Fifty Four";
                if ((rup % 10) == 5) result = "Fifty Five";
                if ((rup % 10) == 6) result = "Fifty Six";
                if ((rup % 10) == 7) result = "Fifty Seven";
                if ((rup % 10) == 8) result = "Fifty Eight";
                if ((rup % 10) == 9) result = "Fifty Nine";
            }
            if (rup > 20 && (rup / 10) == 6 && (rup % 10) != 0)
            {
                if ((rup % 10) == 1) result = "Sixty One";
                if ((rup % 10) == 2) result = "Sixty Two";
                if ((rup % 10) == 3) result = "Sixty Three";
                if ((rup % 10) == 4) result = "Sixty Four";
                if ((rup % 10) == 5) result = "Sixty Five";
                if ((rup % 10) == 6) result = "Sixty Six";
                if ((rup % 10) == 7) result = "Sixty Seven";
                if ((rup % 10) == 8) result = "Sixty Eight";
                if ((rup % 10) == 9) result = "Sixty Nine";
            }
            if (rup > 20 && (rup / 10) == 7 && (rup % 10) != 0)
            {
                if ((rup % 10) == 1) result = "Seventy One";
                if ((rup % 10) == 2) result = "Seventy Two";
                if ((rup % 10) == 3) result = "Seventy Three";
                if ((rup % 10) == 4) result = "Seventy Four";
                if ((rup % 10) == 5) result = "Seventy Five";
                if ((rup % 10) == 6) result = "Seventy Six";
                if ((rup % 10) == 7) result = "Seventy Seven";
                if ((rup % 10) == 8) result = "Seventy Eight";
                if ((rup % 10) == 9) result = "Seventy Nine";
            }
            if (rup > 20 && (rup / 10) == 8 && (rup % 10) != 0)
            {
                if ((rup % 10) == 1) result = "Eighty One";
                if ((rup % 10) == 2) result = "Eighty Two";
                if ((rup % 10) == 3) result = "Eighty Three";
                if ((rup % 10) == 4) result = "Eighty Four";
                if ((rup % 10) == 5) result = "Eighty Five";
                if ((rup % 10) == 6) result = "Eighty Six";
                if ((rup % 10) == 7) result = "Eighty Seven";
                if ((rup % 10) == 8) result = "Eighty Eight";
                if ((rup % 10) == 9) result = "Eighty Nine";
            }
            if (rup > 20 && (rup / 10) == 9 && (rup % 10) != 0)
            {
                if ((rup % 10) == 1) result = "Ninty One";
                if ((rup % 10) == 2) result = "Ninty Two";
                if ((rup % 10) == 3) result = "Ninty Three";
                if ((rup % 10) == 4) result = "Ninty Four";
                if ((rup % 10) == 5) result = "Ninty Five";
                if ((rup % 10) == 6) result = "Ninty Six";
                if ((rup % 10) == 7) result = "Ninty Seven";
                if ((rup % 10) == 8) result = "Ninty Eight";
                if ((rup % 10) == 9) result = "Ninty Nine";
            }
            return result;
        }
        private void btnexit_Click(object sender, EventArgs e)
        {
            this.Dispose();
        }

    }
}
