﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace ACV
{
    public partial class FrmOpeningStock : Form
    {
        public FrmOpeningStock()
        {
            InitializeComponent();
        }
        SqlConnection conn = new SqlConnection("Data Source=" + Genclass.data2 + "; Initial Catalog=" + Genclass.data5 + ";User id=" + Genclass.data3 + ";Password=" + Genclass.data4 + "");
        SQLDBHelper db = new SQLDBHelper();
        int Mode = 1;
        int Fillid = 0;
        int SelectId = 0;
        BindingSource bsItem = new BindingSource();
        private void FrmOpeningStock_Load(object sender, EventArgs e)
        {
            LoadYear();
            LoadStock();
        }
        protected DataTable getItem()
        {
            DataTable dt = new DataTable();
            try
            {
                dt = db.GetDataWithoutParam(CommandType.StoredProcedure, "SP_GetItem", conn);
                bsItem.DataSource = dt;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            return dt;
        }
        protected   void LoadYear()
        {
            try
            {
                conn.Open();
                string qur = "Select * from yearmaster";
                SqlCommand cmd = new SqlCommand(qur, conn);
                SqlDataAdapter apt = new SqlDataAdapter(cmd);
                DataTable tab = new DataTable();
                apt.Fill(tab);
                cmbYear.DataSource = null;
                cmbYear.DisplayMember = "Yrtype";
                cmbYear.ValueMember = "Yearid";
                cmbYear.DataSource = tab;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void buttrqok_Click(object sender, EventArgs e)
        {
            try
            {
                decimal rate = 0;
                rate = Convert.ToDecimal(txtValue.Text) / Convert.ToDecimal(txtOpeningStock.Text);
                if (Mode == 1)
                {
                    SqlParameter[] para = {
                        new SqlParameter("@ItemUid",txtItemName.Tag),
                        new SqlParameter("@OpStkQty",txtOpeningStock.Text),
                        new SqlParameter("@OpStkValue",txtValue.Text),
                        new SqlParameter("@YearId",cmbYear.SelectedValue),
                        new SqlParameter("@CompanyId","1"),
                        new SqlParameter("@OpStkBags",txtBox.Text),
                        new SqlParameter("@OpStkReate",rate),
                        new SqlParameter("@OpStkUid","0")
                    };
                    db.ExecuteNonQuery(CommandType.StoredProcedure, "SP_Openingstock", para, conn);
                }
                else
                {
                    SqlParameter[] para = {
                        new SqlParameter("@ItemUid",txtItemName.Tag),
                        new SqlParameter("@OpStkQty",txtOpeningStock.Text),
                        new SqlParameter("@OpStkValue",txtValue.Text),
                        new SqlParameter("@YearId",cmbYear.SelectedValue),
                        new SqlParameter("@CompanyId","1"),
                        new SqlParameter("@OpStkBags",txtBox.Text),
                        new SqlParameter("@OpStkReate",rate),
                        new SqlParameter("@OpStkUid",txtBox.Tag)
                    };
                    db.ExecuteNonQuery(CommandType.StoredProcedure, "SP_Openingstock", para, conn);
                }
                MessageBox.Show("Record has been Saved", "Inforation", MessageBoxButtons.OK, MessageBoxIcon.Information);
                Genclass.Module.ClearTextBox(this, grFront);
                LoadStock();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }
        protected void LoadStock()
        {
            try
            {
                DataTable dt = db.GetDataWithoutParam(CommandType.StoredProcedure, "SP_GetOpeningStock", conn);
                DataGridOpeningStock.DataSource = null;
                DataGridOpeningStock.AutoGenerateColumns = false;
                DataGridOpeningStock.ColumnCount = 8;
                DataGridOpeningStock.Columns[0].Name = "OpStkUid";
                DataGridOpeningStock.Columns[0].HeaderText = "OpStkUid";
                DataGridOpeningStock.Columns[0].DataPropertyName = "OpStkUid";
                DataGridOpeningStock.Columns[0].Visible = false;

                DataGridOpeningStock.Columns[1].Name = "ItemUid";
                DataGridOpeningStock.Columns[1].HeaderText = "ItemUid";
                DataGridOpeningStock.Columns[1].DataPropertyName = "ItemUid";
                DataGridOpeningStock.Columns[1].Visible = false;

                DataGridOpeningStock.Columns[2].Name = "ItemName";
                DataGridOpeningStock.Columns[2].HeaderText = "ItemName";
                DataGridOpeningStock.Columns[2].DataPropertyName = "ItemName";
                DataGridOpeningStock.Columns[2].Width = 400;
                DataGridOpeningStock.Columns[3].Name = "OpStkQty";
                DataGridOpeningStock.Columns[3].HeaderText = "OpStkQty";
                DataGridOpeningStock.Columns[3].DataPropertyName = "OpStkQty";
                DataGridOpeningStock.Columns[3].Width = 150;
                DataGridOpeningStock.Columns[4].Name = "OpStkValue";
                DataGridOpeningStock.Columns[4].HeaderText = "OpStkValue";
                DataGridOpeningStock.Columns[4].DataPropertyName = "OpStkValue";
                DataGridOpeningStock.Columns[4].Width = 150;
                DataGridOpeningStock.Columns[5].Name = "OpStkBags";
                DataGridOpeningStock.Columns[5].HeaderText = "OpStkBags";
                DataGridOpeningStock.Columns[5].DataPropertyName = "OpStkBags";

                DataGridOpeningStock.Columns[6].Name = "OpStkReate";
                DataGridOpeningStock.Columns[6].HeaderText = "OpStkReate";
                DataGridOpeningStock.Columns[6].DataPropertyName = "OpStkReate";
                DataGridOpeningStock.Columns[6].Visible = false;
                DataGridOpeningStock.Columns[7].Name = "YearId";
                DataGridOpeningStock.Columns[7].HeaderText = "YearId";
                DataGridOpeningStock.Columns[7].DataPropertyName = "YearId";
                DataGridOpeningStock.Columns[7].Visible = false;
                DataGridOpeningStock.DataSource = dt;

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void txtItemName_MouseClick(object sender, MouseEventArgs e)
        {
            try
            {
                DataTable dt = getItem();
                FillGrid(dt, 1);
                Point p = Genclass.FindLocation(txtItemName);
                panelSearch.Location = new Point(p.X, p.Y + 20);
                panelSearch.Visible = true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        protected void FillGrid(DataTable dt, int FillId)
        {
            try
            {
                DataGridCommon.DataSource = null;
                DataGridCommon.AutoGenerateColumns = false;
                if (FillId == 1)
                {
                    Fillid = 1;
                    DataGridCommon.ColumnCount = 3;
                    DataGridCommon.Columns[0].Name = "Uid";
                    DataGridCommon.Columns[0].HeaderText = "Uid";
                    DataGridCommon.Columns[0].DataPropertyName = "Uid";
                    DataGridCommon.Columns[1].Name = "ItemName";
                    DataGridCommon.Columns[1].HeaderText = "ItemName";
                    DataGridCommon.Columns[1].DataPropertyName = "ItemName";
                    DataGridCommon.Columns[1].Width = 360;
                    DataGridCommon.DataSource = bsItem;
                    DataGridCommon.Columns[0].Visible = false;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void DataGridCommon_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            try
            {
                SelectId = 1;
                int Index = DataGridCommon.SelectedCells[0].RowIndex;
                if (Fillid == 1)
                {
                    txtItemName.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                    txtItemName.Tag = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                }
                panelSearch.Visible = false;
                SelectId = 0;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void btnHide_Click(object sender, EventArgs e)
        {
            panelSearch.Visible = false;
        }

        private void btnSelect_Click(object sender, EventArgs e)
        {
            try
            {
                SelectId = 1;
                int Index = DataGridCommon.SelectedCells[0].RowIndex;
                if (Fillid == 1)
                {
                    txtItemName.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                    txtItemName.Tag = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                }
                panelSearch.Visible = false;
                SelectId = 0;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void txtItemName_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (SelectId == 0)
                {
                    bsItem.Filter = string.Format("ItemName LIKE '%{0}%' ", txtItemName.Text);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void DataGridOpeningStock_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            try
            {
                DialogResult res = MessageBox.Show("Do you want edit opening stock ?", "Confirmation", MessageBoxButtons.YesNo, MessageBoxIcon.Information);
                if(res == DialogResult.Yes)
                {
                    int Index = DataGridOpeningStock.SelectedCells[0].RowIndex;
                    txtItemName.Text = "";
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }
    }
}
