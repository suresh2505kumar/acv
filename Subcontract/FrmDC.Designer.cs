﻿namespace Subcontract
{
    partial class FrmDC
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.Genpan = new System.Windows.Forms.Panel();
            this.txtscr5 = new System.Windows.Forms.TextBox();
            this.txtscr4 = new System.Windows.Forms.TextBox();
            this.Txtscr3 = new System.Windows.Forms.TextBox();
            this.Txtscr2 = new System.Windows.Forms.TextBox();
            this.btnexit = new System.Windows.Forms.Button();
            this.btndelete = new System.Windows.Forms.Button();
            this.btnedit = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.txtscr1 = new System.Windows.Forms.TextBox();
            this.HFGP = new System.Windows.Forms.DataGridView();
            this.editpnl = new System.Windows.Forms.Panel();
            this.btnsave = new System.Windows.Forms.Button();
            this.btnaddrcan = new System.Windows.Forms.Button();
            this.HFIT = new System.Windows.Forms.DataGridView();
            this.btnadd = new System.Windows.Forms.Button();
            this.label12 = new System.Windows.Forms.Label();
            this.txtaddnotes = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.txtqty = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.txtitem = new System.Windows.Forms.TextBox();
            this.Dtpdt = new System.Windows.Forms.DateTimePicker();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.txtdcno = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtgrndt = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtnar = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtname = new System.Windows.Forms.TextBox();
            this.Phone = new System.Windows.Forms.Label();
            this.txtgrn = new System.Windows.Forms.TextBox();
            this.txtititd = new System.Windows.Forms.TextBox();
            this.txtpuid = new System.Windows.Forms.TextBox();
            this.txtgrnno = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.txtgrnid = new System.Windows.Forms.TextBox();
            this.Genpan.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.HFGP)).BeginInit();
            this.editpnl.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.HFIT)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Bookman Old Style", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(12, 10);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(95, 18);
            this.label1.TabIndex = 194;
            this.label1.Text = "DC Process";
            // 
            // Genpan
            // 
            this.Genpan.Controls.Add(this.txtscr5);
            this.Genpan.Controls.Add(this.txtscr4);
            this.Genpan.Controls.Add(this.Txtscr3);
            this.Genpan.Controls.Add(this.Txtscr2);
            this.Genpan.Controls.Add(this.btnexit);
            this.Genpan.Controls.Add(this.btndelete);
            this.Genpan.Controls.Add(this.btnedit);
            this.Genpan.Controls.Add(this.button1);
            this.Genpan.Controls.Add(this.txtscr1);
            this.Genpan.Controls.Add(this.HFGP);
            this.Genpan.Location = new System.Drawing.Point(28, 31);
            this.Genpan.Name = "Genpan";
            this.Genpan.Size = new System.Drawing.Size(1061, 522);
            this.Genpan.TabIndex = 222;
            this.Genpan.Paint += new System.Windows.Forms.PaintEventHandler(this.Genpan_Paint);
            // 
            // txtscr5
            // 
            this.txtscr5.Location = new System.Drawing.Point(857, 13);
            this.txtscr5.Margin = new System.Windows.Forms.Padding(4);
            this.txtscr5.Name = "txtscr5";
            this.txtscr5.Size = new System.Drawing.Size(200, 20);
            this.txtscr5.TabIndex = 90;
            // 
            // txtscr4
            // 
            this.txtscr4.Location = new System.Drawing.Point(660, 13);
            this.txtscr4.Margin = new System.Windows.Forms.Padding(4);
            this.txtscr4.Name = "txtscr4";
            this.txtscr4.Size = new System.Drawing.Size(200, 20);
            this.txtscr4.TabIndex = 89;
            // 
            // Txtscr3
            // 
            this.Txtscr3.Location = new System.Drawing.Point(350, 13);
            this.Txtscr3.Margin = new System.Windows.Forms.Padding(4);
            this.Txtscr3.Name = "Txtscr3";
            this.Txtscr3.Size = new System.Drawing.Size(128, 20);
            this.Txtscr3.TabIndex = 88;
            // 
            // Txtscr2
            // 
            this.Txtscr2.Location = new System.Drawing.Point(206, 13);
            this.Txtscr2.Margin = new System.Windows.Forms.Padding(4);
            this.Txtscr2.Name = "Txtscr2";
            this.Txtscr2.Size = new System.Drawing.Size(147, 20);
            this.Txtscr2.TabIndex = 87;
            // 
            // btnexit
            // 
            this.btnexit.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnexit.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnexit.Image = global::Subcontract.Properties.Resources.eee;
            this.btnexit.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnexit.Location = new System.Drawing.Point(968, 334);
            this.btnexit.Name = "btnexit";
            this.btnexit.Size = new System.Drawing.Size(90, 39);
            this.btnexit.TabIndex = 86;
            this.btnexit.Text = "EXIT";
            this.btnexit.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnexit.UseVisualStyleBackColor = false;
            this.btnexit.Click += new System.EventHandler(this.btnexit_Click);
            // 
            // btndelete
            // 
            this.btndelete.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btndelete.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btndelete.Image = global::Subcontract.Properties.Resources.exit8;
            this.btndelete.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btndelete.Location = new System.Drawing.Point(968, 273);
            this.btndelete.Name = "btndelete";
            this.btndelete.Size = new System.Drawing.Size(90, 38);
            this.btndelete.TabIndex = 85;
            this.btndelete.Text = "DELETE";
            this.btndelete.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btndelete.UseVisualStyleBackColor = false;
            // 
            // btnedit
            // 
            this.btnedit.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnedit.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnedit.Image = global::Subcontract.Properties.Resources.edit;
            this.btnedit.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.btnedit.Location = new System.Drawing.Point(968, 210);
            this.btnedit.Name = "btnedit";
            this.btnedit.Size = new System.Drawing.Size(90, 43);
            this.btnedit.TabIndex = 84;
            this.btnedit.Text = "EDIT";
            this.btnedit.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnedit.UseVisualStyleBackColor = false;
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.button1.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.Image = global::Subcontract.Properties.Resources.Add;
            this.button1.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.button1.Location = new System.Drawing.Point(968, 146);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(90, 42);
            this.button1.TabIndex = 83;
            this.button1.Text = "ADD";
            this.button1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // txtscr1
            // 
            this.txtscr1.Location = new System.Drawing.Point(55, 13);
            this.txtscr1.Margin = new System.Windows.Forms.Padding(4);
            this.txtscr1.Name = "txtscr1";
            this.txtscr1.Size = new System.Drawing.Size(149, 20);
            this.txtscr1.TabIndex = 1;
            // 
            // HFGP
            // 
            this.HFGP.BackgroundColor = System.Drawing.Color.White;
            this.HFGP.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.HFGP.Location = new System.Drawing.Point(16, 41);
            this.HFGP.Name = "HFGP";
            this.HFGP.Size = new System.Drawing.Size(939, 451);
            this.HFGP.TabIndex = 3;
            // 
            // editpnl
            // 
            this.editpnl.Controls.Add(this.btnsave);
            this.editpnl.Controls.Add(this.btnaddrcan);
            this.editpnl.Controls.Add(this.HFIT);
            this.editpnl.Controls.Add(this.btnadd);
            this.editpnl.Controls.Add(this.label12);
            this.editpnl.Controls.Add(this.txtaddnotes);
            this.editpnl.Controls.Add(this.label10);
            this.editpnl.Controls.Add(this.txtqty);
            this.editpnl.Controls.Add(this.label7);
            this.editpnl.Controls.Add(this.txtitem);
            this.editpnl.Controls.Add(this.Dtpdt);
            this.editpnl.Controls.Add(this.label6);
            this.editpnl.Controls.Add(this.label5);
            this.editpnl.Controls.Add(this.txtdcno);
            this.editpnl.Controls.Add(this.label2);
            this.editpnl.Controls.Add(this.txtgrndt);
            this.editpnl.Controls.Add(this.label4);
            this.editpnl.Controls.Add(this.txtnar);
            this.editpnl.Controls.Add(this.label3);
            this.editpnl.Controls.Add(this.txtname);
            this.editpnl.Controls.Add(this.Phone);
            this.editpnl.Controls.Add(this.txtgrn);
            this.editpnl.Controls.Add(this.txtititd);
            this.editpnl.Controls.Add(this.txtpuid);
            this.editpnl.Controls.Add(this.txtgrnno);
            this.editpnl.Controls.Add(this.label11);
            this.editpnl.Controls.Add(this.txtgrnid);
            this.editpnl.Location = new System.Drawing.Point(28, 31);
            this.editpnl.Name = "editpnl";
            this.editpnl.Size = new System.Drawing.Size(1061, 459);
            this.editpnl.TabIndex = 223;
            // 
            // btnsave
            // 
            this.btnsave.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnsave.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnsave.Image = global::Subcontract.Properties.Resources.save;
            this.btnsave.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnsave.Location = new System.Drawing.Point(942, 219);
            this.btnsave.Name = "btnsave";
            this.btnsave.Size = new System.Drawing.Size(90, 38);
            this.btnsave.TabIndex = 243;
            this.btnsave.Text = "Save";
            this.btnsave.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnsave.UseVisualStyleBackColor = false;
            this.btnsave.Click += new System.EventHandler(this.btnsave_Click);
            // 
            // btnaddrcan
            // 
            this.btnaddrcan.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnaddrcan.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnaddrcan.Image = global::Subcontract.Properties.Resources.can2;
            this.btnaddrcan.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnaddrcan.Location = new System.Drawing.Point(942, 272);
            this.btnaddrcan.Name = "btnaddrcan";
            this.btnaddrcan.Size = new System.Drawing.Size(93, 38);
            this.btnaddrcan.TabIndex = 244;
            this.btnaddrcan.Text = "Back";
            this.btnaddrcan.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnaddrcan.UseVisualStyleBackColor = false;
            this.btnaddrcan.Click += new System.EventHandler(this.btnaddrcan_Click);
            // 
            // HFIT
            // 
            this.HFIT.BackgroundColor = System.Drawing.Color.White;
            this.HFIT.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.HFIT.Location = new System.Drawing.Point(14, 154);
            this.HFIT.Name = "HFIT";
            this.HFIT.Size = new System.Drawing.Size(910, 289);
            this.HFIT.TabIndex = 242;
            // 
            // btnadd
            // 
            this.btnadd.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnadd.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnadd.Image = global::Subcontract.Properties.Resources.ok1_25x25;
            this.btnadd.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.btnadd.Location = new System.Drawing.Point(905, 119);
            this.btnadd.Name = "btnadd";
            this.btnadd.Size = new System.Drawing.Size(34, 32);
            this.btnadd.TabIndex = 241;
            this.btnadd.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnadd.UseVisualStyleBackColor = false;
            this.btnadd.Click += new System.EventHandler(this.btnadd_Click);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(532, 100);
            this.label12.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(77, 18);
            this.label12.TabIndex = 240;
            this.label12.Text = "Add Notes";
            // 
            // txtaddnotes
            // 
            this.txtaddnotes.Location = new System.Drawing.Point(535, 125);
            this.txtaddnotes.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtaddnotes.Name = "txtaddnotes";
            this.txtaddnotes.Size = new System.Drawing.Size(300, 20);
            this.txtaddnotes.TabIndex = 239;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(447, 102);
            this.label10.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(31, 18);
            this.label10.TabIndex = 238;
            this.label10.Text = "Qty";
            // 
            // txtqty
            // 
            this.txtqty.Location = new System.Drawing.Point(450, 125);
            this.txtqty.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtqty.Name = "txtqty";
            this.txtqty.Size = new System.Drawing.Size(66, 20);
            this.txtqty.TabIndex = 237;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(26, 102);
            this.label7.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(36, 18);
            this.label7.TabIndex = 235;
            this.label7.Text = "Item";
            // 
            // txtitem
            // 
            this.txtitem.Location = new System.Drawing.Point(29, 125);
            this.txtitem.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtitem.Name = "txtitem";
            this.txtitem.Size = new System.Drawing.Size(416, 20);
            this.txtitem.TabIndex = 234;
            this.txtitem.TextChanged += new System.EventHandler(this.txtitem_TextChanged);
            this.txtitem.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtitem_KeyDown);
            // 
            // Dtpdt
            // 
            this.Dtpdt.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Dtpdt.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.Dtpdt.Location = new System.Drawing.Point(419, 25);
            this.Dtpdt.Name = "Dtpdt";
            this.Dtpdt.Size = new System.Drawing.Size(109, 22);
            this.Dtpdt.TabIndex = 233;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(416, 5);
            this.label6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(62, 18);
            this.label6.TabIndex = 232;
            this.label6.Text = "Dc Date";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(286, 5);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(51, 18);
            this.label5.TabIndex = 231;
            this.label5.Text = "Dc No";
            // 
            // txtdcno
            // 
            this.txtdcno.Location = new System.Drawing.Point(289, 28);
            this.txtdcno.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtdcno.Name = "txtdcno";
            this.txtdcno.Size = new System.Drawing.Size(122, 20);
            this.txtdcno.TabIndex = 230;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(156, 5);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(65, 18);
            this.label2.TabIndex = 229;
            this.label2.Text = "DC Date";
            // 
            // txtgrndt
            // 
            this.txtgrndt.Location = new System.Drawing.Point(159, 28);
            this.txtgrndt.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtgrndt.Name = "txtgrndt";
            this.txtgrndt.Size = new System.Drawing.Size(122, 20);
            this.txtgrndt.TabIndex = 228;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(358, 54);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(69, 18);
            this.label4.TabIndex = 225;
            this.label4.Text = "Narration";
            // 
            // txtnar
            // 
            this.txtnar.Location = new System.Drawing.Point(361, 77);
            this.txtnar.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtnar.Name = "txtnar";
            this.txtnar.Size = new System.Drawing.Size(328, 20);
            this.txtnar.TabIndex = 224;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(26, 54);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(86, 18);
            this.label3.TabIndex = 223;
            this.label3.Text = "Party Name";
            // 
            // txtname
            // 
            this.txtname.Location = new System.Drawing.Point(29, 77);
            this.txtname.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtname.Name = "txtname";
            this.txtname.Size = new System.Drawing.Size(324, 20);
            this.txtname.TabIndex = 222;
            this.txtname.TextChanged += new System.EventHandler(this.txtname_TextChanged);
            this.txtname.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtname_KeyDown);
            // 
            // Phone
            // 
            this.Phone.AutoSize = true;
            this.Phone.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Phone.Location = new System.Drawing.Point(34, 5);
            this.Phone.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.Phone.Name = "Phone";
            this.Phone.Size = new System.Drawing.Size(57, 18);
            this.Phone.TabIndex = 227;
            this.Phone.Text = "DC NO";
            // 
            // txtgrn
            // 
            this.txtgrn.Location = new System.Drawing.Point(29, 28);
            this.txtgrn.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtgrn.Name = "txtgrn";
            this.txtgrn.Size = new System.Drawing.Size(122, 20);
            this.txtgrn.TabIndex = 226;
            // 
            // txtititd
            // 
            this.txtititd.Location = new System.Drawing.Point(44, 126);
            this.txtititd.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtititd.Name = "txtititd";
            this.txtititd.Size = new System.Drawing.Size(51, 20);
            this.txtititd.TabIndex = 236;
            // 
            // txtpuid
            // 
            this.txtpuid.Location = new System.Drawing.Point(29, 77);
            this.txtpuid.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtpuid.Name = "txtpuid";
            this.txtpuid.Size = new System.Drawing.Size(66, 20);
            this.txtpuid.TabIndex = 245;
            // 
            // txtgrnno
            // 
            this.txtgrnno.Location = new System.Drawing.Point(535, 258);
            this.txtgrnno.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtgrnno.Name = "txtgrnno";
            this.txtgrnno.Size = new System.Drawing.Size(122, 20);
            this.txtgrnno.TabIndex = 247;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(540, 235);
            this.label11.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(69, 18);
            this.label11.TabIndex = 246;
            this.label11.Text = "DCW No";
            // 
            // txtgrnid
            // 
            this.txtgrnid.Location = new System.Drawing.Point(587, 210);
            this.txtgrnid.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtgrnid.Name = "txtgrnid";
            this.txtgrnid.Size = new System.Drawing.Size(22, 20);
            this.txtgrnid.TabIndex = 248;
            // 
            // FrmDC
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1101, 565);
            this.Controls.Add(this.Genpan);
            this.Controls.Add(this.editpnl);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "FrmDC";
            this.Text = "FrmDC";
            this.Load += new System.EventHandler(this.FrmDC_Load);
            this.Genpan.ResumeLayout(false);
            this.Genpan.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.HFGP)).EndInit();
            this.editpnl.ResumeLayout(false);
            this.editpnl.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.HFIT)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel Genpan;
        private System.Windows.Forms.TextBox txtscr5;
        private System.Windows.Forms.TextBox txtscr4;
        private System.Windows.Forms.TextBox Txtscr3;
        private System.Windows.Forms.TextBox Txtscr2;
        private System.Windows.Forms.Button btnexit;
        private System.Windows.Forms.Button btndelete;
        private System.Windows.Forms.Button btnedit;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox txtscr1;
        private System.Windows.Forms.DataGridView HFGP;
        private System.Windows.Forms.Panel editpnl;
        private System.Windows.Forms.Button btnsave;
        private System.Windows.Forms.Button btnaddrcan;
        private System.Windows.Forms.DataGridView HFIT;
        private System.Windows.Forms.Button btnadd;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox txtaddnotes;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox txtqty;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtitem;
        private System.Windows.Forms.DateTimePicker Dtpdt;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtdcno;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtgrndt;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtnar;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtname;
        private System.Windows.Forms.Label Phone;
        private System.Windows.Forms.TextBox txtgrn;
        private System.Windows.Forms.TextBox txtititd;
        private System.Windows.Forms.TextBox txtpuid;
        private System.Windows.Forms.TextBox txtgrnno;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox txtgrnid;
    }
}