﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Drawing.Imaging;
using System.Configuration;
using System.Data.SqlClient;
using System.Security.Cryptography;
namespace ACV
{
    public partial class Frmprodrecpt : Form
    {
        public Frmprodrecpt()
        {
            CenterToParent();
            this.BackColor = Color.White;
            InitializeComponent();
        }
        string uid = "";
        int mode = 0;
        SqlConnection conn = new SqlConnection("Data Source=" + Genclass.data2 + "; Initial Catalog=" + Genclass.data5 + ";User id=" + Genclass.data3 + ";Password=" + Genclass.data4 + "");
        SqlCommand qur = new SqlCommand();

        private void Frmprodrecpt_Load(object sender, EventArgs e)
        {
            qur.Connection = conn;
            Left = (MdiParent.ClientRectangle.Width - Width) / 3;
            Top = (MdiParent.ClientRectangle.Height - Height) / 3;

            this.HFGP.DefaultCellStyle.Font = new Font("Arial", 10);
            this.HFGP.ColumnHeadersDefaultCellStyle.Font = new Font("Arial", 10, FontStyle.Bold);
            this.HFIT.DefaultCellStyle.Font = new Font("Arial", 10);
            this.HFIT.ColumnHeadersDefaultCellStyle.Font = new Font("Arial", 10, FontStyle.Bold);

            HFGP.RowHeadersVisible = false;

            HFIT.RowHeadersVisible = false;

            HFGP.AllowUserToAddRows = true;

            Genpan.Visible = true;
            Editpnl.Visible = false;

            chkact.Checked = true;
            chkedtact.Checked = true;
            Loadgrid();
            Titlep();
            Genclass.Module.buttonstyleform(this);
            Genclass.Module.buttonstylepanel(panadd);
            panadd.Visible = true;
            HFGP.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            HFGP.EnableHeadersVisualStyles = false;
            HFGP.ColumnHeadersDefaultCellStyle.BackColor = SystemColors.GradientActiveCaption;
            Reqbk.Visible = false;
            button3.Visible = false;
            HFGP.Focus();
        }

        private void Loadgrid()
        {
            try
            {
                conn.Open();

                Genclass.StrSrch = "";


                Genclass.FSSQLSortStr = "Docno";
                Genclass.FSSQLSortStr1 = "Docdate";
                Genclass.FSSQLSortStr2 = "Name";
                Genclass.FSSQLSortStr3 = "Itemname";
                Genclass.FSSQLSortStr4 = "pQty";




                if (txtscr1.Text != "")
                {
                    if (Genclass.StrSrch == "")
                    {
                        Genclass.StrSrch = Genclass.FSSQLSortStr + " like '%" + txtscr1.Text + "%'";
                    }
                    else
                    {
                        Genclass.StrSrch = Genclass.StrSrch + " and " + Genclass.FSSQLSortStr + " like '%" + txtscr1.Text + "%'";
                    }

                }

                if (Txtscr2.Text != "")
                {
                    if (Genclass.StrSrch == "")
                    {
                        Genclass.StrSrch = Genclass.FSSQLSortStr1 + " like '%" + Txtscr2.Text + "%'";
                    }
                    else
                    {
                        Genclass.StrSrch = Genclass.StrSrch + " and " + Genclass.FSSQLSortStr1 + " like '%" + Txtscr2.Text + "%'";
                    }

                }

                if (Txtscr3.Text != "")
                {
                    if (Genclass.StrSrch == "")
                    {
                        Genclass.StrSrch = Genclass.FSSQLSortStr2 + " like '%" + Txtscr3.Text + "%'";
                    }
                    else
                    {
                        Genclass.StrSrch = Genclass.StrSrch + " and " + Genclass.FSSQLSortStr2 + " like '%" + Txtscr3.Text + "%'";
                    }

                }

                if (txtscr4.Text != "")
                {
                    if (Genclass.StrSrch == "")
                    {
                        Genclass.StrSrch = Genclass.FSSQLSortStr3 + " like '%" + txtscr4.Text + "%'";
                    }
                    else
                    {
                        Genclass.StrSrch = Genclass.StrSrch + " and " + Genclass.FSSQLSortStr3 + " like '%" + txtscr4.Text + "%'";
                    }

                }

                if (txtscr5.Text != "")
                {
                    if (Genclass.StrSrch == "")
                    {
                        Genclass.StrSrch = Genclass.FSSQLSortStr4 + " like '%" + txtscr5.Text + "%'";
                    }
                    else
                    {
                        Genclass.StrSrch = Genclass.StrSrch + " and " + Genclass.FSSQLSortStr4 + " like '%" + txtscr5.Text + "%'";
                    }

                }



                if (txtscr1.Text != "")
                {
                    Genclass.StrSrch = " " + Genclass.StrSrch;
                }
                else if (Txtscr2.Text != "")
                {
                    Genclass.StrSrch = " " + Genclass.StrSrch;
                }

                else if (Txtscr3.Text != "")
                {
                    Genclass.StrSrch = " " + Genclass.StrSrch;
                }
                else if (txtscr4.Text != "")
                {
                    Genclass.StrSrch = " " + Genclass.StrSrch;
                }
                else if (txtscr5.Text != "")
                {
                    Genclass.StrSrch = " " + Genclass.StrSrch;
                }
                else
                {
                    Genclass.StrSrch = "a.uid <> 0";
                }

                if (chkact.Checked == true)
                {
                    string quy = "select a.UId,DocNo,DocDate,Name as Party,c.itemname,adduty as Qty,a.partyuid,c.uid,a.active,d.generalname,d.uid,e.processname,e.uid,f.generalname,f.uid,a.narration,a.jjno,z.Rejrem,z.Rewrem,accqty,rewqty,rejqty from TransactionsP a inner join PartyM b on a.PartyUid=b.uid inner join itemm c on a.adddutyuid=c.uid left join generalm d on a.pfamount=d.uid left join processm e on a.roff=e.uid left join generalm f on a.netvalue=f.uid left join RejReason z on a.uid=z.Tplistuid where a.active=1 and a.doctypeid=" + Genclass.Dtype + " and a.companyid=" + Genclass.data1 + " and  " + Genclass.StrSrch + "";
                    Genclass.cmd = new SqlCommand(quy, conn);
                }
                else
                {
                    string quy = "select a.UId,DocNo,DocDate,Name as Party,c.itemname,adduty as Qty,a.partyuid,c.uid,a.active,d.generalname,d.uid,e.processname,e.uid,f.generalname,f.uid,a.narration,a.jjno,z.Rejrem,z.Rewrem,accqty,rewqty,rejqty from TransactionsP a inner join PartyM b on a.PartyUid=b.uid inner join itemm c on a.adddutyuid=c.uid left join generalm d on a.pfamount=d.uid left join processm e on a.roff=e.uid left join generalm f on a.netvalue=f.uid left join RejReason z on a.uid=z.Tplistuid  where a.active=0 and a.doctypeid=" + Genclass.Dtype + " and a.companyid=" + Genclass.data1 + " and  " + Genclass.StrSrch + "";
                    Genclass.cmd = new SqlCommand(quy, conn);
                }

                SqlDataAdapter aptr = new SqlDataAdapter(Genclass.cmd);
                DataTable tap = new DataTable();
                aptr.Fill(tap);

                this.HFGP.DefaultCellStyle.Font = new Font("Arial", 10);
                //dgv.ColumnHeadersDefaultCellStyle.Font = new Font(dgv.ColumnHeadersDefaultCellStyle.Font, FontStyle.Bold);
                this.HFGP.ColumnHeadersDefaultCellStyle.Font = new Font("Arial", 10, FontStyle.Bold);
                HFGP.AutoGenerateColumns = false;
                HFGP.Refresh();
                HFGP.DataSource = null;
                HFGP.Rows.Clear();


                HFGP.ColumnCount = tap.Columns.Count;
                Genclass.i = 0;
                foreach (DataColumn column in tap.Columns)
                {
                    HFGP.Columns[Genclass.i].Name = column.ColumnName;
                    HFGP.Columns[Genclass.i].HeaderText = column.ColumnName;
                    HFGP.Columns[Genclass.i].DataPropertyName = column.ColumnName;
                    Genclass.i = Genclass.i + 1;
                }



                HFGP.Columns[0].Visible = false;

                HFGP.Columns[1].Width = 88;
                HFGP.Columns[2].Width = 90;
                HFGP.Columns[3].Width = 280;
                HFGP.Columns[4].Width = 280;
                HFGP.Columns[5].Width = 100;
                HFGP.Columns[6].Visible = false;
                HFGP.Columns[7].Visible = false;
                HFGP.Columns[8].Visible = false;
                HFGP.Columns[9].Visible = false;
                HFGP.Columns[10].Visible = false;
                HFGP.Columns[11].Visible = false;
                HFGP.Columns[12].Visible = false;
                HFGP.Columns[13].Visible = false;
                HFGP.Columns[14].Visible = false;
                HFGP.Columns[15].Visible = false;
                HFGP.Columns[16].Visible = false;
                HFGP.Columns[17].Visible = false;
                HFGP.Columns[18].Visible = false;
                HFGP.Columns[19].Visible = false;
                HFGP.Columns[20].Visible = false;
                HFGP.Columns[21].Visible = false;

                //int ct = tap.Rows.Count;
                HFGP.DataSource = tap;

                HFGP.Columns[2].DefaultCellStyle.Format = "yyyy'/'MM'/'dd";
                //HFGP.Columns[4].DefaultCellStyle.Format = "yyyy'/'MM'/'dd";

                int index = HFGP.Rows.Count - 1;

                if (HFGP.Rows[0].Cells[1].Value.ToString() == "" || HFGP.Rows[0].Cells[1].Value == null)
                {
                    lblno1.Text = "0";
                }
                else
                {
                    lblno1.Text = "1";
                }


                lblno2.Text = "of " + index.ToString();


            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return;
            }
            finally
            {
                conn.Close();
                Genclass.cmd.Dispose();
            }
        }

        private void Titlep()
        {
            //HFIT.ReadOnly = false;
            HFIT.ColumnCount = 5;

            HFIT.Columns[0].Name = "Itemname";

            HFIT.Columns[1].Name = "Qty";
            //HFIT.Columns[1].ReadOnly = false;

            HFIT.Columns[2].Name = "Grnno";
            HFIT.Columns[3].Name = "itemid";
            HFIT.Columns[4].Name = "Refid";
            HFIT.Columns[0].Width = 450;
            HFIT.Columns[1].Width = 75;

            HFIT.Columns[2].Width = 100;
            HFIT.Columns[3].Visible = false;
            HFIT.Columns[4].Visible = false;
            //HFIT.EditMode = DataGridViewEditMode.EditOnKeystroke;
            //HFIT.Columns[1].ReadOnly = false;

        }

        private void button1_Click(object sender, EventArgs e)
        {
            mode = 1;

            Genclass.Module.ClearTextBox(this, Editpnl);
            txtrej.Text = "0";
            txtrew.Text = "0";
            txtacc.Text = "0";
            Genclass.Module.Gendocno();
            Genpan.Visible = false;
            Editpnl.Visible = true;

            HFIT.Refresh();
            HFIT.DataSource = null;
            HFIT.Rows.Clear();

            txtdocno.Text = Genclass.ST;
            DateTime d = new DateTime();
            d = DateTime.Now;
            DTPDOCDT.Text = d.ToString("dd.MM.yyyy");
            Titlep();
            txtmac.Focus();
        }

        private void txtname_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                Genclass.type = 1;
                loadput();
            }
            else if (e.KeyCode == Keys.Escape)
            {
                txtname.Text = "";
                txtname.Focus();
            }

        }

        private void loadput()
        {
            conn.Open();
            Genclass.fieldone = "";
            Genclass.fieldtwo = "";
            Genclass.fieldthree = "";
            Genclass.fieldFour = "";
            Genclass.fieldFive = "";

            if (Genclass.type == 1)
            {
                Genclass.strsql = "select uid,Name as Party from Partym where active=1 and  companyid=" + Genclass.data1 + "";
                Genclass.Module.Partylistviewcont("uid", "Name", Genclass.strsql, this, txtpuid, txtname, Editpnl);

                Genclass.FSSQLSortStr = "Name";

            }

            else if (Genclass.type == 2)
            {
                Genclass.strsql = "select uid,ItemName,min(qty) as avqty,min(qty) as avqty1 from (select UsedItem_UId,aa.uid,itemname,convert(int,((isnull(aa.pqty,0)-isnull(bb.qty,0)-isnull(cc.qty,0))/UsedQty)) as qty from(select a.UsedItem_UId,z.Uid,z.ItemName,a.usedqty,isnull(sum(c.pqty),0) as pqty from itemsmbom a inner join itemm b on a.UsedItem_UId=b.uid and a.companyid=" + Genclass.data1 + " and b.partyuid=" + txtpuid.Text + " and b.active=1  inner join ItemM z on a.ItemM_UId=z.uid left join TransactionsPList c on b.Uid=c.ItemUId and c.DocTypeID=10 group by a.UsedItem_UId,z.Uid,z.ItemName,a.usedqty)aa left join (select itemid,isnull(sum(qty),0) as qty from mfg_stock_ledger where trantype='Issue' and doctypeid=20 group by itemid)bb on aa.UsedItem_UId=bb.itemid left join (select itemid,isnull(sum(qty),0) as qty from mfg_stock_ledger where trantype='Issue' and doctypeid=50 group by itemid)cc on aa.UsedItem_UId=cc.itemid)tab group by uid,itemname having min(qty)>0";


                //where active=1 and companyid=" + Genclass.data1 + "";
                Genclass.Module.Partylistviewcont2("uid", "Item", "Avlqty", "Avlqty1", Genclass.strsql, this, txtititd, txtitem, txtqty, txthqty, Editpnl);

                Genclass.FSSQLSortStr = "itemname";
            }
            else if (Genclass.type == 3)
            {
                Genclass.strsql = "select distinct  c.itemname,b.pqty-isnull(sum(d.pqty),0)-isnull(sum(e.pqty),0) as pqty,a.docno,c.uid,b.uid from transactionsp a inner join transactionsplist b on  a.uid=b.transactionspuid and a.doctypeid=10 and a.companyid=" + Genclass.data1 + " and a.partyuid=" + txtpuid.Text + " LEFT JOIN	ITEMSMBOM z ON b.ItemUId=z.UsedItem_UId and z.itemm_uid=" + txtititd.Text + "  inner join itemm c on b.itemuid=c.uid left join transactionsplist d on  b.uid=d.refuid  and b.itemuid=d.itemuid and d.doctypeid=20  left join transactionsplist e on  b.uid=e.refuid and e.doctypeid=50  group by c.itemname,b.pqty,a.docno,c.uid,b.uid having b.pqty-isnull(sum(d.pqty),0)-isnull(sum(e.pqty),0) >0";

                Genclass.Module.Partylistviewcont1("Item", "Qty", "Grnno", "itid", "uid", Genclass.strsql, this, txtit, txtqty, txtgrnno, txtid, txtgrnid, Editpnl);
                Genclass.FSSQLSortStr = "c.itemname";
            }
            else if (Genclass.type == 4)
            {
                Genclass.strsql = "select uid,Generalname as Machine from Generalm where active=1 and typem_uid=10 and companyid=" + Genclass.data1 + "";
                Genclass.Module.Partylistviewcont("uid", "Machine", Genclass.strsql, this, txtmcid, txtmac, Editpnl);

                Genclass.FSSQLSortStr = "Generalname";

            }
            else if (Genclass.type == 5)
            {
                Genclass.strsql = "select uid,Processname as Process from Processm where active=1 and  companyid=" + Genclass.data1 + "";
                Genclass.Module.Partylistviewcont("uid", "Process", Genclass.strsql, this, txtopid, txtop, Editpnl);

                Genclass.FSSQLSortStr = "Processname";

            }
            else if (Genclass.type == 6)
            {
                Genclass.strsql = "select uid,Generalname as Employee from Generalm where active=1 and typem_uid=11 and  companyid=" + Genclass.data1 + "";
                Genclass.Module.Partylistviewcont("uid", "Employee", Genclass.strsql, this, txtempid, txtemp, Editpnl);

                Genclass.FSSQLSortStr = "Generalname";

            }


            Genclass.cmd = new SqlCommand(Genclass.strsql, conn);
            SqlDataAdapter aptr = new SqlDataAdapter(Genclass.cmd);
            DataTable tap = new DataTable();
            aptr.Fill(tap);

            if (tap.Rows.Count == 0)
            {
                MessageBox.Show("No Records");
                return;
            }
            Frmlookup contc = new Frmlookup();
            DataGridView dt = (DataGridView)contc.Controls["Hfgp"];
            dt.Refresh();
            dt.ColumnCount = tap.Columns.Count;

            if (Genclass.type == 3)
            {
                dt.Columns[0].Width = 350;
                dt.Columns[1].Width = 90;
                dt.Columns[2].Width = 90;
                dt.Columns[3].Visible = false;
                dt.Columns[4].Visible = false;
            }
            else if (Genclass.type == 2)
            {
                dt.Columns[0].Visible = false;
                dt.Columns[1].Width = 350;
                dt.Columns[2].Width = 90;
                dt.Columns[3].Visible = false;

            }
            else
            {
                dt.Columns[0].Visible = false;
                dt.Columns[1].Width = 400;
            }


            dt.DefaultCellStyle.Font = new Font("Arial", 10);

            dt.ColumnHeadersDefaultCellStyle.Font = new Font("Arial", 10, FontStyle.Bold);
            dt.AutoGenerateColumns = false;

            Genclass.i = 0;
            foreach (DataColumn column in tap.Columns)
            {
                dt.Columns[Genclass.i].Name = column.ColumnName;
                dt.Columns[Genclass.i].HeaderText = column.ColumnName;
                dt.Columns[Genclass.i].DataPropertyName = column.ColumnName;
                Genclass.i = Genclass.i + 1;
            }

            dt.DataSource = tap;
            contc.Show();
            conn.Close();

        }

        private void txtitem_KeyDown(object sender, KeyEventArgs e)
        {
            if (txtpuid.Text == "")
            {
                MessageBox.Show("Select the Party");
                return;
            }

            if (e.KeyCode == Keys.Enter)
            {
                Genclass.type = 2;
                loadput();
            }
            else if (e.KeyCode == Keys.Escape)
            {
                txtitem.Text = "";
                txtitem.Focus();
            }
        }

        private void txtgrnno_KeyDown(object sender, KeyEventArgs e)
        {

            if (e.KeyCode == Keys.Enter)
            {
                Genclass.type = 3;
                loadput();
            }
            else if (e.KeyCode == Keys.Escape)
            {
                txtgrnno.Text = "";
                txtgrnid.Text = "";
                txtitem.Focus();
            }

        }


        private void btnaddrcan_Click(object sender, EventArgs e)
        {
            Editpnl.Visible = false;
            Genpan.Visible = true;
            panadd.Visible = true;
        }

        private void btnexit_Click(object sender, EventArgs e)
        {
            this.Dispose();
        }



        private void txtit_TextChanged(object sender, EventArgs e)
        {
            if (txtit.Text != "")
            {
                var index = HFIT.Rows.Add();
                HFIT.Rows[index].Cells[0].Value = txtit.Text;
                HFIT.Rows[index].Cells[1].Value = txtqty.Text;
                HFIT.Rows[index].Cells[2].Value = txtgrnno.Text;
                HFIT.Rows[index].Cells[3].Value = txtid.Text;
                HFIT.Rows[index].Cells[4].Value = txtgrnid.Text;
                txtgrnid.Text = "";
                txtgrnno.Text = "";
            }


        }

        //private void btnsave_Click(object sender, EventArgs e)
        //{
        //    if (txtname.Text == "")
        //    {
        //        MessageBox.Show("Enter the Party");
        //        txtname.Focus();
        //        return;
        //    }

        //    if (txtitem.Text == "")
        //    {
        //        MessageBox.Show("Enter the Item");
        //        txtitem.Focus();
        //        return;
        //    }

        //    conn.Open();

        //    for (int i = 0; i < HFIT.Rows.Count - 1; i++)
        //    {
        //        if (mode == 1)
        //        {
        //            qur.CommandText = "Exec Sp_Transactions " + Genclass.Dtype + ",'" + txtdocno.Text + "','" + DTPDOCDT.Value + "','','" + DTPDOCDT.Value + "'," + txtpuid.Text + ",'" + timefrom.Value + "'," + txtmcid.Text + "," + txtopid.Text + "," + txtempid.Text + ",'" + chkedtact.Checked + "'," + txtititd.Text + "," + txthqty.Text + ",'" + timeto.Value + "'," + Genclass.data1 + "," + Genclass.Yearid + "," + HFIT.Rows[i].Cells[3].Value + "," + HFIT.Rows[i].Cells[1].Value + ",0,0,'',0," + HFIT.Rows[i].Cells[4].Value + ",0,0,0,0,0,0,0,0,0," + i + "," + mode + "";
        //            qur.ExecuteNonQuery();
        //        }
        //        else
        //        {
        //            qur.CommandText = "Exec Sp_Transactions " + Genclass.Dtype + ",'" + txtdocno.Text + "','" + DTPDOCDT.Value + "','','" + DTPDOCDT.Value + "'," + txtpuid.Text + ",'" + timefrom.Value + "'," + txtmcid.Text + "," + txtopid.Text + "," + txtempid.Text + ",'" + chkedtact.Checked + "'," + txtititd.Text + "," + txthqty.Text + ",'" + timeto.Value + "'," + Genclass.data1 + "," + Genclass.Yearid + "," + HFIT.Rows[i].Cells[3].Value + "," + HFIT.Rows[i].Cells[1].Value + ",0,0,'',0," + HFIT.Rows[i].Cells[4].Value + ",0,0,0,0,0,0,0," + uid + ",0," + i + "," + mode + "";
        //            qur.ExecuteNonQuery();
        //        }
        //    }


        //    qur.CommandText = "update doctypem set lastno=lastno+1 where doctypeid=" + Genclass.Dtype + "";
        //    qur.ExecuteNonQuery();

        //    MessageBox.Show("Record has been saved", "Save", MessageBoxButtons.OK);



        //    conn.Close();
        //    //btnaddrcan_Click_1(sender, e);
        //    Loadgrid();
        //    Genpan.Visible = true;

        //}



        private void txtmac_KeyDown(object sender, KeyEventArgs e)
        {

            if (e.KeyCode == Keys.Enter)
            {
                Genclass.type = 4;
                loadput();
            }
            else if (e.KeyCode == Keys.Escape)
            {
                txtname.Text = "";
                txtname.Focus();
            }

        }

        private void txtop_KeyDown(object sender, KeyEventArgs e)
        {

            if (e.KeyCode == Keys.Enter)
            {
                Genclass.type = 5;
                loadput();
            }
            else if (e.KeyCode == Keys.Escape)
            {
                txtname.Text = "";
                txtname.Focus();
            }

        }

        private void txtemp_KeyDown(object sender, KeyEventArgs e)
        {

            if (e.KeyCode == Keys.Enter)
            {
                Genclass.type = 6;
                loadput();
            }
            else if (e.KeyCode == Keys.Escape)
            {
                txtname.Text = "";
                txtname.Focus();
            }

        }

        private void btnedit_Click(object sender, EventArgs e)
        {
            mode = 2;
            HFIT.Refresh();
            HFIT.DataSource = null;
            HFIT.Rows.Clear();

            Editpnl.Visible = true;
            Genpan.Visible = false;
            int i = HFGP.SelectedCells[0].RowIndex;
            txtdocno.Text = HFGP.Rows[i].Cells[1].Value.ToString();
            DTPDOCDT.Text = HFGP.Rows[i].Cells[2].Value.ToString();
            txtname.Text = HFGP.Rows[i].Cells[3].Value.ToString();
            txtitem.Text = HFGP.Rows[i].Cells[4].Value.ToString();
            txthqty.Text = HFGP.Rows[i].Cells[5].Value.ToString();
            txtpuid.Text = HFGP.Rows[i].Cells[6].Value.ToString();
            txtititd.Text = HFGP.Rows[i].Cells[7].Value.ToString();

            if (HFGP.Rows[i].Cells[8].Value.ToString() == "True")
            {
                chkedtact.Checked = true;
            }
            else
            {
                chkedtact.Checked = false;
            }
            txtmac.Text = HFGP.Rows[i].Cells[9].Value.ToString();
            txtmcid.Text = HFGP.Rows[i].Cells[10].Value.ToString();
            txtop.Text = HFGP.Rows[i].Cells[11].Value.ToString();
            txtopid.Text = HFGP.Rows[i].Cells[12].Value.ToString();
            txtemp.Text = HFGP.Rows[i].Cells[13].Value.ToString();
            txtempid.Text = HFGP.Rows[i].Cells[14].Value.ToString();
            //timefrom = HFGP.Rows[i].Cells[15].Value.ToString();
            //timeto = HFGP.Rows[i].Cells[16].Value.ToString();
            txtrwrea.Text = HFGP.Rows[i].Cells[17].Value.ToString();
            txtrejrea.Text = HFGP.Rows[i].Cells[18].Value.ToString();
            txtacc.Text = HFGP.Rows[i].Cells[19].Value.ToString();
            txtrew.Text = HFGP.Rows[i].Cells[20].Value.ToString();
            txtrej.Text = HFGP.Rows[i].Cells[21].Value.ToString();

            uid = HFGP.Rows[i].Cells[0].Value.ToString();

            string quy = "select c.ItemName,a.PQty,e.DocNo,c.Uid,d.uid as tluid from TransactionsPList a inner join TransactionsP b on a.TransactionsPUId=b.UId and a.DocTypeID=b.DocTypeId and a.DocTypeID=50 inner join itemm c on a.ItemUId=c.uid  inner join TransactionsPList d on a.Refuid=d.uid inner join TransactionsP e on d.TransactionsPUId=e.uid where b.uid=" + uid + "";
            Genclass.cmd = new SqlCommand(quy, conn);
            SqlDataAdapter aptr = new SqlDataAdapter(Genclass.cmd);
            DataTable tap = new DataTable();
            aptr.Fill(tap);

            for (int j = 0; j < tap.Rows.Count; j++)
            {
                var index = HFIT.Rows.Add();
                HFIT.Rows[index].Cells[0].Value = tap.Rows[j]["ItemName"].ToString();
                HFIT.Rows[index].Cells[1].Value = tap.Rows[j]["PQty"].ToString();
                HFIT.Rows[index].Cells[2].Value = tap.Rows[j]["DocNo"].ToString();
                HFIT.Rows[index].Cells[3].Value = tap.Rows[j]["Uid"].ToString();
                HFIT.Rows[index].Cells[4].Value = tap.Rows[j]["tluid"].ToString();
            }


        }

        private void txtscr1_TextChanged(object sender, EventArgs e)
        {
            Loadgrid();
        }

        private void Txtscr2_TextChanged(object sender, EventArgs e)
        {
            Loadgrid();
        }

        private void Txtscr3_TextChanged(object sender, EventArgs e)
        {
            Loadgrid();
        }

        private void txtscr4_TextChanged(object sender, EventArgs e)
        {
            Loadgrid();
        }

        private void txtscr5_TextChanged(object sender, EventArgs e)
        {
            Loadgrid();
        }


        private void button2_Click_1(object sender, EventArgs e)
        {
            txtrejrea.Text = "";
            txtrwrea.Text = "";

            //if (txtrej.Text == "" || txtrej.Text == "0" || txtrew.Text == "" || txtrew.Text == "0")
            //{
            //    panrea.Visible = true;
            //        txtrwrea.Focus();
            //}

            if (txtpuid.Text == "")
            {
                MessageBox.Show("Select the Party");
                return;
            }

            if (txtmac.Text == "")
            {
                MessageBox.Show("Select the Machine");
                return;
            }

            if (txtop.Text == "")
            {
                MessageBox.Show("Select the Operation");
                return;
            }

            if (txtemp.Text == "")
            {
                MessageBox.Show("Select the Employee");
                return;
            }

            if (txtititd.Text == "")
            {
                MessageBox.Show("Select the Item");
                return;
            }

            if (Convert.ToDouble(txthqty.Text) != (Convert.ToDouble(txtacc.Text) + Convert.ToDouble(txtrew.Text) + Convert.ToDouble(txtrej.Text)))
            {
                MessageBox.Show("Qty not matched");
                return;
            }

            conn.Open();
            if (mode == 2)
            {
                qur.CommandText = "Delete from transactionsplist where transactionspuid=" + uid + "";
                qur.ExecuteNonQuery();
                qur.CommandText = "Delete from mfg_stock_ledger where hdid=" + uid + "";
                qur.ExecuteNonQuery();

                Genclass.strsql = "select min(qty) as avqty from(select UsedItem_UId,aa.uid,itemname,round((isnull(aa.pqty,0)-isnull(bb.qty,0)-isnull(cc.qty,0))/UsedQty,0) as qty from(select a.UsedItem_UId,z.Uid,z.ItemName,a.usedqty,isnull(sum(c.pqty),0) as pqty from itemsmbom a inner join itemm b on a.UsedItem_UId=b.uid and a.companyid=" + Genclass.data1 + " and b.active=1  inner join ItemM z on a.ItemM_UId=z.uid left join TransactionsPList c on b.Uid=c.ItemUId and c.DocTypeID=10 group by a.UsedItem_UId,z.Uid,z.ItemName,a.usedqty)aa left join (select itemid,isnull(sum(qty),0) as qty from mfg_stock_ledger where trantype='Issue' and doctypeid=20 group by itemid)bb on aa.UsedItem_UId=bb.itemid left join (select itemid,isnull(sum(qty),0) as qty from mfg_stock_ledger where trantype='Issue' and doctypeid=50 group by itemid)cc on aa.UsedItem_UId=cc.itemid)tab group by uid,itemname having min(qty)>0";
                Genclass.cmd = new SqlCommand(Genclass.strsql, conn);
                SqlDataAdapter aptr6 = new SqlDataAdapter(Genclass.cmd);
                DataTable tap6 = new DataTable();
                aptr6.Fill(tap6);
                txtqty.Text = tap6.Rows[0]["avqty"].ToString();
            }

            if (Convert.ToDouble(txthqty.Text) > Convert.ToDouble(txtqty.Text))
            {
                MessageBox.Show("Qty Exeeds the avaliable qty");
                txthqty.Text = txtqty.Text;
                txtqty.Focus();
                return;
            }


            Genclass.strsql = "select UsedItem_UId,usedqty*" + txthqty.Text + " as qty from itemsmbom a inner join itemm b on a.UsedItem_UId=b.uid and ItemM_UId=" + txtititd.Text + "";
            Genclass.cmd = new SqlCommand(Genclass.strsql, conn);
            SqlDataAdapter aptr1 = new SqlDataAdapter(Genclass.cmd);
            DataTable tap1 = new DataTable();
            aptr1.Fill(tap1);

            for (int i = 0; i < tap1.Rows.Count; i++)
            {
                Double cqty1 = 0;
                Double cqty2 = 0;
                Double cqty3 = 0;
                Double cqty4 = 0;
                //Double cqty5 = 0;

                if (mode == 1)
                {
                    if (i == 0)
                    {
                        qur.CommandText = "Insert into transactionsp values (" + Genclass.Dtype + ",'" + txtdocno.Text + "','" + DTPDOCDT.Value + "','','" + DTPDOCDT.Value + "'," + txtpuid.Text + ",'" + timefrom.Value + "'," + txtmcid.Text + "," + txtopid.Text + "," + txtempid.Text + ",'" + chkedtact.Checked + "'," + txtititd.Text + "," + txthqty.Text + ",'" + timeto.Value + "'," + Genclass.data1 + "," + Genclass.Yearid + ")";
                        qur.ExecuteNonQuery();
                    }
                }
                cqty1 = Convert.ToDouble(tap1.Rows[i]["qty"].ToString());
                cqty4 = Convert.ToDouble(tap1.Rows[i]["qty"].ToString());
                //cqty5 = cqty5 + cqty1;

                Genclass.strsql = "select uid from transactionsp where docno='" + txtdocno.Text + "' and doctypeid=" + Genclass.Dtype + "";
                Genclass.cmd = new SqlCommand(Genclass.strsql, conn);
                SqlDataAdapter aptr3 = new SqlDataAdapter(Genclass.cmd);
                DataTable tap3 = new DataTable();
                aptr3.Fill(tap3);
                Genclass.tuid = Convert.ToInt16(tap3.Rows[0]["uid"].ToString());



                Genclass.strsql = "select uid,a.itemuid,isnull(a.pqty,0)-isnull(b.pqty,0)-isnull(c.pqty,0) as sqty from (select uid,ItemUId,sum(pqty) as pqty from TransactionsPList where DocTypeID=10 and ItemUId=" + Convert.ToDouble(tap1.Rows[i]["UsedItem_UId"].ToString()) + " group by uid,ItemUId)a left join (select refuid,ItemUId,sum(pqty) as pqty from TransactionsPList where DocTypeID=50 and ItemUId=" + Convert.ToDouble(tap1.Rows[i]["UsedItem_UId"].ToString()) + " group by refuid,ItemUId)b on a.ItemUId=b.ItemUId and a.uid=b.refuid  left join (select refuid,ItemUId,sum(pqty) as pqty from TransactionsPList where DocTypeID=20 and ItemUId=" + Convert.ToDouble(tap1.Rows[i]["UsedItem_UId"].ToString()) + " group by refuid,ItemUId)c on a.ItemUId=c.ItemUId and a.uid=c.refuid group by uid,a.ItemUId,a.pqty,b.pqty,c.pqty having isnull(a.pqty,0)-isnull(b.pqty,0)-isnull(c.pqty,0)>0";
                Genclass.cmd = new SqlCommand(Genclass.strsql, conn);
                SqlDataAdapter aptr2 = new SqlDataAdapter(Genclass.cmd);
                DataTable tap2 = new DataTable();
                aptr2.Fill(tap2);

                for (int j = 0; j < tap2.Rows.Count; j++)
                {
                    cqty2 = Convert.ToDouble(tap2.Rows[j]["sqty"].ToString());


                    if (cqty2 >= cqty1)
                    {
                        cqty3 = cqty1;
                    }
                    else
                    {

                        cqty3 = cqty2;

                    }



                    conn.Close();

                    conn.Open();


                    qur.CommandText = "Insert into transactionsplist values (" + Genclass.Dtype + "," + Convert.ToDouble(tap3.Rows[0]["uid"].ToString()) + "," + Convert.ToDouble(tap1.Rows[i]["UsedItem_UId"].ToString()) + "," + cqty3 + ",0,0,''," + txthqty.Text + "," + Convert.ToDouble(tap2.Rows[0]["uid"].ToString()) + ",0,0,0,0,0,0,0," + mode + ",null)";
                    qur.ExecuteNonQuery();

                    Genclass.strsql = "select uid from transactionsplist where transactionspuid='" + Genclass.tuid + "' and itemuid=" + Convert.ToDouble(tap1.Rows[i]["UsedItem_UId"].ToString()) + "";
                    Genclass.cmd = new SqlCommand(Genclass.strsql, conn);
                    SqlDataAdapter aptr5 = new SqlDataAdapter(Genclass.cmd);
                    DataTable tap5 = new DataTable();
                    aptr5.Fill(tap5);
                    //Genclass.tuid = Convert.ToInt16(tap3.Rows[0]["uid"].ToString());






                    Genclass.strsql = "select isnull(sum(pqty),0) as qty from TransactionsPList where TransactionsPUId=" + Convert.ToDouble(tap3.Rows[0]["uid"].ToString()) + " and ItemUId=" + Convert.ToDouble(tap1.Rows[i]["UsedItem_UId"].ToString()) + "";
                    Genclass.cmd = new SqlCommand(Genclass.strsql, conn);
                    SqlDataAdapter aptr4 = new SqlDataAdapter(Genclass.cmd);
                    DataTable tap4 = new DataTable();
                    aptr4.Fill(tap4);

                    cqty1 = cqty1 - Convert.ToDouble(tap2.Rows[0]["sqty"].ToString());

                }

            }

            if (txtrej.Text == "" || txtrej.Text == "0" || txtrew.Text == "" || txtrew.Text == "0")
            {


                panrea.Visible = true;
                Reqbk.Visible = true;
                button3.Visible = true;
                txtrew.Focus();


            }
            else
            {
                Genclass.strsql = "select c.ItemName,a.PQty,e.DocNo,c.Uid,d.uid as tluid from TransactionsPList a inner join TransactionsP b on a.TransactionsPUId=b.UId and a.DocTypeID=b.DocTypeId and a.DocTypeID=50 inner join itemm c on a.ItemUId=c.uid  inner join TransactionsPList d on a.Refuid=d.uid inner join TransactionsP e on d.TransactionsPUId=e.uid where b.uid=" + Genclass.tuid + "";
                Genclass.cmd = new SqlCommand(Genclass.strsql, conn);
                SqlDataAdapter aptr7 = new SqlDataAdapter(Genclass.cmd);
                DataTable tap7 = new DataTable();
                aptr7.Fill(tap7);

                HFIT.Refresh();
                HFIT.DataSource = null;
                HFIT.Rows.Clear();

                for (int k = 0; k < tap7.Rows.Count; k++)
                {
                    var index = HFIT.Rows.Add();
                    HFIT.Rows[index].Cells[0].Value = tap7.Rows[k]["ItemName"].ToString();
                    HFIT.Rows[index].Cells[1].Value = Convert.ToDouble(tap7.Rows[k]["PQty"].ToString());
                    HFIT.Rows[index].Cells[2].Value = tap7.Rows[k]["DocNo"].ToString();
                    HFIT.Rows[index].Cells[3].Value = Convert.ToDouble(tap7.Rows[k]["Uid"].ToString());
                    HFIT.Rows[index].Cells[4].Value = Convert.ToDouble(tap7.Rows[k]["tluid"].ToString());

                }

                panrea.Visible = false;
                Reqbk.Visible = false;
                button3.Visible = false;
            }



        }


        private void txthqty_TextChanged(object sender, EventArgs e)
        {
            txtacc.Text = txthqty.Text;
        }


        private void button4_Click(object sender, EventArgs e)
        {

            if (mode == 1)
            {
                qur.CommandText = "update doctypem set lastno= lastno + 1 where doctypeid=" + Genclass.Dtype + "";
                qur.ExecuteNonQuery();
            }

            MessageBox.Show("Record Saved");
            Editpnl.Visible = false;
            Genpan.Visible = true;
            conn.Close();
            panadd.Visible = true;
            Loadgrid();
            return;
        }

        private void txtrew_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.F3)
            {
                panrea.Visible = true;
            }
        }

        private void txtrej_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.F3)
            {
                panrea.Visible = true;
            }
        }

        private void btndelete_Click(object sender, EventArgs e)
        {

        }

        private void cmdprt_Click(object sender, EventArgs e)
        {

        }

        private void chkact_CheckedChanged(object sender, EventArgs e)
        {
            Loadgrid();
        }

        private void btnadd_Click(object sender, EventArgs e)
        {

            mode = 1;

            Genclass.Module.ClearTextBox(this, Editpnl);
            txtrej.Text = "0";
            txtrew.Text = "0";
            txtacc.Text = "0";
            Genclass.Module.Gendocno();
            Genpan.Visible = false;
            Editpnl.Visible = true;
            panadd.Visible = false;
            HFIT.Refresh();
            HFIT.DataSource = null;
            HFIT.Rows.Clear();

            txtdocno.Text = Genclass.ST;
            DateTime d = new DateTime();
            d = DateTime.Now;
            DTPDOCDT.Text = d.ToString("dd.MM.yyyy");
            Titlep();
            txtmac.Focus();
        }

        private void butedit_Click(object sender, EventArgs e)
        {
            mode = 2;
            HFIT.Refresh();
            HFIT.DataSource = null;
            HFIT.Rows.Clear();
            panadd.Visible = false;
            Editpnl.Visible = true;
            Genpan.Visible = false;
            Genclass.Module.ClearTextBox(this, Editpnl);
            int i = HFGP.SelectedCells[0].RowIndex;
            txtdocno.Text = HFGP.Rows[i].Cells[1].Value.ToString();
            DTPDOCDT.Text = HFGP.Rows[i].Cells[2].Value.ToString();
            txtname.Text = HFGP.Rows[i].Cells[3].Value.ToString();
            txtitem.Text = HFGP.Rows[i].Cells[4].Value.ToString();
            txthqty.Text = HFGP.Rows[i].Cells[5].Value.ToString();
            txtpuid.Text = HFGP.Rows[i].Cells[6].Value.ToString();
            txtititd.Text = HFGP.Rows[i].Cells[7].Value.ToString();

            if (HFGP.Rows[i].Cells[8].Value.ToString() == "True")
            {
                chkedtact.Checked = true;
            }
            else
            {
                chkedtact.Checked = false;
            }
            txtmac.Text = HFGP.Rows[i].Cells[9].Value.ToString();
            txtmcid.Text = HFGP.Rows[i].Cells[10].Value.ToString();
            txtop.Text = HFGP.Rows[i].Cells[11].Value.ToString();
            txtopid.Text = HFGP.Rows[i].Cells[12].Value.ToString();
            txtemp.Text = HFGP.Rows[i].Cells[13].Value.ToString();
            txtempid.Text = HFGP.Rows[i].Cells[14].Value.ToString();
            //timefrom = HFGP.Rows[i].Cells[15].Value.ToString();
            //timeto = HFGP.Rows[i].Cells[16].Value.ToString();
            txtrwrea.Text = HFGP.Rows[i].Cells[17].Value.ToString();
            txtrejrea.Text = HFGP.Rows[i].Cells[18].Value.ToString();
            txtacc.Text = HFGP.Rows[i].Cells[19].Value.ToString();
            txtrew.Text = HFGP.Rows[i].Cells[20].Value.ToString();
            txtrej.Text = HFGP.Rows[i].Cells[21].Value.ToString();

            uid = HFGP.Rows[i].Cells[0].Value.ToString();

            string quy = "select c.ItemName,a.PQty,e.DocNo,c.Uid,d.uid as tluid from TransactionsPList a inner join TransactionsP b on a.TransactionsPUId=b.UId and a.DocTypeID=b.DocTypeId and a.DocTypeID=50 inner join itemm c on a.ItemUId=c.uid  inner join TransactionsPList d on a.Refuid=d.uid inner join TransactionsP e on d.TransactionsPUId=e.uid where b.uid=" + uid + "";
            Genclass.cmd = new SqlCommand(quy, conn);
            SqlDataAdapter aptr = new SqlDataAdapter(Genclass.cmd);
            DataTable tap = new DataTable();
            aptr.Fill(tap);

            for (int j = 0; j < tap.Rows.Count; j++)
            {
                var index = HFIT.Rows.Add();
                HFIT.Rows[index].Cells[0].Value = tap.Rows[j]["ItemName"].ToString();
                HFIT.Rows[index].Cells[1].Value = tap.Rows[j]["PQty"].ToString();
                HFIT.Rows[index].Cells[2].Value = tap.Rows[j]["DocNo"].ToString();
                HFIT.Rows[index].Cells[3].Value = tap.Rows[j]["Uid"].ToString();
                HFIT.Rows[index].Cells[4].Value = tap.Rows[j]["tluid"].ToString();
            }

        }

        private void buttnext1_Click(object sender, EventArgs e)
        {
            this.Dispose();

        }

        private void HFGP_CellMouseUp(object sender, DataGridViewCellMouseEventArgs e)
        {
            if (HFGP.Rows[0].Cells[0].Value.ToString() != "")
            {
                int index = HFGP.SelectedRows[0].Index;
                if (index < HFGP.Rows.Count - 1)
                {
                    index = index + 1;
                    int index1 = HFGP.Rows.Count - 1;
                    lblno1.Text = index.ToString();
                    lblno2.Text = "of " + index1.ToString();
                }
            }
        }

        private void HFGP_KeyUp(object sender, KeyEventArgs e)
        {
            if (HFGP.Rows[0].Cells[0].Value.ToString() != "")
            {
                int index = HFGP.SelectedRows[0].Index;
                if (index < HFGP.Rows.Count - 1)
                {
                    index = index + 1;
                    int index1 = HFGP.Rows.Count - 1;
                    lblno1.Text = index.ToString();
                    lblno2.Text = "of " + index1.ToString();
                }
            }
        }

        private void buttnnvfst_Click(object sender, EventArgs e)
        {
            int index = HFGP.SelectedRows[0].Index;
            int index1 = 0;
            int index2 = HFGP.Rows.Count - 1;

            HFGP.Rows[index].Selected = false;
            HFGP.Rows[index1].Selected = true;
            index1 = index1 + 1;
            lblno1.Text = index1.ToString();
            lblno2.Text = "of " + index2.ToString();
        }

        private void buttnnxtlft_Click(object sender, EventArgs e)
        {

            int index = HFGP.SelectedRows[0].Index;
            int index1 = HFGP.Rows.Count - 1; ;
            if (index > 0)
            {
                HFGP.Rows[index].Selected = false;
                HFGP.Rows[index - 1].Selected = true;
                lblno1.Text = index.ToString();
                lblno2.Text = "of " + index1.ToString();
            }
        }

        private void buttrnxt_Click(object sender, EventArgs e)
        {
            int index = HFGP.SelectedRows[0].Index;
            int index2 = HFGP.SelectedRows[0].Index + 2;
            int index1 = HFGP.Rows.Count - 1; ;
            if (index < HFGP.Rows.Count - 2)
            {
                HFGP.Rows[index].Selected = false;
                HFGP.Rows[index + 1].Selected = true;
                lblno1.Text = index2.ToString();
                lblno2.Text = "of " + index1.ToString();

            }
        }

        private void btnfinnxt_Click(object sender, EventArgs e)
        {
            int index = HFGP.SelectedRows[0].Index;
            int index1 = HFGP.Rows.Count - 1;


            HFGP.Rows[index].Selected = false;

            HFGP.Rows[index1 - 1].Selected = true;
            lblno1.Text = index1.ToString();
            lblno2.Text = "of " + index1.ToString();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            this.Dispose();
        }

        private void Reqbk_Click_1(object sender, EventArgs e)
        {
            panrea.Visible = false;
            Reqbk.Visible = false;
            button3.Visible = false;
        }

        private void button3_Click_1(object sender, EventArgs e)
        {
            if (mode == 2)
            {
                qur.CommandText = "Delete from RejReason where Tplistuid=" + uid + "";
                qur.ExecuteNonQuery();
            }
            qur.CommandText = "Insert into RejReason values (" + Genclass.Dtype + "," + Genclass.tuid + "," + txtacc.Text + "," + txtrej.Text + "," + txtrew.Text + ",'" + txtrejrea.Text + "','" + txtrwrea.Text + "')";
            qur.ExecuteNonQuery();

            Genclass.strsql = "select c.ItemName,a.PQty,e.DocNo,c.Uid,d.uid as tluid from TransactionsPList a inner join TransactionsP b on a.TransactionsPUId=b.UId and a.DocTypeID=b.DocTypeId and a.DocTypeID=50 inner join itemm c on a.ItemUId=c.uid  inner join TransactionsPList d on a.Refuid=d.uid inner join TransactionsP e on d.TransactionsPUId=e.uid where b.uid=" + Genclass.tuid + "";
            Genclass.cmd = new SqlCommand(Genclass.strsql, conn);
            SqlDataAdapter aptr7 = new SqlDataAdapter(Genclass.cmd);
            DataTable tap7 = new DataTable();
            aptr7.Fill(tap7);

            HFIT.Refresh();
            HFIT.DataSource = null;
            HFIT.Rows.Clear();

            for (int k = 0; k < tap7.Rows.Count; k++)
            {
                var index = HFIT.Rows.Add();
                HFIT.Rows[index].Cells[0].Value = tap7.Rows[k]["ItemName"].ToString();
                HFIT.Rows[index].Cells[1].Value = Convert.ToDouble(tap7.Rows[k]["PQty"].ToString());
                HFIT.Rows[index].Cells[2].Value = tap7.Rows[k]["DocNo"].ToString();
                HFIT.Rows[index].Cells[3].Value = Convert.ToDouble(tap7.Rows[k]["Uid"].ToString());
                HFIT.Rows[index].Cells[4].Value = Convert.ToDouble(tap7.Rows[k]["tluid"].ToString());

            }

            panrea.Visible = false;
        }

        private void HFIT_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.F2)
            {
                panrea.Visible = true;
                Reqbk.Visible = true;
                button3.Visible = true;

            }
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
        }

        private void butexit_Click(object sender, EventArgs e)
        {

        }

    }

}
