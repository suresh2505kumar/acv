﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Windows.Forms;
using System.Configuration;
using System.Data.SqlClient;



namespace ACV
{
    public partial class Frmitemgrp : Form
    {
        public Frmitemgrp()
        {
            CenterToParent();
            this.BackColor = Color.White;
            InitializeComponent();

        }

        string uid = "";
        int mode = 0;
        //SqlCommand cmd;
        SqlConnection conn = new SqlConnection("Data Source=" + Genclass.data2 + "; Initial Catalog=" + Genclass.data5 + ";User id=" + Genclass.data3 + ";Password=" + Genclass.data4 + "");

        //SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["connstr"].ConnectionString);
        SqlCommand qur = new SqlCommand();

        private void btnexit_Click_1(object sender, EventArgs e)
        {
            this.Dispose();
        }

        private void Loadgrid()
        {
            try
            {
                conn.Open();

                Genclass.StrSrch = "";

                if (Genclass.Gbtxtid == 1)
                {
                    Genclass.FSSQLSortStr = "ItemGroup";
                    //Genclass.FSSQLSortStr1 = "hsncode";
                }
                else if (Genclass.Gbtxtid == 2)
                {
                    Genclass.FSSQLSortStr = "ItemsubGroup";
                }
                else if (Genclass.Gbtxtid == 3)
                {
                    Genclass.FSSQLSortStr = "Categorytype";
                }
                else if (Genclass.Gbtxtid == 4 || Genclass.Gbtxtid == 15 || Genclass.Gbtxtid == 30 || Genclass.Gbtxtid == 8 || Genclass.Gbtxtid == 9 || Genclass.Gbtxtid == 11 || Genclass.Gbtxtid == 20 || Genclass.Gbtxtid == 21 || Genclass.Gbtxtid == 12 || Genclass.Gbtxtid == 13)
                {
                    Genclass.FSSQLSortStr = "Generalname";
                }
                else if (Genclass.Gbtxtid == 5)
                {
                    Genclass.FSSQLSortStr = "ProcessName";
                }

                else if (Genclass.Gbtxtid == 6)
                {
                    Genclass.FSSQLSortStr = "Tax";
                }

                else if (Genclass.Gbtxtid == 7)
                {
                    Genclass.FSSQLSortStr = "ExciseDuty";
                }
                else if (Genclass.Gbtxtid == 10)
                {
                    Genclass.FSSQLSortStr = "Hsncode";
                }


                if (txtscr1.Text != "")
                {
                    if (Genclass.StrSrch == "")
                    {
                        Genclass.StrSrch = Genclass.FSSQLSortStr + " like '%" + txtscr1.Text + "%'";
                    }
                    else
                    {
                        Genclass.StrSrch = Genclass.StrSrch + " and " + Genclass.FSSQLSortStr + " like '%" + txtscr1.Text + "%'";
                    }

                }
              
                
                if (txtscr1.Text != "")
                {
                    Genclass.StrSrch = " " + Genclass.StrSrch;
                }
               
                else
                {
                    if (Genclass.Gbtxtid == 1 || Genclass.Gbtxtid == 2 || Genclass.Gbtxtid == 30 || Genclass.Gbtxtid == 4 || Genclass.Gbtxtid == 15 || Genclass.Gbtxtid == 5 || Genclass.Gbtxtid == 7 || Genclass.Gbtxtid == 8 || Genclass.Gbtxtid == 9 || Genclass.Gbtxtid == 12 || Genclass.Gbtxtid == 13)
                    {
                        Genclass.StrSrch = "uid <> 0";
                    }
                    else if (Genclass.Gbtxtid == 3 || Genclass.Gbtxtid == 6 || Genclass.Gbtxtid == 10 || Genclass.Gbtxtid == 11 || Genclass.Gbtxtid == 20 || Genclass.Gbtxtid == 21)
                    {
                        Genclass.StrSrch = "a.uid <> 0";
                    }

                }

                if (Genclass.Gbtxtid == 1)
                {
                    if (chkact.Checked == true)
                    {
                        string quy = "select uid,ItemGroup,active from ItemGroup   where active=1 and companyid=" + Genclass.data1 + " and " + Genclass.StrSrch + " order by " + Genclass.FSSQLSortStr + "";
                        Genclass.cmd = new SqlCommand(quy, conn);
                    }
                    else
                    {
                        string quy = "select uid,ItemGroup,active from ItemGroup  where active=0 and companyid=" + Genclass.data1 + " and " + Genclass.StrSrch + " order by " + Genclass.FSSQLSortStr + "";
                        Genclass.cmd = new SqlCommand(quy, conn);
                    }

                }
                else if (Genclass.Gbtxtid == 2)
                {
                    if (chkact.Checked == true)
                    {
                        string quy = "select uid as Uid,ItemSubGroup as Brand,active from itemsubgroup  where active=1 and companyid=" + Genclass.data1 + " and " + Genclass.StrSrch + " order by " + Genclass.FSSQLSortStr + "";
                        Genclass.cmd = new SqlCommand(quy, conn);
                    }
                    else
                    {
                        string quy = "select uid as Uid,ItemSubGroup as Brand,active from itemsubgroup  where active=0 and companyid=" + Genclass.data1 + " and " + Genclass.StrSrch + " order by " + Genclass.FSSQLSortStr + "";
                        Genclass.cmd = new SqlCommand(quy, conn);
                    }

                }
                else if (Genclass.Gbtxtid == 3)
                {
                    if (chkact.Checked == true)
                    {
                        string quy = "select a.uid as Uid,a.Categorytype as Category,b.Category as CategoryType,a.active from Categorytypem a inner join  CategoryM b on b.uid=a.CategoryM_uid where a.active=1 and a.companyid=" + Genclass.data1 + " and " + Genclass.StrSrch + " order by " + Genclass.FSSQLSortStr + "";
                        Genclass.cmd = new SqlCommand(quy, conn);
                    }
                    else
                    {
                        string quy = "select a.uid as Uid,a.Categorytype as Category,b.Category as CategoryType,a.active from Categorytypem a inner join  CategoryM b on b.uid=a.CategoryM_uid where a.active=0 and a.companyid=" + Genclass.data1 + " and " + Genclass.StrSrch + " order by " + Genclass.FSSQLSortStr + "";
                        Genclass.cmd = new SqlCommand(quy, conn);
                    }
                }
                else if (Genclass.Gbtxtid == 4)
                {
                    if (chkact.Checked == true)
                    {
                        string quy = "select uid,Generalname as UoM,active from Generalm where active=1 and typem_uid=1 and companyid=" + Genclass.data1 + " and " + Genclass.StrSrch + " order by " + Genclass.FSSQLSortStr + "";
                        Genclass.cmd = new SqlCommand(quy, conn);
                    }
                    else
                    {
                        string quy = "select uid,Generalname as UoM,active from Generalm where active=0 and typem_uid=1 and companyid=" + Genclass.data1 + " and " + Genclass.StrSrch + " order by " + Genclass.FSSQLSortStr + "";
                        Genclass.cmd = new SqlCommand(quy, conn);
                    }
                }
                else if (Genclass.Gbtxtid == 30)
                {
                    if (chkact.Checked == true)
                    {
                        string quy = "select uid,Generalname as Location,active from Generalm where active=1 and typem_uid=17 and companyid=" + Genclass.data1 + " and " + Genclass.StrSrch + " order by " + Genclass.FSSQLSortStr + "";
                        Genclass.cmd = new SqlCommand(quy, conn);
                    }
                    else
                    {
                        string quy = "select uid,Generalname as Location,active from Generalm where active=0 and typem_uid=17 and companyid=" + Genclass.data1 + " and " + Genclass.StrSrch + " order by " + Genclass.FSSQLSortStr + "";
                        Genclass.cmd = new SqlCommand(quy, conn);
                    }
                }
                else if (Genclass.Gbtxtid == 15)
                {
                    if (chkact.Checked == true)
                    {
                        string quy = "select uid,Generalname as Terms,active from Generalm where active=1 and typem_uid=14 and companyid=" + Genclass.data1 + " and " + Genclass.StrSrch + " order by " + Genclass.FSSQLSortStr + "";
                        Genclass.cmd = new SqlCommand(quy, conn);
                    }
                    else
                    {
                        string quy = "select uid,Generalname as Terms,active from Generalm where active=0 and typem_uid=14 and companyid=" + Genclass.data1 + " and " + Genclass.StrSrch + " order by " + Genclass.FSSQLSortStr + "";
                        Genclass.cmd = new SqlCommand(quy, conn);
                    }
                }
                else if (Genclass.Gbtxtid == 5)
                {
                    if (chkact.Checked == true)
                    {
                        string quy = "select uid,processname as ProcessName,active from processm where active=1 and companyid=" + Genclass.data1 + " and " + Genclass.StrSrch + " order by " + Genclass.FSSQLSortStr + "";
                        Genclass.cmd = new SqlCommand(quy, conn);
                    }
                    else
                    {
                        string quy = "select uid,processname as ProcessName,active from processm where active=0 and companyid=" + Genclass.data1 + " and " + Genclass.StrSrch + " order by " + Genclass.FSSQLSortStr + "";
                        Genclass.cmd = new SqlCommand(quy, conn);
                    }
                }
                else if (Genclass.Gbtxtid == 6)
                {
                    if (chkact.Checked == true)
                    {
                        string quy = " select a.UId,a.GeneralName,b.TypeName,a.F1,a.TypeM_UId,a.active from  GENERALM a inner join typem b  on a.TypeM_Uid=b.UId where a.TypeM_Uid in (6,7,8,9) and Active=1  and a.companyid=" + Genclass.data1 + " and " + Genclass.StrSrch + " order by " + Genclass.FSSQLSortStr + "";
                        Genclass.cmd = new SqlCommand(quy, conn);
                    }
                    else
                    {
                        string quy = " select a.UId,a.GeneralName,b.TypeName,a.F1,a.TypeM_UId,a.active from  GENERALM a inner join typem b  on a.TypeM_Uid=b.UId where a.TypeM_Uid in (6,7,8,9) and Active=0  and a.companyid=" + Genclass.data1 + " and " + Genclass.StrSrch + " order by " + Genclass.FSSQLSortStr + "";
                        Genclass.cmd = new SqlCommand(quy, conn);
                    }
                }
                else if (Genclass.Gbtxtid == 7)
                {
                    if (chkact.Checked == true)
                    {
                        string quy = " Select UId,GeneralName,F1,F2,F3,TypeM_UId,active from GENERALM  where TypeM_UId =5 and companyid=" + Genclass.data1 + " and active=1 and " + Genclass.StrSrch + " order by " + Genclass.FSSQLSortStr + "";
                        Genclass.cmd = new SqlCommand(quy, conn);
                    }
                    else
                    {
                        string quy = " Select UId,GeneralName,F1,F2,F3,TypeM_UId,active from GENERALM  where TypeM_UId =5 and companyid=" + Genclass.data1 + " and active=0 and " + Genclass.StrSrch + " order by " + Genclass.FSSQLSortStr + "";
                        Genclass.cmd = new SqlCommand(quy, conn);
                    }
                }
                else if (Genclass.Gbtxtid == 8)
                {
                    if (chkact.Checked == true)
                    {
                        string quy = "select uid,Generalname as Machine,active from Generalm where active=1 and typem_uid=10 and companyid=" + Genclass.data1 + " and " + Genclass.StrSrch + " order by " + Genclass.FSSQLSortStr + "";
                        Genclass.cmd = new SqlCommand(quy, conn);
                    }
                    else
                    {
                        string quy = "select uid,Generalname as Machine,active from Generalm where active=0 and typem_uid=10 and companyid=" + Genclass.data1 + " and " + Genclass.StrSrch + " order by " + Genclass.FSSQLSortStr + "";
                        Genclass.cmd = new SqlCommand(quy, conn);
                    }
                }
                else if (Genclass.Gbtxtid == 9)
                {
                    if (chkact.Checked == true)
                    {
                        string quy = "select uid,Generalname as Employee,active from Generalm where active=1 and typem_uid=11 and companyid=" + Genclass.data1 + " and " + Genclass.StrSrch + " order by " + Genclass.FSSQLSortStr + "";
                        Genclass.cmd = new SqlCommand(quy, conn);
                    }
                    else
                    {
                        string quy = "select uid,Generalname as Employee,active from Generalm where active=0 and typem_uid=11 and companyid=" + Genclass.data1 + " and " + Genclass.StrSrch + " order by " + Genclass.FSSQLSortStr + "";
                        Genclass.cmd = new SqlCommand(quy, conn);
                    }
                }

                else if (Genclass.Gbtxtid == 10)
                {
                    if (chkact.Checked == true)
                    {
                        string quy = "Select top 25 b.uid,Hsncode as HSN,Hsnname,sname,0 as CGST,c.generalname as GST,0 as IGST,a.active,a.uid as hsnid from Hsn a inner join  hsndet b on a.uid=b.Hsnid left join generalm c on b.sgid=c.uid where a.active=1  and b.companyid=" + Genclass.data1 + " and " + Genclass.StrSrch + " order by " + Genclass.FSSQLSortStr + "";
                        Genclass.cmd = new SqlCommand(quy, conn);
                    }
                    else
                    {
                        string quy = "select top 25 b.uid,Hsncode as HSN,Hsnname,sname,0 as CGST,c.generalname as GST,0 as IGST,a.active,a.uid as hsnid from Hsn a inner join  hsndet b on a.uid=b.Hsnid left join generalm c on b.sgid=c.uid where a.active=0  and b.companyid=" + Genclass.data1 + " and " + Genclass.StrSrch + " order by " + Genclass.FSSQLSortStr + "";
                        Genclass.cmd = new SqlCommand(quy, conn);
                    }
                }

                else if (Genclass.Gbtxtid == 11)
                {
                    if (chkact.Checked == true)
                    {
                        string quy = "select a.uid,Generalname as Tax,f1 as Percentage,b.typename as Type,a.active from Generalm a inner join typem b on a.typem_uid=b.uid where a.active=1 and typem_uid in (6,7,8,9) and a.companyid=" + Genclass.data1 + " and " + Genclass.StrSrch + " order by " + Genclass.FSSQLSortStr + "";
                        Genclass.cmd = new SqlCommand(quy, conn);
                    }
                    else
                    {
                        string quy = "select a.uid,Generalname as Tax,f1 as Percentage,b.typename as Type,a.active from Generalm a inner join typem b on a.typem_uid=b.uid where a.active=0 and typem_uid in (6,7,8,9) and a.companyid=" + Genclass.data1 + " and " + Genclass.StrSrch + " order by " + Genclass.FSSQLSortStr + "";
                        Genclass.cmd = new SqlCommand(quy, conn);
                    }
                }
                else if (Genclass.Gbtxtid == 20)
                {
                    if (chkact.Checked == true)
                    {
                        string quy = "select a.uid,Generalname as BeamNo,f1 as Weight,b.typename as Type,a.active from Generalm a inner join typem b on a.typem_uid=b.uid where a.active=1 and typem_uid in (15) and a.companyid=" + Genclass.data1 + " and " + Genclass.StrSrch + " order by " + Genclass.FSSQLSortStr + "";
                        Genclass.cmd = new SqlCommand(quy, conn);
                    }
                    else
                    {
                        string quy = "select a.uid,Generalname as BeamNo,f1 as Weight,b.typename as Type,a.active from Generalm a inner join typem b on a.typem_uid=b.uid where a.active=0 and typem_uid in (15) and a.companyid=" + Genclass.data1 + " and " + Genclass.StrSrch + " order by " + Genclass.FSSQLSortStr + "";
                        Genclass.cmd = new SqlCommand(quy, conn);
                    }
                }

                else if (Genclass.Gbtxtid == 21)
                {
                    if (chkact.Checked == true)
                    {
                        string quy = "select a.uid,Generalname as SpindleName,f1 as Weight,b.typename as Type,a.active from Generalm a inner join typem b on a.typem_uid=b.uid where a.active=1 and typem_uid in (16) and a.companyid=" + Genclass.data1 + " and " + Genclass.StrSrch + " order by " + Genclass.FSSQLSortStr + "";
                        Genclass.cmd = new SqlCommand(quy, conn);
                    }
                    else
                    {
                        string quy = "select a.uid,Generalname as SpindleName,f1 as Weight,b.typename as Type,a.active from Generalm a inner join typem b on a.typem_uid=b.uid where a.active=0 and typem_uid in (16) and a.companyid=" + Genclass.data1 + " and " + Genclass.StrSrch + " order by " + Genclass.FSSQLSortStr + "";
                        Genclass.cmd = new SqlCommand(quy, conn);
                    }
                }

                else if (Genclass.Gbtxtid == 12)
                {
                    if (chkact.Checked == true)
                    {
                        string quy = "select uid,Generalname as Terms,active from Generalm where active=1 and typem_uid=14 and companyid=" + Genclass.data1 + " and " + Genclass.StrSrch + " order by " + Genclass.FSSQLSortStr + "";
                        Genclass.cmd = new SqlCommand(quy, conn);
                    }
                    else
                    {
                        string quy = "select uid,Generalname as Terms,active from Generalm where active=0 and typem_uid=14 and companyid=" + Genclass.data1 + " and " + Genclass.StrSrch + " order by " + Genclass.FSSQLSortStr + "";
                        Genclass.cmd = new SqlCommand(quy, conn);
                    }
                }
                else if (Genclass.Gbtxtid == 13)
                {
                    if (chkact.Checked == true)
                    {
                        string quy = "select uid,Generalname as AdditionalCharges,active from Generalm where active=1 and typem_uid=13 and companyid=" + Genclass.data1 + " and " + Genclass.StrSrch + " order by " + Genclass.FSSQLSortStr + "";
                        Genclass.cmd = new SqlCommand(quy, conn);
                    }
                    else
                    {
                        string quy = "select uid,Generalname as AdditionalCharges,active from Generalm where active=0 and typem_uid=13 and companyid=" + Genclass.data1 + " and " + Genclass.StrSrch + " order by " + Genclass.FSSQLSortStr + "";
                        Genclass.cmd = new SqlCommand(quy, conn);
                    }
                }







                SqlDataAdapter aptr = new SqlDataAdapter(Genclass.cmd);
                DataTable tap = new DataTable();
                aptr.Fill(tap);


                this.HFGP.DefaultCellStyle.Font = new Font("Arial", 10);
                //dgv.ColumnHeadersDefaultCellStyle.Font = new Font(dgv.ColumnHeadersDefaultCellStyle.Font, FontStyle.Bold);
                this.HFGP.ColumnHeadersDefaultCellStyle.Font = new Font("Arial", 10, FontStyle.Bold);
                HFGP.AutoGenerateColumns = false;
                HFGP.Refresh();
                HFGP.DataSource = null;
                HFGP.Rows.Clear();


                HFGP.ColumnCount = tap.Columns.Count;
                Genclass.i = 0;
                foreach (DataColumn column in tap.Columns)
                {
                    HFGP.Columns[Genclass.i].Name = column.ColumnName;
                    HFGP.Columns[Genclass.i].HeaderText = column.ColumnName;
                    HFGP.Columns[Genclass.i].DataPropertyName = column.ColumnName;
                    Genclass.i = Genclass.i + 1;
                }



                HFGP.Columns[0].Visible = false;

                HFGP.Columns[1].Width = 380;

                if (Genclass.Gbtxtid == 1 || Genclass.Gbtxtid == 2 || Genclass.Gbtxtid == 30 || Genclass.Gbtxtid == 4 || Genclass.Gbtxtid == 15 || Genclass.Gbtxtid == 5 || Genclass.Gbtxtid == 8 || Genclass.Gbtxtid == 9 || Genclass.Gbtxtid == 12 || Genclass.Gbtxtid == 13)
                {

                    HFGP.Columns[2].Visible = false;

                   
                }
                if (Genclass.Gbtxtid == 3)
                {

                    HFGP.Columns[2].Width = 250;

                    HFGP.Columns[3].Visible = false;
                }
                if (Genclass.Gbtxtid == 6)
                {
                    HFGP.Columns[2].Width = 150;
                    HFGP.Columns[3].Width = 100;
                    HFGP.Columns[4].Visible = false;
                    HFGP.Columns[5].Visible = false;
                }
                if (Genclass.Gbtxtid == 7)
                {
                    HFGP.Columns[2].Width = 50;
                    HFGP.Columns[3].Width = 50;
                    HFGP.Columns[4].Width = 50;
                    HFGP.Columns[5].Visible = false;
                    HFGP.Columns[6].Visible = false;
                }
                if (Genclass.Gbtxtid == 10)
                {
                    HFGP.Columns[1].Width = 80;
                    HFGP.Columns[2].Visible = false;
                    HFGP.Columns[3].Width = 420;
                    HFGP.Columns[4].Visible = false;
                    HFGP.Columns[5].Width = 80;
                    HFGP.Columns[6].Visible = false;
                    HFGP.Columns[7].Visible = false;
                    HFGP.Columns[8].Visible = false;
                }

                if (Genclass.Gbtxtid == 11)
                {
                    HFGP.Columns[1].Width = 300;
                    HFGP.Columns[3].Visible = false;
                    HFGP.Columns[4].Visible = false;
                }
                if (Genclass.Gbtxtid == 20)
                {
                    HFGP.Columns[1].Width = 300;
                    HFGP.Columns[3].Visible = false;
                    HFGP.Columns[4].Visible = false;
                }
                if (Genclass.Gbtxtid == 21)
                {
                    HFGP.Columns[1].Width = 300;
                    HFGP.Columns[3].Visible = false;
                    HFGP.Columns[4].Visible = false;
                }
                HFGP.DataSource = tap;

                int index = HFGP.Rows.Count - 1;

                if (HFGP.Rows[0].Cells[1].Value.ToString() == "" || HFGP.Rows[0].Cells[1].Value == null)
                {
                    lblno1.Text = "0";
                }
                else
                {
                    lblno1.Text = "1";
                }


                lblno2.Text = "of " + index.ToString();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return;
            }
            finally
            {
                conn.Close();
                Genclass.cmd.Dispose();
            }
        }

        public void Loadgrp()
        {
            conn.Open();
            if (this.Text == "ItemGroup")
            {
                string qur = "select a.uid,b.hsncode  from hsndet a inner join hsn b on a.hsnid=b.uid where  a.companyid=" + Genclass.data1 + "";
                SqlCommand cmd = new SqlCommand(qur, conn);
                SqlDataAdapter apt = new SqlDataAdapter(cmd);
                DataTable tab = new DataTable();
                apt.Fill(tab);
                cbogrp.DataSource = null;
                cbogrp.ValueMember = "uid";
                cbogrp.DisplayMember = "hsncode";
                cbogrp.DataSource = tab;


                //cbogrp.SelectedIndex = -1;
                //cbogrp.Text = tab.Rows[0]["hsncode"].ToString();
            }
            else
            {
                string qur = "select * from Itemgroup where  companyid=1";
                SqlCommand cmd = new SqlCommand(qur, conn);
                SqlDataAdapter apt = new SqlDataAdapter(cmd);
                DataTable tab = new DataTable();
                apt.Fill(tab);
                cbogrp.DataSource = null;
                cbogrp.ValueMember = "uid";
                cbogrp.DisplayMember = "Itemgroup";
                cbogrp.DataSource = tab;


                cbogrp.SelectedIndex = -1;
            }


            conn.Close();
        }




        public void Category()
        {
            conn.Open();
            string qur = "select * from Categorym";
            SqlCommand cmd = new SqlCommand(qur, conn);
            SqlDataAdapter apt = new SqlDataAdapter(cmd);
            DataTable tab = new DataTable();
            apt.Fill(tab);
            cbogrp.DataSource = null;
            cbogrp.DataSource = tab;
            cbogrp.DisplayMember = "Category";
            cbogrp.ValueMember = "uid";
            cbogrp.SelectedIndex = -1;
            conn.Close();
        }

        public void process()
        {
            conn.Open();
            string qur = "select * from processm where companyid=" + Genclass.data1 + "";
            SqlCommand cmd = new SqlCommand(qur, conn);
            SqlDataAdapter apt = new SqlDataAdapter(cmd);
            DataTable tab = new DataTable();
            apt.Fill(tab);
            cbogrp.DataSource = null;
            cbogrp.DataSource = tab;
            cbogrp.DisplayMember = "processname";
            cbogrp.ValueMember = "uid";
            cbogrp.SelectedIndex = -1;
            conn.Close();
        }

        private void
            Frmitemgrp_Load(object sender, EventArgs e)
        {
            qur.Connection = conn;
            Left = (MdiParent.ClientRectangle.Width - Width) / 3;
            Top = (MdiParent.ClientRectangle.Height - Height) / 3;
            Genclass.Module.buttonstylepanel(panadd);
            panadd.Visible = true;
            editsvpan.Visible = false;
            Hsnsvpan.Visible = false;
            Taxsvpan.Visible = false;
            //label1.Width = 757;
            Genpan.Visible = true;


            if (Genclass.Gbtxtid == 1)
            {
                label1.Text = "Item Group";
                //lblgen.Text = Genclass.Genlbl;
            
                cbogrp.Visible = false;
                lblgrp.Visible = false;
                    txthsncode.Visible = false;
                    txttax.Visible = false;
                        label17.Visible = false;
                        lblhsss.Visible = false;
                        txthsss.Visible = false;
                        cbotax.Visible = false;
                        label3.Visible = false;
                        Txthsnname.Visible = false;
                lblgen.Text = "ItemGroup Type";
                txtscr2.Visible = true;
                //Loadgrid();
            }
            else if (Genclass.Gbtxtid == 2)
            {
                label1.Text = "Brand";
                //lblgen.Text = Genclass.Genlbl;
                cbogrp.Visible = false;
                lblgen.Visible = false;
                cbotype.Visible = false;
                txthsncode.Visible = false;
                txttax.Visible = false;
                label17.Visible = false;

                Loadgrp();
                txtscr2.Visible = false;

                //Loadgrid();
            }

            else if (Genclass.Gbtxtid == 30)
            {
                label1.Text = "Location";
                //lblgen.Text = Genclass.Genlbl;
                cbogrp.Visible = false;
                lblgen.Visible = false;
                cbotype.Visible = false;
                txthsncode.Visible = false;
                txttax.Visible = false;
                label17.Visible = false;

                Loadgrp();
                txtscr2.Visible = false;

                //Loadgrid();
            }
            else if (Genclass.Gbtxtid == 3)
            {
                label1.Text = "Category";
                lblgen.Text = Genclass.Genlbl;
                Category();
                txtscr2.Visible = false;

                //Loadgrid();
            }
            else if (Genclass.Gbtxtid == 4)
            {
                label1.Text = "UoM";
                cbogrp.Visible = false;
                lblgen.Visible = false;
                cbotype.Visible = false;
                txthsncode.Visible = false;
                txttax.Visible = false;
                label17.Visible = false;
                Category();
                txtscr2.Visible = false;

                //Loadgrid();
            }
            else if (Genclass.Gbtxtid == 15)
            {
                label1.Text = "Terms";
                cbogrp.Visible = false;
                lblgen.Visible = false;
                cbotype.Visible = false;
                txthsncode.Visible = false;
                txttax.Visible = false;
                label17.Visible = false;
                Category();
                txtscr2.Visible = false;

                //Loadgrid();
            }

            else if (Genclass.Gbtxtid == 5)
            {
                label1.Text = "Process";
                cbogrp.Visible = false;
                lblgen.Visible = false;
                cbotype.Visible = false;
                txthsncode.Visible = false;
                txttax.Visible = false;
                label17.Visible = false;
                txtscr2.Visible = false;

                //Loadgrid();
            }

            else if (Genclass.Gbtxtid == 6)
            {
                label1.Text = Genclass.Genlbl;
                Loadtax();
                txtscr2.Visible = false;
                //Loadgrid();
            }


            else if (Genclass.Gbtxtid == 7)
            {
                label1.Text = Genclass.Genlbl;
                txtscr2.Visible = false;
                //Loadgrid();

            }


            else if (Genclass.Gbtxtid == 8)
            {
                label1.Text = "Machine";
                cbogrp.Visible = false;
                lblgen.Visible = false;
                cbotype.Visible = false;
                txthsncode.Visible = false;
                txttax.Visible = false;
                label17.Visible = false;
                txtscr2.Visible = false;
                //Loadgrid();
            }



            else if (Genclass.Gbtxtid == 9)
            {
                label1.Text = "Employee";
                cbogrp.Visible = false;
                lblgen.Visible = false;
                cbotype.Visible = false;
                txthsncode.Visible = false;
                txttax.Visible = false;
                label17.Visible = false;
                txtscr2.Visible = false;
                //Loadgrid();
            }

            else if (Genclass.Gbtxtid == 10)
            {
                label1.Text = "HSN";
                lblgen.Text = Genclass.Genlbl;
                txthsn.Visible = true;
                txtscr2.Visible = false;
            }

            else if (Genclass.Gbtxtid == 11)
            {
                Loadtax();
                label1.Text = Genclass.Genlbl3;
                label3.Text = Genclass.Genlbl;
                label2.Text = Genclass.Genlbl1;
                label4.Text = Genclass.Genlbl2;
                txtscr2.Visible = false;
                label18.Visible = false;
                DTPDOCDT.Visible = false;
            }
            else if (Genclass.Gbtxtid == 20)
            {
                Loadtax();
                label1.Text = Genclass.Genlbl3;
                label3.Text = Genclass.Genlbl;
                label2.Text = Genclass.Genlbl1;
                label4.Text = Genclass.Genlbl2;
                label18.Text="Date";
                DTPDOCDT.Visible = true;
                var BeamType = new List<string>(ConfigurationManager.AppSettings["BeamType"].Split(new char[] { ';' }));
                cbotax.DataSource = BeamType;
                txtscr2.Visible = false;
                label3.Visible = true;
                cbotax.Visible = true;
            }
            else if (Genclass.Gbtxtid == 21)
            {
                Loadtax();
                label1.Text = Genclass.Genlbl3;
                label3.Text = Genclass.Genlbl;
                label2.Text = Genclass.Genlbl1;
                label4.Text = Genclass.Genlbl2;
                label18.Text = "Date";
                DTPDOCDT.Visible = true;
                txtscr2.Visible = false;
            }
            else if (Genclass.Gbtxtid == 12)
            {
                label1.Text = "Terms & Conditions";
                cbogrp.Visible = false;
                lblgen.Visible = false;
                cbotype.Visible = false;
                txthsncode.Visible = false;
                txttax.Visible = false;
                label17.Visible = false;
                txtscr2.Visible = false;

                //Loadgrid();
            }
            else if (Genclass.Gbtxtid == 13)
            {
                label1.Text = "Additional Charges";
                cbogrp.Visible = false;
                lblgen.Visible = false;
                cbotype.Visible = false;
                txthsncode.Visible = false;
                txttax.Visible = false;
                label17.Visible = false;
                txtscr2.Visible = false;
                label18.Visible = false;
                DTPDOCDT.Visible = false;

                //Loadgrid();
            }
            chkact.Checked = true;


            HFGP.RowHeadersVisible = false;
            HFGP.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            HFGP.EnableHeadersVisualStyles = false;
            HFGP.ColumnHeadersDefaultCellStyle.BackColor = SystemColors.GradientActiveCaption;
            HFGP.Focus();
            this.HFGP.DefaultCellStyle.Font = new Font("Calibri", 10);
            this.HFGP.ColumnHeadersDefaultCellStyle.Font = new Font("Calibri", 10, FontStyle.Regular);
            Loadgrid();
        }



        private void btnadd_Click(object sender, EventArgs e)
        {
            Genclass.Module.ClearTextBox(this, EditPnl);
            Genclass.Module.ClearTextBox(this, excisepan);
            Genclass.Module.ClearTextBox(this, taxpan);
            Genclass.Module.ClearTextBox(this, HSNpan);

            Chkedtact.Checked = true;
            loadscreens();
            uid = "";

        }
        private void
            loadscreens()
        {

            if (Genclass.Gbtxtid == 3)
            {
                lblgrp.Visible = true;
                cbogrp.Visible = true;
                label14.Text = "Category";
                lblhsss.Visible = false;
                txthsss.Visible = false;
                Category();
                Genpan.Visible = false;
                excisepan.Visible = false;
                taxpan.Visible = false;
                EditPnl.Visible = true;
                panadd.Visible = false;
                editsvpan.Visible = true;
                HSNpan.Visible = false;
                lblgrp.Text = "Category Type";
                lblgen.Text = "Category";
                txtgen.Focus();
            }
            else if (Genclass.Gbtxtid == 6)
            {


                Genpan.Visible = false;
                excisepan.Visible = true;
                Taxsvpan.Visible = true;
                EditPnl.Visible = false;
                panadd.Visible = false;
                taxpan.Visible = false;
                HSNpan.Visible = false;
                cbotax.Focus();
            }
            else if (Genclass.Gbtxtid == 7)
            {


                Genpan.Visible = false;
                excisepan.Visible = false;
                EditPnl.Visible = false;
                panadd.Visible = false;
                taxpan.Visible = true;
                HSNpan.Visible = false;
                cbotax.Focus();
            }

            else if (Genclass.Gbtxtid == 11)
            {
                chktax.Checked = true;


                Genpan.Visible = false;
                excisepan.Visible = true;
                Taxsvpan.Visible = true;
                EditPnl.Visible = false;
                panadd.Visible = false;
                taxpan.Visible = false;
                HSNpan.Visible = false;
                //Loadtaxtype();
                cbotax.Focus();
            }
            else if (Genclass.Gbtxtid == 20)
            {
                chktax.Checked = true;


                Genpan.Visible = false;
                excisepan.Visible = true;
                Taxsvpan.Visible = true;
                EditPnl.Visible = false;
                panadd.Visible = false;
                taxpan.Visible = false;
                HSNpan.Visible = false;
                //Loadtaxtype();
                cbotax.Focus();
            }
            else if (Genclass.Gbtxtid == 21)
            {
                chktax.Checked = true;


                Genpan.Visible = false;
                excisepan.Visible = true;
                Taxsvpan.Visible = true;
                EditPnl.Visible = false;
                panadd.Visible = false;
                taxpan.Visible = false;
                HSNpan.Visible = false;
                //Loadtaxtype();
                cbotax.Focus();
            }
            else if (Genclass.Gbtxtid == 10)
            {
                lblgrp.Visible = false;
                cbogrp.Visible = false;

                Loadtax();
                Genpan.Visible = false;
                EditPnl.Visible = false;
                excisepan.Visible = false;
                taxpan.Visible = false;
                panadd.Visible = false;
                HSNpan.Visible = true;
                Hsnsvpan.Visible = true;
                txtgen.Focus();
            }
            else if (Genclass.Gbtxtid == 1)
            {
                lblgrp.Visible = true;
             
                label14.Text = "Item Group";
                cbotype.Text = "Goods";
                cbogrp.Visible = false;
                lblhsss.Visible = false;
                txthsss.Visible = false;
                lblgrp.Visible = false;
                //Loadgrp();
                Genpan.Visible = false;
                editsvpan.Visible = true;
                EditPnl.Visible = true;
                excisepan.Visible = false;
                panadd.Visible = false;
                taxpan.Visible = false;
                HSNpan.Visible = false;
                txtgen.Focus();
            
            }
            else
            {
                if (Genclass.Gbtxtid == 2)
                {
                    label14.Text = "Brand";
                }
                if (Genclass.Gbtxtid == 30)
                {
                    label14.Text = "Location";
                }
                if (Genclass.Gbtxtid == 4)
                {
                    label14.Text = "UoM";
                }


                if (Genclass.Gbtxtid == 15)
                {
                    label14.Text = "Terms";
                }
                if (Genclass.Gbtxtid == 8)
                {
                    label14.Text = "Machine";
                }
                if (Genclass.Gbtxtid == 9)
                {
                    label14.Text = "Employee";
                }
                lblgrp.Visible = false;
                cbogrp.Visible = false;
                lblhsss.Visible = false;
                txthsss.Visible = false;

                //Loadtax();
                Genpan.Visible = false;
                EditPnl.Visible = true;
                editsvpan.Visible = true;
                excisepan.Visible = false;
                panadd.Visible = false;
                taxpan.Visible = false;
                HSNpan.Visible = false;
                txtgen.Focus();
            }
        }
        private void btnaddrcan_Click(object sender, EventArgs e)
        {
            EditPnl.Visible = false;
            Genpan.Visible = true;
        }

        //private void btnsave_Click(object sender, EventArgs e)
        //{
        //    if (txtgen.Text == "")
        //    {
        //        MessageBox.Show("Enter the Item Group");
        //        txtgen.Focus();
        //        return;
        //    }

        //    conn.Open();
        //    //string qur = "insert into ItemGroup values (@Itemgroup,@active,@Sname)";
        //    if (mode == 1)
        //    {

        //        if (Genclass.Gbtxtid == 1)
        //        {
        //            qur.CommandText = "insert into ItemGroup (itemgroup,active) values ('" + txtgen.Text + "',1)";
        //        }
        //        else if (Genclass.Gbtxtid == 2)
        //        {
        //            qur.CommandText = "insert into ItemsubGroup (itemgroup_uid,itemsubgroup,active) values (" + cbogrp.SelectedValue + ",'" + txtgen.Text + "',1)";
        //        }
        //        else if (Genclass.Gbtxtid == 3)
        //        {
        //            qur.CommandText = "insert into Categorytypem (Categorym_uid,categorytype,active) values (" + cbogrp.SelectedValue + ",'" + txtgen.Text + "',1)";
        //        }
        //        else if (Genclass.Gbtxtid == 4)
        //        {
        //            qur.CommandText = "insert into Generalm (Typem_uid,Generalname,active) values (1,'" + txtgen.Text + "',1)";
        //        }

        //        else if (Genclass.Gbtxtid == 5)
        //        {
        //            qur.CommandText = "insert into processm (processname,active) values ('" + txtgen.Text + "',1)";
        //        }
        //        else if (Genclass.Gbtxtid == 8)
        //        {
        //            qur.CommandText = "insert into Generalm (Typem_uid,Generalname,active) values (10,'" + txtgen.Text + "',1)";
        //        }
        //        else if (Genclass.Gbtxtid == 9)
        //        {
        //            qur.CommandText = "insert into Generalm (Typem_uid,Generalname,active) values (11,'" + txtgen.Text + "',1)";
        //        }
        //        qur.ExecuteNonQuery();
        //        MessageBox.Show("Record has been saved", "Save", MessageBoxButtons.OK);
        //    }

        //    if (mode == 2)
        //    {
        //        if (Genclass.Gbtxtid == 1)
        //        {
        //            qur.CommandText = "Update ItemGroup set itemgroup='" + txtgen.Text + "' where uid=" + uid + "";
        //        }
        //        else if (Genclass.Gbtxtid == 2)
        //        {
        //            qur.CommandText = "Update ItemsubGroup set itemsubgroup='" + txtgen.Text + "',itemgroup_uid=" + cbogrp.SelectedValue + " where uid=" + uid + "";
        //        }
        //        else if (Genclass.Gbtxtid == 3)
        //        {
        //            qur.CommandText = "Update Categorytypem set categorytype='" + txtgen.Text + "',Categorym_uid=" + cbogrp.SelectedValue + " where uid=" + uid + "";
        //        }
        //        else if (Genclass.Gbtxtid == 4 || Genclass.Gbtxtid == 10 || Genclass.Gbtxtid == 11)
        //        {
        //            qur.CommandText = "Update Generalm set Generalname='" + txtgen.Text + "' where uid=" + uid + "";
        //        }

        //        else if (Genclass.Gbtxtid == 5)
        //        {
        //            qur.CommandText = "Update processm set processname='" + txtgen.Text + "' where uid=" + uid + "";
        //        }
        //        qur.ExecuteNonQuery();

        //        MessageBox.Show("Record has been Updated", "Save", MessageBoxButtons.OK);
        //    }

        //    ////SqlCommand scmd = new SqlCommand(qur, conn);

        //    ////scmd.Parameters.AddWithValue("@Itemgroup", SqlDbType.NVarChar).Value = txtgen.Text;
        //    ////scmd.Parameters.AddWithValue("@active", SqlDbType.NVarChar).Value = 1;
        //    ////scmd.Parameters.AddWithValue("@Sname", SqlDbType.NVarChar).Value = "";


        //    conn.Close();
        //    Loadgrid();
        //    Genpan.Visible = true;


        //}

        private void btnedit_Click(object sender, EventArgs e)
        {
            Genclass.Module.ClearTextBox(this, EditPnl);
            loadscreens();

            if (Genclass.Gbtxtid == 2 || Genclass.Gbtxtid == 3)
            {

                mode = 2;

                int i = HFGP.SelectedCells[0].RowIndex;
                txtgen.Text = HFGP.Rows[i].Cells[1].Value.ToString();
                uid = HFGP.Rows[i].Cells[0].Value.ToString();
                cbogrp.Text = HFGP.Rows[i].Cells[2].Value.ToString();
                if (HFGP.Rows[i].Cells[3].Value.ToString() == "True")
                {
                    Chkedtact.Checked = true;
                    Chkedtact.Text = "Active";
                }
                else
                {
                    Chkedtact.Checked = false;
                    Chkedtact.Text = "Inactive";
                }
            }
            else if (Genclass.Gbtxtid == 1 || Genclass.Gbtxtid == 4 || Genclass.Gbtxtid == 15 || Genclass.Gbtxtid == 5 || Genclass.Gbtxtid == 8 || Genclass.Gbtxtid == 9 || Genclass.Gbtxtid == 12 || Genclass.Gbtxtid == 13)
            {

                mode = 2;


                int i = HFGP.SelectedCells[0].RowIndex;
                txtgen.Text = HFGP.Rows[i].Cells[1].Value.ToString();
                uid = HFGP.Rows[i].Cells[0].Value.ToString();


                if (HFGP.Rows[i].Cells[2].Value.ToString() == "True")
                {
                    Chkedtact.Checked = true;

                }
                else
                {
                    Chkedtact.Checked = false;

                }

            }

            else if (Genclass.Gbtxtid == 10)
            {


                mode = 2;

                int i = HFGP.SelectedCells[0].RowIndex;
                txthsn.Text = HFGP.Rows[i].Cells[1].Value.ToString();
                txtsname.Text = HFGP.Rows[i].Cells[3].Value.ToString();
                Txthsnname.Text = HFGP.Rows[i].Cells[2].Value.ToString();
                cbocgst.Text = HFGP.Rows[i].Cells[4].Value.ToString();
                cbosgst.Text = HFGP.Rows[i].Cells[5].Value.ToString();
                cboigst.Text = HFGP.Rows[i].Cells[6].Value.ToString();
                uid = HFGP.Rows[i].Cells[0].Value.ToString();
                if (HFGP.Rows[i].Cells[7].Value.ToString() == "1")
                {
                    chkhsn.Checked = true;
                    //Chkedtact.Text = "Active";
                }
                else
                {
                    chkhsn.Checked = false;
                    //Chkedtact.Text = "Inactive";
                }

            }

            else if (Genclass.Gbtxtid == 11)
            {


                mode = 2;

                int i = HFGP.SelectedCells[0].RowIndex;
                txtax.Text = HFGP.Rows[i].Cells[1].Value.ToString();
                cbotax.Text = HFGP.Rows[i].Cells[3].Value.ToString();
                txtper.Text = HFGP.Rows[i].Cells[2].Value.ToString();
                //cbotax.SelectedValue = HFGP.Rows[i].Cells[4].Value.ToString();
                uid = HFGP.Rows[i].Cells[0].Value.ToString();
                if (HFGP.Rows[i].Cells[4].Value.ToString() == "True")
                {
                    chktax.Checked = true;
                    //Chkedtact.Text = "Active";
                }
                else
                {
                    chktax.Checked = false;
                    //Chkedtact.Text = "Inactive";
                }

            }

            else if (Genclass.Gbtxtid == 7)
            {


                mode = 2;

                int i = HFGP.SelectedCells[0].RowIndex;
                txtexcise.Text = HFGP.Rows[i].Cells[1].Value.ToString();
                txted.Text = HFGP.Rows[i].Cells[2].Value.ToString();
                txtcess.Text = HFGP.Rows[i].Cells[3].Value.ToString();
                txthecess.Text = HFGP.Rows[i].Cells[4].Value.ToString();
                uid = HFGP.Rows[i].Cells[0].Value.ToString();
                if (Convert.ToInt16(HFGP.Rows[i].Cells[5].Value.ToString()) == 1)
                {
                    Chkedtact.Checked = true;
                    Chkedtact.Text = "Active";
                }
                else
                {
                    Chkedtact.Checked = false;
                    Chkedtact.Text = "Inactive";
                }
            }
        }

        private void btndelete_Click(object sender, EventArgs e)
        {
            mode = 3;
            string message = "Are You Sure to Inctive this item ?";
            string caption = "Dilama";
            MessageBoxButtons buttons = MessageBoxButtons.YesNo;
            DialogResult result;

            result = MessageBox.Show(message, caption, buttons);

            if (result == System.Windows.Forms.DialogResult.Yes)
            {
                int i = HFGP.SelectedCells[0].RowIndex;

                //if (Genclass.Gbtxtid == 1)
                //{
                //    qur.CommandText = "Update ItemGroup set active=0 where uid=" + HFGP.Rows[i].Cells[0].Value.ToString() + "";
                //}
                //else if (Genclass.Gbtxtid == 2)
                //{
                //    qur.CommandText = "Update ItemsubGroup set active=0 where uid=" + HFGP.Rows[i].Cells[0].Value.ToString() + "";
                //}
                //else if (Genclass.Gbtxtid == 3)
                //{
                //    qur.CommandText = "Update Categorytypem set active=0 where uid=" + HFGP.Rows[i].Cells[0].Value.ToString() + "";
                //}
                //else if (Genclass.Gbtxtid == 4)
                //{
                //    qur.CommandText = "Update generalm set active=0 where uid=" + HFGP.Rows[i].Cells[0].Value.ToString() + "";
                //}
                //else if (Genclass.Gbtxtid == 5)
                //{
                //    qur.CommandText = "Update processm set active=0 where uid=" + HFGP.Rows[i].Cells[0].Value.ToString() + "";
                //}


                //conn.Open();
                //qur.ExecuteNonQuery();
                //MessageBox.Show("Record has been Deactivated", "Delete", MessageBoxButtons.OK);

                if (Genclass.Gbtxtid == 1 || Genclass.Gbtxtid == 5)
                {
                    qur.CommandText = "Exec Sp_Masters '" + txtgen.Text + "',0,0,0,0," + Chkedtact.Checked + ",'" + uid + "'," + Genclass.Gbtxtid + "," + mode + "," + Genclass.data1 + "";
                }
                else if (Genclass.Gbtxtid == 2 || Genclass.Gbtxtid == 3)
                {
                    qur.CommandText = "Exec Sp_Masters '" + txtgen.Text + "'," + cbogrp.SelectedValue + ",0,0,0," + Chkedtact.Checked + ",'" + uid + "'," + Genclass.Gbtxtid + "," + mode + "," + Genclass.data1 + "";
                }
                else if (Genclass.Gbtxtid == 4 || Genclass.Gbtxtid == 8 || Genclass.Gbtxtid == 9 || Genclass.Gbtxtid == 12 || Genclass.Gbtxtid == 13)
                {
                    qur.CommandText = "Exec Sp_Masters '" + txtgen.Text + "',1,0,0,0," + Chkedtact.Checked + ",'" + uid + "'," + Genclass.Gbtxtid + "," + mode + "," + Genclass.data1 + "";
                }


                qur.ExecuteNonQuery();

                string quy = "select * from Msgdes";
                Genclass.cmd = new SqlCommand(quy, conn);

                SqlDataAdapter aptr = new SqlDataAdapter(Genclass.cmd);
                DataTable tap = new DataTable();
                aptr.Fill(tap);

                MessageBox.Show(tap.Rows[0]["Msg"].ToString(), "Save", MessageBoxButtons.OK);


                conn.Close();
                Loadgrid();

            }

        }

        private void btnaddrcan_Click_1(object sender, EventArgs e)
        {

        }

        private void txtscr1_TextChanged(object sender, EventArgs e)
        {
            Loadgrid();
        }


        public bool True { get; set; }

        private void txtgen_TextChanged(object sender, EventArgs e)
        {

        }



        private void cbogrp_SelectedIndexChanged(object sender, EventArgs e)
        {
            //if (cbogrp.SelectedValue == null)
            //{
            //    MessageBox.Show("Enter the ItemGroup");
            //}
        }

        private void cbogrp_Click(object sender, EventArgs e)
        {
            //if (cbogrp.SelectedValue == null)
            //{
            //    MessageBox.Show("Enter the ItemGroup");
            //}
        }

        private void cbogrp_KeyDown(object sender, KeyEventArgs e)
        {
            //if (cbogrp.SelectedValue == null)
            //{
            //    MessageBox.Show("Enter the ItemGroup");
            //    cbogrp.Focus();
            //}
        }

        private void button2_Click(object sender, EventArgs e)
        {
            excisepan.Visible = false;
            Genpan.Visible = true;
            Loadgrid();
        }

        private void cmdexcieok_Click(object sender, EventArgs e)
        {
            if (txtax.Text == "")
            {
                MessageBox.Show("Enter the tax ");
                txtax.Focus();
                return;
            }

            if (txtper.Text == "")
            {
                MessageBox.Show("Enter the Taxpercentage ");
                txtper.Focus();
                return;
            }
            if (cbotax.SelectedValue == null)
            {
                MessageBox.Show("Select  the Taxtype");
                cbotax.Focus();
                return;

            }

            conn.Open();
            //string qur = "insert into ItemGroup values (@Itemgroup,@active,@Sname)";
            if (mode == 1)
            {

                if (Genclass.Gbtxtid == 6)
                {
                    qur.CommandText = "insert into GENERALM (Generalname,f1,typem_uid,active) values ('" + txtax.Text + "','" + txtper.Text + "','" + cbotax.SelectedValue + "',1)";
                }

                qur.ExecuteNonQuery();
                MessageBox.Show("Record has been saved", "Save", MessageBoxButtons.OK);
            }

            if (mode == 2)
            {
                if (Genclass.Gbtxtid == 6)
                {


                    qur.CommandText = "Update GENERALM set Generalname='" + txtax.Text + "',f1='" + txtper.Text + "',typem_uid='" + cbotax.SelectedValue + "' where uid=" + uid + "";
                }

                qur.ExecuteNonQuery();
                MessageBox.Show("Record has been Updated", "Save", MessageBoxButtons.OK);
            }

            ////SqlCommand scmd = new SqlCommand(qur, conn);

            ////scmd.Parameters.AddWithValue("@Itemgroup", SqlDbType.NVarChar).Value = txtgen.Text;
            ////scmd.Parameters.AddWithValue("@active", SqlDbType.NVarChar).Value = 1;
            ////scmd.Parameters.AddWithValue("@Sname", SqlDbType.NVarChar).Value = "";


            conn.Close();
            Loadgrid();
            Genpan.Visible = true;
        }

        private void button3_Click(object sender, EventArgs e)
        {
            taxpan.Visible = false;
            Genpan.Visible = true;
            Loadgrid();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (txtexcise.Text == "")
            {
                MessageBox.Show("Enter the exciseduty ");
                txtexcise.Focus();
                return;
            }

            if (txted.Text == "")
            {
                MessageBox.Show("Enter the ED ");
                txted.Focus();
                return;
            }

            conn.Open();
            //string qur = "insert into ItemGroup values (@Itemgroup,@active,@Sname)";
            //if (mode == 1)
            //{

            //    if (Genclass.Gbtxtid == 7)
            //    {
            //        qur.CommandText = "insert into GENERALM (Generalname,f1,f2,f3,typem_uid,active) values ('" + txtexcise.Text + "','" + txted.Text + "','" + txtcess.Text + "','" + txthecess.Text + "',5,1)";
            //    }

            //    qur.ExecuteNonQuery();
            //    MessageBox.Show("Record has been saved", "Save", MessageBoxButtons.OK);
            //}

            //if (mode == 2)
            //{
            //    if (Genclass.Gbtxtid == 7)
            //    {


            //        qur.CommandText = "Update GENERALM set Generalname='" + txtexcise.Text + "',f1='" + txted.Text + "',f2='" + txtcess.Text + "',f3='" + txthecess.Text + "',typem_uid=5 where uid=" + uid + "";
            //    }

            //    qur.ExecuteNonQuery();
            //    MessageBox.Show("Record has been Updated", "Save", MessageBoxButtons.OK);
            //}



            conn.Close();
            Loadgrid();
            Genpan.Visible = true;
        }

        private void HFGP_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void chkact_Click(object sender, EventArgs e)
        {


            Loadgrid();

        }

        private void Genpan_Paint(object sender, PaintEventArgs e)
        {

        }

        private void chkact_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void Chkedtact_Click(object sender, EventArgs e)
        {

        }

        private void cmdexcieok_Click_1(object sender, EventArgs e)
        {

        }



        private void chkact_CheckedChanged_1(object sender, EventArgs e)
        {

            Loadgrid();

        }

        private void Chkedtact_CheckedChanged(object sender, EventArgs e)
        {

            Loadgrid();
        }


        public void Loadtaxtype()
        {
            conn.Open();

            string qur = " select UId,Typename   from typem where uid in (6,7,8,9)  order by Typename";
            SqlCommand cmd = new SqlCommand(qur, conn);
            SqlDataAdapter apt = new SqlDataAdapter(cmd);
            DataTable tab = new DataTable();
            apt.Fill(tab);
            cbotax.DataSource = null;
            cbotax.DataSource = tab;
            cbotax.DisplayMember = "Typename";
            cbotax.ValueMember = "uid";
            //cbocgst.SelectedIndex = -1;
        }

        public void Loadtax()
        {
            conn.Open();

            //string qur = " select UId,f1 as CGST  from Generalm where Typem_uid in (4,5) and companyid=" + Genclass.data1 + " and active=1 order by Generalname";
            //SqlCommand cmd = new SqlCommand(qur, conn);
            //SqlDataAdapter apt = new SqlDataAdapter(cmd);
            //DataTable tab = new DataTable();
            //apt.Fill(tab);
            //cbocgst.DataSource = null;
            //cbocgst.DataSource = tab;
            //cbocgst.DisplayMember = "CGST";
            //cbocgst.ValueMember = "uid";
            ////cbocgst.SelectedIndex = -1;


            string qur1 = " select UId,Generalname,f1 as SGST from Generalm where Typem_uid in (6,7) and companyid=" + Genclass.data1 + " and active=1 order by Generalname";
            SqlCommand cmd1 = new SqlCommand(qur1, conn);
            SqlDataAdapter apt1 = new SqlDataAdapter(cmd1);
            DataTable tab1 = new DataTable();
            apt1.Fill(tab1);
            cbosgst.DataSource = null;
            cbosgst.DisplayMember = "Generalname";
            cbosgst.ValueMember = "Uid";
            cbosgst.DataSource = tab1;

            if (tab1.Rows.Count > 0)
            {
                txtgstper.Text = tab1.Rows[0]["SGST"].ToString();
            }
            //txtgstper.Text = tab1.Rows[0]["SGST"].ToString();
            //cboSGST.SelectedIndex = -1;


            //string qur2 = " select UId,f1 as IGST from Generalm where Typem_uid in (8,9) and companyid=" + Genclass.data1 + " and active=1 order by Generalname";
            //SqlCommand cmd2 = new SqlCommand(qur2, conn);
            //SqlDataAdapter apt2 = new SqlDataAdapter(cmd2);
            //DataTable tab2 = new DataTable();
            //apt2.Fill(tab2);
            //cboigst.DataSource = null;
            //cboigst.DataSource = tab2;
            //cboigst.DisplayMember = "IGST";
            //cboigst.ValueMember = "uid";
            ////cboigst.SelectedIndex = -1;



            conn.Close();
        }






        private void buttonhsnsave_Click(object sender, EventArgs e)
        {
        }

        private void chkact_CheckedChanged_2(object sender, EventArgs e)
        {
            Loadgrid();
        }

        private void buttnext1_Click(object sender, EventArgs e)
        {
            this.Dispose();
        }

        private void butexit_Click(object sender, EventArgs e)
        {
            mode = 3;
          
            string message = "Are You Sure to Inctive this item ?";
            string caption = "Dilama";
            MessageBoxButtons buttons = MessageBoxButtons.YesNo;
            DialogResult result;

            result = MessageBox.Show(message, caption, buttons);

            if (result == System.Windows.Forms.DialogResult.Yes)
            {
                int i = HFGP.SelectedCells[0].RowIndex;
                int uid = HFGP.SelectedCells[0].RowIndex;

                conn.Close();
                conn.Open();
                if (Genclass.Gbtxtid == 1 || Genclass.Gbtxtid == 5)
                {
                    if (Genclass.Gbtxtid == 5)
                    {
                        Genclass.strsql = "select * from itemm where categorytypem_uid=" + uid + " ";
                    }
                    else if (Genclass.Gbtxtid == 1)
                    {
                        Genclass.strsql = "select * from itemm where itemgroup_uid=" + HFGP.CurrentRow.Cells[0].Value.ToString() + " ";
                    }
                    Genclass.cmd = new SqlCommand(Genclass.strsql, conn);
                    SqlDataAdapter aptr4 = new SqlDataAdapter(Genclass.cmd);
                    DataTable tap4 = new DataTable();
                    aptr4.Fill(tap4);
                    if (tap4.Rows.Count == 0)
                    {
                        qur.CommandText = "Exec Sp_Masters '" + HFGP.CurrentRow.Cells[1].Value.ToString() + "',0,0,0,0," + Chkedtact.Checked + ",'" + uid + "'," + Genclass.Gbtxtid + "," + mode + "," + Genclass.data1 + "";
                        qur.ExecuteNonQuery();
                    }
                    else
                    {
                        MessageBox.Show("This itemgroup already refered in itemmaster");
                    }
                }
                else if (Genclass.Gbtxtid == 2 || Genclass.Gbtxtid == 11)
                {
                    if (Genclass.Gbtxtid == 2)
                    {
                        Genclass.strsql = "select * from itemm where itemsubgroup_uid=" + HFGP.CurrentRow.Cells[0].Value.ToString() + " ";
                    }
                    else if (Genclass.Gbtxtid == 11)
                    {
                        Genclass.strsql = "select * from itemm where tax=" + HFGP.CurrentRow.Cells[0].Value.ToString() + " ";
                    }


                        Genclass.cmd = new SqlCommand(Genclass.strsql, conn);
                        SqlDataAdapter aptr4 = new SqlDataAdapter(Genclass.cmd);
                        DataTable tap4 = new DataTable();
                        aptr4.Fill(tap4);
                  if (tap4.Rows.Count==0)
                  {
                      if (Genclass.Gbtxtid == 2)
                      {
                          qur.CommandText = "Exec Sp_Masters '" + HFGP.CurrentRow.Cells[1].Value.ToString() + "',0,0,0,0," + Chkedtact.Checked + ",'" + uid + "'," + Genclass.Gbtxtid + "," + mode + "," + Genclass.data1 + "";
                          qur.ExecuteNonQuery();
                      }
                      else if (Genclass.Gbtxtid == 11)
                      {
                          qur.CommandText = "delete from generalm where uid=" + HFGP.CurrentRow.Cells[0].Value.ToString()  + "";
                          qur.ExecuteNonQuery();
                      }

                  }
                  else
                  {
                      MessageBox.Show("This Brand already refered in itemmaster");
                  }
                }
                else if (Genclass.Gbtxtid == 4 || Genclass.Gbtxtid == 15 || Genclass.Gbtxtid == 8 || Genclass.Gbtxtid == 9 || Genclass.Gbtxtid == 12 || Genclass.Gbtxtid == 13)
                {
                    if (Genclass.Gbtxtid == 4)
                    {
                        Genclass.strsql = "select * from itemm where uom_uid=" + HFGP.CurrentRow.Cells[0].Value.ToString() + " ";

                        Genclass.cmd = new SqlCommand(Genclass.strsql, conn);
                        SqlDataAdapter aptr4 = new SqlDataAdapter(Genclass.cmd);
                        DataTable tap4 = new DataTable();
                        aptr4.Fill(tap4);
                        if (tap4.Rows.Count == 0)
                        {
                            qur.CommandText = "Exec Sp_Masters '" + txtgen.Text + "',1,0,0,0," + Chkedtact.Checked + ",'" + uid + "'," + Genclass.Gbtxtid + "," + mode + "," + Genclass.data1 + "";
                            qur.ExecuteNonQuery();
                        }
                    }
                    else
                    {
                        qur.CommandText = "Exec Sp_Masters '" + txtgen.Text + "',1,0,0,0," + Chkedtact.Checked + ",'" + uid + "'," + Genclass.Gbtxtid + "," + mode + "," + Genclass.data1 + "";
                        qur.ExecuteNonQuery();
                    }
                }


            

                string quy = "select * from Msgdes";
                Genclass.cmd = new SqlCommand(quy, conn);

                SqlDataAdapter aptr = new SqlDataAdapter(Genclass.cmd);
                DataTable tap = new DataTable();
                aptr.Fill(tap);

                //MessageBox.Show(tap.Rows[0]["Msg"].ToString(), "Save", MessageBoxButtons.OK);


                conn.Close();
                Loadgrid();

            }

        }

        private void butedit_Click(object sender, EventArgs e)
        {
            mode = 2;
            Genclass.Module.ClearTextBox(this, EditPnl);
            loadscreens();
            cbosgst.SelectedIndex = -1;
            cboigst.SelectedIndex = -1;




            if (Genclass.Gbtxtid == 1 || Genclass.Gbtxtid == 2 || Genclass.Gbtxtid == 4 || Genclass.Gbtxtid == 15 || Genclass.Gbtxtid == 5 || Genclass.Gbtxtid == 8 || Genclass.Gbtxtid == 9 || Genclass.Gbtxtid == 12 || Genclass.Gbtxtid == 13)
            {




                int i = HFGP.SelectedCells[0].RowIndex;
                txtgen.Text = HFGP.Rows[i].Cells[1].Value.ToString();
                uid = HFGP.Rows[i].Cells[0].Value.ToString();
                //txttax.Text = HFGP.Rows[i].Cells[4].Value.ToString();


                if (HFGP.Rows[i].Cells[2].Value.ToString() == "True")
                {
                    Chkedtact.Checked = true;

                }
                else
                {
                    Chkedtact.Checked = false;

                }

                if (Genclass.Gbtxtid == 1)
                {

                    //string quy = "select isnull(a.hsnid,0) as hsnid,isnull(Hsncode,'') as hsncode,isnull(hsnname,'') as hsnname from itemgroup a inner join hsndet b on a.hsnid=b.uid inner join Hsn c on b.hsnid=c.uid where a.uid=" + uid + "";
                    string quy = "select  uid,isnull(Hsncode,'') as hsncode,isnull(sname,0) as sname,isnull(hsnname,'') as hsnname,isnull(hsntype,'') as hsntype from itemgroup    where uid=" + uid + "";
                    Genclass.cmd = new SqlCommand(quy, conn);

                    SqlDataAdapter aptr = new SqlDataAdapter(Genclass.cmd);
                    DataTable tap = new DataTable();
                    aptr.Fill(tap);

                    if (tap.Rows.Count > 0)
                    {
                        txthsncode.Text = tap.Rows[0]["hsncode"].ToString();
                        txttax.Text = tap.Rows[0]["sname"].ToString();
                        cbotype.Text = tap.Rows[0]["hsntype"].ToString();
                        txthsss.Text = tap.Rows[0]["hsnname"].ToString();
                    }
                    else
                    {
                        cbogrp.SelectedIndex = -1;
                        txthsss.Text = "";
                    }


                }
            }
                  if (Genclass.Gbtxtid == 20)
                {
                    int i = HFGP.SelectedCells[0].RowIndex;
                    txtgen.Text = HFGP.Rows[i].Cells[1].Value.ToString();
                    uid = HFGP.Rows[i].Cells[0].Value.ToString();
                    //string quy = "select isnull(a.hsnid,0) as hsnid,isnull(Hsncode,'') as hsncode,isnull(hsnname,'') as hsnname from itemgroup a inner join hsndet b on a.hsnid=b.uid inner join Hsn c on b.hsnid=c.uid where a.uid=" + uid + "";
                    string quy = "select * from generalm where typem_uid=15    and uid=" + uid + "";
                    Genclass.cmd = new SqlCommand(quy, conn);

                    SqlDataAdapter aptr = new SqlDataAdapter(Genclass.cmd);
                    DataTable tap = new DataTable();
                    aptr.Fill(tap);

                    if (tap.Rows.Count > 0)
                    {
                        txtax.Text = tap.Rows[0]["Generalname"].ToString();
                        txtper.Text = tap.Rows[0]["f1"].ToString();
                        cbotax.Text = tap.Rows[0]["f2"].ToString();
                        DTPDOCDT.Text = tap.Rows[0]["f3"].ToString();
                    }
                   


                }
                  if (Genclass.Gbtxtid == 21)
                  {
                      int i = HFGP.SelectedCells[0].RowIndex;
                      txtgen.Text = HFGP.Rows[i].Cells[1].Value.ToString();
                      uid = HFGP.Rows[i].Cells[0].Value.ToString();
                      //string quy = "select isnull(a.hsnid,0) as hsnid,isnull(Hsncode,'') as hsncode,isnull(hsnname,'') as hsnname from itemgroup a inner join hsndet b on a.hsnid=b.uid inner join Hsn c on b.hsnid=c.uid where a.uid=" + uid + "";
                      string quy = "select * from generalm where typem_uid=16    and uid=" + uid + "";
                      Genclass.cmd = new SqlCommand(quy, conn);

                      SqlDataAdapter aptr = new SqlDataAdapter(Genclass.cmd);
                      DataTable tap = new DataTable();
                      aptr.Fill(tap);

                      if (tap.Rows.Count > 0)
                      {
                          txtax.Text = tap.Rows[0]["Generalname"].ToString();
                          txtper.Text = tap.Rows[0]["f1"].ToString();
                          //cbotax.Text = tap.Rows[0]["f2"].ToString();
                          DTPDOCDT.Text = tap.Rows[0]["f3"].ToString();
                      }



                  }
            
            if (Genclass.Gbtxtid == 3)
            {




                int i = HFGP.SelectedCells[0].RowIndex;
                txtgen.Text = HFGP.Rows[i].Cells[1].Value.ToString();
                cbogrp.Text = HFGP.Rows[i].Cells[2].Value.ToString();
                uid = HFGP.Rows[i].Cells[0].Value.ToString();


                if (HFGP.Rows[i].Cells[3].Value.ToString() == "True")
                {
                    Chkedtact.Checked = true;

                }
                else
                {
                    Chkedtact.Checked = false;

                }

            }


            else if (Genclass.Gbtxtid == 10)
            {


                int i = HFGP.SelectedCells[0].RowIndex;
                txthsn.Text = HFGP.Rows[i].Cells[1].Value.ToString();
                txthsnid.Text = HFGP.Rows[i].Cells[8].Value.ToString();
                txtsname.Text = HFGP.Rows[i].Cells[3].Value.ToString();
                Txthsnname.Text = HFGP.Rows[i].Cells[2].Value.ToString();
                cbocgst.Text = HFGP.Rows[i].Cells[4].Value.ToString();
                cbosgst.Text = HFGP.Rows[i].Cells[5].Value.ToString();
                cboigst.Text = HFGP.Rows[i].Cells[6].Value.ToString();
                uid = HFGP.Rows[i].Cells[0].Value.ToString();
                if (HFGP.Rows[i].Cells[7].Value.ToString() == "1")
                {
                    chkhsn.Checked = true;
                    //Chkedtact.Text = "Active";
                }
                else
                {
                    chkhsn.Checked = false;
                    //Chkedtact.Text = "Inactive";
                }

            }

            else if (Genclass.Gbtxtid == 11)
            {




                int i = HFGP.SelectedCells[0].RowIndex;
                txtax.Text = HFGP.Rows[i].Cells[1].Value.ToString();
                cbotax.Text = HFGP.Rows[i].Cells[3].Value.ToString();
                txtper.Text = HFGP.Rows[i].Cells[2].Value.ToString();
                //cbotax.SelectedValue = HFGP.Rows[i].Cells[4].Value.ToString();
                uid = HFGP.Rows[i].Cells[0].Value.ToString();
                if (HFGP.Rows[i].Cells[4].Value.ToString() == "True")
                {
                    chktax.Checked = true;
                    //Chkedtact.Text = "Active";
                }
                else
                {
                    chktax.Checked = false;
                    //Chkedtact.Text = "Inactive";
                }

            }

            else if (Genclass.Gbtxtid == 7)
            {




                int i = HFGP.SelectedCells[0].RowIndex;
                txtexcise.Text = HFGP.Rows[i].Cells[1].Value.ToString();
                txted.Text = HFGP.Rows[i].Cells[2].Value.ToString();
                txtcess.Text = HFGP.Rows[i].Cells[3].Value.ToString();
                txthecess.Text = HFGP.Rows[i].Cells[4].Value.ToString();
                uid = HFGP.Rows[i].Cells[0].Value.ToString();
                if (Convert.ToInt16(HFGP.Rows[i].Cells[5].Value.ToString()) == 1)
                {
                    Chkedtact.Checked = true;

                }
                else
                {
                    Chkedtact.Checked = false;

                }
            }
        }

        private void btnadd_Click_1(object sender, EventArgs e)
        {
            mode = 1;
            Genclass.Module.ClearTextBox(this, EditPnl);
            Genclass.Module.ClearTextBox(this, excisepan);
            Genclass.Module.ClearTextBox(this, taxpan);
            Genclass.Module.ClearTextBox(this, HSNpan);
            chkhsn.Checked = true;
            Chkedtact.Checked = true;
            loadscreens();
            uid = "";
        }

        private void HFGP_CellMouseUp(object sender, DataGridViewCellMouseEventArgs e)
        {
            if (HFGP.Rows[0].Cells[0].Value.ToString() != "")
            {
                int index = HFGP.SelectedRows[0].Index;
                if (index < HFGP.Rows.Count - 1)
                {
                    index = index + 1;
                    int index1 = HFGP.Rows.Count - 1;
                    lblno1.Text = index.ToString();
                    lblno2.Text = "of " + index1.ToString();
                }
            }
        }

        private void HFGP_KeyUp(object sender, KeyEventArgs e)
        {
            if (HFGP.Rows[0].Cells[0].Value.ToString() != "")
            {
                int index = HFGP.SelectedRows[0].Index;
                if (index < HFGP.Rows.Count - 1)
                {
                    index = index + 1;
                    int index1 = HFGP.Rows.Count - 1;
                    lblno1.Text = index.ToString();
                    lblno2.Text = "of " + index1.ToString();
                }
            }
        }

        private void buttnnvfst_Click(object sender, EventArgs e)
        {
            int index = HFGP.SelectedRows[0].Index;
            int index1 = 0;
            int index2 = HFGP.Rows.Count - 1;

            HFGP.Rows[index].Selected = false;
            HFGP.Rows[index1].Selected = true;
            index1 = index1 + 1;
            lblno1.Text = index1.ToString();
            lblno2.Text = "of " + index2.ToString();
        }

        private void buttnnxtlft_Click(object sender, EventArgs e)
        {
            int index = HFGP.SelectedRows[0].Index;
            int index1 = HFGP.Rows.Count - 1; ;
            if (index > 0)
            {
                HFGP.Rows[index].Selected = false;
                HFGP.Rows[index - 1].Selected = true;
                lblno1.Text = index.ToString();
                lblno2.Text = "of " + index1.ToString();
            }
        }

        private void buttrnxt_Click(object sender, EventArgs e)
        {

            int index = HFGP.SelectedRows[0].Index;
            int index2 = HFGP.SelectedRows[0].Index + 2;
            int index1 = HFGP.Rows.Count - 1; ;
            if (index < HFGP.Rows.Count - 2)
            {
                HFGP.Rows[index].Selected = false;
                HFGP.Rows[index + 1].Selected = true;
                lblno1.Text = index2.ToString();
                lblno2.Text = "of " + index1.ToString();

            }
        }

        private void btnfinnxt_Click(object sender, EventArgs e)
        {
            int index = HFGP.SelectedRows[0].Index;
            int index1 = HFGP.Rows.Count - 1;


            HFGP.Rows[index].Selected = false;

            HFGP.Rows[index1 - 1].Selected = true;
            lblno1.Text = index1.ToString();
            lblno2.Text = "of " + index1.ToString();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            this.Dispose();
        }




        private void button10_Click(object sender, EventArgs e)
        {
            this.Dispose();
        }

        private void cmdexcieok_Click_2(object sender, EventArgs e)
        {
            //if (cbotax.Text == "")
            //{
            //    MessageBox.Show("Enter the " + Genclass.Genlbl + "");
            //    cbotax.Focus();
            //    return;
            //}
            if (txtax.Text == "")
            {
                MessageBox.Show("Enter the " + Genclass.Genlbl1 + "");
                txtax.Focus();
                return;
            }
            if (txtper.Text == "")
            {
                MessageBox.Show("Enter the " + Genclass.Genlbl2 + "");
                txtper.Focus();
                return;
            }

            if (conn.State != ConnectionState.Open)
            {
                conn.Open();
            }


            if (uid == "")
            {
                uid = "0";
            }

            if (Genclass.Gbtxtid == 11)
            {
                //qur.CommandText = "Exec Sp_Masters '" + txtax.Text + "'," + cbotax.SelectedValue + "," + txtper.Text + ",0,0," + chktax.Checked + ",'" + uid + "'," + Genclass.Gbtxtid + "," + mode + "," + Genclass.data1 + "";
                qur.CommandText = "Exec Sp_Masters '" + txtax.Text + "',6," + txtper.Text + ",0,0," + chktax.Checked + ",'" + uid + "'," + Genclass.Gbtxtid + "," + mode + "," + Genclass.data1 + "";
            }
            else if (Genclass.Gbtxtid == 20)
            {

                qur.CommandText = "Exec Sp_Masters1 '" + txtax.Text + "'," + txtper.Text + ",'"+ cbotax.Text + "',15," + chktax.Checked + ",'" + uid + "'," + mode + "," + Genclass.data1 + ",'" + DTPDOCDT.Value + "'";
            }
            else if (Genclass.Gbtxtid == 21)
            {
                qur.CommandText = "Exec Sp_Masters1 '" + txtax.Text + "'," + txtper.Text + ",'0',16," + chktax.Checked + ",'" + uid + "'," + mode + "," + Genclass.data1 + ",'" + DTPDOCDT.Value + "'";
            }
            qur.ExecuteNonQuery();

            string quy = "select * from Msgdes";
            Genclass.cmd = new SqlCommand(quy, conn);

            SqlDataAdapter aptr = new SqlDataAdapter(Genclass.cmd);
            DataTable tap = new DataTable();
            aptr.Fill(tap);

            MessageBox.Show(tap.Rows[0]["Msg"].ToString(), "Save", MessageBoxButtons.OK);
            conn.Close();
            panadd.Visible = true;
            Taxsvpan.Visible = false;
            editsvpan.Visible = false;
            Genpan.Visible = true;
            panadd.Visible = true;
            EditPnl.Visible = false;
            taxpan.Visible = false;
            excisepan.Visible = false;
            Loadgrid();
            button2_Click(sender, e);
        }

        private void button2_Click_1(object sender, EventArgs e)
        {
            Genpan.Visible = true;
            panadd.Visible = true;
            Taxsvpan.Visible = false;
            EditPnl.Visible = false;
            HSNpan.Visible = false;
            excisepan.Visible = false;
        }

        private void button10_Click_1(object sender, EventArgs e)
        {
            this.Dispose();
        }

        private void btnsave_Click(object sender, EventArgs e)
        {

            if (txtgen.Text == "")
            {
                MessageBox.Show("Enter the " + Genclass.Genlbl + "");
                txtgen.Focus();
                return;
            }

            conn.Open();


            if (Genclass.Gbtxtid == 1)
            {

                if (txthsss.Text == "")
                {
                    txthsss.Text = "0";
                }
                if (mode==1)
                {
                    qur.CommandText = "insert into itemgroup values ('" + txtgen.Text + "',1,'0'," + Genclass.data1 + ",0,'0','" + cbotype.Text + "','0')";
                }
                else
                {
                    qur.CommandText = "update  itemgroup set itemgroup='" + txtgen.Text + "',companyid=" + Genclass.data1 + ",hsntype='" + cbotype.Text + "'  where uid="+ uid +"";
                }

            }
            else if (Genclass.Gbtxtid == 5 || Genclass.Gbtxtid == 2)
            {
                qur.CommandText = "Exec Sp_Masters '" + txtgen.Text + "',0,0,0,0," + Chkedtact.Checked + ",'" + uid + "'," + Genclass.Gbtxtid + "," + mode + "," + Genclass.data1 + "";
            }
        
            else if (Genclass.Gbtxtid == 3)
            {
                qur.CommandText = "Exec Sp_Masters '" + txtgen.Text + "'," + cbogrp.SelectedValue + ",0,0,0," + Chkedtact.Checked + ",'" + uid + "'," + Genclass.Gbtxtid + "," + mode + "," + Genclass.data1 + "";
            }
            else if (Genclass.Gbtxtid == 30)
            {
                qur.CommandText = "Exec Sp_Masters '" + txtgen.Text + "',17,0,0,0," + Chkedtact.Checked + ",'" + uid + "'," + Genclass.Gbtxtid + "," + mode + "," + Genclass.data1 + "";
            }
            else if (Genclass.Gbtxtid == 4)
            {
                qur.CommandText = "Exec Sp_Masters '" + txtgen.Text + "',1,0,0,0," + Chkedtact.Checked + ",'" + uid + "'," + Genclass.Gbtxtid + "," + mode + "," + Genclass.data1 + "";
            }
            else if (Genclass.Gbtxtid == 8)
            {
                qur.CommandText = "Exec Sp_Masters '" + txtgen.Text + "',10,0,0,0," + Chkedtact.Checked + ",'" + uid + "'," + Genclass.Gbtxtid + "," + mode + "," + Genclass.data1 + "";
            }
            else if (Genclass.Gbtxtid == 9)
            {
                qur.CommandText = "Exec Sp_Masters '" + txtgen.Text + "',11,0,0,0," + Chkedtact.Checked + ",'" + uid + "'," + Genclass.Gbtxtid + "," + mode + "," + Genclass.data1 + "";

            }
            else if (Genclass.Gbtxtid == 10)
            {
                qur.CommandText = "Exec Sp_Masters '" + txtgen.Text + "',12,'" + txthsn.Text + "',0,0," + Chkedtact.Checked + ",'" + uid + "'," + Genclass.Gbtxtid + "," + mode + "," + Genclass.data1 + "";
                txthsn.Visible = false;
            }
            else if (Genclass.Gbtxtid == 12)
            {
                qur.CommandText = "Exec Sp_Masters '" + txtgen.Text + "',14,0,0,0," + Chkedtact.Checked + ",'" + uid + "'," + Genclass.Gbtxtid + "," + mode + "," + Genclass.data1 + "";

            }
            else if (Genclass.Gbtxtid == 13)
            {
                qur.CommandText = "Exec Sp_Masters '" + txtgen.Text + "',13,0,0,0," + Chkedtact.Checked + ",'" + uid + "'," + Genclass.Gbtxtid + "," + mode + "," + Genclass.data1 + "";

            }
            else if (Genclass.Gbtxtid == 15)

                if (Chkedtact.Checked == true)
                {
                    qur.CommandText = "insert into Generalm values (14,'" + txtgen.Text + "',1,null,null,0," + Genclass.data1 + ")";
                }
                else
                {
                    qur.CommandText = "insert into Generalm values (14,'" + txtgen.Text + "',0,null,null,0," + Genclass.data1 + ")";
                }



            qur.ExecuteNonQuery();

            //string quy = "select * from Msgdes";
            //Genclass.cmd = new SqlCommand(quy, conn);

            //SqlDataAdapter aptr = new SqlDataAdapter(Genclass.cmd);
            //DataTable tap = new DataTable();
            //aptr.Fill(tap);

            //MessageBox.Show(tap.Rows[0]["Msg"].ToString(), "Save", MessageBoxButtons.OK);


            conn.Close();
            Loadgrid();
            Genpan.Visible = true;
            panadd.Visible = true;
            EditPnl.Visible = false;
            taxpan.Visible = false;
            excisepan.Visible = false;
            editsvpan.Visible = false;
        }

        private void btnaddrcan_Click_3(object sender, EventArgs e)
        {
            panadd.Visible = true;
            editsvpan.Visible = false;
            txthsn.Visible = false;
            txtgen.Text = "";
            Genpan.Visible = true;
            EditPnl.Visible = false;
            Loadgrid();
        }

        private void button6_Click(object sender, EventArgs e)
        {
            this.Dispose();
        }

        private void button5_Click_1(object sender, EventArgs e)
        {
            panadd.Visible = true;

            HSNpan.Visible = false;
            Genpan.Visible = true;
            Hsnsvpan.Visible = false;
        }

        private void buttonhsnsave_Click_1(object sender, EventArgs e)
        {

            conn.Open();
            conn.Close();
            HSNpan.Visible = false;

            Genpan.Visible = true;
            panadd.Visible = true;
            EditPnl.Visible = false;
            taxpan.Visible = false;
            excisepan.Visible = false;

            Loadgrid();
        }

        private void cbogrp_SelectedValueChanged(object sender, EventArgs e)
        {
            if (mode == 1)
            {
                if (cbogrp.SelectedIndex != 0 || cbogrp.SelectedIndex != -1)
                {
                    string quy = "select * from hsn where uid=2758";
                    Genclass.cmd = new SqlCommand(quy, conn);

                    SqlDataAdapter aptr = new SqlDataAdapter(Genclass.cmd);
                    DataTable tap = new DataTable();
                    aptr.Fill(tap);


                    //txthsss.Text = tap.Rows[0]["hsnname"].ToString();
                }
            }
            else
            {
                if (cbogrp.SelectedIndex == 0 || cbogrp.SelectedIndex == -1)
                {
                }
                else
                {
                    string quy = "select * from hsn where uid=" + cbogrp.SelectedValue + "";
                    Genclass.cmd = new SqlCommand(quy, conn);

                    SqlDataAdapter aptr = new SqlDataAdapter(Genclass.cmd);
                    DataTable tap = new DataTable();
                    aptr.Fill(tap);


                    txthsss.Text = tap.Rows[0]["hsnname"].ToString();
                }
            }
        }

        private void txthsn_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                Genclass.Module.Partylistviewcont("uid", "Hsncode", Genclass.strsql, this, txthsnid, txthsn, HSNpan);
                Genclass.strsql = "select top 25 uid,hsncode from hsn where active=1 ";
                Genclass.FSSQLSortStr = "hsncode";


                Genclass.cmd = new SqlCommand(Genclass.strsql, conn);
                SqlDataAdapter aptr = new SqlDataAdapter(Genclass.cmd);
                DataTable tap = new DataTable();
                aptr.Fill(tap);
                Frmlookup contc = new Frmlookup();
                DataGridView dt = (DataGridView)contc.Controls["HFGP"];
                dt.Refresh();
                dt.ColumnCount = tap.Columns.Count;
                dt.Columns[0].Visible = false;
                dt.Columns[1].Width = 530;


                dt.DefaultCellStyle.Font = new Font("Arial", 10);

                dt.ColumnHeadersDefaultCellStyle.Font = new Font("Arial", 10, FontStyle.Bold);
                dt.AutoGenerateColumns = false;

                Genclass.i = 0;
                foreach (DataColumn column in tap.Columns)
                {
                    dt.Columns[Genclass.i].Name = column.ColumnName;
                    dt.Columns[Genclass.i].HeaderText = column.ColumnName;
                    dt.Columns[Genclass.i].DataPropertyName = column.ColumnName;
                    Genclass.i = Genclass.i + 1;
                }

                dt.DataSource = tap;
                conn.Close();
                contc.Show();
            }
        }

        private void txthsnid_KeyDown(object sender, KeyEventArgs e)
        {

        }

        private void txthsnid_TextChanged(object sender, EventArgs e)
        {
            if (txthsnid.Text != "")
            {
                string quy = "select * from hsn where uid=" + txthsnid.Text + "";
                Genclass.cmd = new SqlCommand(quy, conn);

                SqlDataAdapter aptr = new SqlDataAdapter(Genclass.cmd);
                DataTable tap = new DataTable();
                aptr.Fill(tap);


                Txthsnname.Text = tap.Rows[0]["hsnname"].ToString();
            }

        }

        private void cbosgst_Click(object sender, EventArgs e)
        {

        }

        private void cbosgst_SelectedValueChanged(object sender, EventArgs e)
        {
            if (cbosgst.SelectedIndex == -1 || cbosgst.SelectedIndex == -1)
            {
            }
            else
            {
                string quy = "select UId,Generalname,f1 as SGST from Generalm where Typem_uid in (6,7) and companyid=" + Genclass.data1 + " and active=1 and uid=" + cbosgst.SelectedValue + " order by Generalname";
                Genclass.cmd = new SqlCommand(quy, conn);

                SqlDataAdapter aptr = new SqlDataAdapter(Genclass.cmd);
                DataTable tap = new DataTable();
                aptr.Fill(tap);


                txtgstper.Text = tap.Rows[0]["SGST"].ToString();
            }

        }

        private void cbosgst_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void txthsncode_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            Loadgrid();
        }




    }
}


