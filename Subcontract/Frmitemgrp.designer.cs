﻿namespace ACV
{
    partial class Frmitemgrp
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Frmitemgrp));
            this.Genpan = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.HFGP = new System.Windows.Forms.DataGridView();
            this.txtscr1 = new System.Windows.Forms.TextBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.lblno1 = new System.Windows.Forms.Label();
            this.lblno2 = new System.Windows.Forms.Label();
            this.flowLayoutPanel3 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel2 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.buttnnvfst = new System.Windows.Forms.Button();
            this.buttnnxtlft = new System.Windows.Forms.Button();
            this.btnfinnxt = new System.Windows.Forms.Button();
            this.buttrnxt = new System.Windows.Forms.Button();
            this.txtscr2 = new System.Windows.Forms.TextBox();
            this.EditPnl = new System.Windows.Forms.Panel();
            this.txthsncode = new System.Windows.Forms.TextBox();
            this.cbotype = new System.Windows.Forms.ComboBox();
            this.label17 = new System.Windows.Forms.Label();
            this.txttax = new System.Windows.Forms.TextBox();
            this.txthsss = new System.Windows.Forms.RichTextBox();
            this.lblhsss = new System.Windows.Forms.Label();
            this.txtgen = new System.Windows.Forms.RichTextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.lblgen = new System.Windows.Forms.Label();
            this.cbogrp = new System.Windows.Forms.ComboBox();
            this.lblgrp = new System.Windows.Forms.Label();
            this.richTextBox1 = new System.Windows.Forms.RichTextBox();
            this.excisepan = new System.Windows.Forms.Panel();
            this.DTPDOCDT = new System.Windows.Forms.DateTimePicker();
            this.label18 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.cbotax = new System.Windows.Forms.ComboBox();
            this.txtax = new System.Windows.Forms.TextBox();
            this.txtper = new System.Windows.Forms.TextBox();
            this.HSNpan = new System.Windows.Forms.Panel();
            this.label16 = new System.Windows.Forms.Label();
            this.txtgstper = new System.Windows.Forms.TextBox();
            this.SGST = new System.Windows.Forms.Label();
            this.cbosgst = new System.Windows.Forms.ComboBox();
            this.label13 = new System.Windows.Forms.Label();
            this.txtsname = new System.Windows.Forms.TextBox();
            this.Txthsnname = new System.Windows.Forms.RichTextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.cbocgst = new System.Windows.Forms.ComboBox();
            this.txthsn = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.cboigst = new System.Windows.Forms.ComboBox();
            this.txthsnid = new System.Windows.Forms.TextBox();
            this.panadd = new System.Windows.Forms.Panel();
            this.buttnext1 = new System.Windows.Forms.Button();
            this.chkact = new System.Windows.Forms.CheckBox();
            this.butedit = new System.Windows.Forms.Button();
            this.btnadd = new System.Windows.Forms.Button();
            this.butexit = new System.Windows.Forms.Button();
            this.Hsnsvpan = new System.Windows.Forms.Panel();
            this.button4 = new System.Windows.Forms.Button();
            this.chkhsn = new System.Windows.Forms.CheckBox();
            this.buttonhsnsave = new System.Windows.Forms.Button();
            this.button5 = new System.Windows.Forms.Button();
            this.Taxsvpan = new System.Windows.Forms.Panel();
            this.chktax = new System.Windows.Forms.CheckBox();
            this.cmdexcieok = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button10 = new System.Windows.Forms.Button();
            this.taxpan = new System.Windows.Forms.Panel();
            this.txtcess = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.txthecess = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.txted = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txtexcise = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.editsvpan = new System.Windows.Forms.Panel();
            this.button6 = new System.Windows.Forms.Button();
            this.btnsave = new System.Windows.Forms.Button();
            this.btnaddrcan = new System.Windows.Forms.Button();
            this.Chkedtact = new System.Windows.Forms.CheckBox();
            this.Genpan.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.HFGP)).BeginInit();
            this.panel1.SuspendLayout();
            this.EditPnl.SuspendLayout();
            this.excisepan.SuspendLayout();
            this.HSNpan.SuspendLayout();
            this.panadd.SuspendLayout();
            this.Hsnsvpan.SuspendLayout();
            this.Taxsvpan.SuspendLayout();
            this.taxpan.SuspendLayout();
            this.editsvpan.SuspendLayout();
            this.SuspendLayout();
            // 
            // Genpan
            // 
            this.Genpan.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.Genpan.Controls.Add(this.label1);
            this.Genpan.Controls.Add(this.label9);
            this.Genpan.Controls.Add(this.HFGP);
            this.Genpan.Controls.Add(this.txtscr1);
            this.Genpan.Controls.Add(this.panel1);
            this.Genpan.Controls.Add(this.buttnnvfst);
            this.Genpan.Controls.Add(this.buttnnxtlft);
            this.Genpan.Controls.Add(this.btnfinnxt);
            this.Genpan.Controls.Add(this.buttrnxt);
            this.Genpan.Controls.Add(this.txtscr2);
            this.Genpan.Location = new System.Drawing.Point(0, 0);
            this.Genpan.Name = "Genpan";
            this.Genpan.Size = new System.Drawing.Size(584, 455);
            this.Genpan.TabIndex = 83;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.label1.Font = new System.Drawing.Font("Calibri", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(6, 21);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(52, 21);
            this.label1.TabIndex = 94;
            this.label1.Text = "lblgen";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(614, 23);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(0, 18);
            this.label9.TabIndex = 93;
            // 
            // HFGP
            // 
            this.HFGP.BackgroundColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.HFGP.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.HFGP.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.HFGP.Location = new System.Drawing.Point(1, 73);
            this.HFGP.Name = "HFGP";
            this.HFGP.Size = new System.Drawing.Size(583, 381);
            this.HFGP.TabIndex = 3;
            this.HFGP.CellMouseUp += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.HFGP_CellMouseUp);
            this.HFGP.KeyUp += new System.Windows.Forms.KeyEventHandler(this.HFGP_KeyUp);
            // 
            // txtscr1
            // 
            this.txtscr1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtscr1.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtscr1.Location = new System.Drawing.Point(1, 44);
            this.txtscr1.Margin = new System.Windows.Forms.Padding(4);
            this.txtscr1.Name = "txtscr1";
            this.txtscr1.Size = new System.Drawing.Size(380, 26);
            this.txtscr1.TabIndex = 1;
            this.txtscr1.TextChanged += new System.EventHandler(this.txtscr1_TextChanged);
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel1.Controls.Add(this.lblno1);
            this.panel1.Controls.Add(this.lblno2);
            this.panel1.Controls.Add(this.flowLayoutPanel3);
            this.panel1.Controls.Add(this.flowLayoutPanel2);
            this.panel1.Controls.Add(this.flowLayoutPanel1);
            this.panel1.Location = new System.Drawing.Point(88, 331);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(74, 30);
            this.panel1.TabIndex = 214;
            // 
            // lblno1
            // 
            this.lblno1.AutoSize = true;
            this.lblno1.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblno1.ForeColor = System.Drawing.Color.Black;
            this.lblno1.Location = new System.Drawing.Point(4, 5);
            this.lblno1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblno1.Name = "lblno1";
            this.lblno1.Size = new System.Drawing.Size(15, 16);
            this.lblno1.TabIndex = 163;
            this.lblno1.Text = "1";
            // 
            // lblno2
            // 
            this.lblno2.AutoSize = true;
            this.lblno2.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblno2.ForeColor = System.Drawing.Color.Black;
            this.lblno2.Location = new System.Drawing.Point(27, 5);
            this.lblno2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblno2.Name = "lblno2";
            this.lblno2.Size = new System.Drawing.Size(29, 16);
            this.lblno2.TabIndex = 162;
            this.lblno2.Text = "of 1";
            // 
            // flowLayoutPanel3
            // 
            this.flowLayoutPanel3.Location = new System.Drawing.Point(61, 30);
            this.flowLayoutPanel3.Name = "flowLayoutPanel3";
            this.flowLayoutPanel3.Size = new System.Drawing.Size(200, 100);
            this.flowLayoutPanel3.TabIndex = 2;
            // 
            // flowLayoutPanel2
            // 
            this.flowLayoutPanel2.Location = new System.Drawing.Point(38, 35);
            this.flowLayoutPanel2.Name = "flowLayoutPanel2";
            this.flowLayoutPanel2.Size = new System.Drawing.Size(200, 100);
            this.flowLayoutPanel2.TabIndex = 1;
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.Location = new System.Drawing.Point(62, -129);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(200, 100);
            this.flowLayoutPanel1.TabIndex = 0;
            // 
            // buttnnvfst
            // 
            this.buttnnvfst.BackColor = System.Drawing.Color.White;
            this.buttnnvfst.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.buttnnvfst.FlatAppearance.BorderSize = 0;
            this.buttnnvfst.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttnnvfst.Image = ((System.Drawing.Image)(resources.GetObject("buttnnvfst.Image")));
            this.buttnnvfst.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttnnvfst.Location = new System.Drawing.Point(30, 331);
            this.buttnnvfst.Name = "buttnnvfst";
            this.buttnnvfst.Size = new System.Drawing.Size(19, 31);
            this.buttnnvfst.TabIndex = 213;
            this.buttnnvfst.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.buttnnvfst.UseVisualStyleBackColor = false;
            this.buttnnvfst.Click += new System.EventHandler(this.buttnnvfst_Click);
            // 
            // buttnnxtlft
            // 
            this.buttnnxtlft.BackColor = System.Drawing.Color.White;
            this.buttnnxtlft.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.buttnnxtlft.FlatAppearance.BorderSize = 0;
            this.buttnnxtlft.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttnnxtlft.Image = ((System.Drawing.Image)(resources.GetObject("buttnnxtlft.Image")));
            this.buttnnxtlft.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttnnxtlft.Location = new System.Drawing.Point(64, 331);
            this.buttnnxtlft.Name = "buttnnxtlft";
            this.buttnnxtlft.Size = new System.Drawing.Size(18, 31);
            this.buttnnxtlft.TabIndex = 212;
            this.buttnnxtlft.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.buttnnxtlft.UseVisualStyleBackColor = false;
            this.buttnnxtlft.Click += new System.EventHandler(this.buttnnxtlft_Click);
            // 
            // btnfinnxt
            // 
            this.btnfinnxt.BackColor = System.Drawing.Color.White;
            this.btnfinnxt.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.btnfinnxt.FlatAppearance.BorderSize = 0;
            this.btnfinnxt.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnfinnxt.Image = ((System.Drawing.Image)(resources.GetObject("btnfinnxt.Image")));
            this.btnfinnxt.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnfinnxt.Location = new System.Drawing.Point(202, 331);
            this.btnfinnxt.Name = "btnfinnxt";
            this.btnfinnxt.Size = new System.Drawing.Size(19, 31);
            this.btnfinnxt.TabIndex = 211;
            this.btnfinnxt.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnfinnxt.UseVisualStyleBackColor = false;
            this.btnfinnxt.Click += new System.EventHandler(this.btnfinnxt_Click);
            // 
            // buttrnxt
            // 
            this.buttrnxt.BackColor = System.Drawing.Color.White;
            this.buttrnxt.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.buttrnxt.FlatAppearance.BorderSize = 0;
            this.buttrnxt.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttrnxt.Image = ((System.Drawing.Image)(resources.GetObject("buttrnxt.Image")));
            this.buttrnxt.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttrnxt.Location = new System.Drawing.Point(168, 331);
            this.buttrnxt.Name = "buttrnxt";
            this.buttrnxt.Size = new System.Drawing.Size(18, 31);
            this.buttrnxt.TabIndex = 210;
            this.buttrnxt.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.buttrnxt.UseVisualStyleBackColor = false;
            this.buttrnxt.Click += new System.EventHandler(this.buttrnxt_Click);
            // 
            // txtscr2
            // 
            this.txtscr2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtscr2.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtscr2.Location = new System.Drawing.Point(357, 122);
            this.txtscr2.Margin = new System.Windows.Forms.Padding(4);
            this.txtscr2.Name = "txtscr2";
            this.txtscr2.Size = new System.Drawing.Size(100, 26);
            this.txtscr2.TabIndex = 95;
            this.txtscr2.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            // 
            // EditPnl
            // 
            this.EditPnl.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.EditPnl.Controls.Add(this.txthsncode);
            this.EditPnl.Controls.Add(this.cbotype);
            this.EditPnl.Controls.Add(this.label17);
            this.EditPnl.Controls.Add(this.txttax);
            this.EditPnl.Controls.Add(this.txthsss);
            this.EditPnl.Controls.Add(this.lblhsss);
            this.EditPnl.Controls.Add(this.txtgen);
            this.EditPnl.Controls.Add(this.label14);
            this.EditPnl.Controls.Add(this.lblgen);
            this.EditPnl.Controls.Add(this.cbogrp);
            this.EditPnl.Controls.Add(this.lblgrp);
            this.EditPnl.Controls.Add(this.richTextBox1);
            this.EditPnl.Location = new System.Drawing.Point(1, 0);
            this.EditPnl.Name = "EditPnl";
            this.EditPnl.Size = new System.Drawing.Size(582, 453);
            this.EditPnl.TabIndex = 91;
            // 
            // txthsncode
            // 
            this.txthsncode.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txthsncode.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txthsncode.Location = new System.Drawing.Point(18, 198);
            this.txthsncode.Margin = new System.Windows.Forms.Padding(5);
            this.txthsncode.Name = "txthsncode";
            this.txthsncode.Size = new System.Drawing.Size(287, 26);
            this.txthsncode.TabIndex = 107;
            this.txthsncode.TextChanged += new System.EventHandler(this.txthsncode_TextChanged);
            // 
            // cbotype
            // 
            this.cbotype.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cbotype.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cbotype.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbotype.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbotype.FormattingEnabled = true;
            this.cbotype.Items.AddRange(new object[] {
            "Goods",
            "Services"});
            this.cbotype.Location = new System.Drawing.Point(20, 130);
            this.cbotype.Name = "cbotype";
            this.cbotype.Size = new System.Drawing.Size(285, 26);
            this.cbotype.TabIndex = 106;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Calibri", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.Location = new System.Drawing.Point(310, 177);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(44, 21);
            this.label17.TabIndex = 105;
            this.label17.Text = "Tax%";
            // 
            // txttax
            // 
            this.txttax.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txttax.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txttax.Location = new System.Drawing.Point(313, 198);
            this.txttax.Margin = new System.Windows.Forms.Padding(5);
            this.txttax.Name = "txttax";
            this.txttax.Size = new System.Drawing.Size(64, 26);
            this.txttax.TabIndex = 104;
            // 
            // txthsss
            // 
            this.txthsss.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txthsss.Location = new System.Drawing.Point(17, 260);
            this.txthsss.Name = "txthsss";
            this.txthsss.Size = new System.Drawing.Size(530, 106);
            this.txthsss.TabIndex = 101;
            this.txthsss.Text = "";
            // 
            // lblhsss
            // 
            this.lblhsss.AutoSize = true;
            this.lblhsss.Font = new System.Drawing.Font("Calibri", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblhsss.Location = new System.Drawing.Point(17, 241);
            this.lblhsss.Name = "lblhsss";
            this.lblhsss.Size = new System.Drawing.Size(82, 21);
            this.lblhsss.TabIndex = 100;
            this.lblhsss.Text = "Hsn Name";
            // 
            // txtgen
            // 
            this.txtgen.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtgen.Location = new System.Drawing.Point(20, 57);
            this.txtgen.Name = "txtgen";
            this.txtgen.Size = new System.Drawing.Size(530, 28);
            this.txtgen.TabIndex = 99;
            this.txtgen.Text = "";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Calibri", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(21, 38);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(52, 21);
            this.label14.TabIndex = 91;
            this.label14.Text = "label2";
            // 
            // lblgen
            // 
            this.lblgen.AutoSize = true;
            this.lblgen.Font = new System.Drawing.Font("Calibri", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblgen.Location = new System.Drawing.Point(21, 105);
            this.lblgen.Name = "lblgen";
            this.lblgen.Size = new System.Drawing.Size(126, 21);
            this.lblgen.TabIndex = 91;
            this.lblgen.Text = "Item Group Type";
            // 
            // cbogrp
            // 
            this.cbogrp.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cbogrp.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cbogrp.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbogrp.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbogrp.FormattingEnabled = true;
            this.cbogrp.Location = new System.Drawing.Point(17, 197);
            this.cbogrp.Name = "cbogrp";
            this.cbogrp.Size = new System.Drawing.Size(288, 24);
            this.cbogrp.TabIndex = 98;
            this.cbogrp.SelectedValueChanged += new System.EventHandler(this.cbogrp_SelectedValueChanged);
            // 
            // lblgrp
            // 
            this.lblgrp.AutoSize = true;
            this.lblgrp.Font = new System.Drawing.Font("Calibri", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblgrp.Location = new System.Drawing.Point(18, 180);
            this.lblgrp.Name = "lblgrp";
            this.lblgrp.Size = new System.Drawing.Size(90, 21);
            this.lblgrp.TabIndex = 95;
            this.lblgrp.Text = "Item Group";
            // 
            // richTextBox1
            // 
            this.richTextBox1.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.richTextBox1.Location = new System.Drawing.Point(21, 57);
            this.richTextBox1.Name = "richTextBox1";
            this.richTextBox1.Size = new System.Drawing.Size(530, 28);
            this.richTextBox1.TabIndex = 99;
            this.richTextBox1.Text = "";
            // 
            // excisepan
            // 
            this.excisepan.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.excisepan.Controls.Add(this.DTPDOCDT);
            this.excisepan.Controls.Add(this.label18);
            this.excisepan.Controls.Add(this.label4);
            this.excisepan.Controls.Add(this.label2);
            this.excisepan.Controls.Add(this.label3);
            this.excisepan.Controls.Add(this.cbotax);
            this.excisepan.Controls.Add(this.txtax);
            this.excisepan.Controls.Add(this.txtper);
            this.excisepan.Location = new System.Drawing.Point(1, 1);
            this.excisepan.Name = "excisepan";
            this.excisepan.Size = new System.Drawing.Size(582, 454);
            this.excisepan.TabIndex = 107;
            // 
            // DTPDOCDT
            // 
            this.DTPDOCDT.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DTPDOCDT.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.DTPDOCDT.Location = new System.Drawing.Point(101, 333);
            this.DTPDOCDT.Margin = new System.Windows.Forms.Padding(4);
            this.DTPDOCDT.Name = "DTPDOCDT";
            this.DTPDOCDT.Size = new System.Drawing.Size(113, 26);
            this.DTPDOCDT.TabIndex = 222;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Calibri", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.Location = new System.Drawing.Point(99, 300);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(113, 21);
            this.label18.TabIndex = 105;
            this.label18.Text = "Tax Percentage";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Calibri", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(101, 236);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(113, 21);
            this.label4.TabIndex = 103;
            this.label4.Text = "Tax Percentage";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Calibri", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(101, 176);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(114, 21);
            this.label2.TabIndex = 97;
            this.label2.Text = "Tax Description";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(101, 111);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(60, 18);
            this.label3.TabIndex = 101;
            this.label3.Text = "Tax Type";
            this.label3.Visible = false;
            // 
            // cbotax
            // 
            this.cbotax.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cbotax.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cbotax.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbotax.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbotax.FormattingEnabled = true;
            this.cbotax.Location = new System.Drawing.Point(104, 130);
            this.cbotax.Name = "cbotax";
            this.cbotax.Size = new System.Drawing.Size(232, 26);
            this.cbotax.TabIndex = 102;
            this.cbotax.Visible = false;
            // 
            // txtax
            // 
            this.txtax.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtax.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtax.Location = new System.Drawing.Point(103, 199);
            this.txtax.Margin = new System.Windows.Forms.Padding(4);
            this.txtax.Name = "txtax";
            this.txtax.Size = new System.Drawing.Size(386, 26);
            this.txtax.TabIndex = 98;
            // 
            // txtper
            // 
            this.txtper.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtper.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtper.Location = new System.Drawing.Point(104, 261);
            this.txtper.Margin = new System.Windows.Forms.Padding(4);
            this.txtper.Name = "txtper";
            this.txtper.Size = new System.Drawing.Size(61, 26);
            this.txtper.TabIndex = 104;
            // 
            // HSNpan
            // 
            this.HSNpan.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.HSNpan.Controls.Add(this.label16);
            this.HSNpan.Controls.Add(this.txtgstper);
            this.HSNpan.Controls.Add(this.SGST);
            this.HSNpan.Controls.Add(this.cbosgst);
            this.HSNpan.Controls.Add(this.label13);
            this.HSNpan.Controls.Add(this.txtsname);
            this.HSNpan.Controls.Add(this.Txthsnname);
            this.HSNpan.Controls.Add(this.label10);
            this.HSNpan.Controls.Add(this.label11);
            this.HSNpan.Controls.Add(this.label12);
            this.HSNpan.Controls.Add(this.cbocgst);
            this.HSNpan.Controls.Add(this.txthsn);
            this.HSNpan.Controls.Add(this.label15);
            this.HSNpan.Controls.Add(this.cboigst);
            this.HSNpan.Controls.Add(this.txthsnid);
            this.HSNpan.Location = new System.Drawing.Point(1, 1);
            this.HSNpan.Name = "HSNpan";
            this.HSNpan.Size = new System.Drawing.Size(583, 453);
            this.HSNpan.TabIndex = 108;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(270, 20);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(45, 18);
            this.label16.TabIndex = 114;
            this.label16.Text = "GST %";
            // 
            // txtgstper
            // 
            this.txtgstper.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtgstper.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtgstper.Location = new System.Drawing.Point(272, 39);
            this.txtgstper.Margin = new System.Windows.Forms.Padding(4);
            this.txtgstper.Name = "txtgstper";
            this.txtgstper.Size = new System.Drawing.Size(50, 26);
            this.txtgstper.TabIndex = 113;
            // 
            // SGST
            // 
            this.SGST.AutoSize = true;
            this.SGST.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SGST.Location = new System.Drawing.Point(157, 18);
            this.SGST.Name = "SGST";
            this.SGST.Size = new System.Drawing.Size(31, 18);
            this.SGST.TabIndex = 110;
            this.SGST.Text = "GST";
            // 
            // cbosgst
            // 
            this.cbosgst.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cbosgst.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cbosgst.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbosgst.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbosgst.FormattingEnabled = true;
            this.cbosgst.Location = new System.Drawing.Point(160, 37);
            this.cbosgst.Name = "cbosgst";
            this.cbosgst.Size = new System.Drawing.Size(105, 26);
            this.cbosgst.TabIndex = 109;
            this.cbosgst.SelectedIndexChanged += new System.EventHandler(this.cbosgst_SelectedIndexChanged);
            this.cbosgst.SelectedValueChanged += new System.EventHandler(this.cbosgst_SelectedValueChanged);
            this.cbosgst.Click += new System.EventHandler(this.cbosgst_Click);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(15, 69);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(81, 18);
            this.label13.TabIndex = 107;
            this.label13.Text = "Short Name";
            // 
            // txtsname
            // 
            this.txtsname.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtsname.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtsname.Location = new System.Drawing.Point(19, 89);
            this.txtsname.Margin = new System.Windows.Forms.Padding(4);
            this.txtsname.Name = "txtsname";
            this.txtsname.Size = new System.Drawing.Size(552, 26);
            this.txtsname.TabIndex = 108;
            // 
            // Txthsnname
            // 
            this.Txthsnname.BackColor = System.Drawing.Color.White;
            this.Txthsnname.Enabled = false;
            this.Txthsnname.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txthsnname.Location = new System.Drawing.Point(18, 140);
            this.Txthsnname.Name = "Txthsnname";
            this.Txthsnname.ReadOnly = true;
            this.Txthsnname.Size = new System.Drawing.Size(553, 295);
            this.Txthsnname.TabIndex = 106;
            this.Txthsnname.Text = "";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(305, 140);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(44, 16);
            this.label10.TabIndex = 103;
            this.label10.Text = "CGST";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(19, 121);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(74, 18);
            this.label11.TabIndex = 97;
            this.label11.Text = "HSN Name";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(17, 20);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(69, 18);
            this.label12.TabIndex = 101;
            this.label12.Text = "HSN Code";
            // 
            // cbocgst
            // 
            this.cbocgst.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cbocgst.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cbocgst.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbocgst.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbocgst.FormattingEnabled = true;
            this.cbocgst.Location = new System.Drawing.Point(305, 172);
            this.cbocgst.Name = "cbocgst";
            this.cbocgst.Size = new System.Drawing.Size(62, 24);
            this.cbocgst.TabIndex = 102;
            // 
            // txthsn
            // 
            this.txthsn.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txthsn.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txthsn.Location = new System.Drawing.Point(20, 39);
            this.txthsn.Margin = new System.Windows.Forms.Padding(4);
            this.txthsn.Name = "txthsn";
            this.txthsn.ReadOnly = true;
            this.txthsn.Size = new System.Drawing.Size(136, 26);
            this.txthsn.TabIndex = 104;
            this.txthsn.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txthsn_KeyDown);
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(468, 172);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(39, 16);
            this.label15.TabIndex = 112;
            this.label15.Text = "IGST";
            this.label15.Visible = false;
            // 
            // cboigst
            // 
            this.cboigst.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboigst.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboigst.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboigst.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboigst.FormattingEnabled = true;
            this.cboigst.Location = new System.Drawing.Point(368, 169);
            this.cboigst.Name = "cboigst";
            this.cboigst.Size = new System.Drawing.Size(62, 24);
            this.cboigst.TabIndex = 111;
            this.cboigst.Visible = false;
            // 
            // txthsnid
            // 
            this.txthsnid.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txthsnid.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txthsnid.Location = new System.Drawing.Point(132, 169);
            this.txthsnid.Margin = new System.Windows.Forms.Padding(4);
            this.txthsnid.Name = "txthsnid";
            this.txthsnid.Size = new System.Drawing.Size(32, 22);
            this.txthsnid.TabIndex = 115;
            this.txthsnid.TextChanged += new System.EventHandler(this.txthsnid_TextChanged);
            this.txthsnid.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txthsnid_KeyDown);
            // 
            // panadd
            // 
            this.panadd.BackColor = System.Drawing.Color.White;
            this.panadd.Controls.Add(this.buttnext1);
            this.panadd.Controls.Add(this.chkact);
            this.panadd.Controls.Add(this.butedit);
            this.panadd.Controls.Add(this.btnadd);
            this.panadd.Controls.Add(this.butexit);
            this.panadd.Location = new System.Drawing.Point(-4, 454);
            this.panadd.Name = "panadd";
            this.panadd.Size = new System.Drawing.Size(585, 31);
            this.panadd.TabIndex = 207;
            // 
            // buttnext1
            // 
            this.buttnext1.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.buttnext1.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttnext1.Image = ((System.Drawing.Image)(resources.GetObject("buttnext1.Image")));
            this.buttnext1.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttnext1.Location = new System.Drawing.Point(526, 1);
            this.buttnext1.Name = "buttnext1";
            this.buttnext1.Size = new System.Drawing.Size(58, 30);
            this.buttnext1.TabIndex = 208;
            this.buttnext1.Text = "Exit";
            this.buttnext1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.buttnext1.UseVisualStyleBackColor = false;
            this.buttnext1.Click += new System.EventHandler(this.buttnext1_Click);
            // 
            // chkact
            // 
            this.chkact.AutoSize = true;
            this.chkact.BackColor = System.Drawing.Color.White;
            this.chkact.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkact.Location = new System.Drawing.Point(239, 7);
            this.chkact.Name = "chkact";
            this.chkact.Size = new System.Drawing.Size(65, 22);
            this.chkact.TabIndex = 187;
            this.chkact.Text = "Active";
            this.chkact.UseVisualStyleBackColor = false;
            this.chkact.CheckedChanged += new System.EventHandler(this.chkact_CheckedChanged_2);
            // 
            // butedit
            // 
            this.butedit.BackColor = System.Drawing.Color.White;
            this.butedit.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.butedit.Image = ((System.Drawing.Image)(resources.GetObject("butedit.Image")));
            this.butedit.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.butedit.Location = new System.Drawing.Point(397, 1);
            this.butedit.Name = "butedit";
            this.butedit.Size = new System.Drawing.Size(55, 30);
            this.butedit.TabIndex = 185;
            this.butedit.Text = "Edit";
            this.butedit.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.butedit.UseVisualStyleBackColor = false;
            this.butedit.Click += new System.EventHandler(this.butedit_Click);
            // 
            // btnadd
            // 
            this.btnadd.BackColor = System.Drawing.Color.White;
            this.btnadd.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.btnadd.FlatAppearance.BorderSize = 0;
            this.btnadd.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnadd.Image = ((System.Drawing.Image)(resources.GetObject("btnadd.Image")));
            this.btnadd.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnadd.Location = new System.Drawing.Point(310, 1);
            this.btnadd.Name = "btnadd";
            this.btnadd.Size = new System.Drawing.Size(87, 30);
            this.btnadd.TabIndex = 184;
            this.btnadd.Text = "Add new";
            this.btnadd.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnadd.UseVisualStyleBackColor = false;
            this.btnadd.Click += new System.EventHandler(this.btnadd_Click_1);
            // 
            // butexit
            // 
            this.butexit.BackColor = System.Drawing.Color.White;
            this.butexit.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.butexit.Image = ((System.Drawing.Image)(resources.GetObject("butexit.Image")));
            this.butexit.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.butexit.Location = new System.Drawing.Point(452, 1);
            this.butexit.Name = "butexit";
            this.butexit.Size = new System.Drawing.Size(74, 31);
            this.butexit.TabIndex = 186;
            this.butexit.Text = "Delete";
            this.butexit.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.butexit.UseVisualStyleBackColor = false;
            this.butexit.Click += new System.EventHandler(this.butexit_Click);
            // 
            // Hsnsvpan
            // 
            this.Hsnsvpan.BackColor = System.Drawing.Color.White;
            this.Hsnsvpan.Controls.Add(this.button4);
            this.Hsnsvpan.Controls.Add(this.chkhsn);
            this.Hsnsvpan.Controls.Add(this.buttonhsnsave);
            this.Hsnsvpan.Controls.Add(this.button5);
            this.Hsnsvpan.Location = new System.Drawing.Point(-1, 455);
            this.Hsnsvpan.Name = "Hsnsvpan";
            this.Hsnsvpan.Size = new System.Drawing.Size(582, 30);
            this.Hsnsvpan.TabIndex = 209;
            // 
            // button4
            // 
            this.button4.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.button4.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button4.Image = ((System.Drawing.Image)(resources.GetObject("button4.Image")));
            this.button4.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button4.Location = new System.Drawing.Point(213, 0);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(60, 30);
            this.button4.TabIndex = 209;
            this.button4.Text = "Exit";
            this.button4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button4.UseVisualStyleBackColor = false;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // chkhsn
            // 
            this.chkhsn.AutoSize = true;
            this.chkhsn.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkhsn.Location = new System.Drawing.Point(11, 7);
            this.chkhsn.Name = "chkhsn";
            this.chkhsn.Size = new System.Drawing.Size(65, 22);
            this.chkhsn.TabIndex = 108;
            this.chkhsn.Text = "Active";
            this.chkhsn.UseVisualStyleBackColor = true;
            // 
            // buttonhsnsave
            // 
            this.buttonhsnsave.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.buttonhsnsave.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonhsnsave.Image = ((System.Drawing.Image)(resources.GetObject("buttonhsnsave.Image")));
            this.buttonhsnsave.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttonhsnsave.Location = new System.Drawing.Point(79, 0);
            this.buttonhsnsave.Name = "buttonhsnsave";
            this.buttonhsnsave.Size = new System.Drawing.Size(64, 30);
            this.buttonhsnsave.TabIndex = 106;
            this.buttonhsnsave.Text = "Save";
            this.buttonhsnsave.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.buttonhsnsave.UseVisualStyleBackColor = false;
            this.buttonhsnsave.Click += new System.EventHandler(this.buttonhsnsave_Click_1);
            // 
            // button5
            // 
            this.button5.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.button5.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button5.Image = ((System.Drawing.Image)(resources.GetObject("button5.Image")));
            this.button5.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button5.Location = new System.Drawing.Point(144, 0);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(64, 30);
            this.button5.TabIndex = 107;
            this.button5.Text = "Back";
            this.button5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button5.UseVisualStyleBackColor = false;
            this.button5.Click += new System.EventHandler(this.button5_Click_1);
            // 
            // Taxsvpan
            // 
            this.Taxsvpan.BackColor = System.Drawing.Color.White;
            this.Taxsvpan.Controls.Add(this.chktax);
            this.Taxsvpan.Controls.Add(this.cmdexcieok);
            this.Taxsvpan.Controls.Add(this.button2);
            this.Taxsvpan.Controls.Add(this.button10);
            this.Taxsvpan.Location = new System.Drawing.Point(0, 455);
            this.Taxsvpan.Name = "Taxsvpan";
            this.Taxsvpan.Size = new System.Drawing.Size(581, 31);
            this.Taxsvpan.TabIndex = 210;
            // 
            // chktax
            // 
            this.chktax.AutoSize = true;
            this.chktax.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chktax.Location = new System.Drawing.Point(11, 7);
            this.chktax.Name = "chktax";
            this.chktax.Size = new System.Drawing.Size(57, 19);
            this.chktax.TabIndex = 211;
            this.chktax.Text = "Active";
            this.chktax.UseVisualStyleBackColor = true;
            // 
            // cmdexcieok
            // 
            this.cmdexcieok.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.cmdexcieok.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmdexcieok.Image = ((System.Drawing.Image)(resources.GetObject("cmdexcieok.Image")));
            this.cmdexcieok.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.cmdexcieok.Location = new System.Drawing.Point(79, 0);
            this.cmdexcieok.Name = "cmdexcieok";
            this.cmdexcieok.Size = new System.Drawing.Size(60, 31);
            this.cmdexcieok.TabIndex = 209;
            this.cmdexcieok.Text = "Save";
            this.cmdexcieok.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.cmdexcieok.UseVisualStyleBackColor = false;
            this.cmdexcieok.Click += new System.EventHandler(this.cmdexcieok_Click_2);
            // 
            // button2
            // 
            this.button2.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.button2.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button2.Image = ((System.Drawing.Image)(resources.GetObject("button2.Image")));
            this.button2.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button2.Location = new System.Drawing.Point(140, 1);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(60, 30);
            this.button2.TabIndex = 210;
            this.button2.Text = "Back";
            this.button2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button2.UseVisualStyleBackColor = false;
            this.button2.Click += new System.EventHandler(this.button2_Click_1);
            // 
            // button10
            // 
            this.button10.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.button10.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button10.Image = ((System.Drawing.Image)(resources.GetObject("button10.Image")));
            this.button10.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button10.Location = new System.Drawing.Point(202, 1);
            this.button10.Name = "button10";
            this.button10.Size = new System.Drawing.Size(54, 30);
            this.button10.TabIndex = 208;
            this.button10.Text = "Exit";
            this.button10.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button10.UseVisualStyleBackColor = false;
            this.button10.Click += new System.EventHandler(this.button10_Click_1);
            // 
            // taxpan
            // 
            this.taxpan.Controls.Add(this.txtcess);
            this.taxpan.Controls.Add(this.label8);
            this.taxpan.Controls.Add(this.txthecess);
            this.taxpan.Controls.Add(this.label7);
            this.taxpan.Controls.Add(this.txted);
            this.taxpan.Controls.Add(this.label5);
            this.taxpan.Controls.Add(this.txtexcise);
            this.taxpan.Controls.Add(this.label6);
            this.taxpan.Controls.Add(this.button1);
            this.taxpan.Controls.Add(this.button3);
            this.taxpan.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.taxpan.Location = new System.Drawing.Point(529, 198);
            this.taxpan.Name = "taxpan";
            this.taxpan.Size = new System.Drawing.Size(27, 21);
            this.taxpan.TabIndex = 212;
            this.taxpan.Visible = false;
            // 
            // txtcess
            // 
            this.txtcess.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtcess.Location = new System.Drawing.Point(241, 142);
            this.txtcess.Margin = new System.Windows.Forms.Padding(4);
            this.txtcess.Name = "txtcess";
            this.txtcess.Size = new System.Drawing.Size(117, 22);
            this.txtcess.TabIndex = 116;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(153, 144);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(66, 18);
            this.label8.TabIndex = 115;
            this.label8.Text = "Cess %";
            // 
            // txthecess
            // 
            this.txthecess.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txthecess.Location = new System.Drawing.Point(241, 187);
            this.txthecess.Margin = new System.Windows.Forms.Padding(4);
            this.txthecess.Name = "txthecess";
            this.txthecess.Size = new System.Drawing.Size(117, 22);
            this.txthecess.TabIndex = 114;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(130, 191);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(94, 18);
            this.label7.TabIndex = 113;
            this.label7.Text = "HE.Cess %";
            // 
            // txted
            // 
            this.txted.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txted.Location = new System.Drawing.Point(241, 95);
            this.txted.Margin = new System.Windows.Forms.Padding(4);
            this.txted.Name = "txted";
            this.txted.Size = new System.Drawing.Size(117, 22);
            this.txted.TabIndex = 112;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(160, 103);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(55, 18);
            this.label5.TabIndex = 111;
            this.label5.Text = "E D %";
            // 
            // txtexcise
            // 
            this.txtexcise.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtexcise.Location = new System.Drawing.Point(241, 56);
            this.txtexcise.Margin = new System.Windows.Forms.Padding(4);
            this.txtexcise.Name = "txtexcise";
            this.txtexcise.Size = new System.Drawing.Size(252, 22);
            this.txtexcise.TabIndex = 106;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(46, 56);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(188, 18);
            this.label6.TabIndex = 105;
            this.label6.Text = "Excise Duty Description";
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.button1.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.Image = ((System.Drawing.Image)(resources.GetObject("button1.Image")));
            this.button1.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button1.Location = new System.Drawing.Point(185, 260);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(90, 38);
            this.button1.TabIndex = 107;
            this.button1.Text = "Save";
            this.button1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button1.UseVisualStyleBackColor = false;
            // 
            // button3
            // 
            this.button3.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.button3.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button3.Image = ((System.Drawing.Image)(resources.GetObject("button3.Image")));
            this.button3.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button3.Location = new System.Drawing.Point(291, 260);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(93, 38);
            this.button3.TabIndex = 108;
            this.button3.Text = "Back";
            this.button3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button3.UseVisualStyleBackColor = false;
            // 
            // editsvpan
            // 
            this.editsvpan.BackColor = System.Drawing.Color.White;
            this.editsvpan.Controls.Add(this.button6);
            this.editsvpan.Controls.Add(this.btnsave);
            this.editsvpan.Controls.Add(this.btnaddrcan);
            this.editsvpan.Controls.Add(this.Chkedtact);
            this.editsvpan.Location = new System.Drawing.Point(0, 454);
            this.editsvpan.Name = "editsvpan";
            this.editsvpan.Size = new System.Drawing.Size(583, 31);
            this.editsvpan.TabIndex = 213;
            // 
            // button6
            // 
            this.button6.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.button6.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button6.Image = ((System.Drawing.Image)(resources.GetObject("button6.Image")));
            this.button6.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button6.Location = new System.Drawing.Point(202, 0);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(54, 30);
            this.button6.TabIndex = 209;
            this.button6.Text = "Exit";
            this.button6.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button6.UseVisualStyleBackColor = false;
            this.button6.Click += new System.EventHandler(this.button6_Click);
            // 
            // btnsave
            // 
            this.btnsave.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnsave.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnsave.Image = ((System.Drawing.Image)(resources.GetObject("btnsave.Image")));
            this.btnsave.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnsave.Location = new System.Drawing.Point(78, 0);
            this.btnsave.Name = "btnsave";
            this.btnsave.Size = new System.Drawing.Size(60, 30);
            this.btnsave.TabIndex = 99;
            this.btnsave.Text = "Save";
            this.btnsave.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnsave.UseVisualStyleBackColor = false;
            this.btnsave.Click += new System.EventHandler(this.btnsave_Click);
            // 
            // btnaddrcan
            // 
            this.btnaddrcan.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnaddrcan.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnaddrcan.Image = ((System.Drawing.Image)(resources.GetObject("btnaddrcan.Image")));
            this.btnaddrcan.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnaddrcan.Location = new System.Drawing.Point(143, 1);
            this.btnaddrcan.Name = "btnaddrcan";
            this.btnaddrcan.Size = new System.Drawing.Size(60, 30);
            this.btnaddrcan.TabIndex = 100;
            this.btnaddrcan.Text = "Back";
            this.btnaddrcan.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnaddrcan.UseVisualStyleBackColor = false;
            this.btnaddrcan.Click += new System.EventHandler(this.btnaddrcan_Click_3);
            // 
            // Chkedtact
            // 
            this.Chkedtact.AutoSize = true;
            this.Chkedtact.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Chkedtact.Location = new System.Drawing.Point(10, 7);
            this.Chkedtact.Name = "Chkedtact";
            this.Chkedtact.Size = new System.Drawing.Size(57, 19);
            this.Chkedtact.TabIndex = 98;
            this.Chkedtact.Text = "Active";
            this.Chkedtact.UseVisualStyleBackColor = true;
            // 
            // Frmitemgrp
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(584, 485);
            this.Controls.Add(this.taxpan);
            this.Controls.Add(this.Genpan);
            this.Controls.Add(this.HSNpan);
            this.Controls.Add(this.excisepan);
            this.Controls.Add(this.editsvpan);
            this.Controls.Add(this.panadd);
            this.Controls.Add(this.Hsnsvpan);
            this.Controls.Add(this.Taxsvpan);
            this.Controls.Add(this.EditPnl);
            this.Font = new System.Drawing.Font("Bookman Old Style", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Margin = new System.Windows.Forms.Padding(4);
            this.MaximizeBox = false;
            this.Name = "Frmitemgrp";
            this.Text = "Master";
            this.Load += new System.EventHandler(this.Frmitemgrp_Load);
            this.Genpan.ResumeLayout(false);
            this.Genpan.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.HFGP)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.EditPnl.ResumeLayout(false);
            this.EditPnl.PerformLayout();
            this.excisepan.ResumeLayout(false);
            this.excisepan.PerformLayout();
            this.HSNpan.ResumeLayout(false);
            this.HSNpan.PerformLayout();
            this.panadd.ResumeLayout(false);
            this.panadd.PerformLayout();
            this.Hsnsvpan.ResumeLayout(false);
            this.Hsnsvpan.PerformLayout();
            this.Taxsvpan.ResumeLayout(false);
            this.Taxsvpan.PerformLayout();
            this.taxpan.ResumeLayout(false);
            this.taxpan.PerformLayout();
            this.editsvpan.ResumeLayout(false);
            this.editsvpan.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel Genpan;
        private System.Windows.Forms.DataGridView HFGP;
        private System.Windows.Forms.TextBox txtscr1;
        private System.Windows.Forms.Panel EditPnl;
        private System.Windows.Forms.Label lblgen;
        private System.Windows.Forms.ComboBox cbogrp;
        private System.Windows.Forms.Label lblgrp;
        private System.Windows.Forms.Panel excisepan;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox cbotax;
        private System.Windows.Forms.TextBox txtax;
        private System.Windows.Forms.TextBox txtper;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Panel HSNpan;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.ComboBox cbocgst;
        private System.Windows.Forms.TextBox txthsn;
        private System.Windows.Forms.RichTextBox Txthsnname;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.ComboBox cboigst;
        private System.Windows.Forms.Label SGST;
        private System.Windows.Forms.ComboBox cbosgst;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox txtsname;
        private System.Windows.Forms.RichTextBox txtgen;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panadd;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label lblno1;
        private System.Windows.Forms.Label lblno2;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel3;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel2;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private System.Windows.Forms.Button buttnnvfst;
        private System.Windows.Forms.Button buttnnxtlft;
        private System.Windows.Forms.Button btnfinnxt;
        private System.Windows.Forms.Button buttrnxt;
        private System.Windows.Forms.Button buttnext1;
        private System.Windows.Forms.CheckBox chkact;
        private System.Windows.Forms.Button butexit;
        private System.Windows.Forms.Button butedit;
        private System.Windows.Forms.Button btnadd;
        private System.Windows.Forms.Panel Hsnsvpan;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.CheckBox chkhsn;
        private System.Windows.Forms.Button buttonhsnsave;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Panel Taxsvpan;
        private System.Windows.Forms.CheckBox chktax;
        private System.Windows.Forms.Button cmdexcieok;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button10;
        private System.Windows.Forms.Panel taxpan;
        private System.Windows.Forms.TextBox txtcess;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txthecess;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txted;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtexcise;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Panel editsvpan;
        private System.Windows.Forms.Button btnsave;
        private System.Windows.Forms.Button btnaddrcan;
        private System.Windows.Forms.CheckBox Chkedtact;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.RichTextBox richTextBox1;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.RichTextBox txthsss;
        private System.Windows.Forms.Label lblhsss;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.TextBox txtgstper;
        private System.Windows.Forms.TextBox txthsnid;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox txttax;
        private System.Windows.Forms.ComboBox cbotype;
        private System.Windows.Forms.TextBox txthsncode;
        private System.Windows.Forms.TextBox txtscr2;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.DateTimePicker DTPDOCDT;
    }
}