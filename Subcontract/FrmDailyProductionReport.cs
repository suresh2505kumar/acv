﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ACV
{
    public partial class FrmDailyProductionReport : Form
    {
        public FrmDailyProductionReport()
        {
            InitializeComponent();
            sfDataGrid1.Style.HeaderStyle.BackColor = Color.DeepSkyBlue;
            sfDataGrid1.Style.HeaderStyle.Font.FontStyle = FontStyle.Bold;
            sfDataGrid1.Style.CellStyle.Font.FontStyle = FontStyle.Bold;
        }
        SqlConnection conn = new SqlConnection("Data Source=" + Genclass.data2 + "; Initial Catalog=" + Genclass.data5 + ";User id=" + Genclass.data3 + ";Password=" + Genclass.data4 + "");
        SQLDBHelper db = new SQLDBHelper();
        private void FrmDailyProductionReport_Load(object sender, EventArgs e)
        {

        }

        private void BtnSts_Click(object sender, EventArgs e)
        {
            SqlParameter[] sqlParameters = {
                        new SqlParameter("@FDT", Convert.ToDateTime(dateTimePicker1.Text).ToString("yyyy-MM-dd")),
                        new SqlParameter("@TDT",Convert.ToDateTime(dateTimePicker1.Text).ToString("yyyy-MM-dd"))
                    };
            DataTable data = db.GetDataWithParam(CommandType.StoredProcedure, "PROC_RPT_DAILYSORTSTOCK", sqlParameters, conn);
            sfDataGrid1.DataSource = null;
            sfDataGrid1.DataSource = data;
            sfDataGrid1.AutoSizeColumnsMode = Syncfusion.WinForms.DataGrid.Enums.AutoSizeColumnsMode.AllCells;
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            if(checkBox1.Checked == false)
            {
                panel1.Visible = false;
            }
            else
            {
                panel1.Visible = true;
            }
        }

        private void BtnExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
