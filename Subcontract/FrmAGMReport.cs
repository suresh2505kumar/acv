﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ACV
{
    public partial class FrmAGMReport : Form
    {
        public FrmAGMReport()
        {
            InitializeComponent();
        }

        private void FrmAGMReport_Load(object sender, EventArgs e)
        {
            LoadDataGrid();
        }
        SqlConnection conn = new SqlConnection("Data Source=" + Genclass.data2 + "; Initial Catalog=" + Genclass.data5 + ";User id=" + Genclass.data3 + ";Password=" + Genclass.data4 + "");
        protected void LoadDataGrid()
        {
            try
            {
                SqlParameter[] sqlParameters = {
                    new SqlParameter("@DT",DateTime.Now.Date.ToString("yyyyMMdd"))
                };
                SQLDBHelper db = new SQLDBHelper();
                DataTable dt = db.GetDataWithParam(CommandType.StoredProcedure, "RPT_FABRICSTKAGING", sqlParameters, conn);
                SfdDataGridRpt.DataSource = dt;
                SfdDataGridRpt.Columns[4].Visible = false;
                SfdDataGridRpt.Columns[8].Visible = false;
                SfdDataGridRpt.Columns[12].Visible = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.StackTrace);
                return;
            }
        }
    }
}
